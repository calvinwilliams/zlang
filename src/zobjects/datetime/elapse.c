/* Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "zobjects_datetime.h"

struct ZlangDirectProperty_elapse
{
	DECLARE_BEGIN_TIMEVAL;
	struct timeval	diff ;
} ;

ZlangInvokeFunction ZlangInvokeFunction_elapse_GetBeginTimeval;
int ZlangInvokeFunction_elapse_GetBeginTimeval( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_elapse	*elapse_direct_prop = GetObjectDirectProperty(obj) ;
#if defined(_WIN32)
	SYSTEMTIME				st ;
#endif
	struct ZlangObject			*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	
#if defined(__linux__)
	gettimeofday( & (elapse_direct_prop->BEGIN_TIMEVAL) , NULL );
#elif defined(_WIN32)
	elapse_direct_prop->BEGIN_TIMEVAL.tv_sec = (long)time( NULL ) ;
	GetLocalTime( & st );
	elapse_direct_prop->BEGIN_TIMEVAL.tv_usec = st.wMilliseconds ;
#endif

	CallRuntimeFunction_bool_SetBoolValue( rt , out1 , TRUE );
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_elapse_GetEndTimevalAndDiffElapse;
int ZlangInvokeFunction_elapse_GetEndTimevalAndDiffElapse( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_elapse	*elapse_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	
	GET_END_TIMEVAL_AND_DIFF_ELAPSE( elapse_direct_prop->diff , elapse_direct_prop->BEGIN_TIMEVAL )
	
	CallRuntimeFunction_int_SetIntValue( rt , out1 , elapse_direct_prop->diff.tv_sec*1000000+elapse_direct_prop->diff.tv_usec );
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_elapse_GetElapse;
int ZlangInvokeFunction_elapse_GetElapse( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_elapse	*elapse_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	
	CallRuntimeFunction_int_SetIntValue( rt , out1 , elapse_direct_prop->diff.tv_sec*1000000+elapse_direct_prop->diff.tv_usec );
	return 0;
}

ZlangCreateDirectPropertyFunction ZlangCreateDirectProperty_elapse;
void *ZlangCreateDirectProperty_elapse( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_elapse	*elapse_prop = NULL ;
	
	elapse_prop = (struct ZlangDirectProperty_elapse *)ZLMALLOC( sizeof(struct ZlangDirectProperty_elapse) ) ;
	if( elapse_prop == NULL )
	{
		SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_ALLOC , "alloc memory for entity" )
		return NULL;
	}
	memset( elapse_prop , 0x00 , sizeof(struct ZlangDirectProperty_elapse) );
	
	return elapse_prop;
}

ZlangDestroyDirectPropertyFunction ZlangDestroyDirectProperty_elapse;
void ZlangDestroyDirectProperty_elapse( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_elapse	*elapse_direct_prop = GetObjectDirectProperty(obj) ;
	
	ZLFREE( elapse_direct_prop );
	
	return;
}

ZlangToStringFunction ZlangToString_elapse;
int ZlangToString_elapse( struct ZlangRuntime *rt , struct ZlangObject *obj , struct ZlangObject **tostr_obj )
{
	struct ZlangDirectProperty_elapse	*elapse_direct_prop = GetObjectDirectProperty(obj) ;
	char					buf[ 10+1+6 + 1 ] = "" ;
	int32_t					str_len ;
	
	str_len = snprintf( buf , sizeof(buf)-1 , "%ld.%06ld" , elapse_direct_prop->diff.tv_sec,elapse_direct_prop->diff.tv_usec ) ;
	CallRuntimeFunction_string_SetStringValue( rt , (*tostr_obj) , buf , str_len ) ;
	
	return 0;
}

ZlangSummarizeDirectPropertySizeFunction ZlangSummarizeDirectPropertySize_elapse;
void ZlangSummarizeDirectPropertySize_elapse( struct ZlangRuntime *rt , struct ZlangObject *obj , size_t *summarized_obj_size , size_t *summarized_direct_prop_size )
{
	SUMMARIZE_SIZE( summarized_direct_prop_size , sizeof(struct ZlangDirectProperty_elapse) )
	return;
}

static struct ZlangDirectFunctions direct_funcs_elapse =
	{
		ZLANG_OBJECT_elapse , /* char *ancestor_name */
		
		ZlangCreateDirectProperty_elapse , /* ZlangCreateDirectPropertyFunction *create_entity_func */
		ZlangDestroyDirectProperty_elapse , /* ZlangDestroyDirectPropertyFunction *destroy_entity_func */
		
		NULL , /* ZlangFromCharPtrFunction *from_char_ptr_func */
		ZlangToString_elapse , /* ZlangToStringFunction *to_string_func */
		NULL , /* ZlangFromDataPtrFunction *from_data_ptr_func */
		NULL , /* ZlangGetDataPtrFunction *get_data_ptr_func */
		
		NULL , /* ZlangOperatorFunction *oper_PLUS_func */
		NULL , /* ZlangOperatorFunction *oper_MINUS_func */
		NULL , /* ZlangOperatorFunction *oper_MUL_func */
		NULL , /* ZlangOperatorFunction *oper_DIV_func */
		NULL , /* ZlangOperatorFunction *oper_MOD_func */
		
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_NEGATIVE_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_NOT_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_BIT_REVERSE_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_PLUS_PLUS_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_MINUS_MINUS_func */
		
		NULL , /* ZlangCompareFunction *comp_EGUAL_func */
		NULL , /* ZlangCompareFunction *comp_NOTEGUAL_func */
		NULL , /* ZlangCompareFunction *comp_LT_func */
		NULL , /* ZlangCompareFunction *comp_LE_func */
		NULL , /* ZlangCompareFunction *comp_GT_func */
		NULL , /* ZlangCompareFunction *comp_GE_func */
		
		NULL , /* ZlangLogicFunction *logic_AND_func */
		NULL , /* ZlangLogicFunction *logic_OR_func */
		
		NULL , /* ZlangLogicFunction *bit_AND_func */
		NULL , /* ZlangLogicFunction *bit_XOR_func */
		NULL , /* ZlangLogicFunction *bit_OR_func */
		NULL , /* ZlangLogicFunction *bit_MOVELEFT_func */
		NULL , /* ZlangLogicFunction *bit_MOVERIGHT_func */
		
		ZlangSummarizeDirectPropertySize_elapse , /* ZlangSummarizeDirectPropertySizeFunction *summarize_direct_prop_size_func */
	} ;

ZlangImportObjectFunction ZlangImportObject_elapse;
struct ZlangObject *ZlangImportObject_elapse( struct ZlangRuntime *rt )
{
	struct ZlangObject	*obj = NULL ;
	struct ZlangFunction	*func = NULL ;
	int			nret = 0 ;
	
	nret = ImportObject( rt , & obj , ZLANG_OBJECT_elapse , & direct_funcs_elapse , sizeof(struct ZlangDirectFunctions) , NULL ) ;
	if( nret )
	{
		SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_LINK_FUNC_TO_ENTITY , "import object to global objects heap" )
		return NULL;
	}
	
	/* elapse.GetBeginTimeval() */
	func = AddFunctionAndParametersInObject( rt , obj , "GetBeginTimeval" , "GetBeginTimeval()" , ZlangInvokeFunction_elapse_GetBeginTimeval , ZLANG_OBJECT_bool , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* elapse.GetEndTimevalAndDiffElapse() */
	func = AddFunctionAndParametersInObject( rt , obj , "GetEndTimevalAndDiffElapse" , "GetEndTimevalAndDiffElapse()" , ZlangInvokeFunction_elapse_GetEndTimevalAndDiffElapse , ZLANG_OBJECT_int , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* elapse.GetElapse() */
	func = AddFunctionAndParametersInObject( rt , obj , "GetElapse" , "GetElapse()" , ZlangInvokeFunction_elapse_GetElapse , ZLANG_OBJECT_int , NULL ) ;
	if( func == NULL )
		return NULL;
	
	return obj ;
}

