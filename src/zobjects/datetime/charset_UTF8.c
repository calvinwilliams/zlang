﻿/* Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

static struct ZlangCharsetAlias		g_zlang_charset_aliases_UTF8[] = {
		{ ZLANG_CHARSET_UTF8 , "日期时间" , "datetime" } ,
		{ ZLANG_CHARSET_UTF8 , "吸附月末" , "ATTRACT_END_OF_MONTH" } ,
		{ ZLANG_CHARSET_UTF8 , "不吸附月末" , "NO_ATTRACT_END_OF_MONTH" } ,
		{ ZLANG_CHARSET_UTF8 , "得到当前日期时间" , "GetNow" } ,
		{ ZLANG_CHARSET_UTF8 , "从格式化数据设置日期时间" , "GetFromFormat" } ,
		{ ZLANG_CHARSET_UTF8 , "年" , "Year" } ,
		{ ZLANG_CHARSET_UTF8 , "月" , "Month" } ,
		{ ZLANG_CHARSET_UTF8 , "日" , "Day" } ,
		{ ZLANG_CHARSET_UTF8 , "时" , "Hour" } ,
		{ ZLANG_CHARSET_UTF8 , "分" , "Minute" } ,
		{ ZLANG_CHARSET_UTF8 , "秒" , "Second" } ,
		{ ZLANG_CHARSET_UTF8 , "微秒" , "Microsecond" } ,
		{ ZLANG_CHARSET_UTF8 , "格式化字符串" , "FormatString" } ,
		{ ZLANG_CHARSET_UTF8 , "偏移年" , "OffsetYears" } ,
		{ ZLANG_CHARSET_UTF8 , "偏移月" , "OffsetMonths" } ,
		{ ZLANG_CHARSET_UTF8 , "偏移日" , "OffsetDays" } ,
		{ ZLANG_CHARSET_UTF8 , "偏移时" , "OffsetHours" } ,
		{ ZLANG_CHARSET_UTF8 , "偏移分" , "OffsetMinutes" } ,
		{ ZLANG_CHARSET_UTF8 , "偏移秒" , "OffsetSeconds" } ,
		{ 0 , NULL , NULL } ,
	} ;

