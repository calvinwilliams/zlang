/* Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef _H_LIBZLANG_DATETIME_
#define _H_LIBZLANG_DATETIME_

#include "zlang.h"

#include <time.h>
#if defined(__linux__)
#define _XOPEN_SOURCE
#define __USE_XOPEN
#include <sys/time.h>
char *strptime(const char *s, const char *format, struct tm *tm);
#elif defined(_WIN32)
#include <windows.h>
#endif

struct ZlangObject_date;

/*
#define ZLANG_OBJECT_date	"date"
*/
#define ZLANG_OBJECT_datetime	"datetime"
#define ZLANG_OBJECT_elapse	"elapse"

DLLEXPORT ZlangImportObjectsFunction ZlangImportObjects;
int ZlangImportObjects( struct ZlangRuntime *rt );

#endif

