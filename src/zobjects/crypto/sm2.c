/* Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "zobjects_crypto.h"

#ifdef SN_sm2
#ifndef OPENSSL_NO_SM2

ZlangInvokeFunction ZlangInvokeFunction_sm2_Sign_sm2key_string;
int ZlangInvokeFunction_sm2_Sign_sm2key_string( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangObject			*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject			*in2 = GetInputParameterInLocalObjectStack(rt,2) ;
	struct ZlangObject			*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	struct ZlangDirectProperty_sm2key	*sm2key_direct_prop = NULL ;
	char					*data = NULL ;
	int32_t					data_len ;
	EVP_MD_CTX				*ctx = NULL ;
	int32_t					prepare_len ;
	char					**sign_data = NULL ;
	int32_t					*sign_data_len = NULL ;
	unsigned int				len ;
	int					nret = 0 ;
	
	CallRuntimeFunction_string_GetStringValue( rt , in1 , & data , & data_len );
	if( data == NULL || data_len < 0 )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "data or data_len invalid" );
		UnreferObject( rt , out1 );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_DATA_OR_DATA_LEN_INVALID );
	}
	
	sm2key_direct_prop = GetObjectDirectProperty(in2) ;
	if( sm2key_direct_prop->has_prikey == TRUE )
	{
		;
	}
	else
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "sm2key hasn't private key" );
		UnreferObject( rt , out1 );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_SM2KEY_HASNT_PRIVATE_KEY );
	}
	
	ctx = EVP_MD_CTX_new() ;
	if( ctx == NULL )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "EVP_MD_CTX_new failed" );
		UnreferObject( rt , out1 );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_EVP_MD_CTX_NEW_FAILED );
	}
	
	nret = EVP_SignInit( ctx , EVP_sm3() ) ;
	if( nret != 1 )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "EVP_SignInit failed[%d]" , nret );
		EVP_MD_CTX_free( ctx );
		UnreferObject( rt , out1 );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_EVP_SIGNINIT_FAILED );
	}
	
	nret = EVP_SignUpdate( ctx , data , data_len ) ;
	if( nret != 1 )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "EVP_SignUpdate failed[%d]" , nret );
		EVP_MD_CTX_free( ctx );
		UnreferObject( rt , out1 );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_EVP_SIGNUPDATE_FAILED );
	}
	
	CallRuntimeFunction_string_Clear( rt , out1 );
	prepare_len = EVP_PKEY_size(sm2key_direct_prop->pkey) ; 
	CallRuntimeFunction_string_PrepareBuffer( rt , out1 , prepare_len );
	CallRuntimeFunction_string_GetDirectPropertiesPtr( rt , out1 , & sign_data , NULL , & sign_data_len );
	
	nret = EVP_SignFinal( ctx , (unsigned char *)(*sign_data) , & len , sm2key_direct_prop->pkey ) ;
	if( nret != 1 )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "EVP_SignFinal failed[%d]" , nret );
		EVP_MD_CTX_free( ctx );
		UnreferObject( rt , out1 );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_EVP_SIGNFINAL_FAILED );
	}
	(*sign_data_len) = len ;
	(*sign_data)[(*sign_data_len)] = '\0' ;
	
	EVP_MD_CTX_free( ctx );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_sm2_Verify_sm2key_string_string;
int ZlangInvokeFunction_sm2_Verify_sm2key_string_string( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangObject			*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject			*in2 = GetInputParameterInLocalObjectStack(rt,2) ;
	struct ZlangObject			*in3 = GetInputParameterInLocalObjectStack(rt,3) ;
	struct ZlangObject			*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	struct ZlangDirectProperty_sm2key	*sm2key_direct_prop = NULL ;
	char					*data = NULL ;
	int32_t					data_len ;
	char					*sign_data = NULL ;
	int32_t					sign_data_len ;
	EVP_MD_CTX				*ctx = NULL ;
	int					nret = 0 ;
	
	CallRuntimeFunction_string_GetStringValue( rt , in1 , & data , & data_len );
	if( data == NULL || data_len < 0 )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "data or data_len invalid" );
		UnreferObject( rt , out1 );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_DATA_OR_DATA_LEN_INVALID );
	}
	
	CallRuntimeFunction_string_GetStringValue( rt , in2 , & sign_data , & sign_data_len );
	if( sign_data == NULL || sign_data_len < 0 )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "data or data_len invalid" );
		UnreferObject( rt , out1 );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_SIGN_DATA_OR_SIGN_DATA_LEN_INVALID );
	}
	
	sm2key_direct_prop = GetObjectDirectProperty(in3) ;
	if( sm2key_direct_prop->has_pubkey == TRUE )
	{
		;
	}
	else
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "sm2key hasn't private key" );
		UnreferObject( rt , out1 );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_SM2KEY_HASNT_PUBLIC_KEY );
	}
	
	ctx = EVP_MD_CTX_new() ;
	if( ctx == NULL )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "EVP_MD_CTX_new failed" );
		UnreferObject( rt , out1 );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_EVP_MD_CTX_NEW_FAILED );
	}
	
	nret = EVP_VerifyInit( ctx , EVP_sm3() ) ;
	if( nret != 1 )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "EVP_VerifyInit failed[%d]" , nret );
		EVP_MD_CTX_free( ctx );
		UnreferObject( rt , out1 );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_EVP_VERIFYSIGNINIT_FAILED );
	}
	
	nret = EVP_VerifyUpdate( ctx , data , data_len ) ;
	if( nret != 1 )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "EVP_VerifyUpdate failed[%d]" , nret );
		EVP_MD_CTX_free( ctx );
		UnreferObject( rt , out1 );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_EVP_VERIFYUPDATE_FAILED );
	}
	
	nret = EVP_VerifyFinal( ctx , (unsigned char *)sign_data , sign_data_len , sm2key_direct_prop->pkey ) ;
	if( nret == 1 )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "EVP_VerifyFinal correct" );
		CallRuntimeFunction_bool_SetBoolValue( rt , out1 , TRUE );
	}
	else if( nret == 0 )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "EVP_VerifyFinal incorrect[%d]" , nret );
		CallRuntimeFunction_bool_SetBoolValue( rt , out1 , FALSE );
	}
	else
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "EVP_VerifyFinal failed[%d]" , nret );
		EVP_MD_CTX_free( ctx );
		UnreferObject( rt , out1 );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_EVP_VERIFYFINAL_FAILED );
	}
	
	EVP_MD_CTX_free( ctx );
	
	return 0;
}

ZlangCreateDirectPropertyFunction ZlangCreateDirectProperty_sm2;
void *ZlangCreateDirectProperty_sm2( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_sm2	*sm2_direct_prop = NULL ;
	
	sm2_direct_prop = (struct ZlangDirectProperty_sm2 *)ZLMALLOC( sizeof(struct ZlangDirectProperty_sm2) ) ;
	if( sm2_direct_prop == NULL )
	{
		SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_ALLOC , "alloc memory for entity" )
		return NULL;
	}
	memset( sm2_direct_prop , 0x00 , sizeof(struct ZlangDirectProperty_sm2) );
	
	return sm2_direct_prop;
}

ZlangDestroyDirectPropertyFunction ZlangDestroyDirectProperty_sm2;
void ZlangDestroyDirectProperty_sm2( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_sm2	*sm2_direct_prop = GetObjectDirectProperty(obj) ;
	
	ZLFREE( sm2_direct_prop );
	
	return;
}

ZlangSummarizeDirectPropertySizeFunction ZlangSummarizeDirectPropertySize_sm2;
void ZlangSummarizeDirectPropertySize_sm2( struct ZlangRuntime *rt , struct ZlangObject *obj , size_t *summarized_obj_size , size_t *summarized_direct_prop_size )
{
	SUMMARIZE_SIZE( summarized_direct_prop_size , sizeof(struct ZlangDirectProperty_sm2) )
	return;
}

static struct ZlangDirectFunctions direct_funcs_sm2 =
	{
		ZLANG_OBJECT_sm2 , /* char *ancestor_name */
		
		ZlangCreateDirectProperty_sm2 , /* ZlangCreateDirectPropertyFunction *create_entity_func */
		ZlangDestroyDirectProperty_sm2 , /* ZlangDestroyDirectPropertyFunction *destroy_entity_func */
		
		NULL , /* ZlangFromCharPtrFunction *from_char_ptr_func */
		NULL , /* ZlangToStringFunction *to_string_func */
		NULL , /* ZlangFromDataPtrFunction *from_data_ptr_func */
		NULL , /* ZlangGetDataPtrFunction *get_data_ptr_func */
		
		NULL , /* ZlangOperatorFunction *oper_PLUS_func */
		NULL , /* ZlangOperatorFunction *oper_MINUS_func */
		NULL , /* ZlangOperatorFunction *oper_MUL_func */
		NULL , /* ZlangOperatorFunction *oper_DIV_func */
		NULL , /* ZlangOperatorFunction *oper_MOD_func */
		
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_NEGATIVE_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_NOT_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_BIT_REVERSE_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_PLUS_PLUS_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_MINUS_MINUS_func */
		
		NULL , /* ZlangCompareFunction *comp_EGUAL_func */
		NULL , /* ZlangCompareFunction *comp_NOTEGUAL_func */
		NULL , /* ZlangCompareFunction *comp_LT_func */
		NULL , /* ZlangCompareFunction *comp_LE_func */
		NULL , /* ZlangCompareFunction *comp_GT_func */
		NULL , /* ZlangCompareFunction *comp_GE_func */
		
		NULL , /* ZlangLogicFunction *logic_AND_func */
		NULL , /* ZlangLogicFunction *logic_OR_func */
		
		NULL , /* ZlangLogicFunction *bit_AND_func */
		NULL , /* ZlangLogicFunction *bit_XOR_func */
		NULL , /* ZlangLogicFunction *bit_OR_func */
		NULL , /* ZlangLogicFunction *bit_MOVELEFT_func */
		NULL , /* ZlangLogicFunction *bit_MOVERIGHT_func */
		
		ZlangSummarizeDirectPropertySize_sm2 , /* ZlangSummarizeDirectPropertySizeFunction *summarize_direct_prop_size_func */
	} ;

ZlangImportObjectFunction ZlangImportObject_sm2;
struct ZlangObject *ZlangImportObject_sm2( struct ZlangRuntime *rt )
{
	struct ZlangObject	*obj = NULL ;
	struct ZlangFunction	*func = NULL ;
	int			nret = 0 ;
	
	nret = ImportObject( rt , & obj , ZLANG_OBJECT_sm2 , & direct_funcs_sm2 , sizeof(struct ZlangDirectFunctions) , NULL ) ;
	if( nret )
	{
		SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_LINK_FUNC_TO_ENTITY , "import object to global objects heap" )
		return NULL;
	}
	
	/* sm2.Sign(string,sm2key) */
	func = AddFunctionAndParametersInObject( rt , obj , "Sign" , "Sign(string,sm2key)" , ZlangInvokeFunction_sm2_Sign_sm2key_string , ZLANG_OBJECT_string , ZLANG_OBJECT_string,NULL, ZLANG_OBJECT_sm2key,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* sm2.Verify(string,string,sm2key) */
	func = AddFunctionAndParametersInObject( rt , obj , "Verify" , "Verify(string,string,sm2key)" , ZlangInvokeFunction_sm2_Verify_sm2key_string_string , ZLANG_OBJECT_bool , ZLANG_OBJECT_string,NULL , ZLANG_OBJECT_string,NULL , ZLANG_OBJECT_sm2key,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	return obj ;
}

#endif
#endif

