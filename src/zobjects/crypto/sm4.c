/* Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "zobjects_crypto.h"

#ifdef SN_sm4_cbc
#ifndef OPENSSL_NO_SM4

struct ZlangDirectProperty_sm4
{
	int	dummy ;
} ;

ZlangInvokeFunction ZlangInvokeFunction_sm4_Encrypt_int_int_string_string_int;
int ZlangInvokeFunction_sm4_Encrypt_int_int_string_string_int( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangObject	*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject	*in2 = GetInputParameterInLocalObjectStack(rt,2) ;
	struct ZlangObject	*in3 = GetInputParameterInLocalObjectStack(rt,3) ;
	struct ZlangObject	*in4 = GetInputParameterInLocalObjectStack(rt,4) ;
	struct ZlangObject	*in5 = GetInputParameterInLocalObjectStack(rt,5) ;
	struct ZlangObject	*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	int32_t			mode ;
	int32_t			key_len_type ;
	char			*key = NULL ;
	int32_t			key_len ;
	char			*dec = NULL ;
	int32_t			dec_len ;
	int32_t			padding_type ;
	char			iv[ 16 ] ;
	int			nret = 0 ;
	
	CallRuntimeFunction_int_GetIntValue( rt , in1 , & mode );
	
	CallRuntimeFunction_int_GetIntValue( rt , in2 , & key_len_type );
	if( key_len_type != CRYPTO_KEY_128BITS )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "key_len_type[%"PRIi32"] invalid" , key_len_type )
		UnreferObject( rt , out1 );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_KEY_LEN_TYPE_INVALID );
	}
	
	CallRuntimeFunction_string_GetStringValue( rt , in3 , & key , & key_len );
	if( CheckKeyLen( key_len_type , key_len ) == FALSE )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "key_len[%"PRIi32"] not matched key_len_type[%"PRIi32"]" , key_len , key_len_type )
		UnreferObject( rt , out1 );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_KEY_LEN_NOT_MATCHED_KEY_LEN_TYPE );
	}
	
	CallRuntimeFunction_string_GetStringValue( rt , in4 , & dec , & dec_len );
	
	CallRuntimeFunction_int_GetIntValue( rt , in5 , & padding_type );
	if( padding_type != CRYPTO_PKCS7_PADDING )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "padding_type[%"PRIi32"] invalid" , padding_type )
		UnreferObject( rt , out1 );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_PADDING_TYPE_INVALID );
	}
	
	if( mode == CRYPTO_ECB_MODE )
	{
		nret = EvpCipherEncrypt( rt , EVP_sm4_ecb() , key , NULL , dec , dec_len , out1 ) ;
		if( nret )
		{
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "EvpCipherEncrypt failed[%d]" , nret )
			UnreferObject( rt , out1 );
			return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_EVP_CIPHER_ENCRYPT );
		}
	}
	else
	{
		memset( iv , 0x00 , sizeof(iv) );
		nret = EvpCipherEncrypt( rt , EVP_sm4_cbc() , key , iv , dec , dec_len , out1 ) ;
		if( nret )
		{
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "EvpCipherEncrypt failed[%d]" , nret )
			UnreferObject( rt , out1 );
			return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_EVP_CIPHER_ENCRYPT );
		}
	}
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "EvpCipherEncrypt ok" )
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_sm4_Decrypt_int_int_string_string_int;
int ZlangInvokeFunction_sm4_Decrypt_int_int_string_string_int( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangObject	*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject	*in2 = GetInputParameterInLocalObjectStack(rt,2) ;
	struct ZlangObject	*in3 = GetInputParameterInLocalObjectStack(rt,3) ;
	struct ZlangObject	*in4 = GetInputParameterInLocalObjectStack(rt,4) ;
	struct ZlangObject	*in5 = GetInputParameterInLocalObjectStack(rt,5) ;
	struct ZlangObject	*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	int32_t			mode ;
	int32_t			key_len_type ;
	char			*key = NULL ;
	int32_t			key_len ;
	char			*enc = NULL ;
	int32_t			enc_len ;
	int32_t			padding_type ;
	char			iv[ 16 ] ;
	int			nret = 0 ;
	
	CallRuntimeFunction_int_GetIntValue( rt , in1 , & mode );
	
	CallRuntimeFunction_int_GetIntValue( rt , in2 , & key_len_type );
	if( key_len_type != CRYPTO_KEY_128BITS )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "key_len_type[%"PRIi32"] invalid" , key_len_type )
		UnreferObject( rt , out1 );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_KEY_LEN_TYPE_INVALID );
	}
	
	CallRuntimeFunction_string_GetStringValue( rt , in3 , & key , & key_len );
	if( CheckKeyLen( key_len_type , key_len ) == FALSE )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "key_len[%"PRIi32"] not matched key_len_type[%"PRIi32"]" , key_len , key_len_type )
		UnreferObject( rt , out1 );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_KEY_LEN_NOT_MATCHED_KEY_LEN_TYPE );
	}
	
	CallRuntimeFunction_string_GetStringValue( rt , in4 , & enc , & enc_len );
	if( enc_len % 16 != 0 )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "enc_len[%"PRIi32"] must be multiple of 16" , enc_len )
		UnreferObject( rt , out1 );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_ENC_LEN_MUST_BE_MULTIPLE_OF_16 );
	}
	
	CallRuntimeFunction_int_GetIntValue( rt , in5 , & padding_type );
	if( padding_type != CRYPTO_PKCS7_PADDING )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "padding_type[%"PRIi32"] invalid" , padding_type )
		UnreferObject( rt , out1 );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_PADDING_TYPE_INVALID );
	}
	
	if( mode == CRYPTO_ECB_MODE )
	{
		nret = EvpCipherDecrypt( rt , EVP_sm4_ecb() , key , NULL , enc , enc_len , out1 ) ;
		if( nret )
		{
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "EvpCipherDecrypt failed[%d]" , nret )
			UnreferObject( rt , out1 );
			return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_EVP_CIPHER_DECRYPT );
		}
	}
	else
	{
		memset( iv , 0x00 , sizeof(iv) );
		nret = EvpCipherDecrypt( rt , EVP_sm4_cbc() , key , iv , enc , enc_len , out1 ) ;
		if( nret )
		{
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "EvpCipherDecrypt failed[%d]" , nret )
			UnreferObject( rt , out1 );
			return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_EVP_CIPHER_DECRYPT );
		}
	}
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "EvpCipherDecrypt ok" )
	
	return 0;
}

ZlangCreateDirectPropertyFunction ZlangCreateDirectProperty_sm4;
void *ZlangCreateDirectProperty_sm4( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_sm4	*sm4_direct_prop = NULL ;
	
	sm4_direct_prop = (struct ZlangDirectProperty_sm4 *)ZLMALLOC( sizeof(struct ZlangDirectProperty_sm4) ) ;
	if( sm4_direct_prop == NULL )
	{
		SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_ALLOC , "alloc memory for entity" )
		return NULL;
	}
	memset( sm4_direct_prop , 0x00 , sizeof(struct ZlangDirectProperty_sm4) );
	
	return sm4_direct_prop;
}

ZlangDestroyDirectPropertyFunction ZlangDestroyDirectProperty_sm4;
void ZlangDestroyDirectProperty_sm4( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_sm4	*sm4_direct_prop = GetObjectDirectProperty(obj) ;
	
	ZLFREE( sm4_direct_prop );
	
	return;
}

ZlangSummarizeDirectPropertySizeFunction ZlangSummarizeDirectPropertySize_sm4;
void ZlangSummarizeDirectPropertySize_sm4( struct ZlangRuntime *rt , struct ZlangObject *obj , size_t *summarized_obj_size , size_t *summarized_direct_prop_size )
{
	SUMMARIZE_SIZE( summarized_direct_prop_size , sizeof(struct ZlangDirectProperty_sm4) )
	return;
}

static struct ZlangDirectFunctions direct_funcs_sm4 =
	{
		ZLANG_OBJECT_sm4 , /* char *ancestor_name */
		
		ZlangCreateDirectProperty_sm4 , /* ZlangCreateDirectPropertyFunction *create_entity_func */
		ZlangDestroyDirectProperty_sm4 , /* ZlangDestroyDirectPropertyFunction *sm4troy_entity_func */
		
		NULL , /* ZlangFromCharPtrFunction *from_char_ptr_func */
		NULL , /* ZlangToStringFunction *to_string_func */
		NULL , /* ZlangFromDataPtrFunction *from_data_ptr_func */
		NULL , /* ZlangGetDataPtrFunction *get_data_ptr_func */
		
		NULL , /* ZlangOperatorFunction *oper_PLUS_func */
		NULL , /* ZlangOperatorFunction *oper_MINUS_func */
		NULL , /* ZlangOperatorFunction *oper_MUL_func */
		NULL , /* ZlangOperatorFunction *oper_DIV_func */
		NULL , /* ZlangOperatorFunction *oper_MOD_func */
		
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_NEGATIVE_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_NOT_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_BIT_REVERSE_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_PLUS_PLUS_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_MINUS_MINUS_func */
		
		NULL , /* ZlangCompareFunction *comp_EGUAL_func */
		NULL , /* ZlangCompareFunction *comp_NOTEGUAL_func */
		NULL , /* ZlangCompareFunction *comp_LT_func */
		NULL , /* ZlangCompareFunction *comp_LE_func */
		NULL , /* ZlangCompareFunction *comp_GT_func */
		NULL , /* ZlangCompareFunction *comp_GE_func */
		
		NULL , /* ZlangLogicFunction *logic_AND_func */
		NULL , /* ZlangLogicFunction *logic_OR_func */
		
		NULL , /* ZlangLogicFunction *bit_AND_func */
		NULL , /* ZlangLogicFunction *bit_XOR_func */
		NULL , /* ZlangLogicFunction *bit_OR_func */
		NULL , /* ZlangLogicFunction *bit_MOVELEFT_func */
		NULL , /* ZlangLogicFunction *bit_MOVERIGHT_func */
		
		ZlangSummarizeDirectPropertySize_sm4 , /* ZlangSummarizeDirectPropertySizeFunction *summarize_direct_prop_size_func */
	} ;

ZlangImportObjectFunction ZlangImportObject_sm4;
struct ZlangObject *ZlangImportObject_sm4( struct ZlangRuntime *rt )
{
	struct ZlangObject	*obj = NULL ;
	struct ZlangFunction	*func = NULL ;
	int			nret = 0 ;
	
	nret = ImportObject( rt , & obj , ZLANG_OBJECT_sm4 , & direct_funcs_sm4 , sizeof(struct ZlangDirectFunctions) , NULL ) ;
	if( nret )
	{
		SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_LINK_FUNC_TO_ENTITY , "import object to global objects heap" )
		return NULL;
	}
	
	/* sm4.Encrypt(int,int,string,string,int) */
	func = AddFunctionAndParametersInObject( rt , obj , "Encrypt" , "Encrypt(int,int,string,string,int)" , ZlangInvokeFunction_sm4_Encrypt_int_int_string_string_int , ZLANG_OBJECT_string , ZLANG_OBJECT_int,NULL , ZLANG_OBJECT_int,NULL , ZLANG_OBJECT_string,NULL , ZLANG_OBJECT_string,NULL , ZLANG_OBJECT_int,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* sm4.Decrypt(int,int,string,string,int) */
	func = AddFunctionAndParametersInObject( rt , obj , "Decrypt" , "Decrypt(int,int,string,string,int)" , ZlangInvokeFunction_sm4_Decrypt_int_int_string_string_int , ZLANG_OBJECT_string , ZLANG_OBJECT_int,NULL , ZLANG_OBJECT_int,NULL , ZLANG_OBJECT_string,NULL , ZLANG_OBJECT_string,NULL , ZLANG_OBJECT_int,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	return obj ;
}

#endif
#endif

