/* Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "zobjects_crypto.h"

#ifdef SN_sm2
#ifndef OPENSSL_NO_SM2

static void FreePkey( struct ZlangRuntime *rt , struct ZlangDirectProperty_sm2key *sm2key_direct_prop )
{
	if( sm2key_direct_prop )
	{
		if( sm2key_direct_prop->pkey )
		{
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "EVP_PKEY_free[%p]" , sm2key_direct_prop->pkey )
			EVP_PKEY_free( sm2key_direct_prop->pkey );
			sm2key_direct_prop->has_pubkey = FALSE ;
			sm2key_direct_prop->has_prikey = FALSE ;
		}
	}
	
	return;
}

ZlangInvokeFunction ZlangInvokeFunction_sm2key_GenerateKeyPair;
int ZlangInvokeFunction_sm2key_GenerateKeyPair( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_sm2key	*sm2key_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	EVP_PKEY_CTX				*ctx = NULL ;
	int					nret = 0 ;
	
	ctx = EVP_PKEY_CTX_new_id( EVP_PKEY_EC , NULL ) ;
	if( ctx == NULL )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "EVP_PKEY_CTX_new_id failed" )
		CallRuntimeFunction_bool_SetBoolValue( rt , out1 , FALSE );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_EVP_PKEY_NEW_ID_FAILED );
	}
	
	nret = EVP_PKEY_keygen_init( ctx ) ;
	if( nret != 1 )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "EVP_PKEY_CTX_new_id failed[%d]" , nret )
		EVP_PKEY_CTX_free( ctx );
		CallRuntimeFunction_bool_SetBoolValue( rt , out1 , FALSE );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_EVP_PKEY_KEYGEN_INIT );
	}
	
	nret = EVP_PKEY_CTX_set_ec_paramgen_curve_nid( ctx , NID_sm2 ) ;
	if( nret <= 0 )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "EVP_PKEY_CTX_set_ec_paramgen_curve_nid failed[%d]" , nret )
		EVP_PKEY_CTX_free( ctx );
		CallRuntimeFunction_bool_SetBoolValue( rt , out1 , FALSE );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_EVP_PKEY_CTX_SET_EC_PARAMGEN_CURVE_NID_FAILED );
	}
	
	nret = EVP_PKEY_keygen( ctx , & (sm2key_direct_prop->pkey) ) ;
	if( nret <= 0 )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "EVP_PKEY_keygen failed[%d]" , nret )
		EVP_PKEY_CTX_free( ctx );
		CallRuntimeFunction_bool_SetBoolValue( rt , out1 , FALSE );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_EVP_PKEY_KEYGEN_FAILED );
	}
	
	sm2key_direct_prop->has_pubkey = TRUE ;
	sm2key_direct_prop->has_prikey = TRUE ;
	
	EVP_PKEY_CTX_free( ctx );
	CallRuntimeFunction_bool_SetBoolValue( rt , out1 , TRUE );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_sm2key_ImportPublicKeyFromPEM_string;
int ZlangInvokeFunction_sm2key_ImportPublicKeyFromPEM_string( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_sm2key	*sm2key_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject			*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	char					*filename = NULL ;
	int32_t					filename_len ;
	FILE					*fp = NULL ;
	int					nret = 0 ;
	
	FreePkey( rt , sm2key_direct_prop );
	
	CallRuntimeFunction_string_GetStringValue( rt , in1 , & filename , & filename_len );
	filename[filename_len] = '\0' ;
	
	fp = fopen( filename , "rb" ) ;
	if( fp == NULL )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "fopen failed , errno[%d]" , errno )
		CallRuntimeFunction_bool_SetBoolValue( rt , out1 , FALSE );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_FILE_NOT_FOUND );
	}
	
	sm2key_direct_prop->pkey = PEM_read_PUBKEY( fp , NULL , NULL , NULL ) ;
	fclose( fp );
	if( sm2key_direct_prop->pkey == NULL )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "PEM_read_PUBKEY failed[%d]" , nret )
		CallRuntimeFunction_bool_SetBoolValue( rt , out1 , FALSE );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_READ_SM2_PUBLIC_KEY_IN_FILE_FAILED );
	}
	
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "PEM_read_PUBKEY ok" )
	sm2key_direct_prop->has_pubkey = TRUE ;
	sm2key_direct_prop->has_prikey = FALSE ;
	CallRuntimeFunction_bool_SetBoolValue( rt , out1 , TRUE );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_sm2key_ImportPrivateKeyFromPEM_string;
int ZlangInvokeFunction_sm2key_ImportPrivateKeyFromPEM_string( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_sm2key	*sm2key_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject			*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	char					*filename = NULL ;
	int32_t					filename_len ;
	FILE					*fp = NULL ;
	int					nret = 0 ;
	
	FreePkey( rt , sm2key_direct_prop );
	
	CallRuntimeFunction_string_GetStringValue( rt , in1 , & filename , & filename_len );
	filename[filename_len] = '\0' ;
	
	fp = fopen( filename , "rb" ) ;
	if( fp == NULL )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "fopen failed , errno[%d]" , errno )
		CallRuntimeFunction_bool_SetBoolValue( rt , out1 , FALSE );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_FILE_NOT_FOUND );
	}
	
	sm2key_direct_prop->pkey = PEM_read_PrivateKey( fp , NULL , NULL , NULL ) ;
	fclose( fp );
	if( sm2key_direct_prop->pkey == NULL )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "PEM_read_PrivateKey failed[%d]" , nret )
		CallRuntimeFunction_bool_SetBoolValue( rt , out1 , FALSE );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_READ_SM2_PRIVATE_KEY_IN_FILE_FAILED );
	}
	
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "PEM_read_PrivateKey ok" )
	sm2key_direct_prop->has_pubkey = FALSE ;
	sm2key_direct_prop->has_prikey = TRUE ;
	CallRuntimeFunction_bool_SetBoolValue( rt , out1 , TRUE );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_sm2key_ImportPublicKeyFromDER_string;
int ZlangInvokeFunction_sm2key_ImportPublicKeyFromDER_string( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_sm2key	*sm2key_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject			*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	char					*filename = NULL ;
	int32_t					filename_len ;
	FILE					*fp = NULL ;
	int					nret = 0 ;
	
	FreePkey( rt , sm2key_direct_prop );
	
	CallRuntimeFunction_string_GetStringValue( rt , in1 , & filename , & filename_len );
	filename[filename_len] = '\0' ;
	
	fp = fopen( filename , "rb" ) ;
	if( fp == NULL )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "fopen failed , errno[%d]" , errno )
		CallRuntimeFunction_bool_SetBoolValue( rt , out1 , FALSE );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_FILE_NOT_FOUND );
	}
	
	sm2key_direct_prop->pkey = d2i_PUBKEY_fp( fp , NULL ) ;
	fclose( fp );
	if( sm2key_direct_prop->pkey == NULL )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "d2i_PublicKey_fp failed[%d]" , nret )
		CallRuntimeFunction_bool_SetBoolValue( rt , out1 , FALSE );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_READ_SM2_PUBLIC_KEY_IN_FILE_FAILED );
	}
	
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "d2i_PublicKey_fp ok" )
	sm2key_direct_prop->has_pubkey = TRUE ;
	sm2key_direct_prop->has_prikey = FALSE ;
	CallRuntimeFunction_bool_SetBoolValue( rt , out1 , TRUE );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_sm2key_ImportPrivateKeyFromDER_string;
int ZlangInvokeFunction_sm2key_ImportPrivateKeyFromDER_string( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_sm2key	*sm2key_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject			*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	char					*filename = NULL ;
	int32_t					filename_len ;
	FILE					*fp = NULL ;
	int					nret = 0 ;
	
	FreePkey( rt , sm2key_direct_prop );
	
	CallRuntimeFunction_string_GetStringValue( rt , in1 , & filename , & filename_len );
	filename[filename_len] = '\0' ;
	
	fp = fopen( filename , "rb" ) ;
	if( fp == NULL )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "fopen failed , errno[%d]" , errno )
		CallRuntimeFunction_bool_SetBoolValue( rt , out1 , FALSE );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_FILE_NOT_FOUND );
	}
	
	sm2key_direct_prop->pkey = d2i_PrivateKey_fp( fp , NULL ) ;
	fclose( fp );
	if( sm2key_direct_prop->pkey == NULL )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "d2i_PrivateKey_fp failed[%d]" , nret )
		CallRuntimeFunction_bool_SetBoolValue( rt , out1 , FALSE );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_READ_SM2_PRIVATE_KEY_IN_FILE_FAILED );
	}
	
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "d2i_PrivateKey_fp ok" )
	sm2key_direct_prop->has_pubkey = FALSE ;
	sm2key_direct_prop->has_prikey = TRUE ;
	CallRuntimeFunction_bool_SetBoolValue( rt , out1 , TRUE );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_sm2key_ExportPublicKeyFromPEM_string;
int ZlangInvokeFunction_sm2key_ExportPublicKeyToPEM_string( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_sm2key	*sm2key_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject			*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	char					*filename = NULL ;
	int32_t					filename_len ;
	BIO					*bio = NULL ;
	int					nret = 0 ;
	
	if( sm2key_direct_prop->has_pubkey != TRUE )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "hasn't public key" )
		CallRuntimeFunction_bool_SetBoolValue( rt , out1 , FALSE );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_SM2KEY_HASNT_PUBLIC_KEY );
	}
	
	CallRuntimeFunction_string_GetStringValue( rt , in1 , & filename , & filename_len );
	filename[filename_len] = '\0' ;
	
	bio = BIO_new_file( filename , "wb" ) ;
	if( bio == NULL )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "BIO_new_file failed , errno[%d]" , errno )
		CallRuntimeFunction_bool_SetBoolValue( rt , out1 , FALSE );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_OPEN_WRITE_FILE_FAILED );
	}
	
	nret = PEM_write_bio_PUBKEY( bio , sm2key_direct_prop->pkey ) ;
	BIO_free_all( bio );
	if( nret != 1 )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "PEM_write_BIO_PUBKEY failed[%d]" , nret )
		CallRuntimeFunction_bool_SetBoolValue( rt , out1 , FALSE );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_WRITE_SM2_PUBLIC_KEY_INTO_FILE_FAILED );
	}
	
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "PEM_write_BIO_PUBKEY ok" )
	CallRuntimeFunction_bool_SetBoolValue( rt , out1 , TRUE );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_sm2key_ExportPrivateKeyFromPEM_string;
int ZlangInvokeFunction_sm2key_ExportPrivateKeyToPEM_string( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_sm2key	*sm2key_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject			*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	char					*filename = NULL ;
	int32_t					filename_len ;
	BIO					*bio = NULL ;
	int					nret = 0 ;
	
	if( sm2key_direct_prop->has_prikey != TRUE )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "hasn't private key" )
		CallRuntimeFunction_bool_SetBoolValue( rt , out1 , FALSE );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_SM2KEY_HASNT_PRIVATE_KEY );
	}
	
	CallRuntimeFunction_string_GetStringValue( rt , in1 , & filename , & filename_len );
	filename[filename_len] = '\0' ;
	
	bio = BIO_new_file( filename , "wb" ) ;
	if( bio == NULL )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "BIO_new_file failed , errno[%d]" , errno )
		CallRuntimeFunction_bool_SetBoolValue( rt , out1 , FALSE );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_OPEN_WRITE_FILE_FAILED );
	}
	
	nret = PEM_write_bio_PrivateKey( bio , sm2key_direct_prop->pkey , NULL , NULL , 0 , NULL , NULL ) ;
	BIO_free_all( bio );
	if( nret != 1 )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "PEM_write_bio_PrivateKey failed[%d]" , nret )
		CallRuntimeFunction_bool_SetBoolValue( rt , out1 , FALSE );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_WRITE_SM2_PRIVATE_KEY_INTO_FILE_FAILED );
	}
	
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "PEM_write_bio_PrivateKey ok" )
	CallRuntimeFunction_bool_SetBoolValue( rt , out1 , TRUE );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_sm2key_ExportPublicKeyFromDER_string;
int ZlangInvokeFunction_sm2key_ExportPublicKeyToDER_string( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_sm2key	*sm2key_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject			*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	char					*filename = NULL ;
	int32_t					filename_len ;
	BIO					*bio = NULL ;
	int					nret = 0 ;
	
	if( sm2key_direct_prop->has_pubkey != TRUE )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "hasn't public key" )
		CallRuntimeFunction_bool_SetBoolValue( rt , out1 , FALSE );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_SM2KEY_HASNT_PUBLIC_KEY );
	}
	
	CallRuntimeFunction_string_GetStringValue( rt , in1 , & filename , & filename_len );
	filename[filename_len] = '\0' ;
	
	bio = BIO_new_file( filename , "wb" ) ;
	if( bio == NULL )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "BIO_new_file failed , errno[%d]" , errno )
		CallRuntimeFunction_bool_SetBoolValue( rt , out1 , FALSE );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_OPEN_WRITE_FILE_FAILED );
	}
	
	nret = i2d_PUBKEY_bio( bio , sm2key_direct_prop->pkey ) ;
	BIO_free_all( bio );
	if( nret != 1 )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "i2d_PUBKEY_bio failed[%d]" , nret )
		CallRuntimeFunction_bool_SetBoolValue( rt , out1 , FALSE );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_READ_SM2_PUBLIC_KEY_IN_FILE_FAILED );
	}
	
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "i2d_PUBKEY_bio ok" )
	CallRuntimeFunction_bool_SetBoolValue( rt , out1 , TRUE );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_sm2key_ExportPrivateKeyFromDER_string;
int ZlangInvokeFunction_sm2key_ExportPrivateKeyToDER_string( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_sm2key	*sm2key_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject			*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	char					*filename = NULL ;
	int32_t					filename_len ;
	BIO					*bio = NULL ;
	int					nret = 0 ;
	
	if( sm2key_direct_prop->has_pubkey != TRUE )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "hasn't private key" )
		CallRuntimeFunction_bool_SetBoolValue( rt , out1 , FALSE );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_SM2KEY_HASNT_PRIVATE_KEY );
	}
	
	CallRuntimeFunction_string_GetStringValue( rt , in1 , & filename , & filename_len );
	filename[filename_len] = '\0' ;
	
	bio = BIO_new_file( filename , "wb" ) ;
	if( bio == NULL )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "BIO_new_file failed , errno[%d]" , errno )
		CallRuntimeFunction_bool_SetBoolValue( rt , out1 , FALSE );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_OPEN_WRITE_FILE_FAILED );
	}
	
	nret = i2d_PrivateKey_bio( bio , sm2key_direct_prop->pkey ) ;
	BIO_free_all( bio );
	if( nret != 1 )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "i2d_PrivateKey_bio failed[%d]" , nret )
		CallRuntimeFunction_bool_SetBoolValue( rt , out1 , FALSE );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_READ_SM2_PRIVATE_KEY_IN_FILE_FAILED );
	}
	
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "i2d_PrivateKey_bio ok" )
	CallRuntimeFunction_bool_SetBoolValue( rt , out1 , TRUE );
	
	return 0;
}

ZlangCreateDirectPropertyFunction ZlangCreateDirectProperty_sm2key;
void *ZlangCreateDirectProperty_sm2key( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_sm2key	*sm2key_direct_prop = NULL ;
	
	sm2key_direct_prop = (struct ZlangDirectProperty_sm2key *)ZLMALLOC( sizeof(struct ZlangDirectProperty_sm2key) ) ;
	if( sm2key_direct_prop == NULL )
	{
		SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_ALLOC , "alloc memory for entity" )
		return NULL;
	}
	memset( sm2key_direct_prop , 0x00 , sizeof(struct ZlangDirectProperty_sm2key) );
	
	sm2key_direct_prop->has_pubkey = FALSE ;
	sm2key_direct_prop->has_prikey = FALSE ;
	
	return sm2key_direct_prop;
}

ZlangDestroyDirectPropertyFunction ZlangDestroyDirectProperty_sm2key;
void ZlangDestroyDirectProperty_sm2key( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_sm2key	*sm2key_direct_prop = GetObjectDirectProperty(obj) ;
	
	FreePkey( rt , sm2key_direct_prop );
	
	ZLFREE( sm2key_direct_prop );
	
	return;
}

ZlangSummarizeDirectPropertySizeFunction ZlangSummarizeDirectPropertySize_sm2key;
void ZlangSummarizeDirectPropertySize_sm2key( struct ZlangRuntime *rt , struct ZlangObject *obj , size_t *summarized_obj_size , size_t *summarized_direct_prop_size )
{
	SUMMARIZE_SIZE( summarized_direct_prop_size , sizeof(struct ZlangDirectProperty_sm2key) )
	return;
}

static struct ZlangDirectFunctions direct_funcs_sm2key =
	{
		ZLANG_OBJECT_sm2key , /* char *ancestor_name */
		
		ZlangCreateDirectProperty_sm2key , /* ZlangCreateDirectPropertyFunction *create_entity_func */
		ZlangDestroyDirectProperty_sm2key , /* ZlangDestroyDirectPropertyFunction *destroy_entity_func */
		
		NULL , /* ZlangFromCharPtrFunction *from_char_ptr_func */
		NULL , /* ZlangToStringFunction *to_string_func */
		NULL , /* ZlangFromDataPtrFunction *from_data_ptr_func */
		NULL , /* ZlangGetDataPtrFunction *get_data_ptr_func */
		
		NULL , /* ZlangOperatorFunction *oper_PLUS_func */
		NULL , /* ZlangOperatorFunction *oper_MINUS_func */
		NULL , /* ZlangOperatorFunction *oper_MUL_func */
		NULL , /* ZlangOperatorFunction *oper_DIV_func */
		NULL , /* ZlangOperatorFunction *oper_MOD_func */
		
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_NEGATIVE_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_NOT_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_BIT_REVERSE_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_PLUS_PLUS_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_MINUS_MINUS_func */
		
		NULL , /* ZlangCompareFunction *comp_EGUAL_func */
		NULL , /* ZlangCompareFunction *comp_NOTEGUAL_func */
		NULL , /* ZlangCompareFunction *comp_LT_func */
		NULL , /* ZlangCompareFunction *comp_LE_func */
		NULL , /* ZlangCompareFunction *comp_GT_func */
		NULL , /* ZlangCompareFunction *comp_GE_func */
		
		NULL , /* ZlangLogicFunction *logic_AND_func */
		NULL , /* ZlangLogicFunction *logic_OR_func */
		
		NULL , /* ZlangLogicFunction *bit_AND_func */
		NULL , /* ZlangLogicFunction *bit_XOR_func */
		NULL , /* ZlangLogicFunction *bit_OR_func */
		NULL , /* ZlangLogicFunction *bit_MOVELEFT_func */
		NULL , /* ZlangLogicFunction *bit_MOVERIGHT_func */
		
		ZlangSummarizeDirectPropertySize_sm2key , /* ZlangSummarizeDirectPropertySizeFunction *summarize_direct_prop_size_func */
	} ;

ZlangImportObjectFunction ZlangImportObject_sm2key;
struct ZlangObject *ZlangImportObject_sm2key( struct ZlangRuntime *rt )
{
	struct ZlangObject	*obj = NULL ;
	struct ZlangFunction	*func = NULL ;
	int			nret = 0 ;
	
	nret = ImportObject( rt , & obj , ZLANG_OBJECT_sm2key , & direct_funcs_sm2key , sizeof(struct ZlangDirectFunctions) , NULL ) ;
	if( nret )
	{
		SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_LINK_FUNC_TO_ENTITY , "import object to global objects heap" )
		return NULL;
	}
	
	/* sm2key.GenerateKeyPair() */
	func = AddFunctionAndParametersInObject( rt , obj , "GenerateKeyPair" , "GenerateKeyPair()" , ZlangInvokeFunction_sm2key_GenerateKeyPair , ZLANG_OBJECT_bool , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* sm2key.ImportPublicKeyFromPEM(string) */
	func = AddFunctionAndParametersInObject( rt , obj , "ImportPublicKeyFromPEM" , "ImportPublicKeyFromPEM(string)" , ZlangInvokeFunction_sm2key_ImportPublicKeyFromPEM_string , ZLANG_OBJECT_bool , ZLANG_OBJECT_string,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* sm2key.ImportPrivateKeyFromPEM(string) */
	func = AddFunctionAndParametersInObject( rt , obj , "ImportPrivateKeyFromPEM" , "ImportPrivateKeyFromPEM(string)" , ZlangInvokeFunction_sm2key_ImportPrivateKeyFromPEM_string , ZLANG_OBJECT_bool , ZLANG_OBJECT_string,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* sm2key.ImportPublicKeyFromDER(string) */
	func = AddFunctionAndParametersInObject( rt , obj , "ImportPublicKeyFromDER" , "ImportPublicKeyFromDER(string)" , ZlangInvokeFunction_sm2key_ImportPublicKeyFromDER_string , ZLANG_OBJECT_bool , ZLANG_OBJECT_string,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* sm2key.ImportPrivateKeyFromDER(string) */
	func = AddFunctionAndParametersInObject( rt , obj , "ImportPrivateKeyFromDER" , "ImportPrivateKeyFromDER(string)" , ZlangInvokeFunction_sm2key_ImportPrivateKeyFromDER_string , ZLANG_OBJECT_bool , ZLANG_OBJECT_string,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* sm2key.ExportPublicKeyToPEM(string) */
	func = AddFunctionAndParametersInObject( rt , obj , "ExportPublicKeyToPEM" , "ExportPublicKeyToPEM(string)" , ZlangInvokeFunction_sm2key_ExportPublicKeyToPEM_string , ZLANG_OBJECT_bool , ZLANG_OBJECT_string,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* sm2key.ExportPrivateKeyToPEM(string) */
	func = AddFunctionAndParametersInObject( rt , obj , "ExportPrivateKeyToPEM" , "ExportPrivateKeyToPEM(string)" , ZlangInvokeFunction_sm2key_ExportPrivateKeyToPEM_string , ZLANG_OBJECT_bool , ZLANG_OBJECT_string,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* sm2key.ExportPublicKeyToDER(string) */
	func = AddFunctionAndParametersInObject( rt , obj , "ExportPublicKeyToDER" , "ExportPublicKeyToDER(string)" , ZlangInvokeFunction_sm2key_ExportPublicKeyToDER_string , ZLANG_OBJECT_bool , ZLANG_OBJECT_string,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* sm2key.ExportPrivateKeyToDER(string) */
	func = AddFunctionAndParametersInObject( rt , obj , "ExportPrivateKeyToDER" , "ExportPrivateKeyToDER(string)" , ZlangInvokeFunction_sm2key_ExportPrivateKeyToDER_string , ZLANG_OBJECT_bool , ZLANG_OBJECT_string,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	return obj ;
}

#endif
#endif

