/* Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "zobjects_crypto.h"

#define DES_PADDING_SIZE		8

#define TRIPLEDES_ECB_MODE		1
#define TRIPLEDES_CBC_MODE		2

struct ZlangDirectProperty_tripledes
{
	int	dummy ;
} ;

ZlangInvokeFunction ZlangInvokeFunction_tripledes_Encrypt_int_int_string_string_int;
int ZlangInvokeFunction_tripledes_Encrypt_int_int_string_string_int( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangObject	*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject	*in2 = GetInputParameterInLocalObjectStack(rt,2) ;
	struct ZlangObject	*in3 = GetInputParameterInLocalObjectStack(rt,3) ;
	struct ZlangObject	*in4 = GetInputParameterInLocalObjectStack(rt,4) ;
	struct ZlangObject	*in5 = GetInputParameterInLocalObjectStack(rt,5) ;
	struct ZlangObject	*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	int32_t			mode ;
	int32_t			key_len_type ;
	char			*key = NULL ;
	int32_t			key_len ;
	DES_key_schedule	ks1 , ks2 , ks3 ;
	char			*dec = NULL ;
	int32_t			dec_len ;
	int32_t			padding_type ;
	int32_t			prepare_len ;
	char			**enc = NULL ;
	int32_t			*enc_len = NULL ;
	char			*o_dec = NULL ;
	char			*o_enc = NULL ;
	int32_t			offset ;
	int32_t			block_len ;
	char			in[ DES_PADDING_SIZE*2 ] ;
	char			ivec[ DES_PADDING_SIZE ] ;
	
	CallRuntimeFunction_int_GetIntValue( rt , in1 , & mode );
	
	CallRuntimeFunction_int_GetIntValue( rt , in2 , & key_len_type );
	if( key_len_type != CRYPTO_KEY_128BITS && key_len_type != CRYPTO_KEY_192BITS )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "key_len_type[%"PRIi32"] invalid" , key_len_type )
		UnreferObject( rt , out1 );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_KEY_LEN_TYPE_INVALID );
	}
	
	CallRuntimeFunction_string_GetStringValue( rt , in3 , & key , & key_len );
	if( key_len == 16 && key_len_type == CRYPTO_KEY_128BITS )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "key block 1 :" )
		TEST_RUNTIME_DEBUG_THEN_PRINT_HEXSTR( rt , key , 8 )
		DES_set_key_unchecked( (const_DES_cblock*)key , & ks1 );
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "key block 2 :" )
		TEST_RUNTIME_DEBUG_THEN_PRINT_HEXSTR( rt , key+8 , 8 )
		DES_set_key_unchecked( (const_DES_cblock*)(key+8) , & ks2 );
			
	}
	else if( key_len == 24 && key_len_type == CRYPTO_KEY_192BITS )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "key block 1 :" )
		TEST_RUNTIME_DEBUG_THEN_PRINT_HEXSTR( rt , key , 8 )
		DES_set_key_unchecked( (const_DES_cblock*)key , & ks1 );
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "key block 2 :" )
		TEST_RUNTIME_DEBUG_THEN_PRINT_HEXSTR( rt , key+8 , 8 )
		DES_set_key_unchecked( (const_DES_cblock*)(key+8) , & ks2 );
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "key block 3 :" )
		TEST_RUNTIME_DEBUG_THEN_PRINT_HEXSTR( rt , key+16 , 8 )
		DES_set_key_unchecked( (const_DES_cblock*)(key+16) , & ks3 );
	}
	else
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "key_len[%"PRIi32"] not matched key_len_type[%"PRIi32"]" , key_len , key_len_type )
		UnreferObject( rt , out1 );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_KEY_LEN_NOT_MATCHED_KEY_LEN_TYPE );
	}
	
	CallRuntimeFunction_string_GetStringValue( rt , in4 , & dec , & dec_len );
	
	CallRuntimeFunction_int_GetIntValue( rt , in5 , & padding_type );
	if( padding_type != CRYPTO_ZERO_PADDING && padding_type != CRYPTO_PKCS7_PADDING )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "padding_type[%"PRIi32"] invalid" , padding_type )
		UnreferObject( rt , out1 );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_PADDING_TYPE_INVALID );
	}
	
	CallRuntimeFunction_string_Clear( rt , out1 );
	if( dec_len == 0 )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "dec_len[%"PRIi32"]" , dec_len )
		return 0;
	}
	prepare_len = ((dec_len-1)/DES_PADDING_SIZE+1)*DES_PADDING_SIZE + DES_PADDING_SIZE ;
	CallRuntimeFunction_string_PrepareBuffer( rt , out1 , prepare_len );
	CallRuntimeFunction_string_GetDirectPropertiesPtr( rt , out1 , & enc , NULL , & enc_len );
	
	o_dec = dec ;
	o_enc = (*enc) ;
	block_len = 0 ;
	memset( ivec , 0x00 , sizeof(ivec) );
	for( offset = 0 ; offset < dec_len || block_len > 0 ; offset += DES_PADDING_SIZE )
	{
		if( block_len == 0 )
		{
			if( offset + DES_PADDING_SIZE < dec_len )
			{
				block_len = DES_PADDING_SIZE ;
				memcpy( in , o_dec , block_len );
			}
			else
			{
				block_len = dec_len - offset ;
				memcpy( in , o_dec , block_len );
			}
			
			if( offset + DES_PADDING_SIZE >= dec_len )
			{
				block_len = PaddingBlockDataUnsafe( padding_type , in , block_len , DES_PADDING_SIZE ) ;
				if( block_len == -1 )
				{
					TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "PaddingDataUnsafe return[%d]" , block_len )
					UnreferObject( rt , out1 );
					return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_PADDING_FAILED );
				}
			}
		}
		else if( block_len < 0 )
		{
			SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_INTERNAL , "block_len[%"PRIi32"] invalid" , block_len )
			return ZLANG_ERROR_INTERNAL;
		}
		
		if( mode == TRIPLEDES_ECB_MODE )
		{
			if( key_len_type == CRYPTO_KEY_128BITS )
			{
				DES_ecb2_encrypt( (const_DES_cblock*)(in+(offset>=dec_len?DES_PADDING_SIZE:0)) , (DES_cblock*)o_enc , & ks1 , & ks2 , DES_ENCRYPT ) ;
			}
			else if( key_len_type == CRYPTO_KEY_192BITS )
			{
				DES_ecb3_encrypt( (const_DES_cblock*)(in+(offset>=dec_len?DES_PADDING_SIZE:0)) , (DES_cblock*)o_enc , & ks1 , & ks2 , & ks3 , DES_ENCRYPT ) ;
			}
		}
		else if( mode == TRIPLEDES_CBC_MODE )
		{
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "dec block :" )
			TEST_RUNTIME_DEBUG_THEN_PRINT_HEXSTR( rt , in+(offset>=dec_len?DES_PADDING_SIZE:0) , 8 )
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "ivec block :" )
			TEST_RUNTIME_DEBUG_THEN_PRINT_HEXSTR( rt , ivec , 8 )
			
			if( key_len_type == CRYPTO_KEY_128BITS )
			{
				DES_ede2_cbc_encrypt( (unsigned char *)(in+(offset>=dec_len?DES_PADDING_SIZE:0)) , (unsigned char *)o_enc , DES_PADDING_SIZE , & ks1 , & ks2 , (DES_cblock*)ivec , DES_ENCRYPT );
			}
			else if( key_len_type == CRYPTO_KEY_192BITS )
			{
				DES_ede3_cbc_encrypt( (unsigned char *)(in+(offset>=dec_len?DES_PADDING_SIZE:0)) , (unsigned char *)o_enc , DES_PADDING_SIZE , & ks1 , & ks2 , & ks3 , (DES_cblock*)ivec , DES_ENCRYPT );
			}
			
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "enc block :" )
			TEST_RUNTIME_DEBUG_THEN_PRINT_HEXSTR( rt , o_enc , 8 )
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "ivec block :" )
			TEST_RUNTIME_DEBUG_THEN_PRINT_HEXSTR( rt , ivec , 8 )
		}
		
		block_len -= DES_PADDING_SIZE ;
		if( block_len == 0 )
			o_dec += DES_PADDING_SIZE ;
		o_enc += DES_PADDING_SIZE ;
	}
	(*enc_len) = o_enc - (*enc) ;
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_tripledes_Decrypt_int_int_string_string_int;
int ZlangInvokeFunction_tripledes_Decrypt_int_int_string_string_int( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangObject	*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject	*in2 = GetInputParameterInLocalObjectStack(rt,2) ;
	struct ZlangObject	*in3 = GetInputParameterInLocalObjectStack(rt,3) ;
	struct ZlangObject	*in4 = GetInputParameterInLocalObjectStack(rt,4) ;
	struct ZlangObject	*in5 = GetInputParameterInLocalObjectStack(rt,5) ;
	struct ZlangObject	*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	int32_t			mode ;
	int32_t			key_len_type ;
	char			*key = NULL ;
	int32_t			key_len ;
	DES_key_schedule	ks1 , ks2 , ks3 ;
	char			*enc = NULL ;
	int32_t			enc_len ;
	int32_t			padding_type ;
	int32_t			prepare_len ;
	char			**dec = NULL ;
	int32_t			*dec_len = NULL ;
	char			*o_dec = NULL ;
	char			*o_enc = NULL ;
	int32_t			offset ;
	char			in[ DES_PADDING_SIZE ] ;
	char			ivec[ DES_PADDING_SIZE ] ;
	int32_t			unpadding_len ;
	
	CallRuntimeFunction_int_GetIntValue( rt , in1 , & mode );
	
	CallRuntimeFunction_int_GetIntValue( rt , in2 , & key_len_type );
	if( key_len_type != CRYPTO_KEY_128BITS && key_len_type != CRYPTO_KEY_192BITS )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "key_len_type[%"PRIi32"] invalid" , key_len_type )
		UnreferObject( rt , out1 );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_KEY_LEN_TYPE_INVALID );
	}
	
	CallRuntimeFunction_string_GetStringValue( rt , in3 , & key , & key_len );
	if( key_len == 16 && key_len_type == CRYPTO_KEY_128BITS )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "key block 1 :" )
		TEST_RUNTIME_DEBUG_THEN_PRINT_HEXSTR( rt , key , 8 )
		DES_set_key_unchecked( (const_DES_cblock*)key , & ks1 );
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "key block 2 :" )
		TEST_RUNTIME_DEBUG_THEN_PRINT_HEXSTR( rt , key+8 , 8 )
		DES_set_key_unchecked( (const_DES_cblock*)(key+8) , & ks2 );
	}
	else if( key_len == 24 && key_len_type == CRYPTO_KEY_192BITS )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "key block 1 :" )
		TEST_RUNTIME_DEBUG_THEN_PRINT_HEXSTR( rt , key , 8 )
		DES_set_key_unchecked( (const_DES_cblock*)key , & ks1 );
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "key block 2 :" )
		TEST_RUNTIME_DEBUG_THEN_PRINT_HEXSTR( rt , key+8 , 8 )
		DES_set_key_unchecked( (const_DES_cblock*)(key+8) , & ks2 );
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "key block 3 :" )
		TEST_RUNTIME_DEBUG_THEN_PRINT_HEXSTR( rt , key+16 , 8 )
		DES_set_key_unchecked( (const_DES_cblock*)(key+16) , & ks3 );
	}
	else
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "key_len[%"PRIi32"] not matched key_len_type[%"PRIi32"]" , key_len , key_len_type )
		UnreferObject( rt , out1 );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_KEY_LEN_NOT_MATCHED_KEY_LEN_TYPE );
	}
	
	CallRuntimeFunction_string_GetStringValue( rt , in4 , & enc , & enc_len );
	
	CallRuntimeFunction_int_GetIntValue( rt , in5 , & padding_type );
	if( padding_type != CRYPTO_ZERO_PADDING && padding_type != CRYPTO_PKCS7_PADDING )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "padding_type[%"PRIi32"] invalid" , padding_type )
		UnreferObject( rt , out1 );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_PADDING_TYPE_INVALID );
	}
	
	CallRuntimeFunction_string_Clear( rt , out1 );
	if( enc_len == 0 )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "enc_len[%"PRIi32"]" , enc_len )
		return 0;
	}
	else if( enc_len % 8 != 0 )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "enc_len[%"PRIi32"] invalid" , enc_len )
		UnreferObject( rt , out1 );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_ENC_LEN_INVALID );
	}
	prepare_len = ((enc_len-1)/DES_PADDING_SIZE+1)*DES_PADDING_SIZE ;
	CallRuntimeFunction_string_PrepareBuffer( rt , out1 , prepare_len );
	
	CallRuntimeFunction_string_GetDirectPropertiesPtr( rt , out1 , & dec , NULL , & dec_len );
	
	o_enc = enc ;
	o_dec = (*dec) ;
	memset( ivec , 0x00 , sizeof(ivec) );
	for( offset = 0 ; offset < enc_len ; offset += DES_PADDING_SIZE )
	{
		if( offset + DES_PADDING_SIZE <= enc_len )
		{
			memcpy( in , o_enc , DES_PADDING_SIZE );
		}
		else
		{
			memcpy( in , o_enc , enc_len - offset );
		}
		
		if( mode == TRIPLEDES_ECB_MODE )
		{
			if( key_len_type == CRYPTO_KEY_128BITS )
			{
				DES_ecb2_encrypt( (const_DES_cblock*)(in+(offset>=enc_len?DES_PADDING_SIZE:0)) , (DES_cblock*)o_dec , & ks1 , & ks2 , DES_DECRYPT ) ;
			}
			else if( key_len_type == CRYPTO_KEY_192BITS )
			{
				DES_ecb3_encrypt( (const_DES_cblock*)(in+(offset>=enc_len?DES_PADDING_SIZE:0)) , (DES_cblock*)o_dec , & ks1 , & ks2 , & ks3 , DES_DECRYPT ) ;
			}
		}
		else if( mode == TRIPLEDES_CBC_MODE )
		{
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "enc block :" )
			TEST_RUNTIME_DEBUG_THEN_PRINT_HEXSTR( rt , in+(offset>=enc_len?DES_PADDING_SIZE:0) , 8 )
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "ivec block :" )
			TEST_RUNTIME_DEBUG_THEN_PRINT_HEXSTR( rt , ivec , 8 )
			
			if( key_len_type == CRYPTO_KEY_128BITS )
			{
				DES_ede2_cbc_encrypt( (unsigned char *)(in+(offset>=enc_len?DES_PADDING_SIZE:0)) , (unsigned char *)o_dec , DES_PADDING_SIZE , & ks1 , & ks2 , (DES_cblock*)ivec , DES_DECRYPT );
			}
			else if( key_len_type == CRYPTO_KEY_192BITS )
			{
				DES_ede3_cbc_encrypt( (unsigned char *)(in+(offset>=enc_len?DES_PADDING_SIZE:0)) , (unsigned char *)o_dec , DES_PADDING_SIZE , & ks1 , & ks2 , & ks3 , (DES_cblock*)ivec , DES_DECRYPT );
			}
			
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "dec block :" )
			TEST_RUNTIME_DEBUG_THEN_PRINT_HEXSTR( rt , o_dec , 8 )
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "ivec block :" )
			TEST_RUNTIME_DEBUG_THEN_PRINT_HEXSTR( rt , ivec , 8 )
		}
		
		o_enc += DES_PADDING_SIZE ;
		o_dec += DES_PADDING_SIZE ;
	}
	
	unpadding_len = UnpaddingData( padding_type , (*dec) , o_dec-(*dec) ) ;
	if( unpadding_len == -1 )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "UnpaddingData return[%d]" , unpadding_len )
		UnreferObject( rt , out1 );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_UNPADDING_FAILED );
	}
	(*dec_len) = unpadding_len ;
	
	return 0;
}

ZlangCreateDirectPropertyFunction ZlangCreateDirectProperty_tripledes;
void *ZlangCreateDirectProperty_tripledes( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_tripledes	*tripledes_direct_prop = NULL ;
	
	tripledes_direct_prop = (struct ZlangDirectProperty_tripledes *)ZLMALLOC( sizeof(struct ZlangDirectProperty_tripledes) ) ;
	if( tripledes_direct_prop == NULL )
	{
		SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_ALLOC , "alloc memory for entity" )
		return NULL;
	}
	memset( tripledes_direct_prop , 0x00 , sizeof(struct ZlangDirectProperty_tripledes) );
	
	return tripledes_direct_prop;
}

ZlangDestroyDirectPropertyFunction ZlangDestroyDirectProperty_tripledes;
void ZlangDestroyDirectProperty_tripledes( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_tripledes	*tripledes_direct_prop = GetObjectDirectProperty(obj) ;
	
	ZLFREE( tripledes_direct_prop );
	
	return;
}

ZlangSummarizeDirectPropertySizeFunction ZlangSummarizeDirectPropertySize_tripledes;
void ZlangSummarizeDirectPropertySize_tripledes( struct ZlangRuntime *rt , struct ZlangObject *obj , size_t *summarized_obj_size , size_t *summarized_direct_prop_size )
{
	SUMMARIZE_SIZE( summarized_direct_prop_size , sizeof(struct ZlangDirectProperty_tripledes) )
	return;
}

static struct ZlangDirectFunctions direct_funcs_tripledes =
	{
		ZLANG_OBJECT_tripledes , /* char *ancestor_name */
		
		ZlangCreateDirectProperty_tripledes , /* ZlangCreateDirectPropertyFunction *create_entity_func */
		ZlangDestroyDirectProperty_tripledes , /* ZlangDestroyDirectPropertyFunction *tripledestroy_entity_func */
		
		NULL , /* ZlangFromCharPtrFunction *from_char_ptr_func */
		NULL , /* ZlangToStringFunction *to_string_func */
		NULL , /* ZlangFromDataPtrFunction *from_data_ptr_func */
		NULL , /* ZlangGetDataPtrFunction *get_data_ptr_func */
		
		NULL , /* ZlangOperatorFunction *oper_PLUS_func */
		NULL , /* ZlangOperatorFunction *oper_MINUS_func */
		NULL , /* ZlangOperatorFunction *oper_MUL_func */
		NULL , /* ZlangOperatorFunction *oper_DIV_func */
		NULL , /* ZlangOperatorFunction *oper_MOD_func */
		
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_NEGATIVE_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_NOT_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_BIT_REVERSE_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_PLUS_PLUS_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_MINUS_MINUS_func */
		
		NULL , /* ZlangCompareFunction *comp_EGUAL_func */
		NULL , /* ZlangCompareFunction *comp_NOTEGUAL_func */
		NULL , /* ZlangCompareFunction *comp_LT_func */
		NULL , /* ZlangCompareFunction *comp_LE_func */
		NULL , /* ZlangCompareFunction *comp_GT_func */
		NULL , /* ZlangCompareFunction *comp_GE_func */
		
		NULL , /* ZlangLogicFunction *logic_AND_func */
		NULL , /* ZlangLogicFunction *logic_OR_func */
		
		NULL , /* ZlangLogicFunction *bit_AND_func */
		NULL , /* ZlangLogicFunction *bit_XOR_func */
		NULL , /* ZlangLogicFunction *bit_OR_func */
		NULL , /* ZlangLogicFunction *bit_MOVELEFT_func */
		NULL , /* ZlangLogicFunction *bit_MOVERIGHT_func */
		
		ZlangSummarizeDirectPropertySize_tripledes , /* ZlangSummarizeDirectPropertySizeFunction *summarize_direct_prop_size_func */
	} ;

ZlangImportObjectFunction ZlangImportObject_tripledes;
struct ZlangObject *ZlangImportObject_tripledes( struct ZlangRuntime *rt )
{
	struct ZlangObject	*obj = NULL ;
	struct ZlangFunction	*func = NULL ;
	int			nret = 0 ;
	
	nret = ImportObject( rt , & obj , ZLANG_OBJECT_tripledes , & direct_funcs_tripledes , sizeof(struct ZlangDirectFunctions) , NULL ) ;
	if( nret )
	{
		SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_LINK_FUNC_TO_ENTITY , "import object to global objects heap" )
		return NULL;
	}
	
	/* tripledes.Encrypt(int,int,string,string,int) */
	func = AddFunctionAndParametersInObject( rt , obj , "Encrypt" , "Encrypt(int,int,string,string,int)" , ZlangInvokeFunction_tripledes_Encrypt_int_int_string_string_int , ZLANG_OBJECT_string , ZLANG_OBJECT_int,NULL , ZLANG_OBJECT_int,NULL , ZLANG_OBJECT_string,NULL , ZLANG_OBJECT_string,NULL , ZLANG_OBJECT_int,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* tripledes.Decrypt(int,int,string,string,int) */
	func = AddFunctionAndParametersInObject( rt , obj , "Decrypt" , "Decrypt(int,int,string,string,int)" , ZlangInvokeFunction_tripledes_Decrypt_int_int_string_string_int , ZLANG_OBJECT_string , ZLANG_OBJECT_int,NULL , ZLANG_OBJECT_int,NULL , ZLANG_OBJECT_string,NULL , ZLANG_OBJECT_string,NULL , ZLANG_OBJECT_int,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	return obj ;
}

