/* Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "zobjects_crypto.h"

static void FreeRsa( struct ZlangRuntime *rt , struct ZlangDirectProperty_rsakey *rsakey_direct_prop )
{
	if( rsakey_direct_prop )
	{
		if( rsakey_direct_prop->rsa )
		{
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "RSA_free[%p]" , rsakey_direct_prop->rsa )
			RSA_free( rsakey_direct_prop->rsa ); rsakey_direct_prop->rsa = NULL ;
			rsakey_direct_prop->has_pubkey = FALSE ;
			rsakey_direct_prop->has_prikey = FALSE ;
		}
	}
	
	return;
}

static int GenerateKeyPair( struct ZlangRuntime *rt , struct ZlangDirectProperty_rsakey *rsakey_direct_prop , int bits , unsigned long e )
{
	BIGNUM		*bn = NULL ;
	int		nret = 0 ;
	
	FreeRsa( rt , rsakey_direct_prop );
	
	bn = BN_new() ;
	if( bn == NULL )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "BN_new error" )
		return -1;
	}
	
	rsakey_direct_prop->rsa = RSA_new() ;
	if( bn == NULL )
	{
		BN_free( bn );
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "RSA_new error" )
		return -1;
	}
	
	nret = BN_set_word( bn , e ) ;
	if( nret == 0 )
	{
		BN_free( bn );
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "BN_set_word error" )
		return -1;
	}
	
	nret = RSA_generate_key_ex( rsakey_direct_prop->rsa , bits , bn , NULL ) ;
	if( nret != 1 )
	{
		BN_free( bn );
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "RSA_generate_key_ex error" )
		return -1;
	}
	
	BN_free( bn );
	
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "RSA_generate_key ok , rsa[%p]" , rsakey_direct_prop->rsa )
	rsakey_direct_prop->has_pubkey = TRUE ;
	rsakey_direct_prop->has_prikey = TRUE ;
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_rsakey_GenerateKeyPair_int;
int ZlangInvokeFunction_rsakey_GenerateKeyPair_int( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_rsakey	*rsakey_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject			*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	int32_t					bits ;
	int					nret = 0 ;
	
	CallRuntimeFunction_int_GetIntValue( rt , in1 , & bits );
	
	nret = GenerateKeyPair( rt , rsakey_direct_prop , bits , RSA_F4 ) ;
	if( nret )
	{
		CallRuntimeFunction_bool_SetBoolValue( rt , out1 , FALSE );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_GENERATE_KEY_PAIR_FAILED );
	}
	else
	{
		CallRuntimeFunction_bool_SetBoolValue( rt , out1 , TRUE );
	}
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_rsakey_GenerateKeyPair_int_int;
int ZlangInvokeFunction_rsakey_GenerateKeyPair_int_int( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_rsakey	*rsakey_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject			*in2 = GetInputParameterInLocalObjectStack(rt,2) ;
	struct ZlangObject			*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	int32_t					bits ;
	int32_t					e ;
	int					nret = 0 ;
	
	CallRuntimeFunction_int_GetIntValue( rt , in1 , & bits );
	CallRuntimeFunction_int_GetIntValue( rt , in2 , & e );
	
	nret = GenerateKeyPair( rt , rsakey_direct_prop , bits , e ) ;
	if( nret )
	{
		CallRuntimeFunction_bool_SetBoolValue( rt , out1 , FALSE );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_GENERATE_KEY_PAIR_FAILED );
	}
	else
	{
		CallRuntimeFunction_bool_SetBoolValue( rt , out1 , TRUE );
	}
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_rsakey_ImportPublicKeyFromPEM_string;
int ZlangInvokeFunction_rsakey_ImportPublicKeyFromPEM_string( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_rsakey	*rsakey_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject			*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	char					*filename = NULL ;
	int32_t					filename_len ;
	FILE					*fp = NULL ;
	int					nret = 0 ;
	
	FreeRsa( rt , rsakey_direct_prop );
	
	CallRuntimeFunction_string_GetStringValue( rt , in1 , & filename , & filename_len );
	filename[filename_len] = '\0' ;
	
	fp = fopen( filename , "rb" ) ;
	if( fp == NULL )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "fopen failed , errno[%d]" , errno )
		CallRuntimeFunction_bool_SetBoolValue( rt , out1 , FALSE );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_FILE_NOT_FOUND );
	}
	
	rsakey_direct_prop->rsa = PEM_read_RSAPublicKey( fp , NULL , NULL , NULL ) ;
	fclose( fp );
	if( rsakey_direct_prop->rsa == NULL )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "PEM_read_RSAPublicKey failed[%d]" , nret )
		CallRuntimeFunction_bool_SetBoolValue( rt , out1 , FALSE );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_READ_RSA_PUBLIC_KEY_IN_FILE_FAILED );
	}
	
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "PEM_read_RSAPublicKey ok" )
	rsakey_direct_prop->has_pubkey = TRUE ;
	rsakey_direct_prop->has_prikey = FALSE ;
	CallRuntimeFunction_bool_SetBoolValue( rt , out1 , TRUE );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_rsakey_ImportPrivateKeyFromPEM_string;
int ZlangInvokeFunction_rsakey_ImportPrivateKeyFromPEM_string( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_rsakey	*rsakey_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject			*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	char					*filename = NULL ;
	int32_t					filename_len ;
	FILE					*fp = NULL ;
	int					nret = 0 ;
	
	FreeRsa( rt , rsakey_direct_prop );
	
	CallRuntimeFunction_string_GetStringValue( rt , in1 , & filename , & filename_len );
	filename[filename_len] = '\0' ;
	
	fp = fopen( filename , "rb" ) ;
	if( fp == NULL )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "fopen failed , errno[%d]" , errno )
		CallRuntimeFunction_bool_SetBoolValue( rt , out1 , FALSE );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_FILE_NOT_FOUND );
	}
	
	rsakey_direct_prop->rsa = PEM_read_RSAPrivateKey( fp , NULL , NULL , NULL ) ;
	fclose( fp );
	if( rsakey_direct_prop->rsa == NULL )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "PEM_read_RSAPrivateKey failed[%d]" , nret )
		CallRuntimeFunction_bool_SetBoolValue( rt , out1 , FALSE );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_READ_RSA_PRIVATE_KEY_IN_FILE_FAILED );
	}
	
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "PEM_read_RSAPrivateKey ok" )
	rsakey_direct_prop->has_pubkey = FALSE ;
	rsakey_direct_prop->has_prikey = TRUE ;
	CallRuntimeFunction_bool_SetBoolValue( rt , out1 , TRUE );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_rsakey_ImportPublicKeyFromDER_string;
int ZlangInvokeFunction_rsakey_ImportPublicKeyFromDER_string( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_rsakey	*rsakey_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject			*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	char					*filename = NULL ;
	int32_t					filename_len ;
	FILE					*fp = NULL ;
	int					nret = 0 ;
	
	FreeRsa( rt , rsakey_direct_prop );
	
	CallRuntimeFunction_string_GetStringValue( rt , in1 , & filename , & filename_len );
	filename[filename_len] = '\0' ;
	
	fp = fopen( filename , "rb" ) ;
	if( fp == NULL )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "fopen failed , errno[%d]" , errno )
		CallRuntimeFunction_bool_SetBoolValue( rt , out1 , FALSE );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_FILE_NOT_FOUND );
	}
	
	rsakey_direct_prop->rsa = d2i_RSAPublicKey_fp( fp , NULL ) ;
	fclose( fp );
	if( rsakey_direct_prop->rsa == NULL )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "d2i_RSAPublicKey_fp failed[%d]" , nret )
		CallRuntimeFunction_bool_SetBoolValue( rt , out1 , FALSE );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_READ_RSA_PUBLIC_KEY_IN_FILE_FAILED );
	}
	
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "d2i_RSAPublicKey_fp ok" )
	rsakey_direct_prop->has_pubkey = TRUE ;
	rsakey_direct_prop->has_prikey = FALSE ;
	CallRuntimeFunction_bool_SetBoolValue( rt , out1 , TRUE );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_rsakey_ImportPrivateKeyFromDER_string;
int ZlangInvokeFunction_rsakey_ImportPrivateKeyFromDER_string( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_rsakey	*rsakey_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject			*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	char					*filename = NULL ;
	int32_t					filename_len ;
	FILE					*fp = NULL ;
	int					nret = 0 ;
	
	FreeRsa( rt , rsakey_direct_prop );
	
	CallRuntimeFunction_string_GetStringValue( rt , in1 , & filename , & filename_len );
	filename[filename_len] = '\0' ;
	
	fp = fopen( filename , "rb" ) ;
	if( fp == NULL )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "fopen failed , errno[%d]" , errno )
		CallRuntimeFunction_bool_SetBoolValue( rt , out1 , FALSE );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_FILE_NOT_FOUND );
	}
	
	rsakey_direct_prop->rsa = d2i_RSAPrivateKey_fp( fp , NULL ) ;
	fclose( fp );
	if( rsakey_direct_prop->rsa == NULL )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "d2i_RSAPrivateKey_fp failed[%d]" , nret )
		CallRuntimeFunction_bool_SetBoolValue( rt , out1 , FALSE );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_READ_RSA_PUBLIC_KEY_IN_FILE_FAILED );
	}
	
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "d2i_RSAPrivateKey_fp ok" )
	rsakey_direct_prop->has_pubkey = FALSE ;
	rsakey_direct_prop->has_prikey = TRUE ;
	CallRuntimeFunction_bool_SetBoolValue( rt , out1 , TRUE );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_rsakey_ExportPublicKeyFromPEM_string;
int ZlangInvokeFunction_rsakey_ExportPublicKeyToPEM_string( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_rsakey	*rsakey_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject			*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	char					*filename = NULL ;
	int32_t					filename_len ;
	FILE					*fp = NULL ;
	int					nret = 0 ;
	
	if( rsakey_direct_prop->has_pubkey != TRUE )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "hasn't public key" )
		CallRuntimeFunction_bool_SetBoolValue( rt , out1 , FALSE );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_RSAKEY_HASNT_PUBLIC_KEY );
	}
	
	CallRuntimeFunction_string_GetStringValue( rt , in1 , & filename , & filename_len );
	filename[filename_len] = '\0' ;
	
	fp = fopen( filename , "wb" ) ;
	if( fp == NULL )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "fopen failed , errno[%d]" , errno )
		CallRuntimeFunction_bool_SetBoolValue( rt , out1 , FALSE );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_OPEN_WRITE_FILE_FAILED );
	}
	
	nret = PEM_write_RSAPublicKey( fp , rsakey_direct_prop->rsa ) ;
	fclose( fp );
	if( nret != 1 )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "PEM_write_RSAPublicKey failed[%d]" , nret )
		CallRuntimeFunction_bool_SetBoolValue( rt , out1 , FALSE );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_WRITE_RSA_PUBLIC_KEY_INTO_FILE_FAILED );
	}
	
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "PEM_write_RSAPublicKey ok" )
	CallRuntimeFunction_bool_SetBoolValue( rt , out1 , TRUE );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_rsakey_ExportPrivateKeyFromPEM_string;
int ZlangInvokeFunction_rsakey_ExportPrivateKeyToPEM_string( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_rsakey	*rsakey_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject			*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	char					*filename = NULL ;
	int32_t					filename_len ;
	FILE					*fp = NULL ;
	int					nret = 0 ;
	
	if( rsakey_direct_prop->has_prikey != TRUE )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "hasn't prilic key" )
		CallRuntimeFunction_bool_SetBoolValue( rt , out1 , FALSE );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_RSAKEY_HASNT_PRIVATE_KEY );
		return 0;
	}
	
	CallRuntimeFunction_string_GetStringValue( rt , in1 , & filename , & filename_len );
	filename[filename_len] = '\0' ;
	
	fp = fopen( filename , "wb" ) ;
	if( fp == NULL )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "fopen failed , errno[%d]" , errno )
		CallRuntimeFunction_bool_SetBoolValue( rt , out1 , FALSE );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_OPEN_WRITE_FILE_FAILED );
	}
	
	nret = PEM_write_RSAPrivateKey( fp , rsakey_direct_prop->rsa , NULL , NULL , 0 , NULL , NULL ) ;
	fclose( fp );
	if( nret != 1 )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "PEM_write_RSAPrivateKey failed[%d]" , nret )
		CallRuntimeFunction_bool_SetBoolValue( rt , out1 , FALSE );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_WRITE_RSA_PRIVATE_KEY_INTO_FILE_FAILED );
	}
	
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "PEM_write_RSAPrivateKey ok" )
	CallRuntimeFunction_bool_SetBoolValue( rt , out1 , TRUE );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_rsakey_ExportPublicKeyFromDER_string;
int ZlangInvokeFunction_rsakey_ExportPublicKeyToDER_string( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_rsakey	*rsakey_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject			*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	char					*filename = NULL ;
	int32_t					filename_len ;
	FILE					*fp = NULL ;
	int					nret = 0 ;
	
	if( rsakey_direct_prop->has_pubkey != TRUE )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "hasn't public key" )
		CallRuntimeFunction_bool_SetBoolValue( rt , out1 , FALSE );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_RSAKEY_HASNT_PUBLIC_KEY );
	}
	
	CallRuntimeFunction_string_GetStringValue( rt , in1 , & filename , & filename_len );
	filename[filename_len] = '\0' ;
	
	fp = fopen( filename , "wb" ) ;
	if( fp == NULL )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "fopen failed , errno[%d]" , errno )
		CallRuntimeFunction_bool_SetBoolValue( rt , out1 , FALSE );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_OPEN_WRITE_FILE_FAILED );
	}
	
	nret = i2d_RSAPublicKey_fp( fp , rsakey_direct_prop->rsa ) ;
	fclose( fp );
	if( nret != 1 )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "i2d_RSAPublicKey_fp failed[%d]" , nret )
		CallRuntimeFunction_bool_SetBoolValue( rt , out1 , FALSE );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_WRITE_RSA_PUBLIC_KEY_INTO_FILE_FAILED );
	}
	
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "i2d_RSAPublicKey_fp ok" )
	CallRuntimeFunction_bool_SetBoolValue( rt , out1 , TRUE );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_rsakey_ExportPrivateKeyFromDER_string;
int ZlangInvokeFunction_rsakey_ExportPrivateKeyToDER_string( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_rsakey	*rsakey_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject			*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	char					*filename = NULL ;
	int32_t					filename_len ;
	FILE					*fp = NULL ;
	int					nret = 0 ;
	
	if( rsakey_direct_prop->has_pubkey != TRUE )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "hasn't prilic key" )
		CallRuntimeFunction_bool_SetBoolValue( rt , out1 , FALSE );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_RSAKEY_HASNT_PUBLIC_KEY );
	}
	
	CallRuntimeFunction_string_GetStringValue( rt , in1 , & filename , & filename_len );
	filename[filename_len] = '\0' ;
	
	fp = fopen( filename , "wb" ) ;
	if( fp == NULL )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "fopen failed , errno[%d]" , errno )
		CallRuntimeFunction_bool_SetBoolValue( rt , out1 , FALSE );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_OPEN_WRITE_FILE_FAILED );
		return 0;
	}
	
	nret = i2d_RSAPrivateKey_fp( fp , rsakey_direct_prop->rsa ) ;
	fclose( fp );
	if( nret != 1 )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "i2d_RSAPrivateKey_fp failed[%d]" , nret )
		CallRuntimeFunction_bool_SetBoolValue( rt , out1 , FALSE );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_WRITE_RSA_PRIVATE_KEY_INTO_FILE_FAILED );
	}
	
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "i2d_RSAPrivateKey_fp ok" )
	CallRuntimeFunction_bool_SetBoolValue( rt , out1 , TRUE );
	
	return 0;
}

ZlangCreateDirectPropertyFunction ZlangCreateDirectProperty_rsakey;
void *ZlangCreateDirectProperty_rsakey( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_rsakey	*rsakey_direct_prop = NULL ;
	
	rsakey_direct_prop = (struct ZlangDirectProperty_rsakey *)ZLMALLOC( sizeof(struct ZlangDirectProperty_rsakey) ) ;
	if( rsakey_direct_prop == NULL )
	{
		SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_ALLOC , "alloc memory for entity" )
		return NULL;
	}
	memset( rsakey_direct_prop , 0x00 , sizeof(struct ZlangDirectProperty_rsakey) );
	
	rsakey_direct_prop->has_pubkey = FALSE ;
	rsakey_direct_prop->has_prikey = FALSE ;
	
	return rsakey_direct_prop;
}

ZlangDestroyDirectPropertyFunction ZlangDestroyDirectProperty_rsakey;
void ZlangDestroyDirectProperty_rsakey( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_rsakey	*rsakey_direct_prop = GetObjectDirectProperty(obj) ;
	
	FreeRsa( rt , rsakey_direct_prop );
	
	ZLFREE( rsakey_direct_prop );
	
	return;
}

ZlangSummarizeDirectPropertySizeFunction ZlangSummarizeDirectPropertySize_rsakey;
void ZlangSummarizeDirectPropertySize_rsakey( struct ZlangRuntime *rt , struct ZlangObject *obj , size_t *summarized_obj_size , size_t *summarized_direct_prop_size )
{
	SUMMARIZE_SIZE( summarized_direct_prop_size , sizeof(struct ZlangDirectProperty_rsakey) )
	return;
}

static struct ZlangDirectFunctions direct_funcs_rsakey =
	{
		ZLANG_OBJECT_rsakey , /* char *ancestor_name */
		
		ZlangCreateDirectProperty_rsakey , /* ZlangCreateDirectPropertyFunction *create_entity_func */
		ZlangDestroyDirectProperty_rsakey , /* ZlangDestroyDirectPropertyFunction *destroy_entity_func */
		
		NULL , /* ZlangFromCharPtrFunction *from_char_ptr_func */
		NULL , /* ZlangToStringFunction *to_string_func */
		NULL , /* ZlangFromDataPtrFunction *from_data_ptr_func */
		NULL , /* ZlangGetDataPtrFunction *get_data_ptr_func */
		
		NULL , /* ZlangOperatorFunction *oper_PLUS_func */
		NULL , /* ZlangOperatorFunction *oper_MINUS_func */
		NULL , /* ZlangOperatorFunction *oper_MUL_func */
		NULL , /* ZlangOperatorFunction *oper_DIV_func */
		NULL , /* ZlangOperatorFunction *oper_MOD_func */
		
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_NEGATIVE_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_NOT_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_BIT_REVERSE_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_PLUS_PLUS_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_MINUS_MINUS_func */
		
		NULL , /* ZlangCompareFunction *comp_EGUAL_func */
		NULL , /* ZlangCompareFunction *comp_NOTEGUAL_func */
		NULL , /* ZlangCompareFunction *comp_LT_func */
		NULL , /* ZlangCompareFunction *comp_LE_func */
		NULL , /* ZlangCompareFunction *comp_GT_func */
		NULL , /* ZlangCompareFunction *comp_GE_func */
		
		NULL , /* ZlangLogicFunction *logic_AND_func */
		NULL , /* ZlangLogicFunction *logic_OR_func */
		
		NULL , /* ZlangLogicFunction *bit_AND_func */
		NULL , /* ZlangLogicFunction *bit_XOR_func */
		NULL , /* ZlangLogicFunction *bit_OR_func */
		NULL , /* ZlangLogicFunction *bit_MOVELEFT_func */
		NULL , /* ZlangLogicFunction *bit_MOVERIGHT_func */
		
		ZlangSummarizeDirectPropertySize_rsakey , /* ZlangSummarizeDirectPropertySizeFunction *summarize_direct_prop_size_func */
	} ;

ZlangImportObjectFunction ZlangImportObject_rsakey;
struct ZlangObject *ZlangImportObject_rsakey( struct ZlangRuntime *rt )
{
	struct ZlangObject	*obj = NULL ;
	struct ZlangFunction	*func = NULL ;
	int			nret = 0 ;
	
	nret = ImportObject( rt , & obj , ZLANG_OBJECT_rsakey , & direct_funcs_rsakey , sizeof(struct ZlangDirectFunctions) , NULL ) ;
	if( nret )
	{
		SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_LINK_FUNC_TO_ENTITY , "import object to global objects heap" )
		return NULL;
	}
	
	/* rsakey.GenerateKeyPair(int) */
	func = AddFunctionAndParametersInObject( rt , obj , "GenerateKeyPair" , "GenerateKeyPair(int)" , ZlangInvokeFunction_rsakey_GenerateKeyPair_int , ZLANG_OBJECT_bool , ZLANG_OBJECT_int,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* rsakey.GenerateKeyPair(int,int) */
	func = AddFunctionAndParametersInObject( rt , obj , "GenerateKeyPair" , "GenerateKeyPair(int,int)" , ZlangInvokeFunction_rsakey_GenerateKeyPair_int_int , ZLANG_OBJECT_bool , ZLANG_OBJECT_int,NULL , ZLANG_OBJECT_int,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* rsakey.ImportPublicKeyFromPEM(string) */
	func = AddFunctionAndParametersInObject( rt , obj , "ImportPublicKeyFromPEM" , "ImportPublicKeyFromPEM(string)" , ZlangInvokeFunction_rsakey_ImportPublicKeyFromPEM_string , ZLANG_OBJECT_bool , ZLANG_OBJECT_string,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* rsakey.ImportPrivateKeyFromPEM(string) */
	func = AddFunctionAndParametersInObject( rt , obj , "ImportPrivateKeyFromPEM" , "ImportPrivateKeyFromPEM(string)" , ZlangInvokeFunction_rsakey_ImportPrivateKeyFromPEM_string , ZLANG_OBJECT_bool , ZLANG_OBJECT_string,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* rsakey.ImportPublicKeyFromDER(string) */
	func = AddFunctionAndParametersInObject( rt , obj , "ImportPublicKeyFromDER" , "ImportPublicKeyFromDER(string)" , ZlangInvokeFunction_rsakey_ImportPublicKeyFromDER_string , ZLANG_OBJECT_bool , ZLANG_OBJECT_string,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* rsakey.ImportPrivateKeyFromDER(string) */
	func = AddFunctionAndParametersInObject( rt , obj , "ImportPrivateKeyFromDER" , "ImportPrivateKeyFromDER(string)" , ZlangInvokeFunction_rsakey_ImportPrivateKeyFromDER_string , ZLANG_OBJECT_bool , ZLANG_OBJECT_string,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* rsakey.ExportPublicKeyToPEM(string) */
	func = AddFunctionAndParametersInObject( rt , obj , "ExportPublicKeyToPEM" , "ExportPublicKeyToPEM(string)" , ZlangInvokeFunction_rsakey_ExportPublicKeyToPEM_string , ZLANG_OBJECT_bool , ZLANG_OBJECT_string,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* rsakey.ExportPrivateKeyToPEM(string) */
	func = AddFunctionAndParametersInObject( rt , obj , "ExportPrivateKeyToPEM" , "ExportPrivateKeyToPEM(string)" , ZlangInvokeFunction_rsakey_ExportPrivateKeyToPEM_string , ZLANG_OBJECT_bool , ZLANG_OBJECT_string,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* rsakey.ExportPublicKeyToDER(string) */
	func = AddFunctionAndParametersInObject( rt , obj , "ExportPublicKeyToDER" , "ExportPublicKeyToDER(string)" , ZlangInvokeFunction_rsakey_ExportPublicKeyToDER_string , ZLANG_OBJECT_bool , ZLANG_OBJECT_string,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* rsakey.ExportPrivateKeyToDER(string) */
	func = AddFunctionAndParametersInObject( rt , obj , "ExportPrivateKeyToDER" , "ExportPrivateKeyToDER(string)" , ZlangInvokeFunction_rsakey_ExportPrivateKeyToDER_string , ZLANG_OBJECT_bool , ZLANG_OBJECT_string,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	return obj ;
}

