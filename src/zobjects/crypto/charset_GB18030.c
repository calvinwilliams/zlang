/* Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

static struct ZlangCharsetAlias		g_zlang_charset_aliases_GB18030[] = {
		{ ZLANG_CHARSET_GB18030 , "摘要" , "Digest" } ,
		{ ZLANG_CHARSET_GB18030 , "加密" , "Encrypt" } ,
		{ ZLANG_CHARSET_GB18030 , "解密" , "Decrypt" } ,
		{ ZLANG_CHARSET_GB18030 , "生成密钥对" , "GenerateKeyPair" } ,
		{ ZLANG_CHARSET_GB18030 , "从PEM文件导入公钥" , "ImportPublicKeyFromPEM" } ,
		{ ZLANG_CHARSET_GB18030 , "从PEM文件导入私钥" , "ImportPrivateKeyFromPEM" } ,
		{ ZLANG_CHARSET_GB18030 , "从DER文件导入公钥" , "ImportPublicKeyFromDER" } ,
		{ ZLANG_CHARSET_GB18030 , "从DER文件导入私钥" , "ImportPrivateKeyFromDER" } ,
		{ ZLANG_CHARSET_GB18030 , "导出公钥到PEM文件" , "ExportPublicKeyToPEM" } ,
		{ ZLANG_CHARSET_GB18030 , "导出私钥到PEM文件" , "ExportPrivateKeyToPEM" } ,
		{ ZLANG_CHARSET_GB18030 , "导出公钥到DER文件" , "ExportPublicKeyToDER" } ,
		{ ZLANG_CHARSET_GB18030 , "导出私钥到DER文件" , "ExportPrivateKeyToDER" } ,
		{ ZLANG_CHARSET_GB18030 , "公钥加密" , "PublicKeyEncrypt" } ,
		{ ZLANG_CHARSET_GB18030 , "私钥加密" , "PrivateKeyEncrypt" } ,
		{ ZLANG_CHARSET_GB18030 , "公钥解密" , "PublicKeyDecrypt" } ,
		{ ZLANG_CHARSET_GB18030 , "私钥解密" , "PrivateKeyDecrypt" } ,
		{ ZLANG_CHARSET_GB18030 , "签名" , "Sign" } ,
		{ ZLANG_CHARSET_GB18030 , "验签" , "" } ,
		{ ZLANG_CHARSET_GB18030 , "" , "" } ,
		{ 0 , NULL , NULL } ,
	} ;

