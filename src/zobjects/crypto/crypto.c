/* Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "zobjects_crypto.h"

struct ZlangDirectProperty_crypto
{
	int	dummy ;
} ;

ZlangCreateDirectPropertyFunction ZlangCreateDirectProperty_crypto;
void *ZlangCreateDirectProperty_crypto( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_crypto	*crypto_direct_prop = NULL ;
	
	crypto_direct_prop = (struct ZlangDirectProperty_crypto *)ZLMALLOC( sizeof(struct ZlangDirectProperty_crypto) ) ;
	if( crypto_direct_prop == NULL )
	{
		SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_ALLOC , "alloc memory for entity" )
		return NULL;
	}
	memset( crypto_direct_prop , 0x00 , sizeof(struct ZlangDirectProperty_crypto) );
	
	OpenSSL_add_all_algorithms();
	SSL_library_init();
	ERR_load_SSL_strings();
	
	return crypto_direct_prop;
}

ZlangDestroyDirectPropertyFunction ZlangDestroyDirectProperty_crypto;
void ZlangDestroyDirectProperty_crypto( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_crypto	*crypto_direct_prop = GetObjectDirectProperty(obj) ;
	
	ZLFREE( crypto_direct_prop );
	
	return;
}

ZlangSummarizeDirectPropertySizeFunction ZlangSummarizeDirectPropertySize_crypto;
void ZlangSummarizeDirectPropertySize_crypto( struct ZlangRuntime *rt , struct ZlangObject *obj , size_t *summarized_obj_size , size_t *summarized_direct_prop_size )
{
	SUMMARIZE_SIZE( summarized_direct_prop_size , sizeof(struct ZlangDirectProperty_crypto) )
	return;
}

static struct ZlangDirectFunctions direct_funcs_crypto =
	{
		ZLANG_OBJECT_crypto , /* char *ancestor_name */
		
		ZlangCreateDirectProperty_crypto , /* ZlangCreateDirectPropertyFunction *create_entity_func */
		ZlangDestroyDirectProperty_crypto , /* ZlangDestroyDirectPropertyFunction *cryptotroy_entity_func */
		
		NULL , /* ZlangFromCharPtrFunction *from_char_ptr_func */
		NULL , /* ZlangToStringFunction *to_string_func */
		NULL , /* ZlangFromDataPtrFunction *from_data_ptr_func */
		NULL , /* ZlangGetDataPtrFunction *get_data_ptr_func */
		
		NULL , /* ZlangOperatorFunction *oper_PLUS_func */
		NULL , /* ZlangOperatorFunction *oper_MINUS_func */
		NULL , /* ZlangOperatorFunction *oper_MUL_func */
		NULL , /* ZlangOperatorFunction *oper_DIV_func */
		NULL , /* ZlangOperatorFunction *oper_MOD_func */
		
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_NEGATIVE_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_NOT_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_BIT_REVERSE_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_PLUS_PLUS_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_MINUS_MINUS_func */
		
		NULL , /* ZlangCompareFunction *comp_EGUAL_func */
		NULL , /* ZlangCompareFunction *comp_NOTEGUAL_func */
		NULL , /* ZlangCompareFunction *comp_LT_func */
		NULL , /* ZlangCompareFunction *comp_LE_func */
		NULL , /* ZlangCompareFunction *comp_GT_func */
		NULL , /* ZlangCompareFunction *comp_GE_func */
		
		NULL , /* ZlangLogicFunction *logic_AND_func */
		NULL , /* ZlangLogicFunction *logic_OR_func */
		
		NULL , /* ZlangLogicFunction *bit_AND_func */
		NULL , /* ZlangLogicFunction *bit_XOR_func */
		NULL , /* ZlangLogicFunction *bit_OR_func */
		NULL , /* ZlangLogicFunction *bit_MOVELEFT_func */
		NULL , /* ZlangLogicFunction *bit_MOVERIGHT_func */
		
		ZlangSummarizeDirectPropertySize_crypto , /* ZlangSummarizeDirectPropertySizeFunction *summarize_direct_prop_size_func */
	} ;

ZlangImportObjectFunction ZlangImportObject_crypto;
struct ZlangObject *ZlangImportObject_crypto( struct ZlangRuntime *rt )
{
	struct ZlangObject	*obj = NULL ;
	struct ZlangObject	*prop = NULL ;
	int			nret = 0 ;
	
	nret = ImportObject( rt , & obj , ZLANG_OBJECT_crypto , & direct_funcs_crypto , sizeof(struct ZlangDirectFunctions) , NULL ) ;
	if( nret )
	{
		SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_LINK_FUNC_TO_ENTITY , "import object to global objects heap" )
		return NULL;
	}
	
	/* int crypto.ECB_MODE */
	prop = AddPropertyInObject( rt , obj , QueryObjectByObjectName(rt,"int") , "ECB_MODE" ) ;
	if( prop == NULL )
		return NULL;
	CallRuntimeFunction_int_SetIntValue( rt , prop , CRYPTO_ECB_MODE );
	SetConstantObject( prop );
	
	/* int crypto.CBC_MODE */
	prop = AddPropertyInObject( rt , obj , QueryObjectByObjectName(rt,"int") , "CBC_MODE" ) ;
	if( prop == NULL )
		return NULL;
	CallRuntimeFunction_int_SetIntValue( rt , prop , CRYPTO_CBC_MODE );
	SetConstantObject( prop );
	
	/* int crypto.KEY_64BITS */
	prop = AddPropertyInObject( rt , obj , QueryObjectByObjectName(rt,"int") , "KEY_64BITS" ) ;
	if( prop == NULL )
		return NULL;
	CallRuntimeFunction_int_SetIntValue( rt , prop , CRYPTO_KEY_64BITS );
	SetConstantObject( prop );
	
	/* int crypto.KEY_128BITS */
	prop = AddPropertyInObject( rt , obj , QueryObjectByObjectName(rt,"int") , "KEY_128BITS" ) ;
	if( prop == NULL )
		return NULL;
	CallRuntimeFunction_int_SetIntValue( rt , prop , CRYPTO_KEY_128BITS );
	SetConstantObject( prop );
	
	/* int crypto.KEY_192BITS */
	prop = AddPropertyInObject( rt , obj , QueryObjectByObjectName(rt,"int") , "KEY_192BITS" ) ;
	if( prop == NULL )
		return NULL;
	CallRuntimeFunction_int_SetIntValue( rt , prop , CRYPTO_KEY_192BITS );
	SetConstantObject( prop );
	
	/* int crypto.KEY_256BITS */
	prop = AddPropertyInObject( rt , obj , QueryObjectByObjectName(rt,"int") , "KEY_256BITS" ) ;
	if( prop == NULL )
		return NULL;
	CallRuntimeFunction_int_SetIntValue( rt , prop , CRYPTO_KEY_256BITS );
	SetConstantObject( prop );
	
	/* int crypto.KEY_512BITS */
	prop = AddPropertyInObject( rt , obj , QueryObjectByObjectName(rt,"int") , "KEY_512BITS" ) ;
	if( prop == NULL )
		return NULL;
	CallRuntimeFunction_int_SetIntValue( rt , prop , CRYPTO_KEY_512BITS );
	SetConstantObject( prop );
	
	/* int crypto.KEY_1024BITS */
	prop = AddPropertyInObject( rt , obj , QueryObjectByObjectName(rt,"int") , "KEY_1024BITS" ) ;
	if( prop == NULL )
		return NULL;
	CallRuntimeFunction_int_SetIntValue( rt , prop , CRYPTO_KEY_1024BITS );
	SetConstantObject( prop );
	
	/* int crypto.KEY_2048BITS */
	prop = AddPropertyInObject( rt , obj , QueryObjectByObjectName(rt,"int") , "KEY_2048BITS" ) ;
	if( prop == NULL )
		return NULL;
	CallRuntimeFunction_int_SetIntValue( rt , prop , CRYPTO_KEY_2048BITS );
	SetConstantObject( prop );
	
	/* int crypto.KEY_4096BITS */
	prop = AddPropertyInObject( rt , obj , QueryObjectByObjectName(rt,"int") , "KEY_4096BITS" ) ;
	if( prop == NULL )
		return NULL;
	CallRuntimeFunction_int_SetIntValue( rt , prop , CRYPTO_KEY_4096BITS );
	SetConstantObject( prop );
	
	/* int crypto.E_3 */
	prop = AddPropertyInObject( rt , obj , QueryObjectByObjectName(rt,"int") , "E_3" ) ;
	if( prop == NULL )
		return NULL;
	CallRuntimeFunction_int_SetIntValue( rt , prop , CRYPTO_E_3 );
	SetConstantObject( prop );
	
	/* int crypto.E_F4 */
	prop = AddPropertyInObject( rt , obj , QueryObjectByObjectName(rt,"int") , "E_F4" ) ;
	if( prop == NULL )
		return NULL;
	CallRuntimeFunction_int_SetIntValue( rt , prop , CRYPTO_E_F4 );
	SetConstantObject( prop );
	
	/* int crypto.NO_PADDING */
	prop = AddPropertyInObject( rt , obj , QueryObjectByObjectName(rt,"int") , "NO_PADDING" ) ;
	if( prop == NULL )
		return NULL;
	CallRuntimeFunction_int_SetIntValue( rt , prop , CRYPTO_NO_PADDING );
	SetConstantObject( prop );
	
	/* int crypto.ZERO_PADDING */
	prop = AddPropertyInObject( rt , obj , QueryObjectByObjectName(rt,"int") , "ZERO_PADDING" ) ;
	if( prop == NULL )
		return NULL;
	CallRuntimeFunction_int_SetIntValue( rt , prop , CRYPTO_ZERO_PADDING );
	SetConstantObject( prop );
	
	/* int crypto.PKCS7_PADDING */
	prop = AddPropertyInObject( rt , obj , QueryObjectByObjectName(rt,"int") , "PKCS7_PADDING" ) ;
	if( prop == NULL )
		return NULL;
	CallRuntimeFunction_int_SetIntValue( rt , prop , CRYPTO_PKCS7_PADDING );
	SetConstantObject( prop );
	
	/* int crypto.PKCS1_PADDING */
	prop = AddPropertyInObject( rt , obj , QueryObjectByObjectName(rt,"int") , "PKCS1_PADDING" ) ;
	if( prop == NULL )
		return NULL;
	CallRuntimeFunction_int_SetIntValue( rt , prop , CRYPTO_PKCS1_PADDING );
	SetConstantObject( prop );
	
	/* int crypto.PKCS1_OAEP_PADDING */
	prop = AddPropertyInObject( rt , obj , QueryObjectByObjectName(rt,"int") , "PKCS1_OAEP_PADDING" ) ;
	if( prop == NULL )
		return NULL;
	CallRuntimeFunction_int_SetIntValue( rt , prop , CRYPTO_PKCS1_OAEP_PADDING );
	SetConstantObject( prop );
	
	/* int crypto.NID_MD5 */
	prop = AddPropertyInObject( rt , obj , QueryObjectByObjectName(rt,"int") , "NID_MD5" ) ;
	if( prop == NULL )
		return NULL;
	CallRuntimeFunction_int_SetIntValue( rt , prop , CRYPTO_NID_MD5 );
	SetConstantObject( prop );
	
	/* int crypto.NID_SHA1 */
	prop = AddPropertyInObject( rt , obj , QueryObjectByObjectName(rt,"int") , "NID_SHA1" ) ;
	if( prop == NULL )
		return NULL;
	CallRuntimeFunction_int_SetIntValue( rt , prop , CRYPTO_NID_SHA1 );
	SetConstantObject( prop );
	
	/* int crypto.NID_SHA224 */
	prop = AddPropertyInObject( rt , obj , QueryObjectByObjectName(rt,"int") , "NID_SHA224" ) ;
	if( prop == NULL )
		return NULL;
	CallRuntimeFunction_int_SetIntValue( rt , prop , CRYPTO_NID_SHA224 );
	SetConstantObject( prop );
	
	/* int crypto.NID_SHA256 */
	prop = AddPropertyInObject( rt , obj , QueryObjectByObjectName(rt,"int") , "NID_SHA256" ) ;
	if( prop == NULL )
		return NULL;
	CallRuntimeFunction_int_SetIntValue( rt , prop , CRYPTO_NID_SHA256 );
	SetConstantObject( prop );
	
	/* int crypto.NID_SHA384 */
	prop = AddPropertyInObject( rt , obj , QueryObjectByObjectName(rt,"int") , "NID_SHA384" ) ;
	if( prop == NULL )
		return NULL;
	CallRuntimeFunction_int_SetIntValue( rt , prop , CRYPTO_NID_SHA384 );
	SetConstantObject( prop );
	
	/* int crypto.NID_SHA512 */
	prop = AddPropertyInObject( rt , obj , QueryObjectByObjectName(rt,"int") , "NID_SHA512" ) ;
	if( prop == NULL )
		return NULL;
	CallRuntimeFunction_int_SetIntValue( rt , prop , CRYPTO_NID_SHA512 );
	SetConstantObject( prop );
	
	/* int crypto.NID_MD5_SHA1 */
	prop = AddPropertyInObject( rt , obj , QueryObjectByObjectName(rt,"int") , "NID_MD5_SHA1" ) ;
	if( prop == NULL )
		return NULL;
	CallRuntimeFunction_int_SetIntValue( rt , prop , CRYPTO_NID_MD5_SHA1 );
	SetConstantObject( prop );
	
	return obj ;
}

