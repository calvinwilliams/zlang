/* Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

static struct ZlangCharsetAlias		g_zlang_charset_aliases_GB18030[] = {
		{ ZLANG_CHARSET_GB18030 , "中央式缓存" , "redis" } ,
		{ ZLANG_CHARSET_GB18030 , "设置" , "Set" } ,
		{ ZLANG_CHARSET_GB18030 , "得到" , "Get" } ,
		{ ZLANG_CHARSET_GB18030 , "得到多个" , "MGet" } ,
		{ ZLANG_CHARSET_GB18030 , "设置连接池最小空闲连接数" , "SetMinIdleSessionsCount" } ,
		{ ZLANG_CHARSET_GB18030 , "设置连接池最大空闲连接数" , "SetMaxIdleSessionsCount" } ,
		{ ZLANG_CHARSET_GB18030 , "设置连接池最大连接数" , "SetMaxSessionsCount" } ,
		{ ZLANG_CHARSET_GB18030 , "设置连接池最大空闲存活周期" , "SetMaxIdleTimeval" } ,
		{ ZLANG_CHARSET_GB18030 , "设置连接池观察空闲周期" , "SetWatchIdleTimeval" } ,
		{ ZLANG_CHARSET_GB18030 , "设置连接池激活周期" , "SetInspectTimeval" } ,
		{ 0 , NULL , NULL } ,
	} ;

