/* Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "zobjects_redis.h"

struct RedisConnectionContext
{
	redisContext			*redis_ctx ;
} ;

struct ZlangDirectProperty_redis
{
	char				*ip ;
	int32_t				port ;
	
	unsigned char			assistant_thread_enable ;
	int32_t				min_idle_connections_count ;
	int32_t				max_idle_connections_count ;
	int32_t				max_connections_count ;
	int32_t				max_idle_timeval ;
	int32_t				watch_idle_timeval ;
	int32_t				inspect_timeval ;
	struct CommonPool		*connections_pool ;
	
	struct ZlangRuntime		*rt ;
	
	struct RedisConnectionContext	*redis_conn_ctx ;
	struct ZlangObject		*command_buf ;
} ;

funcInitSessionContext InitConnectionContext;
int InitConnectionContext( struct CommonPool *pool , void *session_ctx )
{
	struct RedisConnectionContext		*redis_conn_ctx = (struct RedisConnectionContext *)session_ctx ;
	struct ZlangDirectProperty_redis	*redis_direct_prop = (struct ZlangDirectProperty_redis *)GetCommonPoolUserData( pool ) ;
	struct ZlangRuntime			*rt = redis_direct_prop->rt ;
	
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "InitSessionContext pool[%p] redis_conn_ctx[%p] redis_direct_prop[%p] rt[%p]" , pool , redis_conn_ctx , redis_direct_prop , rt )
	
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "redisConnect ..." )
	redis_conn_ctx->redis_ctx = redisConnect( redis_direct_prop->ip , (int)(redis_direct_prop->port) ) ;
	if( redis_conn_ctx->redis_ctx == NULL )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "redisConnect failed" )
		return -1;
	}
	else
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "redisConnect ok" )
	}
	
	return 0;
}

static int TestConnection( struct ZlangDirectProperty_redis *redis_direct_prop , struct RedisConnectionContext *redis_conn_ctx )
{
	struct ZlangRuntime			*rt = redis_direct_prop->rt ;
	redisReply				*redis_reply = NULL ;
	
	redis_reply = redisCommand( redis_conn_ctx->redis_ctx , REDIS_PING_STR ) ;
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "redisCommand[%s] , return reply[%p] type[%d]" , REDIS_PING_STR , redis_reply , redis_reply?redis_reply->type:0 )
	if( redis_reply == NULL )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "redisCommand failed , return[%p]" , redis_reply )
		return -1;
	}
	else if( redis_reply->type != REDIS_REPLY_STATUS )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "redisCommand failed , return[%p]" , redis_reply )
		freeReplyObject( redis_reply );
		return -2;
	}
	
	if( STRICMP( redis_reply->str , == , REDIS_PONG_STR ) )
	{
		freeReplyObject( redis_reply );
		return 0;
	}
	else
	{
		freeReplyObject( redis_reply );
		return -3;
	}
}

funcWatchingSessions WatchingConnections;
int WatchingConnections( struct CommonPool *pool )
{
	size_t					i ;
	size_t					idle_sessions_count ;
	struct RedisConnectionContext		*redis_conn_ctx = NULL ;
	struct ZlangDirectProperty_redis	*redis_direct_prop = (struct ZlangDirectProperty_redis *)GetCommonPoolUserData( pool ) ;
	struct ZlangRuntime			*rt = redis_direct_prop->rt ;
	int					nret = 0 ;
	
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "WatchingSessions pool[%p] redis_direct_prop[%p] rt[%p]" , pool , redis_direct_prop , rt )
	
	idle_sessions_count = GetCommonPoolIdleSessionsCount( pool ) ;
	for( i = 0 ; i < idle_sessions_count ; i++ )
	{
		nret = JustFetchCommonPoolIdleSession( pool , (void**) & redis_conn_ctx ) ;
		if( nret == COMMONPOOL_ERROR_NO_IDLE_SESSION )
			break;
		
		nret = TestConnection( redis_direct_prop , redis_conn_ctx ) ;
		if( nret )
			ReleaseCommonPoolIdleSession( pool , redis_conn_ctx );
		else
			GivebackCommonPoolSession( pool , redis_conn_ctx );
	}
	
	return 0;
}

funcCleanSessionContext CleanConnectionContext;
int CleanConnectionContext( struct CommonPool *pool , void *session_ctx )
{
	struct RedisConnectionContext		*redis_conn_ctx = (struct RedisConnectionContext *)session_ctx ;
	struct ZlangDirectProperty_redis	*redis_direct_prop = (struct ZlangDirectProperty_redis *)GetCommonPoolUserData( pool ) ;
	struct ZlangRuntime			*rt = redis_direct_prop->rt ;
	
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "CleanSessionContext pool[%p] session_ctx[%p]" , pool , session_ctx )
	
	redisFree( redis_conn_ctx->redis_ctx );
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "redisFree ok" )
	
	return 0;
}

static struct CommonPoolCallback	g_connections_pool_callback =
	{
		sizeof(struct RedisConnectionContext)
		, InitConnectionContext
		, WatchingConnections
		, CleanConnectionContext
	} ;

int CreateAndStartCommonPool( struct ZlangObject *redis_obj )
{
	struct ZlangDirectProperty_redis	*redis_direct_prop = GetObjectDirectProperty(redis_obj) ;
	int					nret = 0 ;
	
	redis_direct_prop->connections_pool = CreateCommonPool( & g_connections_pool_callback , sizeof(struct CommonPoolCallback) ) ;
	if( redis_direct_prop->connections_pool == NULL )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( redis_direct_prop->rt , "CreateCommonPool failed" )
		return -1;
	}
	else
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( redis_direct_prop->rt , "CreateCommonPool ok" )
	}
	
	TEST_RUNTIME_DEBUG_THEN_PRINT( redis_direct_prop->rt , "EnableAssistantThread[%d]" , redis_direct_prop->assistant_thread_enable )
	EnableAssistantThread( redis_direct_prop->connections_pool , redis_direct_prop->assistant_thread_enable );
	TEST_RUNTIME_DEBUG_THEN_PRINT( redis_direct_prop->rt , "SetCommonPoolMinIdleSessionsCount[%"PRIi32"]" , redis_direct_prop->min_idle_connections_count )
	SetCommonPoolMinIdleSessionsCount( redis_direct_prop->connections_pool , redis_direct_prop->min_idle_connections_count );
	TEST_RUNTIME_DEBUG_THEN_PRINT( redis_direct_prop->rt , "SetCommonPoolMaxIdleSessionsCount[%"PRIi32"]" , redis_direct_prop->max_idle_connections_count )
	SetCommonPoolMaxIdleSessionsCount( redis_direct_prop->connections_pool , redis_direct_prop->max_idle_connections_count );
	TEST_RUNTIME_DEBUG_THEN_PRINT( redis_direct_prop->rt , "SetCommonPoolMaxSessionsCount[%"PRIi32"]" , redis_direct_prop->max_connections_count )
	SetCommonPoolMaxSessionsCount( redis_direct_prop->connections_pool , redis_direct_prop->max_connections_count );
	TEST_RUNTIME_DEBUG_THEN_PRINT( redis_direct_prop->rt , "SetCommonPoolMaxIdleTimeval[%"PRIi32"]" , redis_direct_prop->max_idle_timeval )
	SetCommonPoolMaxIdleTimeval( redis_direct_prop->connections_pool , redis_direct_prop->max_idle_timeval );
	TEST_RUNTIME_DEBUG_THEN_PRINT( redis_direct_prop->rt , "SetCommonPoolWatchIdleTimeval[%"PRIi32"]" , redis_direct_prop->watch_idle_timeval )
	SetCommonPoolWatchIdleTimeval( redis_direct_prop->connections_pool , redis_direct_prop->watch_idle_timeval );
	TEST_RUNTIME_DEBUG_THEN_PRINT( redis_direct_prop->rt , "SetCommonPoolInspectTimeval[%"PRIi32"]" , redis_direct_prop->inspect_timeval )
	SetCommonPoolInspectTimeval( redis_direct_prop->connections_pool , redis_direct_prop->inspect_timeval );
	
	SetCommonPoolUserData( redis_direct_prop->connections_pool , redis_direct_prop );
	
	nret = StartCommonPool( redis_direct_prop->connections_pool ) ;
	if( nret )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( redis_direct_prop->rt , "StartCommonPool failed[%d]" , nret )
		return -1;
	}
	else
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( redis_direct_prop->rt , "StartCommonPool ok" )
	}
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_redis_EnableAssistantThread_bool;
int ZlangInvokeFunction_redis_EnableAssistantThread_bool( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_redis	*redis_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*in1_obj = GetInputParameterInLocalObjectStack(rt,1) ;
	
	CallRuntimeFunction_bool_GetBoolValue( rt , in1_obj , & (redis_direct_prop->assistant_thread_enable) );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_redis_SetMinIdleConnectionsCount_int;
int ZlangInvokeFunction_redis_SetMinIdleConnectionsCount_int( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_redis	*redis_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*in1_obj = GetInputParameterInLocalObjectStack(rt,1) ;
	
	CallRuntimeFunction_int_GetIntValue( rt , in1_obj , & (redis_direct_prop->min_idle_connections_count) );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_redis_SetMaxIdleConnectionsCount_int;
int ZlangInvokeFunction_redis_SetMaxIdleConnectionsCount_int( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_redis	*redis_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*in1_obj = GetInputParameterInLocalObjectStack(rt,1) ;
	
	CallRuntimeFunction_int_GetIntValue( rt , in1_obj , & (redis_direct_prop->max_idle_connections_count) );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_redis_SetMaxConnectionsCount_int;
int ZlangInvokeFunction_redis_SetMaxConnectionsCount_int( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_redis	*redis_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*in1_obj = GetInputParameterInLocalObjectStack(rt,1) ;
	
	CallRuntimeFunction_int_GetIntValue( rt , in1_obj , & (redis_direct_prop->max_connections_count) );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_redis_SetMaxIdleTimeval_int;
int ZlangInvokeFunction_redis_SetMaxIdleTimeval_int( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_redis	*redis_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*in1_obj = GetInputParameterInLocalObjectStack(rt,1) ;
	
	CallRuntimeFunction_int_GetIntValue( rt , in1_obj , & (redis_direct_prop->max_idle_timeval) );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_redis_SetWatchIdleTimeval_int;
int ZlangInvokeFunction_redis_SetWatchIdleTimeval_int( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_redis	*redis_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*in1_obj = GetInputParameterInLocalObjectStack(rt,1) ;
	
	CallRuntimeFunction_int_GetIntValue( rt , in1_obj , & (redis_direct_prop->watch_idle_timeval) );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_redis_SetInspectTimeval_int;
int ZlangInvokeFunction_redis_SetInspectTimeval_int( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_redis	*redis_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*in1_obj = GetInputParameterInLocalObjectStack(rt,1) ;
	
	CallRuntimeFunction_int_GetIntValue( rt , in1_obj , & (redis_direct_prop->inspect_timeval) );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_redis_Connect_string_int;
int ZlangInvokeFunction_redis_Connect_string_int( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_redis	*redis_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject			*in2 = GetInputParameterInLocalObjectStack(rt,2) ;
	struct ZlangObject			*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	char					*ip = NULL ;
	int32_t					port ;
	int					nret = 0 ;
	
	CallRuntimeFunction_string_GetStringValue( rt , in1 , & ip , NULL );
	CallRuntimeFunction_int_GetIntValue( rt , in2 , & port );
	
	if( redis_direct_prop->connections_pool )
	{
		StopCommonPool( redis_direct_prop->connections_pool );
		DestroyCommonPool( redis_direct_prop->connections_pool ); redis_direct_prop->connections_pool = NULL ;
	}
	
	redis_direct_prop->rt = rt ;
	redis_direct_prop->ip = ZLSTRDUP( ip ) ;
	redis_direct_prop->port = port ;
	nret = CreateAndStartCommonPool( obj ) ;
	if( nret )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "CreateAndStartCommonPool failed[%d]" , nret )
		CallRuntimeFunction_bool_SetBoolValue( rt , out1 , FALSE );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_CONNECT_REDIS_SERVER_FAILED );
	}
	else
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "CreateAndStartCommonPool ok" )
	}
	
	CallRuntimeFunction_bool_SetBoolValue( rt , out1 , TRUE );
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_redis_Disconnect;
int ZlangInvokeFunction_redis_Disconnect( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_redis	*redis_direct_prop = GetObjectDirectProperty(obj) ;
	
	if( redis_direct_prop->connections_pool )
	{
		StopCommonPool( redis_direct_prop->connections_pool );
		DestroyCommonPool( redis_direct_prop->connections_pool ); redis_direct_prop->connections_pool = NULL ;
	}
	
	return 0;
}

#define SET_IN_FROM_STRING(_cmd_) \
	nret = CallRuntimeFunction_string_SetStringValue( rt , redis_direct_prop->command_buf , (_cmd_) , STRLEN_OF_STRING ); \
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "CallRuntimeFunction_string_SetStringValue[%s] return[%d]" , (_cmd_) , nret ) \
	if( nret ) \
	{ \
		UnreferObject( rt , out1 ); \
		return ThrowFatalException( rt , ZLANG_FATAL_INVOKE_METHOD_RETURN , EXCEPTION_MESSAGE_ALLOC_FAILED ); \
	} \
	
#define APPEND_IN_FROM_INPUTPARAMETER_STRING(_input_parameter_no_) \
	{ \
		char		*str = NULL ; \
		CallRuntimeFunction_string_GetStringValue( rt , GetInputParameterInLocalObjectStack(rt,(_input_parameter_no_)) , & str , NULL ); \
		nret = CallRuntimeFunction_string_AppendFormatStringValue( rt , redis_direct_prop->command_buf , " %s" , str ); \
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "CallRuntimeFunction_string_AppendFormatStringValue[%s] return[%d]" , str , nret ) \
		if( nret ) \
		{ \
			UnreferObject( rt , out1 ); \
			return ThrowFatalException( rt , ZLANG_FATAL_INVOKE_METHOD_RETURN , EXCEPTION_MESSAGE_ALLOC_FAILED ); \
		} \
	} \

#define APPEND_IN_FROM_INPUTPARAMETER_INT(_input_parameter_no_) \
	{ \
		int32_t		n ; \
		CallRuntimeFunction_int_GetIntValue( rt , GetInputParameterInLocalObjectStack(rt,(_input_parameter_no_)) , & n ); \
		nret = CallRuntimeFunction_string_AppendFormatStringValue( rt , redis_direct_prop->command_buf , " %"PRIi32 , n ); \
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "CallRuntimeFunction_string_AppendFormatStringValue[%"PRIi32"] return[%d]" , n , nret ) \
		if( nret ) \
		{ \
			UnreferObject( rt , out1 ); \
			return ThrowFatalException( rt , ZLANG_FATAL_INVOKE_METHOD_RETURN , EXCEPTION_MESSAGE_ALLOC_FAILED ); \
		} \
	} \

#define APPEND_IN_FROM_INPUTPARAMETER_LONG(_input_parameter_no_) \
	{ \
		int64_t		n ; \
		CallRuntimeFunction_long_GetLongValue( rt , GetInputParameterInLocalObjectStack(rt,(_input_parameter_no_)) , & n ); \
		nret = CallRuntimeFunction_string_AppendFormatStringValue( rt , redis_direct_prop->command_buf , " %"PRIi64 , n ); \
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "CallRuntimeFunction_string_AppendFormatStringValue[%"PRIi64"] return[%d]" , n , nret ) \
		if( nret ) \
		{ \
			UnreferObject( rt , out1 ); \
			return ThrowFatalException( rt , ZLANG_FATAL_INVOKE_METHOD_RETURN , EXCEPTION_MESSAGE_ALLOC_FAILED ); \
		} \
	} \

#define APPEND_IN_FROM_INPUTPARAMETERSTACK_STRING \
	{ \
		int			inN_count = GetInputParameterCountInLocalObjectStack( rt ) ; \
		int			inN_index ; \
		struct ZlangObject	*inN = NULL ; \
		char			*str = NULL ; \
		\
		for( inN_index = 1 ; inN_index <= inN_count ; inN_index++ ) \
		{ \
			inN = GetInputParameterInLocalObjectStack( rt , inN_index ) ; \
			if( IsTypeOf( rt , inN , GetStringObjectInRuntimeObjectsHeap(rt) ) == 0 ) \
			{ \
				TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "in param type is not string" ) \
				UnreferObject( rt , out1 ); \
				return ThrowFatalException( rt , ZLANG_FATAL_INVOKE_METHOD_RETURN , EXCEPTION_MESSAGE_ALLOC_FAILED ); \
			} \
			\
			CallRuntimeFunction_string_GetStringValue( rt , inN , & str , NULL ); \
			nret = CallRuntimeFunction_string_AppendFormatStringValue( rt , redis_direct_prop->command_buf , " %s" , str ); \
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "CallRuntimeFunction_string_AppendFormatStringValue[%s] return[%d]" , str , nret ) \
			if( nret ) \
			{ \
				UnreferObject( rt , out1 ); \
				return ThrowFatalException( rt , ZLANG_FATAL_INVOKE_METHOD_RETURN , EXCEPTION_MESSAGE_ALLOC_FAILED ); \
			} \
		} \
	} \

#define APPEND_IN_FROM_INPUTPARAMETERSTACK_STRING_OR_INT \
	{ \
		int			inN_count = GetInputParameterCountInLocalObjectStack( rt ) ; \
		int			inN_index ; \
		struct ZlangObject	*inN = NULL ; \
		char			*str = NULL ; \
		int32_t			n ; \
		\
		for( inN_index = 1 ; inN_index <= inN_count ; inN_index++ ) \
		{ \
			inN = GetInputParameterInLocalObjectStack( rt , inN_index ) ; \
			if( IsTypeOf( rt , inN , GetStringObjectInRuntimeObjectsHeap(rt) ) ) \
			{ \
				CallRuntimeFunction_string_GetStringValue( rt , inN , & str , NULL ); \
				nret = CallRuntimeFunction_string_AppendFormatStringValue( rt , redis_direct_prop->command_buf , " %s" , str ); \
				TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "CallRuntimeFunction_string_AppendFormatStringValue[%s] return[%d]" , str , nret ) \
				if( nret ) \
				{ \
					UnreferObject( rt , out1 ); \
					return ThrowFatalException( rt , ZLANG_FATAL_INVOKE_METHOD_RETURN , EXCEPTION_MESSAGE_ALLOC_FAILED ); \
				} \
			} \
			else if( IsTypeOf( rt , inN , GetIntObjectInRuntimeObjectsHeap(rt) ) ) \
			{ \
				CallRuntimeFunction_int_GetIntValue( rt , inN , & n ); \
				nret = CallRuntimeFunction_string_AppendFormatStringValue( rt , redis_direct_prop->command_buf , " %"PRIi32 , n ); \
				TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "CallRuntimeFunction_string_AppendFormatStringValue[%s] return[%d]" , str , nret ) \
				if( nret ) \
				{ \
					UnreferObject( rt , out1 ); \
					return ThrowFatalException( rt , ZLANG_FATAL_INVOKE_METHOD_RETURN , EXCEPTION_MESSAGE_ALLOC_FAILED ); \
				} \
			} \
			else \
			{ \
				TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "in param type is not string" ) \
				UnreferObject( rt , out1 ); \
				return 0; \
			} \
		} \
	} \

#define FETCH_REDIS_CONNECTION(_error_statement_) \
	if( redis_direct_prop->connections_pool == NULL ) \
	{ \
		nret = CreateAndStartCommonPool( obj ) ; \
		if( nret ) \
		{ \
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "CreateAndStartCommonPool failed[%d]" , nret ) \
			_error_statement_ \
			return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_CONNECT_REDIS_SERVER_FAILED ); \
		} \
		else \
		{ \
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "CreateAndStartCommonPool ok" ) \
		} \
	} \
	\
	nret = FetchCommonPoolSession( redis_direct_prop->connections_pool , (void **) & redis_conn_ctx ) ; \
	if( nret ) \
	{ \
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "FetchCommonPoolSession failed[%d]" , nret ) \
		_error_statement_ \
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_FETCH_CONNECTION_FAILED ); \
	} \
	else \
	{ \
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "FetchCommonPoolSession ok , session_ctx[%p]" , redis_conn_ctx ) \
	} \

#define CALL_REDIS_COMMAND(_expect_redis_reply_type_) \
	{ \
		char		*cmd = NULL ; \
		CallRuntimeFunction_string_GetStringValue( rt , redis_direct_prop->command_buf , & cmd , NULL ); \
		redis_reply = redisCommand( redis_conn_ctx->redis_ctx , cmd ) ; \
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "redisCommand[%s] , return reply[%p]type[%d] , TYPE:STRING[%d]ARRAY[%d]INTEGER[%d]NIL[%d]STATUS[%d]ERROR[%d]" , cmd , redis_reply , redis_reply?redis_reply->type:0 , REDIS_REPLY_STRING , REDIS_REPLY_ARRAY , REDIS_REPLY_INTEGER , REDIS_REPLY_NIL , REDIS_REPLY_STATUS , REDIS_REPLY_ERROR ) \
		if( redis_reply == NULL ) \
		{ \
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "redisCommand failed , return[%p]" , redis_reply ) \
			RELEASE_REDIS_CONNECTION \
			UnreferObject( rt , out1 ); \
			return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_EXECUTE_REDIS_COMMAND_FAILED ); \
		} \
		else if( redis_reply->type != _expect_redis_reply_type_ ) \
		{ \
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "redisCommand response invalid , return[%p]" , redis_reply ) \
			GIVEBACK_REDIS_CONNECTION \
			freeReplyObject( redis_reply ); \
			UnreferObject( rt , out1 ); \
			return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_REDIS_RESPONSE_TYPE_NOT_MATCHED ); \
		} \
	} \

#define CALL_REDIS_COMMAND2(_expect_redis_reply_type_,_or_redis_reply_type_) \
	{ \
		char		*cmd = NULL ; \
		CallRuntimeFunction_string_GetStringValue( rt , redis_direct_prop->command_buf , & cmd , NULL ); \
		redis_reply = redisCommand( redis_conn_ctx->redis_ctx , cmd ) ; \
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "redisCommand[%s] , return reply[%p]type[%d] , TYPE:STRING[%d]ARRAY[%d]INTEGER[%d]NIL[%d]STATUS[%d]ERROR[%d]" , cmd , redis_reply , redis_reply?redis_reply->type:0 , REDIS_REPLY_STRING , REDIS_REPLY_ARRAY , REDIS_REPLY_INTEGER , REDIS_REPLY_NIL , REDIS_REPLY_STATUS , REDIS_REPLY_ERROR ) \
		if( redis_reply == NULL ) \
		{ \
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "redisCommand failed , return[%p]" , redis_reply ) \
			RELEASE_REDIS_CONNECTION \
			UnreferObject( rt , out1 ); \
			return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_EXECUTE_REDIS_COMMAND_FAILED ); \
		} \
		else if( redis_reply->type != _expect_redis_reply_type_ && redis_reply->type != _or_redis_reply_type_ ) \
		{ \
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "redisCommand response invalid , return[%p]" , redis_reply ) \
			GIVEBACK_REDIS_CONNECTION \
			freeReplyObject( redis_reply ); \
			UnreferObject( rt , out1 ); \
			return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_REDIS_RESPONSE_TYPE_NOT_MATCHED ); \
		} \
	} \

#define RELEASE_REDIS_CONNECTION \
	{ \
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "ReleaseCommonPoolIdleSession , session_ctx[%p]" , redis_conn_ctx ) \
		ReleaseCommonPoolIdleSession( redis_direct_prop->connections_pool , redis_conn_ctx ); \
	} \

#define GIVEBACK_REDIS_CONNECTION \
	{ \
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "GivebackCommonPoolSession , session_ctx[%p]" , redis_conn_ctx ) \
		GivebackCommonPoolSession( redis_direct_prop->connections_pool , redis_conn_ctx ); \
	} \

#define IF_REDISREPLY_NIL_TYPE_THEN_SET_OUT_NULL \
	if( redis_reply->type == REDIS_REPLY_NIL ) \
	{ \
		freeReplyObject( redis_reply ); \
		UnreferObject( rt , out1 ); \
		return 0; \
	} \

#define SET_OUT_TRUE_FROM_REDISREPLY_STR_OK \
	{ \
		if( STRICMP( redis_reply->str , == , "OK" ) ) \
		{ \
			CallRuntimeFunction_bool_SetBoolValue( rt , out1 , TRUE ); \
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "CallRuntimeFunction_bool_SetBoolValue[TRUE]" ) \
		} \
		else \
		{ \
			CallRuntimeFunction_bool_SetBoolValue( rt , out1 , FALSE ); \
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "CallRuntimeFunction_bool_SetBoolValue[FALSE]" ) \
		} \
	} \

#define SET_OUT_FROM_REDISREPLY_STRING \
	{ \
		CallRuntimeFunction_string_SetStringValue( rt , out1 , redis_reply->str , redis_reply->len ); \
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "CallRuntimeFunction_string_SetStringValue[%.*s]" , (int)(redis_reply->len),redis_reply->str ) \
	} \

#define SET_OUT_FROM_REDISREPLY_INTEGER \
	{ \
		CallRuntimeFunction_int_SetIntValue( rt , out1 , (int32_t)(redis_reply->integer) ); \
	} \

#define SET_OUT_TRUE_FROM_REDISREPLY_INTEGER_VALUE(_count_) \
	{ \
		if( redis_reply->integer == (_count_) ) \
		{ \
			CallRuntimeFunction_bool_SetBoolValue( rt , out1 , TRUE ); \
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "CallRuntimeFunction_bool_SetBoolValue[TRUE]" ) \
		} \
		else \
		{ \
			CallRuntimeFunction_bool_SetBoolValue( rt , out1 , FALSE ); \
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "CallRuntimeFunction_bool_SetBoolValue[FALSE]" ) \
		} \
	} \

#define SET_OUT_FROM_REDISREPLY_ARRAY \
	{ \
		size_t			redis_reply_array_count ; \
		size_t			redis_reply_array_index ; \
		struct ZlangObject	*array_element = NULL ; \
		\
		redis_reply_array_count = redis_reply->elements ; \
		for( redis_reply_array_index = 0 ; redis_reply_array_index < redis_reply_array_count ; redis_reply_array_index++ ) \
		{ \
			if( redis_reply->element[redis_reply_array_index]->type == REDIS_REPLY_STRING ) \
			{ \
				array_element = CloneStringObject( rt , NULL ) ; \
				if( array_element == NULL ) \
				{ \
					TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "CloneStringObject failed" ); \
					freeReplyObject( redis_reply ); \
					UnreferObject( rt , out1 ); \
					return ThrowFatalException( rt , ZLANG_FATAL_INVOKE_METHOD_RETURN , EXCEPTION_MESSAGE_ALLOC_FAILED ); \
				} \
				\
				CallRuntimeFunction_string_SetStringValue( rt , array_element , redis_reply->element[redis_reply_array_index]->str , redis_reply->element[redis_reply_array_index]->len ); \
				TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "CallRuntimeFunction_string_SetStringValue([%.*s]" , (int)(redis_reply->element[redis_reply_array_index]->len),redis_reply->element[redis_reply_array_index]->str ) \
			} \
			else if( redis_reply->element[redis_reply_array_index]->type == REDIS_REPLY_NIL ) \
			{ \
				array_element = CloneNullObject( rt , NULL ) ; \
				if( array_element == NULL ) \
				{ \
					TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "CloneStringObject failed" ); \
					freeReplyObject( redis_reply ); \
					UnreferObject( rt , out1 ); \
					return ThrowFatalException( rt , ZLANG_FATAL_INVOKE_METHOD_RETURN , EXCEPTION_MESSAGE_ALLOC_FAILED ); \
				} \
			} \
			else \
			{ \
				TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "redis reply array item type[%d] invalid" , redis_reply->element[redis_reply_array_index]->type ); \
				freeReplyObject( redis_reply ); \
				UnreferObject( rt , out1 ); \
				return ThrowFatalException( rt , ZLANG_FATAL_INVOKE_METHOD_RETURN , EXCEPTION_MESSAGE_ALLOC_FAILED ); \
			} \
			\
			nret = CallRuntimeFunction_array_Append( rt , out1 , array_element , NULL ) ; \
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "CallRuntimeFunction_array_Append[%p] return[%d]" , array_element , nret ) \
			if( nret ) \
			{ \
				TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "CallRuntimeFunction_array_Append failed[%d]" , nret ); \
				DestroyObject( rt , array_element ); \
				freeReplyObject( redis_reply ); \
				UnreferObject( rt , out1 ); \
				return ThrowFatalException( rt , ZLANG_FATAL_INVOKE_METHOD_RETURN , EXCEPTION_MESSAGE_ALLOC_FAILED ); \
			} \
			\
			DestroyObject( rt , array_element ); \
		} \
	} \

ZlangInvokeFunction ZlangInvokeFunction_redis_DEL_vargs;
int ZlangInvokeFunction_redis_DEL_vargs( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_redis	*redis_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	struct RedisConnectionContext		*redis_conn_ctx ;
	redisReply				*redis_reply = NULL ;
	int					nret = 0 ;
	
	SET_IN_FROM_STRING( "DEL" )
	APPEND_IN_FROM_INPUTPARAMETERSTACK_STRING
	
	FETCH_REDIS_CONNECTION( UnreferObject( rt , out1 ); )
	CALL_REDIS_COMMAND( REDIS_REPLY_INTEGER )
	GIVEBACK_REDIS_CONNECTION
	
	SET_OUT_FROM_REDISREPLY_INTEGER
	
	freeReplyObject( redis_reply );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_redis_EXISTS_string;
int ZlangInvokeFunction_redis_EXISTS_string( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_redis	*redis_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	struct RedisConnectionContext		*redis_conn_ctx ;
	redisReply				*redis_reply = NULL ;
	int					nret = 0 ;
	
	SET_IN_FROM_STRING( "EXISTS" )
	APPEND_IN_FROM_INPUTPARAMETER_STRING( 1 )
	
	FETCH_REDIS_CONNECTION( UnreferObject( rt , out1 ); )
	CALL_REDIS_COMMAND( REDIS_REPLY_INTEGER )
	GIVEBACK_REDIS_CONNECTION
	
	SET_OUT_FROM_REDISREPLY_INTEGER
	
	freeReplyObject( redis_reply );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_redis_KEYS_string;
int ZlangInvokeFunction_redis_KEYS_string( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_redis	*redis_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	struct RedisConnectionContext		*redis_conn_ctx ;
	redisReply				*redis_reply = NULL ;
	int					nret = 0 ;
	
	SET_IN_FROM_STRING( "KEYS" )
	APPEND_IN_FROM_INPUTPARAMETER_STRING( 1 )
	
	FETCH_REDIS_CONNECTION( UnreferObject( rt , out1 ); )
	CALL_REDIS_COMMAND( REDIS_REPLY_ARRAY )
	GIVEBACK_REDIS_CONNECTION
	
	SET_OUT_FROM_REDISREPLY_ARRAY
	
	freeReplyObject( redis_reply );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_redis_SET_string_string_int_long_string;
int ZlangInvokeFunction_redis_SET_string_string_int_long_string( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_redis	*redis_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	struct RedisConnectionContext		*redis_conn_ctx ;
	redisReply				*redis_reply = NULL ;
	int					nret = 0 ;
	
	SET_IN_FROM_STRING( "SET" )
	APPEND_IN_FROM_INPUTPARAMETER_STRING( 1 )
	APPEND_IN_FROM_INPUTPARAMETER_STRING( 2 )
	
	if( ! IsObjectPropertiesEntityNull( GetInputParameterInLocalObjectStack(rt,3) ) )
	{
		APPEND_IN_FROM_INPUTPARAMETER_INT( 3 )
	}
	
	if( ! IsObjectPropertiesEntityNull( GetInputParameterInLocalObjectStack(rt,4) ) )
	{
		APPEND_IN_FROM_INPUTPARAMETER_LONG( 4 )
	}
	
	if( ! IsObjectPropertiesEntityNull( GetInputParameterInLocalObjectStack(rt,5) ) )
	{
		APPEND_IN_FROM_INPUTPARAMETER_STRING( 5 )
	}
	
	FETCH_REDIS_CONNECTION( CallRuntimeFunction_bool_SetBoolValue( rt , out1 , FALSE ); )
	CALL_REDIS_COMMAND2( REDIS_REPLY_STATUS , REDIS_REPLY_NIL )
	GIVEBACK_REDIS_CONNECTION
	
	IF_REDISREPLY_NIL_TYPE_THEN_SET_OUT_NULL
	SET_OUT_TRUE_FROM_REDISREPLY_STR_OK
	
	freeReplyObject( redis_reply );
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_redis_GET_string;
int ZlangInvokeFunction_redis_GET_string( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_redis	*redis_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	struct RedisConnectionContext		*redis_conn_ctx ;
	redisReply				*redis_reply = NULL ;
	int					nret = 0 ;
	
	SET_IN_FROM_STRING( "GET" )
	APPEND_IN_FROM_INPUTPARAMETER_STRING( 1 )
	
	FETCH_REDIS_CONNECTION( UnreferObject( rt , out1 ); )
	CALL_REDIS_COMMAND( REDIS_REPLY_STRING )
	GIVEBACK_REDIS_CONNECTION
	
	IF_REDISREPLY_NIL_TYPE_THEN_SET_OUT_NULL
	SET_OUT_FROM_REDISREPLY_STRING
	
	freeReplyObject( redis_reply );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_redis_MGET_vargs;
int ZlangInvokeFunction_redis_MGET_vargs( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_redis	*redis_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	struct RedisConnectionContext		*redis_conn_ctx ;
	redisReply				*redis_reply = NULL ;
	int					nret = 0 ;
	
	SET_IN_FROM_STRING( "MGET" )
	APPEND_IN_FROM_INPUTPARAMETERSTACK_STRING
	
	FETCH_REDIS_CONNECTION( UnreferObject( rt , out1 ); )
	CALL_REDIS_COMMAND( REDIS_REPLY_ARRAY )
	GIVEBACK_REDIS_CONNECTION
	
	SET_OUT_FROM_REDISREPLY_ARRAY
	
	freeReplyObject( redis_reply );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_redis_HSET_string_string_string;
int ZlangInvokeFunction_redis_HSET_string_string_string( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_redis	*redis_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	struct RedisConnectionContext		*redis_conn_ctx ;
	redisReply				*redis_reply = NULL ;
	int					nret = 0 ;
	
	SET_IN_FROM_STRING( "MSET" )
	APPEND_IN_FROM_INPUTPARAMETER_STRING( 1 )
	APPEND_IN_FROM_INPUTPARAMETER_STRING( 2 )
	APPEND_IN_FROM_INPUTPARAMETER_STRING( 3 )
	
	FETCH_REDIS_CONNECTION( UnreferObject( rt , out1 ); )
	CALL_REDIS_COMMAND( REDIS_REPLY_INTEGER )
	GIVEBACK_REDIS_CONNECTION
	
	SET_OUT_FROM_REDISREPLY_INTEGER
	
	freeReplyObject( redis_reply );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_redis_HGET_string_string;
int ZlangInvokeFunction_redis_HGET_string_string( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_redis	*redis_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	struct RedisConnectionContext		*redis_conn_ctx ;
	redisReply				*redis_reply = NULL ;
	int					nret = 0 ;
	
	SET_IN_FROM_STRING( "HGET" )
	APPEND_IN_FROM_INPUTPARAMETER_STRING( 1 )
	APPEND_IN_FROM_INPUTPARAMETER_STRING( 2 )
	
	FETCH_REDIS_CONNECTION( UnreferObject( rt , out1 ); )
	CALL_REDIS_COMMAND( REDIS_REPLY_STRING )
	GIVEBACK_REDIS_CONNECTION
	
	IF_REDISREPLY_NIL_TYPE_THEN_SET_OUT_NULL
	SET_OUT_FROM_REDISREPLY_STRING
	
	freeReplyObject( redis_reply );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_redis_HMSET_vargs;
int ZlangInvokeFunction_redis_HMSET_vargs( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_redis	*redis_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	struct RedisConnectionContext		*redis_conn_ctx ;
	redisReply				*redis_reply = NULL ;
	int					nret = 0 ;
	
	SET_IN_FROM_STRING( "HMSET" )
	APPEND_IN_FROM_INPUTPARAMETERSTACK_STRING
	
	FETCH_REDIS_CONNECTION( UnreferObject( rt , out1 ); )
	CALL_REDIS_COMMAND( REDIS_REPLY_STATUS )
	GIVEBACK_REDIS_CONNECTION
	
	SET_OUT_TRUE_FROM_REDISREPLY_STR_OK
	
	freeReplyObject( redis_reply );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_redis_HMGET_vargs;
int ZlangInvokeFunction_redis_HMGET_vargs( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_redis	*redis_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	struct RedisConnectionContext		*redis_conn_ctx ;
	redisReply				*redis_reply = NULL ;
	int					nret = 0 ;
	
	SET_IN_FROM_STRING( "HMGET" )
	APPEND_IN_FROM_INPUTPARAMETERSTACK_STRING
	
	FETCH_REDIS_CONNECTION( UnreferObject( rt , out1 ); )
	CALL_REDIS_COMMAND( REDIS_REPLY_ARRAY )
	GIVEBACK_REDIS_CONNECTION
	
	SET_OUT_FROM_REDISREPLY_ARRAY
	
	freeReplyObject( redis_reply );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_redis_HGETALL_string;
int ZlangInvokeFunction_redis_HGETALL_string( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_redis	*redis_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	struct RedisConnectionContext		*redis_conn_ctx ;
	redisReply				*redis_reply = NULL ;
	int					nret = 0 ;
	
	SET_IN_FROM_STRING( "HGETALL" )
	APPEND_IN_FROM_INPUTPARAMETER_STRING( 1 )
	
	FETCH_REDIS_CONNECTION( UnreferObject( rt , out1 ); )
	CALL_REDIS_COMMAND( REDIS_REPLY_ARRAY )
	GIVEBACK_REDIS_CONNECTION
	
	SET_OUT_FROM_REDISREPLY_ARRAY
	
	freeReplyObject( redis_reply );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_redis_HKEYS_string;
int ZlangInvokeFunction_redis_HKEYS_string( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_redis	*redis_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	struct RedisConnectionContext		*redis_conn_ctx ;
	redisReply				*redis_reply = NULL ;
	int					nret = 0 ;
	
	SET_IN_FROM_STRING( "HKEYS" )
	APPEND_IN_FROM_INPUTPARAMETER_STRING( 1 )
	
	FETCH_REDIS_CONNECTION( UnreferObject( rt , out1 ); )
	CALL_REDIS_COMMAND( REDIS_REPLY_ARRAY )
	GIVEBACK_REDIS_CONNECTION
	
	SET_OUT_FROM_REDISREPLY_ARRAY
	
	freeReplyObject( redis_reply );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_redis_HVALS_string;
int ZlangInvokeFunction_redis_HVALS_string( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_redis	*redis_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	struct RedisConnectionContext		*redis_conn_ctx ;
	redisReply				*redis_reply = NULL ;
	int					nret = 0 ;
	
	SET_IN_FROM_STRING( "HVALS" )
	APPEND_IN_FROM_INPUTPARAMETER_STRING( 1 )
	
	FETCH_REDIS_CONNECTION( UnreferObject( rt , out1 ); )
	CALL_REDIS_COMMAND( REDIS_REPLY_ARRAY )
	GIVEBACK_REDIS_CONNECTION
	
	SET_OUT_FROM_REDISREPLY_ARRAY
	
	freeReplyObject( redis_reply );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_redis_LPUSH_vargs;
int ZlangInvokeFunction_redis_LPUSH_vargs( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_redis	*redis_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	struct RedisConnectionContext		*redis_conn_ctx ;
	redisReply				*redis_reply = NULL ;
	int					nret = 0 ;
	
	SET_IN_FROM_STRING( "LPUSH" )
	APPEND_IN_FROM_INPUTPARAMETERSTACK_STRING
	
	FETCH_REDIS_CONNECTION( UnreferObject( rt , out1 ); )
	CALL_REDIS_COMMAND( REDIS_REPLY_INTEGER )
	GIVEBACK_REDIS_CONNECTION
	
	CallRuntimeFunction_int_SetIntValue( rt , out1 , (int32_t)(redis_reply->integer) );
	
	freeReplyObject( redis_reply );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_redis_LPOP_string;
int ZlangInvokeFunction_redis_LPOP_string( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_redis	*redis_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	struct RedisConnectionContext		*redis_conn_ctx ;
	redisReply				*redis_reply = NULL ;
	int					nret = 0 ;
	
	SET_IN_FROM_STRING( "LPOP" )
	APPEND_IN_FROM_INPUTPARAMETER_STRING( 1 )
	
	FETCH_REDIS_CONNECTION( UnreferObject( rt , out1 ); )
	CALL_REDIS_COMMAND( REDIS_REPLY_ARRAY )
	GIVEBACK_REDIS_CONNECTION
	
	IF_REDISREPLY_NIL_TYPE_THEN_SET_OUT_NULL
	SET_OUT_FROM_REDISREPLY_ARRAY
	
	freeReplyObject( redis_reply );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_redis_RPUSH_vargs;
int ZlangInvokeFunction_redis_RPUSH_vargs( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_redis	*redis_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	struct RedisConnectionContext		*redis_conn_ctx ;
	redisReply				*redis_reply = NULL ;
	int					nret = 0 ;
	
	SET_IN_FROM_STRING( "RPUSH" )
	APPEND_IN_FROM_INPUTPARAMETERSTACK_STRING
	
	FETCH_REDIS_CONNECTION( UnreferObject( rt , out1 ); )
	CALL_REDIS_COMMAND( REDIS_REPLY_INTEGER )
	GIVEBACK_REDIS_CONNECTION
	
	CallRuntimeFunction_int_SetIntValue( rt , out1 , (int32_t)(redis_reply->integer) );
	
	freeReplyObject( redis_reply );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_redis_RPOP_string;
int ZlangInvokeFunction_redis_RPOP_string( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_redis	*redis_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	struct RedisConnectionContext		*redis_conn_ctx ;
	redisReply				*redis_reply = NULL ;
	int					nret = 0 ;
	
	SET_IN_FROM_STRING( "RPOP" )
	APPEND_IN_FROM_INPUTPARAMETER_STRING( 1 )
	
	FETCH_REDIS_CONNECTION( UnreferObject( rt , out1 ); )
	CALL_REDIS_COMMAND( REDIS_REPLY_STRING )
	GIVEBACK_REDIS_CONNECTION
	
	IF_REDISREPLY_NIL_TYPE_THEN_SET_OUT_NULL
	SET_OUT_FROM_REDISREPLY_STRING
	
	freeReplyObject( redis_reply );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_redis_LLEN_string;
int ZlangInvokeFunction_redis_LLEN_string( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_redis	*redis_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	struct RedisConnectionContext		*redis_conn_ctx ;
	redisReply				*redis_reply = NULL ;
	int					nret = 0 ;
	
	SET_IN_FROM_STRING( "LLEN" )
	APPEND_IN_FROM_INPUTPARAMETER_STRING( 1 )
	
	FETCH_REDIS_CONNECTION( UnreferObject( rt , out1 ); )
	CALL_REDIS_COMMAND( REDIS_REPLY_INTEGER )
	GIVEBACK_REDIS_CONNECTION
	
	SET_OUT_FROM_REDISREPLY_INTEGER
	
	freeReplyObject( redis_reply );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_redis_LRANGE_string_int_int;
int ZlangInvokeFunction_redis_LRANGE_string_int_int( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_redis	*redis_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	struct RedisConnectionContext		*redis_conn_ctx ;
	redisReply				*redis_reply = NULL ;
	int					nret = 0 ;
	
	SET_IN_FROM_STRING( "LRANGE" )
	APPEND_IN_FROM_INPUTPARAMETER_STRING( 1 )
	APPEND_IN_FROM_INPUTPARAMETER_INT( 2 )
	APPEND_IN_FROM_INPUTPARAMETER_INT( 3 )
	
	FETCH_REDIS_CONNECTION( UnreferObject( rt , out1 ); )
	CALL_REDIS_COMMAND( REDIS_REPLY_ARRAY )
	GIVEBACK_REDIS_CONNECTION
	
	SET_OUT_FROM_REDISREPLY_ARRAY
	
	freeReplyObject( redis_reply );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_redis_BLPOP_vargs;
int ZlangInvokeFunction_redis_BLPOP_vargs( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_redis	*redis_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	struct RedisConnectionContext		*redis_conn_ctx ;
	redisReply				*redis_reply = NULL ;
	int					nret = 0 ;
	
	SET_IN_FROM_STRING( "BLPOP" )
	APPEND_IN_FROM_INPUTPARAMETERSTACK_STRING_OR_INT
	
	FETCH_REDIS_CONNECTION( UnreferObject( rt , out1 ); )
	CALL_REDIS_COMMAND( REDIS_REPLY_ARRAY )
	GIVEBACK_REDIS_CONNECTION
	
	SET_OUT_FROM_REDISREPLY_ARRAY
	
	freeReplyObject( redis_reply );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_redis_BRPOP_vargs;
int ZlangInvokeFunction_redis_BRPOP_vargs( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_redis	*redis_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	struct RedisConnectionContext		*redis_conn_ctx ;
	redisReply				*redis_reply = NULL ;
	int					nret = 0 ;
	
	SET_IN_FROM_STRING( "BRPOP" )
	APPEND_IN_FROM_INPUTPARAMETERSTACK_STRING_OR_INT
	
	FETCH_REDIS_CONNECTION( UnreferObject( rt , out1 ); )
	CALL_REDIS_COMMAND( REDIS_REPLY_ARRAY )
	GIVEBACK_REDIS_CONNECTION
	
	IF_REDISREPLY_NIL_TYPE_THEN_SET_OUT_NULL
	SET_OUT_FROM_REDISREPLY_ARRAY
	
	freeReplyObject( redis_reply );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_redis_SADD_vargs;
int ZlangInvokeFunction_redis_SADD_vargs( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_redis	*redis_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	struct RedisConnectionContext		*redis_conn_ctx ;
	redisReply				*redis_reply = NULL ;
	int					nret = 0 ;
	
	SET_IN_FROM_STRING( "SADD" )
	APPEND_IN_FROM_INPUTPARAMETERSTACK_STRING
	
	FETCH_REDIS_CONNECTION( UnreferObject( rt , out1 ); )
	CALL_REDIS_COMMAND( REDIS_REPLY_INTEGER )
	GIVEBACK_REDIS_CONNECTION
	
	IF_REDISREPLY_NIL_TYPE_THEN_SET_OUT_NULL
	SET_OUT_FROM_REDISREPLY_INTEGER
	
	freeReplyObject( redis_reply );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_redis_SREM_vargs;
int ZlangInvokeFunction_redis_SREM_vargs( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_redis	*redis_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	struct RedisConnectionContext		*redis_conn_ctx ;
	redisReply				*redis_reply = NULL ;
	int					nret = 0 ;
	
	SET_IN_FROM_STRING( "SREM" )
	APPEND_IN_FROM_INPUTPARAMETERSTACK_STRING
	
	FETCH_REDIS_CONNECTION( UnreferObject( rt , out1 ); )
	CALL_REDIS_COMMAND( REDIS_REPLY_INTEGER )
	GIVEBACK_REDIS_CONNECTION
	
	SET_OUT_FROM_REDISREPLY_INTEGER
	
	freeReplyObject( redis_reply );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_redis_SCARD_string;
int ZlangInvokeFunction_redis_SCARD_string( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_redis	*redis_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	struct RedisConnectionContext		*redis_conn_ctx ;
	redisReply				*redis_reply = NULL ;
	int					nret = 0 ;
	
	SET_IN_FROM_STRING( "SCARD" )
	APPEND_IN_FROM_INPUTPARAMETER_STRING( 1 )
	
	FETCH_REDIS_CONNECTION( UnreferObject( rt , out1 ); )
	CALL_REDIS_COMMAND( REDIS_REPLY_INTEGER )
	GIVEBACK_REDIS_CONNECTION
	
	SET_OUT_FROM_REDISREPLY_INTEGER
	
	freeReplyObject( redis_reply );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_redis_SISMEMBER_string_string;
int ZlangInvokeFunction_redis_SISMEMBER_string_string( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_redis	*redis_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	struct RedisConnectionContext		*redis_conn_ctx ;
	redisReply				*redis_reply = NULL ;
	int					nret = 0 ;
	
	SET_IN_FROM_STRING( "SISMEMBER" )
	APPEND_IN_FROM_INPUTPARAMETER_STRING( 1 )
	APPEND_IN_FROM_INPUTPARAMETER_STRING( 2 )
	
	FETCH_REDIS_CONNECTION( UnreferObject( rt , out1 ); )
	CALL_REDIS_COMMAND( REDIS_REPLY_INTEGER )
	GIVEBACK_REDIS_CONNECTION
	
	SET_OUT_FROM_REDISREPLY_INTEGER
	
	freeReplyObject( redis_reply );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_redis_SMEMBERS_string;
int ZlangInvokeFunction_redis_SMEMBERS_string( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_redis	*redis_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	struct RedisConnectionContext		*redis_conn_ctx ;
	redisReply				*redis_reply = NULL ;
	int					nret = 0 ;
	
	SET_IN_FROM_STRING( "SMEMBERS" )
	APPEND_IN_FROM_INPUTPARAMETER_STRING( 1 )
	
	FETCH_REDIS_CONNECTION( UnreferObject( rt , out1 ); )
	CALL_REDIS_COMMAND( REDIS_REPLY_ARRAY )
	GIVEBACK_REDIS_CONNECTION
	
	SET_OUT_FROM_REDISREPLY_ARRAY
	
	freeReplyObject( redis_reply );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_redis_SINTER_vargs;
int ZlangInvokeFunction_redis_SINTER_vargs( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_redis	*redis_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	struct RedisConnectionContext		*redis_conn_ctx ;
	redisReply				*redis_reply = NULL ;
	int					nret = 0 ;
	
	SET_IN_FROM_STRING( "SINTER" )
	APPEND_IN_FROM_INPUTPARAMETERSTACK_STRING
	
	FETCH_REDIS_CONNECTION( UnreferObject( rt , out1 ); )
	CALL_REDIS_COMMAND( REDIS_REPLY_ARRAY )
	GIVEBACK_REDIS_CONNECTION
	
	SET_OUT_FROM_REDISREPLY_ARRAY
	
	freeReplyObject( redis_reply );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_redis_SDIFF_vargs;
int ZlangInvokeFunction_redis_SDIFF_vargs( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_redis	*redis_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	struct RedisConnectionContext		*redis_conn_ctx ;
	redisReply				*redis_reply = NULL ;
	int					nret = 0 ;
	
	SET_IN_FROM_STRING( "SDIFF" )
	APPEND_IN_FROM_INPUTPARAMETERSTACK_STRING
	
	FETCH_REDIS_CONNECTION( UnreferObject( rt , out1 ); )
	CALL_REDIS_COMMAND( REDIS_REPLY_ARRAY )
	GIVEBACK_REDIS_CONNECTION
	
	SET_OUT_FROM_REDISREPLY_ARRAY
	
	freeReplyObject( redis_reply );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_redis_SUNION_vargs;
int ZlangInvokeFunction_redis_SUNION_vargs( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_redis	*redis_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	struct RedisConnectionContext		*redis_conn_ctx ;
	redisReply				*redis_reply = NULL ;
	int					nret = 0 ;
	
	SET_IN_FROM_STRING( "SUNION" )
	APPEND_IN_FROM_INPUTPARAMETERSTACK_STRING
	
	FETCH_REDIS_CONNECTION( UnreferObject( rt , out1 ); )
	CALL_REDIS_COMMAND( REDIS_REPLY_ARRAY )
	GIVEBACK_REDIS_CONNECTION
	
	SET_OUT_FROM_REDISREPLY_ARRAY
	
	freeReplyObject( redis_reply );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_redis_ZADD_vargs;
int ZlangInvokeFunction_redis_ZADD_vargs( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_redis	*redis_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	struct RedisConnectionContext		*redis_conn_ctx ;
	redisReply				*redis_reply = NULL ;
	int					nret = 0 ;
	
	SET_IN_FROM_STRING( "ZADD" )
	APPEND_IN_FROM_INPUTPARAMETERSTACK_STRING
	
	FETCH_REDIS_CONNECTION( UnreferObject( rt , out1 ); )
	CALL_REDIS_COMMAND( REDIS_REPLY_INTEGER )
	GIVEBACK_REDIS_CONNECTION
	
	SET_OUT_FROM_REDISREPLY_INTEGER
	
	freeReplyObject( redis_reply );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_redis_ZREM_vargs;
int ZlangInvokeFunction_redis_ZREM_vargs( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_redis	*redis_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	struct RedisConnectionContext		*redis_conn_ctx ;
	redisReply				*redis_reply = NULL ;
	int					nret = 0 ;
	
	SET_IN_FROM_STRING( "ZREM" )
	APPEND_IN_FROM_INPUTPARAMETERSTACK_STRING
	
	FETCH_REDIS_CONNECTION( UnreferObject( rt , out1 ); )
	CALL_REDIS_COMMAND( REDIS_REPLY_INTEGER )
	GIVEBACK_REDIS_CONNECTION
	
	SET_OUT_FROM_REDISREPLY_INTEGER
	
	freeReplyObject( redis_reply );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_redis_ZSCORE_string_string;
int ZlangInvokeFunction_redis_ZSCORE_string_string( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_redis	*redis_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	struct RedisConnectionContext		*redis_conn_ctx ;
	redisReply				*redis_reply = NULL ;
	int					nret = 0 ;
	
	SET_IN_FROM_STRING( "ZSCORE" )
	APPEND_IN_FROM_INPUTPARAMETER_STRING( 1 )
	APPEND_IN_FROM_INPUTPARAMETER_STRING( 2 )
	
	FETCH_REDIS_CONNECTION( UnreferObject( rt , out1 ); )
	CALL_REDIS_COMMAND( REDIS_REPLY_STRING )
	GIVEBACK_REDIS_CONNECTION
	
	SET_OUT_FROM_REDISREPLY_STRING
	
	freeReplyObject( redis_reply );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_redis_ZRANK_string_string;
int ZlangInvokeFunction_redis_ZRANK_string_string( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_redis	*redis_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	struct RedisConnectionContext		*redis_conn_ctx ;
	redisReply				*redis_reply = NULL ;
	int					nret = 0 ;
	
	SET_IN_FROM_STRING( "ZRANK" )
	APPEND_IN_FROM_INPUTPARAMETER_STRING( 1 )
	APPEND_IN_FROM_INPUTPARAMETER_STRING( 2 )
	
	FETCH_REDIS_CONNECTION( UnreferObject( rt , out1 ); )
	CALL_REDIS_COMMAND( REDIS_REPLY_INTEGER )
	GIVEBACK_REDIS_CONNECTION
	
	SET_OUT_FROM_REDISREPLY_INTEGER
	
	freeReplyObject( redis_reply );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_redis_ZCARD_string;
int ZlangInvokeFunction_redis_ZCARD_string( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_redis	*redis_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	struct RedisConnectionContext		*redis_conn_ctx ;
	redisReply				*redis_reply = NULL ;
	int					nret = 0 ;
	
	SET_IN_FROM_STRING( "ZCARD" )
	APPEND_IN_FROM_INPUTPARAMETER_STRING( 1 )
	
	FETCH_REDIS_CONNECTION( UnreferObject( rt , out1 ); )
	CALL_REDIS_COMMAND( REDIS_REPLY_INTEGER )
	GIVEBACK_REDIS_CONNECTION
	
	SET_OUT_FROM_REDISREPLY_INTEGER
	
	freeReplyObject( redis_reply );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_redis_ZCOUNT_string_string_string;
int ZlangInvokeFunction_redis_ZCOUNT_string_string_string( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_redis	*redis_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	struct RedisConnectionContext		*redis_conn_ctx ;
	redisReply				*redis_reply = NULL ;
	int					nret = 0 ;
	
	SET_IN_FROM_STRING( "ZCOUNT" )
	APPEND_IN_FROM_INPUTPARAMETER_STRING( 1 )
	APPEND_IN_FROM_INPUTPARAMETER_STRING( 2 )
	APPEND_IN_FROM_INPUTPARAMETER_STRING( 3 )
	
	FETCH_REDIS_CONNECTION( UnreferObject( rt , out1 ); )
	CALL_REDIS_COMMAND( REDIS_REPLY_INTEGER )
	GIVEBACK_REDIS_CONNECTION
	
	SET_OUT_FROM_REDISREPLY_INTEGER
	
	freeReplyObject( redis_reply );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_redis_ZRANGE_vargs;
int ZlangInvokeFunction_redis_ZRANGE_vargs( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_redis	*redis_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	struct RedisConnectionContext		*redis_conn_ctx ;
	redisReply				*redis_reply = NULL ;
	int					nret = 0 ;
	
	SET_IN_FROM_STRING( "ZRANGE" )
	APPEND_IN_FROM_INPUTPARAMETERSTACK_STRING
	
	FETCH_REDIS_CONNECTION( UnreferObject( rt , out1 ); )
	CALL_REDIS_COMMAND( REDIS_REPLY_ARRAY )
	GIVEBACK_REDIS_CONNECTION
	
	SET_OUT_FROM_REDISREPLY_ARRAY
	
	freeReplyObject( redis_reply );
	
	return 0;
}

ZlangCreateDirectPropertyFunction ZlangCreateDirectProperty_redis;
void *ZlangCreateDirectProperty_redis( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_redis	*redis_prop = NULL ;
	
	redis_prop = (struct ZlangDirectProperty_redis *)malloc( sizeof(struct ZlangDirectProperty_redis) ) ;
	if( redis_prop == NULL )
	{
		SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_ALLOC , "alloc memory for entity" )
		return NULL;
	}
	memset( redis_prop , 0x00 , sizeof(struct ZlangDirectProperty_redis) );
	
	redis_prop->command_buf = CloneStringObject( rt , NULL ) ;
	if( redis_prop->command_buf == NULL )
	{
		SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_ALLOC , "alloc memory for command buf" )
		ZLFREE( redis_prop );
		return NULL;
	}
	
	redis_prop->min_idle_connections_count = CONNECTIONSPOOL_MIN_IDLE_CONNECTIONS_COUNT_DEFAULT ;
	redis_prop->max_idle_connections_count = CONNECTIONSPOOL_MAX_IDLE_CONNECTIONS_COUNT_DEFAULT ;
	redis_prop->max_connections_count = CONNECTIONSPOOL_MAX_CONNECTIONS_COUNT_DEFAULT ;
	redis_prop->max_idle_timeval = CONNECTIONSPOOL_MAX_IDLE_TIMEVAL_DEFAULT ;
	redis_prop->watch_idle_timeval = CONNECTIONSPOOL_WATCH_IDLE_TIMEVAL_DEFAULT ;
	redis_prop->inspect_timeval = CONNECTIONSPOOL_INSPECT_TIMEVAL_DEFAULT ;
	
	return redis_prop;
}

ZlangDestroyDirectPropertyFunction ZlangDestroyDirectProperty_redis;
void ZlangDestroyDirectProperty_redis( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_redis	*redis_direct_prop = GetObjectDirectProperty(obj) ;
	
	if( redis_direct_prop->connections_pool )
	{
		StopCommonPool( redis_direct_prop->connections_pool );
		DestroyCommonPool( redis_direct_prop->connections_pool );
	}
	
	if( redis_direct_prop->ip )
		ZLFREE( redis_direct_prop->ip );
	
	DestroyObject( rt , redis_direct_prop->command_buf );
	
	ZLFREE( redis_direct_prop );
	
	return;
}

ZlangSummarizeDirectPropertySizeFunction ZlangSummarizeDirectPropertySize_redis;
void ZlangSummarizeDirectPropertySize_redis( struct ZlangRuntime *rt , struct ZlangObject *obj , size_t *summarized_obj_size , size_t *summarized_direct_prop_size )
{
	struct ZlangDirectProperty_redis	*redis_direct_prop = GetObjectDirectProperty(obj) ;
	
	if( redis_direct_prop->ip )
		SUMMARIZE_SIZE( summarized_direct_prop_size , strlen(redis_direct_prop->ip)+1 )
	
	if( redis_direct_prop->redis_conn_ctx )
	{
		if( redis_direct_prop->redis_conn_ctx->redis_ctx )
			SUMMARIZE_SIZE( summarized_direct_prop_size , sizeof(struct redisContext) )
		
		SUMMARIZE_SIZE( summarized_direct_prop_size , sizeof(struct RedisConnectionContext) )
	}
	
	SummarizeObjectSize( rt , redis_direct_prop->command_buf , summarized_obj_size , summarized_direct_prop_size );
	
	SUMMARIZE_SIZE( summarized_direct_prop_size , sizeof(struct ZlangDirectProperty_redis) )
	
	return;
}

static struct ZlangDirectFunctions direct_funcs_redis =
	{
		ZLANG_OBJECT_redis , /* char *ancestor_name */
		
		ZlangCreateDirectProperty_redis , /* ZlangCreateDirectPropertyFunction *create_entity_func */
		ZlangDestroyDirectProperty_redis , /* ZlangDestroyDirectPropertyFunction *destroy_entity_func */
		
		NULL , /* ZlangFromCharPtrFunction *from_char_ptr_func */
		NULL , /* ZlangToStringFunction *to_string_func */
		NULL , /* ZlangFromDataPtrFunction *from_data_ptr_func */
		NULL , /* ZlangGetDataPtrFunction *get_data_ptr_func */
		
		NULL , /* ZlangOperatorFunction *oper_PLUS_func */
		NULL , /* ZlangOperatorFunction *oper_MINUS_func */
		NULL , /* ZlangOperatorFunction *oper_MUL_func */
		NULL , /* ZlangOperatorFunction *oper_DIV_func */
		NULL , /* ZlangOperatorFunction *oper_MOD_func */
		
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_NEGATIVE_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_NOT_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_BIT_REVERSE_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_PLUS_PLUS_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_MINUS_MINUS_func */
		
		NULL , /* ZlangCompareFunction *comp_EGUAL_func */
		NULL , /* ZlangCompareFunction *comp_NOTEGUAL_func */
		NULL , /* ZlangCompareFunction *comp_LT_func */
		NULL , /* ZlangCompareFunction *comp_LE_func */
		NULL , /* ZlangCompareFunction *comp_GT_func */
		NULL , /* ZlangCompareFunction *comp_GE_func */
		
		NULL , /* ZlangLogicFunction *logic_AND_func */
		NULL , /* ZlangLogicFunction *logic_OR_func */
		
		NULL , /* ZlangLogicFunction *bit_AND_func */
		NULL , /* ZlangLogicFunction *bit_XOR_func */
		NULL , /* ZlangLogicFunction *bit_OR_func */
		NULL , /* ZlangLogicFunction *bit_MOVELEFT_func */
		NULL , /* ZlangLogicFunction *bit_MOVERIGHT_func */
		
		ZlangSummarizeDirectPropertySize_redis , /* ZlangSummarizeDirectPropertySizeFunction *summarize_direct_prop_size_func */
	} ;

ZlangImportObjectFunction ZlangImportObject_redis;
struct ZlangObject *ZlangImportObject_redis( struct ZlangRuntime *rt )
{
	struct ZlangObject	*obj = NULL ;
	struct ZlangObject	*prop = NULL ;
	struct ZlangFunction	*func = NULL ;
	int			nret = 0 ;
	
	nret = ImportObject( rt , & obj , ZLANG_OBJECT_redis , & direct_funcs_redis , sizeof(struct ZlangDirectFunctions) , NULL ) ;
	if( nret )
	{
		SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_LINK_FUNC_TO_ENTITY , "import object to global objects heap" )
		return NULL;
	}
	
	/* string redis.NX */
	prop = AddPropertyInObject( rt , obj , GetStringObjectInRuntimeObjectsHeap(rt) , "NX" ) ;
	if( prop == NULL )
		return NULL;
	CallRuntimeFunction_string_SetStringValue( rt , prop , "NX" , -1 );
	SetConstantObject( prop );
	
	/* string redis.XX */
	prop = AddPropertyInObject( rt , obj , GetStringObjectInRuntimeObjectsHeap(rt) , "XX" ) ;
	if( prop == NULL )
		return NULL;
	CallRuntimeFunction_string_SetStringValue( rt , prop , "XX" , -1 );
	SetConstantObject( prop );
	
	/* redis.EnableAssistantThread(bool) */
	func = AddFunctionAndParametersInObject( rt , obj , "EnableAssistantThread" , "EnableAssistantThread(bool)" , ZlangInvokeFunction_redis_EnableAssistantThread_bool , ZLANG_OBJECT_void , ZLANG_OBJECT_bool,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* redis.SetMinIdleConnectionsCount(int) */
	func = AddFunctionAndParametersInObject( rt , obj , "SetMinIdleConnectionsCount" , "SetMinIdleConnectionsCount(int)" , ZlangInvokeFunction_redis_SetMinIdleConnectionsCount_int , ZLANG_OBJECT_void , ZLANG_OBJECT_int,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* redis.SetMaxIdleConnectionsCount(int) */
	func = AddFunctionAndParametersInObject( rt , obj , "SetMaxIdleConnectionsCount" , "SetMaxIdleConnectionsCount(int)" , ZlangInvokeFunction_redis_SetMaxIdleConnectionsCount_int , ZLANG_OBJECT_void , ZLANG_OBJECT_int,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* redis.SetMaxConnectionsCount(int) */
	func = AddFunctionAndParametersInObject( rt , obj , "SetMaxConnectionsCount" , "SetMaxConnectionsCount(int)" , ZlangInvokeFunction_redis_SetMaxConnectionsCount_int , ZLANG_OBJECT_void , ZLANG_OBJECT_int,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* redis.SetMaxIdleTimeval(int) */
	func = AddFunctionAndParametersInObject( rt , obj , "SetMaxIdleTimeval" , "SetMaxIdleTimeval(int)" , ZlangInvokeFunction_redis_SetMaxIdleTimeval_int , ZLANG_OBJECT_void , ZLANG_OBJECT_int,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* redis.SetWatchIdleTimeval(int) */
	func = AddFunctionAndParametersInObject( rt , obj , "SetWatchIdleTimeval" , "SetWatchIdleTimeval(int)" , ZlangInvokeFunction_redis_SetWatchIdleTimeval_int , ZLANG_OBJECT_void , ZLANG_OBJECT_int,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* redis.SetInspectTimeval(int) */
	func = AddFunctionAndParametersInObject( rt , obj , "SetInspectTimeval" , "SetInspectTimeval(int)" , ZlangInvokeFunction_redis_SetInspectTimeval_int , ZLANG_OBJECT_void , ZLANG_OBJECT_int,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* redis.Connect(string,int) */
	func = AddFunctionAndParametersInObject( rt , obj , "Connect" , "Connect(string,int)" , ZlangInvokeFunction_redis_Connect_string_int , ZLANG_OBJECT_bool , ZLANG_OBJECT_string,NULL , ZLANG_OBJECT_int,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* redis.Disconnect() */
	func = AddFunctionAndParametersInObject( rt , obj , "Disconnect" , "Disconnect()" , ZlangInvokeFunction_redis_Disconnect , ZLANG_OBJECT_void , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* redis.DEL(...) */
	func = AddFunctionAndParametersInObject( rt , obj , "DEL" , "DEL(...)" , ZlangInvokeFunction_redis_DEL_vargs , ZLANG_OBJECT_int , ZLANG_OBJECT_vargs,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* redis.EXISTS(string) */
	func = AddFunctionAndParametersInObject( rt , obj , "EXISTS" , "EXISTS(string)" , ZlangInvokeFunction_redis_EXISTS_string , ZLANG_OBJECT_int , ZLANG_OBJECT_string,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* redis.KEYS(string) */
	func = AddFunctionAndParametersInObject( rt , obj , "KEYS" , "KEYS(string)" , ZlangInvokeFunction_redis_KEYS_string , ZLANG_OBJECT_array , ZLANG_OBJECT_string,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* redis.SET(string,string,int,long,string) */
	func = AddFunctionAndParametersInObject( rt , obj , "SET" , "SET(string,string,(null),(null),(null))" , ZlangInvokeFunction_redis_SET_string_string_int_long_string , ZLANG_OBJECT_bool , ZLANG_OBJECT_string,NULL , ZLANG_OBJECT_string,NULL , ZLANG_OBJECT_int,NULL , ZLANG_OBJECT_long,NULL , ZLANG_OBJECT_string,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	func = AddFunctionAndParametersInObject( rt , obj , "SET" , "SET(string,string,int,(null),(null))" , ZlangInvokeFunction_redis_SET_string_string_int_long_string , ZLANG_OBJECT_bool , ZLANG_OBJECT_string,NULL , ZLANG_OBJECT_string,NULL , ZLANG_OBJECT_int,NULL , ZLANG_OBJECT_long,NULL , ZLANG_OBJECT_string,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	func = AddFunctionAndParametersInObject( rt , obj , "SET" , "SET(string,string,(null),long,(null))" , ZlangInvokeFunction_redis_SET_string_string_int_long_string , ZLANG_OBJECT_bool , ZLANG_OBJECT_string,NULL , ZLANG_OBJECT_string,NULL , ZLANG_OBJECT_int,NULL , ZLANG_OBJECT_long,NULL , ZLANG_OBJECT_string,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	func = AddFunctionAndParametersInObject( rt , obj , "SET" , "SET(string,string,(null),(null),string)" , ZlangInvokeFunction_redis_SET_string_string_int_long_string , ZLANG_OBJECT_bool , ZLANG_OBJECT_string,NULL , ZLANG_OBJECT_string,NULL , ZLANG_OBJECT_int,NULL , ZLANG_OBJECT_long,NULL , ZLANG_OBJECT_string,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	func = AddFunctionAndParametersInObject( rt , obj , "SET" , "SET(string,string,int,(null),string)" , ZlangInvokeFunction_redis_SET_string_string_int_long_string , ZLANG_OBJECT_bool , ZLANG_OBJECT_string,NULL , ZLANG_OBJECT_string,NULL , ZLANG_OBJECT_int,NULL , ZLANG_OBJECT_long,NULL , ZLANG_OBJECT_string,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	func = AddFunctionAndParametersInObject( rt , obj , "SET" , "SET(string,string,(null),long,string)" , ZlangInvokeFunction_redis_SET_string_string_int_long_string , ZLANG_OBJECT_bool , ZLANG_OBJECT_string,NULL , ZLANG_OBJECT_string,NULL , ZLANG_OBJECT_int,NULL , ZLANG_OBJECT_long,NULL , ZLANG_OBJECT_string,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* redis.GET(string) */
	func = AddFunctionAndParametersInObject( rt , obj , "GET" , "GET(string)" , ZlangInvokeFunction_redis_GET_string , ZLANG_OBJECT_string , ZLANG_OBJECT_string,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* redis.MGET(string) */
	func = AddFunctionAndParametersInObject( rt , obj , "MGET" , "MGET(...)" , ZlangInvokeFunction_redis_MGET_vargs , ZLANG_OBJECT_array , ZLANG_OBJECT_vargs,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* redis.HSET(string,string,string) */
	func = AddFunctionAndParametersInObject( rt , obj , "HSET" , "HSET(string,string,string)" , ZlangInvokeFunction_redis_HSET_string_string_string , ZLANG_OBJECT_int , ZLANG_OBJECT_string,NULL , ZLANG_OBJECT_string,NULL , ZLANG_OBJECT_string,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* redis.HGET(string,string) */
	func = AddFunctionAndParametersInObject( rt , obj , "HGET" , "HGET(string,string)" , ZlangInvokeFunction_redis_HGET_string_string , ZLANG_OBJECT_string , ZLANG_OBJECT_string,NULL , ZLANG_OBJECT_string,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* redis.HMSET(...) */
	func = AddFunctionAndParametersInObject( rt , obj , "HMSET" , "HMSET(...)" , ZlangInvokeFunction_redis_HMSET_vargs , ZLANG_OBJECT_bool , ZLANG_OBJECT_vargs,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* redis.HMGET(...) */
	func = AddFunctionAndParametersInObject( rt , obj , "HMGET" , "HMGET(...)" , ZlangInvokeFunction_redis_HMGET_vargs , ZLANG_OBJECT_array , ZLANG_OBJECT_vargs,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* redis.HGETALL(string) */
	func = AddFunctionAndParametersInObject( rt , obj , "HGETALL" , "HGETALL(string)" , ZlangInvokeFunction_redis_HGETALL_string , ZLANG_OBJECT_array , ZLANG_OBJECT_string,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* redis.HKEYS(string) */
	func = AddFunctionAndParametersInObject( rt , obj , "HKEYS" , "HKEYS(string)" , ZlangInvokeFunction_redis_HKEYS_string , ZLANG_OBJECT_array , ZLANG_OBJECT_string,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* redis.HVALS(string) */
	func = AddFunctionAndParametersInObject( rt , obj , "HVALS" , "HVALS(string)" , ZlangInvokeFunction_redis_HVALS_string , ZLANG_OBJECT_array , ZLANG_OBJECT_string,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* redis.LPUSH(...) */
	func = AddFunctionAndParametersInObject( rt , obj , "LPUSH" , "LPUSH(...)" , ZlangInvokeFunction_redis_LPUSH_vargs , ZLANG_OBJECT_int , ZLANG_OBJECT_vargs,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* redis.LPOP(string) */
	func = AddFunctionAndParametersInObject( rt , obj , "LPOP" , "LPOP(string)" , ZlangInvokeFunction_redis_LPOP_string , ZLANG_OBJECT_string , ZLANG_OBJECT_string,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* redis.RPUSH(...) */
	func = AddFunctionAndParametersInObject( rt , obj , "RPUSH" , "RPUSH(...)" , ZlangInvokeFunction_redis_RPUSH_vargs , ZLANG_OBJECT_int , ZLANG_OBJECT_vargs,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* redis.RPOP(string) */
	func = AddFunctionAndParametersInObject( rt , obj , "RPOP" , "RPOP(string)" , ZlangInvokeFunction_redis_RPOP_string , ZLANG_OBJECT_string , ZLANG_OBJECT_string,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* redis.LLEN(string) */
	func = AddFunctionAndParametersInObject( rt , obj , "LLEN" , "LLEN(string)" , ZlangInvokeFunction_redis_LLEN_string , ZLANG_OBJECT_int , ZLANG_OBJECT_string,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* redis.LRANGE(string,int,int) */
	func = AddFunctionAndParametersInObject( rt , obj , "LRANGE" , "LRANGE(string,int,int)" , ZlangInvokeFunction_redis_LRANGE_string_int_int , ZLANG_OBJECT_array , ZLANG_OBJECT_string,NULL , ZLANG_OBJECT_int,NULL , ZLANG_OBJECT_int,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* redis.BLPOP(...) */
	func = AddFunctionAndParametersInObject( rt , obj , "BLPOP" , "BLPOP(...)" , ZlangInvokeFunction_redis_BLPOP_vargs , ZLANG_OBJECT_array , ZLANG_OBJECT_vargs,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* redis.BRPOP(...) */
	func = AddFunctionAndParametersInObject( rt , obj , "BRPOP" , "BRPOP(...)" , ZlangInvokeFunction_redis_BRPOP_vargs , ZLANG_OBJECT_array , ZLANG_OBJECT_vargs,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* redis.SADD(...) */
	func = AddFunctionAndParametersInObject( rt , obj , "SADD" , "SADD(...)" , ZlangInvokeFunction_redis_SADD_vargs , ZLANG_OBJECT_int , ZLANG_OBJECT_vargs,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* redis.SREM(...) */
	func = AddFunctionAndParametersInObject( rt , obj , "SREM" , "SREM(...)" , ZlangInvokeFunction_redis_SREM_vargs , ZLANG_OBJECT_int , ZLANG_OBJECT_vargs,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* redis.SCARD(string) */
	func = AddFunctionAndParametersInObject( rt , obj , "SCARD" , "SCARD(string)" , ZlangInvokeFunction_redis_SCARD_string , ZLANG_OBJECT_int , ZLANG_OBJECT_string,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* redis.SISMEMBER(string,string) */
	func = AddFunctionAndParametersInObject( rt , obj , "SISMEMBER" , "SISMEMBER(string,string)" , ZlangInvokeFunction_redis_SISMEMBER_string_string , ZLANG_OBJECT_int , ZLANG_OBJECT_string,NULL , ZLANG_OBJECT_string,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* redis.SMEMBERS(string) */
	func = AddFunctionAndParametersInObject( rt , obj , "SMEMBERS" , "SMEMBERS(string)" , ZlangInvokeFunction_redis_SMEMBERS_string , ZLANG_OBJECT_array , ZLANG_OBJECT_string,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* redis.SINTER(...) */
	func = AddFunctionAndParametersInObject( rt , obj , "SINTER" , "SINTER(...)" , ZlangInvokeFunction_redis_SINTER_vargs , ZLANG_OBJECT_array , ZLANG_OBJECT_vargs,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* redis.SDIFF(...) */
	func = AddFunctionAndParametersInObject( rt , obj , "SDIFF" , "SDIFF(...)" , ZlangInvokeFunction_redis_SDIFF_vargs , ZLANG_OBJECT_array , ZLANG_OBJECT_vargs,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* redis.SUNION(...) */
	func = AddFunctionAndParametersInObject( rt , obj , "SUNION" , "SUNION(...)" , ZlangInvokeFunction_redis_SUNION_vargs , ZLANG_OBJECT_array , ZLANG_OBJECT_vargs,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* redis.ZADD(...) */
	func = AddFunctionAndParametersInObject( rt , obj , "ZADD" , "ZADD(...)" , ZlangInvokeFunction_redis_ZADD_vargs , ZLANG_OBJECT_int , ZLANG_OBJECT_vargs,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* redis.ZREM(...) */
	func = AddFunctionAndParametersInObject( rt , obj , "ZREM" , "ZREM(...)" , ZlangInvokeFunction_redis_ZREM_vargs , ZLANG_OBJECT_int , ZLANG_OBJECT_vargs,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* redis.ZSCORE(string,string) */
	func = AddFunctionAndParametersInObject( rt , obj , "ZSCORE" , "ZSCORE(string,string)" , ZlangInvokeFunction_redis_ZSCORE_string_string , ZLANG_OBJECT_string , ZLANG_OBJECT_string,NULL , ZLANG_OBJECT_string,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* redis.ZRANK(string,string) */
	func = AddFunctionAndParametersInObject( rt , obj , "ZRANK" , "ZRANK(string,string)" , ZlangInvokeFunction_redis_ZRANK_string_string , ZLANG_OBJECT_int , ZLANG_OBJECT_string,NULL , ZLANG_OBJECT_string,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* redis.ZCARD(string) */
	func = AddFunctionAndParametersInObject( rt , obj , "ZCARD" , "ZCARD(string)" , ZlangInvokeFunction_redis_ZCARD_string , ZLANG_OBJECT_int , ZLANG_OBJECT_string,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* redis.ZCOUNT(string,string,string) */
	func = AddFunctionAndParametersInObject( rt , obj , "ZCOUNT" , "ZCOUNT(string,string,string)" , ZlangInvokeFunction_redis_ZCOUNT_string_string_string , ZLANG_OBJECT_int , ZLANG_OBJECT_string,NULL , ZLANG_OBJECT_string,NULL , ZLANG_OBJECT_string,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* redis.ZRANGE(...) */
	func = AddFunctionAndParametersInObject( rt , obj , "ZRANGE" , "ZRANGE(...)" , ZlangInvokeFunction_redis_ZRANGE_vargs , ZLANG_OBJECT_array , ZLANG_OBJECT_vargs,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	return obj ;
}

