/* Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef _H_LIBZLANG_REDIS_
#define _H_LIBZLANG_REDIS_

#include "zlang.h"

#include "commonpool.h"

#include "hiredis.h"

#define ZLANG_OBJECT_redis	"redis"

#define CONNECTIONSPOOL_MIN_IDLE_CONNECTIONS_COUNT_DEFAULT	1
#define CONNECTIONSPOOL_MAX_IDLE_CONNECTIONS_COUNT_DEFAULT	2
#define CONNECTIONSPOOL_MAX_CONNECTIONS_COUNT_DEFAULT		2
#define CONNECTIONSPOOL_MAX_IDLE_TIMEVAL_DEFAULT		5*60
#define CONNECTIONSPOOL_WATCH_IDLE_TIMEVAL_DEFAULT		60
#define CONNECTIONSPOOL_INSPECT_TIMEVAL_DEFAULT			1000*1000

#define REDIS_PING_STR		"PING"
#define REDIS_PONG_STR		"PONG"

#define EXCEPTION_MESSAGE_CONNECT_REDIS_SERVER_FAILED		"connect redis server failed"
#define EXCEPTION_MESSAGE_EXECUTE_REDIS_COMMAND_FAILED		"execute redis command failed"
#define EXCEPTION_MESSAGE_REDIS_RESPONSE_TYPE_INVALID		"redis response type invalid"
#define EXCEPTION_MESSAGE_REDIS_RESPONSE_PONG_INVALID		"redis response pong invalid"
#define EXCEPTION_MESSAGE_FETCH_CONNECTION_FAILED		"fetch connection invalid"
#define EXCEPTION_MESSAGE_REDIS_RESPONSE_TYPE_NOT_MATCHED	"redis response type not matched"

DLLEXPORT ZlangImportObjectsFunction ZlangImportObjects;
int ZlangImportObjects( struct ZlangRuntime *rt );

#endif

