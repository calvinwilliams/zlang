/* Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "zobjects_random.h"

struct ZlangDirectProperty_random
{
	char		dummy[0] ;
} ;

ZlangInvokeFunction ZlangInvokeFunction_random_Srand;
int ZlangInvokeFunction_random_Srand( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
#if defined(__linux__)
	struct timeval	now ;
	gettimeofday( & now , NULL );
	srand( (unsigned)(now.tv_sec*now.tv_usec) );
#elif defined(_WIN32)
	time_t		tt ;
	SYSTEMTIME	st ;
	GetLocalTime( & (st) );
	time( & (tt) );
	srand( (unsigned)(tt*st.wMilliseconds) );
#endif

	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_random_Srand_int;
int ZlangInvokeFunction_random_Srand_int( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangObject			*in1_obj = GetInputParameterInLocalObjectStack(rt,1) ;
	int32_t					seed ;
	
	CallRuntimeFunction_int_GetIntValue( rt , in1_obj , & seed );
	srand( (unsigned)seed );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_random_Rand;
int ZlangInvokeFunction_random_Rand( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangObject			*out1_obj = GetOutputParameterInLocalObjectStack(rt,1) ;
	int32_t					r ;
	
	r = rand() ;
	if( r < 0 )
		r = -r ;
	CallRuntimeFunction_int_SetIntValue( rt , out1_obj , r );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_random_Rand_int;
int ZlangInvokeFunction_random_Rand_int( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangObject			*in1_obj = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject			*out1_obj = GetOutputParameterInLocalObjectStack(rt,1) ;
	int32_t					m ;
	int32_t					r ;
	
	CallRuntimeFunction_int_GetIntValue( rt , in1_obj , & m );
	
	r = rand() ;
	if( r < 0 )
		r = -r ;
	r %= m ;
	CallRuntimeFunction_int_SetIntValue( rt , out1_obj , r );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_random_Rand_int_int;
int ZlangInvokeFunction_random_Rand_int_int( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangObject			*in1_obj = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject			*in2_obj = GetInputParameterInLocalObjectStack(rt,2) ;
	struct ZlangObject			*out1_obj = GetOutputParameterInLocalObjectStack(rt,1) ;
	int32_t					min , max ;
	int32_t					d ;
	int32_t					r ;
	
	CallRuntimeFunction_int_GetIntValue( rt , in1_obj , & min );
	CallRuntimeFunction_int_GetIntValue( rt , in2_obj , & max );
	
	r = rand() ;
	if( r < 0 )
		r = -r ;
	d = max - min + 1 ;
	if( d == 0 )
		return ThrowFatalException( rt , ZLANG_FATAL_INVOKE_METHOD_RETURN , EXCEPTION_MESSAGE_ALLOC_FAILED );
	r = r % d + min ;
	CallRuntimeFunction_int_SetIntValue( rt , out1_obj , r );
	
	return 0;
}

ZlangCreateDirectPropertyFunction ZlangCreateDirectProperty_random;
void *ZlangCreateDirectProperty_random( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_random	*random_prop = NULL ;
	
	random_prop = (struct ZlangDirectProperty_random *)ZLMALLOC( sizeof(struct ZlangDirectProperty_random) ) ;
	if( random_prop == NULL )
	{
		SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_ALLOC , "alloc memory for entity" )
		return NULL;
	}
	memset( random_prop , 0x00 , sizeof(struct ZlangDirectProperty_random) );
	
	return random_prop;
}

ZlangDestroyDirectPropertyFunction ZlangDestroyDirectProperty_random;
void ZlangDestroyDirectProperty_random( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_random	*random_direct_prop = GetObjectDirectProperty(obj) ;
	
	ZLFREE( random_direct_prop );
	
	return;
}

ZlangSummarizeDirectPropertySizeFunction ZlangSummarizeDirectPropertySize_random;
void ZlangSummarizeDirectPropertySize_random( struct ZlangRuntime *rt , struct ZlangObject *obj , size_t *summarized_obj_size , size_t *summarized_direct_prop_size )
{
	SUMMARIZE_SIZE( summarized_direct_prop_size , sizeof(struct ZlangDirectProperty_random) )
	return;
}

static struct ZlangDirectFunctions direct_funcs_random =
	{
		ZLANG_OBJECT_random , /* char *ancestor_name */
		
		ZlangCreateDirectProperty_random , /* ZlangCreateDirectPropertyFunction *create_entity_func */
		ZlangDestroyDirectProperty_random , /* ZlangDestroyDirectPropertyFunction *destroy_entity_func */
		
		NULL , /* ZlangFromCharPtrFunction *from_char_ptr_func */
		NULL , /* ZlangToStringFunction *to_string_func */
		NULL , /* ZlangFromDataPtrFunction *from_data_ptr_func */
		NULL , /* ZlangGetDataPtrFunction *get_data_ptr_func */
		
		NULL , /* ZlangOperatorFunction *oper_PLUS_func */
		NULL , /* ZlangOperatorFunction *oper_MINUS_func */
		NULL , /* ZlangOperatorFunction *oper_MUL_func */
		NULL , /* ZlangOperatorFunction *oper_DIV_func */
		NULL , /* ZlangOperatorFunction *oper_MOD_func */
		
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_NEGATIVE_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_NOT_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_BIT_REVERSE_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_PLUS_PLUS_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_MINUS_MINUS_func */
		
		NULL , /* ZlangCompareFunction *comp_EGUAL_func */
		NULL , /* ZlangCompareFunction *comp_NOTEGUAL_func */
		NULL , /* ZlangCompareFunction *comp_LT_func */
		NULL , /* ZlangCompareFunction *comp_LE_func */
		NULL , /* ZlangCompareFunction *comp_GT_func */
		NULL , /* ZlangCompareFunction *comp_GE_func */
		
		NULL , /* ZlangLogicFunction *logic_AND_func */
		NULL , /* ZlangLogicFunction *logic_OR_func */
		
		NULL , /* ZlangLogicFunction *bit_AND_func */
		NULL , /* ZlangLogicFunction *bit_XOR_func */
		NULL , /* ZlangLogicFunction *bit_OR_func */
		NULL , /* ZlangLogicFunction *bit_MOVELEFT_func */
		NULL , /* ZlangLogicFunction *bit_MOVERIGHT_func */
		
		ZlangSummarizeDirectPropertySize_random , /* ZlangSummarizeDirectPropertySizeFunction *summarize_direct_prop_size_func */
	} ;

ZlangImportObjectFunction ZlangImportObject_random;
struct ZlangObject *ZlangImportObject_random( struct ZlangRuntime *rt )
{
	struct ZlangObject	*obj = NULL ;
	struct ZlangFunction	*func = NULL ;
	int			nret = 0 ;
	
	nret = ImportObject( rt , & obj , ZLANG_OBJECT_random , & direct_funcs_random , sizeof(struct ZlangDirectFunctions) , NULL ) ;
	if( nret )
	{
		SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_LINK_FUNC_TO_ENTITY , "import object to global objects heap" )
		return NULL;
	}
	
	/* random.Srand() */
	func = AddFunctionAndParametersInObject( rt , obj , "Srand" , "Srand()" , ZlangInvokeFunction_random_Srand , ZLANG_OBJECT_void , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* random.Srand(int) */
	func = AddFunctionAndParametersInObject( rt , obj , "Srand" , "Srand(int)" , ZlangInvokeFunction_random_Srand_int , ZLANG_OBJECT_void , ZLANG_OBJECT_int,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* random.Rand() */
	func = AddFunctionAndParametersInObject( rt , obj , "Rand" , "Rand()" , ZlangInvokeFunction_random_Rand , ZLANG_OBJECT_int , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* random.Rand(int) */
	func = AddFunctionAndParametersInObject( rt , obj , "Rand" , "Rand(int)" , ZlangInvokeFunction_random_Rand_int , ZLANG_OBJECT_int , ZLANG_OBJECT_int,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* random.Rand(int,int) */
	func = AddFunctionAndParametersInObject( rt , obj , "Rand" , "Rand(int,int)" , ZlangInvokeFunction_random_Rand_int_int , ZLANG_OBJECT_int , ZLANG_OBJECT_int,NULL , ZLANG_OBJECT_int,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	return obj ;
}

