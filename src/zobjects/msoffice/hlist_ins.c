#include "hlist_tpl.h"

CREATE_HLIST_ARRAY( AllocNumberFormatHashMap , struct ZlangDirectProperty_msexcel , number_format_hashmap , number_format_hashmap_size )
DESTROY_HLIST_ARRAY( FreeNumberFormatHashMap , struct ZlangDirectProperty_msexcel , number_format_hashmap )
LINK_HLISTNODE_INT( LinkNumberFormatHashNode_BY_number_format_id , struct ZlangDirectProperty_msexcel , number_format_hashmap , struct MsExcelStyleNumberFormat , number_format_hashnode )
QUERY_HLISTNODE_INT( QueryNumberFormatHashNode_BY_number_format_id , struct ZlangDirectProperty_msexcel , number_format_hashmap , struct MsExcelStyleNumberFormat , number_format_hashnode )

CREATE_HLIST_ARRAY( AllocNumberFormatHashMap , struct ZlangDirectProperty_msexcel , xf_hashmap , xf_hashmap_size )
DESTROY_HLIST_ARRAY( FreeNumberFormatHashMap , struct ZlangDirectProperty_msexcel , xf_hashmap )
LINK_HLISTNODE_INT( LinkNumberFormatHashNode_BY_number_format_id , struct ZlangDirectProperty_msexcel , xf_hashmap , struct MsExcelStyleNumberFormat , xf_hashnode )
QUERY_HLISTNODE_INT( QueryNumberFormatHashNode_BY_number_format_id , struct ZlangDirectProperty_msexcel , xf_hashmap , struct MsExcelStyleNumberFormat , xf_hashnode )

