/* Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "zobjects_msoffice.h"

#include <stdio.h>  
 
int OpenAndReadAndCloseFileInZip( unzFile unzip_file , unz_file_info *file_info , char *filename , char **buf , uint64_t *buf_size , uint64_t *buf_len )
{
	int		read_len ;
	int		nret = 0 ;
	
	(*buf_len) = file_info->uncompressed_size ;
	if( (*buf) == NULL || (*buf_len) > (*buf_size)-1 )
	{
		char		*new_buf = NULL ;
		uint64_t	new_buf_size ;
		new_buf_size = (*buf_len) + 1 ;
		new_buf = realloc( (*buf) , new_buf_size ) ;
		if( new_buf == NULL )
		{
			return -1;
		}
		(*buf) = new_buf ;
		(*buf_size) = new_buf_size ;
	}
	memset( (*buf) , 0x00 , (*buf_size) );
	
	nret = unzOpenCurrentFile( unzip_file ) ;
	if( nret != UNZ_OK )
	{
		return -2;
	}
	
	read_len = unzReadCurrentFile( unzip_file , (*buf) , (*buf_size) ) ;
	if( read_len != (*buf_len) )
	{
		return -3;
	}
	(*buf)[(*buf_len)] = '\0' ;
	
	unzCloseCurrentFile( unzip_file );
	
	return 0;
}

#define IS_LEAPYEAR_IN_MSEXCEL(_year_)	((_year_)==1900||((_year_)%4==0&&(_year_)%100!=0)||(_year_)%400==0)

int MsExcel_DaysToDateSince1900_01_01( unsigned int days , unsigned int *year , unsigned int *month , unsigned int *day )
{  
	unsigned int	days_in_this_year ;
	unsigned int	days_in_this_month ;
	
	(*year) = 1900 ;
	(*month) = 1 ;
	(*day) = 0 ;
	if( days == 0 )
		return -1;
	
	while( days > 0 )
	{
		days_in_this_year = IS_LEAPYEAR_IN_MSEXCEL(*year) ? 366 : 365 ;
		if( days > days_in_this_year )
		{
			days -= days_in_this_year ;
			(*year)++;
		}
		else
		{
			break;
		}
	}
	
	while( days > 0 )
	{
		switch( *month )
		{
			case 1: case 3: case 5: case 7: case 8: case 10: case 12:
				days_in_this_month = 31 ;
				break;
			case 4: case 6: case 9: case 11:
				days_in_this_month = 30 ;
				break;
			case 2:
				days_in_this_month = IS_LEAPYEAR_IN_MSEXCEL(*year) ? 29 : 28 ;
				break;
			default:
				return -3;
		}
		
		if( days > days_in_this_month )
		{
			days -= days_in_this_month ;
			(*month)++;
			if( (*month) > 12 )
			{
				(*month) = 1;
				(*year)++;
			}
		}
		else
		{
			(*day) = days ;
			break;
		}
	}
	
	return 0;
}

ZlangImportObjectFunction ZlangImportObject_msexcel;
ZlangImportObjectFunction ZlangImportObject_msword;

#include "charset_GB18030.c"
#include "charset_UTF8.c"

int ZlangImportObjects( struct ZlangRuntime *rt )
{
	struct ZlangObject	*obj = NULL ;
	
	obj = ZlangImportObject_msexcel( rt ) ;
	if( obj == NULL )
		return 1;
	
	obj = ZlangImportObject_msword( rt ) ;
	if( obj == NULL )
		return 1;
	
	ImportCharsetAlias( rt , g_zlang_charset_aliases_GB18030 );
	ImportCharsetAlias( rt , g_zlang_charset_aliases_UTF8 );
	
	return 0;
}

