/* Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef _H_LIBZLANG_MSOFFICE_
#define _H_LIBZLANG_MSOFFICE_

#include "zlang.h"

#include "list.h"
#include "hlist_tpl.h"
#include "rbtree_tpl.h"

#include "minizip/zip.h"
#include "minizip/unzip.h"

#include "fasterxml.h"

int OpenAndReadAndCloseFileInZip( unzFile unzip_file , unz_file_info *file_info , char *filename , char **buf , uint64_t *buf_size , uint64_t *buf_len );

int MsExcel_DaysToDateSince1900_01_01( unsigned int days , unsigned int *year , unsigned int *month , unsigned int *day );

struct ZlangObject_msexcel;
struct ZlangObject_msword;

#define ZLANG_OBJECT_msexcel	"msexcel"
#define ZLANG_OBJECT_msword	"msword"

#define EXCEPTION_MESSAGE_OPEN_ZIP_FILE_FAILED		"open zip file failed"
#define EXCEPTION_MESSAGE_GET_ZIP_INFO_FAILED		"get zip info failed"
#define EXCEPTION_MESSAGE_GET_FILE_INFO_IN_ZIP_FAILED	"get file info in zip failed"
#define EXCEPTION_MESSAGE_OPEN_AND_READ_AND_CLOSE_FILE_IN_ZIP_FAILED	"get and read and close file in zip failed"
#define EXCEPTION_MESSAGE_PARSE_WORKBOOK_FAILED		"parse workbook failed"
#define EXCEPTION_MESSAGE_PARSE_SHARED_STRINGS_FAILED	"parse shared strings failed"
#define EXCEPTION_MESSAGE_PARSE_STYLES_FAILED		"parse styles failed"
#define EXCEPTION_MESSAGE_SHEET_FILE_NAME_INVALID	"sheet file name invalid"
#define EXCEPTION_MESSAGE_PARSE_SHEET_FAILED		"parse sheet invalid"
#define EXCEPTION_MESSAGE_QUERY_WORKBOOK_FAILED		"query workbook failed"
#define EXCEPTION_MESSAGE_SHEET_NOT_FOUND		"sheet not found"
#define EXCEPTION_MESSAGE_SHEET_NOT_SELECTED		"sheet not selected"
#define EXCEPTION_MESSAGE_ROW_NO_INVALID		"row no invalid"
#define EXCEPTION_MESSAGE_COLUMN_NO_INVALID		"column no invalid"
#define EXCEPTION_MESSAGE_MSEXCEL_DAYS_TO_DATE_SINCE_1900_01_01_FAILED		"msexcel days to date since 1900-01-01 failed"
#define EXCEPTION_MESSAGE_PARSE_DOCUMENT_FAILED		"parse document failed"
#define EXCEPTION_MESSAGE_LINE_NO_INVALID		"line no invalid"
#define EXCEPTION_MESSAGE_CONVERT_STRING_ENCODING_FAILED	"convert string encoding failed"
#define EXCEPTION_MESSAGE_STYLE_NOT_FOUND		"style not found"

DLLEXPORT ZlangImportObjectsFunction ZlangImportObjects;
int ZlangImportObjects( struct ZlangRuntime *rt );

#endif

