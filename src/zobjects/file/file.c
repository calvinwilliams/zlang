/* Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "zobjects_file.h"

ZlangInvokeFunction ZlangInvokeFunction_file_GetPathname;
int ZlangInvokeFunction_file_GetPathname( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_file	*file_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject		*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	
	CallRuntimeFunction_string_SetStringValue( rt , out1 , file_direct_prop->pathname , (int32_t)(file_direct_prop->pathname_len) );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_file_GetFilename;
int ZlangInvokeFunction_file_GetFilename( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_file	*file_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject		*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	
	CallRuntimeFunction_string_SetStringValue( rt , out1 , file_direct_prop->filename , (int32_t)(file_direct_prop->filename_len) );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_file_GetFiletype;
int ZlangInvokeFunction_file_GetFiletype( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_file	*file_direct_prop = GetObjectDirectProperty(obj) ;
	char				pathfilename[ PATH_MAX ] ;
	int32_t				file_type ;
	struct ZlangObject		*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	
	memset( pathfilename , 0x00 , sizeof(pathfilename) );
	snprintf( pathfilename , sizeof(pathfilename)-1 , "%.*s/%.*s" , (int)(file_direct_prop->pathname_len),file_direct_prop->pathname , (int)(file_direct_prop->filename_len),file_direct_prop->filename );
	file_type = GetDirectoryOrFileType( pathfilename ) ;
	if( file_type < 0 )
	{
		UnreferObject( rt , out1 );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_GET_DIRECTORY_OR_FILE_TYPE_FAILED );
	}
	else
	{
		CallRuntimeFunction_int_SetIntValue( rt , out1 , file_type );
	}
	
	return 0;
}

ZlangCreateDirectPropertyFunction ZlangCreateDirectProperty_file;
void *ZlangCreateDirectProperty_file( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_file	*file_prop = NULL ;
	
	file_prop = (struct ZlangDirectProperty_file *)ZLMALLOC( sizeof(struct ZlangDirectProperty_file) ) ;
	if( file_prop == NULL )
	{
		SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_ALLOC , "alloc memory for entity" )
		return NULL;
	}
	memset( file_prop , 0x00 , sizeof(struct ZlangDirectProperty_file) );
	
	return file_prop;
}

ZlangDestroyDirectPropertyFunction ZlangDestroyDirectProperty_file;
void ZlangDestroyDirectProperty_file( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_file	*file_direct_prop = GetObjectDirectProperty(obj) ;
	
	ZLFREE( file_direct_prop );
	
	return;
}

ZlangFromDataPtrFunction ZlangFromDataPtr_file;
int ZlangFromDataPtr_file( struct ZlangRuntime *rt , struct ZlangObject *obj , void *value_ptr , int32_t value_len )
{
	struct ZlangDirectProperty_file	*file_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangDirectProperty_file	*from_direct_prop = value_ptr ;
	
	if( value_len != sizeof(struct ZlangDirectProperty_file) )
	{
		SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_DIRECT_PROPERTY_SIZE_NOT_MATCHED , "direct property size not matched" )
		return ThrowFatalException( rt , ZLANG_ERROR_DIRECT_PROPERTY_SIZE_NOT_MATCHED , "direct property size not matched" );
	}
	
	memset( file_direct_prop , 0x00 , sizeof(struct ZlangDirectProperty_file) );
	memmove( file_direct_prop , from_direct_prop , sizeof(struct ZlangDirectProperty_file) );
	
	return 0;
}

ZlangGetDataPtrFunction ZlangGetDataPtr_file;
int ZlangGetDataPtr_file( struct ZlangRuntime *rt , struct ZlangObject *obj , void **value_ptr , int32_t *value_len )
{
	struct ZlangDirectProperty_file	*file_direct_prop = GetObjectDirectProperty(obj) ;
	
	if( value_ptr )
		(*value_ptr) = file_direct_prop ;
	if( value_len )
		(*value_len) = sizeof(struct ZlangDirectProperty_file) ;
	return 0;
}

ZlangSummarizeDirectPropertySizeFunction ZlangSummarizeDirectPropertySize_file;
void ZlangSummarizeDirectPropertySize_file( struct ZlangRuntime *rt , struct ZlangObject *obj , size_t *summarized_obj_size , size_t *summarized_direct_prop_size )
{
	SUMMARIZE_SIZE( summarized_direct_prop_size , sizeof(struct ZlangDirectProperty_file) )
	return;
}

static struct ZlangDirectFunctions direct_funcs_file =
	{
		ZLANG_OBJECT_file , /* char *ancestor_name */
		
		ZlangCreateDirectProperty_file , /* ZlangCreateDirectPropertyFunction *create_entity_func */
		ZlangDestroyDirectProperty_file , /* ZlangDestroyDirectPropertyFunction *destroy_entity_func */
		
		NULL , /* ZlangFromCharPtrFunction *from_char_ptr_func */
		NULL , /* ZlangToStringFunction *to_string_func */
		ZlangFromDataPtr_file , /* ZlangFromDataPtrFunction *from_data_ptr_func */
		ZlangGetDataPtr_file , /* ZlangGetDataPtrFunction *get_data_ptr_func */
		
		NULL , /* ZlangOperatorFunction *oper_PLUS_func */
		NULL , /* ZlangOperatorFunction *oper_MINUS_func */
		NULL , /* ZlangOperatorFunction *oper_MUL_func */
		NULL , /* ZlangOperatorFunction *oper_DIV_func */
		NULL , /* ZlangOperatorFunction *oper_MOD_func */
		
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_NEGATIVE_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_NOT_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_BIT_REVERSE_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_PLUS_PLUS_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_MINUS_MINUS_func */
		
		NULL , /* ZlangCompareFunction *comp_EGUAL_func */
		NULL , /* ZlangCompareFunction *comp_NOTEGUAL_func */
		NULL , /* ZlangCompareFunction *comp_LT_func */
		NULL , /* ZlangCompareFunction *comp_LE_func */
		NULL , /* ZlangCompareFunction *comp_GT_func */
		NULL , /* ZlangCompareFunction *comp_GE_func */
		
		NULL , /* ZlangLogicFunction *logic_AND_func */
		NULL , /* ZlangLogicFunction *logic_OR_func */
		
		NULL , /* ZlangLogicFunction *bit_AND_func */
		NULL , /* ZlangLogicFunction *bit_XOR_func */
		NULL , /* ZlangLogicFunction *bit_OR_func */
		NULL , /* ZlangLogicFunction *bit_MOVELEFT_func */
		NULL , /* ZlangLogicFunction *bit_MOVERIGHT_func */
		
		ZlangSummarizeDirectPropertySize_file , /* ZlangSummarizeDirectPropertySizeFunction *summarize_direct_prop_size_func */
	} ;

ZlangImportObjectFunction ZlangImportObject_file;
struct ZlangObject *ZlangImportObject_file( struct ZlangRuntime *rt )
{
	struct ZlangObject	*obj = NULL ;
	struct ZlangObject	*prop = NULL ;
	struct ZlangFunction	*func = NULL ;
	int			nret = 0 ;
	
	nret = ImportObject( rt , & obj , ZLANG_OBJECT_file , & direct_funcs_file , sizeof(struct ZlangDirectFunctions) , NULL ) ;
	if( nret )
	{
		SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_LINK_FUNC_TO_ENTITY , "import object to global objects heap" )
		return NULL;
	}
	
	/* int file.DIRECTORY_TYPE */
	prop = AddPropertyInObject( rt , obj , QueryObjectByObjectName(rt,"int") , "DIRECTORY_TYPE") ;
	if( prop == NULL )
		return NULL;
	CallRuntimeFunction_int_SetIntValue( rt , prop , FILE_TYPE_DIRECTORY );
	SetConstantObject( prop );
	
	/* int file.REGULAR_TYPE */
	prop = AddPropertyInObject( rt , obj , QueryObjectByObjectName(rt,"int") , "REGULAR_TYPE") ;
	if( prop == NULL )
		return NULL;
	CallRuntimeFunction_int_SetIntValue( rt , prop , FILE_TYPE_REGULAR );
	SetConstantObject( prop );
	
	/* int file.CHARACTER_TYPE */
	prop = AddPropertyInObject( rt , obj , QueryObjectByObjectName(rt,"int") , "CHARACTER_TYPE") ;
	if( prop == NULL )
		return NULL;
	CallRuntimeFunction_int_SetIntValue( rt , prop , FILE_TYPE_CHARACTER );
	SetConstantObject( prop );
	
	/* int file.BLOCK_TYPE */
	prop = AddPropertyInObject( rt , obj , QueryObjectByObjectName(rt,"int") , "BLOCK_TYPE") ;
	if( prop == NULL )
		return NULL;
	CallRuntimeFunction_int_SetIntValue( rt , prop , FILE_TYPE_BLOCK );
	SetConstantObject( prop );
	
	/* int file.FIFO_TYPE */
	prop = AddPropertyInObject( rt , obj , QueryObjectByObjectName(rt,"int") , "FIFO_TYPE") ;
	if( prop == NULL )
		return NULL;
	CallRuntimeFunction_int_SetIntValue( rt , prop , FILE_TYPE_FIFO );
	SetConstantObject( prop );
	
	/* int file.SOCKET_TYPE */
	prop = AddPropertyInObject( rt , obj , QueryObjectByObjectName(rt,"int") , "SOCKET_TYPE") ;
	if( prop == NULL )
		return NULL;
	CallRuntimeFunction_int_SetIntValue( rt , prop , FILE_TYPE_SOCKET );
	SetConstantObject( prop );
	
	/* int file.LINK_TYPE */
	prop = AddPropertyInObject( rt , obj , QueryObjectByObjectName(rt,"int") , "LINK_TYPE") ;
	if( prop == NULL )
		return NULL;
	CallRuntimeFunction_int_SetIntValue( rt , prop , FILE_TYPE_LINK );
	SetConstantObject( prop );
	
	/* int file.OTHER_TYPE */
	prop = AddPropertyInObject( rt , obj , QueryObjectByObjectName(rt,"int") , "OTHER_TYPE") ;
	if( prop == NULL )
		return NULL;
	CallRuntimeFunction_int_SetIntValue( rt , prop , FILE_TYPE_OTHER );
	SetConstantObject( prop );
	
	/* file.GetPathname() */
	func = AddFunctionAndParametersInObject( rt , obj , "GetPathname" , "GetPathname()" , ZlangInvokeFunction_file_GetPathname , ZLANG_OBJECT_string , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* file.GetFilename() */
	func = AddFunctionAndParametersInObject( rt , obj , "GetFilename" , "GetFilename()" , ZlangInvokeFunction_file_GetFilename , ZLANG_OBJECT_string , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* file.GetFiletype() */
	func = AddFunctionAndParametersInObject( rt , obj , "GetFiletype" , "GetFiletype()" , ZlangInvokeFunction_file_GetFiletype , ZLANG_OBJECT_int , NULL ) ;
	if( func == NULL )
		return NULL;
	
	return obj ;
}

