/* Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef _H_LIBZLANG_FILE_
#define _H_LIBZLANG_FILE_

#include "zlang.h"

#if ( defined __linux ) || ( defined __unix )
#include <dirent.h>
#include <sys/stat.h>
#elif ( defined _WIN32 )
#include "windows.h"
#endif

void *memrchr(const void *s, int c, size_t n);

struct ZlangDirectProperty_path
{
	char		pathname[ PATH_MAX ] ;
	size_t		pathname_len ;
} ;

struct ZlangDirectProperty_file
{
	char		pathname[ PATH_MAX ] ;
	size_t		pathname_len ;
	char		filename[ PATH_MAX ] ;
	size_t		filename_len ;
} ;

#define ZLANG_OBJECT_path	"path"
#define ZLANG_OBJECT_file	"file"

#define EXCEPTION_MESSAGE_GET_DIRECTORY_OR_FILE_TYPE_FAILED	"get directory or file type failed"
#define EXCEPTION_MESSAGE_PATH_NAME_TOO_LONG			"path name too long"
#define EXCEPTION_MESSAGE_NO_PARENT_PATH			"no parent path"
#define EXCEPTION_MESSAGE_NO_CHILD_PATH				"no child path"
#define EXCEPTION_MESSAGE_PATH_IS_NOT_A_DIRECTORY		"path is not a directory"
#define EXCEPTION_MESSAGE_PATH_INVALID				"path invalid"
#define EXCEPTION_MESSAGE_FILE_NAME_TOO_LONG			"file name too long"
#define EXCEPTION_MESSAGE_APPEND_FILE_ARRAY_FAILED		"append file array failed"

DLLEXPORT ZlangImportObjectsFunction ZlangImportObjects;
int ZlangImportObjects( struct ZlangRuntime *rt );

#endif

