/* Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "zobjects_trace.h"

struct ZlangDirectProperty_trace
{
	int		dummy ;
} ;

ZlangInvokeFunction ZlangInvokeFunction_trace_PrintRawMemory;
int ZlangInvokeFunction_trace_PrintRawMemory( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	DebugPrintRawMemory();
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_trace_PrintStackMemory;
int ZlangInvokeFunction_trace_PrintStackMemory( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	DebugPrintStackMemory( rt );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_trace_PrintHeapMemory;
int ZlangInvokeFunction_trace_PrintHeapMemory( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	DebugPrintHeapMemory( rt );
	
	return 0;
}

ZlangCreateDirectPropertyFunction ZlangCreateDirectProperty_trace;
void *ZlangCreateDirectProperty_trace( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_trace	*trace_prop = NULL ;
	
	trace_prop = (struct ZlangDirectProperty_trace *)ZLMALLOC( sizeof(struct ZlangDirectProperty_trace) ) ;
	if( trace_prop == NULL )
	{
		SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_ALLOC , "alloc memory for entity" )
		return NULL;
	}
	memset( trace_prop , 0x00 , sizeof(struct ZlangDirectProperty_trace) );
	
	return trace_prop;
}

ZlangDestroyDirectPropertyFunction ZlangDestroyDirectProperty_trace;
void ZlangDestroyDirectProperty_trace( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_trace	*trace_direct_prop = GetObjectDirectProperty(obj) ;
	
	ZLFREE( trace_direct_prop );
	
	return;
}

ZlangSummarizeDirectPropertySizeFunction ZlangSummarizeDirectPropertySize_trace;
void ZlangSummarizeDirectPropertySize_trace( struct ZlangRuntime *rt , struct ZlangObject *obj , size_t *summarized_obj_size , size_t *summarized_direct_prop_size )
{
	SUMMARIZE_SIZE( summarized_direct_prop_size , sizeof(struct ZlangDirectProperty_trace) )
	return;
}

static struct ZlangDirectFunctions direct_funcs_trace =
	{
		ZLANG_OBJECT_trace , /* char *ancestor_name */
		
		ZlangCreateDirectProperty_trace , /* ZlangCreateDirectPropertyFunction *create_entity_func */
		ZlangDestroyDirectProperty_trace , /* ZlangDestroyDirectPropertyFunction *destroy_entity_func */
		
		NULL , /* ZlangFromCharPtrFunction *from_char_ptr_func */
		NULL , /* ZlangToStringFunction *to_string_func */
		NULL , /* ZlangFromDataPtrFunction *from_data_ptr_func */
		NULL , /* ZlangGetDataPtrFunction *get_data_ptr_func */
		
		NULL , /* ZlangOperatorFunction *oper_PLUS_func */
		NULL , /* ZlangOperatorFunction *oper_MINUS_func */
		NULL , /* ZlangOperatorFunction *oper_MUL_func */
		NULL , /* ZlangOperatorFunction *oper_DIV_func */
		NULL , /* ZlangOperatorFunction *oper_MOD_func */
		
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_NEGATIVE_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_NOT_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_BIT_REVERSE_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_PLUS_PLUS_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_MINUS_MINUS_func */
		
		NULL , /* ZlangCompareFunction *comp_EGUAL_func */
		NULL , /* ZlangCompareFunction *comp_NOTEGUAL_func */
		NULL , /* ZlangCompareFunction *comp_LT_func */
		NULL , /* ZlangCompareFunction *comp_LE_func */
		NULL , /* ZlangCompareFunction *comp_GT_func */
		NULL , /* ZlangCompareFunction *comp_GE_func */
		
		NULL , /* ZlangLogicFunction *logic_AND_func */
		NULL , /* ZlangLogicFunction *logic_OR_func */
		
		NULL , /* ZlangLogicFunction *bit_AND_func */
		NULL , /* ZlangLogicFunction *bit_XOR_func */
		NULL , /* ZlangLogicFunction *bit_OR_func */
		NULL , /* ZlangLogicFunction *bit_MOVELEFT_func */
		NULL , /* ZlangLogicFunction *bit_MOVERIGHT_func */
		
		ZlangSummarizeDirectPropertySize_trace , /* ZlangSummarizeDirectPropertySizeFunction *summarize_direct_prop_size_func */
	} ;

ZlangImportObjectFunction ZlangImportObject_trace;
struct ZlangObject *ZlangImportObject_trace( struct ZlangRuntime *rt )
{
	struct ZlangObject	*obj = NULL ;
	struct ZlangFunction	*func = NULL ;
	int			nret = 0 ;
	
	nret = ImportObject( rt , & obj , ZLANG_OBJECT_trace , & direct_funcs_trace , sizeof(struct ZlangDirectFunctions) , NULL ) ;
	if( nret )
	{
		SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_LINK_FUNC_TO_ENTITY , "import object to global objects heap" )
		return NULL;
	}
	
	/* trace.PrintRawMemory() */
	func = AddFunctionAndParametersInObject( rt , obj , "PrintRawMemory" , "PrintRawMemory()" , ZlangInvokeFunction_trace_PrintRawMemory , ZLANG_OBJECT_void , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* trace.PrintStackMemory() */
	func = AddFunctionAndParametersInObject( rt , obj , "PrintStackMemory" , "PrintStackMemory()" , ZlangInvokeFunction_trace_PrintStackMemory , ZLANG_OBJECT_void , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* trace.PrintHeapMemory() */
	func = AddFunctionAndParametersInObject( rt , obj , "PrintHeapMemory" , "PrintHeapMemory()" , ZlangInvokeFunction_trace_PrintHeapMemory , ZLANG_OBJECT_void , NULL ) ;
	if( func == NULL )
		return NULL;
	
	return obj ;
}

