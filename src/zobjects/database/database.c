/* Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "zobjects_database.h"

ZlangInvokeFunction ZlangInvokeFunction_database_EnableAssistantThread_bool;
int ZlangInvokeFunction_database_EnableAssistantThread_bool( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_database	*database_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*in1_obj = GetInputParameterInLocalObjectStack(rt,1) ;
	
	CallRuntimeFunction_bool_GetBoolValue( rt , in1_obj , & (database_direct_prop->assistant_thread_enable) );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_database_SetMinIdleConnectionsCount_int;
int ZlangInvokeFunction_database_SetMinIdleConnectionsCount_int( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_database	*database_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*in1_obj = GetInputParameterInLocalObjectStack(rt,1) ;
	
	CallRuntimeFunction_int_GetIntValue( rt , in1_obj , & (database_direct_prop->min_idle_connections_count) );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_database_SetMaxIdleConnectionsCount_int;
int ZlangInvokeFunction_database_SetMaxIdleConnectionsCount_int( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_database	*database_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*in1_obj = GetInputParameterInLocalObjectStack(rt,1) ;
	
	CallRuntimeFunction_int_GetIntValue( rt , in1_obj , & (database_direct_prop->max_idle_connections_count) );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_database_SetMaxConnectionsCount_int;
int ZlangInvokeFunction_database_SetMaxConnectionsCount_int( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_database	*database_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*in1_obj = GetInputParameterInLocalObjectStack(rt,1) ;
	
	CallRuntimeFunction_int_GetIntValue( rt , in1_obj , & (database_direct_prop->max_connections_count) );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_database_SetMaxIdleTimeval_int;
int ZlangInvokeFunction_database_SetMaxIdleTimeval_int( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_database	*database_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*in1_obj = GetInputParameterInLocalObjectStack(rt,1) ;
	
	CallRuntimeFunction_int_GetIntValue( rt , in1_obj , & (database_direct_prop->max_idle_timeval) );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_database_SetWatchIdleTimeval_int;
int ZlangInvokeFunction_database_SetWatchIdleTimeval_int( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_database	*database_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*in1_obj = GetInputParameterInLocalObjectStack(rt,1) ;
	
	CallRuntimeFunction_int_GetIntValue( rt , in1_obj , & (database_direct_prop->watch_idle_timeval) );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_database_SetInspectTimeval_int;
int ZlangInvokeFunction_database_SetInspectTimeval_int( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_database	*database_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*in1_obj = GetInputParameterInLocalObjectStack(rt,1) ;
	
	CallRuntimeFunction_int_GetIntValue( rt , in1_obj , & (database_direct_prop->inspect_timeval) );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_database_Connect_string_string_int_string_string_string;
int ZlangInvokeFunction_database_Connect_string_string_int_string_string_string( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_database	*database_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*in1_obj = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject			*in2_obj = GetInputParameterInLocalObjectStack(rt,2) ;
	struct ZlangObject			*in3_obj = GetInputParameterInLocalObjectStack(rt,3) ;
	struct ZlangObject			*in4_obj = GetInputParameterInLocalObjectStack(rt,4) ;
	struct ZlangObject			*in5_obj = GetInputParameterInLocalObjectStack(rt,5) ;
	struct ZlangObject			*in6_obj = GetInputParameterInLocalObjectStack(rt,6) ;
	struct ZlangObject			*out1_obj = GetOutputParameterInLocalObjectStack(rt,1) ;
	char					*db_type_name = NULL ;
	char					*db_host = NULL ;
	int32_t					db_port = 0 ;
	char					*db_user = NULL ;
	char					*db_pass = NULL ;
	char					*db_name = NULL ;
	struct ZlangDirectProperty_dbsession	*dbsession_direct_prop = NULL ;
	int					nret = 0 ;
	
	CallRuntimeFunction_string_GetStringValue( rt , in1_obj , & db_type_name , NULL );
	CallRuntimeFunction_string_GetStringValue( rt , in2_obj , & db_host , NULL );
	CallRuntimeFunction_int_GetIntValue( rt , in3_obj , & db_port );
	CallRuntimeFunction_string_GetStringValue( rt , in4_obj , & db_user , NULL );
	CallRuntimeFunction_string_GetStringValue( rt , in5_obj , & db_pass , NULL );
	CallRuntimeFunction_string_GetStringValue( rt , in6_obj , & db_name , NULL );
	
	/*
	nret = InitObject( rt , out1_obj , NULL , & direct_funcs_dbsession , 0 ) ;
	if( nret )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "InitObject failed[%d]" , nret )
		return nret;
	}
	*/
	
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "DBCConnectToDatabase ... db_type_name[%s] db_host[%s] db_port[%d] db_user[%s] db_pass[%s] db_name[%s]" , db_type_name , db_host , db_port , db_user , db_pass , db_name )
	dbsession_direct_prop = GetObjectDirectProperty(out1_obj) ;
	dbsession_direct_prop->db_driver = DBCGetDatabaseDriver(db_type_name) ;
	if( dbsession_direct_prop->db_driver == NULL )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "DBCGetDatabaseDriver[%s] failed[%d]" , db_type_name , DBCGetLastErrno() )
		UnreferObject( rt , out1_obj );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_GET_DATABASE_DRIVER );
	}
	
	dbsession_direct_prop->rt = rt ;
	dbsession_direct_prop->db_host = ZLSTRDUP( db_host ) ;
	dbsession_direct_prop->db_port = db_port ;
	dbsession_direct_prop->db_user = ZLSTRDUP( db_user ) ;
	dbsession_direct_prop->db_pass = ZLSTRDUP( db_pass ) ;
	dbsession_direct_prop->db_name = ZLSTRDUP( db_name ) ;
	dbsession_direct_prop->assistant_thread_enable = database_direct_prop->assistant_thread_enable ;
	dbsession_direct_prop->min_idle_connections_count = database_direct_prop->min_idle_connections_count ;
	dbsession_direct_prop->max_idle_connections_count = database_direct_prop->max_idle_connections_count ;
	dbsession_direct_prop->max_connections_count = database_direct_prop->max_connections_count ;
	dbsession_direct_prop->max_idle_timeval = database_direct_prop->max_idle_timeval ;
	dbsession_direct_prop->watch_idle_timeval = database_direct_prop->watch_idle_timeval ;
	dbsession_direct_prop->inspect_timeval = database_direct_prop->inspect_timeval ;
	nret = CreateAndStartCommonPool( dbsession_direct_prop ) ;
	if( nret )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "CreateAndStartCommonPool failed[%d]" , nret )
		UnreferObject( rt , out1_obj );
		DBCSetLastErrno( -1 );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_CREATE_AND_START_CONNECTION_POOL_FAILED );
	}
	else
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "CreateAndStartCommonPool ok" )
	}
	
	database_direct_prop->last_session_direct_prop = dbsession_direct_prop ;
	dbsession_direct_prop->ref_database_direct_prop = database_direct_prop ;
	dbsession_direct_prop = GetObjectDirectProperty(out1_obj) ;
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_database_DisconnectLastSession;
int ZlangInvokeFunction_database_DisconnectLastSession( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_database	*database_direct_prop = GetObjectDirectProperty(obj) ;
	int					nret = 0 ;
	
	if( database_direct_prop->last_session_direct_prop )
	{
		nret = dbsession_Disconnect( rt , database_direct_prop->last_session_direct_prop );
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "ZlangInvokeFunction_dbsession_Disconnect return[%d]" , nret )
		if( nret )
			return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_RECONNECT_LAST_SESSION_FAILED );
	}
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_database_ReconnectLastSession;
int ZlangInvokeFunction_database_ReconnectLastSession( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_database	*database_direct_prop = GetObjectDirectProperty(obj) ;
	int					nret = 0 ;
	
	if( database_direct_prop->last_session_direct_prop )
	{
		nret = dbsession_Reconnect( rt , database_direct_prop->last_session_direct_prop );
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "ZlangInvokeFunction_dbsession_Reconnect return[%d]" , nret )
		if( nret )
			return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_RECONNECT_LAST_SESSION_FAILED );
	}
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_database_GetLastErrno;
int ZlangInvokeFunction_database_GetLastErrno( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangObject			*out1_obj = GetOutputParameterInLocalObjectStack(rt,1) ;
	
	CallRuntimeFunction_int_SetIntValue( rt , out1_obj , DBCGetLastErrno() );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_database_GetLastNativeErrno;
int ZlangInvokeFunction_database_GetLastNativeErrno( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangObject			*out1_obj = GetOutputParameterInLocalObjectStack(rt,1) ;
	
	CallRuntimeFunction_int_SetIntValue( rt , out1_obj , DBCGetLastNativeErrno() );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_database_GetLastNativeError;
int ZlangInvokeFunction_database_GetLastNativeError( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangObject			*out1_obj = GetOutputParameterInLocalObjectStack(rt,1) ;
	
	CallRuntimeFunction_string_SetStringValue( rt , out1_obj , DBCGetLastNativeError() , -1 );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_database_GetLastSqlState;
int ZlangInvokeFunction_database_GetLastSqlState( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangObject			*out1_obj = GetOutputParameterInLocalObjectStack(rt,1) ;
	
	CallRuntimeFunction_string_SetStringValue( rt , out1_obj , DBCGetLastSqlState() , -1 );
	
	return 0;
}

ZlangCreateDirectPropertyFunction ZlangCreateDirectProperty_database;
void *ZlangCreateDirectProperty_database( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_database	*database_prop = NULL ;
	
	database_prop = (struct ZlangDirectProperty_database *)ZLMALLOC( sizeof(struct ZlangDirectProperty_database) ) ;
	if( database_prop == NULL )
	{
		SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_ALLOC , "alloc memory for entity" )
		return NULL;
	}
	memset( database_prop , 0x00 , sizeof(struct ZlangDirectProperty_database) );
	
	database_prop->min_idle_connections_count = CONNECTIONSPOOL_MIN_IDLE_CONNECTIONS_COUNT_DEFAULT ;
	database_prop->max_idle_connections_count = CONNECTIONSPOOL_MAX_IDLE_CONNECTIONS_COUNT_DEFAULT ;
	database_prop->max_connections_count = CONNECTIONSPOOL_MAX_CONNECTIONS_COUNT_DEFAULT ;
	database_prop->max_idle_timeval = CONNECTIONSPOOL_MAX_IDLE_TIMEVAL_DEFAULT ;
	database_prop->watch_idle_timeval = CONNECTIONSPOOL_WATCH_IDLE_TIMEVAL_DEFAULT ;
	database_prop->inspect_timeval = CONNECTIONSPOOL_INSPECT_TIMEVAL_DEFAULT ;
	
	return database_prop;
}

ZlangDestroyDirectPropertyFunction ZlangDestroyDirectProperty_database;
void ZlangDestroyDirectProperty_database( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_database	*database_direct_prop = GetObjectDirectProperty(obj) ;
	
	if( STRCMP( GetObjectName(obj) , == , ZLANG_OBJECT_database ) )
	{
		DBCUnloadAllDatabaseDrivers();
	}
	
	ZLFREE( database_direct_prop );
	
	return;
}

ZlangSummarizeDirectPropertySizeFunction ZlangSummarizeDirectPropertySize_database;
void ZlangSummarizeDirectPropertySize_database( struct ZlangRuntime *rt , struct ZlangObject *obj , size_t *summarized_obj_size , size_t *summarized_direct_prop_size )
{
	SUMMARIZE_SIZE( summarized_direct_prop_size , sizeof(struct ZlangDirectProperty_database) )
	return;
}

static struct ZlangDirectFunctions direct_funcs_database =
	{
		ZLANG_OBJECT_database , /* char *ancestor_name */
		
		ZlangCreateDirectProperty_database , /* ZlangCreateDirectPropertyFunction *create_entity_func */
		ZlangDestroyDirectProperty_database , /* ZlangDestroyDirectPropertyFunction *destroy_entity_func */
		
		NULL , /* ZlangFromCharPtrFunction *from_char_ptr_func */
		NULL , /* ZlangToStringFunction *to_string_func */
		NULL , /* ZlangFromDataPtrFunction *from_data_ptr_func */
		NULL , /* ZlangGetDataPtrFunction *get_data_ptr_func */
		
		NULL , /* ZlangOperatorFunction *oper_PLUS_func */
		NULL , /* ZlangOperatorFunction *oper_MINUS_func */
		NULL , /* ZlangOperatorFunction *oper_MUL_func */
		NULL , /* ZlangOperatorFunction *oper_DIV_func */
		NULL , /* ZlangOperatorFunction *oper_MOD_func */
		
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_NEGATIVE_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_NOT_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_BIT_REVERSE_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_PLUS_PLUS_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_MINUS_MINUS_func */
		
		NULL , /* ZlangCompareFunction *comp_EGUAL_func */
		NULL , /* ZlangCompareFunction *comp_NOTEGUAL_func */
		NULL , /* ZlangCompareFunction *comp_LT_func */
		NULL , /* ZlangCompareFunction *comp_LE_func */
		NULL , /* ZlangCompareFunction *comp_GT_func */
		NULL , /* ZlangCompareFunction *comp_GE_func */
		
		NULL , /* ZlangLogicFunction *logic_AND_func */
		NULL , /* ZlangLogicFunction *logic_OR_func */
		
		NULL , /* ZlangLogicFunction *bit_AND_func */
		NULL , /* ZlangLogicFunction *bit_XOR_func */
		NULL , /* ZlangLogicFunction *bit_OR_func */
		NULL , /* ZlangLogicFunction *bit_MOVELEFT_func */
		NULL , /* ZlangLogicFunction *bit_MOVERIGHT_func */
		
		ZlangSummarizeDirectPropertySize_database , /* ZlangSummarizeDirectPropertySizeFunction *summarize_direct_prop_size_func */
	} ;

ZlangImportObjectFunction ZlangImportObject_database;
struct ZlangObject *ZlangImportObject_database( struct ZlangRuntime *rt )
{
	struct ZlangObject	*obj = NULL ;
	struct ZlangFunction	*func = NULL ;
	int			nret = 0 ;
	
	nret = ImportObject( rt , & obj , ZLANG_OBJECT_database , & direct_funcs_database , sizeof(struct ZlangDirectFunctions) , NULL ) ;
	if( nret )
	{
		SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_LINK_FUNC_TO_ENTITY , "import object to global objects heap" )
		return NULL;
	}
	
	/* database.EnableAssistantThread(bool) */
	func = AddFunctionAndParametersInObject( rt , obj , "EnableAssistantThread" , "EnableAssistantThread(bool)" , ZlangInvokeFunction_database_EnableAssistantThread_bool , ZLANG_OBJECT_void , ZLANG_OBJECT_bool,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* database.SetMinIdleConnectionsCount(int) */
	func = AddFunctionAndParametersInObject( rt , obj , "SetMinIdleConnectionsCount" , "SetMinIdleConnectionsCount(int)" , ZlangInvokeFunction_database_SetMinIdleConnectionsCount_int , ZLANG_OBJECT_void , ZLANG_OBJECT_int,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* database.SetMaxIdleConnectionsCount(int) */
	func = AddFunctionAndParametersInObject( rt , obj , "SetMaxIdleConnectionsCount" , "SetMaxIdleConnectionsCount(int)" , ZlangInvokeFunction_database_SetMaxIdleConnectionsCount_int , ZLANG_OBJECT_void , ZLANG_OBJECT_int,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* database.SetMaxConnectionsCount(int) */
	func = AddFunctionAndParametersInObject( rt , obj , "SetMaxConnectionsCount" , "SetMaxConnectionsCount(int)" , ZlangInvokeFunction_database_SetMaxConnectionsCount_int , ZLANG_OBJECT_void , ZLANG_OBJECT_int,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* database.SetMaxIdleTimeval(int) */
	func = AddFunctionAndParametersInObject( rt , obj , "SetMaxIdleTimeval" , "SetMaxIdleTimeval(int)" , ZlangInvokeFunction_database_SetMaxIdleTimeval_int , ZLANG_OBJECT_void , ZLANG_OBJECT_int,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* database.SetWatchIdleTimeval(int) */
	func = AddFunctionAndParametersInObject( rt , obj , "SetWatchIdleTimeval" , "SetWatchIdleTimeval(int)" , ZlangInvokeFunction_database_SetWatchIdleTimeval_int , ZLANG_OBJECT_void , ZLANG_OBJECT_int,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* database.SetInspectTimeval(int) */
	func = AddFunctionAndParametersInObject( rt , obj , "SetInspectTimeval" , "SetInspectTimeval(int)" , ZlangInvokeFunction_database_SetInspectTimeval_int , ZLANG_OBJECT_void , ZLANG_OBJECT_int,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* database.Connect(string,string,int,string,string,string) */
	func = AddFunctionAndParametersInObject( rt , obj , "Connect" , "Connect(string,string,int,string,string,string)" , ZlangInvokeFunction_database_Connect_string_string_int_string_string_string
		, ZLANG_OBJECT_dbsession
		, ZLANG_OBJECT_string,NULL
		, ZLANG_OBJECT_string,NULL
		, ZLANG_OBJECT_int,NULL
		, ZLANG_OBJECT_string,NULL
		, ZLANG_OBJECT_string,NULL
		, ZLANG_OBJECT_string,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* database.DisconnectLastSession() */
	func = AddFunctionAndParametersInObject( rt , obj , "DisconnectLastSession" , "DisconnectLastSession()" , ZlangInvokeFunction_database_DisconnectLastSession , ZLANG_OBJECT_void , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* database.ReconnectLastSession() */
	func = AddFunctionAndParametersInObject( rt , obj , "ReconnectLastSession" , "ReconnectLastSession()" , ZlangInvokeFunction_database_ReconnectLastSession , ZLANG_OBJECT_void , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* database.GetLastErrno() */
	func = AddFunctionAndParametersInObject( rt , obj , "GetLastErrno" , "GetLastErrno()" , ZlangInvokeFunction_database_GetLastErrno , ZLANG_OBJECT_int , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* database.GetLastNativeErrno() */
	func = AddFunctionAndParametersInObject( rt , obj , "GetLastNativeErrno" , "GetLastNativeErrno()" , ZlangInvokeFunction_database_GetLastNativeErrno , ZLANG_OBJECT_int , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* database.GetLastNativeError() */
	func = AddFunctionAndParametersInObject( rt , obj , "GetLastNativeError" , "GetLastNativeError()" , ZlangInvokeFunction_database_GetLastNativeError , ZLANG_OBJECT_string , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* database.GetLastSqlState() */
	func = AddFunctionAndParametersInObject( rt , obj , "GetLastSqlState" , "GetLastSqlState()" , ZlangInvokeFunction_database_GetLastSqlState , ZLANG_OBJECT_string , NULL ) ;
	if( func == NULL )
		return NULL;
	
	return obj ;
}

