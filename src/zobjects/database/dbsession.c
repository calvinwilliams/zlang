/* Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "zobjects_database.h"

struct DatabaseConnectionContext
{
	struct DatabaseConnection	*db_conn ;
} ;

funcInitSessionContext InitConnectionContext;
int InitConnectionContext( struct CommonPool *pool , void *session_ctx )
{
	struct DatabaseConnectionContext	*db_conn_ctx = (struct DatabaseConnectionContext *)session_ctx ;
	struct ZlangDirectProperty_dbsession	*dbsession_direct_prop = (struct ZlangDirectProperty_dbsession *)GetCommonPoolUserData( pool ) ;
	struct ZlangRuntime			*rt = dbsession_direct_prop->rt ;
	
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "InitSessionContext pool[%p] db_conn_ctx[%p] dbsession_direct_prop[%p] rt[%p]" , pool , db_conn_ctx , dbsession_direct_prop , rt )
	
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "DBCConnectToDatabase ..." )
	db_conn_ctx->db_conn = DBCConnectToDatabase( dbsession_direct_prop->db_driver , dbsession_direct_prop->db_host , dbsession_direct_prop->db_port , dbsession_direct_prop->db_user , dbsession_direct_prop->db_pass , dbsession_direct_prop->db_name ) ;
	if( db_conn_ctx->db_conn == NULL )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "DBCConnectToDatabase failed[%d]" , DBCGetLastErrno() )
		return -1;
	}
	else
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "DBCConnectToDatabase ok" )
	}
	
	return 0;
}

static int TestConnection( struct ZlangDirectProperty_dbsession *dbsession_direct_prop , struct DatabaseConnectionContext *db_conn_ctx )
{
	struct ZlangRuntime			*rt = dbsession_direct_prop->rt ;
	struct ZlangObject			*dbresult_obj = NULL ;
	char					sql[] = "SELECT 1" ;
	struct ZlangDirectProperty_dbresult	*dbresult_direct_prop = NULL ;
	int					nret = 0 ;
	
	dbresult_obj = AllocObject( rt ) ;
	if( dbresult_obj == NULL )
		return -1;
	
	nret = InitObject( rt , dbresult_obj , NULL , & direct_funcs_dbresult , 0 ) ;
	if( nret )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "InitObject failed[%d]" , nret )
		return nret;
	}
	
	dbresult_direct_prop = GetObjectDirectProperty(dbresult_obj) ;
	DBCExecuteSql( dbsession_direct_prop->db_driver , db_conn_ctx->db_conn , sql , NULL , 0 , & (dbresult_direct_prop->row_count) , & (dbresult_direct_prop->col_count) , & (dbresult_direct_prop->query_field_set) , & (dbresult_direct_prop->query_result_set) , & (dbresult_direct_prop->affected_count) );
	if( DBCGetLastErrno() )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "DBCExecuteSql[%s] failed[%d]" , sql , DBCGetLastErrno() )
		DestroyObject( rt , dbresult_obj );
		return -1;
	}
	else
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "DBCExecuteSql[%s] ok" , sql )
	}
	
	if( dbresult_direct_prop->row_count == 1 && dbresult_direct_prop->col_count == 1 )
	{
	}
	else
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "row_count[%d] col_count[%d] invalid" , dbresult_direct_prop->row_count , dbresult_direct_prop->col_count )
		DestroyObject( rt , dbresult_obj );
		return -1;
	}
	
	DestroyObject( rt , dbresult_obj );
	
	return 0;
}

funcWatchingSessions WatchingConnections;
int WatchingConnections( struct CommonPool *pool )
{
	size_t					i ;
	size_t					idle_sessions_count ;
	struct DatabaseConnectionContext	*db_conn_ctx = NULL ;
	struct ZlangDirectProperty_dbsession	*dbsession_direct_prop = (struct ZlangDirectProperty_dbsession *)GetCommonPoolUserData( pool ) ;
	struct ZlangRuntime			*rt = dbsession_direct_prop->rt ;
	int					nret = 0 ;
	
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "WatchingSessions pool[%p] dbsession_direct_prop[%p] rt[%p]" , pool , dbsession_direct_prop , rt )
	
	idle_sessions_count = GetCommonPoolIdleSessionsCount( pool ) ;
	for( i = 0 ; i < idle_sessions_count ; i++ )
	{
		nret = JustFetchCommonPoolIdleSession( pool , (void**) & db_conn_ctx ) ;
		if( nret == COMMONPOOL_ERROR_NO_IDLE_SESSION )
			break;
		
		nret = TestConnection( dbsession_direct_prop , db_conn_ctx ) ;
		if( nret )
			ReleaseCommonPoolIdleSession( pool , db_conn_ctx );
		else
			GivebackCommonPoolSession( pool , db_conn_ctx );
	}
	
	return 0;
}

funcCleanSessionContext CleanConnectionContext;
int CleanConnectionContext( struct CommonPool *pool , void *session_ctx )
{
	struct DatabaseConnectionContext	*db_conn_ctx = (struct DatabaseConnectionContext *)session_ctx ;
	struct ZlangDirectProperty_dbsession	*dbsession_direct_prop = (struct ZlangDirectProperty_dbsession *)GetCommonPoolUserData( pool ) ;
	struct ZlangRuntime			*rt = dbsession_direct_prop->rt ;
	
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "CleanSessionContext pool[%p] session_ctx[%p]" , pool , session_ctx )
	
	DBCDisconnecFromDatabase( dbsession_direct_prop->db_driver , & (db_conn_ctx->db_conn) );
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "DBCDisconnecFromDatabase ok" )
	
	return 0;
}

static struct CommonPoolCallback	g_connections_pool_callback =
	{
		sizeof(struct DatabaseConnectionContext)
		, InitConnectionContext
		, WatchingConnections
		, CleanConnectionContext
	} ;

int CreateAndStartCommonPool( struct ZlangDirectProperty_dbsession *dbsession_direct_prop )
{
	// struct ZlangDirectProperty_dbsession	*dbsession_direct_prop = GetObjectDirectProperty(dbsession_obj) ;
	int					nret = 0 ;
	
	dbsession_direct_prop->connections_pool = CreateCommonPool( & g_connections_pool_callback , sizeof(struct CommonPoolCallback) ) ;
	if( dbsession_direct_prop->connections_pool == NULL )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( dbsession_direct_prop->rt , "CreateCommonPool failed" )
		return -1;
	}
	else
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( dbsession_direct_prop->rt , "CreateCommonPool ok" )
	}
	
	TEST_RUNTIME_DEBUG_THEN_PRINT( dbsession_direct_prop->rt , "EnableAssistantThread[%d]" , dbsession_direct_prop->assistant_thread_enable )
	EnableAssistantThread( dbsession_direct_prop->connections_pool , dbsession_direct_prop->assistant_thread_enable );
	TEST_RUNTIME_DEBUG_THEN_PRINT( dbsession_direct_prop->rt , "SetCommonPoolMinIdleSessionsCount[%"PRIi32"]" , dbsession_direct_prop->min_idle_connections_count )
	SetCommonPoolMinIdleSessionsCount( dbsession_direct_prop->connections_pool , dbsession_direct_prop->min_idle_connections_count );
	TEST_RUNTIME_DEBUG_THEN_PRINT( dbsession_direct_prop->rt , "SetCommonPoolMaxIdleSessionsCount[%"PRIi32"]" , dbsession_direct_prop->max_idle_connections_count )
	SetCommonPoolMaxIdleSessionsCount( dbsession_direct_prop->connections_pool , dbsession_direct_prop->max_idle_connections_count );
	TEST_RUNTIME_DEBUG_THEN_PRINT( dbsession_direct_prop->rt , "SetCommonPoolMaxSessionsCount[%"PRIi32"]" , dbsession_direct_prop->max_connections_count )
	SetCommonPoolMaxSessionsCount( dbsession_direct_prop->connections_pool , dbsession_direct_prop->max_connections_count );
	TEST_RUNTIME_DEBUG_THEN_PRINT( dbsession_direct_prop->rt , "SetCommonPoolMaxIdleTimeval[%"PRIi32"]" , dbsession_direct_prop->max_idle_timeval )
	SetCommonPoolMaxIdleTimeval( dbsession_direct_prop->connections_pool , dbsession_direct_prop->max_idle_timeval );
	TEST_RUNTIME_DEBUG_THEN_PRINT( dbsession_direct_prop->rt , "SetCommonPoolWatchIdleTimeval[%"PRIi32"]" , dbsession_direct_prop->watch_idle_timeval )
	SetCommonPoolWatchIdleTimeval( dbsession_direct_prop->connections_pool , dbsession_direct_prop->watch_idle_timeval );
	TEST_RUNTIME_DEBUG_THEN_PRINT( dbsession_direct_prop->rt , "SetCommonPoolInspectTimeval[%"PRIi32"]" , dbsession_direct_prop->inspect_timeval )
	SetCommonPoolInspectTimeval( dbsession_direct_prop->connections_pool , dbsession_direct_prop->inspect_timeval );
	
	SetCommonPoolUserData( dbsession_direct_prop->connections_pool , dbsession_direct_prop );
	
	nret = StartCommonPool( dbsession_direct_prop->connections_pool ) ;
	if( nret )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( dbsession_direct_prop->rt , "StartCommonPool failed[%d]" , nret )
		return -1;
	}
	else
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( dbsession_direct_prop->rt , "StartCommonPool ok" )
	}
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_dbsession_Execute_vargs;
int ZlangInvokeFunction_dbsession_Execute_vargs( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_dbsession	*dbsession_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*in1_obj = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject			*inN_obj = NULL ;
	int					in_params_count ;
	int					in_param_no ;
	struct FieldBind			*in_params_array = NULL ;
	struct FieldBind			*binds_array_offsetptr = NULL ;
	struct ZlangObject			*out1_obj = GetOutputParameterInLocalObjectStack(rt,1) ;
	char					*sql = NULL ;
	struct ZlangDirectProperty_dbresult	*dbresult_direct_prop = NULL ;
	DECLARE_BEGIN_TIMEVAL_AND_INIT
	int					nret = 0 ;
	
	CallRuntimeFunction_string_GetStringValue( rt , in1_obj , & sql , NULL );
	
	if( dbsession_direct_prop->connections_pool == NULL )
	{
		nret = CreateAndStartCommonPool( dbsession_direct_prop ) ;
		if( nret )
		{
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "CreateAndStartCommonPool failed[%d]" , nret )
			UnreferObject( rt , out1_obj );
			return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_CREATE_AND_START_CONNECTION_POOL_FAILED );
		}
		else
		{
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "CreateAndStartCommonPool ok" )
		}
	}
	
	in_params_count = GetInputParameterCountInLocalObjectStack( rt ) - 1 ;
	if( in_params_count >= 1 )
	{
		in_params_array = (struct FieldBind *)ZLMALLOC( sizeof(struct FieldBind) * in_params_count ) ;
		if( in_params_array == NULL )
		{
			SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_ALLOC , "alloc memory for binds array" )
			return ThrowFatalException( rt , ZLANG_ERROR_ALLOC , EXCEPTION_MESSAGE_ALLOC_FAILED );
		}
		memset( in_params_array , 0x00 , sizeof(struct FieldBind) * in_params_count );
		
		for( in_param_no = 0 , binds_array_offsetptr = in_params_array ; in_param_no < in_params_count ; in_param_no++ , binds_array_offsetptr++ )
		{
			inN_obj = GetInputParameterInLocalObjectStack(rt,in_param_no+2) ;
			if( inN_obj == NULL )
			{
				SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_INTERNAL , "GetInputParameterInLocalObjectStack return NULL" )
				ZLFREE( in_params_array );
				return ThrowFatalException( rt , ZLANG_ERROR_INTERNAL , EXCEPTION_MESSAGE_GENERAL_ERROR );
			}
			
			if( IsTypeOf( rt , inN_obj , GetShortObjectInRuntimeObjectsHeap(rt) ) )
			{
				binds_array_offsetptr->buffer_type = CDBC_FIELDTYPE_INT16 ;
				binds_array_offsetptr->buffer = (char*)GetObjectDirectProperty( inN_obj ) ;
			}
			else if( IsTypeOf( rt , inN_obj , GetIntObjectInRuntimeObjectsHeap(rt) ) )
			{
				binds_array_offsetptr->buffer_type = CDBC_FIELDTYPE_INT32 ;
				binds_array_offsetptr->buffer = (char*)GetObjectDirectProperty( inN_obj ) ;
			}
			else if( IsTypeOf( rt , inN_obj , GetLongObjectInRuntimeObjectsHeap(rt) ) )
			{
				binds_array_offsetptr->buffer_type = CDBC_FIELDTYPE_INT64 ;
				binds_array_offsetptr->buffer = (char*)GetObjectDirectProperty( inN_obj ) ;
			}
			else if( IsTypeOf( rt , inN_obj , GetFloatObjectInRuntimeObjectsHeap(rt) ) )
			{
				binds_array_offsetptr->buffer_type = CDBC_FIELDTYPE_FLOAT ;
				binds_array_offsetptr->buffer = (char*)GetObjectDirectProperty( inN_obj ) ;
			}
			else if( IsTypeOf( rt , inN_obj , GetDoubleObjectInRuntimeObjectsHeap(rt) ) )
			{
				binds_array_offsetptr->buffer_type = CDBC_FIELDTYPE_DOUBLE ;
				binds_array_offsetptr->buffer = (char*)GetObjectDirectProperty( inN_obj ) ;
			}
			else if( IsTypeOf( rt , inN_obj , GetStringObjectInRuntimeObjectsHeap(rt) ) )
			{
				char	*str = NULL ;
				int32_t	str_len ;
				GetDataPtr( rt , inN_obj , (void**) & str , & str_len );
				binds_array_offsetptr->buffer_type = CDBC_FIELDTYPE_VARCHAR ;
				binds_array_offsetptr->buffer = str ;
				binds_array_offsetptr->buffer_length = str_len ;
			}
			else if( STRCMP( GetCloneObjectName(inN_obj) , == , "datetime" ) )
			{
				binds_array_offsetptr->buffer_type = CDBC_FIELDTYPE_DATETIME ;
				binds_array_offsetptr->buffer = (char*)GetObjectDirectProperty( inN_obj ) ;
			}
			else
			{
				for( in_param_no = 0 , binds_array_offsetptr = in_params_array ; in_param_no < in_params_count ; in_param_no++ , binds_array_offsetptr++ )
					if( binds_array_offsetptr->buffer_alloced )
						ZLFREE( binds_array_offsetptr->buffer_alloced );
				ZLFREE( in_params_array );
				SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_INVOKE_METHOD_RETURN , "object type not supported" )
				return ThrowFatalException( rt , ZLANG_FATAL_INVOKE_METHOD_RETURN , EXCEPTION_MESSAGE_GENERAL_ERROR );
			}
		}
	}
	
	dbresult_direct_prop = GetObjectDirectProperty(out1_obj) ;
	
	if( dbsession_direct_prop->in_transaction == 0 )
	{
		nret = FetchCommonPoolSession( dbsession_direct_prop->connections_pool , (void **) & (dbsession_direct_prop->db_conn_ctx) ) ;
		if( nret )
		{
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "FetchCommonPoolSession failed[%d]" , nret )
			ZLFREE( in_params_array );
			UnreferObject( rt , out1_obj );
			return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_FETCH_CONNECTION_SESSION_FAILED );
		}
		else
		{
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "FetchCommonPoolSession ok , session_ctx[%p]" , dbsession_direct_prop->db_conn_ctx )
		}
	}
	else
	{
		if( dbsession_direct_prop->db_conn_ctx == NULL )
		{
			ZLFREE( in_params_array );
			UnreferObject( rt , out1_obj );
			return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_NO_CONNECTION_POOL );
		}
	}
	
	GET_BEGIN_TIMEVAL
	DBCExecuteSql( dbsession_direct_prop->db_driver , dbsession_direct_prop->db_conn_ctx->db_conn , sql , in_params_array , in_params_count , & (dbresult_direct_prop->row_count) , & (dbresult_direct_prop->col_count) , & (dbresult_direct_prop->query_field_set) , & (dbresult_direct_prop->query_result_set) , & (dbresult_direct_prop->affected_count) );
	GET_END_TIMEVAL_AND_DIFF_ELAPSE( dbsession_direct_prop->sql_elapse , BEGIN_TIMEVAL )
	ACCUMULATE_DIFF_ELAPSE( dbsession_direct_prop->total_sql_elapse , dbsession_direct_prop->sql_elapse )
	for( in_param_no = 0 , binds_array_offsetptr = in_params_array ; in_param_no < in_params_count ; in_param_no++ , binds_array_offsetptr++ )
		if( binds_array_offsetptr->buffer_alloced )
			free( binds_array_offsetptr->buffer_alloced );
	if( in_params_array )
		ZLFREE( in_params_array );
	if( DBCGetLastErrno() )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "DBCExecuteStatementSql[%s] failed[%d]" , sql , DBCGetLastErrno() )
		
		if( dbsession_direct_prop->in_transaction == 0 )
		{
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "GivebackCommonPoolSession , session_ctx[%p]" , dbsession_direct_prop->db_conn_ctx )
			GivebackCommonPoolSession( dbsession_direct_prop->connections_pool , dbsession_direct_prop->db_conn_ctx );
		}
		UnreferObject( rt , out1_obj );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_EXECUTE_SQL );
	}
	else
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "DBCExecuteStatementSql[%s] ok" , sql )
	}
	
	if( dbsession_direct_prop->in_transaction == 0 )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "GivebackCommonPoolSession , session_ctx[%p]" , dbsession_direct_prop->db_conn_ctx )
		GivebackCommonPoolSession( dbsession_direct_prop->connections_pool , dbsession_direct_prop->db_conn_ctx );
	}
	
	return 0;
}

int dbsession_Disconnect( struct ZlangRuntime *rt , struct ZlangDirectProperty_dbsession *dbsession_direct_prop )
{
	if( dbsession_direct_prop )
	{
		if( dbsession_direct_prop->connections_pool )
		{
			StopCommonPool( dbsession_direct_prop->connections_pool );
			DestroyCommonPool( dbsession_direct_prop->connections_pool ); dbsession_direct_prop->connections_pool = NULL ;
		}
	}
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_dbsession_Disconnect;
int ZlangInvokeFunction_dbsession_Disconnect( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	int		nret = 0 ;
	
	nret = dbsession_Disconnect( rt , GetObjectDirectProperty(obj) ) ;
	if( nret )
		return ThrowFatalException( rt , nret , EXCEPTION_MESSAGE_GENERAL_ERROR );
	else
		return 0;
}

int dbsession_Reconnect( struct ZlangRuntime *rt , struct ZlangDirectProperty_dbsession *dbsession_direct_prop )
{
	int		nret = 0 ;
	
	if( dbsession_direct_prop )
	{
		if( dbsession_direct_prop->connections_pool )
		{
			dbsession_Disconnect( rt , dbsession_direct_prop );
		}
			
		nret = CreateAndStartCommonPool( dbsession_direct_prop ) ;
		if( nret )
		{
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "CreateAndStartCommonPool failed[%d]" , nret )
			SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_INVOKE_METHOD_RETURN , "CreateAndStartCommonPool failed[%d]" , nret );
			return ThrowErrorException( rt , nret , EXCEPTION_MESSAGE_CREATE_AND_START_CONNECTION_POOL_FAILED );
		}
		else
		{
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "CreateAndStartCommonPool ok" )
		}
	}
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_dbsession_Reconnect;
int ZlangInvokeFunction_dbsession_Reconnect( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	return dbsession_Reconnect( rt , GetObjectDirectProperty(obj) );
}

ZlangInvokeFunction ZlangInvokeFunction_dbsession_AutoCommit_bool;
int ZlangInvokeFunction_dbsession_AutoCommit_bool( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_dbsession	*dbsession_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*in1_obj = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject			*out1_obj = GetOutputParameterInLocalObjectStack(rt,1) ;
	unsigned char				enable_autocommit ;
	/*
	struct DatabaseConnectionContext	*db_conn_ctx = NULL ;
	*/
	int					nret = 0 ;
	
	if( dbsession_direct_prop->connections_pool == NULL )
	{
		nret = CreateAndStartCommonPool( dbsession_direct_prop ) ;
		if( nret )
		{
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "CreateAndStartCommonPool failed[%d]" , nret )
			CallRuntimeFunction_bool_SetBoolValue( rt , out1_obj , FALSE );
			return ThrowErrorException( rt , nret , EXCEPTION_MESSAGE_CREATE_AND_START_CONNECTION_POOL_FAILED );
		}
		else
		{
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "CreateAndStartCommonPool ok" )
		}
	}
	
	CallRuntimeFunction_bool_GetBoolValue( rt , in1_obj , & enable_autocommit );
	
	/* wait for implements in future
	LockBeforeTravelIdleCommonPool( dbsession_direct_prop->connections_pool );
	{
		db_conn_ctx = TravelCommonPoolIdleSession( dbsession_direct_prop->connections_pool , NULL , 1 ) ;
		while( db_conn_ctx )
		{
			DBCAutoCommitTransaction( dbsession_direct_prop->db_driver , db_conn_ctx->db_conn , enable_autocommit );
			ChangebackAfterTravelCommonPoolSession( dbsession_direct_prop->connections_pool , db_conn_ctx );
			
			db_conn_ctx = TravelCommonPoolIdleSession( dbsession_direct_prop->connections_pool , db_conn_ctx , 1 ) ;
		}
	}
	UnlockAfterTravelIdleCommonPool( dbsession_direct_prop->connections_pool );
	*/
	
	CallRuntimeFunction_bool_SetBoolValue( rt , out1_obj , TRUE );
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_dbsession_Begin;
int ZlangInvokeFunction_dbsession_Begin( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_dbsession	*dbsession_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*out1_obj = GetOutputParameterInLocalObjectStack(rt,1) ;
	int					nret = 0 ;
	
	if( dbsession_direct_prop->connections_pool == NULL )
	{
		nret = CreateAndStartCommonPool( dbsession_direct_prop ) ;
		if( nret )
		{
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "CreateAndStartCommonPool failed[%d]" , nret )
			CallRuntimeFunction_bool_SetBoolValue( rt , out1_obj , FALSE );
			return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_BEGIN_TRANSACTION_FAILED );
		}
		else
		{
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "CreateAndStartCommonPool ok" )
		}
	}
	
	if( dbsession_direct_prop->in_transaction == 0 )
	{
		nret = FetchCommonPoolSession( dbsession_direct_prop->connections_pool , (void **) & (dbsession_direct_prop->db_conn_ctx) ) ;
		if( nret )
		{
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "FetchCommonPoolSession failed[%d]" , nret )
			CallRuntimeFunction_bool_SetBoolValue( rt , out1_obj , FALSE );
			return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_FETCH_CONNECTION_SESSION_FAILED );
		}
		else
		{
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "FetchCommonPoolSession ok , session_ctx[%p]" , dbsession_direct_prop->db_conn_ctx )
		}
		
		DBCBeginTransaction( dbsession_direct_prop->db_driver , dbsession_direct_prop->db_conn_ctx->db_conn );
		
		dbsession_direct_prop->in_transaction = 1 ;
	}
	else
	{
		CallRuntimeFunction_bool_SetBoolValue( rt , out1_obj , FALSE );
		return 0;
	}
	
	CallRuntimeFunction_bool_SetBoolValue( rt , out1_obj , TRUE );
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_dbsession_Commit;
int ZlangInvokeFunction_dbsession_Commit( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_dbsession	*dbsession_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*out1_obj = GetOutputParameterInLocalObjectStack(rt,1) ;
	int					nret = 0 ;
	
	if( dbsession_direct_prop->connections_pool == NULL )
	{
		nret = CreateAndStartCommonPool( dbsession_direct_prop ) ;
		if( nret )
		{
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "CreateAndStartCommonPool failed[%d]" , nret )
			CallRuntimeFunction_bool_SetBoolValue( rt , out1_obj , FALSE );
			return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_CREATE_AND_START_CONNECTION_POOL_FAILED );
		}
		else
		{
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "CreateAndStartCommonPool ok" )
		}
	}
	
	if( dbsession_direct_prop->in_transaction == 0 )
	{
		CallRuntimeFunction_bool_SetBoolValue( rt , out1_obj , FALSE );
		return 0;
	}
	else
	{
		DBCCommitTransaction( dbsession_direct_prop->db_driver , dbsession_direct_prop->db_conn_ctx->db_conn );
		
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "GivebackCommonPoolSession , session_ctx[%p]" , dbsession_direct_prop->db_conn_ctx )
		nret = GivebackCommonPoolSession( dbsession_direct_prop->connections_pool , dbsession_direct_prop->db_conn_ctx ) ;
		if( nret )
		{
			CallRuntimeFunction_bool_SetBoolValue( rt , out1_obj , FALSE );
			return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_GIVEBACK_CONNECTION_SESSION_FAILED );
		}
		
		dbsession_direct_prop->in_transaction = 0 ;
		dbsession_direct_prop->db_conn_ctx = NULL ;
	}
	
	CallRuntimeFunction_bool_SetBoolValue( rt , out1_obj , TRUE );
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_dbsession_Rollback;
int ZlangInvokeFunction_dbsession_Rollback( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_dbsession	*dbsession_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*out1_obj = GetOutputParameterInLocalObjectStack(rt,1) ;
	int					nret = 0 ;
	
	if( dbsession_direct_prop->connections_pool == NULL )
	{
		nret = CreateAndStartCommonPool( dbsession_direct_prop ) ;
		if( nret )
		{
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "CreateAndStartCommonPool failed[%d]" , nret )
			CallRuntimeFunction_bool_SetBoolValue( rt , out1_obj , FALSE );
			return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_CREATE_AND_START_CONNECTION_POOL_FAILED );
		}
		else
		{
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "CreateAndStartCommonPool ok" )
		}
	}
	
	if( dbsession_direct_prop->in_transaction == 0 )
	{
		CallRuntimeFunction_bool_SetBoolValue( rt , out1_obj , FALSE );
		return 0;
	}
	else
	{
		DBCRollbackTransaction( dbsession_direct_prop->db_driver , dbsession_direct_prop->db_conn_ctx->db_conn );
		
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "GivebackCommonPoolSession , session_ctx[%p]" , dbsession_direct_prop->db_conn_ctx )
		nret = GivebackCommonPoolSession( dbsession_direct_prop->connections_pool , dbsession_direct_prop->db_conn_ctx ) ;
		if( nret )
		{
			CallRuntimeFunction_bool_SetBoolValue( rt , out1_obj , FALSE );
			return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_GIVEBACK_CONNECTION_SESSION_FAILED );
		}
		
		dbsession_direct_prop->in_transaction = 0 ;
		dbsession_direct_prop->db_conn_ctx = NULL ;
	}
	
	CallRuntimeFunction_bool_SetBoolValue( rt , out1_obj , TRUE );
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_dbsession_GetSqlElapse;
int ZlangInvokeFunction_dbsession_GetSqlElapse( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_dbsession	*dbsession_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*out1_obj = GetOutputParameterInLocalObjectStack(rt,1) ;
	
	CallRuntimeFunction_int_SetIntValue( rt , out1_obj , dbsession_direct_prop->sql_elapse.tv_sec*1000000+dbsession_direct_prop->sql_elapse.tv_usec );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_dbsession_ClearAccumulateDiffElapse;
int ZlangInvokeFunction_dbsession_ClearAccumulateDiffElapse( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_dbsession	*dbsession_direct_prop = GetObjectDirectProperty(obj) ;
	
	CLEAR_ACCUMULATE_DIFF_ELAPSE( dbsession_direct_prop->total_sql_elapse )
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_dbsession_AccumulateDiffElapse;
int ZlangInvokeFunction_dbsession_AccumulateDiffElapse( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_dbsession	*dbsession_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*out1_obj = GetOutputParameterInLocalObjectStack(rt,1) ;
	
	CallRuntimeFunction_int_SetIntValue( rt , out1_obj , dbsession_direct_prop->total_sql_elapse.tv_sec*1000000+dbsession_direct_prop->total_sql_elapse.tv_usec );
	
	return 0;
}

ZlangCreateDirectPropertyFunction ZlangCreateDirectProperty_dbsession;
void *ZlangCreateDirectProperty_dbsession( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_dbsession	*dbsession_prop = NULL ;
	
	dbsession_prop = (struct ZlangDirectProperty_dbsession *)ZLMALLOC( sizeof(struct ZlangDirectProperty_dbsession) ) ;
	if( dbsession_prop == NULL )
	{
		SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_ALLOC , "alloc memory for entity" )
		return NULL;
	}
	memset( dbsession_prop , 0x00 , sizeof(struct ZlangDirectProperty_dbsession) );
	
	return dbsession_prop;
}

ZlangDestroyDirectPropertyFunction ZlangDestroyDirectProperty_dbsession;
void ZlangDestroyDirectProperty_dbsession( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_dbsession	*dbsession_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangDirectProperty_database	*database_direct_prop = NULL ;
	
	if( dbsession_direct_prop )
	{
		database_direct_prop = dbsession_direct_prop->ref_database_direct_prop ;
		if( database_direct_prop )
		{
			if( database_direct_prop->last_session_direct_prop == dbsession_direct_prop )
			{
				database_direct_prop->last_session_direct_prop = NULL ;
			}
		}
		
		if( dbsession_direct_prop->connections_pool )
		{
			StopCommonPool( dbsession_direct_prop->connections_pool );
			DestroyCommonPool( dbsession_direct_prop->connections_pool );
		}
		
		if( dbsession_direct_prop->db_host )
		{
			ZLFREE( dbsession_direct_prop->db_host );
		}
		if( dbsession_direct_prop->db_user )
			ZLFREE( dbsession_direct_prop->db_user );
		if( dbsession_direct_prop->db_pass )
			ZLFREE( dbsession_direct_prop->db_pass );
		if( dbsession_direct_prop->db_name )
			ZLFREE( dbsession_direct_prop->db_name );
		
		ZLFREE( dbsession_direct_prop );
	}
	
	return;
}

ZlangSummarizeDirectPropertySizeFunction ZlangSummarizeDirectPropertySize_dbsession;
void ZlangSummarizeDirectPropertySize_dbsession( struct ZlangRuntime *rt , struct ZlangObject *obj , size_t *summarized_obj_size , size_t *summarized_direct_prop_size )
{
	struct ZlangDirectProperty_dbsession	*dbsession_direct_prop = GetObjectDirectProperty(obj) ;
	
	if( dbsession_direct_prop->db_host )
		SUMMARIZE_SIZE( summarized_direct_prop_size , strlen(dbsession_direct_prop->db_host)+1 )
	if( dbsession_direct_prop->db_user )
		SUMMARIZE_SIZE( summarized_direct_prop_size , strlen(dbsession_direct_prop->db_user)+1 )
	if( dbsession_direct_prop->db_pass )
		SUMMARIZE_SIZE( summarized_direct_prop_size , strlen(dbsession_direct_prop->db_pass)+1 )
	if( dbsession_direct_prop->db_name )
		SUMMARIZE_SIZE( summarized_direct_prop_size , strlen(dbsession_direct_prop->db_name)+1 )
		
	SUMMARIZE_SIZE( summarized_direct_prop_size , sizeof(struct ZlangDirectProperty_dbsession) )
	
	return;
}

struct ZlangDirectFunctions direct_funcs_dbsession =
	{
		ZLANG_OBJECT_dbsession , /* char *tpye_name */
		
		ZlangCreateDirectProperty_dbsession , /* ZlangCreateDirectPropertyFunction *create_entity_func */
		ZlangDestroyDirectProperty_dbsession , /* ZlangDestroyDirectPropertyFunction *destroy_entity_func */
		
		NULL , /* ZlangFromCharPtrFunction *from_char_ptr_func */
		NULL , /* ZlangToStringFunction *to_string_func */
		NULL , /* ZlangFromDataPtrFunction *from_data_ptr_func */
		NULL , /* ZlangGetDataPtrFunction *get_data_ptr_func */
		
		NULL , /* ZlangOperatorFunction *oper_PLUS_func */
		NULL , /* ZlangOperatorFunction *oper_MINUS_func */
		NULL , /* ZlangOperatorFunction *oper_MUL_func */
		NULL , /* ZlangOperatorFunction *oper_DIV_func */
		NULL , /* ZlangOperatorFunction *oper_MOD_func */
		
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_NEGATIVE_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_NOT_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_BIT_REVERSE_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_PLUS_PLUS_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_MINUS_MINUS_func */
		
		NULL , /* ZlangCompareFunction *comp_EGUAL_func */
		NULL , /* ZlangCompareFunction *comp_NOTEGUAL_func */
		NULL , /* ZlangCompareFunction *comp_LT_func */
		NULL , /* ZlangCompareFunction *comp_LE_func */
		NULL , /* ZlangCompareFunction *comp_GT_func */
		NULL , /* ZlangCompareFunction *comp_GE_func */
		
		NULL , /* ZlangLogicFunction *logic_AND_func */
		NULL , /* ZlangLogicFunction *logic_OR_func */
		
		NULL , /* ZlangLogicFunction *bit_AND_func */
		NULL , /* ZlangLogicFunction *bit_XOR_func */
		NULL , /* ZlangLogicFunction *bit_OR_func */
		NULL , /* ZlangLogicFunction *bit_MOVELEFT_func */
		NULL , /* ZlangLogicFunction *bit_MOVERIGHT_func */
		
		ZlangSummarizeDirectPropertySize_dbsession , /* ZlangSummarizeDirectPropertySizeFunction *summarize_direct_prop_size_func */
	} ;

ZlangImportObjectFunction ZlangImportObject_dbsession;
struct ZlangObject *ZlangImportObject_dbsession( struct ZlangRuntime *rt )
{
	struct ZlangObject	*obj = NULL ;
	struct ZlangFunction	*func = NULL ;
	int			nret = 0 ;
	
	nret = ImportObject( rt , & obj , ZLANG_OBJECT_dbsession , & direct_funcs_dbsession , sizeof(struct ZlangDirectFunctions) , NULL ) ;
	if( nret )
	{
		SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_LINK_FUNC_TO_ENTITY , "import object to global objects heap" )
		return NULL;
	}
	
	/* dbsession.Execute(...) */
	func = AddFunctionAndParametersInObject( rt , obj , "Execute" , "Execute(...)" , ZlangInvokeFunction_dbsession_Execute_vargs , ZLANG_OBJECT_dbresult , ZLANG_OBJECT_vargs,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* dbsession.AutoCommit(bool) */
	func = AddFunctionAndParametersInObject( rt , obj , "AutoCommit" , "AutoCommit(bool)" , ZlangInvokeFunction_dbsession_AutoCommit_bool , ZLANG_OBJECT_bool , ZLANG_OBJECT_bool,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* dbsession.Begin() */
	func = AddFunctionAndParametersInObject( rt , obj , "Begin" , "Begin()" , ZlangInvokeFunction_dbsession_Begin , ZLANG_OBJECT_bool , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* dbsession.Commit() */
	func = AddFunctionAndParametersInObject( rt , obj , "Commit" , "Commit()" , ZlangInvokeFunction_dbsession_Commit , ZLANG_OBJECT_bool , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* dbsession.Rollback() */
	func = AddFunctionAndParametersInObject( rt , obj , "Rollback" , "Rollback()" , ZlangInvokeFunction_dbsession_Rollback , ZLANG_OBJECT_bool , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* dbsession.GetSqlElapse() */
	func = AddFunctionAndParametersInObject( rt , obj , "GetSqlElapse" , "GetSqlElapse()" , ZlangInvokeFunction_dbsession_GetSqlElapse , ZLANG_OBJECT_int , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* dbsession.ClearAccumulateDiffElapse() */
	func = AddFunctionAndParametersInObject( rt , obj , "ClearAccumulateDiffElapse" , "ClearAccumulateDiffElapse()" , ZlangInvokeFunction_dbsession_ClearAccumulateDiffElapse , ZLANG_OBJECT_void , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* dbsession.AccumulateDiffElapse() */
	func = AddFunctionAndParametersInObject( rt , obj , "" , "AccumulateDiffElapse()" , ZlangInvokeFunction_dbsession_AccumulateDiffElapse , ZLANG_OBJECT_int , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* dbsession.Disconnect() */
	func = AddFunctionAndParametersInObject( rt , obj , "Disconnect" , "Disconnect()" , ZlangInvokeFunction_dbsession_Disconnect , ZLANG_OBJECT_void , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* dbsession.Reconnect() */
	func = AddFunctionAndParametersInObject( rt , obj , "Reconnect" , "Reconnect()" , ZlangInvokeFunction_dbsession_Reconnect , ZLANG_OBJECT_void , NULL ) ;
	if( func == NULL )
		return NULL;
	
	return obj ;
}

