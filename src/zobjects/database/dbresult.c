/* Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "zobjects_database.h"

ZlangInvokeFunction ZlangInvokeFunction_dbresult_NotFound;
int ZlangInvokeFunction_dbresult_NotFound( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_dbresult	*dbresult_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*out1_obj = GetOutputParameterInLocalObjectStack(rt,1) ;
	
	if( dbresult_direct_prop->row_count == 0 )
		CallRuntimeFunction_bool_SetBoolValue( rt , out1_obj , TRUE );
	else
		CallRuntimeFunction_bool_SetBoolValue( rt , out1_obj , FALSE );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_dbresult_Found;
int ZlangInvokeFunction_dbresult_Found( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_dbresult	*dbresult_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*out1_obj = GetOutputParameterInLocalObjectStack(rt,1) ;
	
	if( dbresult_direct_prop->row_count > 0 )
		CallRuntimeFunction_bool_SetBoolValue( rt , out1_obj , TRUE );
	else
		CallRuntimeFunction_bool_SetBoolValue( rt , out1_obj , FALSE );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_dbresult_GetRowCount;
int ZlangInvokeFunction_dbresult_GetRowCount( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_dbresult	*dbresult_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*out1_obj = GetOutputParameterInLocalObjectStack(rt,1) ;
	
	CallRuntimeFunction_int_SetIntValue( rt , out1_obj , dbresult_direct_prop->row_count );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_dbresult_GetColumnCount;
int ZlangInvokeFunction_dbresult_GetColumnCount( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_dbresult	*dbresult_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*out1_obj = GetOutputParameterInLocalObjectStack(rt,1) ;
	
	CallRuntimeFunction_int_SetIntValue( rt , out1_obj , dbresult_direct_prop->col_count );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_dbresult_GetFieldName_int;
int ZlangInvokeFunction_dbresult_GetFieldName_int( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_dbresult	*dbresult_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*in1_obj = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject			*out1_obj = GetOutputParameterInLocalObjectStack(rt,1) ;
	int32_t					field_index ;
	char					*field_name = NULL ;
	
	CallRuntimeFunction_int_GetIntValue( rt , in1_obj , & field_index );
	if( field_index < 1 || field_index > dbresult_direct_prop->col_count )
	{
		CallRuntimeFunction_string_SetStringValue( rt , out1_obj , "" , 0 );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_ILLEGAL_ARGUMENT );
	}
	
	field_name = dbresult_direct_prop->query_field_set[field_index-1].field_name ;
	CallRuntimeFunction_string_SetStringValue( rt , out1_obj , field_name , (int32_t)strlen(field_name) );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_dbresult_GetFieldType_int;
int ZlangInvokeFunction_dbresult_GetFieldType_int( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_dbresult	*dbresult_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*in1_obj = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject			*out1_obj = GetOutputParameterInLocalObjectStack(rt,1) ;
	int32_t					field_index ;
	
	CallRuntimeFunction_int_GetIntValue( rt , in1_obj , & field_index );
	if( field_index < 1 || field_index > dbresult_direct_prop->col_count )
	{
		CallRuntimeFunction_int_SetIntValue( rt , out1_obj , -1 );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_ILLEGAL_ARGUMENT );
	}
	
	CallRuntimeFunction_int_SetIntValue( rt , out1_obj , dbresult_direct_prop->query_field_set[field_index-1].field_type );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_dbresult_GetFieldLength_int;
int ZlangInvokeFunction_dbresult_GetFieldLength_int( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_dbresult	*dbresult_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*in1_obj = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject			*out1_obj = GetOutputParameterInLocalObjectStack(rt,1) ;
	int32_t					field_index ;
	
	CallRuntimeFunction_int_GetIntValue( rt , in1_obj , & field_index );
	if( field_index < 1 || field_index > dbresult_direct_prop->col_count )
	{
		CallRuntimeFunction_int_SetIntValue( rt , out1_obj , -1 );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_ILLEGAL_ARGUMENT );
	}
	
	CallRuntimeFunction_int_SetIntValue( rt , out1_obj , dbresult_direct_prop->query_field_set[field_index-1].field_length );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_dbresult_GetFieldDecimalLength_int;
int ZlangInvokeFunction_dbresult_GetFieldDecimalLength_int( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_dbresult	*dbresult_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*in1_obj = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject			*out1_obj = GetOutputParameterInLocalObjectStack(rt,1) ;
	int32_t					field_index ;
	
	CallRuntimeFunction_int_GetIntValue( rt , in1_obj , & field_index );
	if( field_index < 1 || field_index > dbresult_direct_prop->col_count )
	{
		CallRuntimeFunction_int_SetIntValue( rt , out1_obj , -1 );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_ILLEGAL_ARGUMENT );
	}
	
	CallRuntimeFunction_int_SetIntValue( rt , out1_obj , dbresult_direct_prop->query_field_set[field_index-1].field_decimal_length );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_dbresult_GetQueryResult_int_int;
int ZlangInvokeFunction_dbresult_GetFieldValue_int_int( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_dbresult	*dbresult_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*in1_obj = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject			*in2_obj = GetInputParameterInLocalObjectStack(rt,2) ;
	struct ZlangObject			*out1_obj = GetOutputParameterInLocalObjectStack(rt,1) ;
	int32_t					row_index ;
	int32_t					col_index ;
	char					*value = NULL ;
	
	CallRuntimeFunction_int_GetIntValue( rt , in1_obj , & row_index );
	CallRuntimeFunction_int_GetIntValue( rt , in2_obj , & col_index );
	if( row_index < 1 || row_index > dbresult_direct_prop->row_count || col_index < 1 || col_index > dbresult_direct_prop->col_count )
	{
		CallRuntimeFunction_string_SetStringValue( rt , out1_obj , "" , 0 );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_ILLEGAL_ARGUMENT );
	}
	
	value = dbresult_direct_prop->query_result_set[ dbresult_direct_prop->col_count * (row_index-1) + (col_index-1) ] ;
	if( value )
		CallRuntimeFunction_string_SetStringValue( rt , out1_obj , value , (int32_t)strlen(value) );
	else
		UnreferObject( rt , out1_obj ) ;
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_dbresult_GetAffectedRows;
int ZlangInvokeFunction_dbresult_GetAffectedRows( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_dbresult	*dbresult_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*out1_obj = GetOutputParameterInLocalObjectStack(rt,1) ;
	
	CallRuntimeFunction_int_SetIntValue( rt , out1_obj , dbresult_direct_prop->affected_count );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_dbresult_RowToObject_int;
int ZlangInvokeFunction_dbresult_RowToObject_int( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_dbresult	*dbresult_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*in1_obj = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject			*out1_obj = GetOutputParameterInLocalObjectStack(rt,1) ;
	int					row_index , row_count ;
	int					col_index , col_count ;
	enum FieldType				field_type ;
	struct ZlangObject			*parent_obj = NULL ;
	char					*field_value = NULL ;
	struct ZlangObject			*prop = NULL ;
	int					nret = 0 ;
	
	CallRuntimeFunction_int_GetIntValue( rt , in1_obj , & row_index );
	row_count = dbresult_direct_prop->row_count ;
	if( row_index < 1 || row_index > row_count )
	{
		return 0;
	}
	row_index--;
	
	nret = InitObject( rt , out1_obj , NULL , GetObjectDirectFunctions(GetZObjectInRuntimeObjectsHeap(rt)) , 0 ) ;
	if( nret )
	{
		return ThrowFatalException( rt , nret , EXCEPTION_MESSAGE_INIT_ARRAY_FAILED );
	}
	
	col_count = dbresult_direct_prop->col_count ;
	for( col_index = 0 ; col_index < col_count ; col_index++ )
	{
		field_type = dbresult_direct_prop->query_field_set[col_index].field_type ;
		if( field_type == CDBC_FIELDTYPE_INT16 )
		{
			parent_obj = GetShortObjectInRuntimeObjectsHeap( rt ) ;
		}
		else if( field_type == CDBC_FIELDTYPE_INT32 )
		{
			parent_obj = GetIntObjectInRuntimeObjectsHeap( rt ) ;
		}
		else if( field_type == CDBC_FIELDTYPE_INT64 )
		{
			parent_obj = GetLongObjectInRuntimeObjectsHeap( rt ) ;
		}
		else if( field_type == CDBC_FIELDTYPE_FLOAT )
		{
			parent_obj = GetFloatObjectInRuntimeObjectsHeap( rt ) ;
		}
		else if( field_type == CDBC_FIELDTYPE_DOUBLE )
		{
			parent_obj = GetDoubleObjectInRuntimeObjectsHeap( rt ) ;
		}
		else if( field_type == CDBC_FIELDTYPE_DECIMAL )
		{
			parent_obj = GetDoubleObjectInRuntimeObjectsHeap( rt ) ;
		}
		else if( field_type == CDBC_FIELDTYPE_DATE || field_type == CDBC_FIELDTYPE_TIME || field_type == CDBC_FIELDTYPE_DATETIME || field_type == CDBC_FIELDTYPE_TIMESTAMP )
		{
			parent_obj = QueryGlobalObjectByObjectName( rt , "datetime" ) ;
			if( parent_obj == NULL )
			{
				SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_OBJECT_NOT_IMPORTED_OR_DECLARED , "object '%s' not imported or declared" , "datetime" )
				return ThrowFatalException( rt , ZLANG_ERROR_OBJECT_NOT_IMPORTED_OR_DECLARED , EXCEPTION_MESSAGE_GENERAL_ERROR );
			}
		}
		else
		{
			parent_obj = GetStringObjectInRuntimeObjectsHeap( rt ) ;
		}
		
		prop = AddPropertyInObject( rt , out1_obj , parent_obj , dbresult_direct_prop->query_field_set[col_index].field_name ) ;
		
		field_value = dbresult_direct_prop->query_result_set[ dbresult_direct_prop->col_count * row_index + col_index ] ;
		if( field_value == NULL )
		{
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "set NULL" )
			UnreferObject( rt , prop );
		}
		else if( field_type == CDBC_FIELDTYPE_INT16 )
		{
			int16_t		n = atoi(field_value) ;
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "set int16_t[%"PRIi16"]" , n )
			CallRuntimeFunction_short_SetShortValue( rt , prop , n );
		}
		else if( field_type == CDBC_FIELDTYPE_INT32 )
		{
			int32_t		n = atoi(field_value) ;
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "set int32_t[%"PRIi32"]" , n )
			CallRuntimeFunction_int_SetIntValue( rt , prop , n );
		}
		else if( field_type == CDBC_FIELDTYPE_INT64 )
		{
			int64_t		n = atol(field_value) ;
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "set int64_t[%"PRIi64"]" , n )
			CallRuntimeFunction_long_SetLongValue( rt , prop , n );
		}
		else if( field_type == CDBC_FIELDTYPE_FLOAT )
		{
			float		f = (float)atof(field_value) ;
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "set float[%f]" , f )
			CallRuntimeFunction_float_SetFloatValue( rt , prop , f );
		}
		else if( field_type == CDBC_FIELDTYPE_DOUBLE || field_type == CDBC_FIELDTYPE_DECIMAL )
		{
			double		f = atof(field_value) ;
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "set double[%lf]" , f )
			CallRuntimeFunction_double_SetDoubleValue( rt , prop , f );
		}
		else if( field_type == CDBC_FIELDTYPE_DATE )
		{
			struct tm	*p_tm = (struct tm *)GetObjectDirectProperty( prop ) ;
			memset( p_tm , 0x00 , sizeof(struct tm) );
			strptime( field_value , "%Y-%m-%d" , p_tm );
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "set DATE tm[%s]" , field_value )
		}
		else if( field_type == CDBC_FIELDTYPE_TIME )
		{
			struct tm	*p_tm = (struct tm *)GetObjectDirectProperty( prop ) ;
			memset( p_tm , 0x00 , sizeof(struct tm) );
			strptime( field_value+11 , "%H:%M:%S" , p_tm );
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "set TIME tm[%s]" , field_value )
		}
		else if( field_type == CDBC_FIELDTYPE_DATETIME || field_type == CDBC_FIELDTYPE_TIMESTAMP )
		{
			struct tm	*p_tm = (struct tm *)GetObjectDirectProperty( prop ) ;
			memset( p_tm , 0x00 , sizeof(struct tm) );
			strptime( field_value , "%Y-%m-%d %H:%M:%S" , p_tm );
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "set DATETIME tm[%s]" , field_value )
		}
		else
		{
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "set STRING[%s]" , field_value )
			CallRuntimeFunction_string_SetStringValue( rt , prop , field_value , (int32_t)strlen(field_value) );
		}
	}	
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_dbresult_RowToEntityObject_int_object;
int ZlangInvokeFunction_dbresult_RowToEntityObject_int_object( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_dbresult	*dbresult_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*in1_obj = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject			*in2_obj = GetInputParameterInLocalObjectStack(rt,2) ;
	struct ZlangObject			*out1_obj = GetOutputParameterInLocalObjectStack(rt,1) ;
	int					row_index , row_count ;
	int					col_index , col_count ;
	enum FieldType				field_type ;
	struct ZlangObject			*parent_obj = NULL ;
	char					*field_value = NULL ;
	struct ZlangObject			*prop = NULL ;
	
	CallRuntimeFunction_int_GetIntValue( rt , in1_obj , & row_index );
	row_count = dbresult_direct_prop->row_count ;
	if( row_index < 1 || row_index > row_count )
	{
		CallRuntimeFunction_bool_SetBoolValue( rt , out1_obj , FALSE );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_ILLEGAL_ARGUMENT );
	}
	row_index--;
	
	col_count = dbresult_direct_prop->col_count ;
	for( col_index = 0 ; col_index < col_count ; col_index++ )
	{
		field_type = dbresult_direct_prop->query_field_set[col_index].field_type ;
		if( field_type == CDBC_FIELDTYPE_INT16 )
			parent_obj = GetShortObjectInRuntimeObjectsHeap( rt ) ;
		else if( field_type == CDBC_FIELDTYPE_INT32 )
			parent_obj = GetIntObjectInRuntimeObjectsHeap( rt ) ;
		else if( field_type == CDBC_FIELDTYPE_INT64 )
			parent_obj = GetLongObjectInRuntimeObjectsHeap( rt ) ;
		else if( field_type == CDBC_FIELDTYPE_FLOAT )
			parent_obj = GetFloatObjectInRuntimeObjectsHeap( rt ) ;
		else if( field_type == CDBC_FIELDTYPE_DOUBLE )
			parent_obj = GetDoubleObjectInRuntimeObjectsHeap( rt ) ;
		else if( field_type == CDBC_FIELDTYPE_DECIMAL )
			parent_obj = GetDoubleObjectInRuntimeObjectsHeap( rt ) ;
		else if( field_type == CDBC_FIELDTYPE_DATE || field_type == CDBC_FIELDTYPE_TIME || field_type == CDBC_FIELDTYPE_DATETIME || field_type == CDBC_FIELDTYPE_TIMESTAMP )
			parent_obj = QueryGlobalObjectByObjectName( rt , "datetime" ) ;
		else
			parent_obj = GetStringObjectInRuntimeObjectsHeap( rt ) ;
		
		prop = QueryPropertyInObjectByPropertyName( rt , in2_obj , dbresult_direct_prop->query_field_set[col_index].field_name ) ;
		if( IsTypeOf( rt , prop , parent_obj ) == 0 )
		{
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "property[%s] type not matched" , dbresult_direct_prop->query_field_set[col_index].field_name )
			return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_PROPERTY_TYPE_NOT_MATCHED );
		}
		
		field_value = dbresult_direct_prop->query_result_set[ dbresult_direct_prop->col_count * row_index + col_index ] ;
		if( field_value == NULL )
		{
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "set NULL" )
			UnreferObject( rt , prop );
		}
		else if( field_type == CDBC_FIELDTYPE_INT16 )
		{
			int16_t		n = atoi(field_value) ;
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "set int16_t[%"PRIi16"]" , n )
			CallRuntimeFunction_short_SetShortValue( rt , prop , n );
		}
		else if( field_type == CDBC_FIELDTYPE_INT32 )
		{
			int32_t		n = atoi(field_value) ;
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "set int32_t[%"PRIi32"]" , n )
			CallRuntimeFunction_int_SetIntValue( rt , prop , n );
		}
		else if( field_type == CDBC_FIELDTYPE_INT64 )
		{
			int64_t		n = atol(field_value) ;
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "set int64_t[%"PRIi64"]" , n )
			CallRuntimeFunction_long_SetLongValue( rt , prop , n );
		}
		else if( field_type == CDBC_FIELDTYPE_FLOAT )
		{
			float		f = (float)atof(field_value) ;
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "set float[%f]" , f )
			CallRuntimeFunction_float_SetFloatValue( rt , prop , f );
		}
		else if( field_type == CDBC_FIELDTYPE_DOUBLE || field_type == CDBC_FIELDTYPE_DECIMAL )
		{
			double		f = atof(field_value) ;
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "set double[%lf]" , f )
			CallRuntimeFunction_double_SetDoubleValue( rt , prop , f );
		}
		else if( field_type == CDBC_FIELDTYPE_DATE )
		{
			struct tm	*p_tm = (struct tm *)GetObjectDirectProperty( prop ) ;
			memset( p_tm , 0x00 , sizeof(struct tm) );
			strptime( field_value , "%Y-%m-%d" , p_tm );
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "set DATE tm[%s]" , field_value )
		}
		else if( field_type == CDBC_FIELDTYPE_TIME )
		{
			struct tm	*p_tm = (struct tm *)GetObjectDirectProperty( prop ) ;
			memset( p_tm , 0x00 , sizeof(struct tm) );
			strptime( field_value , "%H:%M:%S" , p_tm );
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "set TIME tm[%s]" , field_value )
		}
		else if( field_type == CDBC_FIELDTYPE_DATETIME || field_type == CDBC_FIELDTYPE_TIMESTAMP )
		{
			struct tm	*p_tm = (struct tm *)GetObjectDirectProperty( prop ) ;
			memset( p_tm , 0x00 , sizeof(struct tm) );
			strptime( field_value , "%Y-%m-%d %H:%M:%S" , p_tm );
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "set DATETIME tm[%s]" , field_value )
		}
		else
		{
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "set STRING[%s]" , field_value )
			CallRuntimeFunction_string_SetStringValue( rt , prop , field_value , (int32_t)strlen(field_value) );
		}
	}
	
	CallRuntimeFunction_bool_SetBoolValue( rt , out1_obj , TRUE );
	return 0;
}

ZlangCreateDirectPropertyFunction ZlangCreateDirectProperty_dbresult;
void *ZlangCreateDirectProperty_dbresult( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_dbresult	*dbresult_prop = NULL ;
	
	dbresult_prop = (struct ZlangDirectProperty_dbresult *)ZLMALLOC( sizeof(struct ZlangDirectProperty_dbresult) ) ;
	if( dbresult_prop == NULL )
	{
		SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_ALLOC , "alloc memory for entity" )
		return NULL;
	}
	memset( dbresult_prop , 0x00 , sizeof(struct ZlangDirectProperty_dbresult) );
	
	return dbresult_prop;
}

ZlangDestroyDirectPropertyFunction ZlangDestroyDirectProperty_dbresult;
void ZlangDestroyDirectProperty_dbresult( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_dbresult	*dbresult_direct_prop = GetObjectDirectProperty(obj) ;
	
	if( dbresult_direct_prop->query_field_set || dbresult_direct_prop->query_result_set )
	{
		DBCFreeSqlResult( & (dbresult_direct_prop->query_field_set) , & (dbresult_direct_prop->query_result_set) );
	}
	
	ZLFREE( dbresult_direct_prop );
	
	return;
}

ZlangSummarizeDirectPropertySizeFunction ZlangSummarizeDirectPropertySize_dbresult;
void ZlangSummarizeDirectPropertySize_dbresult( struct ZlangRuntime *rt , struct ZlangObject *obj , size_t *summarized_obj_size , size_t *summarized_direct_prop_size )
{
	struct ZlangDirectProperty_dbresult	*dbresult_direct_prop = GetObjectDirectProperty(obj) ;
	int					col_index ;
	int					row_index ;
	int					set_index ;
	
	if( dbresult_direct_prop->query_field_set )
	{
		for( col_index = 0 ; col_index < dbresult_direct_prop->col_count ; col_index++ )
		{
			// set_index = col_index ;
			SUMMARIZE_SIZE( summarized_direct_prop_size , strlen(dbresult_direct_prop->query_field_set[col_index].field_name)+1 )
			
		}
		SUMMARIZE_SIZE( summarized_direct_prop_size , sizeof(struct FieldInfo)*(1+dbresult_direct_prop->col_count) )
	}
	
	if( dbresult_direct_prop->query_result_set )
	{
		set_index = 0 ;
		for( row_index = 0 ; row_index < dbresult_direct_prop->row_count ; row_index++ )
		{
			for( col_index = 0 ; col_index < dbresult_direct_prop->col_count ; col_index++ , set_index++ )
			{
				// set_index = 1 + dbresult_direct_prop->col_count*row_index + col_index ;
				SUMMARIZE_SIZE( summarized_direct_prop_size , strlen(dbresult_direct_prop->query_result_set[set_index])+1 )
			}
		}
		SUMMARIZE_SIZE( summarized_direct_prop_size , sizeof(char*)*(1+dbresult_direct_prop->col_count*dbresult_direct_prop->row_count) )
	}
	
	SUMMARIZE_SIZE( summarized_direct_prop_size , sizeof(struct ZlangDirectProperty_dbresult) )
	
	return;
}

struct ZlangDirectFunctions direct_funcs_dbresult =
	{
		ZLANG_OBJECT_dbresult , /* char *ancestor_name */
		
		ZlangCreateDirectProperty_dbresult , /* ZlangCreateDirectPropertyFunction *create_entity_func */
		ZlangDestroyDirectProperty_dbresult , /* ZlangDestroyDirectPropertyFunction *destroy_entity_func */
		
		NULL , /* ZlangFromCharPtrFunction *from_char_ptr_func */
		NULL , /* ZlangToStringFunction *to_string_func */
		NULL , /* ZlangFromDataPtrFunction *from_data_ptr_func */
		NULL , /* ZlangGetDataPtrFunction *get_data_ptr_func */
		
		NULL , /* ZlangOperatorFunction *oper_PLUS_func */
		NULL , /* ZlangOperatorFunction *oper_MINUS_func */
		NULL , /* ZlangOperatorFunction *oper_MUL_func */
		NULL , /* ZlangOperatorFunction *oper_DIV_func */
		NULL , /* ZlangOperatorFunction *oper_MOD_func */
		
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_NEGATIVE_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_NOT_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_BIT_REVERSE_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_PLUS_PLUS_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_MINUS_MINUS_func */
		
		NULL , /* ZlangCompareFunction *comp_EGUAL_func */
		NULL , /* ZlangCompareFunction *comp_NOTEGUAL_func */
		NULL , /* ZlangCompareFunction *comp_LT_func */
		NULL , /* ZlangCompareFunction *comp_LE_func */
		NULL , /* ZlangCompareFunction *comp_GT_func */
		NULL , /* ZlangCompareFunction *comp_GE_func */
		
		NULL , /* ZlangLogicFunction *logic_AND_func */
		NULL , /* ZlangLogicFunction *logic_OR_func */
		
		NULL , /* ZlangLogicFunction *bit_AND_func */
		NULL , /* ZlangLogicFunction *bit_XOR_func */
		NULL , /* ZlangLogicFunction *bit_OR_func */
		NULL , /* ZlangLogicFunction *bit_MOVELEFT_func */
		NULL , /* ZlangLogicFunction *bit_MOVERIGHT_func */
		
		ZlangSummarizeDirectPropertySize_dbresult , /* ZlangSummarizeDirectPropertySizeFunction *summarize_direct_prop_size_func */
	} ;

ZlangImportObjectFunction ZlangImportObject_dbresult;
struct ZlangObject *ZlangImportObject_dbresult( struct ZlangRuntime *rt )
{
	struct ZlangObject	*obj = NULL ;
	struct ZlangFunction	*func = NULL ;
	int			nret = 0 ;
	
	nret = ImportObject( rt , & obj , ZLANG_OBJECT_dbresult , & direct_funcs_dbresult , sizeof(struct ZlangDirectFunctions) , NULL ) ;
	if( nret )
	{
		SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_LINK_FUNC_TO_ENTITY , "import object to global objects heap" )
		return NULL;
	}
	
	/* dbresult.NotFound() */
	func = AddFunctionAndParametersInObject( rt , obj , "NotFound" , "NotFound()" , ZlangInvokeFunction_dbresult_NotFound , ZLANG_OBJECT_bool , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* dbresult.Found() */
	func = AddFunctionAndParametersInObject( rt , obj , "Found" , "Found()" , ZlangInvokeFunction_dbresult_Found , ZLANG_OBJECT_bool , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* dbresult.GetRowCount() */
	func = AddFunctionAndParametersInObject( rt , obj , "GetRowCount" , "GetRowCount()" , ZlangInvokeFunction_dbresult_GetRowCount , ZLANG_OBJECT_int , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* dbresult.GetColumnCount() */
	func = AddFunctionAndParametersInObject( rt , obj , "GetColumnCount" , "GetColumnCount()" , ZlangInvokeFunction_dbresult_GetColumnCount , ZLANG_OBJECT_int , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* dbresult.GetFieldName(int) */
	func = AddFunctionAndParametersInObject( rt , obj , "GetFieldName" , "GetFieldName(int)" , ZlangInvokeFunction_dbresult_GetFieldName_int , ZLANG_OBJECT_string , ZLANG_OBJECT_int,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* dbresult.GetFieldType(int) */
	func = AddFunctionAndParametersInObject( rt , obj , "GetFieldType" , "GetFieldType(int)" , ZlangInvokeFunction_dbresult_GetFieldType_int , ZLANG_OBJECT_int , ZLANG_OBJECT_int,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* dbresult.GetFieldLength(int) */
	func = AddFunctionAndParametersInObject( rt , obj , "GetFieldLength" , "GetFieldLength(int)" , ZlangInvokeFunction_dbresult_GetFieldLength_int , ZLANG_OBJECT_int , ZLANG_OBJECT_int,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* dbresult.GetFieldDecimalLength(int) */
	func = AddFunctionAndParametersInObject( rt , obj , "GetFieldDecimalLength" , "GetFieldDecimalLength(int)" , ZlangInvokeFunction_dbresult_GetFieldDecimalLength_int , ZLANG_OBJECT_int , ZLANG_OBJECT_int,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* dbresult.GetFieldValue(int,int) */
	func = AddFunctionAndParametersInObject( rt , obj , "GetFieldValue" , "GetFieldValue(int,int)" , ZlangInvokeFunction_dbresult_GetFieldValue_int_int , ZLANG_OBJECT_string , ZLANG_OBJECT_int,NULL , ZLANG_OBJECT_int,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* dbresult.GetAffectedRows() */
	func = AddFunctionAndParametersInObject( rt , obj , "GetAffectedRows" , "GetAffectedRows()" , ZlangInvokeFunction_dbresult_GetAffectedRows , ZLANG_OBJECT_int , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* dbresult.RowToObject(int) */
	func = AddFunctionAndParametersInObject( rt , obj , "RowToObject" , "RowToObject(int)" , ZlangInvokeFunction_dbresult_RowToObject_int , ZLANG_OBJECT_object , ZLANG_OBJECT_int,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* dbresult.RowToEntityObject(int,object) */
	func = AddFunctionAndParametersInObject( rt , obj , "RowToEntityObject" , "RowToEntityObject(int,object)" , ZlangInvokeFunction_dbresult_RowToEntityObject_int_object , ZLANG_OBJECT_bool , ZLANG_OBJECT_int,NULL , ZLANG_OBJECT_object,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	return obj ;
}

