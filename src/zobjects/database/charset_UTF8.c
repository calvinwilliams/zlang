﻿/* Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

static struct ZlangCharsetAlias		g_zlang_charset_aliases_UTF8[] = {
		{ ZLANG_CHARSET_UTF8 , "数据库" , "database" } ,
		{ ZLANG_CHARSET_UTF8 , "连接" , "Connect" } ,
		{ ZLANG_CHARSET_UTF8 , "得到最后错误" , "GetLastError" } ,
		{ ZLANG_CHARSET_UTF8 , "数据库会话" , "dbsession" } ,
		{ ZLANG_CHARSET_UTF8 , "执行SQL" , "Execute" } ,
		{ ZLANG_CHARSET_UTF8 , "设置自动提交" , "AutoCommit" } ,
		{ ZLANG_CHARSET_UTF8 , "开始事务" , "Begin" } ,
		{ ZLANG_CHARSET_UTF8 , "提交事务" , "Commit" } ,
		{ ZLANG_CHARSET_UTF8 , "回滚事务" , "Rollback" } ,
		{ ZLANG_CHARSET_UTF8 , "断开" , "Disconnect" } ,
		{ ZLANG_CHARSET_UTF8 , "数据库处理结果" , "dbresult" } ,
		{ ZLANG_CHARSET_UTF8 , "未找到记录" , "NotFound" } ,
		{ ZLANG_CHARSET_UTF8 , "得到行数" , "GetRowCount" } ,
		{ ZLANG_CHARSET_UTF8 , "得到列数" , "GetColumnCount" } ,
		{ ZLANG_CHARSET_UTF8 , "得到字段名" , "GetFieldName" } ,
		{ ZLANG_CHARSET_UTF8 , "得到字段类型" , "GetFieldType" } ,
		{ ZLANG_CHARSET_UTF8 , "得到字段长度" , "GetFieldLength" } ,
		{ ZLANG_CHARSET_UTF8 , "得到字段精度" , "GetFieldDecimalLength" } ,
		{ ZLANG_CHARSET_UTF8 , "得到字段值" , "GetFieldValue" } ,
		{ ZLANG_CHARSET_UTF8 , "得到受影响行数" , "GetAffectedRows" } ,
		{ ZLANG_CHARSET_UTF8 , "记录转对象" , "RowToObject" } ,
		{ ZLANG_CHARSET_UTF8 , "记录转实体对象" , "RowToEntityObject" } ,
		{ ZLANG_CHARSET_UTF8 , "设置连接池最小空闲连接数" , "SetMinIdleSessionsCount" } ,
		{ ZLANG_CHARSET_UTF8 , "设置连接池最大空闲连接数" , "SetMaxIdleSessionsCount" } ,
		{ ZLANG_CHARSET_UTF8 , "设置连接池最大连接数" , "SetMaxSessionsCount" } ,
		{ ZLANG_CHARSET_UTF8 , "设置连接池最大空闲存活周期" , "SetMaxIdleTimeval" } ,
		{ ZLANG_CHARSET_UTF8 , "设置连接池观察空闲周期" , "SetWatchIdleTimeval" } ,
		{ ZLANG_CHARSET_UTF8 , "设置连接池激活周期" , "SetInspectTimeval" } ,
		{ 0 , NULL , NULL } ,
	} ;

