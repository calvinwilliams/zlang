/* Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "zobjects_log.h"

ZlangInvokeFunction ZlangInvokeFunction_log_SetOutput_int_string;
int ZlangInvokeFunction_log_SetOutput_int_string( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_log	*log_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject		*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject		*in2 = GetInputParameterInLocalObjectStack(rt,2) ;
	struct ZlangObject		*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	int32_t				log_output ;
	char				*log_pathfilename = NULL ;
	int32_t				nret = 0 ;
	
	if( in1 == NULL || in2 == NULL || out1 == NULL )
		return ZLANG_ERROR_PARAMETER;
	
	CallRuntimeFunction_int_GetIntValue( rt , in1 , & log_output );
	CallRuntimeFunction_string_GetStringValue( rt , in2 , & log_pathfilename , NULL );
	nret = SetLogOutput( log_direct_prop->g , log_output , log_pathfilename , LOG_NO_OUTPUTFUNC ) ;
	CallRuntimeFunction_int_SetIntValue( rt , out1 , nret );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_log_SetLevel_int;
int ZlangInvokeFunction_log_SetLevel_int( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_log	*log_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject		*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject		*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	int32_t				log_level ;
	int32_t				nret = 0 ;
	
	if( in1 == NULL || out1 == NULL )
		return ZLANG_ERROR_PARAMETER;
	
	CallRuntimeFunction_int_GetIntValue( rt , in1 , & log_level );
	nret = SetLogLevel( log_direct_prop->g , log_level ) ;
	CallRuntimeFunction_int_SetIntValue( rt , out1 , nret );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_log_SetStyles_int;
int ZlangInvokeFunction_log_SetStyles_int( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_log	*log_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject		*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject		*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	int32_t				log_styles ;
	int32_t				nret = 0 ;
	
	if( in1 == NULL || out1 == NULL )
		return ZLANG_ERROR_PARAMETER;
	
	CallRuntimeFunction_int_GetIntValue( rt , in1 , & log_styles );
	nret = SetLogStyles( log_direct_prop->g , log_styles , LOG_NO_STYLEFUNC ) ;
	CallRuntimeFunction_int_SetIntValue( rt , out1 , nret );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_log_SetOptions_int;
int ZlangInvokeFunction_log_SetOptions_int( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_log	*log_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject		*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject		*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	int32_t				log_options ;
	int32_t				nret = 0 ;
	
	if( in1 == NULL || out1 == NULL )
		return ZLANG_ERROR_PARAMETER;
	
	CallRuntimeFunction_int_GetIntValue( rt , in1 , & log_options );
	nret = SetLogOptions( log_direct_prop->g , log_options ) ;
	CallRuntimeFunction_int_SetIntValue( rt , out1 , nret );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_log_SetRotateMode_int;
int ZlangInvokeFunction_log_SetRotateMode_int( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_log	*log_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject		*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject		*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	int32_t				log_rotate_mode ;
	int32_t				nret = 0 ;
	
	if( in1 == NULL || out1 == NULL )
		return ZLANG_ERROR_PARAMETER;
	
	CallRuntimeFunction_int_GetIntValue( rt , in1 , & log_rotate_mode );
	nret = SetLogRotateMode( log_direct_prop->g , log_rotate_mode ) ;
	CallRuntimeFunction_int_SetIntValue( rt , out1 , nret );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_log_SetRotateSize_int;
int ZlangInvokeFunction_log_SetRotateSize_int( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_log	*log_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject		*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject		*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	int32_t				log_rotate_size ;
	int32_t				nret = 0 ;
	
	if( in1 == NULL || out1 == NULL )
		return ZLANG_ERROR_PARAMETER;
	
	CallRuntimeFunction_int_GetIntValue( rt , in1 , & log_rotate_size );
	nret = SetLogRotateSize( log_direct_prop->g , log_rotate_size ) ;
	CallRuntimeFunction_int_SetIntValue( rt , out1 , nret );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_log_Debug_vargs;
int ZlangInvokeFunction_log_Debug_vargs( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_log	*log_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject		*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject		*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject		*buf_obj = NULL ;
	char				*str = NULL ;
	int32_t				str_len ;
	int32_t				nret = 0 ;
	
	if( in1 == NULL || out1 == NULL )
		return ZLANG_ERROR_PARAMETER;
	
	buf_obj = CloneStringObjectInTmpStack( rt , NULL ) ;
	if( buf_obj == NULL )
		return GetRuntimeErrorNo(rt);
	
	nret = CallRuntimeFunction_string_AppendFormatFromArgsStack( rt , buf_obj ) ;
	if( nret )
		return nret;
	GetDataPtr( rt , buf_obj , (void**) & str , & str_len );
	
	nret = WriteDebugLog( log_direct_prop->g , GetRuntimeSourceFilename(rt) , GetRuntimeSourceRow(rt) , "%.*s" , str_len,str ) ;
	CallRuntimeFunction_int_SetIntValue( rt , out1 , nret );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_log_Info_vargs;
int ZlangInvokeFunction_log_Info_vargs( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_log	*log_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject		*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject		*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject		*buf_obj = NULL ;
	char				*str = NULL ;
	int32_t				str_len ;
	int32_t				nret = 0 ;
	
	if( in1 == NULL || out1 == NULL )
		return ZLANG_ERROR_PARAMETER;
	
	buf_obj = CloneStringObjectInTmpStack( rt , NULL ) ;
	if( buf_obj == NULL )
		return GetRuntimeErrorNo(rt);
	
	nret = CallRuntimeFunction_string_AppendFormatFromArgsStack( rt , buf_obj ) ;
	if( nret )
		return nret;
	GetDataPtr( rt , buf_obj , (void**) & str , & str_len );
	
	nret = WriteInfoLog( log_direct_prop->g , GetRuntimeSourceFilename(rt) , GetRuntimeSourceRow(rt) , "%.*s" , str_len,str ) ;
	CallRuntimeFunction_int_SetIntValue( rt , out1 , nret );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_log_Notice_vargs;
int ZlangInvokeFunction_log_Notice_vargs( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_log	*log_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject		*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject		*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject		*buf_obj = NULL ;
	char				*str = NULL ;
	int32_t				str_len ;
	int32_t				nret = 0 ;
	
	if( in1 == NULL || out1 == NULL )
		return ZLANG_ERROR_PARAMETER;
	
	buf_obj = CloneStringObjectInTmpStack( rt , NULL ) ;
	if( buf_obj == NULL )
		return GetRuntimeErrorNo(rt);
	
	nret = CallRuntimeFunction_string_AppendFormatFromArgsStack( rt , buf_obj ) ;
	if( nret )
		return nret;
	GetDataPtr( rt , buf_obj , (void**) & str , & str_len );
	
	nret = WriteNoticeLog( log_direct_prop->g , GetRuntimeSourceFilename(rt) , GetRuntimeSourceRow(rt) , "%.*s" , str_len,str ) ;
	CallRuntimeFunction_int_SetIntValue( rt , out1 , nret );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_log_Warn_vargs;
int ZlangInvokeFunction_log_Warn_vargs( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_log	*log_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject		*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject		*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject		*buf_obj = NULL ;
	char				*str = NULL ;
	int32_t				str_len ;
	int32_t				nret = 0 ;
	
	if( in1 == NULL || out1 == NULL )
		return ZLANG_ERROR_PARAMETER;
	
	buf_obj = CloneStringObjectInTmpStack( rt , NULL ) ;
	if( buf_obj == NULL )
		return GetRuntimeErrorNo(rt);
	
	nret = CallRuntimeFunction_string_AppendFormatFromArgsStack( rt , buf_obj ) ;
	if( nret )
		return nret;
	GetDataPtr( rt , buf_obj , (void**) & str , & str_len );
	
	nret = WriteWarnLog( log_direct_prop->g , GetRuntimeSourceFilename(rt) , GetRuntimeSourceRow(rt) , "%.*s" , str_len,str ) ;
	CallRuntimeFunction_int_SetIntValue( rt , out1 , nret );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_log_Error_vargs;
int ZlangInvokeFunction_log_Error_vargs( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_log	*log_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject		*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject		*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject		*buf_obj = NULL ;
	char				*str = NULL ;
	int32_t				str_len ;
	int32_t				nret = 0 ;
	
	if( in1 == NULL || out1 == NULL )
		return ZLANG_ERROR_PARAMETER;
	
	buf_obj = CloneStringObjectInTmpStack( rt , NULL ) ;
	if( buf_obj == NULL )
		return GetRuntimeErrorNo(rt);
	
	nret = CallRuntimeFunction_string_AppendFormatFromArgsStack( rt , buf_obj ) ;
	if( nret )
		return nret;
	GetDataPtr( rt , buf_obj , (void**) & str , & str_len );
	
	nret = WriteErrorLog( log_direct_prop->g , GetRuntimeSourceFilename(rt) , GetRuntimeSourceRow(rt) , "%.*s" , str_len,str ) ;
	CallRuntimeFunction_int_SetIntValue( rt , out1 , nret );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_log_Fatal_vargs;
int ZlangInvokeFunction_log_Fatal_vargs( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_log	*log_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject		*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject		*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject		*buf_obj = NULL ;
	char				*str = NULL ;
	int32_t				str_len ;
	int32_t				nret = 0 ;
	
	if( in1 == NULL || out1 == NULL )
		return ZLANG_ERROR_PARAMETER;
	
	buf_obj = CloneStringObjectInTmpStack( rt , NULL ) ;
	if( buf_obj == NULL )
		return GetRuntimeErrorNo(rt);
	
	nret = CallRuntimeFunction_string_AppendFormatFromArgsStack( rt , buf_obj ) ;
	if( nret )
		return nret;
	GetDataPtr( rt , buf_obj , (void**) & str , & str_len );
	
	nret = WriteFatalLog( log_direct_prop->g , GetRuntimeSourceFilename(rt) , GetRuntimeSourceRow(rt) , "%.*s" , str_len,str ) ;
	CallRuntimeFunction_int_SetIntValue( rt , out1 , nret );
	
	return 0;
}

ZlangCreateDirectPropertyFunction ZlangCreateDirectProperty_log;
void *ZlangCreateDirectProperty_log( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_log	*log_prop = NULL ;
	
	log_prop = (struct ZlangDirectProperty_log *)ZLMALLOC( sizeof(struct ZlangDirectProperty_log) ) ;
	if( log_prop == NULL )
	{
		SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_ALLOC , "alloc memory for entity" )
		return NULL;
	}
	memset( log_prop , 0x00 , sizeof(struct ZlangDirectProperty_log) );
	
	log_prop->g = CreateLogHandle() ;
	if( log_prop->g == NULL )
	{
		SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_ALLOC , "alloc memory for ->log" )
		return NULL;
	}
	log_prop->no_need_for_destroy_log_handle = 0 ;
	
	return log_prop;
}

ZlangDestroyDirectPropertyFunction ZlangDestroyDirectProperty_log;
void ZlangDestroyDirectProperty_log( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_log	*log_direct_prop = GetObjectDirectProperty(obj) ;
	
	if( log_direct_prop->no_need_for_destroy_log_handle == 0 )
	{
		DestroyLogHandle( log_direct_prop->g );
	}
	
	ZLFREE( log_direct_prop );
	
	return;
}

ZlangSummarizeDirectPropertySizeFunction ZlangSummarizeDirectPropertySize_log;
void ZlangSummarizeDirectPropertySize_log( struct ZlangRuntime *rt , struct ZlangObject *obj , size_t *summarized_obj_size , size_t *summarized_direct_prop_size )
{
	struct ZlangDirectProperty_log	*log_direct_prop = GetObjectDirectProperty(obj) ;
	
	if( log_direct_prop->g )
		SUMMARIZE_SIZE( summarized_direct_prop_size , sizeof(LOG) )
	
	SUMMARIZE_SIZE( summarized_direct_prop_size , sizeof(struct ZlangDirectProperty_log) )
	
	return;
}

static struct ZlangDirectFunctions direct_funcs_log =
	{
		ZLANG_OBJECT_log , /* char *ancestor_name */
		
		ZlangCreateDirectProperty_log , /* ZlangCreateDirectPropertyFunction *create_entity_func */
		ZlangDestroyDirectProperty_log , /* ZlangDestroyDirectPropertyFunction *destroy_entity_func */
		
		NULL , /* ZlangFromCharPtrFunction *from_char_ptr_func */
		NULL , /* ZlangToStringFunction *to_string_func */
		NULL , /* ZlangFromDataPtrFunction *from_data_ptr_func */
		NULL , /* ZlangGetDataPtrFunction *get_data_ptr_func */
		
		NULL , /* ZlangOperatorFunction *oper_PLUS_func */
		NULL , /* ZlangOperatorFunction *oper_MINUS_func */
		NULL , /* ZlangOperatorFunction *oper_MUL_func */
		NULL , /* ZlangOperatorFunction *oper_DIV_func */
		NULL , /* ZlangOperatorFunction *oper_MOD_func */
		
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_NEGATIVE_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_NOT_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_BIT_REVERSE_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_PLUS_PLUS_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_MINUS_MINUS_func */
		
		NULL , /* ZlangCompareFunction *comp_EGUAL_func */
		NULL , /* ZlangCompareFunction *comp_NOTEGUAL_func */
		NULL , /* ZlangCompareFunction *comp_LT_func */
		NULL , /* ZlangCompareFunction *comp_LE_func */
		NULL , /* ZlangCompareFunction *comp_GT_func */
		NULL , /* ZlangCompareFunction *comp_GE_func */
		
		NULL , /* ZlangLogicFunction *logic_AND_func */
		NULL , /* ZlangLogicFunction *logic_OR_func */
		
		NULL , /* ZlangLogicFunction *bit_AND_func */
		NULL , /* ZlangLogicFunction *bit_XOR_func */
		NULL , /* ZlangLogicFunction *bit_OR_func */
		NULL , /* ZlangLogicFunction *bit_MOVELEFT_func */
		NULL , /* ZlangLogicFunction *bit_MOVERIGHT_func */
		
		ZlangSummarizeDirectPropertySize_log , /* ZlangSummarizeDirectPropertySizeFunction *summarize_direct_prop_size_func */
	} ;

ZlangImportObjectFunction ZlangImportObject_log;
struct ZlangObject *ZlangImportObject_log( struct ZlangRuntime *rt )
{
	struct ZlangObject	*obj = NULL ;
	struct ZlangObject	*prop = NULL ;
	struct ZlangFunction	*func = NULL ;
	int			nret = 0 ;
	
	nret = ImportObject( rt , & obj , ZLANG_OBJECT_log , & direct_funcs_log , sizeof(struct ZlangDirectFunctions) , NULL ) ;
	if( nret )
	{
		SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_LINK_FUNC_TO_ENTITY , "import object to global objects heap" )
		return NULL;
	}
	
	/* int log.OUTPUT_INVALID */
	prop = AddPropertyInObject( rt , obj , QueryObjectByObjectName(rt,"int") , "OUTPUT_INVALID") ;
	if( prop == NULL )
		return NULL;
	CallRuntimeFunction_int_SetIntValue( rt , prop , -1 );
	SetConstantObject( prop );
	
	/* int log.OUTPUT_STDOUT */
	prop = AddPropertyInObject( rt , obj , QueryObjectByObjectName(rt,"int") , "OUTPUT_STDOUT") ;
	if( prop == NULL )
		return NULL;
	CallRuntimeFunction_int_SetIntValue( rt , prop , 1 );
	SetConstantObject( prop );
	
	/* int log.OUTPUT_STDERR */
	prop = AddPropertyInObject( rt , obj , QueryObjectByObjectName(rt,"int") , "OUTPUT_STDERR") ;
	if( prop == NULL )
		return NULL;
	CallRuntimeFunction_int_SetIntValue( rt , prop , 2 );
	SetConstantObject( prop );
	
	/* int log.OUTPUT_SYSLOG */
	prop = AddPropertyInObject( rt , obj , QueryObjectByObjectName(rt,"int") , "OUTPUT_SYSLOG") ;
	if( prop == NULL )
		return NULL;
	CallRuntimeFunction_int_SetIntValue( rt , prop , 3 );
	SetConstantObject( prop );
	
	/* int log.OUTPUT_FILE */
	prop = AddPropertyInObject( rt , obj , QueryObjectByObjectName(rt,"int") , "OUTPUT_FILE") ;
	if( prop == NULL )
		return NULL;
	CallRuntimeFunction_int_SetIntValue( rt , prop , 11 );
	SetConstantObject( prop );
	
	/* int log.LEVEL_DEBUG */
	prop = AddPropertyInObject( rt , obj , QueryObjectByObjectName(rt,"int") , "LEVEL_DEBUG") ;
	if( prop == NULL )
		return NULL;
	CallRuntimeFunction_int_SetIntValue( rt , prop , 0 );
	SetConstantObject( prop );
	
	/* int log.LEVEL_INFO */
	prop = AddPropertyInObject( rt , obj , QueryObjectByObjectName(rt,"int") , "LEVEL_INFO") ;
	if( prop == NULL )
		return NULL;
	CallRuntimeFunction_int_SetIntValue( rt , prop , 1 );
	SetConstantObject( prop );
	
	/* int log.LEVEL_NOTICE */
	prop = AddPropertyInObject( rt , obj , QueryObjectByObjectName(rt,"int") , "LEVEL_NOTICE") ;
	if( prop == NULL )
		return NULL;
	CallRuntimeFunction_int_SetIntValue( rt , prop , 2 );
	SetConstantObject( prop );
	
	/* int log.LEVEL_WARN */
	prop = AddPropertyInObject( rt , obj , QueryObjectByObjectName(rt,"int") , "LEVEL_WARN") ;
	if( prop == NULL )
		return NULL;
	CallRuntimeFunction_int_SetIntValue( rt , prop , 3 );
	SetConstantObject( prop );
	
	/* int log.LEVEL_ERROR */
	prop = AddPropertyInObject( rt , obj , QueryObjectByObjectName(rt,"int") , "LEVEL_ERROR") ;
	if( prop == NULL )
		return NULL;
	CallRuntimeFunction_int_SetIntValue( rt , prop , 4 );
	SetConstantObject( prop );
	
	/* int log.LEVEL_FATAL */
	prop = AddPropertyInObject( rt , obj , QueryObjectByObjectName(rt,"int") , "LEVEL_FATAL") ;
	if( prop == NULL )
		return NULL;
	CallRuntimeFunction_int_SetIntValue( rt , prop , 5 );
	SetConstantObject( prop );
	
	/* int log.LEVEL_NOLOG */
	prop = AddPropertyInObject( rt , obj , QueryObjectByObjectName(rt,"int") , "LEVEL_NOLOG") ;
	if( prop == NULL )
		return NULL;
	CallRuntimeFunction_int_SetIntValue( rt , prop , 6 );
	SetConstantObject( prop );
	
	/* int log.STYLE_DATE */
	prop = AddPropertyInObject( rt , obj , QueryObjectByObjectName(rt,"int") , "STYLE_DATE") ;
	if( prop == NULL )
		return NULL;
	CallRuntimeFunction_int_SetIntValue( rt , prop , 0x00001 );
	SetConstantObject( prop );
	
	/* int log.STYLE_DATETIME */
	prop = AddPropertyInObject( rt , obj , QueryObjectByObjectName(rt,"int") , "STYLE_DATETIME") ;
	if( prop == NULL )
		return NULL;
	CallRuntimeFunction_int_SetIntValue( rt , prop , 0x00002 );
	SetConstantObject( prop );
	
	/* int log.STYLE_DATETIMEMS */
	prop = AddPropertyInObject( rt , obj , QueryObjectByObjectName(rt,"int") , "STYLE_DATETIMEMS") ;
	if( prop == NULL )
		return NULL;
	CallRuntimeFunction_int_SetIntValue( rt , prop , 0x00004 );
	SetConstantObject( prop );
	
	/* int log.STYLE_LOGLEVEL */
	prop = AddPropertyInObject( rt , obj , QueryObjectByObjectName(rt,"int") , "STYLE_LOGLEVEL") ;
	if( prop == NULL )
		return NULL;
	CallRuntimeFunction_int_SetIntValue( rt , prop , 0x00008 );
	SetConstantObject( prop );
	
	/* int log.STYLE_PID */
	prop = AddPropertyInObject( rt , obj , QueryObjectByObjectName(rt,"int") , "STYLE_PID") ;
	if( prop == NULL )
		return NULL;
	CallRuntimeFunction_int_SetIntValue( rt , prop , 0x00010 );
	SetConstantObject( prop );
	
	/* int log.STYLE_TID */
	prop = AddPropertyInObject( rt , obj , QueryObjectByObjectName(rt,"int") , "STYLE_TID") ;
	if( prop == NULL )
		return NULL;
	CallRuntimeFunction_int_SetIntValue( rt , prop , 0x00020 );
	SetConstantObject( prop );
	
	/* int log.STYLE_SOURCE */
	prop = AddPropertyInObject( rt , obj , QueryObjectByObjectName(rt,"int") , "STYLE_SOURCE") ;
	if( prop == NULL )
		return NULL;
	CallRuntimeFunction_int_SetIntValue( rt , prop , 0x00040 );
	SetConstantObject( prop );
	
	/* int log.STYLE_FORMAT */
	prop = AddPropertyInObject( rt , obj , QueryObjectByObjectName(rt,"int") , "STYLE_FORMAT") ;
	if( prop == NULL )
		return NULL;
	CallRuntimeFunction_int_SetIntValue( rt , prop , 0x00080 );
	SetConstantObject( prop );
	
	/* int log.STYLE_NEWLINE */
	prop = AddPropertyInObject( rt , obj , QueryObjectByObjectName(rt,"int") , "STYLE_NEWLINE") ;
	if( prop == NULL )
		return NULL;
	CallRuntimeFunction_int_SetIntValue( rt , prop , 0x00100 );
	SetConstantObject( prop );
	
	/* int log.OPTION_OPEN_AND_CLOSE */
	prop = AddPropertyInObject( rt , obj , QueryObjectByObjectName(rt,"int") , "OPTION_OPEN_AND_CLOSE") ;
	if( prop == NULL )
		return NULL;
	CallRuntimeFunction_int_SetIntValue( rt , prop , 1 );
	SetConstantObject( prop );
	
	/* int log.OPTION_CHANGE_TEST */
	prop = AddPropertyInObject( rt , obj , QueryObjectByObjectName(rt,"int") , "OPTION_CHANGE_TEST") ;
	if( prop == NULL )
		return NULL;
	CallRuntimeFunction_int_SetIntValue( rt , prop , 2 );
	SetConstantObject( prop );
	
	/* int log.OPTION_OPEN_ONCE */
	prop = AddPropertyInObject( rt , obj , QueryObjectByObjectName(rt,"int") , "OPTION_OPEN_ONCE") ;
	if( prop == NULL )
		return NULL;
	CallRuntimeFunction_int_SetIntValue( rt , prop , 4 );
	SetConstantObject( prop );
	
	/* int log.OPTION_SET_OUTPUT_BY_FILENAME */
	prop = AddPropertyInObject( rt , obj , QueryObjectByObjectName(rt,"int") , "OPTION_SET_OUTPUT_BY_FILENAME") ;
	if( prop == NULL )
		return NULL;
	CallRuntimeFunction_int_SetIntValue( rt , prop , 8 );
	SetConstantObject( prop );
	
	/* int log.OPTION_FILENAME_APPEND_DOT_LOG */
	prop = AddPropertyInObject( rt , obj , QueryObjectByObjectName(rt,"int") , "OPTION_FILENAME_APPEND_DOT_LOG") ;
	if( prop == NULL )
		return NULL;
	CallRuntimeFunction_int_SetIntValue( rt , prop , 16 );
	SetConstantObject( prop );
	
	/* int log.ROTATEMODE_NONE */
	prop = AddPropertyInObject( rt , obj , QueryObjectByObjectName(rt,"int") , "ROTATEMODE_NONE") ;
	if( prop == NULL )
		return NULL;
	CallRuntimeFunction_int_SetIntValue( rt , prop , 0 );
	SetConstantObject( prop );
	
	/* int log.ROTATEMODE_SIZE */
	prop = AddPropertyInObject( rt , obj , QueryObjectByObjectName(rt,"int") , "ROTATEMODE_SIZE") ;
	if( prop == NULL )
		return NULL;
	CallRuntimeFunction_int_SetIntValue( rt , prop , 1 );
	SetConstantObject( prop );
	
	/* int log.ROTATEMODE_PER_DAY */
	prop = AddPropertyInObject( rt , obj , QueryObjectByObjectName(rt,"int") , "ROTATEMODE_PER_DAY") ;
	if( prop == NULL )
		return NULL;
	CallRuntimeFunction_int_SetIntValue( rt , prop , 2 );
	SetConstantObject( prop );
	
	/* int log.ROTATEMODE_PER_HOUR */
	prop = AddPropertyInObject( rt , obj , QueryObjectByObjectName(rt,"int") , "ROTATEMODE_PER_HOUR") ;
	if( prop == NULL )
		return NULL;
	CallRuntimeFunction_int_SetIntValue( rt , prop , 3 );
	SetConstantObject( prop );
	
	/* log.SetOutput() */
	func = AddFunctionAndParametersInObject( rt , obj , "SetOutput" , "SetOutput(int,string)" , ZlangInvokeFunction_log_SetOutput_int_string , ZLANG_OBJECT_int , ZLANG_OBJECT_int,NULL , ZLANG_OBJECT_string,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* log.SetLevel() */
	func = AddFunctionAndParametersInObject( rt , obj , "SetLevel" , "SetLevel(int)" , ZlangInvokeFunction_log_SetLevel_int , ZLANG_OBJECT_int , ZLANG_OBJECT_int,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* log.SetStyles() */
	func = AddFunctionAndParametersInObject( rt , obj , "SetStyles" , "SetStyles(int)" , ZlangInvokeFunction_log_SetStyles_int , ZLANG_OBJECT_int , ZLANG_OBJECT_int,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* log.SetOptions() */
	func = AddFunctionAndParametersInObject( rt , obj , "SetOptions" , "SetOptions(int)" , ZlangInvokeFunction_log_SetOptions_int , ZLANG_OBJECT_int , ZLANG_OBJECT_int,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* log.SetRotateMode() */
	func = AddFunctionAndParametersInObject( rt , obj , "SetRotateMode" , "SetRotateMode(int)" , ZlangInvokeFunction_log_SetRotateMode_int , ZLANG_OBJECT_int , ZLANG_OBJECT_int,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* log.SetRotateSize() */
	func = AddFunctionAndParametersInObject( rt , obj , "SetRotateSize" , "SetRotateSize(int)" , ZlangInvokeFunction_log_SetRotateSize_int , ZLANG_OBJECT_int , ZLANG_OBJECT_int,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* log.LevelLog(...) */
	/*
	func = AddFunctionAndParametersInObject( rt , obj , "LevelLog" , "LevelLog(...)" , ZlangInvokeFunction_log_LevelLog_vargs , ZLANG_OBJECT_int , ZLANG_OBJECT_vargs,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	*/
	
	/* log.Debug(...) */
	func = AddFunctionAndParametersInObject( rt , obj , "Debug" , "Debug(...)" , ZlangInvokeFunction_log_Debug_vargs , ZLANG_OBJECT_int , ZLANG_OBJECT_vargs,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* log.Info(...) */
	func = AddFunctionAndParametersInObject( rt , obj , "Info" , "Info(...)" , ZlangInvokeFunction_log_Info_vargs , ZLANG_OBJECT_int , ZLANG_OBJECT_vargs,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* log.Notice(...) */
	func = AddFunctionAndParametersInObject( rt , obj , "Notice" , "Notice(...)" , ZlangInvokeFunction_log_Notice_vargs , ZLANG_OBJECT_int , ZLANG_OBJECT_vargs,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* log.Warn(...) */
	func = AddFunctionAndParametersInObject( rt , obj , "Warn" , "Warn(...)" , ZlangInvokeFunction_log_Warn_vargs , ZLANG_OBJECT_int , ZLANG_OBJECT_vargs,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* log.Error(...) */
	func = AddFunctionAndParametersInObject( rt , obj , "Error" , "Error(...)" , ZlangInvokeFunction_log_Error_vargs , ZLANG_OBJECT_int , ZLANG_OBJECT_vargs,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* log.Fatal(...) */
	func = AddFunctionAndParametersInObject( rt , obj , "Fatal" , "Fatal(...)" , ZlangInvokeFunction_log_Fatal_vargs , ZLANG_OBJECT_int , ZLANG_OBJECT_vargs,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	return obj ;
}

