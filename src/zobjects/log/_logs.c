/* Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "zobjects_log.h"

ZlangInvokeFunction ZlangInvokeFunction_logs_AddLog_string;
int ZlangInvokeFunction_logs_AddLog_string( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_logs	*logs_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangDirectProperty_log	*log_direct_prop = NULL ;
	struct ZlangObject		*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject		*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	char				*g_id = NULL ;
	int				nret = 0 ;
	
	if( out1 == NULL )
		return ZLANG_ERROR_PARAMETER;
	
	CallRuntimeFunction_string_GetStringValue( rt , in1 , & g_id , NULL );
	
	log_direct_prop = GetObjectDirectProperty(out1) ;
	if( log_direct_prop == NULL )
	{
		UnreferObject( rt , out1 );
		return ZLANG_ERROR_INTERNAL;
	}
	
	log_direct_prop->no_need_for_destroy_log_handle = 1 ;
	
	nret = AddLogToLogs( logs_direct_prop->gs , g_id , log_direct_prop->g ) ;
	if( nret )
	{
		UnreferObject( rt , out1 );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_ADD_LOG_TO_LOGS_FAILED );
	}
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_logs_Debug_vargs;
int ZlangInvokeFunction_logs_Debug_vargs( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_logs	*logs_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject		*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject		*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject		*buf_obj = NULL ;
	char				*str = NULL ;
	int32_t				str_len ;
	int32_t				nret = 0 ;
	
	if( in1 == NULL || out1 == NULL )
		return ZLANG_ERROR_PARAMETER;
	
	buf_obj = CloneStringObjectInTmpStack( rt , NULL ) ;
	if( buf_obj == NULL )
		return GetRuntimeErrorNo(rt);
	
	nret = CallRuntimeFunction_string_AppendFormatFromArgsStack( rt , buf_obj ) ;
	if( nret )
		return nret;
	GetDataPtr( rt , buf_obj , (void**) & str , & str_len );
	
	nret = WriteDebugLogs( logs_direct_prop->gs , GetRuntimeSourceFilename(rt) , GetRuntimeSourceRow(rt) , "%.*s" , str_len,str ) ;
	CallRuntimeFunction_int_SetIntValue( rt , out1 , nret );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_logs_Info_vargs;
int ZlangInvokeFunction_logs_Info_vargs( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_logs	*logs_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject		*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject		*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject		*buf_obj = NULL ;
	char				*str = NULL ;
	int32_t				str_len ;
	int32_t				nret = 0 ;
	
	if( in1 == NULL || out1 == NULL )
		return ZLANG_ERROR_PARAMETER;
	
	buf_obj = CloneStringObjectInTmpStack( rt , NULL ) ;
	if( buf_obj == NULL )
		return GetRuntimeErrorNo(rt);
	
	nret = CallRuntimeFunction_string_AppendFormatFromArgsStack( rt , buf_obj ) ;
	if( nret )
		return nret;
	GetDataPtr( rt , buf_obj , (void**) & str , & str_len );
	
	nret = WriteInfoLogs( logs_direct_prop->gs , GetRuntimeSourceFilename(rt) , GetRuntimeSourceRow(rt) , "%.*s" , str_len,str ) ;
	CallRuntimeFunction_int_SetIntValue( rt , out1 , nret );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_logs_Notice_vargs;
int ZlangInvokeFunction_logs_Notice_vargs( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_logs	*logs_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject		*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject		*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject		*buf_obj = NULL ;
	char				*str = NULL ;
	int32_t				str_len ;
	int32_t				nret = 0 ;
	
	if( in1 == NULL || out1 == NULL )
		return ZLANG_ERROR_PARAMETER;
	
	buf_obj = CloneStringObjectInTmpStack( rt , NULL ) ;
	if( buf_obj == NULL )
		return GetRuntimeErrorNo(rt);
	
	nret = CallRuntimeFunction_string_AppendFormatFromArgsStack( rt , buf_obj ) ;
	if( nret )
		return nret;
	GetDataPtr( rt , buf_obj , (void**) & str , & str_len );
	
	nret = WriteNoticeLogs( logs_direct_prop->gs , GetRuntimeSourceFilename(rt) , GetRuntimeSourceRow(rt) , "%.*s" , str_len,str ) ;
	CallRuntimeFunction_int_SetIntValue( rt , out1 , nret );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_logs_Warn_vargs;
int ZlangInvokeFunction_logs_Warn_vargs( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_logs	*logs_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject		*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject		*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject		*buf_obj = NULL ;
	char				*str = NULL ;
	int32_t				str_len ;
	int32_t				nret = 0 ;
	
	if( in1 == NULL || out1 == NULL )
		return ZLANG_ERROR_PARAMETER;
	
	buf_obj = CloneStringObjectInTmpStack( rt , NULL ) ;
	if( buf_obj == NULL )
		return GetRuntimeErrorNo(rt);
	
	nret = CallRuntimeFunction_string_AppendFormatFromArgsStack( rt , buf_obj ) ;
	if( nret )
		return nret;
	GetDataPtr( rt , buf_obj , (void**) & str , & str_len );
	
	nret = WriteWarnLogs( logs_direct_prop->gs , GetRuntimeSourceFilename(rt) , GetRuntimeSourceRow(rt) , "%.*s" , str_len,str ) ;
	CallRuntimeFunction_int_SetIntValue( rt , out1 , nret );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_logs_Error_vargs;
int ZlangInvokeFunction_logs_Error_vargs( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_logs	*logs_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject		*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject		*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject		*buf_obj = NULL ;
	char				*str = NULL ;
	int32_t				str_len ;
	int32_t				nret = 0 ;
	
	if( in1 == NULL || out1 == NULL )
		return ZLANG_ERROR_PARAMETER;
	
	buf_obj = CloneStringObjectInTmpStack( rt , NULL ) ;
	if( buf_obj == NULL )
		return GetRuntimeErrorNo(rt);
	
	nret = CallRuntimeFunction_string_AppendFormatFromArgsStack( rt , buf_obj ) ;
	if( nret )
		return nret;
	GetDataPtr( rt , buf_obj , (void**) & str , & str_len );
	
	nret = WriteErrorLogs( logs_direct_prop->gs , GetRuntimeSourceFilename(rt) , GetRuntimeSourceRow(rt) , "%.*s" , str_len,str ) ;
	CallRuntimeFunction_int_SetIntValue( rt , out1 , nret );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_logs_Fatal_vargs;
int ZlangInvokeFunction_logs_Fatal_vargs( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_logs	*logs_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject		*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject		*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject		*buf_obj = NULL ;
	char				*str = NULL ;
	int32_t				str_len ;
	int32_t				nret = 0 ;
	
	if( in1 == NULL || out1 == NULL )
		return ZLANG_ERROR_PARAMETER;
	
	buf_obj = CloneStringObjectInTmpStack( rt , NULL ) ;
	if( buf_obj == NULL )
		return GetRuntimeErrorNo(rt);
	
	nret = CallRuntimeFunction_string_AppendFormatFromArgsStack( rt , buf_obj ) ;
	if( nret )
		return nret;
	GetDataPtr( rt , buf_obj , (void**) & str , & str_len );
	
	nret = WriteFatalLogs( logs_direct_prop->gs , GetRuntimeSourceFilename(rt) , GetRuntimeSourceRow(rt) , "%.*s" , str_len,str ) ;
	CallRuntimeFunction_int_SetIntValue( rt , out1 , nret );
	
	return 0;
}

ZlangCreateDirectPropertyFunction ZlangCreateDirectProperty_logs;
void *ZlangCreateDirectProperty_logs( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_logs	*logs_prop = NULL ;
	
	logs_prop = (struct ZlangDirectProperty_logs *)ZLMALLOC( sizeof(struct ZlangDirectProperty_logs) ) ;
	if( logs_prop == NULL )
	{
		SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_ALLOC , "alloc memory for entity" )
		return NULL;
	}
	memset( logs_prop , 0x00 , sizeof(struct ZlangDirectProperty_logs) );
	
	logs_prop->gs = CreateLogsHandle() ;
	if( logs_prop->gs == NULL )
	{
		SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_ALLOC , "alloc memory for ->logs" )
		return NULL;
	}
	
	return logs_prop;
}

ZlangDestroyDirectPropertyFunction ZlangDestroyDirectProperty_logs;
void ZlangDestroyDirectProperty_logs( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_logs	*logs_direct_prop = GetObjectDirectProperty(obj) ;
	
	DestroyLogsHandle( logs_direct_prop->gs);
	
	ZLFREE( logs_direct_prop );
	
	return;
}

ZlangSummarizeDirectPropertySizeFunction ZlangSummarizeDirectPropertySize_logs;
void ZlangSummarizeDirectPropertySize_logs( struct ZlangRuntime *rt , struct ZlangObject *obj , size_t *summarized_obj_size , size_t *summarized_direct_prop_size )
{
	/*
	struct ZlangDirectProperty_logs	*logs_direct_prop = GetObjectDirectProperty(obj) ;
	
	if( logs_direct_prop->gs )
		SUMMARIZE_SIZE( summarized_direct_prop_size , sizeof(LOGS) )
	*/
	
	SUMMARIZE_SIZE( summarized_direct_prop_size , sizeof(struct ZlangDirectProperty_logs) )
	
	return;
}

static struct ZlangDirectFunctions direct_funcs_logs =
	{
		ZLANG_OBJECT_logs , /* char *ancestor_name */
		
		ZlangCreateDirectProperty_logs , /* ZlangCreateDirectPropertyFunction *create_entity_func */
		ZlangDestroyDirectProperty_logs , /* ZlangDestroyDirectPropertyFunction *destroy_entity_func */
		
		NULL , /* ZlangFromCharPtrFunction *from_char_ptr_func */
		NULL , /* ZlangToStringFunction *to_string_func */
		NULL , /* ZlangFromDataPtrFunction *from_data_ptr_func */
		NULL , /* ZlangGetDataPtrFunction *get_data_ptr_func */
		
		NULL , /* ZlangOperatorFunction *oper_PLUS_func */
		NULL , /* ZlangOperatorFunction *oper_MINUS_func */
		NULL , /* ZlangOperatorFunction *oper_MUL_func */
		NULL , /* ZlangOperatorFunction *oper_DIV_func */
		NULL , /* ZlangOperatorFunction *oper_MOD_func */
		
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_NEGATIVE_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_NOT_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_BIT_REVERSE_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_PLUS_PLUS_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_MINUS_MINUS_func */
		
		NULL , /* ZlangCompareFunction *comp_EGUAL_func */
		NULL , /* ZlangCompareFunction *comp_NOTEGUAL_func */
		NULL , /* ZlangCompareFunction *comp_LT_func */
		NULL , /* ZlangCompareFunction *comp_LE_func */
		NULL , /* ZlangCompareFunction *comp_GT_func */
		NULL , /* ZlangCompareFunction *comp_GE_func */
		
		NULL , /* ZlangLogicFunction *logsic_AND_func */
		NULL , /* ZlangLogicFunction *logsic_OR_func */
		
		NULL , /* ZlangLogicFunction *bit_AND_func */
		NULL , /* ZlangLogicFunction *bit_XOR_func */
		NULL , /* ZlangLogicFunction *bit_OR_func */
		NULL , /* ZlangLogicFunction *bit_MOVELEFT_func */
		NULL , /* ZlangLogicFunction *bit_MOVERIGHT_func */
		
		ZlangSummarizeDirectPropertySize_logs , /* ZlangSummarizeDirectPropertySizeFunction *summarize_direct_prop_size_func */
	} ;

ZlangImportObjectFunction ZlangImportObject_logs;
struct ZlangObject *ZlangImportObject_logs( struct ZlangRuntime *rt )
{
	struct ZlangObject	*obj = NULL ;
	struct ZlangFunction	*func = NULL ;
	int			nret = 0 ;
	
	nret = ImportObject( rt , & obj , ZLANG_OBJECT_logs , & direct_funcs_logs , sizeof(struct ZlangDirectFunctions) , NULL ) ;
	if( nret )
	{
		SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_LINK_FUNC_TO_ENTITY , "import object to global objects heap" )
		return NULL;
	}
	
	/* logs.AddLog() */
	func = AddFunctionAndParametersInObject( rt , obj , "AddLog" , "AddLog(string)" , ZlangInvokeFunction_logs_AddLog_string , ZLANG_OBJECT_log , ZLANG_OBJECT_string,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* logs.Debug(...) */
	func = AddFunctionAndParametersInObject( rt , obj , "Debug" , "Debug(...)" , ZlangInvokeFunction_logs_Debug_vargs , ZLANG_OBJECT_int , ZLANG_OBJECT_vargs,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* logs.Info(...) */
	func = AddFunctionAndParametersInObject( rt , obj , "Info" , "Info(...)" , ZlangInvokeFunction_logs_Info_vargs , ZLANG_OBJECT_int , ZLANG_OBJECT_vargs,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* logs.Notice(...) */
	func = AddFunctionAndParametersInObject( rt , obj , "Notice" , "Notice(...)" , ZlangInvokeFunction_logs_Notice_vargs , ZLANG_OBJECT_int , ZLANG_OBJECT_vargs,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* logs.Warn(...) */
	func = AddFunctionAndParametersInObject( rt , obj , "Warn" , "Warn(...)" , ZlangInvokeFunction_logs_Warn_vargs , ZLANG_OBJECT_int , ZLANG_OBJECT_vargs,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* logs.Error(...) */
	func = AddFunctionAndParametersInObject( rt , obj , "Error" , "Error(...)" , ZlangInvokeFunction_logs_Error_vargs , ZLANG_OBJECT_int , ZLANG_OBJECT_vargs,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* logs.Fatal(...) */
	func = AddFunctionAndParametersInObject( rt , obj , "Fatal" , "Fatal(...)" , ZlangInvokeFunction_logs_Fatal_vargs , ZLANG_OBJECT_int , ZLANG_OBJECT_vargs,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	return obj ;
}

