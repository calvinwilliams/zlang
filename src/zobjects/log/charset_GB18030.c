/* Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

static struct ZlangCharsetAlias		g_zlang_charset_aliases_GB18030[] = {
		{ ZLANG_CHARSET_GB18030 , "日志" , "log" } ,
		{ ZLANG_CHARSET_GB18030 , "不输出" , "OUTPUT_INVALID" } ,
		{ ZLANG_CHARSET_GB18030 , "输出到标准输出" , "OUTPUT_STDOUT" } ,
		{ ZLANG_CHARSET_GB18030 , "输出到标准错误" , "OUTPUT_STDERR" } ,
		{ ZLANG_CHARSET_GB18030 , "输出到SYSLOG" , "OUTPUT_SYSLOG" } ,
		{ ZLANG_CHARSET_GB18030 , "输出到文件" , "OUTPUT_FILE" } ,
		{ ZLANG_CHARSET_GB18030 , "调试级别" , "LEVEL_DEBUG" } ,
		{ ZLANG_CHARSET_GB18030 , "信息级别" , "LEVEL_INFO" } ,
		{ ZLANG_CHARSET_GB18030 , "通知级别" , "LEVEL_NOTICE" } ,
		{ ZLANG_CHARSET_GB18030 , "警告级别" , "LEVEL_WARN" } ,
		{ ZLANG_CHARSET_GB18030 , "错误级别" , "LEVEL_ERROR" } ,
		{ ZLANG_CHARSET_GB18030 , "致命错误级别" , "LEVEL_FATAL" } ,
		{ ZLANG_CHARSET_GB18030 , "无日志级别" , "LEVEL_NOLOG" } ,
		{ ZLANG_CHARSET_GB18030 , "日期样式" , "STYLE_DATE" } ,
		{ ZLANG_CHARSET_GB18030 , "日期时间样式" , "STYLE_DATETIME" } ,
		{ ZLANG_CHARSET_GB18030 , "日期时间毫秒样式" , "STYLE_DATETIMEMS" } ,
		{ ZLANG_CHARSET_GB18030 , "日志等级样式" , "STYLE_LOGLEVEL" } ,
		{ ZLANG_CHARSET_GB18030 , "进程编号样式" , "STYLE_PID" } ,
		{ ZLANG_CHARSET_GB18030 , "线程编号样式" , "STYLE_TID" } ,
		{ ZLANG_CHARSET_GB18030 , "源代码文件和行数样式" , "STYLE_SOURCE" } ,
		{ ZLANG_CHARSET_GB18030 , "用户内容样式" , "STYLE_FORMAT" } ,
		{ ZLANG_CHARSET_GB18030 , "换行样式" , "STYLE_NEWLINE" } ,
		{ ZLANG_CHARSET_GB18030 , "每次写日志都打开关闭文件选项" , "OPTION_OPEN_AND_CLOSE" } ,
		{ ZLANG_CHARSET_GB18030 , "文件变动监测选项" , "OPTION_CHANGE_TEST" } ,
		{ ZLANG_CHARSET_GB18030 , "只打开文件一次选项" , "OPTION_OPEN_ONCE" } ,
		{ ZLANG_CHARSET_GB18030 , "通过文件名设置输出选项" , "OPTION_SET_OUTPUT_BY_FILENAME" } ,
		{ ZLANG_CHARSET_GB18030 , "文件名自动追加log后缀选项" , "OPTION_FILENAME_APPEND_DOT_LOG" } ,
		{ ZLANG_CHARSET_GB18030 , "无转档文件模式" , "ROTATEMODE_NONE" } ,
		{ ZLANG_CHARSET_GB18030 , "按文件大小转档文件模式" , "ROTATEMODE_SIZE" } ,
		{ ZLANG_CHARSET_GB18030 , "按天转档文件模式" , "ROTATEMODE_PER_DAY" } ,
		{ ZLANG_CHARSET_GB18030 , "按小时转档文件模式" , "ROTATEMODE_PER_HOUR" } ,
		{ ZLANG_CHARSET_GB18030 , "设置输出" , "SetOutput" } ,
		{ ZLANG_CHARSET_GB18030 , "设置日志级别" , "SetLevel" } ,
		{ ZLANG_CHARSET_GB18030 , "设置日志样式" , "SetStyles" } ,
		{ ZLANG_CHARSET_GB18030 , "设置日志选项" , "SetOptions" } ,
		{ ZLANG_CHARSET_GB18030 , "设置日志转档模式" , "SetRotateMode" } ,
		{ ZLANG_CHARSET_GB18030 , "设置日志转档大小" , "SetRotateSize" } ,
		{ ZLANG_CHARSET_GB18030 , "调试" , "Debug" } ,
		{ ZLANG_CHARSET_GB18030 , "信息" , "Info" } ,
		{ ZLANG_CHARSET_GB18030 , "通知" , "Notice" } ,
		{ ZLANG_CHARSET_GB18030 , "警告" , "Warn" } ,
		{ ZLANG_CHARSET_GB18030 , "错误" , "Error" } ,
		{ ZLANG_CHARSET_GB18030 , "致命错误" , "Fatal" } ,
		{ ZLANG_CHARSET_GB18030 , "日志集" , "logs" } ,
		{ ZLANG_CHARSET_GB18030 , "加日志" , "AddLog" } ,
		{ 0 , NULL , NULL } ,
	} ;

