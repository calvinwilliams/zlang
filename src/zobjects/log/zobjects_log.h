/* Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef _H_LIBZLANG_LOG_
#define _H_LIBZLANG_LOG_

#include "zlang.h"

#include "LOG.h"
#include "LOGS.h"

#define ZLANG_OBJECT_log	"log"
#define ZLANG_OBJECT_logs	"logs"

#define EXCEPTION_MESSAGE_ADD_LOG_TO_LOGS_FAILED	"add log to logs failed"

DLLEXPORT ZlangImportObjectsFunction ZlangImportObjects;
int ZlangImportObjects( struct ZlangRuntime *rt );

struct ZlangDirectProperty_log
{
	LOG		*g ;
	unsigned char	no_need_for_destroy_log_handle ;
} ;

struct ZlangDirectProperty_logs
{
	LOGS		*gs ;
} ;

#endif

