/* Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef _H_LIBZLANG_XML_
#define _H_LIBZLANG_XML_

#include "zlang.h"

#include "fasterxml.h"

#define ZLANG_OBJECT_xml	"xml"

#define EXCEPTION_MESSAGE_FIND_XPATH_FAILED	"find xpath failed"
#define EXCEPTION_MESSAGE_FIND_XNODE_FAILED	"find xnode failed"
#define EXCEPTION_MESSAGE_TRAVEL_XML_FAILED	"travel xml failed"
#define EXCEPTION_MESSAGE_UNCOMPLETED_STRING	"uncompleted string"
#define EXCEPTION_MESSAGE_OBJECT_TO_STRING	"object to string failed"
#define EXCEPTION_MESSAGE_STRING_TO_ENTITY_OBJECT_FAILED	"string to entity object failed"
#define EXCEPTION_MESSAGE_STRING_TO_OBJECT_FAILED	"string to object failed"

DLLEXPORT ZlangImportObjectsFunction ZlangImportObjects;
int ZlangImportObjects( struct ZlangRuntime *rt );

#endif

