﻿/* Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

static struct ZlangCharsetAlias		g_zlang_charset_aliases_UTF8[] = {
		{ ZLANG_CHARSET_UTF8 , "字符串转对象紧缩样式" , "OBJECTTOSTRING_STYLE_COMPACT" } ,
		{ ZLANG_CHARSET_UTF8 , "对象转字符串缩进样式" , "OBJECTTOSTRING_STYLE_INDENT_TAB" } ,
		{ ZLANG_CHARSET_UTF8 , "字符串转对象" , "StringToObject" } ,
		{ ZLANG_CHARSET_UTF8 , "字符串转实体对象" , "StringToEntityObject" } ,
		{ ZLANG_CHARSET_UTF8 , "对象转字符串" , "ObjectToString" } ,
		{ 0 , NULL , NULL } ,
	} ;

