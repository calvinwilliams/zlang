/* Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "zobjects_stdtypes.h"

struct ZlangDirectProperty_int
{
	int32_t		value ;
} ;

#define ATOX		atol
#define FORMAT		"%"PRIi32""
#define ZERO		0
#include "basetype.tpl2.c"
ZlangCreateDirectProperty_(int)
ZlangDestroyDirectProperty_(int)
ZlangFromCharPtr_(int)
ZlangToString_(int)
ZlangFromDataPtr_(int,int32_t)
ZlangGetDataPtr_(int)
basetype_SetBasetypeValue(int,Int,int32_t)
basetype_GetBasetypeValue(int,Int,int32_t)
ZlangOperator_PLUS_(int,Int)
ZlangOperator_MINUS_(int,Int)
ZlangOperator_MUL_(int,Int)
ZlangOperator_DIV_(int,Int)
ZlangOperatorFunction ZlangOperator_int_MOD_int;
int ZlangOperator_int_MOD_int( struct ZlangRuntime *rt , struct ZlangObject *in_obj1 , struct ZlangObject *in_obj2 , struct ZlangObject **out_obj )
{
	struct ZlangDirectProperty_int	*in1 = GetObjectDirectProperty(in_obj1) ;
	struct ZlangDirectProperty_int	*in2 = GetObjectDirectProperty(in_obj2) ;
	struct ZlangObject		*o = NULL ;
	struct ZlangDirectProperty_int	*out = NULL ;
	
	if( in2->value == 0 )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "*** FATAL : division by zero" )
		return ThrowFatalException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_DIVISION_BY_ZERO );
	}
	
	o = CloneIntObjectInTmpStack( rt , NULL ) ;
	if( o == NULL )
		return ThrowFatalException( rt , ZLANG_ERROR_INTERNAL , GetRuntimeErrorString(rt) );
	out = GetObjectDirectProperty(o) ;
	
	out->value = in1->value % in2->value ;
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "["FORMAT"]%%["FORMAT"]=>["FORMAT"]" , in1->value , in2->value , out->value )
	
	(*out_obj) = o ;
	return 0;
}
ZlangUnaryOperator_NEGATIVE_(int)
ZlangUnaryOperator_NOT_(int,int32_t)
ZlangUnaryOperator_BIT_REVERSE_(int,int32_t)
ZlangUnaryOperator_PLUS_PLUS_(int)
ZlangUnaryOperator_MINUS_MINUS_(int)
ZlangCompare_EGUAL_(int)
ZlangCompare_NOTEGUAL_(int)
ZlangCompare_LT_(int)
ZlangCompare_LE_(int)
ZlangCompare_GT_(int)
ZlangCompare_GE_(int)
ZlangBit_AND_(int)
ZlangBit_XOR_(int)
ZlangBit_OR_(int)
ZlangBit_MOVELEFT_(int)
ZlangBit_MOVERIGHT_(int)
ZlangSummarizeDirectPropertySize_(int)

ZlangCompare_FormatString_string_(int)
ZlangCompare_IsBetween_v_v(int,Int,int32_t)

static struct ZlangDirectFunctions direct_funcs_int =
	{
		ZLANG_OBJECT_int ,
		
		ZlangCreateDirectProperty_int ,
		ZlangDestroyDirectProperty_int ,
		
		ZlangFromCharPtr_int ,
		ZlangToString_int ,
		ZlangFromDataPtr_int ,
		ZlangGetDataPtr_int ,
		
		ZlangOperator_int_PLUS_int ,
		ZlangOperator_int_MINUS_int ,
		ZlangOperator_int_MUL_int ,
		ZlangOperator_int_DIV_int ,
		ZlangOperator_int_MOD_int ,
		
		ZlangUnaryOperator_NEGATIVE_int ,
		ZlangUnaryOperator_NOT_int ,
		ZlangUnaryOperator_BIT_REVERSE_int ,
		ZlangUnaryOperator_PLUS_PLUS_int ,
		ZlangUnaryOperator_MINUS_MINUS_int ,
		
		ZlangCompare_int_EGUAL_int ,
		ZlangCompare_int_NOTEGUAL_int ,
		ZlangCompare_int_LT_int ,
		ZlangCompare_int_LE_int ,
		ZlangCompare_int_GT_int ,
		ZlangCompare_int_GE_int ,
		
		NULL ,
		NULL ,
		
		ZlangBit_int_AND_int ,
		ZlangBit_int_XOR_int ,
		ZlangBit_int_OR_int ,
		ZlangBit_int_MOVELEFT_int ,
		ZlangBit_int_MOVERIGHT_int ,
		
		ZlangSummarizeDirectPropertySize_int ,
	} ;

ZlangImportObject_(int,Int)

