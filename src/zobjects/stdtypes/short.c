/* Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "zobjects_stdtypes.h"

struct ZlangDirectProperty_short
{
	int16_t		value ;
} ;

#define ATOX		atoi
#define FORMAT		"%"PRIi16""
#define ZERO		0
#include "basetype.tpl2.c"
ZlangCreateDirectProperty_(short)
ZlangDestroyDirectProperty_(short)
ZlangFromCharPtr_(short)
ZlangToString_(short)
ZlangFromDataPtr_(short,int16_t)
ZlangGetDataPtr_(short)
basetype_SetBasetypeValue(short,Short,int16_t)
basetype_GetBasetypeValue(short,Short,int16_t)
ZlangOperator_PLUS_(short,Short)
ZlangOperator_MINUS_(short,Short)
ZlangOperator_MUL_(short,Short)
ZlangOperator_DIV_(short,Short)
ZlangOperatorFunction ZlangOperator_short_MOD_short;
int ZlangOperator_short_MOD_short( struct ZlangRuntime *rt , struct ZlangObject *in_obj1 , struct ZlangObject *in_obj2 , struct ZlangObject **out_obj )
{
	struct ZlangDirectProperty_short	*in1 = GetObjectDirectProperty(in_obj1) ;
	struct ZlangDirectProperty_short	*in2 = GetObjectDirectProperty(in_obj2) ;
	struct ZlangObject			*o = NULL ;
	struct ZlangDirectProperty_short	*out = NULL ;
	
	if( in2->value == 0 )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "*** FATAL : division by zero" )
		return ThrowFatalException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_DIVISION_BY_ZERO );
	}
	
	o = CloneShortObjectInTmpStack( rt , NULL ) ;
	if( o == NULL )
		return ThrowFatalException( rt , ZLANG_ERROR_INTERNAL , GetRuntimeErrorString(rt) );
	out = GetObjectDirectProperty(o) ;
	
	out->value = in1->value % in2->value ;
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "["FORMAT"]%%["FORMAT"]=>["FORMAT"]" , in1->value , in2->value , out->value )
	
	(*out_obj) = o ;
	return 0;
}
ZlangUnaryOperator_NEGATIVE_(short)
ZlangUnaryOperator_NOT_(short,int16_t)
ZlangUnaryOperator_BIT_REVERSE_(short,int16_t)
ZlangUnaryOperator_PLUS_PLUS_(short)
ZlangUnaryOperator_MINUS_MINUS_(short)
ZlangCompare_EGUAL_(short)
ZlangCompare_NOTEGUAL_(short)
ZlangCompare_LT_(short)
ZlangCompare_LE_(short)
ZlangCompare_GT_(short)
ZlangCompare_GE_(short)
ZlangBit_AND_(short)
ZlangBit_XOR_(short)
ZlangBit_OR_(short)
ZlangBit_MOVELEFT_(short)
ZlangBit_MOVERIGHT_(short)
ZlangSummarizeDirectPropertySize_(short)

ZlangCompare_FormatString_string_(short)
ZlangCompare_IsBetween_v_v(short,Short,int16_t)

static struct ZlangDirectFunctions direct_funcs_short =
	{
		ZLANG_OBJECT_short ,
		
		ZlangCreateDirectProperty_short ,
		ZlangDestroyDirectProperty_short ,
		
		ZlangFromCharPtr_short ,
		ZlangToString_short ,
		ZlangFromDataPtr_short ,
		ZlangGetDataPtr_short ,
		
		ZlangOperator_short_PLUS_short ,
		ZlangOperator_short_MINUS_short ,
		ZlangOperator_short_MUL_short ,
		ZlangOperator_short_DIV_short ,
		ZlangOperator_short_MOD_short ,
		
		ZlangUnaryOperator_NEGATIVE_short ,
		ZlangUnaryOperator_NOT_short ,
		ZlangUnaryOperator_BIT_REVERSE_short ,
		ZlangUnaryOperator_PLUS_PLUS_short ,
		ZlangUnaryOperator_MINUS_MINUS_short ,
		
		ZlangCompare_short_EGUAL_short ,
		ZlangCompare_short_NOTEGUAL_short ,
		ZlangCompare_short_LT_short ,
		ZlangCompare_short_LE_short ,
		ZlangCompare_short_GT_short ,
		ZlangCompare_short_GE_short ,
		
		NULL ,
		NULL ,
		
		ZlangBit_short_AND_short ,
		ZlangBit_short_XOR_short ,
		ZlangBit_short_OR_short ,
		ZlangBit_short_MOVELEFT_int ,
		ZlangBit_short_MOVERIGHT_int ,
		
		ZlangSummarizeDirectPropertySize_short ,
	} ;

ZlangImportObject_(short,Short)

