/* Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "zobjects_stdtypes.h"

struct ZlangDirectProperty_long
{
	int64_t		value ;
} ;

#define ATOX		atoll
#define FORMAT		"%"PRIi64""
#define ZERO		0
#include "basetype.tpl2.c"
ZlangCreateDirectProperty_(long)
ZlangDestroyDirectProperty_(long)
ZlangFromCharPtr_(long)
ZlangToString_(long)
ZlangFromDataPtr_(long,int64_t)
ZlangGetDataPtr_(long)
basetype_SetBasetypeValue(long,Long,int64_t)
basetype_GetBasetypeValue(long,Long,int64_t)
ZlangOperator_PLUS_(long,Long)
ZlangOperator_MINUS_(long,Long)
ZlangOperator_MUL_(long,Long)
ZlangOperator_DIV_(long,Long)
ZlangOperatorFunction ZlangOperator_long_MOD_long;
int ZlangOperator_long_MOD_long( struct ZlangRuntime *rt , struct ZlangObject *in_obj1 , struct ZlangObject *in_obj2 , struct ZlangObject **out_obj )
{
	struct ZlangDirectProperty_long	*in1 = GetObjectDirectProperty(in_obj1) ;
	struct ZlangDirectProperty_long	*in2 = GetObjectDirectProperty(in_obj2) ;
	struct ZlangObject		*o = NULL ;
	struct ZlangDirectProperty_long	*out = NULL ;
	
	if( in2->value == 0 )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "*** FATAL : division by zero" )
		return ThrowFatalException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_DIVISION_BY_ZERO );
	}
	
	o = CloneLongObjectInTmpStack( rt , NULL ) ;
	if( o == NULL )
		return ThrowFatalException( rt , ZLANG_ERROR_INTERNAL , GetRuntimeErrorString(rt) );
	out = GetObjectDirectProperty(o) ;
	
	out->value = in1->value % in2->value ;
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "["FORMAT"]%%["FORMAT"]=>["FORMAT"]" , in1->value , in2->value , out->value )
	
	(*out_obj) = o ;
	return 0;
}
ZlangUnaryOperator_NEGATIVE_(long)
ZlangUnaryOperator_NOT_(long,int64_t)
ZlangUnaryOperator_BIT_REVERSE_(long,int64_t)
ZlangUnaryOperator_PLUS_PLUS_(long)
ZlangUnaryOperator_MINUS_MINUS_(long)
ZlangCompare_EGUAL_(long)
ZlangCompare_NOTEGUAL_(long)
ZlangCompare_LT_(long)
ZlangCompare_LE_(long)
ZlangCompare_GT_(long)
ZlangCompare_GE_(long)
ZlangBit_AND_(long)
ZlangBit_XOR_(long)
ZlangBit_OR_(long)
ZlangBit_MOVELEFT_(long)
ZlangBit_MOVERIGHT_(long)
ZlangSummarizeDirectPropertySize_(long)

ZlangCompare_FormatString_string_(long)
ZlangCompare_IsBetween_v_v(long,Long,int64_t)

static struct ZlangDirectFunctions direct_funcs_long =
	{
		ZLANG_OBJECT_long ,
		
		ZlangCreateDirectProperty_long ,
		ZlangDestroyDirectProperty_long ,
		
		ZlangFromCharPtr_long ,
		ZlangToString_long ,
		ZlangFromDataPtr_long ,
		ZlangGetDataPtr_long ,
		
		ZlangOperator_long_PLUS_long ,
		ZlangOperator_long_MINUS_long ,
		ZlangOperator_long_MUL_long ,
		ZlangOperator_long_DIV_long ,
		ZlangOperator_long_MOD_long ,
		
		ZlangUnaryOperator_NEGATIVE_long ,
		ZlangUnaryOperator_NOT_long ,
		ZlangUnaryOperator_BIT_REVERSE_long ,
		ZlangUnaryOperator_PLUS_PLUS_long ,
		ZlangUnaryOperator_MINUS_MINUS_long ,
		
		ZlangCompare_long_EGUAL_long ,
		ZlangCompare_long_NOTEGUAL_long ,
		ZlangCompare_long_LT_long ,
		ZlangCompare_long_LE_long ,
		ZlangCompare_long_GT_long ,
		ZlangCompare_long_GE_long ,
		
		NULL ,
		NULL ,
		
		ZlangBit_long_AND_long ,
		ZlangBit_long_XOR_long ,
		ZlangBit_long_OR_long ,
		ZlangBit_long_MOVELEFT_int ,
		ZlangBit_long_MOVERIGHT_int ,
		
		ZlangSummarizeDirectPropertySize_long ,
	} ;

ZlangImportObject_(long,Long)

