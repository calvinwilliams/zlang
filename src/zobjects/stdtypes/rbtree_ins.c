/*
 * rbtree_tpl
 * author	: calvin
 * email	: calvinwilliams@163.com
 *
 * Licensed under the LGPL v2.1, see the file LICENSE in base directory.
 */

#include "rbtree_tpl.h"

#include "map.h"

void FreeMapNode( void *pv );

LINK_RBTREENODE_INT( LinkMapTreeNodeByKeyId , struct ZlangDirectProperty_map , map , struct MapNode , node , map_key_id )
UNLINK_RBTREENODE( UnlinkMapTreeNode , struct ZlangDirectProperty_map , map , struct MapNode , node )
QUERY_RBTREENODE_INT( QueryMapTreeNodeByKeyId , struct ZlangDirectProperty_map , map , struct MapNode , node , map_key_id )
TRAVEL_RBTREENODE( TravelMapTreeNodeByKeyId , struct ZlangDirectProperty_map , map , struct MapNode , node )
TRAVELPREV_RBTREENODE( TravelPrevMapTreeNodeByKeyId , struct ZlangDirectProperty_map , map , struct MapNode , node )
DESTROY_RBTREE( DestroyMapTree , struct ZlangDirectProperty_map , map , struct MapNode , node , FreeMapNode )

