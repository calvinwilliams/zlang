/* Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "zobjects_stdtypes.h"

#include <math.h>

struct ZlangDirectProperty_float
{
	float		value ;
} ;

#define ATOX		atof
#define FORMAT		"%f"
#define ZERO		0.0
#include "basetype.tpl2.c"
ZlangCreateDirectProperty_(float)
ZlangDestroyDirectProperty_(float)
ZlangFromCharPtr_(float)
ZlangToString_(float)
ZlangFromDataPtr_(float,float)
ZlangGetDataPtr_(float)
basetype_SetBasetypeValue(float,Float,float)
basetype_GetBasetypeValue(float,Float,float)
ZlangOperator_PLUS_(float,Float)
ZlangOperator_MINUS_(float,Float)
ZlangOperator_MUL_(float,Float)
ZlangOperator_DIV_(float,Float)
ZlangOperatorFunction ZlangOperator_float_MOD_float;
int ZlangOperator_float_MOD_float( struct ZlangRuntime *rt , struct ZlangObject *in_obj1 , struct ZlangObject *in_obj2 , struct ZlangObject **out_obj )
{
	struct ZlangDirectProperty_float	*in1 = GetObjectDirectProperty(in_obj1) ;
	struct ZlangDirectProperty_float	*in2 = GetObjectDirectProperty(in_obj2) ;
	struct ZlangObject			*o = NULL ;
	struct ZlangDirectProperty_float	*out = NULL ;
	
	if( fabsf(in2->value) <= FLOAT_EPSILON )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "*** FATAL : division by zero" )
		return ThrowFatalException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_DIVISION_BY_ZERO );
	}
	
	o = CloneFloatObjectInTmpStack( rt , NULL ) ;
	if( o == NULL )
		return ThrowFatalException( rt , ZLANG_ERROR_INTERNAL , GetRuntimeErrorString(rt) );
	out = GetObjectDirectProperty(o) ;
	
	out->value = fmodf( in1->value , in2->value ) ;
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "["FORMAT"]%%["FORMAT"]=>["FORMAT"]" , in1->value , in2->value , out->value )
	
	(*out_obj) = o ;
	return 0;
}
ZlangUnaryOperator_NEGATIVE_(float)
ZlangUnaryOperator_NOT_(float,float)
ZlangUnaryOperator_PLUS_PLUS_(float)
ZlangUnaryOperator_MINUS_MINUS_(float)
ZlangCompare_EGUAL_(float)
ZlangCompare_NOTEGUAL_(float)
ZlangCompare_LT_(float)
ZlangCompare_LE_(float)
ZlangCompare_GT_(float)
ZlangCompare_GE_(float)
ZlangSummarizeDirectPropertySize_(float)

ZlangCompare_FormatString_string_(float)
ZlangCompare_IsBetween_v_v(float,Float,float)

static struct ZlangDirectFunctions direct_funcs_float =
	{
		ZLANG_OBJECT_float ,
		
		ZlangCreateDirectProperty_float ,
		ZlangDestroyDirectProperty_float ,
		
		ZlangFromCharPtr_float ,
		ZlangToString_float ,
		ZlangFromDataPtr_float ,
		ZlangGetDataPtr_float ,
		
		ZlangOperator_float_PLUS_float ,
		ZlangOperator_float_MINUS_float ,
		ZlangOperator_float_MUL_float ,
		ZlangOperator_float_DIV_float ,
		ZlangOperator_float_MOD_float ,
		
		ZlangUnaryOperator_NEGATIVE_float ,
		ZlangUnaryOperator_NOT_float ,
		NULL ,
		ZlangUnaryOperator_PLUS_PLUS_float ,
		ZlangUnaryOperator_MINUS_MINUS_float ,
		
		ZlangCompare_float_EGUAL_float ,
		ZlangCompare_float_NOTEGUAL_float ,
		ZlangCompare_float_LT_float ,
		ZlangCompare_float_LE_float ,
		ZlangCompare_float_GT_float ,
		ZlangCompare_float_GE_float ,
		
		NULL ,
		NULL ,
		
		NULL ,
		NULL ,
		NULL ,
		NULL ,
		NULL ,
		
		ZlangSummarizeDirectPropertySize_float ,
	} ;

ZlangImportObject_(float,Float)

