/* Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "zobjects_stdtypes.h"

#include <math.h>

struct ZlangDirectProperty_double
{
	double		value ;
} ;

#define ATOX		atof
#define FORMAT		"%lf"
#define ZERO		0.00
#include "basetype.tpl2.c"
ZlangCreateDirectProperty_(double)
ZlangDestroyDirectProperty_(double)
ZlangFromCharPtr_(double)
ZlangToString_(double)
ZlangFromDataPtr_(double,double)
ZlangGetDataPtr_(double)
basetype_SetBasetypeValue(double,Double,double)
basetype_GetBasetypeValue(double,Double,double)
ZlangOperator_PLUS_(double,Double)
ZlangOperator_MINUS_(double,Double)
ZlangOperator_MUL_(double,Double)
ZlangOperator_DIV_(double,Double)
ZlangOperatorFunction ZlangOperator_short_MOD_double;
int ZlangOperator_double_MOD_double( struct ZlangRuntime *rt , struct ZlangObject *in_obj1 , struct ZlangObject *in_obj2 , struct ZlangObject **out_obj )
{
	struct ZlangDirectProperty_double	*in1 = GetObjectDirectProperty(in_obj1) ;
	struct ZlangDirectProperty_double	*in2 = GetObjectDirectProperty(in_obj2) ;
	struct ZlangObject			*o = NULL ;
	struct ZlangDirectProperty_double	*out = NULL ;
	
	if( fabs(in2->value) <= DOUBLE_EPSILON )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "*** FATAL : division by zero" )
		return ThrowFatalException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_DIVISION_BY_ZERO );
	}
	
	o = CloneDoubleObjectInTmpStack( rt , NULL ) ;
	if( o == NULL )
		return ThrowFatalException( rt , ZLANG_ERROR_INTERNAL , GetRuntimeErrorString(rt) );
	out = GetObjectDirectProperty(o) ;
	
	out->value = fmod( in1->value , in2->value ) ;
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "["FORMAT"]%%["FORMAT"]=>["FORMAT"]" , in1->value , in2->value , out->value )
	
	(*out_obj) = o ;
	return 0;
}
ZlangUnaryOperator_NEGATIVE_(double)
ZlangUnaryOperator_NOT_(double,double)
ZlangUnaryOperator_PLUS_PLUS_(double)
ZlangUnaryOperator_MINUS_MINUS_(double)
ZlangCompare_EGUAL_(double)
ZlangCompare_NOTEGUAL_(double)
ZlangCompare_LT_(double)
ZlangCompare_LE_(double)
ZlangCompare_GT_(double)
ZlangCompare_GE_(double)
ZlangSummarizeDirectPropertySize_(double)

ZlangCompare_FormatString_string_(double)
ZlangCompare_IsBetween_v_v(double,Double,double)

static struct ZlangDirectFunctions direct_funcs_double =
	{
		ZLANG_OBJECT_double ,
		
		ZlangCreateDirectProperty_double ,
		ZlangDestroyDirectProperty_double ,
		
		ZlangFromCharPtr_double ,
		ZlangToString_double ,
		ZlangFromDataPtr_double ,
		ZlangGetDataPtr_double ,
		
		ZlangOperator_double_PLUS_double ,
		ZlangOperator_double_MINUS_double ,
		ZlangOperator_double_MUL_double ,
		ZlangOperator_double_DIV_double ,
		ZlangOperator_double_MOD_double ,
		
		ZlangUnaryOperator_NEGATIVE_double ,
		ZlangUnaryOperator_NOT_double ,
		NULL ,
		ZlangUnaryOperator_PLUS_PLUS_double ,
		ZlangUnaryOperator_MINUS_MINUS_double ,
		
		ZlangCompare_double_EGUAL_double ,
		ZlangCompare_double_NOTEGUAL_double ,
		ZlangCompare_double_LT_double ,
		ZlangCompare_double_LE_double ,
		ZlangCompare_double_GT_double ,
		ZlangCompare_double_GE_double ,
		
		NULL ,
		NULL ,
		
		NULL ,
		NULL ,
		NULL ,
		NULL ,
		NULL ,
		
		ZlangSummarizeDirectPropertySize_double ,
	} ;

ZlangImportObject_(double,Double)

