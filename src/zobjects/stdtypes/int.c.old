#include "libzlang_stdtypes.h"

struct ZlangDirectProperty_int
{
	int			data ;
} ;

ZlangImportObjectFunction ZlangImportObject_int;

ZlangCreateDirectPropertyFunction ZlangCreateDirectProperty_int;
ZlangDestroyDirectPropertyFunction ZlangDestroyDirectProperty_int;

ZlangFromCharPtrFunction ZlangFromCharPtr_int;
ZlangToStringFunction ZlangToString_int;
ZlangFromDataPtrFunction ZlangFromDataPtr_int;
ZlangGetDataPtrFunction ZlangGetDataPtr_int;

ZlangOperatorFunction ZlangOperator_int_PLUS_int;
ZlangOperatorFunction ZlangOperator_int_MINUS_int;
ZlangOperatorFunction ZlangOperator_int_MUL_int;
ZlangOperatorFunction ZlangOperator_int_DIV_int;
ZlangOperatorFunction ZlangOperator_int_MOD_int;

ZlangUnaryOperatorFunction ZlangUnaryOperator_NEGATIVE_int;
ZlangUnaryOperatorFunction ZlangUnaryOperator_PLUS_PLUS_int;
ZlangUnaryOperatorFunction ZlangUnaryOperator_MINUS_MINUS_int;

ZlangCompareFunction ZlangCompare_int_EGUAL_int;
ZlangCompareFunction ZlangCompare_int_NOTEGUAL_int;
ZlangCompareFunction ZlangCompare_int_LT_int;
ZlangCompareFunction ZlangCompare_int_LE_int;
ZlangCompareFunction ZlangCompare_int_GT_int;
ZlangCompareFunction ZlangCompare_int_GE_int;

static struct ZlangDirectFunctions direct_funcs_int =
	{
		ZLANG_OBJECT_int ,
		
		ZlangCreateDirectProperty_int ,
		ZlangDestroyDirectProperty_int ,
		
		ZlangFromCharPtr_int ,
		ZlangToString_int ,
		ZlangFromDataPtr_int ,
		ZlangGetDataPtr_int ,
		
		ZlangOperator_int_PLUS_int ,
		ZlangOperator_int_MINUS_int ,
		ZlangOperator_int_MUL_int ,
		ZlangOperator_int_DIV_int ,
		ZlangOperator_int_MOD_int ,
		
		ZlangUnaryOperator_NEGATIVE_int ,
		ZlangUnaryOperator_PLUS_PLUS_int ,
		ZlangUnaryOperator_MINUS_MINUS_int ,
		
		ZlangCompare_int_EGUAL_int ,
		ZlangCompare_int_NOTEGUAL_int ,
		NULL ,
		ZlangCompare_int_LT_int ,
		ZlangCompare_int_LE_int ,
		ZlangCompare_int_GT_int ,
		ZlangCompare_int_GE_int ,
		
		NULL ,
		NULL ,
	} ;

void *ZlangCreateDirectProperty_int( struct ZlangRuntime *rt , struct ZlangObject *obj , void *copy_direct_prop )
{
	struct ZlangDirectProperty_int	*int_direct_prop = NULL ;
	struct ZlangDirectProperty_int	*copy_int_direct_prop = NULL ;
	
	int_direct_prop = (struct ZlangDirectProperty_int *)malloc( sizeof(struct ZlangDirectProperty_int) ) ;
	if( int_direct_prop == NULL )
	{
		SET_RUNTIME_ERROR( rt , NULL , NULL , RUNTIME_ERROR , ZLANG_ERROR_ALLOC , "alloc memory for entity" )
		return NULL;
	}
	memset( int_direct_prop , 0x00 , sizeof(struct ZlangDirectProperty_int) );
	
	if( copy_direct_prop )
	{
		copy_int_direct_prop = (struct ZlangDirectProperty_int *)copy_direct_prop ;
		int_direct_prop->data = copy_int_direct_prop->data ;
	}
	
	return (void *)int_direct_prop;
}

void ZlangDestroyDirectProperty_int( struct ZlangRuntime *rt , struct ZlangObject *obj , void *direct_prop )
{
	free( direct_prop );
	
	return;
}

struct ZlangObject *ZlangImportObject_int( struct ZlangRuntime *rt )
{
	struct ZlangObject	*obj = NULL ;
	int			nret = 0 ;
	
	obj = CreateObject( rt , ZLANG_OBJECT_int , & direct_funcs_int , NULL , NULL ) ;
	if( obj == NULL )
		return NULL;
	
	nret = ImportObjectToGlobalObjectsHeap( rt , obj ) ;
	if( nret )
	{
		SET_RUNTIME_ERROR( rt , NULL , NULL , RUNTIME_ERROR , ZLANG_ERROR_LINK_FUNC_TO_ENTITY , "import object to global objects heap" )
		DestroyObject( rt , obj );
		return NULL;
	}
	
	return obj ;
}

int ZlangFromCharPtr_int( struct ZlangRuntime *rt , struct ZlangObject *obj , char *data_ptr , int data_len )
{
	struct ZlangDirectProperty_int	*int_direct_prop = GetObjectDirectProperty(obj) ;
	char				buf[100+1] ;
	
	if( data_len < sizeof(buf) )
	{
		memcpy( buf , data_ptr , data_len );
		buf[data_len] = '\0' ;
	}
	else
	{
		memcpy( buf , data_ptr , sizeof(buf)-1 );
		buf[sizeof(buf)-1] = '\0' ;
	}
	
	int_direct_prop->data = atoi(buf) ;
	
	return 0;
}

int ZlangToString_int( struct ZlangRuntime *rt , struct ZlangObject *obj , struct ZlangObject **tostr_obj )
{
	struct ZlangDirectProperty_int	*int_direct_prop = GetObjectDirectProperty(obj) ;
	char				buf[100+1] ;
	int				len ;
	int				nret = 0 ;
	
	(*tostr_obj) = CreateStringObjectInTempStack( rt , NULL , NULL , NULL ) ;
	if( (*tostr_obj) == NULL )
		return ZLANG_ERROR_CREATE_OBJECT;
	
	memset( buf , 0x00 , sizeof(buf) );
	len = snprintf( buf , sizeof(buf)-1 , "%d" , int_direct_prop->data ) ;
	nret = FromCharPtr( rt , (*tostr_obj) , buf , len ) ;
	if( nret )
		return nret;
	
	return 0;
}

int ZlangFromDataPtr_int( struct ZlangRuntime *rt , struct ZlangObject *obj , void *data_ptr , int data_len )
{
	struct ZlangDirectProperty_int	*int_direct_prop = GetObjectDirectProperty(obj) ;
	
	if( data_ptr )
	{
		int_direct_prop->data = *((int*)data_ptr) ;
	}
	
	return 0;
}

int ZlangGetDataPtr_int( struct ZlangRuntime *rt , struct ZlangObject *obj , void **data_ptr , int *data_len )
{
	struct ZlangDirectProperty_int	*int_direct_prop = GetObjectDirectProperty(obj) ;
	
	if( data_ptr )
	{
		(*data_ptr) = & (int_direct_prop->data) ;
	}
	
	if( data_len )
	{
		(*data_len) = sizeof(int_direct_prop->data) ;
	}
	
	return 0;
}

int ZlangOperator_int_PLUS_int( struct ZlangRuntime *rt , struct ZlangObject *in_obj1 , struct ZlangObject *in_obj2 , struct ZlangObject **out_obj )
{
	struct ZlangDirectProperty_int	*in1 = GetObjectDirectProperty(in_obj1) ;
	struct ZlangDirectProperty_int	*in2 = GetObjectDirectProperty(in_obj2) ;
	struct ZlangObject		*o = NULL ;
	struct ZlangDirectProperty_int	*out = NULL ;
	
	o = CreateIntObjectInTempStack( rt , NULL , NULL , NULL ) ;
	if( o == NULL )
		return -1;
	out = GetObjectDirectProperty(o) ;
	
	out->data = in1->data + in2->data ;
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "[%d]+[%d]=>[%d]" , in1->data , in2->data , out->data )
	
	(*out_obj) = o ;
	return 0;
}

int ZlangOperator_int_MINUS_int( struct ZlangRuntime *rt , struct ZlangObject *in_obj1 , struct ZlangObject *in_obj2 , struct ZlangObject **out_obj )
{
	struct ZlangDirectProperty_int	*in1 = GetObjectDirectProperty(in_obj1) ;
	struct ZlangDirectProperty_int	*in2 = GetObjectDirectProperty(in_obj2) ;
	struct ZlangObject		*o = NULL ;
	struct ZlangDirectProperty_int	*out = NULL ;
	
	o = CreateIntObjectInTempStack( rt , NULL , NULL , NULL ) ;
	if( o == NULL )
		return -1;
	out = GetObjectDirectProperty(o) ;
	
	out->data = in1->data - in2->data ;
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "[%d]-[%d]=>[%d]" , in1->data , in2->data , out->data )
	
	(*out_obj) = o ;
	return 0;
}

int ZlangOperator_int_MUL_int( struct ZlangRuntime *rt , struct ZlangObject *in_obj1 , struct ZlangObject *in_obj2 , struct ZlangObject **out_obj )
{
	struct ZlangDirectProperty_int	*in1 = GetObjectDirectProperty(in_obj1) ;
	struct ZlangDirectProperty_int	*in2 = GetObjectDirectProperty(in_obj2) ;
	struct ZlangObject		*o = NULL ;
	struct ZlangDirectProperty_int	*out = NULL ;
	
	o = CreateIntObjectInTempStack( rt , NULL , NULL , NULL ) ;
	if( o == NULL )
		return -1;
	out = GetObjectDirectProperty(o) ;
	
	out->data = in1->data * in2->data ;
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "[%d]*[%d]=>[%d]" , in1->data , in2->data , out->data )
	
	(*out_obj) = o ;
	return 0;
}

int ZlangOperator_int_DIV_int( struct ZlangRuntime *rt , struct ZlangObject *in_obj1 , struct ZlangObject *in_obj2 , struct ZlangObject **out_obj )
{
	struct ZlangDirectProperty_int	*in1 = GetObjectDirectProperty(in_obj1) ;
	struct ZlangDirectProperty_int	*in2 = GetObjectDirectProperty(in_obj2) ;
	struct ZlangObject		*o = NULL ;
	struct ZlangDirectProperty_int	*out = NULL ;
	
	if( in2->data == 0 )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "*** FATAL : division by zero" )
		return ZLANG_ERROR_DIVISION_BY_ZERO;
	}
	
	o = CreateIntObjectInTempStack( rt , NULL , NULL , NULL ) ;
	if( o == NULL )
		return -1;
	out = GetObjectDirectProperty(o) ;
	
	out->data = in1->data / in2->data ;
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "[%d]/[%d]=>[%d]" , in1->data , in2->data , out->data )
	
	(*out_obj) = o ;
	return 0;
}

int ZlangOperator_int_MOD_int( struct ZlangRuntime *rt , struct ZlangObject *in_obj1 , struct ZlangObject *in_obj2 , struct ZlangObject **out_obj )
{
	struct ZlangDirectProperty_int	*in1 = GetObjectDirectProperty(in_obj1) ;
	struct ZlangDirectProperty_int	*in2 = GetObjectDirectProperty(in_obj2) ;
	struct ZlangObject		*o = NULL ;
	struct ZlangDirectProperty_int	*out = NULL ;
	
	o = CreateIntObjectInTempStack( rt , NULL , NULL , NULL ) ;
	if( o == NULL )
		return -1;
	out = GetObjectDirectProperty(o) ;
	
	out->data = in1->data % in2->data ;
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "[%d]%%[%d]=>[%d]" , in1->data , in2->data , out->data )
	
	(*out_obj) = o ;
	return 0;
}

int ZlangUnaryOperator_NEGATIVE_int( struct ZlangRuntime *rt , struct ZlangObject *in_out_obj1 )
{
	struct ZlangDirectProperty_int	*in_out = GetObjectDirectProperty(in_out_obj1) ;
	
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "-[%d]=>[%d]" , in_out->data , -in_out->data )
	in_out->data = -in_out->data ;
	
	return 0;
}

int ZlangUnaryOperator_PLUS_PLUS_int( struct ZlangRuntime *rt , struct ZlangObject *in_out_obj1 )
{
	struct ZlangDirectProperty_int	*in_out = GetObjectDirectProperty(in_out_obj1) ;
	
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "++[%d]=>[%d]" , in_out->data , in_out->data+1 )
	in_out->data = in_out->data + 1 ;
	
	return 0;
}

int ZlangUnaryOperator_MINUS_MINUS_int( struct ZlangRuntime *rt , struct ZlangObject *in_out_obj1 )
{
	struct ZlangDirectProperty_int	*in_out = GetObjectDirectProperty(in_out_obj1) ;
	
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "--[%d]=>[%d]" , in_out->data , in_out->data-1 )
	in_out->data = in_out->data - 1 ;
	
	return 0;
}

int ZlangCompare_int_EGUAL_int( struct ZlangRuntime *rt , struct ZlangObject *in_obj1 , struct ZlangObject *in_obj2 , struct ZlangObject **out_obj )
{
	struct ZlangDirectProperty_int	*in1 = GetObjectDirectProperty(in_obj1) ;
	struct ZlangDirectProperty_int	*in2 = GetObjectDirectProperty(in_obj2) ;
	struct ZlangObject		*b = NULL ;
	
	b = CreateBoolObjectInTempStack( rt , NULL , NULL , NULL ) ;
	if( b == NULL )
		return -1;
	
	if( in1->data == in2->data )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "[%d]==[%d]=>[true]" , in1->data , in2->data )
		CallDirectFunction_SetBoolValue( rt , b , 1 );
	}
	else
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "[%d]==[%d]=>[false]" , in1->data , in2->data )
		CallDirectFunction_SetBoolValue( rt , b , 0 );
	}
	
	if( out_obj )
		(*out_obj) = b ;
	return 0;
}

int ZlangCompare_int_NOTEGUAL_int( struct ZlangRuntime *rt , struct ZlangObject *in_obj1 , struct ZlangObject *in_obj2 , struct ZlangObject **out_obj )
{
	struct ZlangDirectProperty_int	*in1 = GetObjectDirectProperty(in_obj1) ;
	struct ZlangDirectProperty_int	*in2 = GetObjectDirectProperty(in_obj2) ;
	struct ZlangObject		*b = NULL ;
	
	b = CreateBoolObjectInTempStack( rt , NULL , NULL , NULL ) ;
	if( b == NULL )
		return -1;
	
	if( in1->data != in2->data )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "[%d]!=[%d]=>[true]" , in1->data , in2->data )
		CallDirectFunction_SetBoolValue( rt , b , 1 );
	}
	else
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "[%d]!=[%d]=>[false]" , in1->data , in2->data )
		CallDirectFunction_SetBoolValue( rt , b , 0 );
	}
	
	if( out_obj )
		(*out_obj) = b ;
	return 0;
}

int ZlangCompare_int_LT_int( struct ZlangRuntime *rt , struct ZlangObject *in_obj1 , struct ZlangObject *in_obj2 , struct ZlangObject **out_obj )
{
	struct ZlangDirectProperty_int	*in1 = GetObjectDirectProperty(in_obj1) ;
	struct ZlangDirectProperty_int	*in2 = GetObjectDirectProperty(in_obj2) ;
	struct ZlangObject		*b = NULL ;
	
	b = CreateBoolObjectInTempStack( rt , NULL , NULL , NULL ) ;
	if( b == NULL )
		return -1;
	
	if( in1->data < in2->data )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "[%d]<[%d]=>[true]" , in1->data , in2->data )
		CallDirectFunction_SetBoolValue( rt , b , 1 );
	}
	else
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "[%d]<[%d]=>[false]" , in1->data , in2->data )
		CallDirectFunction_SetBoolValue( rt , b , 0 );
	}
	
	if( out_obj )
		(*out_obj) = b ;
	return 0;
}

int ZlangCompare_int_LE_int( struct ZlangRuntime *rt , struct ZlangObject *in_obj1 , struct ZlangObject *in_obj2 , struct ZlangObject **out_obj )
{
	struct ZlangDirectProperty_int	*in1 = GetObjectDirectProperty(in_obj1) ;
	struct ZlangDirectProperty_int	*in2 = GetObjectDirectProperty(in_obj2) ;
	struct ZlangObject		*b = NULL ;
	
	b = CreateBoolObjectInTempStack( rt , NULL , NULL , NULL ) ;
	if( b == NULL )
		return -1;
	
	if( in1->data <= in2->data )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "[%d]<=[%d]=>[true]" , in1->data , in2->data )
		CallDirectFunction_SetBoolValue( rt , b , 1 );
	}
	else
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "[%d]<=[%d]=>[false]" , in1->data , in2->data )
		CallDirectFunction_SetBoolValue( rt , b , 0 );
	}
	
	if( out_obj )
		(*out_obj) = b ;
	return 0;
}

int ZlangCompare_int_GT_int( struct ZlangRuntime *rt , struct ZlangObject *in_obj1 , struct ZlangObject *in_obj2 , struct ZlangObject **out_obj )
{
	struct ZlangDirectProperty_int	*in1 = GetObjectDirectProperty(in_obj1) ;
	struct ZlangDirectProperty_int	*in2 = GetObjectDirectProperty(in_obj2) ;
	struct ZlangObject		*b = NULL ;
	
	b = CreateBoolObjectInTempStack( rt , NULL , NULL , NULL ) ;
	if( b == NULL )
		return -1;
	
	if( in1->data > in2->data )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "[%d]>[%d]=>[true]" , in1->data , in2->data )
		CallDirectFunction_SetBoolValue( rt , b , 1 );
	}
	else
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "[%d]>[%d]=>[false]" , in1->data , in2->data )
		CallDirectFunction_SetBoolValue( rt , b , 0 );
	}
	
	if( out_obj )
		(*out_obj) = b ;
	return 0;
}

int ZlangCompare_int_GE_int( struct ZlangRuntime *rt , struct ZlangObject *in_obj1 , struct ZlangObject *in_obj2 , struct ZlangObject **out_obj )
{
	struct ZlangDirectProperty_int	*in1 = GetObjectDirectProperty(in_obj1) ;
	struct ZlangDirectProperty_int	*in2 = GetObjectDirectProperty(in_obj2) ;
	struct ZlangObject		*b = NULL ;
	
	b = CreateBoolObjectInTempStack( rt , NULL , NULL , NULL ) ;
	if( b == NULL )
		return -1;
	
	if( in1->data >= in2->data )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "[%d]>=[%d]=>[true]" , in1->data , in2->data )
		CallDirectFunction_SetBoolValue( rt , b , 1 );
	}
	else
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "[%d]>=[%d]=>[false]" , in1->data , in2->data )
		CallDirectFunction_SetBoolValue( rt , b , 0 );
	}
	
	if( out_obj )
		(*out_obj) = b ;
	return 0;
}

