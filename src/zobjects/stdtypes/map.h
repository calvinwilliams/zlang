/* Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef _H_ZLANG_STDTYPE_MAP_
#define _H_ZLANG_STDTYPE_MAP_

#include "zobjects_stdtypes.h"

#include "list.h"
#include "rbtree.h"

struct MapNode
{
	uint64_t		map_key_id ;
	struct ZlangObject	*key ;
	struct ZlangObject	*value ;
	
	struct rb_node		node ;
} ;

struct ZlangDirectProperty_map
{
	struct rb_root		map ;
	
	int32_t			map_length ;
} ;

int LinkMapTreeNodeByKeyId( struct ZlangDirectProperty_map *map_direct_prop , struct MapNode *map_node );
void UnlinkMapTreeNode( struct ZlangDirectProperty_map *map_direct_prop , struct MapNode *map_node );
struct MapNode *QueryMapTreeNodeByKeyId( struct ZlangDirectProperty_map *map_direct_prop , struct MapNode *map_node );
struct MapNode *TravelMapTreeNodeByKeyId( struct ZlangDirectProperty_map *map_direct_prop , struct MapNode *map_node );
struct MapNode *TravelPrevMapTreeNodeByKeyId( struct ZlangDirectProperty_map *map_direct_prop , struct MapNode *map_node );
void DestroyMapTree( struct ZlangDirectProperty_map *map_direct_prop );

#endif

