/* Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "zobjects_stdtypes.h"

struct ZlangDirectProperty_ulong
{
	uint64_t		value ;
} ;

#define ATOX		atoll
#define FORMAT		"%"PRIu64""
#define ZERO		0
#include "basetype.tpl2.c"
ZlangCreateDirectProperty_(ulong)
ZlangDestroyDirectProperty_(ulong)
ZlangFromCharPtr_(ulong)
ZlangToString_(ulong)
ZlangFromDataPtr_(ulong,uint64_t)
ZlangGetDataPtr_(ulong)
basetype_SetBasetypeValue(ulong,ULong,uint64_t)
basetype_GetBasetypeValue(ulong,ULong,uint64_t)
ZlangOperator_PLUS_(ulong,ULong)
ZlangOperator_MINUS_(ulong,ULong)
ZlangOperator_MUL_(ulong,ULong)
ZlangOperator_DIV_(ulong,ULong)
ZlangOperatorFunction ZlangOperator_ulong_MOD_ulong;
int ZlangOperator_ulong_MOD_ulong( struct ZlangRuntime *rt , struct ZlangObject *in_obj1 , struct ZlangObject *in_obj2 , struct ZlangObject **out_obj )
{
	struct ZlangDirectProperty_ulong	*in1 = GetObjectDirectProperty(in_obj1) ;
	struct ZlangDirectProperty_ulong	*in2 = GetObjectDirectProperty(in_obj2) ;
	struct ZlangObject		*o = NULL ;
	struct ZlangDirectProperty_ulong	*out = NULL ;
	
	if( in2->value == 0 )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "*** FATAL : division by zero" )
		return ThrowFatalException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_DIVISION_BY_ZERO );
	}
	
	o = CloneULongObjectInTmpStack( rt , NULL ) ;
	if( o == NULL )
		return ThrowFatalException( rt , ZLANG_ERROR_INTERNAL , GetRuntimeErrorString(rt) );
	out = GetObjectDirectProperty(o) ;
	
	out->value = in1->value % in2->value ;
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "["FORMAT"]%%["FORMAT"]=>["FORMAT"]" , in1->value , in2->value , out->value )
	
	(*out_obj) = o ;
	return 0;
}
ZlangUnaryOperator_NOT_(ulong,uint64_t)
ZlangUnaryOperator_BIT_REVERSE_(ulong,uint64_t)
ZlangUnaryOperator_PLUS_PLUS_(ulong)
ZlangUnaryOperator_MINUS_MINUS_(ulong)
ZlangCompare_EGUAL_(ulong)
ZlangCompare_NOTEGUAL_(ulong)
ZlangCompare_LT_(ulong)
ZlangCompare_LE_(ulong)
ZlangCompare_GT_(ulong)
ZlangCompare_GE_(ulong)
ZlangBit_AND_(ulong)
ZlangBit_XOR_(ulong)
ZlangBit_OR_(ulong)
ZlangBit_MOVELEFT_(ulong)
ZlangBit_MOVERIGHT_(ulong)
ZlangSummarizeDirectPropertySize_(ulong)

ZlangCompare_FormatString_string_(ulong)
ZlangCompare_IsBetween_v_v(ulong,ULong,uint64_t)

static struct ZlangDirectFunctions direct_funcs_ulong =
	{
		ZLANG_OBJECT_ulong ,
		
		ZlangCreateDirectProperty_ulong ,
		ZlangDestroyDirectProperty_ulong ,
		
		ZlangFromCharPtr_ulong ,
		ZlangToString_ulong ,
		ZlangFromDataPtr_ulong ,
		ZlangGetDataPtr_ulong ,
		
		ZlangOperator_ulong_PLUS_ulong ,
		ZlangOperator_ulong_MINUS_ulong ,
		ZlangOperator_ulong_MUL_ulong ,
		ZlangOperator_ulong_DIV_ulong ,
		ZlangOperator_ulong_MOD_ulong ,
		
		NULL ,
		ZlangUnaryOperator_NOT_ulong ,
		ZlangUnaryOperator_BIT_REVERSE_ulong ,
		ZlangUnaryOperator_PLUS_PLUS_ulong ,
		ZlangUnaryOperator_MINUS_MINUS_ulong ,
		
		ZlangCompare_ulong_EGUAL_ulong ,
		ZlangCompare_ulong_NOTEGUAL_ulong ,
		ZlangCompare_ulong_LT_ulong ,
		ZlangCompare_ulong_LE_ulong ,
		ZlangCompare_ulong_GT_ulong ,
		ZlangCompare_ulong_GE_ulong ,
		
		NULL ,
		NULL ,
		
		ZlangBit_ulong_AND_ulong ,
		ZlangBit_ulong_XOR_ulong ,
		ZlangBit_ulong_OR_ulong ,
		ZlangBit_ulong_MOVELEFT_int ,
		ZlangBit_ulong_MOVERIGHT_int ,
		
		ZlangSummarizeDirectPropertySize_ulong ,
	} ;

ZlangImportObject_(ulong,ULong)

