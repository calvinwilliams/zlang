/* Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef _H_LIBZLANG_STDTYPES_
#define _H_LIBZLANG_STDTYPES_

#include "zlang.h"

#include <ctype.h>

#include "pcre.h"

#define EXCEPTION_MESSAGE_APPEND_ARRAY_FAILED		"append array failed"
#define EXCEPTION_MESSAGE_INSERT_BEFORE_ARRAY_FAILED	"insert before array failed"
#define EXCEPTION_MESSAGE_MAP_GET_ERROR			"map get failed"

DLLEXPORT ZlangImportObjectsFunction ZlangImportObjects;
int ZlangImportObjects( struct ZlangRuntime *rt );

#endif

