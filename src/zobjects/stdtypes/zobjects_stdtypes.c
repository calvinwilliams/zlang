/* Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "zobjects_stdtypes.h"

ZlangImportObjectFunction ZlangImportObject_string;
ZlangImportObjectFunction ZlangImportObject_bool;
ZlangImportObjectFunction ZlangImportObject_short;
ZlangImportObjectFunction ZlangImportObject_ushort;
ZlangImportObjectFunction ZlangImportObject_int;
ZlangImportObjectFunction ZlangImportObject_uint;
ZlangImportObjectFunction ZlangImportObject_long;
ZlangImportObjectFunction ZlangImportObject_ulong;
ZlangImportObjectFunction ZlangImportObject_float;
ZlangImportObjectFunction ZlangImportObject_double;
ZlangImportObjectFunction ZlangImportObject_functionptr;
ZlangImportObjectFunction ZlangImportObject_array;
ZlangImportObjectFunction ZlangImportObject_list;
ZlangImportObjectFunction ZlangImportObject_list_node;
ZlangImportObjectFunction ZlangImportObject_stack;
ZlangImportObjectFunction ZlangImportObject_queue;
ZlangImportObjectFunction ZlangImportObject_map;
ZlangImportObjectFunction ZlangImportObject_iterator;

#include "charset_GB18030.c"
#include "charset_UTF8.c"

int ZlangImportObjects( struct ZlangRuntime *rt )
{
	struct ZlangObject	*obj = NULL ;
	
	obj = ZlangImportObject_string( rt ) ;
	if( obj == NULL )
		return 1;
	
	obj = ZlangImportObject_bool( rt ) ;
	if( obj == NULL )
		return 1;
	
	obj = ZlangImportObject_short( rt ) ;
	if( obj == NULL )
		return 1;
	
	obj = ZlangImportObject_ushort( rt ) ;
	if( obj == NULL )
		return 1;
	
	obj = ZlangImportObject_int( rt ) ;
	if( obj == NULL )
		return 1;
	
	obj = ZlangImportObject_uint( rt ) ;
	if( obj == NULL )
		return 1;
	
	obj = ZlangImportObject_long( rt ) ;
	if( obj == NULL )
		return 1;
	
	obj = ZlangImportObject_ulong( rt ) ;
	if( obj == NULL )
		return 1;
	
	obj = ZlangImportObject_float( rt ) ;
	if( obj == NULL )
		return 1;
	
	obj = ZlangImportObject_double( rt ) ;
	if( obj == NULL )
		return 1;
	
	obj = ZlangImportObject_functionptr( rt ) ;
	if( obj == NULL )
		return 1;
	
	obj = ZlangImportObject_array( rt ) ;
	if( obj == NULL )
		return 1;
	
	obj = ZlangImportObject_list( rt ) ;
	if( obj == NULL )
		return 1;
	
	obj = ZlangImportObject_list_node( rt ) ;
	if( obj == NULL )
		return 1;
	
	obj = ZlangImportObject_stack( rt ) ;
	if( obj == NULL )
		return 1;
	
	obj = ZlangImportObject_queue( rt ) ;
	if( obj == NULL )
		return 1;
	
	obj = ZlangImportObject_map( rt ) ;
	if( obj == NULL )
		return 1;
	
	obj = ZlangImportObject_iterator( rt ) ;
	if( obj == NULL )
		return 1;
	
	ImportCharsetAlias( rt , g_zlang_charset_aliases_GB18030 );
	ImportCharsetAlias( rt , g_zlang_charset_aliases_UTF8 );
	
	return 0;
}

