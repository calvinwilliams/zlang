/* Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "zobjects_stdtypes.h"

struct ZlangDirectProperty_uint
{
	uint32_t		value ;
} ;

#define ATOX		atol
#define FORMAT		"%"PRIi32""
#define ZERO		0
#include "basetype.tpl2.c"
ZlangCreateDirectProperty_(uint)
ZlangDestroyDirectProperty_(uint)
ZlangFromCharPtr_(uint)
ZlangToString_(uint)
ZlangFromDataPtr_(uint,uint32_t)
ZlangGetDataPtr_(uint)
basetype_SetBasetypeValue(uint,UInt,uint32_t)
basetype_GetBasetypeValue(uint,UInt,uint32_t)
ZlangOperator_PLUS_(uint,UInt)
ZlangOperator_MINUS_(uint,UInt)
ZlangOperator_MUL_(uint,UInt)
ZlangOperator_DIV_(uint,UInt)
ZlangOperatorFunction ZlangOperator_uint_MOD_uint;
int ZlangOperator_uint_MOD_uint( struct ZlangRuntime *rt , struct ZlangObject *in_obj1 , struct ZlangObject *in_obj2 , struct ZlangObject **out_obj )
{
	struct ZlangDirectProperty_uint	*in1 = GetObjectDirectProperty(in_obj1) ;
	struct ZlangDirectProperty_uint	*in2 = GetObjectDirectProperty(in_obj2) ;
	struct ZlangObject		*o = NULL ;
	struct ZlangDirectProperty_uint	*out = NULL ;
	
	if( in2->value == 0 )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "*** FATAL : division by zero" )
		return ThrowFatalException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_DIVISION_BY_ZERO );
	}
	
	o = CloneUIntObjectInTmpStack( rt , NULL ) ;
	if( o == NULL )
		return ThrowFatalException( rt , ZLANG_ERROR_INTERNAL , GetRuntimeErrorString(rt) );
	out = GetObjectDirectProperty(o) ;
	
	out->value = in1->value % in2->value ;
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "["FORMAT"]%%["FORMAT"]=>["FORMAT"]" , in1->value , in2->value , out->value )
	
	(*out_obj) = o ;
	return 0;
}
ZlangUnaryOperator_NOT_(uint,uint32_t)
ZlangUnaryOperator_BIT_REVERSE_(uint,uint32_t)
ZlangUnaryOperator_PLUS_PLUS_(uint)
ZlangUnaryOperator_MINUS_MINUS_(uint)
ZlangCompare_EGUAL_(uint)
ZlangCompare_NOTEGUAL_(uint)
ZlangCompare_LT_(uint)
ZlangCompare_LE_(uint)
ZlangCompare_GT_(uint)
ZlangCompare_GE_(uint)
ZlangBit_AND_(uint)
ZlangBit_XOR_(uint)
ZlangBit_OR_(uint)
ZlangBit_MOVELEFT_(uint)
ZlangBit_MOVERIGHT_(uint)
ZlangSummarizeDirectPropertySize_(uint)

ZlangCompare_FormatString_string_(uint)
ZlangCompare_IsBetween_v_v(uint,UInt,uint32_t)

static struct ZlangDirectFunctions direct_funcs_uint =
	{
		ZLANG_OBJECT_uint ,
		
		ZlangCreateDirectProperty_uint ,
		ZlangDestroyDirectProperty_uint ,
		
		ZlangFromCharPtr_uint ,
		ZlangToString_uint ,
		ZlangFromDataPtr_uint ,
		ZlangGetDataPtr_uint ,
		
		ZlangOperator_uint_PLUS_uint ,
		ZlangOperator_uint_MINUS_uint ,
		ZlangOperator_uint_MUL_uint ,
		ZlangOperator_uint_DIV_uint ,
		ZlangOperator_uint_MOD_uint ,
		
		NULL ,
		ZlangUnaryOperator_NOT_uint ,
		ZlangUnaryOperator_BIT_REVERSE_uint ,
		ZlangUnaryOperator_PLUS_PLUS_uint ,
		ZlangUnaryOperator_MINUS_MINUS_uint ,
		
		ZlangCompare_uint_EGUAL_uint ,
		ZlangCompare_uint_NOTEGUAL_uint ,
		ZlangCompare_uint_LT_uint ,
		ZlangCompare_uint_LE_uint ,
		ZlangCompare_uint_GT_uint ,
		ZlangCompare_uint_GE_uint ,
		
		NULL ,
		NULL ,
		
		ZlangBit_uint_AND_uint ,
		ZlangBit_uint_XOR_uint ,
		ZlangBit_uint_OR_uint ,
		ZlangBit_uint_MOVELEFT_int ,
		ZlangBit_uint_MOVERIGHT_int ,
		
		ZlangSummarizeDirectPropertySize_uint ,
	} ;

ZlangImportObject_(uint,UInt)

