/* Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "zobjects_stdtypes.h"

struct ZlangDirectProperty_ushort
{
	uint16_t	value ;
} ;

#define ATOX		atoi
#define FORMAT		"%"PRIu16""
#define ZERO		0
#include "basetype.tpl2.c"
ZlangCreateDirectProperty_(ushort)
ZlangDestroyDirectProperty_(ushort)
ZlangFromCharPtr_(ushort)
ZlangToString_(ushort)
ZlangFromDataPtr_(ushort,uint16_t)
ZlangGetDataPtr_(ushort)
basetype_SetBasetypeValue(ushort,UShort,uint16_t)
basetype_GetBasetypeValue(ushort,UShort,uint16_t)
ZlangOperator_PLUS_(ushort,UShort)
ZlangOperator_MINUS_(ushort,UShort)
ZlangOperator_MUL_(ushort,UShort)
ZlangOperator_DIV_(ushort,UShort)
ZlangOperatorFunction ZlangOperator_ushort_MOD_ushort;
int ZlangOperator_ushort_MOD_ushort( struct ZlangRuntime *rt , struct ZlangObject *in_obj1 , struct ZlangObject *in_obj2 , struct ZlangObject **out_obj )
{
	struct ZlangDirectProperty_ushort	*in1 = GetObjectDirectProperty(in_obj1) ;
	struct ZlangDirectProperty_ushort	*in2 = GetObjectDirectProperty(in_obj2) ;
	struct ZlangObject			*o = NULL ;
	struct ZlangDirectProperty_ushort	*out = NULL ;
	
	if( in2->value == 0 )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "*** FATAL : division by zero" )
		return ThrowFatalException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_DIVISION_BY_ZERO );
	}
	
	o = CloneShortObjectInTmpStack( rt , NULL ) ;
	if( o == NULL )
		return ThrowFatalException( rt , ZLANG_ERROR_INTERNAL , GetRuntimeErrorString(rt) );
	out = GetObjectDirectProperty(o) ;
	
	out->value = in1->value % in2->value ;
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "["FORMAT"]%%["FORMAT"]=>["FORMAT"]" , in1->value , in2->value , out->value )
	
	(*out_obj) = o ;
	return 0;
}
ZlangUnaryOperator_NEGATIVE_(ushort)
ZlangUnaryOperator_NOT_(ushort,uint16_t)
ZlangUnaryOperator_BIT_REVERSE_(ushort,uint16_t)
ZlangUnaryOperator_PLUS_PLUS_(ushort)
ZlangUnaryOperator_MINUS_MINUS_(ushort)
ZlangCompare_EGUAL_(ushort)
ZlangCompare_NOTEGUAL_(ushort)
ZlangCompare_LT_(ushort)
ZlangCompare_LE_(ushort)
ZlangCompare_GT_(ushort)
ZlangCompare_GE_(ushort)
ZlangBit_AND_(ushort)
ZlangBit_XOR_(ushort)
ZlangBit_OR_(ushort)
ZlangBit_MOVELEFT_(ushort)
ZlangBit_MOVERIGHT_(ushort)
ZlangSummarizeDirectPropertySize_(ushort)

ZlangCompare_FormatString_string_(ushort)
ZlangCompare_IsBetween_v_v(ushort,UShort,uint16_t)

static struct ZlangDirectFunctions direct_funcs_ushort =
	{
		ZLANG_OBJECT_ushort ,
		
		ZlangCreateDirectProperty_ushort ,
		ZlangDestroyDirectProperty_ushort ,
		
		ZlangFromCharPtr_ushort ,
		ZlangToString_ushort ,
		ZlangFromDataPtr_ushort ,
		ZlangGetDataPtr_ushort ,
		
		ZlangOperator_ushort_PLUS_ushort ,
		ZlangOperator_ushort_MINUS_ushort ,
		ZlangOperator_ushort_MUL_ushort ,
		ZlangOperator_ushort_DIV_ushort ,
		ZlangOperator_ushort_MOD_ushort ,
		
		ZlangUnaryOperator_NEGATIVE_ushort ,
		ZlangUnaryOperator_NOT_ushort ,
		ZlangUnaryOperator_BIT_REVERSE_ushort ,
		ZlangUnaryOperator_PLUS_PLUS_ushort ,
		ZlangUnaryOperator_MINUS_MINUS_ushort ,
		
		ZlangCompare_ushort_EGUAL_ushort ,
		ZlangCompare_ushort_NOTEGUAL_ushort ,
		ZlangCompare_ushort_LT_ushort ,
		ZlangCompare_ushort_LE_ushort ,
		ZlangCompare_ushort_GT_ushort ,
		ZlangCompare_ushort_GE_ushort ,
		
		NULL ,
		NULL ,
		
		ZlangBit_ushort_AND_ushort ,
		ZlangBit_ushort_XOR_ushort ,
		ZlangBit_ushort_OR_ushort ,
		ZlangBit_ushort_MOVELEFT_int ,
		ZlangBit_ushort_MOVERIGHT_int ,
		
		ZlangSummarizeDirectPropertySize_ushort ,
	} ;

ZlangImportObject_(ushort,UShort)

