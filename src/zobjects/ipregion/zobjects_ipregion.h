/* Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef _H_LIBZLANG_IPREGION_
#define _H_LIBZLANG_IPREGION_

#include "zlang.h"

#include "libip0region.h"

#define ZLANG_OBJECT_ipregion	"ipregion"

#define EXCEPTION_MESSAGE_LOAD_IPREGION_IMAGE_FAILED	"load ipregion image failed"
#define EXCEPTION_MESSAGE_IPREGION_IMAGE_NOT_LOAD	"ipregion image not load"
#define EXCEPTION_MESSAGE_IPREGION_NOT_FOUND		"ipregion not found"

DLLEXPORT ZlangImportObjectsFunction ZlangImportObjects;
int ZlangImportObjects( struct ZlangRuntime *rt );

#endif

