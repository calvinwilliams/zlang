/* Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "zobjects_ipregion.h"

struct ZlangDirectProperty_ipregion
{
	struct Ip0regionContext		*ctx ;
} ;

ZlangInvokeFunction ZlangInvokeFunction_LoadIpRegionImage_string;
int ZlangInvokeFunction_LoadIpRegionImage_string( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_ipregion	*ipregion_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject			*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	char					*image_filename = NULL ;
	int					nret = 0 ;
	
	CallRuntimeFunction_string_ExpandEnvironmentVar( rt , in1 );
	CallRuntimeFunction_string_GetStringValue( rt , in1 , & image_filename , NULL );
	
	nret = LoadIp0regionImage( image_filename , & (ipregion_direct_prop->ctx) ) ;
	if( nret )
	{
		CallRuntimeFunction_bool_SetBoolValue( rt , out1 , FALSE );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_LOAD_IPREGION_IMAGE_FAILED );
	}
	
	CallRuntimeFunction_bool_SetBoolValue( rt , out1 , TRUE );
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_QueryIpRegion_string_string_string_string_string_string;
int ZlangInvokeFunction_QueryIpRegion_string_string_string_string_string_string( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_ipregion	*ipregion_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject			*in2 = GetInputParameterInLocalObjectStack(rt,2) ;
	struct ZlangObject			*in3 = GetInputParameterInLocalObjectStack(rt,3) ;
	struct ZlangObject			*in4 = GetInputParameterInLocalObjectStack(rt,4) ;
	struct ZlangObject			*in5 = GetInputParameterInLocalObjectStack(rt,5) ;
	struct ZlangObject			*in6 = GetInputParameterInLocalObjectStack(rt,6) ;
	struct ZlangObject			*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	char					*ip_str = NULL ;
	char					*country_name = NULL ;
	char					*area_name = NULL ;
	char					*province_name = NULL ;
	char					*city_name = NULL ;
	char					*isp_name = NULL ;
	int					nret = 0 ;
	
	if( ipregion_direct_prop->ctx == NULL )
	{
		CallRuntimeFunction_bool_SetBoolValue( rt , out1 , FALSE );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_IPREGION_IMAGE_NOT_LOAD );
	}
	
	CallRuntimeFunction_string_GetStringValue( rt , in1 , & ip_str , NULL );
	
	nret = QueryIp0regionImage( ipregion_direct_prop->ctx , ip_str , NULL , NULL , & country_name , & area_name , & province_name , & city_name , & isp_name ) ;
	if( nret )
	{
		CallRuntimeFunction_bool_SetBoolValue( rt , out1 , FALSE );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_IPREGION_NOT_FOUND );
	}
	
	CallRuntimeFunction_string_SetStringValue( rt , in2 , country_name , strlen(country_name) );
	CallRuntimeFunction_string_SetStringValue( rt , in3 , area_name , strlen(area_name) );
	CallRuntimeFunction_string_SetStringValue( rt , in4 , province_name , strlen(province_name) );
	CallRuntimeFunction_string_SetStringValue( rt , in5 , city_name , strlen(city_name) );
	CallRuntimeFunction_string_SetStringValue( rt , in6 , isp_name , strlen(isp_name) );
	
	CallRuntimeFunction_bool_SetBoolValue( rt , out1 , TRUE );
	return 0;
}

ZlangCreateDirectPropertyFunction ZlangCreateDirectProperty_ipregion;
void *ZlangCreateDirectProperty_ipregion( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_ipregion	*ipregion_prop = NULL ;
	
	ipregion_prop = (struct ZlangDirectProperty_ipregion *)ZLMALLOC( sizeof(struct ZlangDirectProperty_ipregion) ) ;
	if( ipregion_prop == NULL )
	{
		SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_ALLOC , "alloc memory for entity" )
		return NULL;
	}
	memset( ipregion_prop , 0x00 , sizeof(struct ZlangDirectProperty_ipregion) );
	
	return ipregion_prop;
}

ZlangDestroyDirectPropertyFunction ZlangDestroyDirectProperty_ipregion;
void ZlangDestroyDirectProperty_ipregion( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_ipregion	*ipregion_direct_prop = GetObjectDirectProperty(obj) ;
	
	if( ipregion_direct_prop->ctx )
	{
		FreeIp0regionImage( & (ipregion_direct_prop->ctx) );
	}
	
	ZLFREE( ipregion_direct_prop );
	
	return;
}

ZlangSummarizeDirectPropertySizeFunction ZlangSummarizeDirectPropertySize_ipregion;
void ZlangSummarizeDirectPropertySize_ipregion( struct ZlangRuntime *rt , struct ZlangObject *obj , size_t *summarized_obj_size , size_t *summarized_direct_prop_size )
{
	SUMMARIZE_SIZE( summarized_direct_prop_size , sizeof(struct ZlangDirectProperty_ipregion) )
	return;
}

static struct ZlangDirectFunctions direct_funcs_ipregion =
	{
		ZLANG_OBJECT_ipregion , /* char *ancestor_name */
		
		ZlangCreateDirectProperty_ipregion , /* ZlangCreateDirectPropertyFunction *create_entity_func */
		ZlangDestroyDirectProperty_ipregion , /* ZlangDestroyDirectPropertyFunction *destroy_entity_func */
		
		NULL , /* ZlangFromCharPtrFunction *from_char_ptr_func */
		NULL , /* ZlangToStringFunction *to_string_func */
		NULL , /* ZlangFromDataPtrFunction *from_data_ptr_func */
		NULL , /* ZlangGetDataPtrFunction *get_data_ptr_func */
		
		NULL , /* ZlangOperatorFunction *oper_PLUS_func */
		NULL , /* ZlangOperatorFunction *oper_MINUS_func */
		NULL , /* ZlangOperatorFunction *oper_MUL_func */
		NULL , /* ZlangOperatorFunction *oper_DIV_func */
		NULL , /* ZlangOperatorFunction *oper_MOD_func */
		
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_NEGATIVE_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_NOT_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_BIT_REVERSE_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_PLUS_PLUS_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_MINUS_MINUS_func */
		
		NULL , /* ZlangCompareFunction *comp_EGUAL_func */
		NULL , /* ZlangCompareFunction *comp_NOTEGUAL_func */
		NULL , /* ZlangCompareFunction *comp_LT_func */
		NULL , /* ZlangCompareFunction *comp_LE_func */
		NULL , /* ZlangCompareFunction *comp_GT_func */
		NULL , /* ZlangCompareFunction *comp_GE_func */
		
		NULL , /* ZlangLogicFunction *logic_AND_func */
		NULL , /* ZlangLogicFunction *logic_OR_func */
		
		NULL , /* ZlangLogicFunction *bit_AND_func */
		NULL , /* ZlangLogicFunction *bit_XOR_func */
		NULL , /* ZlangLogicFunction *bit_OR_func */
		NULL , /* ZlangLogicFunction *bit_MOVELEFT_func */
		NULL , /* ZlangLogicFunction *bit_MOVERIGHT_func */
		
		ZlangSummarizeDirectPropertySize_ipregion , /* ZlangSummarizeDirectPropertySizeFunction *summarize_direct_prop_size_func */
	} ;

ZlangImportObjectFunction ZlangImportObject_ipregion;
struct ZlangObject *ZlangImportObject_ipregion( struct ZlangRuntime *rt )
{
	struct ZlangObject	*obj = NULL ;
	struct ZlangFunction	*func = NULL ;
	int			nret = 0 ;
	
	nret = ImportObject( rt , & obj , ZLANG_OBJECT_ipregion , & direct_funcs_ipregion , sizeof(struct ZlangDirectFunctions) , NULL ) ;
	if( nret )
	{
		SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_LINK_FUNC_TO_ENTITY , "import object to global objects heap" )
		return NULL;
	}
	
	/* ipregion.LoadIpRegionImage(string) */
	func = AddFunctionAndParametersInObject( rt , obj , "LoadIpRegionImage" , "LoadIpRegionImage(string)" , ZlangInvokeFunction_LoadIpRegionImage_string , ZLANG_OBJECT_bool , ZLANG_OBJECT_string,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* ipregion.QueryIpRegion(string,string,string,string,string,string) */
	func = AddFunctionAndParametersInObject( rt , obj , "QueryIpRegion" , "QueryIpRegion(string,string,string,string,string,string)" , ZlangInvokeFunction_QueryIpRegion_string_string_string_string_string_string , ZLANG_OBJECT_bool , ZLANG_OBJECT_string,NULL , ZLANG_OBJECT_string,NULL , ZLANG_OBJECT_string,NULL , ZLANG_OBJECT_string,NULL , ZLANG_OBJECT_string,NULL , ZLANG_OBJECT_string,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	return obj ;
}

