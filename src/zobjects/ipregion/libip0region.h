/* Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef _H_LIBIP0REGION_
#define _H_LIBIP0REGION_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#ifndef STRCMP
#define STRCMP(_a_,_C_,_b_) ( strcmp(_a_,_b_) _C_ 0 )
#define STRNCMP(_a_,_C_,_b_,_n_) ( strncmp(_a_,_b_,_n_) _C_ 0 )
#endif

#define __LIBIP0REGION_VERSION			"1.0.0.0"

#define IP0REGION_ERROR_PARAMETER		-1
#define IP0REGION_ERROR_ALLOC			-11
#define IP0REGION_ERROR_READ_FILE		-111
#define IP0REGION_ERROR_FILEHEADER_MAGIC	-121
#define IP0REGION_ERROR_FILEHEADER_REVERED	-122
#define IP0REGION_ERROR_FILEHEADER_VERSION	-123
#define IP0REGION_ERROR_VERSION			-12
#define IP0REGION_ERROR_FILE_TRUNCATED		-13
#define IP0REGION_ERROR_IP_STR_FORMAT		-14
#define IP0REGION_ERROR_IP_NOT_FOUND		-15

struct Ip0regionContext ;

int LoadIp0regionImage( char *image_filename , struct Ip0regionContext **ctx );
int QueryIp0regionImage( struct Ip0regionContext *ctx , char *ip_str , char **start_ip_str , char **end_ip_str , char **country_name , char **area_name , char **province_name , char **city_name , char **isp_name );
void FreeIp0regionImage( struct Ip0regionContext **ctx );

#endif

