/* Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef _H_LIBIP0REGION_IN_
#define _H_LIBIP0REGION_IN_

#include "libip0region.h"

#include <inttypes.h>
#include <sys/types.h>
#include <sys/stat.h>
#if ( defined __linux ) || ( defined __unix )
#include <unistd.h>
#include <sys/time.h>
#include <arpa/inet.h>
#elif ( defined _WIN32 )
#include <ws2tcpip.h>
#include <windows.h>
#include <winsock.h>
#endif

#if ( defined __linux ) || ( defined __unix )
#define STRDUP  strdup
#define STRNDUP strndup
#elif ( defined _WIN32 )
#define STRDUP  _strdup
#define STRNDUP _strndup
#endif

#ifndef STRCMP
#define STRCMP(_a_,_C_,_b_) ( strcmp(_a_,_b_) _C_ 0 )
#define STRNCMP(_a_,_C_,_b_,_n_) ( strncmp(_a_,_b_,_n_) _C_ 0 )
#endif

#ifndef STRICMP
#if ( defined _WIN32 )
#define STRICMP(_a_,_C_,_b_) ( _stricmp(_a_,_b_) _C_ 0 )
#define STRNICMP(_a_,_C_,_b_,_n_) ( _strnicmp(_a_,_b_,_n_) _C_ 0 )
#define STRISTR(_a_,_C_,_b_) ( _stristr(_a_,_b_) _C_ 0 )
#elif ( defined __unix ) || ( defined _AIX ) || ( defined __linux__ )
#define STRICMP(_a_,_C_,_b_) ( strcasecmp(_a_,_b_) _C_ 0 )
#define STRNICMP(_a_,_C_,_b_,_n_) ( strncasecmp(_a_,_b_,_n_) _C_ 0 )
char *strcasestr(const char *haystack, const char *needle);
#define STRISTR(_a_,_b_) ( strcasestr(_a_,_b_) )
#endif
#endif

#ifndef MEMCMP
#define MEMCMP(_a_,_C_,_b_,_n_) ( memcmp(_a_,_b_,_n_) _C_ 0 )
#endif

#define IP0REGION_V1					1

#define IP0REGION_FILEHEADER_MAGIC_STRING		"IPRG"

#define IP0REGION_V1_FILEHEADER_SIZE			16
	#define IP0REGION_V1_MAGIC_OFFSET		0
	#define IP0REGION_V1_MAGIC_LENGTH		sizeof(IP0REGION_FILEHEADER_MAGIC_STRING)-1
	#define IP0REGION_V1_REVERVED_OFFSET		4
	#define IP0REGION_V1_REVERVED_LENGTH		2
	#define IP0REGION_V1_VERSION_OFFSET		6
	#define IP0REGION_V1_VERSION_LENGTH		2
	#define IP0REGION_V1_REGION_SECTION_OFFSET	8
	#define IP0REGION_V1_REGION_SECTION_LENGTH	4
	#define IP0REGION_V1_IP_SECTION_OFFSET		12
	#define IP0REGION_V1_IP_SECTION_LENGTH		4

#define IP0REGION_V1_REGIONHEADER_SIZE			5
	
#define IP0REGION_V1_IPBLOCK_SIZE			12
	#define IP0REGION_V1_START_IP_OFFSET		0
	#define IP0REGION_V1_END_IP_OFFSET		4
	#define IP0REGION_V1_REGION_OFFSET		8

#endif

