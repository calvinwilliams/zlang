#ifndef _H_FASTERTPL_
#define _H_FASTERTPL_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdarg.h>
#if defined(__linux__)
#include <unistd.h>
#elif defined(_WIN32)
#include <windows.h>
#include <io.h>
#endif

#ifndef TLS
#if ( defined __linux__ ) || ( defined __unix ) || ( defined _AIX )
#define TLS             __thread
#elif ( defined _WIN32 )
#define TLS             __declspec(thread)
#endif
#endif

int vasprintf(char **strp, const char *fmt, va_list ap);

extern char	*g_FASTERTPL_VERSION ;

struct FasterTempate ;
struct FasterSection ;
struct FasterDetail ;

struct FasterTempate *FTCreateTemplate( char *template_filename );

struct FasterSection *FTCreateSection( struct FasterSection *parent_section , char *section_name );
int FTSetShort( struct FasterSection *section , char *name , short s );
int FTSetInt( struct FasterSection *section , char *name , int i );
int FTSetLong( struct FasterSection *section , char *name , long l );
int FTSetFloat( struct FasterSection *section , char *name , float f );
int FTSetDouble( struct FasterSection *section , char *name , double d );
int FTSetChar( struct FasterSection *section , char *name , char ch );
int FTSetString( struct FasterSection *section , char *name , char *str );
int FTSetFormat( struct FasterSection *section , char *name , char *format , ... );

struct FasterSection *FTCreateDetail( struct FasterSection *parent_section , char *detail_name );

char *FTInstantiateTemplate( struct FasterTempate *tpl , struct FasterSection *section );

void FTDestroySection( struct FasterSection *section );

void FTDestroyTemplate( struct FasterTempate *tpl );

int FTGetLastErrorLine();

#ifdef __cplusplus
extern }
#endif

#endif

