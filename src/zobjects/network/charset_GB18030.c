/* Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

static struct ZlangCharsetAlias		g_zlang_charset_aliases_GB18030[] = {
		{ ZLANG_CHARSET_GB18030 , "设置连接超时" , "SetConnectingTimeout" } ,
		{ ZLANG_CHARSET_GB18030 , "设置数据传输超时" , "SetDataTransmissionTimeout" } ,
		{ ZLANG_CHARSET_GB18030 , "得到连接耗时" , "GetConnectingElapse" } ,
		{ ZLANG_CHARSET_GB18030 , "得到数据传输耗时" , "GetDataTransmissionElapse" } ,
		{ ZLANG_CHARSET_GB18030 , "创建侦听" , "Listen" } ,
		{ ZLANG_CHARSET_GB18030 , "接受新连接" , "Accept" } ,
		{ ZLANG_CHARSET_GB18030 , "连接" , "Connect" } ,
		{ ZLANG_CHARSET_GB18030 , "关闭" , "Close" } ,
		{ ZLANG_CHARSET_GB18030 , "得到IP" , "GetIp" } ,
		{ ZLANG_CHARSET_GB18030 , "得到端口" , "GetPort" } ,
		{ ZLANG_CHARSET_GB18030 , "发送" , "Send" } ,
		{ ZLANG_CHARSET_GB18030 , "发送含换行" , "Sendln" } ,
		{ ZLANG_CHARSET_GB18030 , "接收" , "Receive" } ,
		{ ZLANG_CHARSET_GB18030 , "按换行接收" , "Receiveln" } ,
		{ ZLANG_CHARSET_GB18030 , "设置HTTP请求地址" , "SetHttpRequestUri" } ,
		{ ZLANG_CHARSET_GB18030 , "设置HTTP请求头" , "SetHttpRequestHeader" } ,
		{ ZLANG_CHARSET_GB18030 , "设置HTTP请求体" , "SetHttpRequestBody" } ,
		{ ZLANG_CHARSET_GB18030 , "发起HTTP请求" , "RequestHttp" } ,
		{ ZLANG_CHARSET_GB18030 , "得到HTTP响应状态" , "GetHttpResponseStatus" } ,
		{ ZLANG_CHARSET_GB18030 , "得到HTTP响应头" , "GetHttpResponseHeader" } ,
		{ ZLANG_CHARSET_GB18030 , "得到HTTP响应体" , "GetHttpResponseBody" } ,
		{ ZLANG_CHARSET_GB18030 , "接收HTTP请求" , "ReceiveHttpRequest" } ,
		{ ZLANG_CHARSET_GB18030 , "得到HTTP请求头" , "GetHttpRequestHeader" } ,
		{ ZLANG_CHARSET_GB18030 , "得到HTTP请求体" , "GetHttpRequestBody" } ,
		{ ZLANG_CHARSET_GB18030 , "设置HTTP响应状态" , "SetHttpResponseStatus" } ,
		{ ZLANG_CHARSET_GB18030 , "设置HTTP响应头" , "SetHttpResponseHeader" } ,
		{ ZLANG_CHARSET_GB18030 , "设置HTTP响应体" , "SetHttpResponseBody" } ,
		{ ZLANG_CHARSET_GB18030 , "发送HTTP响应" , "SendHttpResponse" } ,
		{ ZLANG_CHARSET_GB18030 , "发起Get请求" , "Get" } ,
		{ ZLANG_CHARSET_GB18030 , "发起Post请求" , "Post" } ,
		{ ZLANG_CHARSET_GB18030 , "设置侦听地址" , "SetListenAddress" } ,
		{ ZLANG_CHARSET_GB18030 , "设置域名" , "SetDomain" } ,
		{ ZLANG_CHARSET_GB18030 , "设置网站主目录" , "SetWwwroot" } ,
		{ ZLANG_CHARSET_GB18030 , "设置索引页面" , "SetIndex" } ,
		{ ZLANG_CHARSET_GB18030 , "设置动态页面扩展名" , "SetSocgiType" } ,
		{ ZLANG_CHARSET_GB18030 , "设置日志文件名" , "SetLogFile" } ,
		{ ZLANG_CHARSET_GB18030 , "跑起来" , "Run" } ,
		{ ZLANG_CHARSET_GB18030 , "得到HTTP请求地址" , "GetHttpRequestUri" } ,
		{ ZLANG_CHARSET_GB18030 , "得到HTTP请求目录" , "GetHttpRequestUriPath" } ,
		{ ZLANG_CHARSET_GB18030 , "得到HTTP请求" , "GetHttpRequest" } ,
		{ ZLANG_CHARSET_GB18030 , "得到HTTP请求地址参数" , "GetHttpRequestUriParameters" } ,
		{ ZLANG_CHARSET_GB18030 , "得到HTTP请求Post数据" , "GetHttpRequestPost" } ,
		{ ZLANG_CHARSET_GB18030 , "得到HTTP请求Cookie数据" , "GetHttpRequestCookies" } ,
		{ ZLANG_CHARSET_GB18030 , "设置HTTP响应" , "SetHttpResponse" } ,
		{ ZLANG_CHARSET_GB18030 , "读文件到HTTP响应" , "SetHttpResponseFromFile" } ,
		{ ZLANG_CHARSET_GB18030 , "实例化HTML模板到HTTP响应" , "SetHttpResponseFromHtmlTemplate" } ,
		{ ZLANG_CHARSET_GB18030 , "格式化HTTP响应" , "FormatHttpResponse" } ,
		{ ZLANG_CHARSET_GB18030 , "写HTTP响应头" , "WriteHttpResponseHeader" } ,
		{ ZLANG_CHARSET_GB18030 , "写HTTP响应Cookie" , "WriteHttpResponseCookie" } ,
		{ ZLANG_CHARSET_GB18030 , "写HTTP响应体" , "WriteHttpResponseBody" } ,
		{ ZLANG_CHARSET_GB18030 , "格式化写HTTP响应体" , "FormatWriteHttpResponseBody" } ,
		{ ZLANG_CHARSET_GB18030 , "写HTTP响应完成" , "WriteHttpResponseEnd" } ,
		{ 0 , NULL , NULL } ,
	} ;

