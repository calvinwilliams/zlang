/*
 * hetao - High Performance Web Server
 * author	: calvin
 * email	: calvinwilliams@163.com
 *
 * Licensed under the LGPL v2.1, see the file LICENSE in base directory.
 */

#include "hetao_in.h"

static int SelectAndConnectForwardServer( struct HetaoEnv *p_env , struct HttpSession *p_http_session )
{
#if ( defined __linux ) || ( defined __unix )
	struct epoll_event	event ;
#elif ( defined _WIN32 )
#endif

	int			nret = 0 ;

	while(1)
	{
		/* 选择转发服务端 */
		nret = SelectForwardAddress( p_env , p_http_session ) ;
		if( nret == HTTP_OK )
		{
			/* 连接转发服务端 */
			nret = ConnectForwardServer( p_env , p_http_session ) ;
			if( nret == HTTP_OK )
			{
#if ( defined __linux ) || ( defined __unix )
				/* 暂禁原连接事件 */
				memset( & event , 0x00 , sizeof(struct epoll_event) );
				event.events = EPOLLRDHUP | EPOLLERR ;
				event.data.ptr = p_http_session ;
				nret = epoll_ctl( p_env->p_this_process_info->epoll_fd , EPOLL_CTL_MOD , p_http_session->netaddr.sock , & event ) ;
				if( nret == -1 )
				{
					ErrorLogc( __FILE__ , __LINE__ , "epoll_ctl failed , errno[%d]" , ERRNO );
					return HTTP_INTERNAL_SERVER_ERROR;
				}
#endif
				
				return 0;
			}
			else
			{
				ErrorLogc( __FILE__ , __LINE__ , "ConnectForwardServer failed[%d] , errno[%d]" , nret , ERRNO );
				return HTTP_SERVICE_UNAVAILABLE;
			}
		}
		else
		{
			ErrorLogc( __FILE__ , __LINE__ , "SelectForwardAddress failed[%d] , errno[%d]" , nret , ERRNO );
			return HTTP_SERVICE_UNAVAILABLE;
		}
	}
}

static int CallHttpApplication( struct HetaoEnv *p_env , struct HttpSession *p_http_session )
{
	struct SoCgi	*p_socgi = & (p_http_session->p_uri_location->socgi) ;
	
	int		nret = 0 ;

	InfoLogc( __FILE__ , __LINE__ , "call pfuncCallHttpApplication[%p] ..." , p_socgi->pfuncCallHttpApplication );
	nret = p_socgi->pfuncCallHttpApplication( & (p_socgi->http_application_context) ) ;
	if( nret < 0 )
	{
		FatalLogc( __FILE__ , __LINE__ , "call pfuncCallHttpApplication[%p] failed[%d]" , p_socgi->pfuncCallHttpApplication , nret );
		exit(1);
	}
	else
	{
		InfoLogc( __FILE__ , __LINE__ , "call pfuncCallHttpApplication[%p] ok[%d]" , p_socgi->pfuncCallHttpApplication , nret );
		return nret;
	}
	/*
	else
	{
		ErrorLogc( __FILE__ , __LINE__ , "call pfuncCallHttpApplication[%p] failed[%d]" , p_socgi->pfuncCallHttpApplication , nret );
		return HTTP_INTERNAL_SERVER_ERROR;
	}
	*/
}

/* 压缩HTTP响应 */
static int CompressData( char *html_content , int html_content_len , int compress_algorithm , char **html_gzip_content , int *html_gzip_content_len )
{
	uLong		in_len ;
	uLong		out_len ;
	Bytef		*out_base = NULL ;
	z_stream	stream ;
	
	int		nret = 0 ;
	
	stream.zalloc = NULL ;
	stream.zfree = NULL ;
	stream.opaque = NULL ;
	nret = deflateInit2( &stream , Z_BEST_COMPRESSION , Z_DEFLATED , compress_algorithm , MAX_MEM_LEVEL , Z_DEFAULT_STRATEGY ) ;
	if( nret != Z_OK )
	{
		ErrorLogc( __FILE__ , __LINE__ , "deflateInit2 failed , errno[%d]" , ERRNO );
		return HTTP_INTERNAL_SERVER_ERROR;
	}
	
	in_len = html_content_len ;
	out_len = deflateBound( &stream , in_len ) ;
	out_base = (Bytef *)malloc( out_len+1 ) ;
	if( out_base == NULL )
	{
		ErrorLogc( __FILE__ , __LINE__ , "malloc failed , errno[%d]" , ERRNO );
		deflateEnd( &stream );
		return HTTP_INTERNAL_SERVER_ERROR;
	}
	
	stream.next_in = (Bytef*)html_content ;
	stream.avail_in = in_len ;
	stream.next_out = out_base ;
	stream.avail_out = out_len ;
	nret = deflate( &stream , Z_FINISH ) ;
	if( nret != Z_OK && nret != Z_STREAM_END )
	{
		ErrorLogc( __FILE__ , __LINE__ , "malloc failed , errno[%d]" , ERRNO );
		free( out_base );
		deflateEnd( &stream );
		return HTTP_INTERNAL_SERVER_ERROR;
	}
	
	(*html_gzip_content) = (char*)out_base ;
	(*html_gzip_content_len) = stream.total_out ;
	
	return 0;
}

extern struct MimeType		__hetao_mimetype_octet_stream ;

static int ProcessHttpAcceptEncoding( struct HetaoEnv *p_env , struct HttpSession *p_http_session , struct HtmlCacheSession *p_htmlcache_session )
{
	struct MimeType		*p_mime_type = NULL ;
	struct HttpBuffer	*b = NULL ;
	char			*token_base = NULL ;
	char			*p_compress_algorithm = NULL ;
	int			compress_algorithm_len ;
	
	int			nret = 0 ;

	/* 查询流类型 */
	p_mime_type = QueryMimeTypeHashNode( p_env , p_http_session->http_uri.ext_filename_base , p_http_session->http_uri.ext_filename_len ) ;
	if( p_mime_type == NULL )
	{
		/*
		ErrorLogc( __FILE__ , __LINE__ , "QueryMimeTypeHashNode[%.*s] failed" , p_http_session->http_uri.ext_filename_len,p_http_session->http_uri.ext_filename_base );
		return HTTP_FORBIDDEN;
		*/
		WarnLogc( __FILE__ , __LINE__ , "QueryMimeTypeHashNode[%.*s] failed" , p_http_session->http_uri.ext_filename_len,p_http_session->http_uri.ext_filename_base );
		p_mime_type = & __hetao_mimetype_octet_stream ;
	}
	else
	{
		DebugLogc( __FILE__ , __LINE__ , "QueryMimeTypeHashNode[%.*s] ok" , p_http_session->http_uri.ext_filename_len,p_http_session->http_uri.ext_filename_base );
	}
	
	/* 解析浏览器可以接受的压缩算法 */
	b = GetHttpResponseBuffer(p_http_session->http) ;
	token_base = QueryHttpHeaderPtr( p_http_session->http , HTTP_HEADER_ACCEPTENCODING , NULL ) ;
	while( token_base && p_env->http_options__compress_on )
	{
		token_base = TokenHttpHeaderValue( token_base , & p_compress_algorithm , & compress_algorithm_len ) ;
		if( p_compress_algorithm )
		{
			if( p_mime_type->compress_enable == 1 && compress_algorithm_len == 4 && STRNICMP( p_compress_algorithm , == , "gzip" , compress_algorithm_len ) )
			{
				if( p_htmlcache_session->html_gzip_content == NULL )
				{
					/* 按gzip压缩 */
					nret = CompressData( p_htmlcache_session->html_content , p_htmlcache_session->html_content_len , HTTP_COMPRESSALGORITHM_GZIP , &(p_htmlcache_session->html_gzip_content) , &(p_htmlcache_session->html_gzip_content_len) ) ;
					if( nret )
					{
						ErrorLogc( __FILE__ , __LINE__ , "CompressBuffer HTTP_COMPRESSALGORITHM_GZIP failed , errno[%d]" , ERRNO );
						return nret;
					}
					else
					{
						DebugLogc( __FILE__ , __LINE__ , "CompressBuffer HTTP_COMPRESSALGORITHM_GZIP ok , [%d]bytes" , p_htmlcache_session->html_gzip_content_len );
					}
				}
				else
				{
					DebugLogc( __FILE__ , __LINE__ , "gzip[%s] cache hited , [%d]bytes" , p_htmlcache_session->full_pathfilename , p_htmlcache_session->html_gzip_content_len );
				}
				
				/* 组织HTTP响应 */
				nret = StrcatfHttpBuffer( b ,	"Server: hetao/%s\r\n"
								"Content-Type: %s\r\n"
								"Content-Encoding: gzip\r\n"
								"Content-Length: %d\r\n"
								"%s"
								"\r\n"
								, __HETAO_VERSION
								, p_mime_type->mime
								, p_htmlcache_session->html_gzip_content_len
								, CheckHttpKeepAlive(p_http_session->http)?"Connection: Keep-Alive\r\n":"" ) ;
				if( nret )
				{
					ErrorLogc( __FILE__ , __LINE__ , "StrcatfHttpBuffer failed , errno[%d]" , ERRNO );
					return HTTP_INTERNAL_SERVER_ERROR;
				}
				
				if( GetHttpHeader_METHOD(p_http_session->http) == HTTP_METHOD_GET_N )
				{
					/*
					nret = MemcatHttpBuffer( b , p_htmlcache_session->html_gzip_content , p_htmlcache_session->html_gzip_content_len ) ;
					if( nret )
					{
						ErrorLogc( __FILE__ , __LINE__ , "MemcatHttpBuffer failed , errno[%d]" , ERRNO );
						return HTTP_INTERNAL_SERVER_ERROR;
					}
					*/
					SetHttpBufferPtr( p_http_session->http_buf , p_htmlcache_session->html_gzip_content_len+1 , p_htmlcache_session->html_gzip_content );
					AppendHttpBuffer( p_http_session->http , p_http_session->http_buf );
				}
				
				break;
			}
			else if( p_mime_type->compress_enable == 1 && compress_algorithm_len == 7 && STRNICMP( p_compress_algorithm , == , "deflate" , compress_algorithm_len ) )
			{
				if( p_htmlcache_session->html_deflate_content == NULL )
				{
					/* 按deflate压缩 */
					nret = CompressData( p_htmlcache_session->html_content , p_htmlcache_session->html_content_len , HTTP_COMPRESSALGORITHM_DEFLATE , &(p_htmlcache_session->html_deflate_content) , &(p_htmlcache_session->html_deflate_content_len) ) ;
					if( nret )
					{
						ErrorLogc( __FILE__ , __LINE__ , "CompressBuffer HTTP_COMPRESSALGORITHM_DEFLATE failed , errno[%d]" , ERRNO );
						return nret;
					}
					else
					{
						DebugLogc( __FILE__ , __LINE__ , "CompressBuffer HTTP_COMPRESSALGORITHM_DEFLATE ok , [%d]bytes" , p_htmlcache_session->html_deflate_content_len );
					}
				}
				else
				{
					DebugLogc( __FILE__ , __LINE__ , "deflate[%s] cache hited , [%d]bytes" , p_htmlcache_session->full_pathfilename , p_htmlcache_session->html_deflate_content_len );
				}
				
				/* 组织HTTP响应 */
				nret = StrcatfHttpBuffer( b ,	"Server: hetao/%s\r\n"
								"Content-Type: %s\r\n"
								"Content-Encoding: deflate\r\n"
								"Content-Length: %d\r\n"
								"%s"
								"\r\n"
								, __HETAO_VERSION
								, p_mime_type->mime
								, p_htmlcache_session->html_deflate_content_len
								, CheckHttpKeepAlive(p_http_session->http)?"Connection: Keep-Alive\r\n":"" ) ;
				if( nret )
				{
					ErrorLogc( __FILE__ , __LINE__ , "StrcatfHttpBuffer failed , errno[%d]" , ERRNO );
					return HTTP_INTERNAL_SERVER_ERROR;
				}
				
				if( GetHttpHeader_METHOD(p_http_session->http) == HTTP_METHOD_GET_N )
				{
					SetHttpBufferPtr( p_http_session->http_buf , p_htmlcache_session->html_deflate_content_len+1 , p_htmlcache_session->html_deflate_content );
					AppendHttpBuffer( p_http_session->http , p_http_session->http_buf );
				}
				
				break;
			}
		}
	}
	if( token_base == NULL )
	{
		/* 组织HTTP响应。未压缩 */
		nret = StrcatfHttpBuffer( b ,	"Server: hetao/%s\r\n"
						"Content-Type: %s\r\n"
						"Content-Length: %d\r\n"
						"%s"
						"\r\n"
						, __HETAO_VERSION
						, p_mime_type->mime
						, p_htmlcache_session->html_content_len
						, CheckHttpKeepAlive(p_http_session->http)?"Connection: Keep-Alive\r\n":"" ) ;
		if( nret )
		{
			ErrorLogc( __FILE__ , __LINE__ , "StrcatfHttpBuffer failed , errno[%d]" , ERRNO );
			return HTTP_INTERNAL_SERVER_ERROR;
		}
		
		if( GetHttpHeader_METHOD(p_http_session->http) == HTTP_METHOD_GET_N )
		{
			/*
			nret = MemcatHttpBuffer( b , p_htmlcache_session->html_content , p_htmlcache_session->html_content_len ) ;
			if( nret )
			{
				ErrorLogc( __FILE__ , __LINE__ , "MemcatHttpBuffer failed , errno[%d]" , ERRNO );
				return HTTP_INTERNAL_SERVER_ERROR;
			}
			*/
			SetHttpBufferPtr( p_http_session->http_buf , p_htmlcache_session->html_content_len+1 , p_htmlcache_session->html_content );
			AppendHttpBuffer( p_http_session->http , p_http_session->http_buf );
		}
	}

	return HTTP_OK;
}

static int GetHttpResource( struct HetaoEnv *p_env , struct HttpSession *p_http_session , char *wwwroot , char *uri , int uri_len )
{
	struct UriLocation	*p_uri_location = p_http_session->p_uri_location ;
	struct SoCgi		*p_socgi = & (p_uri_location->socgi) ;
	
	char			full_pathfilename[ 1024 + 1 ] ;
	int			full_pathfilename_len ;
	
	struct HtmlCacheSession	htmlcache_session ;
	struct HtmlCacheSession	*p_htmlcache_session = NULL ;
	struct HtmlCacheSession	*p_need_watch_file = NULL ;
	
	int			nret = 0 ;
	
	/* 自定义处理HTTP */
	if( p_uri_location->socgi.pfuncGetHttpResource )
	{
		InfoLogc( __FILE__ , __LINE__ , "call pfuncGetHttpResource[%p] ..." , p_socgi->pfuncGetHttpResource );
		nret = p_uri_location->socgi.pfuncGetHttpResource( & (p_uri_location->socgi.http_application_context) ) ;
		if( nret == HTTP_OK )
		{
			InfoLogc( __FILE__ , __LINE__ , "call pfuncGetHttpResource return[%d]" , nret );
			return nret;
		}
		else
		{
			ErrorLogc( __FILE__ , __LINE__ , "call pfuncGetHttpResource return[%d]" , nret );
			return nret;
		}
	}
	else
	{
		/* 组装URL */
		memset( full_pathfilename , 0x00 , sizeof(full_pathfilename) );
		SNPRINTF( full_pathfilename , sizeof(full_pathfilename)-1 , "%s%.*s" , wwwroot , uri_len,uri );
		if( strstr( full_pathfilename , ".." ) )
		{
			WarnLogc( __FILE__ , __LINE__ , "URI[%s%.*s] include \"..\"" , wwwroot , uri_len,uri );
			return HTTP_BAD_REQUEST;
		}
		full_pathfilename_len = (int)strlen(full_pathfilename) ;
		
		/* 查询网页缓存 */
		p_htmlcache_session = QueryHtmlCachePathfilenameTreeNode( p_env , full_pathfilename ) ;
		p_need_watch_file = p_htmlcache_session ;
		if( p_htmlcache_session == NULL )
		{
			memset( & htmlcache_session , 0x00 , sizeof(struct HtmlCacheSession) );
			p_htmlcache_session = & htmlcache_session ;
			
			p_htmlcache_session->type = DATASESSION_TYPE_HTMLCACHE ;
			
			p_htmlcache_session->full_pathfilename = STRDUP( full_pathfilename ) ;
			if( p_htmlcache_session->full_pathfilename == NULL )
			{
				ErrorLogc( __FILE__ , __LINE__ , "strdup failed , errno[%d]" , ERRNO );
				return HTTP_INTERNAL_SERVER_ERROR;
			}
			p_htmlcache_session->full_pathfilename_len = full_pathfilename_len ;
			
			/* 判断是目录还是文件 */
			nret = STAT( full_pathfilename , & (p_htmlcache_session->st) ) ;
			if( nret == -1 )
			{
				ErrorLogc( __FILE__ , __LINE__ , "stat[%s] failed , errno[%d]" , full_pathfilename , ERRNO );
				return HTTP_NOT_FOUND;
			}
			else
			{
				DebugLogc( __FILE__ , __LINE__ , "stat[%s] ok" , full_pathfilename );
			}
			
			if( STAT_DIRECTORY(p_htmlcache_session->st) )
			{
				/* 如果是目录，尝试所有索引文件 */
				char	index_copy[ sizeof( ((hetao_conf*)0)->listen[0].website[0].index ) ] ;
				char	*index_filename = NULL ;
				char	append_index_filename_to_uri[ MAX_URI_LENGTH ] ;
				int	append_index_filename_to_uri_len ;
				
				if( p_http_session->p_virtual_host->index[0] == '\0' )
					return HTTP_NOT_FOUND;
				
				strcpy( index_copy , p_http_session->p_virtual_host->index );
				index_filename = strtok( index_copy , "," ) ;
				while( index_filename )
				{
					memset( append_index_filename_to_uri , 0x00 , sizeof(append_index_filename_to_uri) );
					append_index_filename_to_uri_len = SNPRINTF( append_index_filename_to_uri , sizeof(append_index_filename_to_uri)-1 , "%.*s%s" , uri_len,uri , index_filename ) ;
					if( append_index_filename_to_uri_len <= 0 || append_index_filename_to_uri_len == uri_len )
					{
						ErrorLogc( __FILE__ , __LINE__ , "[%.*s]+[%s] too long" , uri_len,uri , index_filename );
						return HTTP_URI_TOO_LONG;
					}
					
					DebugLogc( __FILE__ , __LINE__ , "ProcessHttpResource wwwroot[%s] append_index_filename_to_uri[%.*s]" , wwwroot , append_index_filename_to_uri_len,append_index_filename_to_uri );
					nret = ProcessHttpResource( p_env , p_http_session , wwwroot , append_index_filename_to_uri , append_index_filename_to_uri_len ) ;
					DebugLogc( __FILE__ , __LINE__ , "ProcessHttpResource wwwroot[%s] append_index_filename_to_uri[%.*s] return [%d]" , wwwroot , append_index_filename_to_uri_len,append_index_filename_to_uri , nret );
					if( nret == HTTP_OK )
						return HTTP_OK;
					else if( nret == 0 )
						return 0;
					else if( nret != HTTP_NOT_FOUND )
						return nret;
					
					index_filename = strtok( NULL , "," ) ;
				}
				
				ErrorLogc( __FILE__ , __LINE__ , "index[%s] not found in wwwroot[%s] uri[%.*s]" , p_http_session->p_virtual_host->index , p_http_session->p_virtual_host->wwwroot , uri_len,uri );
				return HTTP_NOT_FOUND;
			}
			else
			{
				/* 缓存里没有 */
				FILE		*fp = NULL ;
				
				/* 打开文件，或目录 */
				fp = fopen( full_pathfilename , "rb" ) ;
				if( fp == NULL )
				{
					ErrorLogc( __FILE__ , __LINE__ , "fopen[%s] failed , errno[%d]" , full_pathfilename , ERRNO );
					return HTTP_NOT_FOUND;
				}
				else
				{
					DebugLogc( __FILE__ , __LINE__ , "fopen[%s] ok" , full_pathfilename );
				}
				
				/* 是文件，读取整个文件 */
				p_htmlcache_session->html_content_len = (int)(p_htmlcache_session->st.st_size) ;
				p_htmlcache_session->html_content = (char*)malloc( p_htmlcache_session->html_content_len+1 ) ;
				if( p_htmlcache_session->html_content == NULL )
				{
					ErrorLogc( __FILE__ , __LINE__ , "malloc failed , errno[%d]" , ERRNO );
					free( p_htmlcache_session->full_pathfilename );
					return HTTP_INTERNAL_SERVER_ERROR;
				}
				
				nret = (int)fread( p_htmlcache_session->html_content , p_htmlcache_session->html_content_len , 1 , fp ) ;
				if( nret != 1 && p_htmlcache_session->html_content_len > 0 )
				{
					ErrorLogc( __FILE__ , __LINE__ , "fread failed , errno[%d]" , ERRNO );
					free( p_htmlcache_session->full_pathfilename );
					free( p_htmlcache_session->html_content );
					return HTTP_INTERNAL_SERVER_ERROR;
				}
				else
				{
					DebugLogc( __FILE__ , __LINE__ , "fread[%s] ok , [%d]bytes" , p_htmlcache_session->full_pathfilename , p_htmlcache_session->html_content_len );
				}
				
				/* 关闭文件，或目录 */
				fclose( fp );
				DebugLogc( __FILE__ , __LINE__ , "fclose[%s] ok" , p_htmlcache_session->full_pathfilename );
			}
		}
		else
		{
			/* 击中缓存 */
			DebugLogc( __FILE__ , __LINE__ , "html[%s] cache hited , [%d]bytes" , full_pathfilename , p_htmlcache_session->html_content_len );
		}
		
		/* 先格式化响应头首行，用成功状态码 */
		nret = FormatHttpResponseStartLine( HTTP_OK , p_http_session->http , 0 , NULL ) ;
		if( nret )
		{
			ErrorLogc( __FILE__ , __LINE__ , "FormatHttpResponseStartLine failed[%d] , errno[%d]" , nret , ERRNO );
			return HTTP_INTERNAL_SERVER_ERROR;
		}
		
		/* 根据HTTP请求头中的AcceptEncoding选项，处理HTTP响应 */
		nret = ProcessHttpAcceptEncoding( p_env , p_http_session , p_htmlcache_session ) ;
		if( nret != HTTP_OK )
		{
			ErrorLogc( __FILE__ , __LINE__ , "ProcessHttpAcceptEncoding failed[%d]" , nret );
			return nret;
		}
		else
		{
			DebugLogc( __FILE__ , __LINE__ , "ProcessHttpAcceptEncoding ok" );
		}
		
		if( p_need_watch_file == NULL )
		{
			if( htmlcache_session.st.st_size <= p_env->limits__max_file_cache )
			{
				/* 缓存文件内容，并注册文件变动通知 */
				p_htmlcache_session = (struct HtmlCacheSession *)malloc( sizeof(struct HtmlCacheSession) ) ;
				if( p_htmlcache_session == NULL )
				{
					ErrorLogc( __FILE__ , __LINE__ , "malloc failed , errno[%d]" , ERRNO );
					return -1;
				}
				memcpy( p_htmlcache_session , & htmlcache_session , sizeof(struct HtmlCacheSession) );
				
#if ( defined __linux ) || ( defined __unix )
				p_htmlcache_session->wd = inotify_add_watch( p_env->htmlcache_inotify_fd , p_htmlcache_session->full_pathfilename , IN_MODIFY | IN_CLOSE_WRITE | IN_DELETE_SELF | IN_MOVE_SELF ) ; 
				if( p_htmlcache_session->wd == -1 )
				{
					ErrorLogc( __FILE__ , __LINE__ , "inotify_add_watch[%s] failed , errno[%d]" , p_htmlcache_session->full_pathfilename , ERRNO );
					FreeHtmlCacheSession( p_htmlcache_session , 1 );
					return HTTP_INTERNAL_SERVER_ERROR;
				}
				else
				{
					DebugLogc( __FILE__ , __LINE__ , "inotify_add_watch[%s] ok , wd[%d]" , p_htmlcache_session->full_pathfilename , p_htmlcache_session->wd );
				}
#elif ( defined _WIN32 )
#endif
				
				nret = AddHtmlCacheWdTreeNode( p_env , p_htmlcache_session ) ;
				if( nret )
				{
					ErrorLogc( __FILE__ , __LINE__ , "AddHtmlCacheWdTreeNode failed , errno[%d]" , ERRNO );
#if ( defined __linux ) || ( defined __unix )
					inotify_rm_watch( p_env->htmlcache_inotify_fd , p_htmlcache_session->wd );
#elif ( defined _WIN32 )
#endif
					FreeHtmlCacheSession( p_htmlcache_session , 1 );
					return HTTP_INTERNAL_SERVER_ERROR;
				}
				
				nret = AddHtmlCachePathfilenameTreeNode( p_env , p_htmlcache_session ) ;
				if( nret )
				{
					ErrorLogc( __FILE__ , __LINE__ , "AddHtmlCachePathfilenameTreeNode[%.*s] failed , errno[%d]" , p_htmlcache_session->full_pathfilename_len,p_htmlcache_session->full_pathfilename , ERRNO );
					RemoveHtmlCacheWdTreeNode( p_env , p_htmlcache_session );
#if ( defined __linux ) || ( defined __unix )
					inotify_rm_watch( p_env->htmlcache_inotify_fd , p_htmlcache_session->wd );
#elif ( defined _WIN32 )
#endif
					FreeHtmlCacheSession( p_htmlcache_session , 1 );
					return HTTP_INTERNAL_SERVER_ERROR;
				}
				else
				{
					DebugLogc( __FILE__ , __LINE__ , "AddHtmlCachePathfilenameTreeNode[%.*s] ok" , p_htmlcache_session->full_pathfilename_len,p_htmlcache_session->full_pathfilename );
				}
				
				LIST_ADD( & (p_htmlcache_session->list) , & (p_env->htmlcache_session_list.list) );
				p_env->htmlcache_session_count++;
			}
			else
			{
				FreeHtmlCacheSession( & htmlcache_session , 0 );
				DebugLogc( __FILE__ , __LINE__ , "FreeHtmlCacheSession" );
			}
		}
		
		return HTTP_OK;
	}
}

int ProcessHttpResource( struct HetaoEnv *p_env , struct HttpSession *p_http_session , char *wwwroot , char *uri , int uri_len )
{
	struct UriLocation	*p_uri_location = p_http_session->p_uri_location ;
	
	int			nret = 0 ;
	
	/* 分解URI */
	DebugLogc( __FILE__ , __LINE__ , "SplitHttpUri[%s][%.*s] ok" , wwwroot , uri_len , uri );
	memset( & (p_http_session->http_uri) , 0x00 , sizeof(struct HttpUri) );
	nret = SplitHttpUriEx( wwwroot , uri , uri_len , & (p_http_session->http_uri) , 0 ) ;
	if( nret )
	{
		ErrorLogc( __FILE__ , __LINE__ , "SplitHttpUri[%s][%.*s] failed[%d] , errno[%d]" , wwwroot , uri_len , uri , nret , ERRNO );
		return HTTP_NOT_FOUND;
	}
	else
	{
		DebugLogc( __FILE__ , __LINE__ , "SplitHttpUriEx - dirname[%.*s]" , p_http_session->http_uri.dirname_len,p_http_session->http_uri.dirname_base );
		DebugLogc( __FILE__ , __LINE__ , "SplitHttpUriEx - uri[%.*s]" , p_http_session->http_uri.filename_len,p_http_session->http_uri.filename_base );
		DebugLogc( __FILE__ , __LINE__ , "SplitHttpUriEx - main_filename[%.*s]" , p_http_session->http_uri.main_filename_len,p_http_session->http_uri.main_filename_base );
		DebugLogc( __FILE__ , __LINE__ , "SplitHttpUriEx - ext_filename[%.*s]" , p_http_session->http_uri.ext_filename_len,p_http_session->http_uri.ext_filename_base );
		DebugLogc( __FILE__ , __LINE__ , "SplitHttpUriEx - param[%.*s]" , p_http_session->http_uri.param_len,p_http_session->http_uri.param_base );
	}
	
	DebugLogc( __FILE__ , __LINE__ , "ProcessHttpResource - forward_rule[%.*s]" , p_uri_location->forward_servers.forward_type_len,p_uri_location->forward_servers.forward_type );
	DebugLogc( __FILE__ , __LINE__ , "ProcessHttpResource - pfuncCallHttpApplication[%p]" , p_uri_location->socgi.pfuncCallHttpApplication );
	DebugLogc( __FILE__ , __LINE__ , "ProcessHttpResource - socgi_type[%.*s]" , p_uri_location->socgi.socgi_type_len,p_uri_location->socgi.socgi_type );
	DebugLogc( __FILE__ , __LINE__ , "ProcessHttpResource - socgi_bin_pathfilename[%s]" , p_uri_location->socgi.socgi_bin_pathfilename );
	DebugLogc( __FILE__ , __LINE__ , "ProcessHttpResource - socgi_config_pathfilename[%s]" , p_uri_location->socgi.socgi_config_pathfilename );
	
	/* 如果文件类型为转发后端服务器 */
	if( p_uri_location->forward_servers.forward_rule[0]
		&& p_http_session->http_uri.ext_filename_len == p_uri_location->forward_servers.forward_type_len
		&& MEMCMP( p_http_session->http_uri.ext_filename_base , == , p_uri_location->forward_servers.forward_type , p_http_session->http_uri.ext_filename_len ) )
	{
		return SelectAndConnectForwardServer( p_env , p_http_session );
	}
	/* 如果是执行本地应用 */
	else if(	p_uri_location->socgi.pfuncCallHttpApplication
			&&
			(
				p_uri_location->socgi.socgi_type_len == 0
				||
				(
					p_http_session->http_uri.ext_filename_len == p_uri_location->socgi.socgi_type_len
					&&
					MEMCMP( p_http_session->http_uri.ext_filename_base , == , p_uri_location->socgi.socgi_type , p_http_session->http_uri.ext_filename_len )
				)
			)
	)
	{
		return CallHttpApplication( p_env , p_http_session );
	}
	/* 如果是获取本地静态资源 */
	else
	{
		return GetHttpResource( p_env , p_http_session , wwwroot , uri , uri_len );
	}
}
