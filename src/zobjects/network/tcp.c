/* Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "zobjects_network.h"

struct ZlangDirectProperty_tcp
{
	struct ZlangTcp		tcp ;
} ;

static struct ZlangDirectFunctions direct_funcs_tcp;

static TLS char _g_zlang_tcp_buffer[ 4096 ] = "" ;

ZlangInvokeFunction ZlangInvokeFunction_tcp_SetConnectingTimeout_int;
int ZlangInvokeFunction_tcp_SetConnectingTimeout_int( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_tcp	*tcp_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject		*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	int32_t				connecting_timeout ;
	
	CallRuntimeFunction_int_GetIntValue( rt , in1 , & connecting_timeout );
	
	ZLANG_TCP_SET_CONNECTINGTIMEOUT( tcp_direct_prop->tcp , connecting_timeout );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_tcp_GetConnectingElapse;
int ZlangInvokeFunction_tcp_GetConnectingElapse( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_tcp	*tcp_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject		*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	
	CallRuntimeFunction_int_SetIntValue( rt , out1 , ZLANG_TCP_GET_CONNECTINGELAPSE(tcp_direct_prop->tcp) );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_tcp_SetDataTransmissionTimeout_int;
int ZlangInvokeFunction_tcp_SetDataTransmissionTimeout_int( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_tcp	*tcp_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject		*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	int32_t				data_transmission_timeout ;
	
	CallRuntimeFunction_int_GetIntValue( rt , in1 , & data_transmission_timeout );
	
	ZLANG_TCP_SET_DATATRANSMISSIONTIMEOUT( tcp_direct_prop->tcp , data_transmission_timeout );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_tcp_GetDataTransmissionElapse;
int ZlangInvokeFunction_tcp_GetDataTransmissionElapse( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_tcp	*tcp_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject		*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	
	CallRuntimeFunction_int_SetIntValue( rt , out1 , ZLANG_TCP_GET_DATATRANSMISSIONELAPSE(tcp_direct_prop->tcp) );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_tcp_Listen_string_int;
int ZlangInvokeFunction_tcp_Listen_string_int( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_tcp	*tcp_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject		*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject		*in2 = GetInputParameterInLocalObjectStack(rt,2) ;
	struct ZlangObject		*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	char				*ip = NULL ;
	int32_t				port ;
	int				nret = 0 ;
	
	CallRuntimeFunction_string_GetStringValue( rt , in1 , & ip , NULL );
	CallRuntimeFunction_int_GetIntValue( rt , in2 , & port );
	nret = ZlangListenTcp( rt , & (tcp_direct_prop->tcp) , ip , port ) ;
	if( nret )
	{
		CallRuntimeFunction_bool_SetBoolValue( rt , out1 , FALSE );
		return ThrowErrorException( rt , EXCEPTION_CODE_SOCKET_ERROR , EXCEPTION_MESSAGE_LISTEN_SOCKET_FAILED );
	}
	
	CallRuntimeFunction_bool_SetBoolValue( rt , out1 , TRUE );
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_tcp_Accept;
int ZlangInvokeFunction_tcp_Accept( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_tcp	*listen_tcp_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject		*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	struct ZlangTcp			accept_tcp ;
	struct ZlangDirectProperty_tcp	*accept_tcp_direct_prop = NULL ;
	int				nret = 0 ;
	
	memset( & accept_tcp , 0x00 , sizeof(struct ZlangTcp) );
	nret = ZlangAcceptTcp( rt , & (listen_tcp_direct_prop->tcp) , & accept_tcp ) ;
	if( nret )
	{
		UnreferObject( rt , out1 );
		return ThrowErrorException( rt , EXCEPTION_CODE_SOCKET_ERROR , EXCEPTION_MESSAGE_ACCEPT_SOCKET_FAILED );
	}
	
	nret = InitObject( rt , out1 , NULL , & direct_funcs_tcp , 0 ) ;
	if( nret )
	{
		UnreferObject( rt , out1 );
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "InitObject failed[%d]" , nret )
		return nret;
	}
	accept_tcp_direct_prop = GetObjectDirectProperty(out1) ;
	
	strcpy( accept_tcp_direct_prop->tcp.ip , accept_tcp.ip );
	accept_tcp_direct_prop->tcp.port = accept_tcp.port ;
	accept_tcp_direct_prop->tcp.sock = accept_tcp.sock ;
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_tcp_Connect_string_int;
int ZlangInvokeFunction_tcp_Connect_string_int( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_tcp	*tcp_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject		*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject		*in2 = GetInputParameterInLocalObjectStack(rt,2) ;
	struct ZlangObject		*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	char				*ip = NULL ;
	int32_t				port ;
	int				nret = 0 ;
	
	CallRuntimeFunction_string_GetStringValue( rt , in1 , & ip , NULL );
	CallRuntimeFunction_int_GetIntValue( rt , in2 , & port );
	
	nret = ZlangConnectTcp( rt , & (tcp_direct_prop->tcp) , ip , port ) ;
	if( nret )
	{
		CallRuntimeFunction_bool_SetBoolValue( rt , out1 , FALSE );
		return ThrowErrorException( rt , EXCEPTION_CODE_SOCKET_ERROR , EXCEPTION_MESSAGE_CONNECT_SOCKET_FAILED );
	}
	
	CallRuntimeFunction_bool_SetBoolValue( rt , out1 , TRUE );
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_tcp_Close;
int ZlangInvokeFunction_tcp_Close( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_tcp	*tcp_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject		*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	int				nret = 0 ;
	
	nret = ZlangCloseTcp( rt , & (tcp_direct_prop->tcp) ) ;
	if( nret )
	{
		CallRuntimeFunction_bool_SetBoolValue( rt , out1 , FALSE );
		return ThrowErrorException( rt , EXCEPTION_CODE_SOCKET_ERROR , EXCEPTION_MESSAGE_CLOSE_SOCKET_FAILED );
	}
	
	CallRuntimeFunction_bool_SetBoolValue( rt , out1 , TRUE );
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_tcp_GetIp;
int ZlangInvokeFunction_tcp_GetIp( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_tcp	*tcp_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject		*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	
	if( ZLANG_TCP_IS_SOCK_INVALID(tcp_direct_prop->tcp) )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "sock[%"PRIsock"] is not avarible" , ZLANG_TCP_GET_SOCK(tcp_direct_prop->tcp) )
		CallRuntimeFunction_string_SetStringValue( rt , out1 , "" , 0 );
		return ThrowErrorException( rt , EXCEPTION_CODE_SOCKET_ERROR , EXCEPTION_MESSAGE_SOCKET_INVALID );
	}
	
	CallRuntimeFunction_string_SetStringValue( rt , out1 , ZLANG_TCP_GET_IP(tcp_direct_prop->tcp) , (int32_t)strlen(ZLANG_TCP_GET_IP(tcp_direct_prop->tcp)) );
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_tcp_GetPort;
int ZlangInvokeFunction_tcp_GetPort( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_tcp	*tcp_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject		*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	
	if( ZLANG_TCP_IS_SOCK_INVALID(tcp_direct_prop->tcp) )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "sock[%"PRIsock"] is not avarible" , ZLANG_TCP_GET_SOCK(tcp_direct_prop->tcp) )
		CallRuntimeFunction_int_SetIntValue( rt , out1 , -1 );
		return ThrowErrorException( rt , EXCEPTION_CODE_SOCKET_ERROR , EXCEPTION_MESSAGE_SOCKET_INVALID );
	}
	
	CallRuntimeFunction_int_SetIntValue( rt , out1 , ZLANG_TCP_GET_PORT(tcp_direct_prop->tcp) );
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_tcp_Send_string;
int ZlangInvokeFunction_tcp_Send_string( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_tcp	*tcp_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject		*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject		*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	char				*str = NULL ;
	int32_t				str_len ;
	int32_t				sent_len ;
	
	if( ZLANG_TCP_IS_SOCK_INVALID(tcp_direct_prop->tcp) )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "sock[%"PRIsock"] is not avarible" , ZLANG_TCP_GET_SOCK(tcp_direct_prop->tcp) )
		CallRuntimeFunction_int_SetIntValue( rt , out1 , -1 );
		return ThrowErrorException( rt , EXCEPTION_CODE_SOCKET_ERROR , EXCEPTION_MESSAGE_SOCKET_INVALID );
	}
	
	CallRuntimeFunction_string_GetStringValue( rt , in1 , & str , & str_len );
	
	ZLANG_TCP_SET_DATATRANSMISSIONELAPSE( tcp_direct_prop->tcp , 0 );
	sent_len = ZlangSendTcp( rt , ZLANG_TCP_GET_SOCK(tcp_direct_prop->tcp) , str , str_len , & ZLANG_TCP_GET_DATATRANSMISSIONTIMEOUT(tcp_direct_prop->tcp) , & ZLANG_TCP_GET_DATATRANSMISSIONELAPSE(tcp_direct_prop->tcp) ) ;
	if( sent_len == ZLANG_TCP_ERROR )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "writen sock[%"PRIsock"] send_len[%d] failed[%d]" , ZLANG_TCP_GET_SOCK(tcp_direct_prop->tcp) , str_len , sent_len )
		CallRuntimeFunction_int_SetIntValue( rt , out1 , -1 );
		return ThrowErrorException( rt , EXCEPTION_CODE_SOCKET_ERROR , EXCEPTION_MESSAGE_SEND_SOCKET_FAILED );
	}
	else if( sent_len == ZLANG_TCP_TIMEOUT )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "writen sock[%"PRIsock"] send_len[%d] ok , sent_len[%d]" , ZLANG_TCP_GET_SOCK(tcp_direct_prop->tcp) , str_len , sent_len )
		CallRuntimeFunction_int_SetIntValue( rt , out1 , -2 );
		return ThrowErrorException( rt , EXCEPTION_CODE_SOCKET_TIMEOUT , EXCEPTION_MESSAGE_SEND_SOCKET_TIMEOUT );
	}
	else
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "writen sock[%"PRIsock"] send_len[%d] ok , sent_len[%d]" , ZLANG_TCP_GET_SOCK(tcp_direct_prop->tcp) , str_len , sent_len )
		CallRuntimeFunction_int_SetIntValue( rt , out1 , sent_len );
	}
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_tcp_Send_string_int;
int ZlangInvokeFunction_tcp_Send_string_int( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_tcp	*tcp_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject		*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject		*in2 = GetInputParameterInLocalObjectStack(rt,2) ;
	struct ZlangObject		*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	char				*str = NULL ;
	int32_t				str_len ;
	int32_t				send_len ;
	int32_t				sent_len ;
	
	if( ZLANG_TCP_IS_SOCK_INVALID(tcp_direct_prop->tcp) )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "sock[%"PRIsock"] is not avarible" , ZLANG_TCP_GET_SOCK(tcp_direct_prop->tcp) )
		CallRuntimeFunction_int_SetIntValue( rt , out1 , -1 );
		return ThrowErrorException( rt , EXCEPTION_CODE_SOCKET_ERROR , EXCEPTION_MESSAGE_SOCKET_INVALID );
	}
	
	CallRuntimeFunction_string_GetStringValue( rt , in1 , & str , & str_len );
	CallRuntimeFunction_int_GetIntValue( rt , in2 , & send_len );
	
	if( send_len == -1 )
	{
		send_len = str_len ;
		ZLANG_TCP_SET_DATATRANSMISSIONELAPSE( tcp_direct_prop->tcp , 0 );
		sent_len = ZlangSendTcp( rt , ZLANG_TCP_GET_SOCK(tcp_direct_prop->tcp) , str , send_len , & ZLANG_TCP_GET_DATATRANSMISSIONTIMEOUT(tcp_direct_prop->tcp) , & ZLANG_TCP_GET_DATATRANSMISSIONELAPSE(tcp_direct_prop->tcp) ) ;
	}
	else
	{
		if( send_len > str_len )
		{
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "write len more than str len" )
			CallRuntimeFunction_int_SetIntValue( rt , out1 , -1 );
			return ThrowFatalException( rt , ZLANG_ERROR_INTERNAL , EXCEPTION_MESSAGE_GENERAL_ERROR );
		}
		
		ZLANG_TCP_SET_DATATRANSMISSIONELAPSE( tcp_direct_prop->tcp , 0 );
		sent_len = ZlangSendTcp( rt , ZLANG_TCP_GET_SOCK(tcp_direct_prop->tcp) , str , send_len , & ZLANG_TCP_GET_DATATRANSMISSIONTIMEOUT(tcp_direct_prop->tcp) , & ZLANG_TCP_GET_DATATRANSMISSIONELAPSE(tcp_direct_prop->tcp) ) ;
	}
	if( sent_len == ZLANG_TCP_ERROR )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "writen sock[%"PRIsock"] send_len[%d] failed[%d]" , ZLANG_TCP_GET_SOCK(tcp_direct_prop->tcp) , send_len , sent_len )
		CallRuntimeFunction_int_SetIntValue( rt , out1 , -1 );
		return ThrowErrorException( rt , EXCEPTION_CODE_SOCKET_ERROR , EXCEPTION_MESSAGE_SEND_SOCKET_FAILED );
	}
	else if( sent_len == ZLANG_TCP_TIMEOUT )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "writen sock[%"PRIsock"] send_len[%d] timeout , sent_len[%d]" , ZLANG_TCP_GET_SOCK(tcp_direct_prop->tcp) , send_len , sent_len )
		CallRuntimeFunction_int_SetIntValue( rt , out1 , -2 );
		return ThrowErrorException( rt , EXCEPTION_CODE_SOCKET_TIMEOUT , EXCEPTION_MESSAGE_SEND_SOCKET_TIMEOUT );
	}
	else
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "writen sock[%"PRIsock"] send_len[%d] ok , sent_len[%d]" , ZLANG_TCP_GET_SOCK(tcp_direct_prop->tcp) , send_len , sent_len )
		CallRuntimeFunction_int_SetIntValue( rt , out1 , sent_len );
	}
	
	return 0;
}

#define ZLANGINVOKEFUNCTION_TCP_SEND_BASETYPE(_basetype_,_Basetype_,_ctype_,_byteorder_func_) \
	ZlangInvokeFunction ZlangInvokeFunction_tcp_Send_##_basetype_; \
	int ZlangInvokeFunction_tcp_Send_##_basetype_( struct ZlangRuntime *rt , struct ZlangObject *obj ) \
	{ \
		struct ZlangDirectProperty_tcp	*tcp_direct_prop = GetObjectDirectProperty(obj) ; \
		struct ZlangObject		*in1 = GetInputParameterInLocalObjectStack(rt,1) ; \
		struct ZlangObject		*out1 = GetOutputParameterInLocalObjectStack(rt,1) ; \
		_ctype_				value ; \
		int32_t				sent_len ; \
		\
		if( ZLANG_TCP_IS_SOCK_INVALID(tcp_direct_prop->tcp) ) \
		{ \
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "sock[%"PRIsock"] is not avarible" , ZLANG_TCP_GET_SOCK(tcp_direct_prop->tcp) ) \
			CallRuntimeFunction_int_SetIntValue( rt , out1 , -1 ); \
			return 0; \
		} \
		\
		CallRuntimeFunction_##_basetype_##_Get##_Basetype_##Value( rt , in1 , & value ); \
		\
		ZLANG_TCP_SET_DATATRANSMISSIONELAPSE( tcp_direct_prop->tcp , 0 ); \
		value = _byteorder_func_(value) ; \
		sent_len = ZlangSendTcp( rt , ZLANG_TCP_GET_SOCK(tcp_direct_prop->tcp) , (char*) & value , sizeof(_ctype_) , & ZLANG_TCP_GET_DATATRANSMISSIONTIMEOUT(tcp_direct_prop->tcp) , & ZLANG_TCP_GET_DATATRANSMISSIONELAPSE(tcp_direct_prop->tcp) ) ; \
		if( sent_len == ZLANG_TCP_ERROR ) \
		{ \
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "writen sock[%"PRIsock"] send_len[%d] failed[%d] , errno[%d]" , ZLANG_TCP_GET_SOCK(tcp_direct_prop->tcp) , (int)sizeof(_ctype_) , sent_len , errno ) \
			CallRuntimeFunction_int_SetIntValue( rt , out1 , -1 ); \
			return 0; \
		} \
		else if( sent_len == ZLANG_TCP_TIMEOUT ) \
		{ \
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "writen sock[%"PRIsock"] send_len[%d] timeout , errno[%d]" , ZLANG_TCP_GET_SOCK(tcp_direct_prop->tcp) , (int)sizeof(_ctype_) , errno ) \
			CallRuntimeFunction_int_SetIntValue( rt , out1 , -2 ); \
			return 0; \
		} \
		else \
		{ \
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "writen sock[%"PRIsock"] send_len[%d] ok , sent_len[%d]" , ZLANG_TCP_GET_SOCK(tcp_direct_prop->tcp) , (int)sizeof(_ctype_) , sent_len ) \
			CallRuntimeFunction_int_SetIntValue( rt , out1 , sent_len ); \
		} \
		\
		return 0; \
	} \

#define HTON_DUMMY(_value_)	(_value_)

ZLANGINVOKEFUNCTION_TCP_SEND_BASETYPE(short,Short,int16_t,HTON16)
ZLANGINVOKEFUNCTION_TCP_SEND_BASETYPE(int,Int,int32_t,HTON32)
ZLANGINVOKEFUNCTION_TCP_SEND_BASETYPE(long,Long,int64_t,HTON64)
ZLANGINVOKEFUNCTION_TCP_SEND_BASETYPE(float,Float,float,HTON_DUMMY)
ZLANGINVOKEFUNCTION_TCP_SEND_BASETYPE(double,Double,double,HTON_DUMMY)

ZlangInvokeFunction ZlangInvokeFunction_tcp_Sendln_string;
int ZlangInvokeFunction_tcp_Sendln_string( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_tcp	*tcp_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject		*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject		*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	char				*str = NULL ;
	int32_t				str_len ;
	int32_t				sent_len = 0 ;
	int32_t				sent_len2 = 0 ;
	
	if( ZLANG_TCP_IS_SOCK_INVALID(tcp_direct_prop->tcp) )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "sock[%"PRIsock"] is not avarible" , ZLANG_TCP_GET_SOCK(tcp_direct_prop->tcp) )
		CallRuntimeFunction_int_SetIntValue( rt , out1 , -1 );
		return ThrowErrorException( rt , EXCEPTION_CODE_SOCKET_TIMEOUT , EXCEPTION_MESSAGE_SOCKET_INVALID );
	}
	
	CallRuntimeFunction_string_GetStringValue( rt , in1 , & str , & str_len );
	
	ZLANG_TCP_SET_DATATRANSMISSIONELAPSE( tcp_direct_prop->tcp , 0 );
	
	sent_len = ZlangSendTcp( rt , ZLANG_TCP_GET_SOCK(tcp_direct_prop->tcp) , str , str_len , & ZLANG_TCP_GET_DATATRANSMISSIONTIMEOUT(tcp_direct_prop->tcp) , & ZLANG_TCP_GET_DATATRANSMISSIONELAPSE(tcp_direct_prop->tcp) ) ;
	if( sent_len == ZLANG_TCP_ERROR )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "writen sock[%"PRIsock"] send_len[%d] failed[%d]" , ZLANG_TCP_GET_SOCK(tcp_direct_prop->tcp) , str_len , sent_len ) \
		CallRuntimeFunction_int_SetIntValue( rt , out1 , -1 );
		return ThrowErrorException( rt , EXCEPTION_CODE_SOCKET_ERROR , EXCEPTION_MESSAGE_SEND_SOCKET_FAILED );
	}
	else if( sent_len == ZLANG_TCP_TIMEOUT )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "writen sock[%"PRIsock"] send_len[%d] timeout , sent_len[%d]" , ZLANG_TCP_GET_SOCK(tcp_direct_prop->tcp) , str_len , sent_len ) \
		CallRuntimeFunction_int_SetIntValue( rt , out1 , -2 );
		return ThrowErrorException( rt , EXCEPTION_CODE_SOCKET_TIMEOUT , EXCEPTION_MESSAGE_SEND_SOCKET_TIMEOUT );
	}
	else
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "writen sock[%"PRIsock"] send_len[%d] ok , sent_len[%d]" , ZLANG_TCP_GET_SOCK(tcp_direct_prop->tcp) , str_len , sent_len ) \
		CallRuntimeFunction_int_SetIntValue( rt , out1 , sent_len );
	}
	
	sent_len2 = ZlangSendTcp( rt , ZLANG_TCP_GET_SOCK(tcp_direct_prop->tcp) , "\n" , 1 , & ZLANG_TCP_GET_DATATRANSMISSIONTIMEOUT(tcp_direct_prop->tcp) , & ZLANG_TCP_GET_DATATRANSMISSIONELAPSE(tcp_direct_prop->tcp) ) ;
	if( sent_len2 == ZLANG_TCP_ERROR )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "writen sock[%"PRIsock"] send_len[%d] failed[%d]" , ZLANG_TCP_GET_SOCK(tcp_direct_prop->tcp) , 1 , sent_len2 ) \
		CallRuntimeFunction_int_SetIntValue( rt , out1 , -1 );
		return ThrowErrorException( rt , EXCEPTION_CODE_SOCKET_ERROR , EXCEPTION_MESSAGE_SEND_SOCKET_FAILED );
	}
	else if( sent_len == ZLANG_TCP_TIMEOUT )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "writen sock[%"PRIsock"] send_len[%d] timeout , sent_len[%d]" , ZLANG_TCP_GET_SOCK(tcp_direct_prop->tcp) , str_len , sent_len ) \
		CallRuntimeFunction_int_SetIntValue( rt , out1 , -2 );
		return ThrowErrorException( rt , EXCEPTION_CODE_SOCKET_TIMEOUT , EXCEPTION_MESSAGE_SEND_SOCKET_TIMEOUT );
	}
	else
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "writen sock[%"PRIsock"] send_len[%d] ok , sent_len[%d]" , ZLANG_TCP_GET_SOCK(tcp_direct_prop->tcp) , 1 , sent_len2 ) \
		CallRuntimeFunction_int_SetIntValue( rt , out1 , sent_len+sent_len2 );
	}
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_tcp_Receive_string_int;
int ZlangInvokeFunction_tcp_Receive_string_int( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_tcp	*tcp_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject		*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject		*in2 = GetInputParameterInLocalObjectStack(rt,2) ;
	struct ZlangObject		*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	int32_t				recv_len ;
	int32_t				recvd_len ;
	char				**buf = NULL ;
	int32_t				*buf_len = NULL ;
	int				nret = 0 ;
	
	if( ZLANG_TCP_IS_SOCK_INVALID(tcp_direct_prop->tcp) )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "sock[%"PRIsock"] is not avarible" , ZLANG_TCP_GET_SOCK(tcp_direct_prop->tcp) )
		CallRuntimeFunction_bool_SetBoolValue( rt , out1 , -1 );
		return ThrowErrorException( rt , EXCEPTION_CODE_SOCKET_ERROR , EXCEPTION_MESSAGE_SOCKET_INVALID );
	}
	
	CallRuntimeFunction_int_GetIntValue( rt , in2 , & recv_len );
	
	nret = CallRuntimeFunction_string_PrepareBuffer( rt , in1 , recv_len+1 ) ;
	if( nret )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "prepare buffer failed" )
		CallRuntimeFunction_int_SetIntValue( rt , out1 , -1 );
		return ThrowFatalException( rt , nret , EXCEPTION_MESSAGE_PREPARE_STRING_BUFFER );
	}
	
	CallRuntimeFunction_string_GetDirectPropertiesPtr( rt , in1 , & buf , NULL , & buf_len );
	ZLANG_TCP_SET_DATATRANSMISSIONELAPSE( tcp_direct_prop->tcp , 0 );
	recvd_len = ZlangReceiveTcp( rt , ZLANG_TCP_GET_SOCK(tcp_direct_prop->tcp) , (*buf) , recv_len , & ZLANG_TCP_GET_DATATRANSMISSIONTIMEOUT(tcp_direct_prop->tcp) , & ZLANG_TCP_GET_DATATRANSMISSIONELAPSE(tcp_direct_prop->tcp) ) ;
	if( recvd_len == ZLANG_TCP_ERROR )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "readn sock[%"PRIsock"] recv_len[%d] failed[%d]" , ZLANG_TCP_GET_SOCK(tcp_direct_prop->tcp) , recv_len , recvd_len )
		CallRuntimeFunction_int_SetIntValue( rt , out1 , -1 );
		return ThrowErrorException( rt , EXCEPTION_CODE_SOCKET_ERROR , EXCEPTION_MESSAGE_RECEIVE_SOCKET_FAILED );
	}
	else if( recvd_len == ZLANG_TCP_TIMEOUT )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "readn sock[%"PRIsock"] recv_len[%d] timeout , recvd_len[%d]" , ZLANG_TCP_GET_SOCK(tcp_direct_prop->tcp) , recv_len , recvd_len )
		CallRuntimeFunction_int_SetIntValue( rt , out1 , -2 );
		(*buf_len) = recvd_len ;
		return ThrowErrorException( rt , EXCEPTION_CODE_SOCKET_TIMEOUT , EXCEPTION_MESSAGE_RECEIVE_SOCKET_TIMEOUT );
	}
	else if( recvd_len == ZLANG_TCP_DISCONNECTED_FROM_PEER )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "readn sock[%"PRIsock"] closed from peer" , ZLANG_TCP_GET_SOCK(tcp_direct_prop->tcp) )
		CallRuntimeFunction_int_SetIntValue( rt , out1 , -2 );
		return ThrowErrorException( rt , EXCEPTION_CODE_SOCKET_CLOSED_FROM_PEER , EXCEPTION_MESSAGE_SOCKET_CLOSED_FROM_PEER );
	}
	else
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "readn sock[%"PRIsock"] recv_len[%d] ok , recvd_len[%d]" , ZLANG_TCP_GET_SOCK(tcp_direct_prop->tcp) , recv_len , recvd_len )
		CallRuntimeFunction_int_SetIntValue( rt , out1 , recvd_len );
		(*buf_len) = recvd_len ;
	}
	
	return 0;
}

#define ZLANGINVOKEFUNCTION_TCP_RECV_BASETYPE(_basetype_,_Basetype_,_ctype_,_byteorder_func_) \
	ZlangInvokeFunction ZlangInvokeFunction_tcp_Receive_##_basetype_; \
	int ZlangInvokeFunction_tcp_Receive_##_basetype_( struct ZlangRuntime *rt , struct ZlangObject *obj ) \
	{ \
		struct ZlangDirectProperty_tcp	*tcp_direct_prop = GetObjectDirectProperty(obj) ; \
		struct ZlangObject		*in1 = GetInputParameterInLocalObjectStack(rt,1) ; \
		struct ZlangObject		*out1 = GetOutputParameterInLocalObjectStack(rt,1) ; \
		_ctype_				value ; \
		int32_t				recvd_len ; \
		\
		if( ZLANG_TCP_IS_SOCK_INVALID(tcp_direct_prop->tcp) ) \
		{ \
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "sock[%"PRIsock"] is not avarible" , ZLANG_TCP_GET_SOCK(tcp_direct_prop->tcp) ) \
			CallRuntimeFunction_int_SetIntValue( rt , out1 , -1 ); \
			return ThrowErrorException( rt , EXCEPTION_CODE_SOCKET_ERROR , EXCEPTION_MESSAGE_SOCKET_INVALID ); \
		} \
		\
		ZLANG_TCP_SET_DATATRANSMISSIONELAPSE( tcp_direct_prop->tcp , 0 ); \
		recvd_len = ZlangReceiveTcp( rt , ZLANG_TCP_GET_SOCK(tcp_direct_prop->tcp) , (char*) & value , sizeof(_ctype_) , & ZLANG_TCP_GET_DATATRANSMISSIONTIMEOUT(tcp_direct_prop->tcp) , & ZLANG_TCP_GET_DATATRANSMISSIONELAPSE(tcp_direct_prop->tcp) ) ;\
		if( recvd_len == ZLANG_TCP_ERROR ) \
		{ \
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "readn sock[%"PRIsock"] recv_len[%d] failed[%d] , errno[%d]" , ZLANG_TCP_GET_SOCK(tcp_direct_prop->tcp) , (int)sizeof(_ctype_) , recvd_len , errno ) \
			CallRuntimeFunction_int_SetIntValue( rt , out1 , -1 ); \
		} \
		else if( recvd_len == ZLANG_TCP_TIMEOUT ) \
		{ \
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "readn sock[%"PRIsock"] recv_len[%d] timeout , recvd_len[%d]" , ZLANG_TCP_GET_SOCK(tcp_direct_prop->tcp) , (int)sizeof(_ctype_) , recvd_len ) \
			CallRuntimeFunction_int_SetIntValue( rt , out1 , recvd_len ); \
		} \
		else \
		{ \
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "readn sock[%"PRIsock"] recv_len[%d] ok , recvd_len[%d]" , ZLANG_TCP_GET_SOCK(tcp_direct_prop->tcp) , (int)sizeof(_ctype_) , recvd_len ) \
			value = _byteorder_func_(value) ; \
			CallRuntimeFunction_##_basetype_##_Set##_Basetype_##Value( rt , in1 , value ); \
			CallRuntimeFunction_int_SetIntValue( rt , out1 , recvd_len ); \
		} \
		\
		return 0; \
	} \

ZLANGINVOKEFUNCTION_TCP_RECV_BASETYPE(short,Short,int16_t,HTON16)
ZLANGINVOKEFUNCTION_TCP_RECV_BASETYPE(int,Int,int32_t,HTON32)
ZLANGINVOKEFUNCTION_TCP_RECV_BASETYPE(long,Long,int64_t,HTON64)
ZLANGINVOKEFUNCTION_TCP_RECV_BASETYPE(float,Float,float,HTON_DUMMY)
ZLANGINVOKEFUNCTION_TCP_RECV_BASETYPE(double,Double,double,HTON_DUMMY)

ZlangInvokeFunction ZlangInvokeFunction_tcp_Receiveln_string;
int ZlangInvokeFunction_tcp_Receiveln_string( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_tcp	*tcp_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject		*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject		*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	char				*p = NULL ;
	int32_t				remain_len ;
	int32_t				len ;
	int32_t				recvd_len ;
	
	int				nret = 0 ;
	
	if( ZLANG_TCP_IS_SOCK_INVALID(tcp_direct_prop->tcp) )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "sock[%"PRIsock"] is not avarible" , ZLANG_TCP_GET_SOCK(tcp_direct_prop->tcp) )
		CallRuntimeFunction_int_SetIntValue( rt , out1 , -1 );
		return ThrowErrorException( rt , EXCEPTION_CODE_SOCKET_ERROR , EXCEPTION_MESSAGE_SOCKET_INVALID );
	}
	
	ZLANG_TCP_SET_DATATRANSMISSIONELAPSE( tcp_direct_prop->tcp , 0 );
	
	memset( _g_zlang_tcp_buffer , 0x00 , sizeof(_g_zlang_tcp_buffer) );
	p = _g_zlang_tcp_buffer ;
	remain_len = sizeof(_g_zlang_tcp_buffer) - 1 ;
	recvd_len = 0 ;
	for( ; ; )
	{
		nret = ZlangWaitforTcp( rt , POLLIN , ZLANG_TCP_GET_SOCK(tcp_direct_prop->tcp) , & ZLANG_TCP_GET_DATATRANSMISSIONTIMEOUT(tcp_direct_prop->tcp) , & ZLANG_TCP_GET_DATATRANSMISSIONELAPSE(tcp_direct_prop->tcp) ) ;
		if( nret == ZLANG_TCP_ERROR )
		{
			CallRuntimeFunction_int_SetIntValue( rt , out1 , ZLANG_TCP_ERROR );
			return ThrowErrorException( rt , EXCEPTION_CODE_SOCKET_ERROR , EXCEPTION_MESSAGE_SOCKET_ERROR );
		}
		else if( nret == ZLANG_TCP_TIMEOUT )
		{
			CallRuntimeFunction_int_SetIntValue( rt , out1 , ZLANG_TCP_TIMEOUT );
			return ThrowErrorException( rt , EXCEPTION_CODE_SOCKET_TIMEOUT , EXCEPTION_MESSAGE_RECEIVE_SOCKET_TIMEOUT );
		}
		
		len = RECV( ZLANG_TCP_GET_SOCK(tcp_direct_prop->tcp) , p , remain_len , 0 ) ;
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "[%d] = read( [%"PRIsock"] , [%.*s] , [%d] ) ; errno[%d]" , len , ZLANG_TCP_GET_SOCK(tcp_direct_prop->tcp) , remain_len,p , remain_len , errno )
		if( len < 0 )
		{
			CallRuntimeFunction_int_SetIntValue( rt , out1 , ZLANG_TCP_ERROR );
			return ThrowErrorException( rt , EXCEPTION_CODE_SOCKET_ERROR , EXCEPTION_MESSAGE_RECEIVE_SOCKET_FAILED );
		}
		else if( len == 0 )
		{
			CallRuntimeFunction_int_SetIntValue( rt , out1 , ZLANG_TCP_DISCONNECTED_FROM_PEER );
			return ThrowErrorException( rt , EXCEPTION_CODE_SOCKET_CLOSED_FROM_PEER , EXCEPTION_MESSAGE_SOCKET_CLOSED_FROM_PEER );
		}
		
		p += len ;
		remain_len -= len ;
		recvd_len += len ;
		
		if( recvd_len >= 1 && p[-1] == '\n' )
		{
			p[-1] = '\0' ;
			p--;
			remain_len++;
			recvd_len--;
			
			if( recvd_len >= 1 && p[-1] == '\r' )
			{
				p[-1] = '\0' ;
				p--;
				remain_len++;
				recvd_len--;
			}
			
			break;
		}
		
		if( remain_len == 0 )
		{
			break;
		}
	}
	
	CallRuntimeFunction_string_SetStringValue( rt , in1 , _g_zlang_tcp_buffer , recvd_len );
	
	CallRuntimeFunction_int_SetIntValue( rt , out1 , recvd_len ); \
	return 0;
}

ZlangCreateDirectPropertyFunction ZlangCreateDirectProperty_tcp;
void *ZlangCreateDirectProperty_tcp( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_tcp	*tcp_direct_prop = NULL ;
	
	tcp_direct_prop = (struct ZlangDirectProperty_tcp *)ZLMALLOC( sizeof(struct ZlangDirectProperty_tcp) ) ;
	if( tcp_direct_prop == NULL )
	{
		SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_ALLOC , "alloc memory for entity" )
		return NULL;
	}
	memset( tcp_direct_prop , 0x00 , sizeof(struct ZlangDirectProperty_tcp) );
	
	ZLANG_TCP_SET_SOCK( tcp_direct_prop->tcp , -1 );
	ZLANG_TCP_SET_CONNECTINGTIMEOUT( tcp_direct_prop->tcp , -1 );
	ZLANG_TCP_SET_DATATRANSMISSIONTIMEOUT( tcp_direct_prop->tcp , -1 );
	
	return tcp_direct_prop;
}

ZlangDestroyDirectPropertyFunction ZlangDestroyDirectProperty_tcp;
void ZlangDestroyDirectProperty_tcp( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_tcp	*tcp_direct_prop = GetObjectDirectProperty(obj) ;
	
	if( ZLANG_TCP_IS_SOCK_VALID(tcp_direct_prop->tcp) )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "close sock[%"PRIsock"]" , ZLANG_TCP_GET_SOCK(tcp_direct_prop->tcp) )
		CLOSESOCKET( ZLANG_TCP_GET_SOCK(tcp_direct_prop->tcp) ); ZLANG_TCP_SET_SOCK( tcp_direct_prop->tcp , -1 );
	}
	
	ZLFREE( tcp_direct_prop );
	
	return;
}

ZlangToStringFunction ZlangToString_tcp;
int ZlangToString_tcp( struct ZlangRuntime *rt , struct ZlangObject *obj , struct ZlangObject **tostr_obj )
{
	struct ZlangDirectProperty_tcp	*tcp_direct_prop = GetObjectDirectProperty(obj) ;
	char				buf[ 1+10+1+40+1+10 + 1 ] ;
	int32_t				buf_len ;
	
	memset( buf , 0x00 , sizeof(buf) );
	buf_len = snprintf( buf , sizeof(buf)-1 , "#%"PRIsock"#%s:%"PRIi32 , ZLANG_TCP_GET_SOCK(tcp_direct_prop->tcp) , ZLANG_TCP_GET_IP(tcp_direct_prop->tcp) , ZLANG_TCP_GET_PORT(tcp_direct_prop->tcp) ) ;
	CallRuntimeFunction_string_SetStringValue( rt , (*tostr_obj) , buf , buf_len );
	
	return 0;
}

ZlangSummarizeDirectPropertySizeFunction ZlangSummarizeDirectPropertySize_tcp;
void ZlangSummarizeDirectPropertySize_tcp( struct ZlangRuntime *rt , struct ZlangObject *obj , size_t *summarized_obj_size , size_t *summarized_direct_prop_size )
{
	SUMMARIZE_SIZE( summarized_direct_prop_size , sizeof(struct ZlangDirectProperty_tcp) )
	return;
}

static struct ZlangDirectFunctions direct_funcs_tcp =
	{
		ZLANG_OBJECT_tcp , /* char *ancestor_name */
		
		ZlangCreateDirectProperty_tcp , /* ZlangCreateDirectPropertyFunction *create_entity_func */
		ZlangDestroyDirectProperty_tcp , /* ZlangDestroyDirectPropertyFunction *destroy_entity_func */
		
		NULL , /* ZlangFromCharPtrFunction *from_char_ptr_func */
		ZlangToString_tcp , /* ZlangToStringFunction *to_string_func */
		NULL , /* ZlangFromDataPtrFunction *from_data_ptr_func */
		NULL , /* ZlangGetDataPtrFunction *get_data_ptr_func */
		
		NULL , /* ZlangOperatorFunction *oper_PLUS_func */
		NULL , /* ZlangOperatorFunction *oper_MINUS_func */
		NULL , /* ZlangOperatorFunction *oper_MUL_func */
		NULL , /* ZlangOperatorFunction *oper_DIV_func */
		NULL , /* ZlangOperatorFunction *oper_MOD_func */
		
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_NEGATIVE_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_NOT_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_BIT_REVERSE_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_PLUS_PLUS_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_MINUS_MINUS_func */
		
		NULL , /* ZlangCompareFunction *comp_EGUAL_func */
		NULL , /* ZlangCompareFunction *comp_NOTEGUAL_func */
		NULL , /* ZlangCompareFunction *comp_LT_func */
		NULL , /* ZlangCompareFunction *comp_LE_func */
		NULL , /* ZlangCompareFunction *comp_GT_func */
		NULL , /* ZlangCompareFunction *comp_GE_func */
		
		NULL , /* ZlangLogicFunction *logic_AND_func */
		NULL , /* ZlangLogicFunction *logic_OR_func */
		
		NULL , /* ZlangLogicFunction *bit_AND_func */
		NULL , /* ZlangLogicFunction *bit_XOR_func */
		NULL , /* ZlangLogicFunction *bit_OR_func */
		NULL , /* ZlangLogicFunction *bit_MOVELEFT_func */
		NULL , /* ZlangLogicFunction *bit_MOVERIGHT_func */
		
		ZlangSummarizeDirectPropertySize_tcp , /* ZlangSummarizeDirectPropertySizeFunction *summarize_direct_prop_size_func */
	} ;

ZlangImportObjectFunction ZlangImportObject_tcp;
struct ZlangObject *ZlangImportObject_tcp( struct ZlangRuntime *rt )
{
	struct ZlangObject	*obj = NULL ;
	struct ZlangObject	*prop = NULL ;
	struct ZlangFunction	*func = NULL ;
	int			nret = 0 ;
	
	nret = ImportObject( rt , & obj , ZLANG_OBJECT_tcp , & direct_funcs_tcp , sizeof(struct ZlangDirectFunctions) , NULL ) ;
	if( nret )
	{
		SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_LINK_FUNC_TO_ENTITY , "import object to global objects heap" )
		return NULL;
	}
	
	/* int tcp.ERROR */
	prop = AddPropertyInObject( rt , obj , QueryObjectByObjectName(rt,"int") , "ERROR") ;
	if( prop == NULL )
		return NULL;
	CallRuntimeFunction_int_SetIntValue( rt , prop , ZLANG_TCP_ERROR );
	SetConstantObject( prop );
	
	/* int tcp.TIMEOUT */
	prop = AddPropertyInObject( rt , obj , QueryObjectByObjectName(rt,"int") , "TIMEOUT") ;
	if( prop == NULL )
		return NULL;
	CallRuntimeFunction_int_SetIntValue( rt , prop , ZLANG_TCP_TIMEOUT );
	SetConstantObject( prop );
	
	/* int tcp.DISCONNECTED_FROM_PEER */
	prop = AddPropertyInObject( rt , obj , QueryObjectByObjectName(rt,"int") , "DISCONNECTED_FROM_PEER") ;
	if( prop == NULL )
		return NULL;
	CallRuntimeFunction_int_SetIntValue( rt , prop , ZLANG_TCP_DISCONNECTED_FROM_PEER );
	SetConstantObject( prop );
	
	/* tcp.SetConnectingTimeout() */
	func = AddFunctionAndParametersInObject( rt , obj , "SetConnectingTimeout" , "SetConnectingTimeout(int)" , ZlangInvokeFunction_tcp_SetConnectingTimeout_int , ZLANG_OBJECT_void , ZLANG_OBJECT_int,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* tcp.SetDataTransmissionTimeout() */
	func = AddFunctionAndParametersInObject( rt , obj , "SetDataTransmissionTimeout" , "SetDataTransmissionTimeout(int)" , ZlangInvokeFunction_tcp_SetDataTransmissionTimeout_int , ZLANG_OBJECT_void , ZLANG_OBJECT_int,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* tcp.GetConnectingElapse() */
	func = AddFunctionAndParametersInObject( rt , obj , "GetConnectingElapse" , "GetConnectingElapse()" , ZlangInvokeFunction_tcp_GetConnectingElapse , ZLANG_OBJECT_int , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* tcp.GetDataTransmissionElapse() */
	func = AddFunctionAndParametersInObject( rt , obj , "GetDataTransmissionElapse" , "GetDataTransmissionElapse()" , ZlangInvokeFunction_tcp_GetDataTransmissionElapse , ZLANG_OBJECT_int , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* tcp.Listen() */
	func = AddFunctionAndParametersInObject( rt , obj , "Listen" , "Listen(string,int)" , ZlangInvokeFunction_tcp_Listen_string_int , ZLANG_OBJECT_bool , ZLANG_OBJECT_string,NULL , ZLANG_OBJECT_int,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* tcp.Accept() */
	func = AddFunctionAndParametersInObject( rt , obj , "Accept" , "Accept()" , ZlangInvokeFunction_tcp_Accept , ZLANG_OBJECT_tcp , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* tcp.Connect() */
	func = AddFunctionAndParametersInObject( rt , obj , "Connect" , "Connect(string,int)" , ZlangInvokeFunction_tcp_Connect_string_int , ZLANG_OBJECT_bool , ZLANG_OBJECT_string,NULL , ZLANG_OBJECT_int,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* tcp.Close() */
	func = AddFunctionAndParametersInObject( rt , obj , "Close" , "Close()" , ZlangInvokeFunction_tcp_Close , ZLANG_OBJECT_bool , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* tcp.GetIp() */
	func = AddFunctionAndParametersInObject( rt , obj , "GetIp" , "GetIp()" , ZlangInvokeFunction_tcp_GetIp , ZLANG_OBJECT_string , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* tcp.GetPort() */
	func = AddFunctionAndParametersInObject( rt , obj , "GetPort" , "GetPort()" , ZlangInvokeFunction_tcp_GetPort , ZLANG_OBJECT_int , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* tcp.Send(string) */
	func = AddFunctionAndParametersInObject( rt , obj , "Send" , "Send(string)" , ZlangInvokeFunction_tcp_Send_string , ZLANG_OBJECT_int , ZLANG_OBJECT_string,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* tcp.Send(string,int) */
	func = AddFunctionAndParametersInObject( rt , obj , "Send" , "Send(string,int)" , ZlangInvokeFunction_tcp_Send_string_int , ZLANG_OBJECT_int , ZLANG_OBJECT_string,NULL , ZLANG_OBJECT_int,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* tcp.Send(short) */
	func = AddFunctionAndParametersInObject( rt , obj , "Send" , "Send(short)" , ZlangInvokeFunction_tcp_Send_short , ZLANG_OBJECT_int , ZLANG_OBJECT_short,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* tcp.Send(int) */
	func = AddFunctionAndParametersInObject( rt , obj , "Send" , "Send(int)" , ZlangInvokeFunction_tcp_Send_int , ZLANG_OBJECT_int , ZLANG_OBJECT_int,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* tcp.Send(long) */
	func = AddFunctionAndParametersInObject( rt , obj , "Send" , "Send(long)" , ZlangInvokeFunction_tcp_Send_long , ZLANG_OBJECT_int , ZLANG_OBJECT_long,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* tcp.Send(float) */
	func = AddFunctionAndParametersInObject( rt , obj , "Send" , "Send(float)" , ZlangInvokeFunction_tcp_Send_float , ZLANG_OBJECT_int , ZLANG_OBJECT_float,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* tcp.Send(double) */
	func = AddFunctionAndParametersInObject( rt , obj , "Send" , "Send(double)" , ZlangInvokeFunction_tcp_Send_double , ZLANG_OBJECT_int , ZLANG_OBJECT_double,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* tcp.Sendln(string) */
	func = AddFunctionAndParametersInObject( rt , obj , "Sendln" , "Sendln(string)" , ZlangInvokeFunction_tcp_Sendln_string , ZLANG_OBJECT_int , ZLANG_OBJECT_string,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* tcp.Receive(string,int) */
	func = AddFunctionAndParametersInObject( rt , obj , "Receive" , "Receive(string,int)" , ZlangInvokeFunction_tcp_Receive_string_int , ZLANG_OBJECT_int , ZLANG_OBJECT_string,NULL , ZLANG_OBJECT_int,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* tcp.Receive(short) */
	func = AddFunctionAndParametersInObject( rt , obj , "Receive" , "Receive(short)" , ZlangInvokeFunction_tcp_Receive_short , ZLANG_OBJECT_int , ZLANG_OBJECT_short,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* tcp.Receive(int) */
	func = AddFunctionAndParametersInObject( rt , obj , "Receive" , "Receive(int)" , ZlangInvokeFunction_tcp_Receive_int , ZLANG_OBJECT_int , ZLANG_OBJECT_int,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* tcp.Receive(long) */
	func = AddFunctionAndParametersInObject( rt , obj , "Receive" , "Receive(long)" , ZlangInvokeFunction_tcp_Receive_long , ZLANG_OBJECT_int , ZLANG_OBJECT_long,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* tcp.Receive(float) */
	func = AddFunctionAndParametersInObject( rt , obj , "Receive" , "Receive(float)" , ZlangInvokeFunction_tcp_Receive_float , ZLANG_OBJECT_int , ZLANG_OBJECT_float,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* tcp.Receive(double) */
	func = AddFunctionAndParametersInObject( rt , obj , "Receive" , "Receive(double)" , ZlangInvokeFunction_tcp_Receive_double , ZLANG_OBJECT_int , ZLANG_OBJECT_double,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* tcp.Receiveln(string) */
	func = AddFunctionAndParametersInObject( rt , obj , "Receiveln" , "Receiveln(string)" , ZlangInvokeFunction_tcp_Receiveln_string , ZLANG_OBJECT_int , ZLANG_OBJECT_string,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	return obj ;
}

