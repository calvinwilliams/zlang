/*
 * hetao - High Performance Web Server
 * author	: calvin
 * email	: calvinwilliams@163.com
 *
 * Licensed under the LGPL v2.1, see the file LICENSE in base directory.
 */

#include "hetao_in.h"

#define PREFIX_DSCLOG_hetao_conf	DebugLogc( __FILE__ , __LINE__ , 
#define NEWLINE_DSCLOG_hetao_conf
#include "IDL_hetao_conf.dsc.LOG.c"

#if ( defined _WIN32 )
static	WSADATA		wsd;
#endif

TLS char		g_hetao_api_error_buffer[ 256 ] = "" ;

int RunHttpServer( hetao_conf *conf , funcCallHttpApplication *pfuncCallHttpApplication , void *user_data )
{
	hetao_conf		*p_conf = NULL ;
	struct HetaoEnv		*p_env = NULL ;
	int			log_level ;
	
	struct ListenSession	*p_listen_session = NULL ;
	struct VirtualHost	*p_virtual_host = NULL ;
	
	int			nret = 0 ;
	
	memset( g_hetao_api_error_buffer , 0x00 , sizeof(g_hetao_api_error_buffer) );
	
	if( conf == NULL )
	{
		if( g_hetao_api_error_buffer[0] == '\0' )
			SNPRINTF( g_hetao_api_error_buffer , sizeof(g_hetao_api_error_buffer)-1 , "RunHttpServer input parameter 'conf' is null" );
		return -1;
	}
	
	/* 设置标准输出无缓冲 */
	setbuf( stdout , NULL );
	
	/* 设置随机数种子 */
	srand( (unsigned)time(NULL) );
	
	/* 设置文件掩码0 */
	UMASK(0);
	
	/*
#if ( defined _WIN32 )
	if( WSAStartup( MAKEWORD(2,2) , & wsd ) != 0 )
	{
		return 1;
	}
#endif
	*/

	/* 申请环境结构内存 */
	p_env = (struct HetaoEnv *)malloc( sizeof(struct HetaoEnv) ) ;
	if( p_env == NULL )
	{
		if( g_hetao_api_error_buffer[0] == '\0' )
			SNPRINTF( g_hetao_api_error_buffer , sizeof(g_hetao_api_error_buffer)-1 , "alloc failed , errno[%d]" , errno );
		return 1;
	}
	memset( p_env , 0x00 , sizeof(struct HetaoEnv) );
	g_p_env = p_env ;
	
	/* 申请配置结构内存 */
	p_conf = (hetao_conf *)malloc( sizeof(hetao_conf) ) ;
	if( p_conf == NULL )
	{
		if( g_hetao_api_error_buffer[0] == '\0' )
			SNPRINTF( g_hetao_api_error_buffer , sizeof(g_hetao_api_error_buffer)-1 , "alloc failed , errno[%d]" , errno );
		return 1;
	}
	memset( p_conf , 0x00 , sizeof(hetao_conf) );
	
	/* 重置所有HTTP状态码、描述为缺省 */
	ResetAllHttpStatus();
	
	/* 设置缺省主日志 */
	if( getenv(HETAO_ERROR_LOG_PATHFILENAME) == NULL )
		SetLogcFile( "" );
	else
		SetLogcFile( getenv(HETAO_ERROR_LOG_PATHFILENAME) );
	if( getenv(HETAO_ERROR_LOG_LEVEL) == NULL )
	{
		SetLogcLevel( LOGCLEVEL_ERROR );
	}
	else
	{
		ConvertLogcLevel_atoi( getenv(HETAO_ERROR_LOG_LEVEL) , & log_level );
		SetLogcLevel( log_level );
	}
	SetLogcPidCache();
	SetLogcTidCache();
	UpdateLogcDateTimeCacheFirst();
	
	/* 设置缺省配置 */
	SetDefaultConfig( p_conf );
	
	p_conf->worker_processes = conf->worker_processes ;
	
	strcpy( p_conf->listen[0].ip , conf->listen[0].ip );
	p_conf->listen[0].port = conf->listen[0].port ;
	p_conf->_listen_count = 1 ;
	
	strcpy( p_conf->listen[0].website[0].domain , conf->listen[0].website[0].domain );
	strcpy( p_conf->listen[0].website[0].wwwroot , conf->listen[0].website[0].wwwroot );
	strcpy( p_conf->listen[0].website[0].index , conf->listen[0].website[0].index );
	strcpy( p_conf->listen[0].website[0].access_log , conf->listen[0].website[0].access_log );
	p_conf->listen[0].website[0].disable_x_forwarded_for = conf->listen[0].website[0].disable_x_forwarded_for ;
	p_conf->listen[0]._website_count = 1 ;
	
	strcpy( p_conf->error_log , conf->error_log );
	strcpy( p_conf->log_level , conf->log_level );
	
	/* 追加缺省配置 */
	AppendDefaultConfig( p_conf );
	
	/* 转换配置 */
	nret = ConvertConfig( p_conf , p_env ) ;
	if( nret )
	{
		if( g_hetao_api_error_buffer[0] == '\0' )
			SNPRINTF( g_hetao_api_error_buffer , sizeof(g_hetao_api_error_buffer)-1 , "ConvertConfig failed[%d]" , nret );
		free( p_conf );
		free( p_env );
		return -nret;
	}
	
	/* 重新设置主日志 */
	CloseLogcFile();
	
	if( getenv(HETAO_ERROR_LOG_PATHFILENAME) )
		strncpy( p_env->error_log , getenv(HETAO_ERROR_LOG_PATHFILENAME) , sizeof(p_env->error_log)-1 );
	if( getenv(HETAO_ERROR_LOG_LEVEL) )
		ConvertLogcLevel_atoi( getenv(HETAO_ERROR_LOG_LEVEL) , & (p_env->log_level) );
	
	SetLogcFile( p_env->error_log );
	SetLogcLevel( p_env->log_level );
	SetLogcPidCache();
	SetLogcTidCache();
	UpdateLogcDateTimeCacheFirst();
	InfoLogc( __FILE__ , __LINE__ , "--- hetao v%s build %s %s ---" , __HETAO_VERSION , __DATE__ , __TIME__ );
	SetHttpCloseExec( GetLogcFileFd() );
	
	/* 输出配置到日志 */
	DSCLOG_hetao_conf( p_conf );
	
	/* 初始化服务器环境 */
	nret = InitEnvirment( p_env , p_conf ) ;
	free( p_conf );
	if( nret )
	{
		if( g_hetao_api_error_buffer[0] == '\0' )
			SNPRINTF( g_hetao_api_error_buffer , sizeof(g_hetao_api_error_buffer)-1 , "InitEnvirment failed[%d]" , nret );
		ErrorLogc( __FILE__ , __LINE__ , "InitEnvirment failed[%d]" , nret );
		return -nret;
	}
	
	/* 注入回调函数 */
	p_listen_session = list_first_entry( & (p_env->listen_session_list.list) , struct ListenSession , list ) ;
	if( conf->listen[0].website[0].domain[0] == '\0' )
	{
		p_virtual_host = p_listen_session->virtual_hosts.p_virtual_host_default ;
	}
	else
	{
		p_virtual_host = QueryVirtualHostHashNode( & (p_listen_session->virtual_hosts) , conf->listen[0].website[0].domain , (int)strlen(conf->listen[0].website[0].domain) ) ;
		if( p_virtual_host == NULL )
		{
			if( g_hetao_api_error_buffer[0] == '\0' )
				SNPRINTF( g_hetao_api_error_buffer , sizeof(g_hetao_api_error_buffer)-1 , "internal error , domain[%s] not found" , conf->listen[0].website[0].domain );
			FatalLogc( __FILE__ , __LINE__ , "internal error , domain[%s] not found" , conf->listen[0].website[0].domain );
			return -1;
		}
	}
	strcpy( p_virtual_host->uri_location_default.socgi.socgi_type , conf->listen[0].website[0].socgi.socgi_type );
	p_virtual_host->uri_location_default.socgi.socgi_type_len = (int)strlen(p_virtual_host->uri_location_default.socgi.socgi_type) ;
	p_virtual_host->uri_location_default.socgi.pfuncCallHttpApplication = pfuncCallHttpApplication ;
	p_virtual_host->uri_location_default.socgi.http_application_context.user_data = user_data ;
	
#if defined(__linux__)
	return -MonitorProcess( p_env );
#elif defined(_WIN32)
	return -WorkerProcess( p_env );
#endif
}

char *GetHetaoApiError()
{
	return g_hetao_api_error_buffer;
}

