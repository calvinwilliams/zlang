/* Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "zobjects_network.h"

#include "fastertpl.h"

#define CHECK_AND_CREATE_SECTION(_htmlsection_direct_prop_) \
	if( (_htmlsection_direct_prop_)->sec == NULL ) \
	{ \
		(_htmlsection_direct_prop_)->sec = FTCreateSection( NULL , NULL ) ; \
		if( (_htmlsection_direct_prop_)->sec == NULL ) \
		{ \
			SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_ALLOC , "alloc memory for entity" ) \
			return ZLANG_ERROR_ALLOC; \
		} \
		(_htmlsection_direct_prop_)->no_need_for_destroy = 0 ; \
	} \

ZlangInvokeFunction ZlangInvokeFunction_htmlsection_SetShort_string_short;
int ZlangInvokeFunction_htmlsection_SetShort_string_short( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_htmlsection	*htmlsection_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject			*in2 = GetInputParameterInLocalObjectStack(rt,2) ;
	struct ZlangObject			*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	char					*name = NULL ;
	int16_t					s ;
	int					nret = 0 ;
	
	CHECK_AND_CREATE_SECTION( htmlsection_direct_prop )
	
	CallRuntimeFunction_string_GetStringValue( rt , in1 , & name , NULL );
	CallRuntimeFunction_short_GetShortValue( rt , in2 , & s );
	
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "FTSetShort name[%s] s[%"PRIi16"]" , name , s )
	nret = FTSetShort( htmlsection_direct_prop->sec , name , s ) ;
	if( nret )
	{
		CallRuntimeFunction_bool_SetBoolValue( rt , out1 , FALSE );
		return ThrowErrorException( rt , EXCEPTION_CODE_HTMLSECTION_ERROR , EXCEPTION_MESSAGE_SET_SHORT_FAILED );
	}
	
	CallRuntimeFunction_bool_SetBoolValue( rt , out1 , TRUE );
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_htmlsection_SetInt_string_int;
int ZlangInvokeFunction_htmlsection_SetInt_string_int( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_htmlsection	*htmlsection_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject			*in2 = GetInputParameterInLocalObjectStack(rt,2) ;
	struct ZlangObject			*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	char					*name = NULL ;
	int32_t					i ;
	int					nret = 0 ;
	
	CHECK_AND_CREATE_SECTION( htmlsection_direct_prop )
	
	CallRuntimeFunction_string_GetStringValue( rt , in1 , & name , NULL );
	CallRuntimeFunction_int_GetIntValue( rt , in2 , & i );
	
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "FTSetInt name[%s] i[%"PRIi32"]" , name , i )
	nret = FTSetInt( htmlsection_direct_prop->sec , name , i ) ;
	if( nret )
	{
		CallRuntimeFunction_bool_SetBoolValue( rt , out1 , FALSE );
		return ThrowErrorException( rt , EXCEPTION_CODE_HTMLSECTION_ERROR , EXCEPTION_MESSAGE_SET_INT_FAILED );
	}
	
	CallRuntimeFunction_bool_SetBoolValue( rt , out1 , TRUE );
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_htmlsection_SetLong_string_long;
int ZlangInvokeFunction_htmlsection_SetLong_string_long( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_htmlsection	*htmlsection_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject			*in2 = GetInputParameterInLocalObjectStack(rt,2) ;
	struct ZlangObject			*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	char					*name = NULL ;
	int64_t					l ;
	int					nret = 0 ;
	
	CHECK_AND_CREATE_SECTION( htmlsection_direct_prop )
	
	CallRuntimeFunction_string_GetStringValue( rt , in1 , & name , NULL );
	CallRuntimeFunction_long_GetLongValue( rt , in2 , & l );
	
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "FTSetLong name[%s] i[%"PRIi64"]" , name , l )
	nret = FTSetLong( htmlsection_direct_prop->sec , name , (long)l ) ;
	if( nret )
	{
		CallRuntimeFunction_bool_SetBoolValue( rt , out1 , FALSE );
		return ThrowErrorException( rt , EXCEPTION_CODE_HTMLSECTION_ERROR , EXCEPTION_MESSAGE_SET_LONG_FAILED );
	}
	
	CallRuntimeFunction_bool_SetBoolValue( rt , out1 , TRUE );
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_htmlsection_SetFloat_string_float;
int ZlangInvokeFunction_htmlsection_SetFloat_string_float( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_htmlsection	*htmlsection_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject			*in2 = GetInputParameterInLocalObjectStack(rt,2) ;
	struct ZlangObject			*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	char					*name = NULL ;
	float					f ;
	int					nret = 0 ;
	
	CHECK_AND_CREATE_SECTION( htmlsection_direct_prop )
	
	CallRuntimeFunction_string_GetStringValue( rt , in1 , & name , NULL );
	CallRuntimeFunction_float_GetFloatValue( rt , in2 , & f );
	
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "FTSetFloat name[%s] f[%f]" , name , f )
	nret = FTSetFloat( htmlsection_direct_prop->sec , name , f ) ;
	if( nret )
	{
		CallRuntimeFunction_bool_SetBoolValue( rt , out1 , FALSE );
		return ThrowErrorException( rt , EXCEPTION_CODE_HTMLSECTION_ERROR , EXCEPTION_MESSAGE_SET_FLOAT_FAILED );
	}
	
	CallRuntimeFunction_bool_SetBoolValue( rt , out1 , TRUE );
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_htmlsection_SetDouble_string_double;
int ZlangInvokeFunction_htmlsection_SetDouble_string_double( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_htmlsection	*htmlsection_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject			*in2 = GetInputParameterInLocalObjectStack(rt,2) ;
	struct ZlangObject			*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	char					*name = NULL ;
	double					d ;
	int					nret = 0 ;
	
	CHECK_AND_CREATE_SECTION( htmlsection_direct_prop )
	
	CallRuntimeFunction_string_GetStringValue( rt , in1 , & name , NULL );
	CallRuntimeFunction_double_GetDoubleValue( rt , in2 , & d );
	
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "FTSetDouble name[%s] d[%lf]" , name , d )
	nret = FTSetDouble( htmlsection_direct_prop->sec , name , d ) ;
	if( nret )
	{
		CallRuntimeFunction_bool_SetBoolValue( rt , out1 , FALSE );
		return ThrowErrorException( rt , EXCEPTION_CODE_HTMLSECTION_ERROR , EXCEPTION_MESSAGE_SET_DOUBLE_FAILED );
	}
	
	CallRuntimeFunction_bool_SetBoolValue( rt , out1 , TRUE );
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_htmlsection_SetString_string_string;
int ZlangInvokeFunction_htmlsection_SetString_string_string( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_htmlsection	*htmlsection_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject			*in2 = GetInputParameterInLocalObjectStack(rt,2) ;
	struct ZlangObject			*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	char					*name = NULL ;
	char					*str = NULL ;
	int					nret = 0 ;
	
	CHECK_AND_CREATE_SECTION( htmlsection_direct_prop )
	
	CallRuntimeFunction_string_GetStringValue( rt , in1 , & name , NULL );
	CallRuntimeFunction_string_GetStringValue( rt , in2 , & str , NULL );
	
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "FTSetString name[%s] str[%s]" , name , str )
	nret = FTSetString( htmlsection_direct_prop->sec , name , str ) ;
	if( nret )
	{
		CallRuntimeFunction_bool_SetBoolValue( rt , out1 , FALSE );
		return ThrowErrorException( rt , EXCEPTION_CODE_HTMLSECTION_ERROR , EXCEPTION_MESSAGE_SET_STRING_FAILED );
	}
	
	CallRuntimeFunction_bool_SetBoolValue( rt , out1 , TRUE );
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_htmlsection_CreateSection_string;
int ZlangInvokeFunction_htmlsection_CreateSection_string( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_htmlsection	*htmlsection_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject			*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	struct ZlangDirectProperty_htmlsection	*out1_direct_prop = GetObjectDirectProperty(out1) ;
	char					*section_name = NULL ;
	
	CHECK_AND_CREATE_SECTION( htmlsection_direct_prop )
	
	CallRuntimeFunction_string_GetStringValue( rt , in1 , & section_name , NULL );
	
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "FTCreateSection name[%s]" , section_name )
	out1_direct_prop->sec = FTCreateSection( htmlsection_direct_prop->sec , section_name );
	if( out1_direct_prop->sec == NULL )
	{
		UnreferObject( rt , out1 );
		return ThrowErrorException( rt , EXCEPTION_CODE_HTMLSECTION_ERROR , EXCEPTION_MESSAGE_CREATE_SECTION_FAILED );
	}
	out1_direct_prop->no_need_for_destroy = 1 ;
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_htmlsection_CreateDetail_string;
int ZlangInvokeFunction_htmlsection_CreateDetail_string( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_htmlsection	*htmlsection_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject			*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	struct ZlangDirectProperty_htmlsection	*out1_direct_prop = GetObjectDirectProperty(out1) ;
	char					*section_name = NULL ;
	
	CHECK_AND_CREATE_SECTION( htmlsection_direct_prop )
	
	CallRuntimeFunction_string_GetStringValue( rt , in1 , & section_name , NULL );
	
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "FTCreateDetail name[%s]" , section_name )
	out1_direct_prop->sec = FTCreateDetail( htmlsection_direct_prop->sec , section_name );
	if( out1_direct_prop->sec == NULL )
	{
		UnreferObject( rt , out1 );
		return ThrowErrorException( rt , EXCEPTION_CODE_HTMLSECTION_ERROR , EXCEPTION_MESSAGE_CREATE_DETAIL_FAILED );
	}
	out1_direct_prop->no_need_for_destroy = 1 ;
	
	return 0;
}

ZlangCreateDirectPropertyFunction ZlangCreateDirectProperty_htmlsection;
void *ZlangCreateDirectProperty_htmlsection( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_htmlsection	*htmlsection_direct_prop = NULL ;
	
	htmlsection_direct_prop = (struct ZlangDirectProperty_htmlsection *)malloc( sizeof(struct ZlangDirectProperty_htmlsection) ) ;
	if( htmlsection_direct_prop == NULL )
	{
		SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_ALLOC , "alloc memory for entity" )
		return NULL;
	}
	memset( htmlsection_direct_prop , 0x00 , sizeof(struct ZlangDirectProperty_htmlsection) );
	
	return htmlsection_direct_prop;
}

ZlangDestroyDirectPropertyFunction ZlangDestroyDirectProperty_htmlsection;
void ZlangDestroyDirectProperty_htmlsection( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_htmlsection	*htmlsection_direct_prop = GetObjectDirectProperty(obj) ;
	
	if( htmlsection_direct_prop->sec && htmlsection_direct_prop->no_need_for_destroy == 0 )
	{
		FTDestroySection( htmlsection_direct_prop->sec ); htmlsection_direct_prop->sec = NULL ;
	}
	
	free( htmlsection_direct_prop );
	
	return;
}

static struct ZlangDirectFunctions direct_funcs_htmlsection =
	{
		ZLANG_OBJECT_htmlsection , /* char *ancestor_name */
		
		ZlangCreateDirectProperty_htmlsection , /* ZlangCreateDirectPropertyFunction *create_entity_func */
		ZlangDestroyDirectProperty_htmlsection , /* ZlangDestroyDirectPropertyFunction *destroy_entity_func */
		
		NULL , /* ZlangFromCharPtrFunction *from_char_ptr_func */
		NULL , /* ZlangToStringFunction *to_string_func */
		NULL , /* ZlangFromDataPtrFunction *from_data_ptr_func */
		NULL , /* ZlangGetDataPtrFunction *get_data_ptr_func */
		
		NULL , /* ZlangOperatorFunction *oper_PLUS_func */
		NULL , /* ZlangOperatorFunction *oper_MINUS_func */
		NULL , /* ZlangOperatorFunction *oper_MUL_func */
		NULL , /* ZlangOperatorFunction *oper_DIV_func */
		NULL , /* ZlangOperatorFunction *oper_MOD_func */
		
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_NEGATIVE_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_NOT_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_BIT_REVERSE_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_PLUS_PLUS_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_MINUS_MINUS_func */
		
		NULL , /* ZlangCompareFunction *comp_EGUAL_func */
		NULL , /* ZlangCompareFunction *comp_NOTEGUAL_func */
		NULL , /* ZlangCompareFunction *comp_LT_func */
		NULL , /* ZlangCompareFunction *comp_LE_func */
		NULL , /* ZlangCompareFunction *comp_GT_func */
		NULL , /* ZlangCompareFunction *comp_GE_func */
		
		NULL , /* ZlangLogicFunction *logic_AND_func */
		NULL , /* ZlangLogicFunction *logic_OR_func */
		
		NULL , /* ZlangLogicFunction *bit_AND_func */
		NULL , /* ZlangLogicFunction *bit_XOR_func */
		NULL , /* ZlangLogicFunction *bit_OR_func */
		NULL , /* ZlangLogicFunction *bit_MOVELEFT_func */
		NULL , /* ZlangLogicFunction *bit_MOVERIGHT_func */
	} ;

ZlangImportObjectFunction ZlangImportObject_htmlsection;
struct ZlangObject *ZlangImportObject_htmlsection( struct ZlangRuntime *rt )
{
	struct ZlangObject	*obj = NULL ;
	struct ZlangObject	*prop = NULL ;
	struct ZlangFunction	*func = NULL ;
	int			nret = 0 ;
	
	nret = ImportObject( rt , & obj , ZLANG_OBJECT_htmlsection , & direct_funcs_htmlsection , sizeof(struct ZlangDirectFunctions) , NULL ) ;
	if( nret )
	{
		SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_LINK_FUNC_TO_ENTITY , "import object to global objects heap" )
		return NULL;
	}
	
	/* int htmlsection.HTMLSECTION_ERROR */
	prop = AddPropertyInObject( rt , obj , QueryObjectByObjectName(rt,"int") , "HTMLSECTION_ERROR" ) ;
	if( prop == NULL )
		return NULL;
	CallRuntimeFunction_int_SetIntValue( rt , prop , EXCEPTION_CODE_HTMLSECTION_ERROR );
	SetConstantObject( prop );
	
	/* string htmlsection.HTML_FORM_SELECT_OPTION_SELECTED */
	prop = AddPropertyInObject( rt , obj , QueryObjectByObjectName(rt,"string") , "HTML_FORM_SELECT_OPTION_SELECTED" ) ;
	if( prop == NULL )
		return NULL;
	CallRuntimeFunction_string_SetStringValue( rt , prop , " selected=\"selected\"" , -1 );
	SetConstantObject( prop );
	
	/* htmlsection.SetShort(string,short) */
	func = AddFunctionAndParametersInObject( rt , obj , "SetShort" , "SetShort(string,short)" , ZlangInvokeFunction_htmlsection_SetShort_string_short , ZLANG_OBJECT_bool , ZLANG_OBJECT_string,NULL , ZLANG_OBJECT_short,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* htmlsection.SetInt(string,int) */
	func = AddFunctionAndParametersInObject( rt , obj , "SetInt" , "SetInt(string,int)" , ZlangInvokeFunction_htmlsection_SetInt_string_int , ZLANG_OBJECT_bool , ZLANG_OBJECT_string,NULL , ZLANG_OBJECT_int,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* htmlsection.SetLong(string,long) */
	func = AddFunctionAndParametersInObject( rt , obj , "SetLong" , "SetLong(string,long)" , ZlangInvokeFunction_htmlsection_SetLong_string_long , ZLANG_OBJECT_bool , ZLANG_OBJECT_string,NULL , ZLANG_OBJECT_long,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* htmlsection.SetFloat(string,float) */
	func = AddFunctionAndParametersInObject( rt , obj , "SetFloat" , "SetFloat(string,float)" , ZlangInvokeFunction_htmlsection_SetFloat_string_float , ZLANG_OBJECT_bool , ZLANG_OBJECT_string,NULL , ZLANG_OBJECT_float,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* htmlsection.SetDouble(string,double) */
	func = AddFunctionAndParametersInObject( rt , obj , "SetDouble" , "SetDouble(string,double)" , ZlangInvokeFunction_htmlsection_SetDouble_string_double , ZLANG_OBJECT_bool , ZLANG_OBJECT_string,NULL , ZLANG_OBJECT_double,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* htmlsection.SetString(string,string) */
	func = AddFunctionAndParametersInObject( rt , obj , "SetString" , "SetString(string,string)" , ZlangInvokeFunction_htmlsection_SetString_string_string , ZLANG_OBJECT_bool , ZLANG_OBJECT_string,NULL , ZLANG_OBJECT_string,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* htmlsection.CreateSection(string) */
	func = AddFunctionAndParametersInObject( rt , obj , "CreateSection" , "CreateSection(string)" , ZlangInvokeFunction_htmlsection_CreateSection_string , ZLANG_OBJECT_htmlsection , ZLANG_OBJECT_string,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* htmlsection.CreateDetail(string) */
	func = AddFunctionAndParametersInObject( rt , obj , "CreateDetail" , "CreateDetail(string)" , ZlangInvokeFunction_htmlsection_CreateDetail_string , ZLANG_OBJECT_htmlsection , ZLANG_OBJECT_string,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	return obj ;
}

