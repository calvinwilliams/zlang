/*
 * hetao - High Performance Web Server
 * author	: calvin
 * email	: calvinwilliams@163.com
 *
 * Licensed under the LGPL v2.1, see the file LICENSE in base directory.
 */

#include "hetao_in.h"

int ProcessHttpRequest( struct HetaoEnv *p_env , struct HttpSession *p_http_session )
{
	char			*host = NULL ;
	int			host_len ;
	
	int			location_ovector[ PATTERN_OVECCOUNT ] ;
	int			location_ovector_count ;
	char			*uri ;
	int			uri_len ;
	
	/* 查询虚拟主机 */
	host = QueryHttpHeaderPtr( p_http_session->http , "Host" , & host_len ) ;
	if( host == NULL )
	{
		if( p_http_session->p_listen_session->virtual_hosts.p_virtual_host_default )
		{
			DebugLogc( __FILE__ , __LINE__ , "no http request Host , use default virtual host" );
			host = "" , host_len = 0 ;
			p_http_session->p_virtual_host = p_http_session->p_listen_session->virtual_hosts.p_virtual_host_default ;
		}
		else
		{
			ErrorLogc( __FILE__ , __LINE__ , "no http request Host and no default virtual host config matched" );
			return HTTP_FORBIDDEN;
		}
	}
	else
	{
		p_http_session->p_virtual_host = QueryVirtualHostHashNode( & (p_http_session->p_listen_session->virtual_hosts) , host , host_len ) ;
		if( p_http_session->p_virtual_host == NULL )
		{
			if( p_http_session->p_listen_session->virtual_hosts.p_virtual_host_default )
			{
				DebugLogc( __FILE__ , __LINE__ , "http request Host[%.*s] use default virtual host" , host_len,host );
				p_http_session->p_virtual_host = p_http_session->p_listen_session->virtual_hosts.p_virtual_host_default ;
			}
			else
			{
				ErrorLogc( __FILE__ , __LINE__ , "http request Host[%.*s] none or default virtual host config matched" , host_len,host );
				return HTTP_FORBIDDEN;
			}
		}
		else
		{
			DebugLogc( __FILE__ , __LINE__ , "http request Host[%.*s] and get a virtual host wwwroot[%s]" , host_len,host , p_http_session->p_virtual_host->wwwroot );
		}
	}
	
	/* 细分URI子配置 */
	uri = GetHttpHeaderPtr_URI(p_http_session->http,NULL) ;
	uri_len = GetHttpHeaderLen_URI(p_http_session->http) ;
	if( list_empty(&(p_http_session->p_virtual_host->uri_locations.uri_location_node)) )
	{
		/* 如果没有配置location，使用website下的缺省配置 */
		DebugLogc( __FILE__ , __LINE__ , "http request uri[%.*s] use default location" , uri_len,uri );
		p_http_session->p_uri_location = & (p_http_session->p_virtual_host->uri_location_default) ;
		return ProcessHttpUriRequest( p_env , p_http_session , host , host_len );
	}
	else
	{
		/* 如果有配置location，正则匹配URI，如果找不到则报错 */
		list_for_each_entry( p_http_session->p_uri_location , & (p_http_session->p_virtual_host->uri_locations.uri_location_node) , struct UriLocation , uri_location_node )
		{
			memset( location_ovector , 0x00 , sizeof(location_ovector) );
			DebugLogc( __FILE__ , __LINE__ , "pcre_exec location[%s] uri[%d][%.*s] ..." , p_http_session->p_uri_location->location , uri_len , uri_len,uri );
			location_ovector_count = pcre_exec( p_http_session->p_uri_location->location_re , NULL , uri , uri_len , 0 , 0 , location_ovector , PATTERN_OVECCOUNT ) ;
			DebugLogc( __FILE__ , __LINE__ , "pcre_exec location[%s] uri[%d][%.*s] return[%d]" , p_http_session->p_uri_location->location , uri_len , uri_len,uri , location_ovector_count );
			if( location_ovector_count > 0 )
			{
				DebugLogc( __FILE__ , __LINE__ , "http request uri[%.*s] set location[%s]" , uri_len,uri , p_http_session->p_uri_location->location );
				return ProcessHttpUriRequest( p_env , p_http_session , host , host_len );
			}
		}

		ErrorLogc( __FILE__ , __LINE__ , "http request uri[%.*s] not found in all locations" , uri_len,uri );
		return HTTP_FORBIDDEN;
	}
}

int ProcessHttpUriRequest( struct HetaoEnv *p_env , struct HttpSession *p_http_session , char *host , char host_len )
{
	struct UriLocation	*p_uri_location = p_http_session->p_uri_location ;
	struct SoCgi		*p_socgi = & (p_uri_location->socgi) ;
	
	struct RewriteUriNode	*p_rewrite_uri = NULL ;
	struct RedirectDomain	*p_redirect_domain = NULL ;
	char			*p_uri = NULL ;
	char			uri[ MAX_URI_LENGTH ] ;
	int			uri_len ;
	
	int			nret = 0 ;
	int			nret2 = 0 ;
	
	p_uri_location->socgi.http_application_context.p_http_session = p_http_session ;
	p_uri_location->socgi.http_application_context.p_listen_session = p_http_session->p_listen_session ;
	p_uri_location->socgi.http_application_context.p_virtual_host = p_http_session->p_virtual_host ;
	p_uri_location->socgi.http_application_context.p_uri_location = p_http_session->p_uri_location ;
	
	/* 域名重定向 */
	if( p_uri_location->socgi.pfuncRedirectHttpDomain )
	{
		InfoLogc( __FILE__ , __LINE__ , "call pfuncRedirectHttpDomain[%p] ..." , p_socgi->pfuncRedirectHttpDomain );
		nret = p_uri_location->socgi.pfuncRedirectHttpDomain( & (p_uri_location->socgi.http_application_context) ) ;
		if( nret == 0 || nret == HTTP_OK )
		{
			InfoLogc( __FILE__ , __LINE__ , "call pfuncRedirectHttpDomain return[%d]" , nret );
		}
		else
		{
			ErrorLogc( __FILE__ , __LINE__ , "call pfuncRedirectHttpDomain return[%d]" , nret );
			return nret;
		}
	}
	
	if(	
		(
			p_uri_location->socgi.pfuncRedirectHttpDomain
			&&
			nret == 0
		)
		||
		(
			p_uri_location->socgi.pfuncRedirectHttpDomain == NULL
			&&
			p_uri_location->redirect_domains.redirect_domain_count > 0
		)
	)
	{
		p_redirect_domain = QueryRedirectDomainHashNode( & (p_uri_location->redirect_domains) , host , host_len ) ;
		if( p_redirect_domain == NULL )
		{
			if( p_uri_location->redirect_domains.p_redirect_domain_default )
			{
				DebugLogc( __FILE__ , __LINE__ , "use default redirect domain" );
				p_redirect_domain = p_uri_location->redirect_domains.p_redirect_domain_default ;
			}
		}
		else
		{
			DebugLogc( __FILE__ , __LINE__ , "redirect domain[%s]" , p_redirect_domain->new_domain );
		}
		
		if( p_redirect_domain )
		{
			nret = FormatHttpResponseStartLine( HTTP_MOVED_PERMANNETLY , p_http_session->http , 0 , "Location: %s%.*s" HTTP_RETURN_NEWLINE HTTP_RETURN_NEWLINE , p_redirect_domain->new_domain , GetHttpHeaderLen_URI(p_http_session->http),GetHttpHeaderPtr_URI(p_http_session->http,NULL) ) ;
			if( nret )
			{
				ErrorLogc( __FILE__ , __LINE__ , "FormatHttpResponseStartLine failed[%d] , errno[%d]" , nret , ERRNO );
				return -1;
			}
			
			SetHttpKeepAlive( p_http_session->http , 0 );

			return HTTP_MOVED_PERMANNETLY;
		}
	}
	
	/* 重写地址处理 */
	if( p_uri_location->socgi.pfuncRewriteHttpUri )
	{
		InfoLogc( __FILE__ , __LINE__ , "call pfuncRewriteHttpUri[%p] ..." , p_socgi->pfuncRewriteHttpUri );
		nret = p_uri_location->socgi.pfuncRewriteHttpUri( & (p_uri_location->socgi.http_application_context) ) ;
		if( nret == 0 || nret == HTTP_OK )
		{
			InfoLogc( __FILE__ , __LINE__ , "call pfuncRewriteHttpUri return[%d]" , nret );
		}
		else
		{
			ErrorLogc( __FILE__ , __LINE__ , "call pfuncRewriteHttpUri return[%d]" , nret );
			return nret;
		}
	}
	
	if(
		(
			p_uri_location->socgi.pfuncRewriteHttpUri
			&&
			nret == 0
		)
		||
		(
			p_uri_location->socgi.pfuncRewriteHttpUri == NULL
			&&
			p_env->new_uri_re
		)
	)
	{
		p_uri = NULL ;
		list_for_each_entry( p_rewrite_uri , & (p_uri_location->rewrite_uris.rewrite_uri_list.rewrite_uri_node) , struct RewriteUriNode , rewrite_uri_node )
		{
			strcpy( uri , p_rewrite_uri->new_uri );
			uri_len = p_rewrite_uri->new_uri_len ;
			
			nret = RegexReplaceString( p_rewrite_uri->pattern_re , GetHttpHeaderPtr_URI(p_http_session->http,NULL) , GetHttpHeaderLen_URI(p_http_session->http) , p_env->new_uri_re , uri , & uri_len , sizeof(uri) ) ;
			if( nret == 0 )
			{
				DebugLogc( __FILE__ , __LINE__ , "RegexReplaceString[%.*s][%s][%s] ok[%.*s]" , GetHttpHeaderLen_URI(p_http_session->http) , GetHttpHeaderPtr_URI(p_http_session->http,NULL) , p_rewrite_uri->pattern , p_rewrite_uri->new_uri , uri_len , uri );
				p_uri = uri ;
				break;
			}
			else if( nret == -1 )
			{
				ErrorLogc( __FILE__ , __LINE__ , "RegexReplaceString[%.*s][%s][%s] failed[%d] , errno[%d]" , GetHttpHeaderLen_URI(p_http_session->http) , GetHttpHeaderPtr_URI(p_http_session->http,NULL) , p_rewrite_uri->pattern , p_rewrite_uri->new_uri , nret , ERRNO );
				return HTTP_BAD_REQUEST;
			}
			else
			{
				DebugLogc( __FILE__ , __LINE__ , "RegexReplaceString[%.*s][%s][%s] continue" , GetHttpHeaderLen_URI(p_http_session->http) , GetHttpHeaderPtr_URI(p_http_session->http,NULL) , p_rewrite_uri->pattern , p_rewrite_uri->new_uri );
			}
		}
		if( p_uri == NULL )
		{
			p_uri = GetHttpHeaderPtr_URI(p_http_session->http,NULL) ;
			uri_len = GetHttpHeaderLen_URI(p_http_session->http) ;
		}
	}
	else
	{
		p_uri = GetHttpHeaderPtr_URI(p_http_session->http,NULL) ;
		uri_len = GetHttpHeaderLen_URI(p_http_session->http) ;
	}
	
#if ( defined _WIN32 )
	if( p_uri[uri_len-1] == '/' || p_uri[uri_len-1] == '\\' )
		uri_len--;
#endif
	
	/* 处理HTTP资源前 */
	if( p_uri_location->socgi.pfuncBeforeProcessHttpResource )
	{
		InfoLogc( __FILE__ , __LINE__ , "call pfuncBeforeProcessHttpResource[%p] ..." , p_socgi->pfuncBeforeProcessHttpResource );
		nret = p_uri_location->socgi.pfuncBeforeProcessHttpResource( & (p_uri_location->socgi.http_application_context) ) ;
		if( nret == HTTP_OK )
		{
			InfoLogc( __FILE__ , __LINE__ , "call pfuncBeforeProcessHttpResource return[%d]" , nret );
		}
		else
		{
			ErrorLogc( __FILE__ , __LINE__ , "call pfuncBeforeProcessHttpResource return[%d]" , nret );
			return nret;
		}
	}
	
	/* 处理HTTP资源 */
	if( p_uri_location->socgi.pfuncProcessHttpResource )
	{
		InfoLogc( __FILE__ , __LINE__ , "call pfuncProcessHttpResource[%p] ..." , p_socgi->pfuncProcessHttpResource );
		nret = p_uri_location->socgi.pfuncProcessHttpResource( & (p_uri_location->socgi.http_application_context) ) ;
		if( nret == 0 || nret == HTTP_OK )
		{
			InfoLogc( __FILE__ , __LINE__ , "call pfuncProcessHttpResource return[%d]" , nret );
		}
		else
		{
			ErrorLogc( __FILE__ , __LINE__ , "call pfuncProcessHttpResource return[%d]" , nret );
			return nret;
		}
	}
	
	if(
		p_uri_location->socgi.pfuncProcessHttpResource == NULL
		||
		(
			p_uri_location->socgi.pfuncProcessHttpResource
			&&
			nret == 0
		)
	)
	{
		nret = ProcessHttpResource( p_env , p_http_session , p_http_session->p_virtual_host->wwwroot , p_uri , uri_len ) ;
		if( nret < 0 )
		{
			FatalLogc( __FILE__ , __LINE__ , "ProcessHttpResource return[%d]" , nret );
		}
		else if( nret == HTTP_OK )
		{
			InfoLogc( __FILE__ , __LINE__ , "ProcessHttpResource return[%d]" , nret );
		}
		else if( nret < HTTP_MULTIPLE_CHOICES )
		{
			InfoLogc( __FILE__ , __LINE__ , "ProcessHttpResource return[%d]" , nret );
		}
		else
		{
			ErrorLogc( __FILE__ , __LINE__ , "ProcessHttpResource return[%d]" , nret );
		}
	}
	
	/* 处理HTTP资源后 */
	if( p_uri_location->socgi.pfuncAfterProcessHttpResource )
	{
		InfoLogc( __FILE__ , __LINE__ , "call pfuncAfterProcessHttpResource[%p] ..." , p_socgi->pfuncAfterProcessHttpResource );
		nret2 = p_uri_location->socgi.pfuncAfterProcessHttpResource( & (p_uri_location->socgi.http_application_context) ) ;
		if( nret2 == HTTP_OK )
		{
			InfoLogc( __FILE__ , __LINE__ , "pfuncAfterProcessHttpResource return[%d]" , nret );
		}
		else
		{
			ErrorLogc( __FILE__ , __LINE__ , "pfuncAfterProcessHttpResource return[%d]" , nret );
			return nret2;
		}
	}
	
	return nret;
}

