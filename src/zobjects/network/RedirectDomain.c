/*
 * hetao - High Performance Web Server
 * author	: calvin
 * email	: calvinwilliams@163.com
 *
 * Licensed under the LGPL v2.1, see the file LICENSE in base directory.
 */

#include "hetao_in.h"

int InitRedirectDomainHash( struct RedirectDomains *p_redirect_domain_hash , int count )
{
	int			i ;
	struct hlist_head	*p_hlist_head = NULL ;
	
	p_redirect_domain_hash->redirect_domain_hashsize = count * 2 ;
	p_redirect_domain_hash->redirect_domain_hash = (struct hlist_head *)malloc( sizeof(struct hlist_head) * p_redirect_domain_hash->redirect_domain_hashsize ) ;
	if( p_redirect_domain_hash->redirect_domain_hash == NULL )
	{
		ErrorLogc( __FILE__ , __LINE__ , "malloc failed , errno[%d]" , ERRNO );
		return -1;
	}
	memset( p_redirect_domain_hash->redirect_domain_hash , 0x00 , sizeof(struct hlist_head) * p_redirect_domain_hash->redirect_domain_hashsize );
	
	for( i = 0 , p_hlist_head = p_redirect_domain_hash->redirect_domain_hash ; i < p_redirect_domain_hash->redirect_domain_hashsize ; i++ , p_hlist_head++ )
	{
		INIT_HLIST_HEAD( p_hlist_head );
	}
	
	return 0;
}

struct RedirectDomain *CreateRedirectDomainHashNode( char *domain , char *new_domain )
{
	struct RedirectDomain	*p_redirect_domain = NULL ;
	
	if( new_domain[0] == '\0' )
	{
		ErrorLogc( __FILE__ , __LINE__ , "redirect domain invalid , domain[%s] new_domain[%s]" , domain , new_domain );
		return NULL;
	}
	
	p_redirect_domain = (struct RedirectDomain *)malloc( sizeof(struct RedirectDomain) ) ;
	if( p_redirect_domain == NULL )
	{
		ErrorLogc( __FILE__ , __LINE__ , "malloc failed[%d] , errno[%d]" , ERRNO );
		return NULL;
	}
	memset( p_redirect_domain , 0x00 , sizeof(struct RedirectDomain) );
	
	strcpy( p_redirect_domain->domain , domain );
	strcpy( p_redirect_domain->new_domain , new_domain );
	p_redirect_domain->domain_len = (int)strlen( p_redirect_domain->domain ) ;
	
	return p_redirect_domain;
}

void CleanRedirectDomainHash( struct RedirectDomains *p_redirect_domain_hash )
{
	int			i ;
	struct hlist_head	*p_hlist_head = NULL ;
	struct hlist_node	*curr = NULL , *next = NULL ;
	struct RedirectDomain	*p_redirect_domain = NULL ;
	
	for( i = 0 , p_hlist_head = p_redirect_domain_hash->redirect_domain_hash ; i < p_redirect_domain_hash->redirect_domain_hashsize ; i++ , p_hlist_head++ )
	{
		hlist_for_each_safe( curr , next , p_hlist_head )
		{
			hlist_del( curr );
			p_redirect_domain = container_of(curr,struct RedirectDomain,redirect_domain_node) ;
			free( p_redirect_domain );
		}
	}
	
	free( p_redirect_domain_hash->redirect_domain_hash );
	p_redirect_domain_hash->redirect_domain_hash = NULL ;
	
	return;
}

int PushRedirectDomainHashNode( struct RedirectDomains *p_redirect_domain_hash , struct RedirectDomain *p_redirect_domain )
{
	int			index ;
	struct hlist_head	*p_hlist_head = NULL ;
	struct RedirectDomain	*p ;
	
	index = CalcHash(p_redirect_domain->domain,p_redirect_domain->domain_len) % (p_redirect_domain_hash->redirect_domain_hashsize) ;
	DebugLogc( __FILE__ , __LINE__ , "RedirectDomain[%d]=CalcHash[%.*s][%d]" , index , p_redirect_domain->domain_len , p_redirect_domain->domain , p_redirect_domain_hash->redirect_domain_hashsize );
	p_hlist_head = p_redirect_domain_hash->redirect_domain_hash + index ;
	hlist_for_each_entry( p , p_hlist_head , struct RedirectDomain , redirect_domain_node )
	{
		if( STRCMP( p->domain , == , p_redirect_domain->domain ) )
			return 1;
	}
	hlist_add_head( & (p_redirect_domain->redirect_domain_node) , p_hlist_head );
	
	p_redirect_domain_hash->redirect_domain_count++;
	
	return 0;
}

struct RedirectDomain *QueryRedirectDomainHashNode( struct RedirectDomains *p_redirect_domain_hash , char *domain , int domain_len )
{
	int			index ;
	struct hlist_head	*p_hlist_head = NULL ;
	struct RedirectDomain	*p ;
	
	index = CalcHash(domain,domain_len) % (p_redirect_domain_hash->redirect_domain_hashsize) ;
	p_hlist_head = p_redirect_domain_hash->redirect_domain_hash + index ;
	hlist_for_each_entry( p , p_hlist_head , struct RedirectDomain , redirect_domain_node )
	{
		if( p->domain_len == domain_len && STRNCMP( p->domain , == , domain , domain_len ) )
			return p;
	}
	
	return NULL;
}

