/* Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "zobjects_network.h"

#include "fasterhttp.h"

struct ZlangDirectProperty_http
{
	struct ZlangTcp		tcp ;
	struct HttpEnv		*env ;
	struct HttpBuffer	*headers ;
	struct HttpBuffer	*body ;
} ;

static struct ZlangDirectFunctions direct_funcs_http ;

ZlangInvokeFunction ZlangInvokeFunction_http_SetConnectingTimeout_int;
int ZlangInvokeFunction_http_SetConnectingTimeout_int( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_http	*http_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject		*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	int32_t				connecting_timeout ;
	
	CallRuntimeFunction_int_GetIntValue( rt , in1 , & connecting_timeout );
	
	ZLANG_TCP_SET_CONNECTINGTIMEOUT( http_direct_prop->tcp , connecting_timeout );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_http_GetConnectingElapse;
int ZlangInvokeFunction_http_GetConnectingElapse( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_http	*http_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject		*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	
	CallRuntimeFunction_int_SetIntValue( rt , out1 , ZLANG_TCP_GET_CONNECTINGELAPSE(http_direct_prop->tcp) );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_http_SetDataTransmissionTimeout_int;
int ZlangInvokeFunction_http_SetDataTransmissionTimeout_int( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_http	*http_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject		*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	int32_t				data_transmission_timeout ;
	
	CallRuntimeFunction_int_GetIntValue( rt , in1 , & data_transmission_timeout );
	
	if( http_direct_prop->env )
	{
		SetHttpTimeout( http_direct_prop->env , data_transmission_timeout );
	}
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_http_Listen_string_int;
int ZlangInvokeFunction_http_Listen_string_int( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_http	*http_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject		*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject		*in2 = GetInputParameterInLocalObjectStack(rt,2) ;
	struct ZlangObject		*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	char				*ip = NULL ;
	int32_t				port ;
	int				nret = 0 ;
	
	CallRuntimeFunction_string_GetStringValue( rt , in1 , & ip , NULL );
	CallRuntimeFunction_int_GetIntValue( rt , in2 , & port );
	nret = ZlangListenTcp( rt , & (http_direct_prop->tcp) , ip , port ) ;
	if( nret )
	{
		CallRuntimeFunction_bool_SetBoolValue( rt , out1 , FALSE );
		return ThrowErrorException( rt , EXCEPTION_CODE_SOCKET_ERROR , EXCEPTION_MESSAGE_LISTEN_SOCKET_FAILED );
	}
	
	CallRuntimeFunction_bool_SetBoolValue( rt , out1 , TRUE );
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_http_Accept;
int ZlangInvokeFunction_http_Accept( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_http	*listen_http_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject		*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	struct ZlangTcp			accept_tcp ;
	struct ZlangDirectProperty_http	*accept_http_direct_prop = NULL ;
	int				nret = 0 ;
	
	memset( & accept_tcp , 0x00 , sizeof(struct ZlangTcp) );
	nret = ZlangAcceptTcp( rt , & (listen_http_direct_prop->tcp) , & accept_tcp ) ;
	if( nret )
	{
		UnreferObject( rt , out1 );
		return ThrowErrorException( rt , EXCEPTION_CODE_SOCKET_ERROR , EXCEPTION_MESSAGE_ACCEPT_SOCKET_FAILED );
	}
	
	nret = InitObject( rt , out1 , NULL , & direct_funcs_http , 0 ) ;
	if( nret )
	{
		UnreferObject( rt , out1 );
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "InitObject failed[%d]" , nret )
		return nret;
	}
	accept_http_direct_prop = GetObjectDirectProperty(out1) ;
	
	strcpy( accept_http_direct_prop->tcp.ip , accept_tcp.ip );
	accept_http_direct_prop->tcp.port = accept_tcp.port ;
	accept_http_direct_prop->tcp.sock = accept_tcp.sock ;
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_http_Connect_string_int;
int ZlangInvokeFunction_http_Connect_string_int( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_http	*http_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject		*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject		*in2 = GetInputParameterInLocalObjectStack(rt,2) ;
	struct ZlangObject		*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	char				*ip = NULL ;
	int32_t				port ;
	int				nret = 0 ;
	
	CallRuntimeFunction_string_GetStringValue( rt , in1 , & ip , NULL );
	CallRuntimeFunction_int_GetIntValue( rt , in2 , & port );
	
	nret = ZlangConnectTcp( rt , & (http_direct_prop->tcp) , ip , port ) ;
	if( nret )
	{
		CallRuntimeFunction_bool_SetBoolValue( rt , out1 , FALSE );
		return ThrowErrorException( rt , EXCEPTION_CODE_SOCKET_ERROR , EXCEPTION_MESSAGE_CONNECT_SOCKET_FAILED );
	}
	
	CallRuntimeFunction_bool_SetBoolValue( rt , out1 , TRUE );
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_http_Close;
int ZlangInvokeFunction_http_Close( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_http	*http_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject		*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	int				nret = 0 ;
	
	nret = ZlangCloseTcp( rt , & (http_direct_prop->tcp) ) ;
	if( nret )
	{
		CallRuntimeFunction_bool_SetBoolValue( rt , out1 , FALSE );
		return ThrowErrorException( rt , EXCEPTION_CODE_SOCKET_ERROR , EXCEPTION_MESSAGE_CLOSE_SOCKET_FAILED );
	}
	
	CallRuntimeFunction_bool_SetBoolValue( rt , out1 , TRUE );
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_http_GetIp;
int ZlangInvokeFunction_http_GetIp( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_http	*http_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject		*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	
	if( ZLANG_TCP_IS_SOCK_INVALID(http_direct_prop->tcp) )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "sock[%"PRIsock"] is not avarible" , ZLANG_TCP_GET_SOCK(http_direct_prop->tcp) )
		CallRuntimeFunction_string_SetStringValue( rt , out1 , "" , 0 );
		return ThrowErrorException( rt , EXCEPTION_CODE_SOCKET_ERROR , EXCEPTION_MESSAGE_SOCKET_INVALID );
	}
	
	CallRuntimeFunction_string_SetStringValue( rt , out1 , ZLANG_TCP_GET_IP(http_direct_prop->tcp) , (int32_t)strlen(ZLANG_TCP_GET_IP(http_direct_prop->tcp)) );
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_http_GetPort;
int ZlangInvokeFunction_http_GetPort( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_http	*http_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject		*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	
	if( ZLANG_TCP_IS_SOCK_INVALID(http_direct_prop->tcp) )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "sock[%"PRIsock"] is not avarible" , ZLANG_TCP_GET_SOCK(http_direct_prop->tcp) )
		CallRuntimeFunction_int_SetIntValue( rt , out1 , -1 );
		return ThrowErrorException( rt , EXCEPTION_CODE_SOCKET_ERROR , EXCEPTION_MESSAGE_SOCKET_INVALID );
	}
	
	CallRuntimeFunction_int_SetIntValue( rt , out1 , ZLANG_TCP_GET_PORT(http_direct_prop->tcp) );
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_http_SetHttpRequestUri_string_string_string;
int ZlangInvokeFunction_http_SetHttpRequestUri_string_string_string( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_http	*http_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject		*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject		*in2 = GetInputParameterInLocalObjectStack(rt,2) ;
	struct ZlangObject		*in3 = GetInputParameterInLocalObjectStack(rt,3) ;
	struct ZlangObject		*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	char				*http_method = NULL ;
	char				*http_uri = NULL ;
	char				*http_version = NULL ;
	struct HttpBuffer		*http_req_buf = NULL ;
	int				nret = 0 ;
	
	CallRuntimeFunction_string_GetStringValue( rt , in1 , & http_method , NULL );
	CallRuntimeFunction_string_GetStringValue( rt , in2 , & http_uri , NULL );
	CallRuntimeFunction_string_GetStringValue( rt , in3 , & http_version , NULL );
	
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "http method[%s]" , http_method )
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "http uri[%s]" , http_uri )
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "http version[%s]" , http_version )
	
	if( STRCMP( http_method , != , HTTP_METHOD_GET ) && STRCMP( http_method , != , HTTP_METHOD_POST ) )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "http method[%s] not supported" , http_method )
		CallRuntimeFunction_bool_SetBoolValue( rt , out1 , FALSE );
		return ThrowErrorException( rt , EXCEPTION_CODE_HTTP_ERROR , EXCEPTION_MESSAGE_HTTP_METHOD_INVALID );
	}
	
	if( http_uri[0] != '/' )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "http uri[%s] invalid" , http_uri )
		CallRuntimeFunction_bool_SetBoolValue( rt , out1 , FALSE );
		return ThrowErrorException( rt , EXCEPTION_CODE_HTTP_ERROR , EXCEPTION_MESSAGE_HTTP_URI_INVALID );
	}
	
	if( STRCMP( http_version , != , HTTP_VERSION_1_1 ) )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "http version[%s] not supported" , http_version )
		CallRuntimeFunction_bool_SetBoolValue( rt , out1 , FALSE );
		return ThrowErrorException( rt , EXCEPTION_CODE_HTTP_ERROR , EXCEPTION_MESSAGE_HTTP_VERSION_INVALID );
	}
	
	if( http_direct_prop->env == NULL )
	{
		/* init http env */
		http_direct_prop->env = CreateHttpEnv() ;
		if( http_direct_prop->env == NULL )
		{
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "CreateHttpEnv failed" )
			CallRuntimeFunction_bool_SetBoolValue( rt , out1 , FALSE );
			return ThrowFatalException( rt , ZLANG_ERROR_INVOKE_METHOD_RETURN , EXCEPTION_MESSAGE_ALLOC_FAILED );
		}
		else
		{
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "CreateHttpEnv ok" )
		}
	}
	else
	{
		ResetHttpEnv( http_direct_prop->env );
	}
	
	http_req_buf = GetHttpRequestBuffer( http_direct_prop->env ) ;
	
	nret = StrcpyfHttpBuffer( http_req_buf , "%s %s %s\r\n" , http_method , http_uri , http_version ) ;
	if( nret )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "StrcpyfHttpBuffer uri failed" )
		CallRuntimeFunction_bool_SetBoolValue( rt , out1 , FALSE );
		return ZLANG_FATAL_INVOKE_METHOD_RETURN;
	}
	
	CallRuntimeFunction_bool_SetBoolValue( rt , out1 , TRUE );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_http_SetHttpRequestHeader_string_string;
int ZlangInvokeFunction_http_SetHttpRequestHeader_string_string( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_http	*http_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject		*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject		*in2 = GetInputParameterInLocalObjectStack(rt,2) ;
	struct ZlangObject		*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	char				*http_header_name = NULL ;
	char				*http_header_value = NULL ;
	struct HttpBuffer		*http_req_buf = NULL ;
	int				nret = 0 ;
	
	if( http_direct_prop->env == NULL )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "call SetHttpRequestUriFirst" )
		CallRuntimeFunction_bool_SetBoolValue( rt , out1 , FALSE );
		return ThrowErrorException( rt , EXCEPTION_CODE_HTTP_ERROR , EXCEPTION_MESSAGE_NO_HTTP_ENVIRONMENT );
	}
	
	CallRuntimeFunction_string_GetStringValue( rt , in1 , & http_header_name , NULL );
	CallRuntimeFunction_string_GetStringValue( rt , in2 , & http_header_value , NULL );
	
	http_req_buf = GetHttpRequestBuffer( http_direct_prop->env ) ;
	
	nret = StrcatfHttpBuffer( http_req_buf , "%s: %s\r\n" , http_header_name , http_header_value ) ;
	if( nret )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "StrcatfHttpBuffer headers failed" )
		CallRuntimeFunction_bool_SetBoolValue( rt , out1 , FALSE );
		return ThrowFatalException( rt , ZLANG_ERROR_INVOKE_METHOD_RETURN , EXCEPTION_MESSAGE_ALLOC_FAILED );
	}
	
	CallRuntimeFunction_bool_SetBoolValue( rt , out1 , TRUE );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_http_SetHttpRequestBody_string;
int ZlangInvokeFunction_http_SetHttpRequestBody_string( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_http	*http_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject		*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject		*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	char				*http_body = NULL ;
	int32_t				http_body_len ;
	struct HttpBuffer		*http_req_buf = NULL ;
	int				nret = 0 ;
	
	if( http_direct_prop->env == NULL )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "call SetHttpRequestUriFirst" )
		CallRuntimeFunction_bool_SetBoolValue( rt , out1 , FALSE );
		return ThrowErrorException( rt , EXCEPTION_CODE_HTTP_ERROR , EXCEPTION_MESSAGE_NO_HTTP_ENVIRONMENT );
	}
	
	CallRuntimeFunction_string_GetStringValue( rt , in1 , & http_body , & http_body_len );
	
	http_req_buf = GetHttpRequestBuffer( http_direct_prop->env ) ;
	
	nret = StrcatfHttpBuffer( http_req_buf , "%s: %d\r\n\r\n" , HTTP_HEADER_CONTENT_LENGTH , http_body_len ) ;
	if( nret )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "StrcatfHttpBuffer '%s' failed" , HTTP_HEADER_CONTENT_LENGTH )
		CallRuntimeFunction_bool_SetBoolValue( rt , out1 , FALSE );
		return ThrowFatalException( rt , ZLANG_FATAL_INVOKE_METHOD_RETURN , EXCEPTION_MESSAGE_ALLOC_FAILED );
	}
	
	nret = MemcatHttpBuffer( http_req_buf , http_body , http_body_len ) ;
	if( nret )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "MemcatHttpBuffer body failed , body_len[%d]" , http_body_len )
		CallRuntimeFunction_bool_SetBoolValue( rt , out1 , FALSE );
		return ThrowFatalException( rt , ZLANG_FATAL_INVOKE_METHOD_RETURN , EXCEPTION_MESSAGE_ALLOC_FAILED );
	}
	
	CallRuntimeFunction_bool_SetBoolValue( rt , out1 , TRUE );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_http_RequestHttp;
int ZlangInvokeFunction_http_RequestHttp( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_http	*http_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject		*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	int				nret = 0 ;
	
	if( ZLANG_TCP_IS_SOCK_INVALID(http_direct_prop->tcp) )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "socket is not listen" )
		CallRuntimeFunction_bool_SetBoolValue( rt , out1 , FALSE );
		return ThrowErrorException( rt , EXCEPTION_CODE_HTTP_ERROR , EXCEPTION_MESSAGE_SOCKET_INVALID );
	}
	
	if( http_direct_prop->env == NULL )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "call SetHttpRequestUriFirst" )
		CallRuntimeFunction_bool_SetBoolValue( rt , out1 , FALSE );
		return ThrowErrorException( rt , EXCEPTION_CODE_HTTP_ERROR , EXCEPTION_MESSAGE_NO_HTTP_ENVIRONMENT );
	}
	
	nret = RequestHttp( ZLANG_TCP_GET_SOCK(http_direct_prop->tcp) , NULL , http_direct_prop->env ) ;
	if( nret )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "HttpRequest failed[%d]" , nret )
		CallRuntimeFunction_bool_SetBoolValue( rt , out1 , FALSE );
		return ThrowErrorException( rt , EXCEPTION_CODE_HTTP_ERROR , EXCEPTION_MESSAGE_REQUESTHTTP_FAILED );
	}
	
	CallRuntimeFunction_bool_SetBoolValue( rt , out1 , TRUE );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_http_GetHttpResponseStatus;
int ZlangInvokeFunction_http_GetHttpResponseStatus( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_http	*http_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject		*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	int				http_status_code ;
	
	if( http_direct_prop->env == NULL )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "call SetHttpRequestUriFirst" )
		CallRuntimeFunction_int_SetIntValue( rt , out1 , -1 );
		return ThrowErrorException( rt , EXCEPTION_CODE_HTTP_ERROR , EXCEPTION_MESSAGE_NO_HTTP_ENVIRONMENT );
	}
	
	http_status_code = GetHttpStatusCode( http_direct_prop->env ) ;
	if( http_status_code < 0 )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "GetHttpStatusCode failed" )
		CallRuntimeFunction_int_SetIntValue( rt , out1 , -1 );
		return ThrowErrorException( rt , EXCEPTION_CODE_HTTP_ERROR , EXCEPTION_MESSAGE_GET_HTTP_STATUS_CODE_FAILED );
	}
	
	CallRuntimeFunction_int_SetIntValue( rt , out1 , http_status_code );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_http_GetHttpResponseHeader_string;
int ZlangInvokeFunction_http_GetHttpResponseHeader_string( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_http	*http_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject		*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject		*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	char				*http_header_name = NULL ;
	char				*http_header_value = NULL ;
	int32_t				http_header_value_len ;
	
	if( http_direct_prop->env == NULL )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "call SetHttpRequestUriFirst" )
		CallRuntimeFunction_int_SetIntValue( rt , out1 , -1 );
		return ThrowErrorException( rt , EXCEPTION_CODE_HTTP_ERROR , EXCEPTION_MESSAGE_NO_HTTP_ENVIRONMENT );
	}
	
	CallRuntimeFunction_string_GetStringValue( rt , in1 , & http_header_name , NULL );
	http_header_value = QueryHttpHeaderPtr( http_direct_prop->env , http_header_name , & http_header_value_len ) ;
	if( http_header_value == NULL )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "QueryHttpHeaderPtr[%s] failed" , http_header_name )
		CallRuntimeFunction_int_SetIntValue( rt , out1 , -1 );
		return ThrowErrorException( rt , EXCEPTION_CODE_HTTP_ERROR , EXCEPTION_MESSAGE_QUERY_HTTP_HEADER_FAILED );
	}
	
	CallRuntimeFunction_string_SetStringValue( rt , out1 , http_header_value , http_header_value_len );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_http_GetHttpResponseBody;
int ZlangInvokeFunction_http_GetHttpResponseBody( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_http	*http_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject		*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	char				*http_body = NULL ;
	int32_t				http_body_len ;
	
	if( http_direct_prop->env == NULL )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "call SetHttpRequestUriFirst" )
		CallRuntimeFunction_int_SetIntValue( rt , out1 , -1 );
		return ThrowErrorException( rt , EXCEPTION_CODE_HTTP_ERROR , EXCEPTION_MESSAGE_NO_HTTP_ENVIRONMENT );
	}
	
	http_body = GetHttpBodyPtr( http_direct_prop->env , & http_body_len ) ;
	if( http_body == NULL )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "GetHttpBodyPtr failed" )
		CallRuntimeFunction_int_SetIntValue( rt , out1 , -1 );
		return ThrowErrorException( rt , EXCEPTION_CODE_HTTP_ERROR , EXCEPTION_MESSAGE_GET_HTTP_BODY_FAILED );
	}
	
	CallRuntimeFunction_string_SetStringValue( rt , out1 , http_body , http_body_len );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_http_ReceiveHttpRequest;
int ZlangInvokeFunction_http_ReceiveHttpRequest( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_http	*http_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject		*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	int				nret = 0 ;
	
	if( http_direct_prop->env == NULL )
	{
		/* init http env */
		http_direct_prop->env = CreateHttpEnv() ;
		if( http_direct_prop->env == NULL )
		{
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "CreateHttpEnv failed" )
			CallRuntimeFunction_bool_SetBoolValue( rt , out1 , FALSE );
			return ThrowFatalException( rt , ZLANG_FATAL_INVOKE_METHOD_RETURN , EXCEPTION_MESSAGE_ALLOC_FAILED );
		}
		else
		{
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "CreateHttpEnv ok" )
		}
	}
	else
	{
		ResetHttpEnv( http_direct_prop->env );
	}
	
	nret = ReceiveHttpRequest( ZLANG_TCP_GET_SOCK(http_direct_prop->tcp) , NULL , http_direct_prop->env ) ;
	if( nret )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "ReceiveHttpRequest failed[%d]" , nret )
		CallRuntimeFunction_bool_SetBoolValue( rt , out1 , FALSE );
		return ThrowErrorException( rt , EXCEPTION_CODE_HTTP_ERROR , EXCEPTION_MESSAGE_RECEIVE_HTTP_REQUEST_FAILED );
	}
	
	CallRuntimeFunction_bool_SetBoolValue( rt , out1 , TRUE );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_http_GetHttpRequestHeader_string;
int ZlangInvokeFunction_http_GetHttpRequestHeader_string( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_http	*http_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject		*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject		*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	char				*http_header_name = NULL ;
	char				*http_header_value = NULL ;
	int32_t				http_header_value_len ;
	
	if( http_direct_prop->env == NULL )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "call SetHttpRequestUriFirst" )
		CallRuntimeFunction_string_SetStringValue( rt , out1 , NULL , 0 );
		return ThrowErrorException( rt , EXCEPTION_CODE_HTTP_ERROR , EXCEPTION_MESSAGE_NO_HTTP_ENVIRONMENT );
	}
	
	CallRuntimeFunction_string_GetStringValue( rt , in1 , & http_header_name , NULL );
	http_header_value = QueryHttpHeaderPtr( http_direct_prop->env , http_header_name , & http_header_value_len ) ;
	if( http_header_value == NULL )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "QueryHttpHeaderPtr[%s] failed" , http_header_name )
		CallRuntimeFunction_string_SetStringValue( rt , out1 , NULL , 0 );
		return ThrowErrorException( rt , EXCEPTION_CODE_HTTP_ERROR , EXCEPTION_MESSAGE_QUERY_HTTP_HEADER_FAILED );
	}
	
	CallRuntimeFunction_string_SetStringValue( rt , out1 , http_header_value , http_header_value_len );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_http_GetHttpRequestBody;
int ZlangInvokeFunction_http_GetHttpRequestBody( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_http	*http_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject		*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	char				*http_body = NULL ;
	int32_t				http_body_len ;
	
	if( http_direct_prop->env == NULL )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "call SetHttpRequestUriFirst" )
		CallRuntimeFunction_string_SetStringValue( rt , out1 , NULL , 0 );
		return ThrowErrorException( rt , EXCEPTION_CODE_HTTP_ERROR , EXCEPTION_MESSAGE_NO_HTTP_ENVIRONMENT );
	}
	
	http_body = GetHttpBodyPtr( http_direct_prop->env , & http_body_len ) ;
	if( http_body == NULL )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "GetHttpBodyPtr failed" )
		CallRuntimeFunction_string_SetStringValue( rt , out1 , NULL , 0 );
		return ThrowErrorException( rt , EXCEPTION_CODE_HTTP_ERROR , EXCEPTION_MESSAGE_GET_HTTP_BODY_FAILED );
	}
	
	CallRuntimeFunction_string_SetStringValue( rt , out1 , http_body , http_body_len );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_http_SetHttpResponseStatus_int;
int ZlangInvokeFunction_http_SetHttpResponseStatus_int( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_http	*http_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject		*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject		*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	int				http_status_code ;
	int				nret = 0 ;
	
	CallRuntimeFunction_int_GetIntValue( rt , in1 , & http_status_code );
	
	nret = FormatHttpResponseStartLine( http_status_code , http_direct_prop->env , 0 , NULL ) ;
	if( nret )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "FormatHttpResponseStartLine failed" )
		CallRuntimeFunction_bool_SetBoolValue( rt , out1 , FALSE );
		return ThrowFatalException( rt , ZLANG_FATAL_INVOKE_METHOD_RETURN , EXCEPTION_MESSAGE_ALLOC_FAILED );
	}
	
	CallRuntimeFunction_bool_SetBoolValue( rt , out1 , TRUE );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_http_SetHttpResponseHeader_string_string;
int ZlangInvokeFunction_http_SetHttpResponseHeader_string_string( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_http	*http_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject		*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject		*in2 = GetInputParameterInLocalObjectStack(rt,2) ;
	struct ZlangObject		*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	char				*http_header_name = NULL ;
	char				*http_header_value = NULL ;
	struct HttpBuffer		*http_rsp_buf = NULL ;
	int				nret = 0 ;
	
	if( http_direct_prop->env == NULL )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "call SetHttpRequestUriFirst" )
		CallRuntimeFunction_bool_SetBoolValue( rt , out1 , FALSE );
		return ThrowErrorException( rt , EXCEPTION_CODE_HTTP_ERROR , EXCEPTION_MESSAGE_NO_HTTP_ENVIRONMENT );
	}
	
	CallRuntimeFunction_string_GetStringValue( rt , in1 , & http_header_name , NULL );
	CallRuntimeFunction_string_GetStringValue( rt , in2 , & http_header_value , NULL );
	
	http_rsp_buf = GetHttpResponseBuffer( http_direct_prop->env ) ;
	
	nret = StrcatfHttpBuffer( http_rsp_buf , "%s: %s\r\n" , http_header_name , http_header_value ) ;
	if( nret )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "StrcatfHttpBuffer headers failed" )
		CallRuntimeFunction_bool_SetBoolValue( rt , out1 , FALSE );
		return ThrowFatalException( rt , ZLANG_FATAL_INVOKE_METHOD_RETURN , EXCEPTION_MESSAGE_ALLOC_FAILED );
	}
	
	CallRuntimeFunction_bool_SetBoolValue( rt , out1 , TRUE );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_http_SetHttpResponseBody_string;
int ZlangInvokeFunction_http_SetHttpResponseBody_string( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_http	*http_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject		*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject		*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	char				*http_body = NULL ;
	int32_t				http_body_len ;
	struct HttpBuffer		*http_rsp_buf = NULL ;
	int				nret = 0 ;
	
	if( http_direct_prop->env == NULL )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "call SetHttpRequestUriFirst" )
		CallRuntimeFunction_bool_SetBoolValue( rt , out1 , FALSE );
		return ThrowErrorException( rt , EXCEPTION_CODE_HTTP_ERROR , EXCEPTION_MESSAGE_NO_HTTP_ENVIRONMENT );
	}
	
	CallRuntimeFunction_string_GetStringValue( rt , in1 , & http_body , & http_body_len );
	
	http_rsp_buf = GetHttpResponseBuffer( http_direct_prop->env ) ;
	
	nret = StrcatfHttpBuffer( http_rsp_buf , "%s: %d\r\n\r\n" , HTTP_HEADER_CONTENT_LENGTH , http_body_len ) ;
	if( nret )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "StrcatfHttpBuffer '%s' failed" , HTTP_HEADER_CONTENT_LENGTH )
		CallRuntimeFunction_bool_SetBoolValue( rt , out1 , FALSE );
		return ThrowFatalException( rt , ZLANG_FATAL_INVOKE_METHOD_RETURN , EXCEPTION_MESSAGE_ALLOC_FAILED );
	}
	
	nret = MemcatHttpBuffer( http_rsp_buf , http_body , http_body_len ) ;
	if( nret )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "MemcatHttpBuffer body failed , body_len[%d]" , http_body_len )
		CallRuntimeFunction_bool_SetBoolValue( rt , out1 , FALSE );
		return ThrowFatalException( rt , ZLANG_FATAL_INVOKE_METHOD_RETURN , EXCEPTION_MESSAGE_ALLOC_FAILED );
	}
	
	CallRuntimeFunction_bool_SetBoolValue( rt , out1 , TRUE );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_http_SendHttpResponse;
int ZlangInvokeFunction_http_SendHttpResponse( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_http	*http_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject		*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	int				nret = 0 ;
	
	if( ZLANG_TCP_IS_SOCK_INVALID(http_direct_prop->tcp) )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "socket is not listen" )
		CallRuntimeFunction_bool_SetBoolValue( rt , out1 , FALSE );
		return ThrowErrorException( rt , EXCEPTION_CODE_SOCKET_ERROR , EXCEPTION_MESSAGE_SOCKET_INVALID );
	}
	
	if( http_direct_prop->env == NULL )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "call SetHttpRequestUriFirst" )
		CallRuntimeFunction_bool_SetBoolValue( rt , out1 , FALSE );
		return ThrowErrorException( rt , EXCEPTION_CODE_HTTP_ERROR , EXCEPTION_MESSAGE_NO_HTTP_ENVIRONMENT );
	}
	
	nret = SendHttpResponse( ZLANG_TCP_GET_SOCK(http_direct_prop->tcp) , NULL , http_direct_prop->env ) ;
	if( nret )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "SendHttpResponse failed[%d]" , nret )
		CallRuntimeFunction_bool_SetBoolValue( rt , out1 , FALSE );
		return 0;
	}
	
	CallRuntimeFunction_bool_SetBoolValue( rt , out1 , TRUE );
	
	return 0;
}

ZlangCreateDirectPropertyFunction ZlangCreateDirectProperty_http;
void *ZlangCreateDirectProperty_http( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_http	*http_direct_prop = NULL ;
	
	http_direct_prop = (struct ZlangDirectProperty_http *)ZLMALLOC( sizeof(struct ZlangDirectProperty_http) ) ;
	if( http_direct_prop == NULL )
	{
		SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_ALLOC , "alloc memory for entity" )
		return NULL;
	}
	memset( http_direct_prop , 0x00 , sizeof(struct ZlangDirectProperty_http) );
	
	ZLANG_TCP_SET_SOCK( http_direct_prop->tcp , -1 );
	ZLANG_TCP_SET_CONNECTINGTIMEOUT( http_direct_prop->tcp , -1 );
	ZLANG_TCP_SET_DATATRANSMISSIONTIMEOUT( http_direct_prop->tcp , -1 );
	
	return http_direct_prop;
}

ZlangDestroyDirectPropertyFunction ZlangDestroyDirectProperty_http;
void ZlangDestroyDirectProperty_http( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_http	*http_direct_prop = GetObjectDirectProperty(obj) ;
	
	/* destroy http env */
	if( http_direct_prop->env )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "DestroyHttpEnv" )
		DestroyHttpEnv( http_direct_prop->env ); http_direct_prop->env = NULL ;
	}
	
	/* close sock */
	if( ZLANG_TCP_IS_SOCK_VALID(http_direct_prop->tcp) )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "close sock[%"PRIsock"]" , ZLANG_TCP_GET_SOCK(http_direct_prop->tcp) )
		CLOSESOCKET( ZLANG_TCP_GET_SOCK(http_direct_prop->tcp) ); ZLANG_TCP_GET_SOCK(http_direct_prop->tcp) = SOCKET_INIT ;
	}
	
	ZLFREE( http_direct_prop );
	
	return;
}

ZlangToStringFunction ZlangToString_http;
int ZlangToString_http( struct ZlangRuntime *rt , struct ZlangObject *obj , struct ZlangObject **tostr_obj )
{
	struct ZlangDirectProperty_http	*http_direct_prop = GetObjectDirectProperty(obj) ;
	char				buf[ 1+10+1+40+1+10 + 1 ] ;
	int32_t				buf_len ;
	
	memset( buf , 0x00 , sizeof(buf) );
	buf_len = snprintf( buf , sizeof(buf)-1 , "#%"PRIsock"#%s:%"PRIi32 , ZLANG_TCP_GET_SOCK(http_direct_prop->tcp) , ZLANG_TCP_GET_IP(http_direct_prop->tcp) , ZLANG_TCP_GET_PORT(http_direct_prop->tcp) ) ;
	CallRuntimeFunction_string_SetStringValue( rt , (*tostr_obj) , buf , buf_len );
	
	return 0;
}

ZlangSummarizeDirectPropertySizeFunction ZlangSummarizeDirectPropertySize_http;
void ZlangSummarizeDirectPropertySize_http( struct ZlangRuntime *rt , struct ZlangObject *obj , size_t *summarized_obj_size , size_t *summarized_direct_prop_size )
{
	SUMMARIZE_SIZE( summarized_direct_prop_size , sizeof(struct ZlangDirectProperty_http) )
	return;
}

static struct ZlangDirectFunctions direct_funcs_http =
	{
		ZLANG_OBJECT_http , /* char *ancestor_name */
		
		ZlangCreateDirectProperty_http , /* ZlangCreateDirectPropertyFunction *create_entity_func */
		ZlangDestroyDirectProperty_http , /* ZlangDestroyDirectPropertyFunction *destroy_entity_func */
		
		NULL , /* ZlangFromCharPtrFunction *from_char_ptr_func */
		ZlangToString_http , /* ZlangToStringFunction *to_string_func */
		NULL , /* ZlangFromDataPtrFunction *from_data_ptr_func */
		NULL , /* ZlangGetDataPtrFunction *get_data_ptr_func */
		
		NULL , /* ZlangOperatorFunction *oper_PLUS_func */
		NULL , /* ZlangOperatorFunction *oper_MINUS_func */
		NULL , /* ZlangOperatorFunction *oper_MUL_func */
		NULL , /* ZlangOperatorFunction *oper_DIV_func */
		NULL , /* ZlangOperatorFunction *oper_MOD_func */
		
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_NEGATIVE_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_NOT_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_BIT_REVERSE_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_PLUS_PLUS_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_MINUS_MINUS_func */
		
		NULL , /* ZlangCompareFunction *comp_EGUAL_func */
		NULL , /* ZlangCompareFunction *comp_NOTEGUAL_func */
		NULL , /* ZlangCompareFunction *comp_LT_func */
		NULL , /* ZlangCompareFunction *comp_LE_func */
		NULL , /* ZlangCompareFunction *comp_GT_func */
		NULL , /* ZlangCompareFunction *comp_GE_func */
		
		NULL , /* ZlangLogicFunction *logic_AND_func */
		NULL , /* ZlangLogicFunction *logic_OR_func */
		
		NULL , /* ZlangLogicFunction *bit_AND_func */
		NULL , /* ZlangLogicFunction *bit_XOR_func */
		NULL , /* ZlangLogicFunction *bit_OR_func */
		NULL , /* ZlangLogicFunction *bit_MOVELEFT_func */
		NULL , /* ZlangLogicFunction *bit_MOVERIGHT_func */
		
		ZlangSummarizeDirectPropertySize_http , /* ZlangSummarizeDirectPropertySizeFunction *summarize_direct_prop_size_func */
	} ;

ZlangImportObjectFunction ZlangImportObject_http;
struct ZlangObject *ZlangImportObject_http( struct ZlangRuntime *rt )
{
	struct ZlangObject	*obj = NULL ;
	struct ZlangObject	*prop = NULL ;
	struct ZlangFunction	*func = NULL ;
	int			nret = 0 ;
	
	nret = ImportObject( rt , & obj , ZLANG_OBJECT_http , & direct_funcs_http , sizeof(struct ZlangDirectFunctions) , NULL ) ;
	if( nret )
	{
		SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_LINK_FUNC_TO_ENTITY , "import object to global objects heap" )
		return NULL;
	}
	
	/* int http.HTTP_ERROR */
	prop = AddPropertyInObject( rt , obj , QueryObjectByObjectName(rt,"int") , "HTTP_ERROR" ) ;
	if( prop == NULL )
		return NULL;
	CallRuntimeFunction_int_SetIntValue( rt , prop , EXCEPTION_CODE_HTTP_ERROR );
	SetConstantObject( prop );
	
	/* string http.HTTP_METHOD_GET */
	prop = AddPropertyInObject( rt , obj , QueryObjectByObjectName(rt,"string") , "HTTP_METHOD_GET" ) ;
	if( prop == NULL )
		return NULL;
	CallRuntimeFunction_string_SetStringValue( rt , prop , HTTP_METHOD_GET , -1 );
	SetConstantObject( prop );
	
	/* string http.HTTP_METHOD_POST */
	prop = AddPropertyInObject( rt , obj , QueryObjectByObjectName(rt,"string") , "HTTP_METHOD_POST" ) ;
	if( prop == NULL )
		return NULL;
	CallRuntimeFunction_string_SetStringValue( rt , prop , HTTP_METHOD_POST , -1 );
	SetConstantObject( prop );
	
	/* string http.HTTP_VERSION_1_0 */
	prop = AddPropertyInObject( rt , obj , QueryObjectByObjectName(rt,"string") , "HTTP_VERSION_1_0" ) ;
	if( prop == NULL )
		return NULL;
	CallRuntimeFunction_string_SetStringValue( rt , prop , HTTP_VERSION_1_0 , -1 );
	SetConstantObject( prop );
	
	/* string http.HTTP_VERSION_1_1 */
	prop = AddPropertyInObject( rt , obj , QueryObjectByObjectName(rt,"string") , "HTTP_VERSION_1_1" ) ;
	if( prop == NULL )
		return NULL;
	CallRuntimeFunction_string_SetStringValue( rt , prop , HTTP_VERSION_1_1 , -1 );
	SetConstantObject( prop );
	
	/* int http.HTTP_STATUSCODE_OK */
	prop = AddPropertyInObject( rt , obj , QueryObjectByObjectName(rt,"int") , "HTTP_STATUSCODE_OK" ) ;
	if( prop == NULL )
		return NULL;
	CallRuntimeFunction_int_SetIntValue( rt , prop , HTTP_OK );
	SetConstantObject( prop );
	
	/* int http.HTTP_STATUSCODE_CREATED */
	prop = AddPropertyInObject( rt , obj , QueryObjectByObjectName(rt,"int") , "HTTP_STATUSCODE_CREATED" ) ;
	if( prop == NULL )
		return NULL;
	CallRuntimeFunction_int_SetIntValue( rt , prop , HTTP_CREATED );
	SetConstantObject( prop );
	
	/* int http.HTTP_STATUSCODE_NO_CONTENT */
	prop = AddPropertyInObject( rt , obj , QueryObjectByObjectName(rt,"int") , "HTTP_STATUSCODE_NO_CONTENT" ) ;
	if( prop == NULL )
		return NULL;
	CallRuntimeFunction_int_SetIntValue( rt , prop , HTTP_NO_CONTENT );
	SetConstantObject( prop );
	
	/* int http.HTTP_STATUSCODE_BAD_REQUEST */
	prop = AddPropertyInObject( rt , obj , QueryObjectByObjectName(rt,"int") , "HTTP_STATUSCODE_BAD_REQUEST" ) ;
	if( prop == NULL )
		return NULL;
	CallRuntimeFunction_int_SetIntValue( rt , prop , HTTP_BAD_REQUEST );
	SetConstantObject( prop );
	
	/* int http.HTTP_STATUSCODE_FORBIDDEN */
	prop = AddPropertyInObject( rt , obj , QueryObjectByObjectName(rt,"int") , "HTTP_STATUSCODE_FORBIDDEN" ) ;
	if( prop == NULL )
		return NULL;
	CallRuntimeFunction_int_SetIntValue( rt , prop , HTTP_FORBIDDEN );
	SetConstantObject( prop );
	
	/* int http.HTTP_STATUSCODE_NOT_FOUND */
	prop = AddPropertyInObject( rt , obj , QueryObjectByObjectName(rt,"int") , "HTTP_STATUSCODE_NOT_FOUND" ) ;
	if( prop == NULL )
		return NULL;
	CallRuntimeFunction_int_SetIntValue( rt , prop , HTTP_NOT_FOUND );
	SetConstantObject( prop );
	
	/* int http.HTTP_STATUSCODE_METHOD_NOT_ALLOWED */
	prop = AddPropertyInObject( rt , obj , QueryObjectByObjectName(rt,"int") , "HTTP_STATUSCODE_METHOD_NOT_ALLOWED" ) ;
	if( prop == NULL )
		return NULL;
	CallRuntimeFunction_int_SetIntValue( rt , prop , HTTP_METHOD_NOT_ALLOWED );
	SetConstantObject( prop );
	
	/* int http.HTTP_STATUSCODE_INTERNAL_SERVER_ERROR */
	prop = AddPropertyInObject( rt , obj , QueryObjectByObjectName(rt,"int") , "HTTP_STATUSCODE_INTERNAL_SERVER_ERROR" ) ;
	if( prop == NULL )
		return NULL;
	CallRuntimeFunction_int_SetIntValue( rt , prop , HTTP_INTERNAL_SERVER_ERROR );
	SetConstantObject( prop );
	
	/* http.SetConnectingTimeout(int) */
	func = AddFunctionAndParametersInObject( rt , obj , "SetConnectingTimeout" , "SetConnectingTimeout(int)" , ZlangInvokeFunction_http_SetConnectingTimeout_int , ZLANG_OBJECT_void , ZLANG_OBJECT_int,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* http.SetDataTransmissionTimeout(int) */
	func = AddFunctionAndParametersInObject( rt , obj , "SetDataTransmissionTimeout" , "SetDataTransmissionTimeout(int)" , ZlangInvokeFunction_http_SetDataTransmissionTimeout_int , ZLANG_OBJECT_void , ZLANG_OBJECT_int,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* tcp.GetConnectingElapse() */
	func = AddFunctionAndParametersInObject( rt , obj , "GetConnectingElapse" , "GetConnectingElapse()" , ZlangInvokeFunction_http_GetConnectingElapse , ZLANG_OBJECT_int , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* http.Listen() */
	func = AddFunctionAndParametersInObject( rt , obj , "Listen" , "Listen(string,int)" , ZlangInvokeFunction_http_Listen_string_int , ZLANG_OBJECT_bool , ZLANG_OBJECT_string,NULL , ZLANG_OBJECT_int,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* http.Accept() */
	func = AddFunctionAndParametersInObject( rt , obj , "Accept" , "Accept()" , ZlangInvokeFunction_http_Accept , ZLANG_OBJECT_http , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* http.Connect() */
	func = AddFunctionAndParametersInObject( rt , obj , "Connect" , "Connect(string,int)" , ZlangInvokeFunction_http_Connect_string_int , ZLANG_OBJECT_bool , ZLANG_OBJECT_string,NULL , ZLANG_OBJECT_int,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* http.Close() */
	func = AddFunctionAndParametersInObject( rt , obj , "Close" , "Close()" , ZlangInvokeFunction_http_Close , ZLANG_OBJECT_bool , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* http.GetIp() */
	func = AddFunctionAndParametersInObject( rt , obj , "GetIp" , "GetIp()" , ZlangInvokeFunction_http_GetIp , ZLANG_OBJECT_string , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* http.GetPort() */
	func = AddFunctionAndParametersInObject( rt , obj , "GetPort" , "GetPort()" , ZlangInvokeFunction_http_GetPort , ZLANG_OBJECT_int , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* http.SetHttpRequestUri(string,string,string) */
	func = AddFunctionAndParametersInObject( rt , obj , "SetHttpRequestUri" , "SetHttpRequestUri(string,string,string)" , ZlangInvokeFunction_http_SetHttpRequestUri_string_string_string , ZLANG_OBJECT_bool , ZLANG_OBJECT_string,NULL , ZLANG_OBJECT_string,NULL , ZLANG_OBJECT_string,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* http.SetHttpRequestHeader(string,string) */
	func = AddFunctionAndParametersInObject( rt , obj , "SetHttpRequestHeader" , "SetHttpRequestHeader(string,string)" , ZlangInvokeFunction_http_SetHttpRequestHeader_string_string , ZLANG_OBJECT_bool , ZLANG_OBJECT_string,NULL , ZLANG_OBJECT_string,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* http.SetHttpRequestBody(string) */
	func = AddFunctionAndParametersInObject( rt , obj , "SetHttpRequestBody" , "SetHttpRequestBody(string)" , ZlangInvokeFunction_http_SetHttpRequestBody_string , ZLANG_OBJECT_bool , ZLANG_OBJECT_string,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* http.RequestHttp() */
	func = AddFunctionAndParametersInObject( rt , obj , "RequestHttp" , "RequestHttp()" , ZlangInvokeFunction_http_RequestHttp , ZLANG_OBJECT_bool , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* http.GetHttpResponseStatus() */
	func = AddFunctionAndParametersInObject( rt , obj , "GetHttpResponseStatus" , "GetHttpResponseStatus()" , ZlangInvokeFunction_http_GetHttpResponseStatus , ZLANG_OBJECT_int , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* http.GetHttpResponseHeader(string) */
	func = AddFunctionAndParametersInObject( rt , obj , "GetHttpResponseHeader" , "GetHttpResponseHeader(string)" , ZlangInvokeFunction_http_GetHttpResponseHeader_string , ZLANG_OBJECT_string , ZLANG_OBJECT_string,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* http.GetHttpResponseBody() */
	func = AddFunctionAndParametersInObject( rt , obj , "GetHttpResponseBody" , "GetHttpResponseBody()" , ZlangInvokeFunction_http_GetHttpResponseBody , ZLANG_OBJECT_string , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* http.ReceiveHttpRequest() */
	func = AddFunctionAndParametersInObject( rt , obj , "ReceiveHttpRequest" , "ReceiveHttpRequest()" , ZlangInvokeFunction_http_ReceiveHttpRequest , ZLANG_OBJECT_bool , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* http.GetHttpRequestHeader(string) */
	func = AddFunctionAndParametersInObject( rt , obj , "GetHttpRequestHeader" , "GetHttpRequestHeader(string)" , ZlangInvokeFunction_http_GetHttpRequestHeader_string , ZLANG_OBJECT_string , ZLANG_OBJECT_string,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* http.GetHttpRequestBody() */
	func = AddFunctionAndParametersInObject( rt , obj , "GetHttpRequestBody" , "GetHttpRequestBody()" , ZlangInvokeFunction_http_GetHttpRequestBody , ZLANG_OBJECT_string , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* http.SetHttpResponseStatus(int) */
	func = AddFunctionAndParametersInObject( rt , obj , "SetHttpResponseStatus" , "SetHttpResponseStatus(int)" , ZlangInvokeFunction_http_SetHttpResponseStatus_int , ZLANG_OBJECT_bool , ZLANG_OBJECT_int,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* http.SetHttpResponseHeader(string,string) */
	func = AddFunctionAndParametersInObject( rt , obj , "SetHttpResponseHeader" , "SetHttpResponseHeader(string,string)" , ZlangInvokeFunction_http_SetHttpResponseHeader_string_string , ZLANG_OBJECT_bool , ZLANG_OBJECT_string,NULL , ZLANG_OBJECT_string,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* http.SetHttpResponseBody(string) */
	func = AddFunctionAndParametersInObject( rt , obj , "SetHttpResponseBody" , "SetHttpResponseBody(string)" , ZlangInvokeFunction_http_SetHttpResponseBody_string , ZLANG_OBJECT_bool , ZLANG_OBJECT_string,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* http.SendHttpResponse() */
	func = AddFunctionAndParametersInObject( rt , obj , "SendHttpResponse" , "SendHttpResponse()" , ZlangInvokeFunction_http_SendHttpResponse , ZLANG_OBJECT_bool , NULL ) ;
	if( func == NULL )
		return NULL;
	
	return obj ;
}

