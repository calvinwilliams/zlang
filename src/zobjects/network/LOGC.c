#include "LOGC.h"

/*
 * iLOG3Lite - logc function library written in c
 * author	: calvin
 * email	: calvinwilliams.c@gmail.com
 * LastVersion	: v1.0.9-A
 *
 * Licensed under the LGPL v2.1, see the file LICENSE in base directory.
 */

/* 代码宏 */
#define OFFSET_BUFPTR(_buffer_,_bufptr_,_len_,_buflen_,_remain_len_) \
	if( _len_ > 0 && _buflen_+_len_ <= sizeof(_buffer_)-1 ) \
	{ \
		_bufptr_ += _len_ ; \
		_buflen_ += _len_ ; \
		_remain_len_ -= _len_ ; \
	} \

/* 日志文件名 */
TLS char		g_logc_pathfilename[ MAXLEN_FILENAME + 1 ] = "" ;
TLS int			g_log_level = LOGCLEVEL_INFO ;
TLS int			g_file_fd = -1 ;
TLS unsigned long	g_pid = 0 ;
TLS unsigned long	g_tid = 0 ;
struct DateTimeCache	g_date_time_cache[ LOGC_DATETIMECACHE_SIZE ] = { { 0 } } ;
unsigned char		g_date_time_cache_index = 0 ;

const char log_level_itoa[][6] = { "DEBUG" , "INFO" , "WARN" , "ERROR" , "FATAL" } ;

/* 打开日志文件 */
static int OpenLogcFile()
{
	CloseLogcFile();
	
	if( g_logc_pathfilename[0] == '#' )
		return 0;
	if( g_logc_pathfilename[0] == '\0' )
		return -1;
	
#if ( defined __linux__ ) || ( defined __unix ) || ( defined _AIX )
	g_file_fd = OPEN( g_logc_pathfilename , O_CREAT | O_WRONLY | O_APPEND , S_IRWXU | S_IRWXG | S_IRWXO ) ;
#elif ( defined _WIN32 )
	g_file_fd = OPEN( g_logc_pathfilename , _O_CREAT | _O_WRONLY | _O_APPEND | _O_BINARY , _S_IREAD | _S_IWRITE ) ;
#endif
	if( g_file_fd == -1 )
		return -1;
	
	return 0;
}

/* 设置日志文件名，并打开文件 */
void SetLogcFile( char *format , ... )
{
	va_list		valist ;
	
	if( g_file_fd != -1 )
	{
		CLOSE( g_file_fd );
		g_file_fd = -1 ;
	}
	
	va_start( valist , format );
	VSNPRINTF( g_logc_pathfilename , sizeof(g_logc_pathfilename)-1 , format , valist );
	va_end( valist );
	
	return;
}

/* 关闭日志文件 */
void CloseLogcFile()
{
	if( g_file_fd != -1 )
	{
		CLOSE( g_file_fd );
		g_file_fd = -1 ;
	}
}

/* 设置日志等级 */
void SetLogcLevel( int log_level )
{
	g_log_level = log_level ;
	
	return;
}

int GetLogcLevel()
{
	return g_log_level;
}

static void _UpdateDateTimeCache( unsigned char index )
{
#if ( defined __linux__ ) || ( defined __unix ) || ( defined _AIX )
	struct tm		stime ;
	
	g_date_time_cache[index].second_stamp = time( NULL ) ;
	localtime_r( &(g_date_time_cache[index].second_stamp) , & stime );
	
	SNPRINTF( g_date_time_cache[index].date_and_time_str , sizeof(g_date_time_cache[index].date_and_time_str)-1 , "%04d-%02d-%02d %02d:%02d:%02d" , stime.tm_year+1900 , stime.tm_mon+1 , stime.tm_mday , stime.tm_hour , stime.tm_min , stime.tm_sec );
#elif ( defined _WIN32 )
	{
	SYSTEMTIME	stNow ;
	GetLocalTime( & stNow );
	time( & (g_date_time_cache[index].second_stamp) );
	SNPRINTF( g_date_time_cache[index].date_and_time_str , sizeof(g_date_time_cache[index].date_and_time_str)-1 , "%04d-%02d-%02d %02d:%02d:%02d" , stNow.wYear , stNow.wMonth , stNow.wDay , stNow.wHour , stNow.wMinute , stNow.wSecond );
	}
#endif
	
	
	return;
}

char *GetLogcPathFilename()
{
	return g_logc_pathfilename;
}

int GetLogcFileFd()
{
	return g_file_fd;
}

void SetLogcPidCache()
{
	g_pid = PROCESSID ;
	return;
}

unsigned long GetLogcPidCache()
{
	return g_pid;
}

void SetLogcTidCache()
{
	g_tid = THREADID ;
	return;
}

unsigned long GetLogcTidCache()
{
	return g_tid;
}

/* 第一次更新时间缓冲区 */
void UpdateLogcDateTimeCacheFirst()
{
	_UpdateDateTimeCache( 0 );
	
	return;
}

/* 更新时间缓冲区 */
void UpdateLogcDateTimeCache()
{
	unsigned char	next_date_time_cache_index = g_date_time_cache_index ;
	
	next_date_time_cache_index = ( next_date_time_cache_index + 1 ) % LOGC_DATETIMECACHE_SIZE ;
	
	_UpdateDateTimeCache( next_date_time_cache_index );
	
	g_date_time_cache_index = next_date_time_cache_index ;
	
	return;
}

time_t GetLogcSecondStampCache()
{
	return g_date_time_cache[g_date_time_cache_index].second_stamp;
}

char *GetLogcDateTimeStrCache()
{
	return g_date_time_cache[g_date_time_cache_index].date_and_time_str;
}

/* 输出日志 */
int WriteLogcBaseV( int log_level , char *c_filename , long c_fileline , char *format , va_list valist )
{
	char		c_filename_copy[ MAXLEN_FILENAME + 1 ] ;
	char		*p_c_filename = NULL ;
	
	char		logc_buffer[ 40960 + 1 ] ;
	char		*logc_bufptr = NULL ;
	size_t		logc_buflen ;
	size_t		logc_buf_remain_len ;
	size_t		len ;
	
	int		nret = 0 ;
	
	/* 处理源代码文件名 */
	memset( c_filename_copy , 0x00 , sizeof(c_filename_copy) );
	strncpy( c_filename_copy , c_filename , sizeof(c_filename_copy)-1 );
	p_c_filename = strrchr( c_filename_copy , '\\' ) ;
	if( p_c_filename )
		p_c_filename++;
	else
		p_c_filename = c_filename_copy ;
	
	/* 填充行日志 */
	/* memset( logc_buffer , 0x00 , sizeof(logc_buffer) ); */
	logc_bufptr = logc_buffer ;
	logc_buflen = 0 ;
	logc_buf_remain_len = sizeof(logc_buffer) - 1 ;
	
	/*
	{
	struct timeval	tv ;
	gettimeofday( & tv , NULL );
	len = SNPRINTF( logc_bufptr , logc_buf_remain_len , "%s.%06ld | %-5s | %lu:%lu:%s:%ld | " , g_date_time_cache[g_date_time_cache_index].date_and_time_str , tv.tv_usec , log_level_itoa[log_level] , g_pid , g_tid , p_c_filename , c_fileline ) ;
	}
	*/
	len = SNPRINTF( logc_bufptr , logc_buf_remain_len , "%s | %-5s | %lu:%lu:%s:%ld | " , g_date_time_cache[g_date_time_cache_index].date_and_time_str , log_level_itoa[log_level] , g_pid , g_tid , p_c_filename , c_fileline ) ;
	OFFSET_BUFPTR( logc_buffer , logc_bufptr , len , logc_buflen , logc_buf_remain_len );
	len = VSNPRINTF( logc_bufptr , logc_buf_remain_len , format , valist );
	OFFSET_BUFPTR( logc_buffer , logc_bufptr , len , logc_buflen , logc_buf_remain_len );
	len = SNPRINTF( logc_bufptr , logc_buf_remain_len , NEWLINE ) ;
	OFFSET_BUFPTR( logc_buffer , logc_bufptr , len , logc_buflen , logc_buf_remain_len );
	
	/* 输出行日志 */
	if( g_logc_pathfilename[0] != '#' )
	{
		if( g_file_fd == -1 )
		{
			nret = OpenLogcFile() ;
			if( nret )
				return nret;
		}
		
		WRITE( g_file_fd , logc_buffer , logc_buflen );
	}
	else
	{
		WRITE( 1 , logc_buffer , logc_buflen );
	}
	
	return 0;
}

int WriteLogcBase( int log_level , char *c_filename , long c_fileline , char *format , ... )
{
	va_list		valist ;
	
	int		nret ;
	
	va_start( valist , format );
	nret = WriteLogcBaseV( log_level , c_filename , c_fileline , format , valist );
	va_end( valist );
	
	return nret;
}

#if ( defined __STDC_VERSION__ ) && ( __STDC_VERSION__ >= 199901 )

#else

int WriteLogc( int log_level , char *c_filename , long c_fileline , char *format , ... )
{
	va_list		valist ;
	
	if( log_level < g_log_level )
		return 0;
	
	va_start( valist , format );
	WriteLogcBaseV( log_level , c_filename , c_fileline , format , valist );
	va_end( valist );
	
	return 0;
}

int FatalLogc( char *c_filename , long c_fileline , char *format , ... )
{
	va_list		valist ;
	
	if( LOGCLEVEL_FATAL < g_log_level )
		return 0;
	
	va_start( valist , format );
	WriteLogcBaseV( LOGCLEVEL_FATAL , c_filename , c_fileline , format , valist );
	va_end( valist );
	
	return 0;
}

int ErrorLogc( char *c_filename , long c_fileline , char *format , ... )
{
	va_list		valist ;
	
	if( LOGCLEVEL_ERROR < g_log_level )
		return 0;
	
	va_start( valist , format );
	WriteLogcBaseV( LOGCLEVEL_ERROR , c_filename , c_fileline , format , valist );
	va_end( valist );
	
	return 0;
}

int WarnLogc( char *c_filename , long c_fileline , char *format , ... )
{
	va_list		valist ;
	
	if( LOGCLEVEL_WARN < g_log_level )
		return 0;
	
	va_start( valist , format );
	WriteLogcBaseV( LOGCLEVEL_WARN , c_filename , c_fileline , format , valist );
	va_end( valist );
	
	return 0;
}

int InfoLogc( char *c_filename , long c_fileline , char *format , ... )
{
	va_list		valist ;
	
	if( LOGCLEVEL_INFO < g_log_level )
		return 0;
	
	va_start( valist , format );
	WriteLogcBaseV( LOGCLEVEL_INFO , c_filename , c_fileline , format , valist );
	va_end( valist );
	
	return 0;
}

int DebugLogc( char *c_filename , long c_fileline , char *format , ... )
{
	va_list		valist ;
	
	if( LOGCLEVEL_DEBUG < g_log_level )
		return 0;
	
	va_start( valist , format );
	WriteLogcBaseV( LOGCLEVEL_DEBUG , c_filename , c_fileline , format , valist );
	va_end( valist );
	
	return 0;
}

#endif

int WriteHexLogcBaseV( int log_level , char *c_filename , long c_fileline , char *buf , long buflen , char *format , va_list valist )
{
	char		hexlogc_buffer[ 409600 + 1 ] ;
	char		*hexlogc_bufptr = NULL ;
	size_t		hexlogc_buflen ;
	size_t		hexlogc_buf_remain_len ;
	size_t		len ;
	
	int		row_offset , col_offset ;
	
	int		nret = 0 ;
	
	if( buf == NULL && buflen <= 0 )
		return 0;
	if( buflen > sizeof(hexlogc_buffer) - 1 )
		return -1;
	
	/* 输出行日志 */
	if( format )
	{
		WriteLogcBaseV( log_level , c_filename , c_fileline , format , valist );
	}
	
	/* 填充十六进制块日志 */
	memset( hexlogc_buffer , 0x00 , sizeof(hexlogc_buffer) );
	hexlogc_bufptr = hexlogc_buffer ;
	hexlogc_buflen = 0 ;
	hexlogc_buf_remain_len = sizeof(hexlogc_buffer) - 1 ;
	
	len = SNPRINTF( hexlogc_bufptr , hexlogc_buf_remain_len , "             0  1  2  3  4  5  6  7  8  9  A  B  C  D  E  F    0123456789ABCDEF" ) ;
	OFFSET_BUFPTR( hexlogc_buffer , hexlogc_bufptr , len , hexlogc_buflen , hexlogc_buf_remain_len );
	len = SNPRINTF( hexlogc_bufptr , hexlogc_buf_remain_len , NEWLINE ) ;
	OFFSET_BUFPTR( hexlogc_buffer , hexlogc_bufptr , len , hexlogc_buflen , hexlogc_buf_remain_len );
	
	row_offset = 0 ;
	col_offset = 0 ;
	while(1)
	{
		len = SNPRINTF( hexlogc_bufptr , hexlogc_buf_remain_len , "0x%08X   " , row_offset * 16 ) ;
		OFFSET_BUFPTR( hexlogc_buffer , hexlogc_bufptr , len , hexlogc_buflen , hexlogc_buf_remain_len );
		for( col_offset = 0 ; col_offset < 16 ; col_offset++ )
		{
			if( row_offset * 16 + col_offset < buflen )
			{
				len = SNPRINTF( hexlogc_bufptr , hexlogc_buf_remain_len , "%02X " , *((unsigned char *)buf+row_offset*16+col_offset)) ;
				OFFSET_BUFPTR( hexlogc_buffer , hexlogc_bufptr , len , hexlogc_buflen , hexlogc_buf_remain_len );
			}
			else
			{
				len = SNPRINTF( hexlogc_bufptr , hexlogc_buf_remain_len , "   " ) ;
				OFFSET_BUFPTR( hexlogc_buffer , hexlogc_bufptr , len , hexlogc_buflen , hexlogc_buf_remain_len );
			}
		}
		len = SNPRINTF( hexlogc_bufptr , hexlogc_buf_remain_len , "  " ) ;
		OFFSET_BUFPTR( hexlogc_buffer , hexlogc_bufptr , len , hexlogc_buflen , hexlogc_buf_remain_len );
		for( col_offset = 0 ; col_offset < 16 ; col_offset++ )
		{
			if( row_offset * 16 + col_offset < buflen )
			{
				if( isprint( (int)(unsigned char)*(buf+row_offset*16+col_offset) ) )
				{
					len = SNPRINTF( hexlogc_bufptr , hexlogc_buf_remain_len , "%c" , *((unsigned char *)buf+row_offset*16+col_offset) ) ;
					OFFSET_BUFPTR( hexlogc_buffer , hexlogc_bufptr , len , hexlogc_buflen , hexlogc_buf_remain_len );
				}
				else
				{
					len = SNPRINTF( hexlogc_bufptr , hexlogc_buf_remain_len , "." ) ;
					OFFSET_BUFPTR( hexlogc_buffer , hexlogc_bufptr , len , hexlogc_buflen , hexlogc_buf_remain_len );
				}
			}
			else
			{
				len = SNPRINTF( hexlogc_bufptr , hexlogc_buf_remain_len , " " ) ;
				OFFSET_BUFPTR( hexlogc_buffer , hexlogc_bufptr , len , hexlogc_buflen , hexlogc_buf_remain_len );
			}
		}
		len = SNPRINTF( hexlogc_bufptr , hexlogc_buf_remain_len , NEWLINE ) ;
		OFFSET_BUFPTR( hexlogc_buffer , hexlogc_bufptr , len , hexlogc_buflen , hexlogc_buf_remain_len );
		if( row_offset * 16 + col_offset >= buflen )
			break;
		row_offset++;
	}
	
	/* 输出十六进制块日志 */
	if( g_logc_pathfilename[0] != '#' )
	{
		if( g_file_fd == -1 )
		{
			nret = OpenLogcFile() ;
			if( nret )
				return nret;
		}
		
		WRITE( g_file_fd , hexlogc_buffer , hexlogc_buflen );
	}
	else
	{
		WRITE( 1 , hexlogc_buffer , hexlogc_buflen );
	}
	
	return 0;
}

int WriteHexLogcBase( int log_level , char *c_filename , long c_fileline , char *buf , long buflen , char *format , ... )
{
	va_list		valist ;
	
	int		nret ;
	
	va_start( valist , format );
	nret = WriteHexLogcBaseV( log_level , c_filename , c_fileline , buf , buflen , format , valist ) ;
	va_end( valist );
	
	return nret;
}

#if ( defined __STDC_VERSION__ ) && ( __STDC_VERSION__ >= 199901 )

#else

int WriteHexLogc( int log_level , char *c_filename , long c_fileline , char *buf , long buflen , char *format , ... )
{
	va_list		valist ;
	
	if( log_level < g_log_level )
		return 0;
	
	va_start( valist , format );
	WriteHexLogcBaseV( log_level , c_filename , c_fileline , buf , buflen , format , valist );
	va_end( valist );
	
	return 0;
}

int FatalHexLogc( char *c_filename , long c_fileline , char *buf , long buflen , char *format , ... )
{
	va_list		valist ;
	
	if( LOGCLEVEL_FATAL < g_log_level )
		return 0;
	
	va_start( valist , format );
	WriteHexLogcBaseV( LOGCLEVEL_FATAL , c_filename , c_fileline , buf , buflen , format , valist );
	va_end( valist );
	
	return 0;
}

int ErrorHexLogc( char *c_filename , long c_fileline , char *buf , long buflen , char *format , ... )
{
	va_list		valist ;
	
	if( LOGCLEVEL_ERROR < g_log_level )
		return 0;
	
	va_start( valist , format );
	WriteHexLogcBaseV( LOGCLEVEL_ERROR , c_filename , c_fileline , buf , buflen , format , valist );
	va_end( valist );
	
	return 0;
}

int WarnHexLogc( char *c_filename , long c_fileline , char *buf , long buflen , char *format , ... )
{
	va_list		valist ;
	
	if( LOGCLEVEL_WARN < g_log_level )
		return 0;
	
	va_start( valist , format );
	WriteHexLogcBaseV( LOGCLEVEL_WARN , c_filename , c_fileline , buf , buflen , format , valist );
	va_end( valist );
	
	return 0;
}

int InfoHexLogc( char *c_filename , long c_fileline , char *buf , long buflen , char *format , ... )
{
	va_list		valist ;
	
	if( LOGCLEVEL_INFO < g_log_level )
		return 0;
	
	va_start( valist , format );
	WriteHexLogcBaseV( LOGCLEVEL_INFO , c_filename , c_fileline , buf , buflen , format , valist );
	va_end( valist );
	
	return 0;
}

int DebugHexLogc( char *c_filename , long c_fileline , char *buf , long buflen , char *format , ... )
{
	va_list		valist ;
	
	if( LOGCLEVEL_DEBUG < g_log_level )
		return 0;
	
	va_start( valist , format );
	WriteHexLogcBaseV( LOGCLEVEL_DEBUG , c_filename , c_fileline , buf , buflen , format , valist );
	va_end( valist );
	
	return 0;
}

#endif
