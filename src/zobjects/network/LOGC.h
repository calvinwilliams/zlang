#ifndef LOGCC_H
#define LOGCC_H

/*
 * iLOGC3Lite - logc function library written in c
 * author	: calvin
 * email	: calvinwilliams.c@gmail.com
 * LastVersion	: v1.0.9-A
 *
 * Licensed under the LGPL v2.1, see the file LICENSE in base directory.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <time.h>
#include <stdarg.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <errno.h>

#ifdef __cplusplus
extern "C" {
#endif

#if ( defined __unix ) || ( defined _AIX ) || ( defined __linux__ ) || ( defined __hpux )
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/epoll.h>
#ifndef EPOLLRDHUP
#define EPOLLRDHUP	0x2000
#endif
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/inotify.h>
#include <signal.h>
#include <sys/wait.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include <dirent.h>
#include <pwd.h>
#include <syslog.h>
#define __USE_GNU
#include <sched.h>
#include <pthread.h>
#include <dlfcn.h>
#elif ( defined _WIN32 )
#include <winsock2.h>
#include <stdio.h>
#include <time.h>
#include <sys/types.h>
#include <io.h>
#include <windows.h>
#include <share.h>
#include <io.h>
#include <fcntl.h>
#endif

#if ( defined _WIN32 )
#ifndef _WINDLL_FUNC
#define _WINDLL_FUNC		_declspec(dllexport)
#endif
#elif ( defined __unix ) || ( defined __linux__ )
#ifndef _WINDLL_FUNC
#define _WINDLL_FUNC		extern
#endif
#endif

/* 公共宏 */
#ifndef MAXLEN_FILENAME
#define MAXLEN_FILENAME			256
#endif

#ifndef STRCMP
#define STRCMP(_a_,_C_,_b_) ( strcmp(_a_,_b_) _C_ 0 )
#define STRNCMP(_a_,_C_,_b_,_n_) ( strncmp(_a_,_b_,_n_) _C_ 0 )
#endif

#ifndef MEMCMP
#define MEMCMP(_a_,_C_,_b_,_n_) ( memcmp(_a_,_b_,_n_) _C_ 0 )
#endif

/* 跨平台宏 */
#if ( defined __linux__ ) || ( defined __unix ) || ( defined _AIX )
#define TLS		__thread
#define STRDUP		strdup
#define OPEN		open
#define O_CREAT_WRONLY_APPEND	O_CREAT | O_WRONLY | O_APPEND , S_IRWXU | S_IRWXG | S_IRWXO
#define READ		read
#define WRITE		write
#define CLOSE		close
#define PROCESSID	(unsigned long)getpid()
#define THREADID	(unsigned long)pthread_self()
#define NEWLINE		"\n"
#define VSNPRINTF	vsnprintf
#define SNPRINTF	snprintf
#define CLOSESOCKET	close
#define CLOSESOCKET2(_fd1_,_fd2_)	close(_fd1_),close(_fd2_);
#define ERRNO		errno
#define SOCKLEN_T	socklen_t
#define GETTIMEOFDAY(_tv_)	gettimeofday(&(_tv_),NULL)
#define LOCALTIME(_tt_,_stime_) \
	localtime_r(&(_tt_),&(_stime_));
#define STAT		stat
#define FSTAT		fstat
#define FILENO		fileno
#define ACCESS		access
#define ACCESS_MODE	R_OK
#define UMASK		umask
#define PUTENV		putenv
#define GETENV		getenv
#elif ( defined _WIN32 )
#define TLS		__declspec( thread )
#define STRDUP		_strdup
#define VSNPRINTF	_vsnprintf
#define SNPRINTF	_snprintf
#define OPEN		_open
#define O_CREAT_WRONLY_APPEND	_O_CREAT | _O_WRONLY | _O_APPEND | _O_BINARY , _S_IREAD | _S_IWRITE
#define READ		_read
#define WRITE		_write
#define CLOSE		_close
#define PROCESSID	(unsigned long)GetCurrentProcessId()
#define THREADID	(unsigned long)GetCurrentThreadId()
#define NEWLINE		"\r\n"
#define CLOSESOCKET	closesocket
#define CLOSESOCKET2(_fd1_,_fd2_)	closesocket(_fd1_),closesocket(_fd2_);
#define ERRNO		GetLastError()
#ifndef EWOULDBLOCK
#define EWOULDBLOCK	WSAEWOULDBLOCK
#define ECONNABORTED	WSAECONNABORTED
#define EINPROGRESS	WSAEINPROGRESS
#define ECONNRESET	WSAECONNRESET
#define ENOTCONN	WSAENOTCONN
#define EISCONN		WSAEISCONN
#endif
#define SOCKLEN_T	int
#define GETTIMEOFDAY(_tv_) \
	{ \
		SYSTEMTIME stNow ; \
		GetLocalTime( & stNow ); \
		(_tv_).tv_usec = stNow.wMilliseconds * 1000 ; \
		time( & ((_tv_).tv_sec) ); \
	}
#define SYSTEMTIME2TIMEVAL_USEC(_syst_,_tv_) \
		(_tv_).tv_usec = (_syst_).wMilliseconds * 1000 ;
#define SYSTEMTIME2TM(_syst_,_stime_) \
		(_stime_).tm_year = (_syst_).wYear - 1900 ; \
		(_stime_).tm_mon = (_syst_).wMonth - 1 ; \
		(_stime_).tm_mday = (_syst_).wDay ; \
		(_stime_).tm_hour = (_syst_).wHour ; \
		(_stime_).tm_min = (_syst_).wMinute ; \
		(_stime_).tm_sec = (_syst_).wSecond ;
#define LOCALTIME(_tt_,_stime_) \
	{ \
		SYSTEMTIME	stNow ; \
		GetLocalTime( & stNow ); \
		_SYSTEMTIME2TM( stNow , (_stime_) ); \
	}
#define STAT		_stat
#define FSTAT		_fstat
#define FILENO		_fileno
#define ACCESS		_access
#define ACCESS_MODE	04
#define UMASK		_umask
#define PUTENV		_putenv
#define GETENV		getenv
#endif

/* 简单日志函数 */
#ifndef LOGCLEVEL_DEBUG
#define LOGCLEVEL_DEBUG		0
#define LOGCLEVEL_INFO		1
#define LOGCLEVEL_WARN		2
#define LOGCLEVEL_ERROR		3
#define LOGCLEVEL_FATAL		4
#define LOGCLEVEL_NOLOG		9
#endif

_WINDLL_FUNC void SetLogcFile( char *format , ... );
_WINDLL_FUNC void CloseLogcFile();
_WINDLL_FUNC void SetLogcLevel( int log_level );
_WINDLL_FUNC int GetLogcLevel();

_WINDLL_FUNC char *GetLogcPathFilename();
_WINDLL_FUNC int GetLogcFileFd();
_WINDLL_FUNC void SetLogcPidCache();
_WINDLL_FUNC unsigned long GetLogcPidCache();
_WINDLL_FUNC void SetLogcTidCache();
_WINDLL_FUNC unsigned long GetLogcTidCache();

#define LOGC_DATETIMECACHE_SIZE	64

struct DateTimeCache
{
	time_t			second_stamp ;
	char			date_and_time_str[ 10 + 1 + 8 + 2 ] ;
} ;

_WINDLL_FUNC void UpdateLogcDateTimeCacheFirst();
_WINDLL_FUNC void UpdateLogcDateTimeCache();

#if 0
#define UPDATEDATETIMECACHEFIRST	UpdateLogcDateTimeCacheFirst()
#define UPDATEDATETIMECACHE		UpdateLogcDateTimeCache()
#define GETSECONDSTAMP			GetSecondStamp()
#define GETDATETIMESTR			GetDateTimeStr()
#endif

_WINDLL_FUNC time_t GetLogcSecondStampCache();
_WINDLL_FUNC char *GetLogcDateTimeStrCache();

#if ( defined __STDC_VERSION__ ) && ( __STDC_VERSION__ >= 199901 )

_WINDLL_FUNC int WriteLogcBaseV( int log_level , char *c_filename , long c_fileline , char *format , va_list valist );
_WINDLL_FUNC int WriteLogcBase( int log_level , char *c_filename , long c_fileline , char *format , ... );

#define WriteLogc(_log_level_,_c_filename_,_c_fileline_,...) \
	if( log_level >= GetLogcLevel() ) \
		WriteLogcBase( _log_level_ , _c_filename_ , _c_fileline_ , __VA_ARGS__ );

#define FatalLogc(_c_filename_,_c_fileline_,...) \
	if( LOGCLEVEL_FATAL >= GetLogcLevel() ) \
		WriteLogcBase( LOGCLEVEL_FATAL , _c_filename_ , _c_fileline_ , __VA_ARGS__ );

#define ErrorLogc(_c_filename_,_c_fileline_,...) \
	if( LOGCLEVEL_ERROR >= GetLogcLevel() ) \
		WriteLogcBase( LOGCLEVEL_ERROR , _c_filename_ , _c_fileline_ , __VA_ARGS__ );

#define WarnLogc(_c_filename_,_c_fileline_,...) \
	if( LOGCLEVEL_WARN >= GetLogcLevel() ) \
		WriteLogcBase( LOGCLEVEL_WARN , _c_filename_ , _c_fileline_ , __VA_ARGS__ );

#define InfoLogc(_c_filename_,_c_fileline_,...) \
	if( LOGCLEVEL_INFO >= GetLogcLevel() ) \
		WriteLogcBase( LOGCLEVEL_INFO , _c_filename_ , _c_fileline_ , __VA_ARGS__ );

#define DebugLogc(_c_filename_,_c_fileline_,...) \
	if( LOGCLEVEL_DEBUG >= GetLogcLevel() ) \
		WriteLogcBase( LOGCLEVEL_DEBUG , _c_filename_ , _c_fileline_ , __VA_ARGS__ );

_WINDLL_FUNC int WriteHexLogcBaseV( int log_level , char *c_filename , long c_fileline , char *buf , long buflen , char *format , va_list valist );
_WINDLL_FUNC int WriteHexLogcBase( int log_level , char *c_filename , long c_fileline , char *buf , long buflen , char *format , ... );

#define WriteHexLogc(_log_level_,_c_filename_,_c_fileline_,_buf_,_buflen_,...) \
	if( log_level >= GetLogcLevel() ) \
		WriteHexLogcBase( _log_level_ , _c_filename_ , _c_fileline_ , _buf_ , _buflen_ , __VA_ARGS__ );

#define FatalHexLogc(_c_filename_,_c_fileline_,_buf_,_buflen_,...) \
	if( LOGCLEVEL_FATAL >= GetLogcLevel() ) \
		WriteHexLogcBase( LOGCLEVEL_FATAL , _c_filename_ , _c_fileline_ , _buf_ , _buflen_ , __VA_ARGS__ );

#define ErrorHexLogc(_c_filename_,_c_fileline_,_buf_,_buflen_,...) \
	if( LOGCLEVEL_ERROR >= GetLogcLevel() ) \
		WriteHexLogcBase( LOGCLEVEL_ERROR , _c_filename_ , _c_fileline_ , _buf_ , _buflen_ , __VA_ARGS__ );

#define WarnHexLogc(_c_filename_,_c_fileline_,_buf_,_buflen_,...) \
	if( LOGCLEVEL_WARN >= GetLogcLevel() ) \
		WriteHexLogcBase( LOGCLEVEL_WARN , _c_filename_ , _c_fileline_ , _buf_ , _buflen_ , __VA_ARGS__ );

#define InfoHexLogc(_c_filename_,_c_fileline_,_buf_,_buflen_,...) \
	if( LOGCLEVEL_INFO >= GetLogcLevel() ) \
		WriteHexLogcBase( LOGCLEVEL_INFO , _c_filename_ , _c_fileline_ , _buf_ , _buflen_ , __VA_ARGS__ );

#define DebugHexLogc(_c_filename_,_c_fileline_,_buf_,_buflen_,...) \
	if( LOGCLEVEL_DEBUG >= GetLogcLevel() ) \
		WriteHexLogcBase( LOGCLEVEL_DEBUG , _c_filename_ , _c_fileline_ , _buf_ , _buflen_ , __VA_ARGS__ );

#else

_WINDLL_FUNC int WriteLogc( int log_level , char *c_filename , long c_fileline , char *format , ... );
_WINDLL_FUNC int FatalLogc( char *c_filename , long c_fileline , char *format , ... );
_WINDLL_FUNC int ErrorLogc( char *c_filename , long c_fileline , char *format , ... );
_WINDLL_FUNC int WarnLogc( char *c_filename , long c_fileline , char *format , ... );
_WINDLL_FUNC int InfoLogc( char *c_filename , long c_fileline , char *format , ... );
_WINDLL_FUNC int DebugLogc( char *c_filename , long c_fileline , char *format , ... );

_WINDLL_FUNC int WriteHexLogc( int log_level , char *c_filename , long c_fileline , char *buf , long buflen , char *format , ... );
_WINDLL_FUNC int FatalHexLogc( char *c_filename , long c_fileline , char *buf , long buflen , char *format , ... );
_WINDLL_FUNC int ErrorHexLogc( char *c_filename , long c_fileline , char *buf , long buflen , char *format , ... );
_WINDLL_FUNC int WarnHexLogc( char *c_filename , long c_fileline , char *buf , long buflen , char *format , ... );
_WINDLL_FUNC int InfoHexLogc( char *c_filename , long c_fileline , char *buf , long buflen , char *format , ... );
_WINDLL_FUNC int DebugHexLogc( char *c_filename , long c_fileline , char *buf , long buflen , char *format , ... );

#endif

#if ( defined __STDC_VERSION__ ) && ( __STDC_VERSION__ >= 199901 )

#define WRITELOGC(_log_level_,...)	WriteLogc( _log_level_ , __FILE__ , __LINE__ , __VA_ARGS__ );
#define FATALLOGC(...)			FatalLogc( __FILE__ , __LINE__ , __VA_ARGS__ );
#define ERRORLOGC(...)			ErrorLogc( __FILE__ , __LINE__ , __VA_ARGS__ );
#define WARNLOGC(...)			WarnLogc( __FILE__ , __LINE__ , __VA_ARGS__ );
#define INFOLOGC(...)			InfoLogc( __FILE__ , __LINE__ , __VA_ARGS__ );
#define DEBUGLOGC(...)			DebugLogc( __FILE__ , __LINE__ , __VA_ARGS__ );

#define WRITEHEXLOGC(_log_level_,_buf_,_buflen_,...)	WriteHexLogc( _log_level_ , __FILE__ , __LINE__ , buf , buflen , __VA_ARGS__ );
#define FATALHEXLOGC(_buf_,_buflen_,...)	FatalHexLogc( __FILE__ , __LINE__ , buf , buflen , __VA_ARGS__ );
#define ERRORHEXLOGC(_buf_,_buflen_,...)	ErrorHexLogc( __FILE__ , __LINE__ , buf , buflen , __VA_ARGS__ );
#define WARNHEXLOGC(_buf_,_buflen_,...)	WarnHexLogc( __FILE__ , __LINE__ , buf , buflen , __VA_ARGS__ );
#define INFOHEXLOGC(_buf_,_buflen_,...)	InfoHexLogc( __FILE__ , __LINE__ , buf , buflen , __VA_ARGS__ );
#define DEBUGHEXLOGC(_buf_,_buflen_,...)	DebugHexLogc( __FILE__ , __LINE__ , buf , buflen , __VA_ARGS__ );

#endif

#if ( defined __STDC_VERSION__ ) && ( __STDC_VERSION__ >= 199901 )

#define set_logc_file		SetLogcFile
#define set_log_level		SetLogcLevel

#define write_logc		WriteLogc
#define fatal_logc		FatalLogc
#define error_logc		ErrorLogc
#define warn_logc		WarnLogc
#define info_logc		InfoLogc
#define debug_logc		DebugLogc

#define write_hex_logc		WriteHexLogc
#define fatal_hex_logc		FatalHexLogc
#define error_hex_logc		ErrorHexLogc
#define warn_hex_logc		WarnHexLogc
#define info_hex_logc		InfoHexLogc
#define debug_hex_logc		DebugHexLogc

#endif

#ifdef __cplusplus
}
#endif

#endif
