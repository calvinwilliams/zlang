#ifndef _H_HETAOAPI_
#define _H_HETAOAPI_

#include "IDL_hetao_conf.dsc.h"

#include "hetao_socgi.h"

int RunHttpServer( hetao_conf *conf , funcCallHttpApplication *pfuncCallHttpApplication , void *user_data );

char *GetHetaoApiError();

#endif

