/* Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "zobjects_network.h"

struct ZlangDirectProperty_ip
{
	struct in_addr			ip_addr ;
} ;

static struct ZlangDirectFunctions direct_funcs_ip;

ZlangInvokeFunction ZlangInvokeFunction_ip_SetFromString_string;
int ZlangInvokeFunction_ip_SetFromString_string( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_ip	*ip_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject		*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject		*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	char				*ip_str = NULL ;
	int				nret = 0 ;
	
	CallRuntimeFunction_string_GetStringValue( rt , in1 , & ip_str , NULL );
	
	nret = inet_aton( ip_str , & (ip_direct_prop->ip_addr) ) ;
	if( nret != 1 )
	{
		CallRuntimeFunction_bool_SetBoolValue( rt , out1 , FALSE );
		return ThrowFatalException( rt , ZLANG_FATAL_INVOKE_METHOD_RETURN , EXCEPTION_MESSAGE_INET_ATON_FAILED );
	}
	
	CallRuntimeFunction_bool_SetBoolValue( rt , out1 , TRUE );
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_ip_SetFromIntegerInNetworkOrder_int;
int ZlangInvokeFunction_ip_SetFromIntegerInNetworkOrder_int( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_ip	*ip_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject		*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	int32_t				ip_num ;
	
	CallRuntimeFunction_int_GetIntValue( rt , in1 , & ip_num );
	
	ip_direct_prop->ip_addr.s_addr = ip_num ;
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_ip_SetFromIntegerInHostOrder_int;
int ZlangInvokeFunction_ip_SetFromIntegerInHostOrder_int( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_ip	*ip_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject		*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	int32_t				ip_num ;
	
	CallRuntimeFunction_int_GetIntValue( rt , in1 , & ip_num );
	
	ip_direct_prop->ip_addr.s_addr = HTON32( ip_num ) ;
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_ip_ToIntegerInNetworkOrder;
int ZlangInvokeFunction_ip_ToIntegerInNetworkOrder( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_ip	*ip_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject		*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	int32_t				ip_num ;
	
	ip_num = ip_direct_prop->ip_addr.s_addr ;
	CallRuntimeFunction_int_SetIntValue( rt , out1 , ip_num );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_ip_ToIntegerInHostOrder;
int ZlangInvokeFunction_ip_ToIntegerInHostOrder( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_ip	*ip_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject		*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	int32_t				ip_num ;
	
	ip_num = NTOH32( ip_direct_prop->ip_addr.s_addr ) ;
	CallRuntimeFunction_int_SetIntValue( rt , out1 , ip_num );
	
	return 0;
}

ZlangCreateDirectPropertyFunction ZlangCreateDirectProperty_ip;
void *ZlangCreateDirectProperty_ip( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_ip	*ip_direct_prop = NULL ;
	
	ip_direct_prop = (struct ZlangDirectProperty_ip *)ZLMALLOC( sizeof(struct ZlangDirectProperty_ip) ) ;
	if( ip_direct_prop == NULL )
	{
		SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_ALLOC , "alloc memory for entity" )
		return NULL;
	}
	memset( ip_direct_prop , 0x00 , sizeof(struct ZlangDirectProperty_ip) );
	
	return ip_direct_prop;
}

ZlangDestroyDirectPropertyFunction ZlangDestroyDirectProperty_ip;
void ZlangDestroyDirectProperty_ip( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_ip	*ip_direct_prop = GetObjectDirectProperty(obj) ;
	
	ZLFREE( ip_direct_prop );
	
	return;
}

ZlangToStringFunction ZlangToString_ip;
int ZlangToString_ip( struct ZlangRuntime *rt , struct ZlangObject *obj , struct ZlangObject **tostr_obj )
{
	struct ZlangDirectProperty_ip	*ip_direct_prop = GetObjectDirectProperty(obj) ;
	char				buf[ 15 + 1 ] ;
	const char			*p ;
	
	memset( buf , 0x00 , sizeof(buf) );
	p = inet_ntop( AF_INET , (const void *) & (ip_direct_prop->ip_addr) , buf , sizeof(buf) ) ;
	if( p == NULL )
	{
		UnreferObject( rt , (*tostr_obj) );
		return 0;
	}
	
	CallRuntimeFunction_string_SetStringValue( rt , (*tostr_obj) , buf , (int32_t)strlen(buf) );
	
	return 0;
}

ZlangSummarizeDirectPropertySizeFunction ZlangSummarizeDirectPropertySize_ip;
void ZlangSummarizeDirectPropertySize_ip( struct ZlangRuntime *rt , struct ZlangObject *obj , size_t *summarized_obj_size , size_t *summarized_direct_prop_size )
{
	SUMMARIZE_SIZE( summarized_direct_prop_size , sizeof(struct ZlangDirectProperty_ip) )
	return;
}

static struct ZlangDirectFunctions direct_funcs_ip =
	{
		ZLANG_OBJECT_ip , /* char *ancestor_name */
		
		ZlangCreateDirectProperty_ip , /* ZlangCreateDirectPropertyFunction *create_entity_func */
		ZlangDestroyDirectProperty_ip , /* ZlangDestroyDirectPropertyFunction *destroy_entity_func */
		
		NULL , /* ZlangFromCharPtrFunction *from_char_ptr_func */
		ZlangToString_ip , /* ZlangToStringFunction *to_string_func */
		NULL , /* ZlangFromDataPtrFunction *from_data_ptr_func */
		NULL , /* ZlangGetDataPtrFunction *get_data_ptr_func */
		
		NULL , /* ZlangOperatorFunction *oper_PLUS_func */
		NULL , /* ZlangOperatorFunction *oper_MINUS_func */
		NULL , /* ZlangOperatorFunction *oper_MUL_func */
		NULL , /* ZlangOperatorFunction *oper_DIV_func */
		NULL , /* ZlangOperatorFunction *oper_MOD_func */
		
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_NEGATIVE_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_NOT_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_BIT_REVERSE_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_PLUS_PLUS_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_MINUS_MINUS_func */
		
		NULL , /* ZlangCompareFunction *comp_EGUAL_func */
		NULL , /* ZlangCompareFunction *comp_NOTEGUAL_func */
		NULL , /* ZlangCompareFunction *comp_LT_func */
		NULL , /* ZlangCompareFunction *comp_LE_func */
		NULL , /* ZlangCompareFunction *comp_GT_func */
		NULL , /* ZlangCompareFunction *comp_GE_func */
		
		NULL , /* ZlangLogicFunction *logic_AND_func */
		NULL , /* ZlangLogicFunction *logic_OR_func */
		
		NULL , /* ZlangLogicFunction *bit_AND_func */
		NULL , /* ZlangLogicFunction *bit_XOR_func */
		NULL , /* ZlangLogicFunction *bit_OR_func */
		NULL , /* ZlangLogicFunction *bit_MOVELEFT_func */
		NULL , /* ZlangLogicFunction *bit_MOVERIGHT_func */
		
		ZlangSummarizeDirectPropertySize_ip , /* ZlangSummarizeDirectPropertySizeFunction *summarize_direct_prop_size_func */
	} ;

ZlangImportObjectFunction ZlangImportObject_ip;
struct ZlangObject *ZlangImportObject_ip( struct ZlangRuntime *rt )
{
	struct ZlangObject	*obj = NULL ;
	struct ZlangFunction	*func = NULL ;
	int			nret = 0 ;
	
	nret = ImportObject( rt , & obj , ZLANG_OBJECT_ip , & direct_funcs_ip , sizeof(struct ZlangDirectFunctions) , NULL ) ;
	if( nret )
	{
		SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_LINK_FUNC_TO_ENTITY , "import object to global objects heap" )
		return NULL;
	}
	
	/* ip.SetFromString(string) */
	func = AddFunctionAndParametersInObject( rt , obj , "SetFromString" , "SetFromString(string)" , ZlangInvokeFunction_ip_SetFromString_string , ZLANG_OBJECT_bool , ZLANG_OBJECT_string,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* ip.SetFromIntegerNetworkOrder(int) */
	func = AddFunctionAndParametersInObject( rt , obj , "SetFromIntegerInNetworkOrder" , "SetFromIntegerInNetworkOrder(int)" , ZlangInvokeFunction_ip_SetFromIntegerInNetworkOrder_int , ZLANG_OBJECT_void , ZLANG_OBJECT_int,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* ip.SetFromIntegerInHostOrder(int) */
	func = AddFunctionAndParametersInObject( rt , obj , "SetFromIntegerInHostOrder" , "SetFromIntegerInHostOrder(int)" , ZlangInvokeFunction_ip_SetFromIntegerInHostOrder_int , ZLANG_OBJECT_void , ZLANG_OBJECT_int,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* ip.ToIntegerInNetworkOrder() */
	func = AddFunctionAndParametersInObject( rt , obj , "ToIntegerInNetworkOrder" , "ToIntegerInNetworkOrder()" , ZlangInvokeFunction_ip_ToIntegerInNetworkOrder , ZLANG_OBJECT_int , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* ip.ToIntegerInHostOrder() */
	func = AddFunctionAndParametersInObject( rt , obj , "ToIntegerInHostOrder" , "ToIntegerInHostOrder()" , ZlangInvokeFunction_ip_ToIntegerInHostOrder , ZLANG_OBJECT_int , NULL ) ;
	if( func == NULL )
		return NULL;
	
	return obj ;
}

