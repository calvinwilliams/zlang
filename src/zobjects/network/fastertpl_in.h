#ifndef _H_FASTERTPL_IN_
#define _H_FASTERTPL_IN_

#ifdef __cplusplus
extern "C" {
#endif

enum FasterTemplatePieceType
{
	FASTERTEMPLATEPIECETYPE_TEXT = 1 ,
	FASTERTEMPLATEPIECETYPE_VAR = 2 ,
	FASTERTEMPLATEPIECETYPE_SECTION = 6 ,
	FASTERTEMPLATEPIECETYPE_DETAIL = 7
} ;

struct FasterTempatePiece
{
	enum FasterTemplatePieceType	type ;
	union
	{
		struct
		{
			char		*p_text ;
			int		text_len ;
		} text_data ;
		struct
		{
			char		*p_var_name ;
			int		var_name_len ;
			char		*p_format ;
			int		format_len ;
		} var_data ;
		struct
		{
			char		*p_section_name ;
			int		section_name_len ;
		} section_data ;
		struct
		{
			char		*p_detail_name ;
			int		detail_name_len ;
		} detail_data ;
	} data ;
	struct FasterTempatePiece	*forward_piece ;
	struct FasterTempatePiece	*backward_piece ;
	
	struct list_head		tpl_piece_list_node ;
} ;

struct FasterTempate
{
	int				file_length ;
	char				*file_content ;
	
	struct list_head		tpl_piece_list ; /* struct FasterTempatePiece */
	
	char				*instance_buffer ;
	int				instance_buffer_size ;
} ;

enum FasterDataType
{
	FASTERDATATYPE_SHORT = 101 ,
	FASTERDATATYPE_INT = 102 ,
	FASTERDATATYPE_LONG = 103 ,
	FASTERDATATYPE_FLOAT = 201 ,
	FASTERDATATYPE_DOUBLE = 202 ,
	FASTERDATATYPE_CHAR = 301 ,
	FASTERDATATYPE_STRING = 401
} ;

struct FasterData
{
	char			*name ;
	enum FasterDataType	type ;
	union
	{
		short		s ;
		int		i ;
		long		l ;
		float		f ;
		double		d ;
		char		ch ;
		char		*str ;
	} data ;
	
	struct rb_node		data_tree_node ;
} ;

struct FasterData *LinkFasterDataToTreeByName( struct FasterSection *section , struct FasterData *data );
struct FasterData *QueryFasterDataFromTreeByName( struct FasterSection *section , struct FasterData *data );
void UnlinkFasterDataFromTreeNode( struct FasterSection *section , struct FasterData *data );
struct FasterData *TravelFasterDatasTree( struct FasterSection *section , struct FasterData *data );
void DestroyFasterDatasTree( struct FasterSection *section );

struct FasterDetail
{
	char			*detail_name ;
	
	struct list_head	section_list ; /* struct FasterSection */
	
	struct rb_node		detail_tree_node ; /* struct FasterDetail */
} ;

int LinkFasterDetailToTreeByDetailName( struct FasterSection *section , struct FasterDetail *detail );
struct FasterDetail *QueryFasterDetailFromTreeByDetailName( struct FasterSection *section , struct FasterDetail *detail );
void UnlinkFasterDetailFromTreeNode( struct FasterSection *section , struct FasterDetail *detail );
struct FasterDetail *TravelFasterDetailsTree( struct FasterSection *section , struct FasterDetail *detail );
void DestroyFasterDetailsTree( struct FasterSection *section );

struct FasterSection
{
	char			*section_name ;
	
	struct rb_root		datas_tree ; /* struct FasterData */
	
	struct rb_root		sub_section_tree ; /* struct FasterSection */
	struct rb_node		section_tree_node ; /* struct FasterSection */
	
	struct rb_root		details_tree ; /* struct FasterDetail */
	struct list_head	section_list_node ; /* struct FasterDetail */
} ;

int LinkFasterSectionToTreeBySectionName( struct FasterSection *parent_section , struct FasterSection *sub_section );
struct FasterSection *QueryFasterSectionFromTreeBySectionName( struct FasterSection *parent_section , struct FasterSection *sub_section );
void UnlinkFasterSectionFromTreeNode( struct FasterSection *parent_section , struct FasterSection *sub_section );
struct FasterSection *TravelFasterSectionTree( struct FasterSection *parent_section , struct FasterSection *sub_section );
void DestroyFasterSectionTree( struct FasterSection *parent_section );

struct FasterInstancePiece
{
	char			*str_piece ;
	int			str_piece_len ;
	
	struct list_head	instance_piece_list_node ; /* struct FasterInstancePiece */
} ;

#ifdef __cplusplus
extern }
#endif

#endif
