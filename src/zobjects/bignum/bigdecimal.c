#include "zobjects_bignum.h"

#define ZLANG_OBJECT_bigdecimal		"bigdecimal"

struct ZlangDirectProperty_bigdecimal
{
	mpf_t		value ;
} ;

static int Mpf2StringObject( struct ZlangRuntime *rt , mpf_t value , size_t n_digits , struct ZlangObject *str_obj )
{
	char					*dump_str = NULL ;
	int32_t					dump_str_len ;
	int					nret = 0 ;
	
	dump_str_len = gmp_asprintf( & dump_str , "%.*Ff" , n_digits , value ) ;
	if( dump_str_len < 0 )
		return -1;
	
	nret = CallRuntimeFunction_string_SetStringValue( rt , str_obj , dump_str , dump_str_len ) ;
	if( nret )
		return -2;
	
	free( dump_str );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_bigdecimal_Set_double;
int ZlangInvokeFunction_bigdecimal_Set_double( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_bigdecimal	*bigdecimal_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	double					d ;
	
	CallRuntimeFunction_double_GetDoubleValue( rt , in1 , & d );
	mpf_set_d( bigdecimal_direct_prop->value , d );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_bigdecimal_Set_string;
int ZlangInvokeFunction_bigdecimal_Set_string( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_bigdecimal	*bigdecimal_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	char					*str = NULL ;
	int32_t					str_len ;
	
	CallRuntimeFunction_string_GetStringValue( rt , in1 , & str , & str_len );
	mpf_set_str( bigdecimal_direct_prop->value , str , 10 );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_bigdecimal_Set_string_int;
int ZlangInvokeFunction_bigdecimal_Set_string_int( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_bigdecimal	*bigdecimal_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject			*in2 = GetInputParameterInLocalObjectStack(rt,2) ;
	char					*str = NULL ;
	int32_t					str_len ;
	int32_t					base ;
	
	CallRuntimeFunction_string_GetStringValue( rt , in1 , & str , & str_len );
	CallRuntimeFunction_int_GetIntValue( rt , in2 , & base );
	mpf_set_str( bigdecimal_direct_prop->value , str , (int)base );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_bigdecimal_ToString_int;
int ZlangInvokeFunction_bigdecimal_ToString_int( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_bigdecimal	*bigdecimal_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject			*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	int32_t					n_digits ;
	int					nret = 0 ;
	
	CallRuntimeFunction_int_GetIntValue( rt , in1 , & n_digits );
	
	nret = Mpf2StringObject( rt , bigdecimal_direct_prop->value , (size_t)n_digits , out1 );
	if( nret )
		return nret;
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_bigdecimal_Swap_bigdecimal_bigdecimal;
int ZlangInvokeFunction_bigdecimal_Swap_bigdecimal_bigdecimal( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangObject			*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangDirectProperty_bigdecimal	*in1_bigdecimal_direct_prop = GetObjectDirectProperty(in1) ;
	struct ZlangObject			*in2 = GetInputParameterInLocalObjectStack(rt,2) ;
	struct ZlangDirectProperty_bigdecimal	*in2_bigdecimal_direct_prop = GetObjectDirectProperty(in2) ;
	
	mpf_swap( in1_bigdecimal_direct_prop->value , in2_bigdecimal_direct_prop->value );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_bigdecimal_Abs;
int ZlangInvokeFunction_bigdecimal_Abs( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_bigdecimal	*bigdecimal_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	struct ZlangDirectProperty_bigdecimal	*out1_bigdecimal_direct_prop = GetObjectDirectProperty(out1) ;
	
	mpf_abs( out1_bigdecimal_direct_prop->value , bigdecimal_direct_prop->value );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_bigdecimal_Pow_int;
int ZlangInvokeFunction_bigdecimal_Pow_int( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_bigdecimal	*bigdecimal_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject			*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	struct ZlangDirectProperty_bigdecimal	*out1_bigdecimal_direct_prop = GetObjectDirectProperty(out1) ;
	int32_t					exp ;
	
	CallRuntimeFunction_int_GetIntValue( rt , in1 , & exp );
	
	mpf_pow_ui( out1_bigdecimal_direct_prop->value , bigdecimal_direct_prop->value , (unsigned long)exp );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_bigdecimal_Sqrt;
int ZlangInvokeFunction_bigdecimal_Sqrt( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_bigdecimal	*bigdecimal_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	struct ZlangDirectProperty_bigdecimal	*out1_bigdecimal_direct_prop = GetObjectDirectProperty(out1) ;
	
	mpf_sqrt( out1_bigdecimal_direct_prop->value , bigdecimal_direct_prop->value );
	
	return 0;
}

ZlangCreateDirectPropertyFunction ZlangCreateDirectProperty_bigdecimal;
void *ZlangCreateDirectProperty_bigdecimal( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_bigdecimal	*bigdecimal_prop = NULL ;
	
	bigdecimal_prop = (struct ZlangDirectProperty_bigdecimal *)malloc( sizeof(struct ZlangDirectProperty_bigdecimal) ) ;
	if( bigdecimal_prop == NULL )
	{
		SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_ALLOC , "alloc memory for entity" )
		return NULL;
	}
	memset( bigdecimal_prop , 0x00 , sizeof(struct ZlangDirectProperty_bigdecimal) );
	
	mpf_init( bigdecimal_prop->value );
	
	return bigdecimal_prop;
}

ZlangDestroyDirectPropertyFunction ZlangDestroyDirectProperty_bigdecimal;
void ZlangDestroyDirectProperty_bigdecimal( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_bigdecimal	*bigdecimal_direct_prop = GetObjectDirectProperty(obj) ;
	
	if( bigdecimal_direct_prop )
	{
		mpf_clear( bigdecimal_direct_prop->value );
		
		free( bigdecimal_direct_prop );
	}
	
	return;
}

ZlangToStringFunction ZlangToString_bigdecimal;
int ZlangToString_bigdecimal( struct ZlangRuntime *rt , struct ZlangObject *obj , struct ZlangObject **tostr_obj )
{
	struct ZlangDirectProperty_bigdecimal	*bigdecimal_direct_prop = GetObjectDirectProperty(obj) ;
	int					nret = 0 ;
	
	nret = Mpf2StringObject( rt , bigdecimal_direct_prop->value , (size_t)6 , (*tostr_obj) );
	if( nret )
		return nret;
	
	return 0;
}

ZlangFromDataPtrFunction ZlangFromDataPtr_bigdecimal;
int ZlangFromDataPtr_bigdecimal( struct ZlangRuntime *rt , struct ZlangObject *obj , void *value_ptr , int32_t value_len )
{
	struct ZlangDirectProperty_bigdecimal	*obj_direct_prop = GetObjectDirectProperty(obj) ;
	
	mpf_set( obj_direct_prop->value , *(mpf_t *)value_ptr );
	
	return 0;
}

ZlangGetDataPtrFunction ZlangGetDataPtr_bigdecimal;
int ZlangGetDataPtr_bigdecimal( struct ZlangRuntime *rt , struct ZlangObject *obj , void **value_ptr , int32_t *value_len )
{
	struct ZlangDirectProperty_bigdecimal	*obj_direct_prop = GetObjectDirectProperty(obj) ;
	
	if( value_ptr )
		(*value_ptr) = & (obj_direct_prop->value) ;
	if( value_len )
		(*value_len) = sizeof(obj_direct_prop->value) ;
	return 0;
}

ZlangOperatorFunction ZlangOperator_bigdecimal_PLUS_bigdecimal;
int ZlangOperator_bigdecimal_PLUS_bigdecimal( struct ZlangRuntime *rt , struct ZlangObject *in_obj1 , struct ZlangObject *in_obj2 , struct ZlangObject **out_obj )
{
	struct ZlangDirectProperty_bigdecimal	*in1 = GetObjectDirectProperty(in_obj1) ;
	struct ZlangDirectProperty_bigdecimal	*in2 = GetObjectDirectProperty(in_obj2) ;
	struct ZlangDirectProperty_bigdecimal	*out = NULL ;
	
	out = GetObjectDirectProperty(*out_obj) ;
	mpf_add( out->value , in1->value , in2->value );
	
	return 0;
}

ZlangOperatorFunction ZlangOperator_bigdecimal_MINUS_bigdecimal;
int ZlangOperator_bigdecimal_MINUS_bigdecimal( struct ZlangRuntime *rt , struct ZlangObject *in_obj1 , struct ZlangObject *in_obj2 , struct ZlangObject **out_obj )
{
	struct ZlangDirectProperty_bigdecimal	*in1 = GetObjectDirectProperty(in_obj1) ;
	struct ZlangDirectProperty_bigdecimal	*in2 = GetObjectDirectProperty(in_obj2) ;
	struct ZlangDirectProperty_bigdecimal	*out = NULL ;
	
	out = GetObjectDirectProperty(*out_obj) ;
	mpf_sub( out->value , in1->value , in2->value );
	
	return 0;
}

ZlangOperatorFunction ZlangOperator_bigdecimal_MUL_bigdecimal;
int ZlangOperator_bigdecimal_MUL_bigdecimal( struct ZlangRuntime *rt , struct ZlangObject *in_obj1 , struct ZlangObject *in_obj2 , struct ZlangObject **out_obj )
{
	struct ZlangDirectProperty_bigdecimal	*in1 = GetObjectDirectProperty(in_obj1) ;
	struct ZlangDirectProperty_bigdecimal	*in2 = GetObjectDirectProperty(in_obj2) ;
	struct ZlangDirectProperty_bigdecimal	*out = NULL ;
	
	out = GetObjectDirectProperty(*out_obj) ;
	mpf_mul( out->value , in1->value , in2->value );
	
	return 0;
}

ZlangOperatorFunction ZlangOperator_bigdecimal_DIV_bigdecimal;
int ZlangOperator_bigdecimal_DIV_bigdecimal( struct ZlangRuntime *rt , struct ZlangObject *in_obj1 , struct ZlangObject *in_obj2 , struct ZlangObject **out_obj )
{
	struct ZlangDirectProperty_bigdecimal	*in1 = GetObjectDirectProperty(in_obj1) ;
	struct ZlangDirectProperty_bigdecimal	*in2 = GetObjectDirectProperty(in_obj2) ;
	struct ZlangDirectProperty_bigdecimal	*out = NULL ;
	
	if( mpf_get_ui(in2->value) == 0 )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "*** FATAL : division by zero" )
		return ThrowFatalException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_DIVISION_BY_ZERO );
	}
	
	out = GetObjectDirectProperty(*out_obj) ;
	mpf_div( out->value , in1->value , in2->value );
	
	return 0;
}

ZlangOperatorFunction ZlangOperator_bigdecimal_MOD_bigdecimal;
int ZlangOperator_bigdecimal_MOD_bigdecimal( struct ZlangRuntime *rt , struct ZlangObject *in_obj1 , struct ZlangObject *in_obj2 , struct ZlangObject **out_obj )
{
	struct ZlangDirectProperty_bigdecimal	*in1 = GetObjectDirectProperty(in_obj1) ;
	struct ZlangDirectProperty_bigdecimal	*in2 = GetObjectDirectProperty(in_obj2) ;
	struct ZlangDirectProperty_bigdecimal	*out = NULL ;
	mpf_t					q , p ;
	
	if( fabs(mpf_get_d(in2->value)) <= DOUBLE_EPSILON )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "*** FATAL : division by zero" )
		return ThrowFatalException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_DIVISION_BY_ZERO );
	}
	
	out = GetObjectDirectProperty(*out_obj) ;
	mpf_init( q );
	mpf_init( p );
	mpf_div( q , in1->value , in2->value );
	mpf_mul( p , q , in2->value );
	mpf_sub( out->value , in1->value , p );
	mpf_clear( q );
	mpf_clear( p );
	
	return 0;
}

ZlangUnaryOperatorFunction ZlangUnaryOperator_NEGATIVE_bigdecimal;
int ZlangUnaryOperator_NEGATIVE_bigdecimal( struct ZlangRuntime *rt , struct ZlangObject *in_out_obj1 )
{
	struct ZlangDirectProperty_bigdecimal	*in_out = GetObjectDirectProperty(in_out_obj1) ;
	
	mpf_neg( in_out->value , in_out->value );
	
	return 0;
} 

ZlangUnaryOperatorFunction ZlangUnaryOperator_NOT_bigdecimal;
int ZlangUnaryOperator_NOT_bigdecimal( struct ZlangRuntime *rt , struct ZlangObject *in_out_obj1 )
{
	struct ZlangDirectProperty_bigdecimal	*in_out = GetObjectDirectProperty(in_out_obj1) ;
	
	if( mpf_cmp_si( in_out->value , 0 ) )
		mpf_set_si( in_out->value , (signed long)1 );
	else
		mpf_set_si( in_out->value , (signed long)0 );
	
	return 0;
}

ZlangUnaryOperatorFunction ZlangUnaryOperator_PLUS_PLUS_bigdecimal;
int ZlangUnaryOperator_PLUS_PLUS_bigdecimal( struct ZlangRuntime *rt , struct ZlangObject *in_out_obj1 )
{
	struct ZlangDirectProperty_bigdecimal	*in_out = GetObjectDirectProperty(in_out_obj1) ;
	
	mpf_add_ui( in_out->value , in_out->value , (unsigned long)1 );
	
	return 0;
}

ZlangUnaryOperatorFunction ZlangUnaryOperator_MINUS_MINUS_bigdecimal;
int ZlangUnaryOperator_MINUS_MINUS_bigdecimal( struct ZlangRuntime *rt , struct ZlangObject *in_out_obj1 )
{
	struct ZlangDirectProperty_bigdecimal	*in_out = GetObjectDirectProperty(in_out_obj1) ;
	
	mpf_sub_ui( in_out->value , in_out->value , (unsigned long)1 );
	
	return 0;
}

ZlangCompareFunction ZlangCompare_bigdecimal_EGUAL_bigdecimal;
int ZlangCompare_bigdecimal_EGUAL_bigdecimal( struct ZlangRuntime *rt , struct ZlangObject *in_obj1 , struct ZlangObject *in_obj2 , struct ZlangObject *out_obj )
{
	struct ZlangDirectProperty_bigdecimal	*in1 = GetObjectDirectProperty(in_obj1) ;
	struct ZlangDirectProperty_bigdecimal	*in2 = GetObjectDirectProperty(in_obj2) ;
	
	if( mpf_cmp( in1->value , in2->value ) == 0 )
	{
		CallRuntimeFunction_bool_SetBoolValue( rt , out_obj , TRUE );
	}
	else
	{
		CallRuntimeFunction_bool_SetBoolValue( rt , out_obj , FALSE );
	}
	
	return 0;
}

ZlangCompareFunction ZlangCompare_bigdecimal_NOTEGUAL_bigdecimal;
int ZlangCompare_bigdecimal_NOTEGUAL_bigdecimal( struct ZlangRuntime *rt , struct ZlangObject *in_obj1 , struct ZlangObject *in_obj2 , struct ZlangObject *out_obj )
{
	struct ZlangDirectProperty_bigdecimal	*in1 = GetObjectDirectProperty(in_obj1) ;
	struct ZlangDirectProperty_bigdecimal	*in2 = GetObjectDirectProperty(in_obj2) ;
	
	if( mpf_cmp( in1->value , in2->value ) != 0 )
	{
		CallRuntimeFunction_bool_SetBoolValue( rt , out_obj , TRUE );
	}
	else
	{
		CallRuntimeFunction_bool_SetBoolValue( rt , out_obj , FALSE );
	}
	
	return 0;
}

ZlangCompareFunction ZlangCompare_bigdecimal_LT_bigdecimal;
int ZlangCompare_bigdecimal_LT_bigdecimal( struct ZlangRuntime *rt , struct ZlangObject *in_obj1 , struct ZlangObject *in_obj2 , struct ZlangObject *out_obj )
{
	struct ZlangDirectProperty_bigdecimal	*in1 = GetObjectDirectProperty(in_obj1) ;
	struct ZlangDirectProperty_bigdecimal	*in2 = GetObjectDirectProperty(in_obj2) ;
	
	if( mpf_cmp( in1->value , in2->value ) < 0 )
	{
		CallRuntimeFunction_bool_SetBoolValue( rt , out_obj , TRUE );
	}
	else
	{
		CallRuntimeFunction_bool_SetBoolValue( rt , out_obj , FALSE );
	}
	
	return 0;
}

ZlangCompareFunction ZlangCompare_bigdecimal_LE_bigdecimal;
int ZlangCompare_bigdecimal_LE_bigdecimal( struct ZlangRuntime *rt , struct ZlangObject *in_obj1 , struct ZlangObject *in_obj2 , struct ZlangObject *out_obj )
{
	struct ZlangDirectProperty_bigdecimal	*in1 = GetObjectDirectProperty(in_obj1) ;
	struct ZlangDirectProperty_bigdecimal	*in2 = GetObjectDirectProperty(in_obj2) ;
	
	if( mpf_cmp( in1->value , in2->value ) <= 0 )
	{
		CallRuntimeFunction_bool_SetBoolValue( rt , out_obj , TRUE );
	}
	else
	{
		CallRuntimeFunction_bool_SetBoolValue( rt , out_obj , FALSE );
	}
	
	return 0;
}

ZlangCompareFunction ZlangCompare_bigdecimal_GT_bigdecimal;
int ZlangCompare_bigdecimal_GT_bigdecimal( struct ZlangRuntime *rt , struct ZlangObject *in_obj1 , struct ZlangObject *in_obj2 , struct ZlangObject *out_obj )
{
	struct ZlangDirectProperty_bigdecimal	*in1 = GetObjectDirectProperty(in_obj1) ;
	struct ZlangDirectProperty_bigdecimal	*in2 = GetObjectDirectProperty(in_obj2) ;
	
	if( mpf_cmp( in1->value , in2->value ) > 0 )
	{
		CallRuntimeFunction_bool_SetBoolValue( rt , out_obj , TRUE );
	}
	else
	{
		CallRuntimeFunction_bool_SetBoolValue( rt , out_obj , FALSE );
	}
	
	return 0;
}

ZlangCompareFunction ZlangCompare_bigdecimal_GE_bigdecimal;
int ZlangCompare_bigdecimal_GE_bigdecimal( struct ZlangRuntime *rt , struct ZlangObject *in_obj1 , struct ZlangObject *in_obj2 , struct ZlangObject *out_obj )
{
	struct ZlangDirectProperty_bigdecimal	*in1 = GetObjectDirectProperty(in_obj1) ;
	struct ZlangDirectProperty_bigdecimal	*in2 = GetObjectDirectProperty(in_obj2) ;
	
	if( mpf_cmp( in1->value , in2->value ) >= 0 )
	{
		CallRuntimeFunction_bool_SetBoolValue( rt , out_obj , TRUE );
	}
	else
	{
		CallRuntimeFunction_bool_SetBoolValue( rt , out_obj , FALSE );
	}
	
	return 0;
}

ZlangSummarizeDirectPropertySizeFunction ZlangSummarizeDirectPropertySize_bigdecimal;
void ZlangSummarizeDirectPropertySize_bigdecimal( struct ZlangRuntime *rt , struct ZlangObject *obj , size_t *summarized_obj_size , size_t *summarized_direct_prop_size )
{
	SUMMARIZE_SIZE( summarized_direct_prop_size , sizeof(struct ZlangDirectProperty_bigdecimal) )
	
	return;
}

static struct ZlangDirectFunctions direct_funcs_bigdecimal =
	{
		ZLANG_OBJECT_bigdecimal , /* char *ancestor_name */
		
		ZlangCreateDirectProperty_bigdecimal , /* ZlangCreateDirectPropertyFunction *create_entity_func */
		ZlangDestroyDirectProperty_bigdecimal , /* ZlangDestroyDirectPropertyFunction *destroy_entity_func */
		
		NULL , /* ZlangFromCharPtrFunction *from_char_ptr_func */
		ZlangToString_bigdecimal , /* ZlangToStringFunction *to_string_func */
		ZlangFromDataPtr_bigdecimal , /* ZlangFromDataPtrFunction *from_data_ptr_func */
		ZlangGetDataPtr_bigdecimal , /* ZlangGetDataPtrFunction *get_data_ptr_func */
		
		ZlangOperator_bigdecimal_PLUS_bigdecimal , /* ZlangOperatorFunction *oper_PLUS_func */
		ZlangOperator_bigdecimal_MINUS_bigdecimal , /* ZlangOperatorFunction *oper_MINUS_func */
		ZlangOperator_bigdecimal_MUL_bigdecimal , /* ZlangOperatorFunction *oper_MUL_func */
		ZlangOperator_bigdecimal_DIV_bigdecimal , /* ZlangOperatorFunction *oper_DIV_func */
		ZlangOperator_bigdecimal_MOD_bigdecimal , /* ZlangOperatorFunction *oper_MOD_func */
		
		ZlangUnaryOperator_NEGATIVE_bigdecimal , /* ZlangUnaryOperatorFunction *unaryoper_NEGATIVE_func */
		ZlangUnaryOperator_NOT_bigdecimal , /* ZlangUnaryOperatorFunction *unaryoper_NOT_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_BIT_REVERSE_func */
		ZlangUnaryOperator_PLUS_PLUS_bigdecimal , /* ZlangUnaryOperatorFunction *unaryoper_PLUS_PLUS_func */
		ZlangUnaryOperator_MINUS_MINUS_bigdecimal , /* ZlangUnaryOperatorFunction *unaryoper_MINUS_MINUS_func */
		
		ZlangCompare_bigdecimal_EGUAL_bigdecimal , /* ZlangCompareFunction *comp_EGUAL_func */
		ZlangCompare_bigdecimal_NOTEGUAL_bigdecimal , /* ZlangCompareFunction *comp_NOTEGUAL_func */
		ZlangCompare_bigdecimal_LT_bigdecimal , /* ZlangCompareFunction *comp_LT_func */
		ZlangCompare_bigdecimal_LE_bigdecimal , /* ZlangCompareFunction *comp_LE_func */
		ZlangCompare_bigdecimal_GT_bigdecimal , /* ZlangCompareFunction *comp_GT_func */
		ZlangCompare_bigdecimal_GE_bigdecimal , /* ZlangCompareFunction *comp_GE_func */
		
		NULL , /* ZlangLogicFunction *logic_AND_func */
		NULL , /* ZlangLogicFunction *logic_OR_func */
		
		NULL , /* ZlangLogicFunction *bit_AND_func */
		NULL , /* ZlangLogicFunction *bit_XOR_func */
		NULL , /* ZlangLogicFunction *bit_OR_func */
		NULL , /* ZlangLogicFunction *bit_MOVELEFT_func */
		NULL , /* ZlangLogicFunction *bit_MOVERIGHT_func */
		
		ZlangSummarizeDirectPropertySize_bigdecimal , /* ZlangSummarizeDirectPropertySizeFunction */
	} ;

ZlangImportObjectFunction ZlangImportObject_bigdecimal;
struct ZlangObject *ZlangImportObject_bigdecimal( struct ZlangRuntime *rt )
{
	struct ZlangObject	*obj = NULL ;
	struct ZlangFunction	*func = NULL ;
	int			nret = 0 ;
	
	nret = ImportObject( rt , & obj , ZLANG_OBJECT_bigdecimal , & direct_funcs_bigdecimal , sizeof(struct ZlangDirectFunctions) , NULL ) ;
	if( nret )
	{
		SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_LINK_FUNC_TO_ENTITY , "import object to global objects heap" )
		return NULL;
	}
	
	/* bigdecimal.bigdecimal(double) */
	func = AddFunctionAndParametersInObject( rt , obj , "bigdecimal" , "bigdecimal(double)" , ZlangInvokeFunction_bigdecimal_Set_double , ZLANG_OBJECT_void , ZLANG_OBJECT_double,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* bigdecimal.bigdecimal(string) */
	func = AddFunctionAndParametersInObject( rt , obj , "bigdecimal" , "bigdecimal(string)" , ZlangInvokeFunction_bigdecimal_Set_string , ZLANG_OBJECT_void , ZLANG_OBJECT_string,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* bigdecimal.bigdecimal(string,int) */
	func = AddFunctionAndParametersInObject( rt , obj , "bigdecimal" , "bigdecimal(string,int)" , ZlangInvokeFunction_bigdecimal_Set_string_int , ZLANG_OBJECT_void , ZLANG_OBJECT_string,NULL , ZLANG_OBJECT_int,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* bigdecimal.Set(double) */
	func = AddFunctionAndParametersInObject( rt , obj , "Set" , "Set(double)" , ZlangInvokeFunction_bigdecimal_Set_double , ZLANG_OBJECT_void , ZLANG_OBJECT_double,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* bigdecimal.Set(string) */
	func = AddFunctionAndParametersInObject( rt , obj , "Set" , "Set(string)" , ZlangInvokeFunction_bigdecimal_Set_string , ZLANG_OBJECT_void , ZLANG_OBJECT_string,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* bigdecimal.Set(string,int) */
	func = AddFunctionAndParametersInObject( rt , obj , "Set" , "Set(string,int)" , ZlangInvokeFunction_bigdecimal_Set_string_int , ZLANG_OBJECT_void , ZLANG_OBJECT_string,NULL , ZLANG_OBJECT_int,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* bigdecimal.ToString(int) */
	func = AddFunctionAndParametersInObject( rt , obj , "ToString" , "ToString(int)" , ZlangInvokeFunction_bigdecimal_ToString_int , ZLANG_OBJECT_string , ZLANG_OBJECT_int,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* bigdecimal.Swap(bigdecimal,bigdecimal) */
	func = AddFunctionAndParametersInObject( rt , obj , "Swap" , "Swap(bigdecimal,bigdecimal)" , ZlangInvokeFunction_bigdecimal_Swap_bigdecimal_bigdecimal , ZLANG_OBJECT_void , ZLANG_OBJECT_bigdecimal,NULL , ZLANG_OBJECT_bigdecimal,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* bigdecimal.Abs() */
	func = AddFunctionAndParametersInObject( rt , obj , "Abs" , "Abs()" , ZlangInvokeFunction_bigdecimal_Abs , ZLANG_OBJECT_bigdecimal , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* bigdecimal.Pow(int) */
	func = AddFunctionAndParametersInObject( rt , obj , "Pow" , "Pow(int)" , ZlangInvokeFunction_bigdecimal_Pow_int , ZLANG_OBJECT_bigdecimal , ZLANG_OBJECT_int,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* bigdecimal.Sqrt() */
	func = AddFunctionAndParametersInObject( rt , obj , "Sqrt" , "Sqrt()" , ZlangInvokeFunction_bigdecimal_Sqrt , ZLANG_OBJECT_bigdecimal , NULL ) ;
	if( func == NULL )
		return NULL;
	
	return obj;
}

