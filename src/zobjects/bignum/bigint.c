#include "zobjects_bignum.h"

#define ZLANG_OBJECT_bigint		"bigint"

struct ZlangDirectProperty_bigint
{
	mpz_t		value ;
} ;

ZlangInvokeFunction ZlangInvokeFunction_bigint_Set_int;
int ZlangInvokeFunction_bigint_Set_int( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_bigint	*bigint_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	int32_t					n ;
	
	CallRuntimeFunction_int_GetIntValue( rt , in1 , & n );
	mpz_set_si( bigint_direct_prop->value , (signed long)n );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_bigint_Set_string;
int ZlangInvokeFunction_bigint_Set_string( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_bigint	*bigint_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	char					*str = NULL ;
	int32_t					str_len ;
	
	CallRuntimeFunction_string_GetStringValue( rt , in1 , & str , & str_len );
	mpz_set_str( bigint_direct_prop->value , str , 10 );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_bigint_Set_string_int;
int ZlangInvokeFunction_bigint_Set_string_int( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_bigint	*bigint_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject			*in2 = GetInputParameterInLocalObjectStack(rt,2) ;
	char					*str = NULL ;
	int32_t					str_len ;
	int32_t					base ;
	
	CallRuntimeFunction_string_GetStringValue( rt , in1 , & str , & str_len );
	CallRuntimeFunction_int_GetIntValue( rt , in2 , & base );
	mpz_set_str( bigint_direct_prop->value , str , (int)base );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_bigint_ToString_int;
int ZlangInvokeFunction_bigint_ToString_int( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_bigint	*bigint_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject			*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	char					*to_str = NULL ;
	int32_t					to_str_len ;
	int32_t					base ;
	
	CallRuntimeFunction_int_GetIntValue( rt , in1 , & base );
	
	to_str = mpz_get_str( NULL , base , bigint_direct_prop->value ) ;
	to_str_len = (int32_t)strlen( to_str ) ;
	CallRuntimeFunction_string_SetStringValue( rt , out1 , to_str , to_str_len );
	free( to_str );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_bigint_Swap_bigint_bigint;
int ZlangInvokeFunction_bigint_Swap_bigint_bigint( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangObject			*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangDirectProperty_bigint	*in1_bigint_direct_prop = GetObjectDirectProperty(in1) ;
	struct ZlangObject			*in2 = GetInputParameterInLocalObjectStack(rt,2) ;
	struct ZlangDirectProperty_bigint	*in2_bigint_direct_prop = GetObjectDirectProperty(in2) ;
	
	mpz_swap( in1_bigint_direct_prop->value , in2_bigint_direct_prop->value );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_bigint_Abs;
int ZlangInvokeFunction_bigint_Abs( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_bigint	*bigint_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	struct ZlangDirectProperty_bigint	*out1_bigint_direct_prop = GetObjectDirectProperty(out1) ;
	
	mpz_abs( out1_bigint_direct_prop->value , bigint_direct_prop->value );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_bigint_Pow_int;
int ZlangInvokeFunction_bigint_Pow_int( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_bigint	*bigint_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject			*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	struct ZlangDirectProperty_bigint	*out1_bigint_direct_prop = GetObjectDirectProperty(out1) ;
	int32_t					exp ;
	
	CallRuntimeFunction_int_GetIntValue( rt , in1 , & exp );
	
	mpz_pow_ui( out1_bigint_direct_prop->value , bigint_direct_prop->value , (unsigned long)exp );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_bigint_Root_int;
int ZlangInvokeFunction_bigint_Root_int( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_bigint	*bigint_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject			*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	struct ZlangDirectProperty_bigint	*out1_bigint_direct_prop = GetObjectDirectProperty(out1) ;
	int32_t					n ;
	
	CallRuntimeFunction_int_GetIntValue( rt , in1 , & n );
	
	mpz_root( out1_bigint_direct_prop->value , bigint_direct_prop->value , (unsigned long)n );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_bigint_Sqrt;
int ZlangInvokeFunction_bigint_Sqrt( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_bigint	*bigint_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	struct ZlangDirectProperty_bigint	*out1_bigint_direct_prop = GetObjectDirectProperty(out1) ;
	
	mpz_sqrt( out1_bigint_direct_prop->value , bigint_direct_prop->value );
	
	return 0;
}

ZlangCreateDirectPropertyFunction ZlangCreateDirectProperty_bigint;
void *ZlangCreateDirectProperty_bigint( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_bigint	*bigint_prop = NULL ;
	
	bigint_prop = (struct ZlangDirectProperty_bigint *)malloc( sizeof(struct ZlangDirectProperty_bigint) ) ;
	if( bigint_prop == NULL )
	{
		SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_ALLOC , "alloc memory for entity" )
		return NULL;
	}
	memset( bigint_prop , 0x00 , sizeof(struct ZlangDirectProperty_bigint) );
	
	mpz_init( bigint_prop->value );
	
	return bigint_prop;
}

ZlangDestroyDirectPropertyFunction ZlangDestroyDirectProperty_bigint;
void ZlangDestroyDirectProperty_bigint( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_bigint	*bigint_direct_prop = GetObjectDirectProperty(obj) ;
	
	if( bigint_direct_prop )
	{
		mpz_clear( bigint_direct_prop->value );
		
		free( bigint_direct_prop );
	}
	
	return;
}

ZlangToStringFunction ZlangToString_bigint;
int ZlangToString_bigint( struct ZlangRuntime *rt , struct ZlangObject *obj , struct ZlangObject **tostr_obj )
{
	struct ZlangDirectProperty_bigint	*bigint_direct_prop = GetObjectDirectProperty(obj) ;
	char					*to_str = NULL ;
	int32_t					to_str_len ;
	
	to_str = mpz_get_str( NULL , 10 , bigint_direct_prop->value ) ;
	to_str_len = (int32_t)strlen( to_str ) ;
	CallRuntimeFunction_string_SetStringValue( rt , (*tostr_obj) , to_str , to_str_len );
	free( to_str );
	
	return 0;
}

ZlangFromDataPtrFunction ZlangFromDataPtr_bigint;
int ZlangFromDataPtr_bigint( struct ZlangRuntime *rt , struct ZlangObject *obj , void *value_ptr , int32_t value_len )
{
	struct ZlangDirectProperty_bigint	*obj_direct_prop = GetObjectDirectProperty(obj) ;
	
	mpz_set( obj_direct_prop->value , *(mpz_t *)value_ptr );
	
	return 0;
}

ZlangGetDataPtrFunction ZlangGetDataPtr_bigint;
int ZlangGetDataPtr_bigint( struct ZlangRuntime *rt , struct ZlangObject *obj , void **value_ptr , int32_t *value_len )
{
	struct ZlangDirectProperty_bigint	*obj_direct_prop = GetObjectDirectProperty(obj) ;
	
	if( value_ptr )
		(*value_ptr) = & (obj_direct_prop->value) ;
	if( value_len )
		(*value_len) = sizeof(obj_direct_prop->value) ;
	return 0;
}

ZlangOperatorFunction ZlangOperator_bigint_PLUS_bigint;
int ZlangOperator_bigint_PLUS_bigint( struct ZlangRuntime *rt , struct ZlangObject *in_obj1 , struct ZlangObject *in_obj2 , struct ZlangObject **out_obj )
{
	struct ZlangDirectProperty_bigint	*in1 = GetObjectDirectProperty(in_obj1) ;
	struct ZlangDirectProperty_bigint	*in2 = GetObjectDirectProperty(in_obj2) ;
	struct ZlangDirectProperty_bigint	*out = NULL ;
	
	out = GetObjectDirectProperty(*out_obj) ;
	mpz_add( out->value , in1->value , in2->value );
	
	return 0;
}

ZlangOperatorFunction ZlangOperator_bigint_MINUS_bigint;
int ZlangOperator_bigint_MINUS_bigint( struct ZlangRuntime *rt , struct ZlangObject *in_obj1 , struct ZlangObject *in_obj2 , struct ZlangObject **out_obj )
{
	struct ZlangDirectProperty_bigint	*in1 = GetObjectDirectProperty(in_obj1) ;
	struct ZlangDirectProperty_bigint	*in2 = GetObjectDirectProperty(in_obj2) ;
	struct ZlangDirectProperty_bigint	*out = NULL ;
	
	out = GetObjectDirectProperty(*out_obj) ;
	mpz_sub( out->value , in1->value , in2->value );
	
	return 0;
}

ZlangOperatorFunction ZlangOperator_bigint_MUL_bigint;
int ZlangOperator_bigint_MUL_bigint( struct ZlangRuntime *rt , struct ZlangObject *in_obj1 , struct ZlangObject *in_obj2 , struct ZlangObject **out_obj )
{
	struct ZlangDirectProperty_bigint	*in1 = GetObjectDirectProperty(in_obj1) ;
	struct ZlangDirectProperty_bigint	*in2 = GetObjectDirectProperty(in_obj2) ;
	struct ZlangDirectProperty_bigint	*out = NULL ;
	
	out = GetObjectDirectProperty(*out_obj) ;
	mpz_mul( out->value , in1->value , in2->value );
	
	return 0;
}

ZlangOperatorFunction ZlangOperator_bigint_DIV_bigint;
int ZlangOperator_bigint_DIV_bigint( struct ZlangRuntime *rt , struct ZlangObject *in_obj1 , struct ZlangObject *in_obj2 , struct ZlangObject **out_obj )
{
	struct ZlangDirectProperty_bigint	*in1 = GetObjectDirectProperty(in_obj1) ;
	struct ZlangDirectProperty_bigint	*in2 = GetObjectDirectProperty(in_obj2) ;
	struct ZlangDirectProperty_bigint	*out = NULL ;
	
	if( mpz_get_ui(in2->value) == 0 )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "*** FATAL : division by zero" )
		return ThrowFatalException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_DIVISION_BY_ZERO );
	}
	
	out = GetObjectDirectProperty(*out_obj) ;
	mpz_cdiv_q( out->value , in1->value , in2->value );
	
	return 0;
}

ZlangOperatorFunction ZlangOperator_bigint_MOD_bigint;
int ZlangOperator_bigint_MOD_bigint( struct ZlangRuntime *rt , struct ZlangObject *in_obj1 , struct ZlangObject *in_obj2 , struct ZlangObject **out_obj )
{
	struct ZlangDirectProperty_bigint	*in1 = GetObjectDirectProperty(in_obj1) ;
	struct ZlangDirectProperty_bigint	*in2 = GetObjectDirectProperty(in_obj2) ;
	struct ZlangDirectProperty_bigint	*out = NULL ;
	
	if( mpz_get_ui(in2->value) == 0 )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "*** FATAL : division by zero" )
		return ThrowFatalException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_DIVISION_BY_ZERO );
	}
	
	out = GetObjectDirectProperty(*out_obj) ;
	mpz_cdiv_r( out->value , in1->value , in2->value );
	
	return 0;
}

ZlangUnaryOperatorFunction ZlangUnaryOperator_NEGATIVE_bigint;
int ZlangUnaryOperator_NEGATIVE_bigint( struct ZlangRuntime *rt , struct ZlangObject *in_out_obj1 )
{
	struct ZlangDirectProperty_bigint	*in_out = GetObjectDirectProperty(in_out_obj1) ;
	
	mpz_neg( in_out->value , in_out->value );
	
	return 0;
} 

ZlangUnaryOperatorFunction ZlangUnaryOperator_NOT_bigint;
int ZlangUnaryOperator_NOT_bigint( struct ZlangRuntime *rt , struct ZlangObject *in_out_obj1 )
{
	struct ZlangDirectProperty_bigint	*in_out = GetObjectDirectProperty(in_out_obj1) ;
	
	if( mpz_cmp_si( in_out->value , 0 ) )
		mpz_set_si( in_out->value , (signed long)1 );
	else
		mpz_set_si( in_out->value , (signed long)0 );
	
	return 0;
}

ZlangUnaryOperatorFunction ZlangUnaryOperator_PLUS_PLUS_bigint;
int ZlangUnaryOperator_PLUS_PLUS_bigint( struct ZlangRuntime *rt , struct ZlangObject *in_out_obj1 )
{
	struct ZlangDirectProperty_bigint	*in_out = GetObjectDirectProperty(in_out_obj1) ;
	
	mpz_add_ui( in_out->value , in_out->value , (unsigned long)1 );
	
	return 0;
}

ZlangUnaryOperatorFunction ZlangUnaryOperator_MINUS_MINUS_bigint;
int ZlangUnaryOperator_MINUS_MINUS_bigint( struct ZlangRuntime *rt , struct ZlangObject *in_out_obj1 )
{
	struct ZlangDirectProperty_bigint	*in_out = GetObjectDirectProperty(in_out_obj1) ;
	
	mpz_sub_ui( in_out->value , in_out->value , (unsigned long)1 );
	
	return 0;
}

ZlangCompareFunction ZlangCompare_bigint_EGUAL_bigint;
int ZlangCompare_bigint_EGUAL_bigint( struct ZlangRuntime *rt , struct ZlangObject *in_obj1 , struct ZlangObject *in_obj2 , struct ZlangObject *out_obj )
{
	struct ZlangDirectProperty_bigint	*in1 = GetObjectDirectProperty(in_obj1) ;
	struct ZlangDirectProperty_bigint	*in2 = GetObjectDirectProperty(in_obj2) ;
	
	if( mpz_cmp( in1->value , in2->value ) == 0 )
	{
		CallRuntimeFunction_bool_SetBoolValue( rt , out_obj , TRUE );
	}
	else
	{
		CallRuntimeFunction_bool_SetBoolValue( rt , out_obj , FALSE );
	}
	
	return 0;
}

ZlangCompareFunction ZlangCompare_bigint_NOTEGUAL_bigint;
int ZlangCompare_bigint_NOTEGUAL_bigint( struct ZlangRuntime *rt , struct ZlangObject *in_obj1 , struct ZlangObject *in_obj2 , struct ZlangObject *out_obj )
{
	struct ZlangDirectProperty_bigint	*in1 = GetObjectDirectProperty(in_obj1) ;
	struct ZlangDirectProperty_bigint	*in2 = GetObjectDirectProperty(in_obj2) ;
	
	if( mpz_cmp( in1->value , in2->value ) != 0 )
	{
		CallRuntimeFunction_bool_SetBoolValue( rt , out_obj , TRUE );
	}
	else
	{
		CallRuntimeFunction_bool_SetBoolValue( rt , out_obj , FALSE );
	}
	
	return 0;
}

ZlangCompareFunction ZlangCompare_bigint_LT_bigint;
int ZlangCompare_bigint_LT_bigint( struct ZlangRuntime *rt , struct ZlangObject *in_obj1 , struct ZlangObject *in_obj2 , struct ZlangObject *out_obj )
{
	struct ZlangDirectProperty_bigint	*in1 = GetObjectDirectProperty(in_obj1) ;
	struct ZlangDirectProperty_bigint	*in2 = GetObjectDirectProperty(in_obj2) ;
	
	if( mpz_cmp( in1->value , in2->value ) < 0 )
	{
		CallRuntimeFunction_bool_SetBoolValue( rt , out_obj , TRUE );
	}
	else
	{
		CallRuntimeFunction_bool_SetBoolValue( rt , out_obj , FALSE );
	}
	
	return 0;
}

ZlangCompareFunction ZlangCompare_bigint_LE_bigint;
int ZlangCompare_bigint_LE_bigint( struct ZlangRuntime *rt , struct ZlangObject *in_obj1 , struct ZlangObject *in_obj2 , struct ZlangObject *out_obj )
{
	struct ZlangDirectProperty_bigint	*in1 = GetObjectDirectProperty(in_obj1) ;
	struct ZlangDirectProperty_bigint	*in2 = GetObjectDirectProperty(in_obj2) ;
	
	if( mpz_cmp( in1->value , in2->value ) <= 0 )
	{
		CallRuntimeFunction_bool_SetBoolValue( rt , out_obj , TRUE );
	}
	else
	{
		CallRuntimeFunction_bool_SetBoolValue( rt , out_obj , FALSE );
	}
	
	return 0;
}

ZlangCompareFunction ZlangCompare_bigint_GT_bigint;
int ZlangCompare_bigint_GT_bigint( struct ZlangRuntime *rt , struct ZlangObject *in_obj1 , struct ZlangObject *in_obj2 , struct ZlangObject *out_obj )
{
	struct ZlangDirectProperty_bigint	*in1 = GetObjectDirectProperty(in_obj1) ;
	struct ZlangDirectProperty_bigint	*in2 = GetObjectDirectProperty(in_obj2) ;
	
	if( mpz_cmp( in1->value , in2->value ) > 0 )
	{
		CallRuntimeFunction_bool_SetBoolValue( rt , out_obj , TRUE );
	}
	else
	{
		CallRuntimeFunction_bool_SetBoolValue( rt , out_obj , FALSE );
	}
	
	return 0;
}

ZlangCompareFunction ZlangCompare_bigint_GE_bigint;
int ZlangCompare_bigint_GE_bigint( struct ZlangRuntime *rt , struct ZlangObject *in_obj1 , struct ZlangObject *in_obj2 , struct ZlangObject *out_obj )
{
	struct ZlangDirectProperty_bigint	*in1 = GetObjectDirectProperty(in_obj1) ;
	struct ZlangDirectProperty_bigint	*in2 = GetObjectDirectProperty(in_obj2) ;
	
	if( mpz_cmp( in1->value , in2->value ) >= 0 )
	{
		CallRuntimeFunction_bool_SetBoolValue( rt , out_obj , TRUE );
	}
	else
	{
		CallRuntimeFunction_bool_SetBoolValue( rt , out_obj , FALSE );
	}
	
	return 0;
}

ZlangBitFunction ZlangBit_bigint_AND_bigint;
int ZlangBit_bigint_AND_bigint( struct ZlangRuntime *rt , struct ZlangObject *in_obj1 , struct ZlangObject *in_obj2 , struct ZlangObject *out_obj )
{
	struct ZlangDirectProperty_bigint	*in1 = GetObjectDirectProperty(in_obj1) ;
	struct ZlangDirectProperty_bigint	*in2 = GetObjectDirectProperty(in_obj2) ;
	struct ZlangDirectProperty_bigint	*out = NULL ;
	
	out = GetObjectDirectProperty(out_obj) ;
	mpz_and( out->value , in1->value , in2->value );
	
	return 0;
}

ZlangBitFunction ZlangBit_bigint_XOR_bigint;
int ZlangBit_bigint_XOR_bigint( struct ZlangRuntime *rt , struct ZlangObject *in_obj1 , struct ZlangObject *in_obj2 , struct ZlangObject *out_obj )
{
	struct ZlangDirectProperty_bigint	*in1 = GetObjectDirectProperty(in_obj1) ;
	struct ZlangDirectProperty_bigint	*in2 = GetObjectDirectProperty(in_obj2) ;
	struct ZlangDirectProperty_bigint	*out = NULL ;
	
	out = GetObjectDirectProperty(out_obj) ;
	mpz_xor( out->value , in1->value , in2->value );
	
	return 0;
}

ZlangBitFunction ZlangBit_bigint_OR_bigint;
int ZlangBit_bigint_OR_bigint( struct ZlangRuntime *rt , struct ZlangObject *in_obj1 , struct ZlangObject *in_obj2 , struct ZlangObject *out_obj )
{
	struct ZlangDirectProperty_bigint	*in1 = GetObjectDirectProperty(in_obj1) ;
	struct ZlangDirectProperty_bigint	*in2 = GetObjectDirectProperty(in_obj2) ;
	struct ZlangDirectProperty_bigint	*out = NULL ;
	
	out = GetObjectDirectProperty(out_obj) ;
	mpz_ior( out->value , in1->value , in2->value );
	
	return 0;
}

ZlangSummarizeDirectPropertySizeFunction ZlangSummarizeDirectPropertySize_bigint;
void ZlangSummarizeDirectPropertySize_bigint( struct ZlangRuntime *rt , struct ZlangObject *obj , size_t *summarized_obj_size , size_t *summarized_direct_prop_size )
{
	SUMMARIZE_SIZE( summarized_direct_prop_size , sizeof(struct ZlangDirectProperty_bigint) )
	
	return;
}

static struct ZlangDirectFunctions direct_funcs_bigint =
	{
		ZLANG_OBJECT_bigint , /* char *ancestor_name */
		
		ZlangCreateDirectProperty_bigint , /* ZlangCreateDirectPropertyFunction *create_entity_func */
		ZlangDestroyDirectProperty_bigint , /* ZlangDestroyDirectPropertyFunction *destroy_entity_func */
		
		NULL , /* ZlangFromCharPtrFunction *from_char_ptr_func */
		ZlangToString_bigint , /* ZlangToStringFunction *to_string_func */
		ZlangFromDataPtr_bigint , /* ZlangFromDataPtrFunction *from_data_ptr_func */
		ZlangGetDataPtr_bigint , /* ZlangGetDataPtrFunction *get_data_ptr_func */
		
		ZlangOperator_bigint_PLUS_bigint , /* ZlangOperatorFunction *oper_PLUS_func */
		ZlangOperator_bigint_MINUS_bigint , /* ZlangOperatorFunction *oper_MINUS_func */
		ZlangOperator_bigint_MUL_bigint , /* ZlangOperatorFunction *oper_MUL_func */
		ZlangOperator_bigint_DIV_bigint , /* ZlangOperatorFunction *oper_DIV_func */
		ZlangOperator_bigint_MOD_bigint , /* ZlangOperatorFunction *oper_MOD_func */
		
		ZlangUnaryOperator_NEGATIVE_bigint , /* ZlangUnaryOperatorFunction *unaryoper_NEGATIVE_func */
		ZlangUnaryOperator_NOT_bigint , /* ZlangUnaryOperatorFunction *unaryoper_NOT_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_BIT_REVERSE_func */
		ZlangUnaryOperator_PLUS_PLUS_bigint , /* ZlangUnaryOperatorFunction *unaryoper_PLUS_PLUS_func */
		ZlangUnaryOperator_MINUS_MINUS_bigint , /* ZlangUnaryOperatorFunction *unaryoper_MINUS_MINUS_func */
		
		ZlangCompare_bigint_EGUAL_bigint , /* ZlangCompareFunction *comp_EGUAL_func */
		ZlangCompare_bigint_NOTEGUAL_bigint , /* ZlangCompareFunction *comp_NOTEGUAL_func */
		ZlangCompare_bigint_LT_bigint , /* ZlangCompareFunction *comp_LT_func */
		ZlangCompare_bigint_LE_bigint , /* ZlangCompareFunction *comp_LE_func */
		ZlangCompare_bigint_GT_bigint , /* ZlangCompareFunction *comp_GT_func */
		ZlangCompare_bigint_GE_bigint , /* ZlangCompareFunction *comp_GE_func */
		
		NULL , /* ZlangLogicFunction *logic_AND_func */
		NULL , /* ZlangLogicFunction *logic_OR_func */
		
		ZlangBit_bigint_AND_bigint , /* ZlangLogicFunction *bit_AND_func */
		ZlangBit_bigint_XOR_bigint , /* ZlangLogicFunction *bit_XOR_func */
		ZlangBit_bigint_OR_bigint , /* ZlangLogicFunction *bit_OR_func */
		NULL , /* ZlangLogicFunction *bit_MOVELEFT_func */
		NULL , /* ZlangLogicFunction *bit_MOVERIGHT_func */
		
		ZlangSummarizeDirectPropertySize_bigint , /* ZlangSummarizeDirectPropertySizeFunction */
	} ;

ZlangImportObjectFunction ZlangImportObject_bigint;
struct ZlangObject *ZlangImportObject_bigint( struct ZlangRuntime *rt )
{
	struct ZlangObject	*obj = NULL ;
	struct ZlangFunction	*func = NULL ;
	int			nret = 0 ;
	
	nret = ImportObject( rt , & obj , ZLANG_OBJECT_bigint , & direct_funcs_bigint , sizeof(struct ZlangDirectFunctions) , NULL ) ;
	if( nret )
	{
		SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_LINK_FUNC_TO_ENTITY , "import object to global objects heap" )
		return NULL;
	}
	
	/* bigint.bigint(int) */
	func = AddFunctionAndParametersInObject( rt , obj , "bigint" , "bigint(int)" , ZlangInvokeFunction_bigint_Set_int , ZLANG_OBJECT_void , ZLANG_OBJECT_int,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* bigint.bigint(string) */
	func = AddFunctionAndParametersInObject( rt , obj , "bigint" , "bigint(string)" , ZlangInvokeFunction_bigint_Set_string , ZLANG_OBJECT_void , ZLANG_OBJECT_string,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* bigint.bigint(string,int) */
	func = AddFunctionAndParametersInObject( rt , obj , "bigint" , "bigint(string,int)" , ZlangInvokeFunction_bigint_Set_string_int , ZLANG_OBJECT_void , ZLANG_OBJECT_string,NULL , ZLANG_OBJECT_int,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* bigint.Set(int) */
	func = AddFunctionAndParametersInObject( rt , obj , "Set" , "Set(int)" , ZlangInvokeFunction_bigint_Set_int , ZLANG_OBJECT_void , ZLANG_OBJECT_int,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* bigint.Set(string) */
	func = AddFunctionAndParametersInObject( rt , obj , "Set" , "Set(string)" , ZlangInvokeFunction_bigint_Set_string , ZLANG_OBJECT_void , ZLANG_OBJECT_string,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* bigint.Set(string,int) */
	func = AddFunctionAndParametersInObject( rt , obj , "Set" , "Set(string,int)" , ZlangInvokeFunction_bigint_Set_string_int , ZLANG_OBJECT_void , ZLANG_OBJECT_string,NULL , ZLANG_OBJECT_int,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* bigint.ToString(int) */
	func = AddFunctionAndParametersInObject( rt , obj , "ToString" , "ToString(int)" , ZlangInvokeFunction_bigint_ToString_int , ZLANG_OBJECT_string , ZLANG_OBJECT_int,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* bigint.Swap(bigint,bigint) */
	func = AddFunctionAndParametersInObject( rt , obj , "Swap" , "Swap(bigint,bigint)" , ZlangInvokeFunction_bigint_Swap_bigint_bigint , ZLANG_OBJECT_void , ZLANG_OBJECT_bigint,NULL , ZLANG_OBJECT_bigint,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* bigint.Abs() */
	func = AddFunctionAndParametersInObject( rt , obj , "Abs" , "Abs()" , ZlangInvokeFunction_bigint_Abs , ZLANG_OBJECT_bigint , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* bigint.Pow(int) */
	func = AddFunctionAndParametersInObject( rt , obj , "Pow" , "Pow(int)" , ZlangInvokeFunction_bigint_Pow_int , ZLANG_OBJECT_bigint , ZLANG_OBJECT_int,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* bigint.Root(int) */
	func = AddFunctionAndParametersInObject( rt , obj , "Root" , "Root(int)" , ZlangInvokeFunction_bigint_Root_int , ZLANG_OBJECT_bigint , ZLANG_OBJECT_int,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* bigint.Sqrt() */
	func = AddFunctionAndParametersInObject( rt , obj , "Sqrt" , "Sqrt()" , ZlangInvokeFunction_bigint_Sqrt , ZLANG_OBJECT_bigint , NULL ) ;
	if( func == NULL )
		return NULL;
	
	return obj;
}

