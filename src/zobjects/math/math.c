/* Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "zobjects_math.h"

struct ZlangDirectProperty_math
{
	int		dummy ;
} ;

#define GET_VALUE___FLOAT_OR_DOUBLE_DEFAULT_DOUBLE \
	double		d ; \
	\
	if( IsTypeOf( rt , obj , GetFloatObjectInRuntimeObjectsHeap(rt) ) ) \
	{ \
		float		f ; \
		CallRuntimeFunction_float_GetFloatValue( rt , obj , & f ); \
		d = (double) f ; \
	} \
	else if( IsTypeOf( rt , obj , GetDoubleObjectInRuntimeObjectsHeap(rt) ) ) \
	{ \
		CallRuntimeFunction_double_GetDoubleValue( rt , obj , & d ); \
	} \
	else \
	{ \
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "object type[%s] not supported" , GetCloneObjectName(obj) ) \
		return ThrowFatalException( rt , ZLANG_FATAL_INVOKE_METHOD_RETURN , "object type not supported" ); \
	} \
	
#define SET_VALUE___FLOAT_OR_DOUBLE_DEFAULT_DOUBLE \
	if( IsTypeOf( rt , out1 , GetFloatObjectInRuntimeObjectsHeap(rt) ) ) \
	{ \
		float		f ; \
		f = (float) r ; \
		CallRuntimeFunction_float_SetFloatValue( rt , out1 , f ); \
	} \
	else if( IsTypeOf( rt , out1 , GetDoubleObjectInRuntimeObjectsHeap(rt) ) ) \
	{ \
		CallRuntimeFunction_double_SetDoubleValue( rt , out1 , r ); \
	} \
	else \
	{ \
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "object type[%s] not supported" , GetCloneObjectName(obj) ) \
		return ThrowFatalException( rt , ZLANG_FATAL_INVOKE_METHOD_RETURN , "object type not supported" ); \
	} \

#define GET_VALUE___SHORT_OR_INT_OR_LONG_DEFAULT_LONG \
	int64_t		l ; \
	\
	if( IsTypeOf( rt , obj , GetShortObjectInRuntimeObjectsHeap(rt) ) ) \
	{ \
		int16_t		s ; \
		CallRuntimeFunction_short_GetShortValue( rt , obj , & s ); \
		l = (long) s ; \
	} \
	else if( IsTypeOf( rt , obj , GetIntObjectInRuntimeObjectsHeap(rt) ) ) \
	{ \
		int32_t		i ; \
		CallRuntimeFunction_int_GetIntValue( rt , obj , & i ); \
		l = (long) i ; \
	} \
	else if( IsTypeOf( rt , obj , GetLongObjectInRuntimeObjectsHeap(rt) ) ) \
	{ \
		CallRuntimeFunction_long_GetLongValue( rt , obj , & l ); \
	} \
	else \
	{ \
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "object type[%s] not supported" , GetCloneObjectName(obj) ) \
		return ThrowFatalException( rt , ZLANG_FATAL_INVOKE_METHOD_RETURN , "object type not supported" ); \
	} \
	
#define SET_VALUE___SHORT_OR_INT_OR_LONG_DEFAULT_LONG \
	if( IsTypeOf( rt , out1 , GetShortObjectInRuntimeObjectsHeap(rt) ) ) \
	{ \
		int16_t		s ; \
		s = (int16_t) r ; \
		CallRuntimeFunction_short_SetShortValue( rt , out1 , s ); \
	} \
	else if( IsTypeOf( rt , out1 , GetIntObjectInRuntimeObjectsHeap(rt) ) ) \
	{ \
		int32_t		i ; \
		i = (int32_t) r ; \
		CallRuntimeFunction_int_SetIntValue( rt , out1 , i ); \
	} \
	else if( IsTypeOf( rt , out1 , GetLongObjectInRuntimeObjectsHeap(rt) ) ) \
	{ \
		CallRuntimeFunction_long_SetLongValue( rt , out1 , r ); \
	} \
	else \
	{ \
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "object type[%s] not supported" , GetCloneObjectName(obj) ) \
		return ThrowFatalException( rt , ZLANG_FATAL_INVOKE_METHOD_RETURN , "object type not supported" ); \
	} \

ZlangInvokeFunction ZlangInvokeFunction_floatingnumber_Sin;
int ZlangInvokeFunction_floatingnumber_Sin( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangObject	*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	double			r ;
	
	GET_VALUE___FLOAT_OR_DOUBLE_DEFAULT_DOUBLE
	
	r = sin( d ) ;
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "%lf = sin(%lf) ;" , r , d )
	
	SET_VALUE___FLOAT_OR_DOUBLE_DEFAULT_DOUBLE
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_floatingnumber_Cos;
int ZlangInvokeFunction_floatingnumber_Cos( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangObject		*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	double				r ; \
	
	GET_VALUE___FLOAT_OR_DOUBLE_DEFAULT_DOUBLE
	
	r = cos( d ) ;
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "%lf = cos(%lf) ;" , r , d )
	
	SET_VALUE___FLOAT_OR_DOUBLE_DEFAULT_DOUBLE
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_floatingnumber_Tan;
int ZlangInvokeFunction_floatingnumber_Tan( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangObject		*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	double				r ; \
	
	GET_VALUE___FLOAT_OR_DOUBLE_DEFAULT_DOUBLE
	
	r = tan( d ) ;
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "%lf = tan(%lf) ;" , r , d )
	
	SET_VALUE___FLOAT_OR_DOUBLE_DEFAULT_DOUBLE
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_floatingnumber_ASin;
int ZlangInvokeFunction_floatingnumber_ASin( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangObject	*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	double			r ;
	
	GET_VALUE___FLOAT_OR_DOUBLE_DEFAULT_DOUBLE
	
	r = asin( d ) ;
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "%lf = asin(%lf) ;" , r , d )
	
	SET_VALUE___FLOAT_OR_DOUBLE_DEFAULT_DOUBLE
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_floatingnumber_ACos;
int ZlangInvokeFunction_floatingnumber_ACos( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangObject		*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	double				r ; \
	
	GET_VALUE___FLOAT_OR_DOUBLE_DEFAULT_DOUBLE
	
	r = acos( d ) ;
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "%lf = acos(%lf) ;" , r , d )
	
	SET_VALUE___FLOAT_OR_DOUBLE_DEFAULT_DOUBLE
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_floatingnumber_ATan;
int ZlangInvokeFunction_floatingnumber_ATan( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangObject		*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	double				r ; \
	
	GET_VALUE___FLOAT_OR_DOUBLE_DEFAULT_DOUBLE
	
	r = atan( d ) ;
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "%lf = atan(%lf) ;" , r , d )
	
	SET_VALUE___FLOAT_OR_DOUBLE_DEFAULT_DOUBLE
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_floatingnumber_SinH;
int ZlangInvokeFunction_floatingnumber_SinH( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangObject	*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	double			r ;
	
	GET_VALUE___FLOAT_OR_DOUBLE_DEFAULT_DOUBLE
	
	r = sinh( d ) ;
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "%lf = sinh(%lf) ;" , r , d )
	
	SET_VALUE___FLOAT_OR_DOUBLE_DEFAULT_DOUBLE
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_floatingnumber_CosH;
int ZlangInvokeFunction_floatingnumber_CosH( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangObject		*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	double				r ; \
	
	GET_VALUE___FLOAT_OR_DOUBLE_DEFAULT_DOUBLE
	
	r = cosh( d ) ;
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "%lf = cosh(%lf) ;" , r , d )
	
	SET_VALUE___FLOAT_OR_DOUBLE_DEFAULT_DOUBLE
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_floatingnumber_TanH;
int ZlangInvokeFunction_floatingnumber_TanH( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangObject		*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	double				r ; \
	
	GET_VALUE___FLOAT_OR_DOUBLE_DEFAULT_DOUBLE
	
	r = tanh( d ) ;
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "%lf = tanh(%lf) ;" , r , d )
	
	SET_VALUE___FLOAT_OR_DOUBLE_DEFAULT_DOUBLE
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_floatingnumber_Log;
int ZlangInvokeFunction_floatingnumber_Log( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangObject		*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	double				r ; \
	
	GET_VALUE___FLOAT_OR_DOUBLE_DEFAULT_DOUBLE
	
	r = log( d ) ;
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "%lf = log(%lf) ;" , r , d )
	
	SET_VALUE___FLOAT_OR_DOUBLE_DEFAULT_DOUBLE
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_floatingnumber_Log10;
int ZlangInvokeFunction_floatingnumber_Log10( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangObject		*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	double				r ; \
	
	GET_VALUE___FLOAT_OR_DOUBLE_DEFAULT_DOUBLE
	
	r = log10( d ) ;
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "%lf = log10(%lf) ;" , r , d )
	
	SET_VALUE___FLOAT_OR_DOUBLE_DEFAULT_DOUBLE
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_floatingnumber_Pow_double;
int ZlangInvokeFunction_floatingnumber_Pow_double( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangObject		*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject		*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	double				exponent ;
	double				r ; \
	
	GET_VALUE___FLOAT_OR_DOUBLE_DEFAULT_DOUBLE
	
	CallRuntimeFunction_double_GetDoubleValue( rt , in1 , & exponent );
	
	r = pow( d , exponent ) ;
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "%lf = pow(%lf,%lf) ;" , r , d , exponent )
	
	SET_VALUE___FLOAT_OR_DOUBLE_DEFAULT_DOUBLE
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_floatingnumber_Exp;
int ZlangInvokeFunction_floatingnumber_Exp( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangObject		*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	double				r ; \
	
	GET_VALUE___FLOAT_OR_DOUBLE_DEFAULT_DOUBLE
	
	r = exp( d ) ;
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "%lf = exp(%lf) ;" , r , d )
	
	SET_VALUE___FLOAT_OR_DOUBLE_DEFAULT_DOUBLE
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_floatingnumber_Sqrt;
int ZlangInvokeFunction_floatingnumber_Sqrt( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangObject		*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	double				r ; \
	
	GET_VALUE___FLOAT_OR_DOUBLE_DEFAULT_DOUBLE
	
	r = sqrt( d ) ;
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "%lf = sqrt(%lf) ;" , r , d )
	
	SET_VALUE___FLOAT_OR_DOUBLE_DEFAULT_DOUBLE
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_integernumber_Pow_int;
int ZlangInvokeFunction_integernumber_Pow_int( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangObject		*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject		*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	int32_t				exponent ;
	int64_t				r ; \
	
	GET_VALUE___SHORT_OR_INT_OR_LONG_DEFAULT_LONG
	
	CallRuntimeFunction_int_GetIntValue( rt , in1 , & exponent );
	
	r = (int64_t)pow( (double)l , (double)exponent ) ;
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "%"PRIi64" = pow(%"PRIi64",%"PRIi32") ;" , r , l , exponent )
	
	SET_VALUE___SHORT_OR_INT_OR_LONG_DEFAULT_LONG
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_integernumber_Sqrt;
int ZlangInvokeFunction_integernumber_Sqrt( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangObject		*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	int64_t				r ; \
	
	GET_VALUE___SHORT_OR_INT_OR_LONG_DEFAULT_LONG
	
	r = (int64_t)sqrt( (double)l ) ;
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "%"PRIi64" = pow(%"PRIi64") ;" , r , l )
	
	SET_VALUE___SHORT_OR_INT_OR_LONG_DEFAULT_LONG
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_floatingnumber_Ceil;
int ZlangInvokeFunction_floatingnumber_Ceil( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangObject		*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	double				r ; \
	
	GET_VALUE___FLOAT_OR_DOUBLE_DEFAULT_DOUBLE
	
	r = ceil( d ) ;
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "%lf = ceil(%lf) ;" , r , d )
	
	SET_VALUE___FLOAT_OR_DOUBLE_DEFAULT_DOUBLE
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_floatingnumber_Floor;
int ZlangInvokeFunction_floatingnumber_Floor( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangObject		*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	double				r ; \
	
	GET_VALUE___FLOAT_OR_DOUBLE_DEFAULT_DOUBLE
	
	r = floor( d ) ;
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "%lf = floor(%lf) ;" , r , d )
	
	SET_VALUE___FLOAT_OR_DOUBLE_DEFAULT_DOUBLE
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_integernumber_Abs;
int ZlangInvokeFunction_integernumber_Abs( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangObject		*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	int64_t				r ; \
	
	GET_VALUE___SHORT_OR_INT_OR_LONG_DEFAULT_LONG
	
	r = (int64_t)labs( (long)l ) ;
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "%"PRIi64" = abs(%"PRIi64") ;" , r , l )
	
	SET_VALUE___SHORT_OR_INT_OR_LONG_DEFAULT_LONG
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_floatingnumber_Abs;
int ZlangInvokeFunction_floatingnumber_Abs( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangObject		*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	double				r ; \
	
	GET_VALUE___FLOAT_OR_DOUBLE_DEFAULT_DOUBLE
	
	r = fabs( d ) ;
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "%lf = fabs(%lf) ;" , r , d )
	
	SET_VALUE___FLOAT_OR_DOUBLE_DEFAULT_DOUBLE
	
	return 0;
}

ZlangCreateDirectPropertyFunction ZlangCreateDirectProperty_math;
void *ZlangCreateDirectProperty_math( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_math	*math_prop = NULL ;
	
	math_prop = (struct ZlangDirectProperty_math *)ZLMALLOC( sizeof(struct ZlangDirectProperty_math) ) ;
	if( math_prop == NULL )
	{
		SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_ALLOC , "alloc memory for entity" )
		return NULL;
	}
	memset( math_prop , 0x00 , sizeof(struct ZlangDirectProperty_math) );
	
	return math_prop;
}

ZlangDestroyDirectPropertyFunction ZlangDestroyDirectProperty_math;
void ZlangDestroyDirectProperty_math( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_math	*math_direct_prop = GetObjectDirectProperty(obj) ;
	
	ZLFREE( math_direct_prop );
	
	return;
}

ZlangSummarizeDirectPropertySizeFunction ZlangSummarizeDirectPropertySize_math;
void ZlangSummarizeDirectPropertySize_math( struct ZlangRuntime *rt , struct ZlangObject *obj , size_t *summarized_obj_size , size_t *summarized_direct_prop_size )
{
	SUMMARIZE_SIZE( summarized_direct_prop_size , sizeof(struct ZlangDirectProperty_math) )
	return;
}

static struct ZlangDirectFunctions direct_funcs_math =
	{
		ZLANG_OBJECT_math , /* char *tpye_name */
		
		ZlangCreateDirectProperty_math , /* ZlangCreateDirectPropertyFunction *create_entity_func */
		ZlangDestroyDirectProperty_math , /* ZlangDestroyDirectPropertyFunction *destroy_entity_func */
		
		NULL , /* ZlangFromCharPtrFunction *from_char_ptr_func */
		NULL , /* ZlangToStringFunction *to_string_func */
		NULL , /* ZlangFromDataPtrFunction *from_data_ptr_func */
		NULL , /* ZlangGetDataPtrFunction *get_data_ptr_func */
		
		NULL , /* ZlangOperatorFunction *oper_PLUS_func */
		NULL , /* ZlangOperatorFunction *oper_MINUS_func */
		NULL , /* ZlangOperatorFunction *oper_MUL_func */
		NULL , /* ZlangOperatorFunction *oper_DIV_func */
		NULL , /* ZlangOperatorFunction *oper_MOD_func */
		
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_NEGATIVE_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_NOT_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_BIT_REVERSE_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_PLUS_PLUS_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_MINUS_MINUS_func */
		
		NULL , /* ZlangCompareFunction *comp_EGUAL_func */
		NULL , /* ZlangCompareFunction *comp_NOTEGUAL_func */
		NULL , /* ZlangCompareFunction *comp_LT_func */
		NULL , /* ZlangCompareFunction *comp_LE_func */
		NULL , /* ZlangCompareFunction *comp_GT_func */
		NULL , /* ZlangCompareFunction *comp_GE_func */
		
		NULL , /* ZlangLogicFunction *logic_AND_func */
		NULL , /* ZlangLogicFunction *logic_OR_func */
		
		NULL , /* ZlangLogicFunction *bit_AND_func */
		NULL , /* ZlangLogicFunction *bit_XOR_func */
		NULL , /* ZlangLogicFunction *bit_OR_func */
		NULL , /* ZlangLogicFunction *bit_MOVELEFT_func */
		NULL , /* ZlangLogicFunction *bit_MOVERIGHT_func */
		
		ZlangSummarizeDirectPropertySize_math , /* ZlangSummarizeDirectPropertySizeFunction *summarize_direct_prop_size_func */
	} ;

ZlangImportObjectFunction ZlangImportObject_math;
struct ZlangObject *ZlangImportObject_math( struct ZlangRuntime *rt )
{
	struct ZlangObject	*obj = NULL ;
	struct ZlangObject	*prop = NULL ;
	struct ZlangFunction	*func = NULL ;
	struct ZlangObject	*short_obj = NULL ;
	struct ZlangObject	*int_obj = NULL ;
	struct ZlangObject	*long_obj = NULL ;
	struct ZlangObject	*float_obj = NULL ;
	struct ZlangObject	*double_obj = NULL ;
	int			nret = 0 ;
	
	nret = ImportObject( rt , & obj , ZLANG_OBJECT_math , & direct_funcs_math , sizeof(struct ZlangDirectFunctions) , NULL ) ;
	if( nret )
	{
		SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_LINK_FUNC_TO_ENTITY , "import object to global objects heap" )
		return NULL;
	}
	
	/* double math.E */
	prop = AddPropertyInObject( rt , obj , QueryObjectByObjectName(rt,"double") , "E" ) ;
	if( prop == NULL )
		return NULL;
	CallRuntimeFunction_double_SetDoubleValue( rt , prop , 2.71828182845904523536 );
	SetConstantObject( prop );
	
	/* double math.PI */
	prop = AddPropertyInObject( rt , obj , QueryObjectByObjectName(rt,"double") , "PI" ) ;
	if( prop == NULL )
		return NULL;
	CallRuntimeFunction_double_SetDoubleValue( rt , prop , 3.14159265358979323846 );
	SetConstantObject( prop );
	
	/* stdtypes.Functions */
	short_obj = GetShortObjectInRuntimeObjectsHeap(rt) ;
	int_obj = GetIntObjectInRuntimeObjectsHeap(rt) ;
	long_obj = GetLongObjectInRuntimeObjectsHeap(rt) ;
	float_obj = GetFloatObjectInRuntimeObjectsHeap(rt) ;
	double_obj = GetDoubleObjectInRuntimeObjectsHeap(rt) ;
	
	if( short_obj )
	{
		/* short.Pow(int) */
		func = AddFunctionAndParametersInObject( rt , short_obj , "Pow" , "Pow(int)" , ZlangInvokeFunction_integernumber_Pow_int , ZLANG_OBJECT_short , ZLANG_OBJECT_int,NULL , NULL ) ;
		if( func == NULL )
			return NULL;
		
		/* short.Sqrt() */
		func = AddFunctionAndParametersInObject( rt , short_obj , "Sqrt" , "Sqrt()" , ZlangInvokeFunction_integernumber_Sqrt , ZLANG_OBJECT_short , NULL ) ;
		if( func == NULL )
			return NULL;
		
		/* short.Abs() */
		func = AddFunctionAndParametersInObject( rt , short_obj , "Abs" , "Abs()" , ZlangInvokeFunction_integernumber_Abs , ZLANG_OBJECT_short , NULL ) ;
		if( func == NULL )
			return NULL;
	}
	if( int_obj )
	{
		/* int.Pow(int) */
		func = AddFunctionAndParametersInObject( rt , int_obj , "Pow" , "Pow(int)" , ZlangInvokeFunction_integernumber_Pow_int , ZLANG_OBJECT_int , ZLANG_OBJECT_int,NULL , NULL ) ;
		if( func == NULL )
			return NULL;
		
		/* int.Sqrt() */
		func = AddFunctionAndParametersInObject( rt , int_obj , "Sqrt" , "Sqrt()" , ZlangInvokeFunction_integernumber_Sqrt , ZLANG_OBJECT_int , NULL ) ;
		if( func == NULL )
			return NULL;
		
		/* int.Abs() */
		func = AddFunctionAndParametersInObject( rt , int_obj , "Abs" , "Abs()" , ZlangInvokeFunction_integernumber_Abs , ZLANG_OBJECT_int , NULL ) ;
		if( func == NULL )
			return NULL;
	}
	if( long_obj )
	{
		/* long.Pow(int) */
		func = AddFunctionAndParametersInObject( rt , long_obj , "Pow" , "Pow(int)" , ZlangInvokeFunction_integernumber_Pow_int , ZLANG_OBJECT_long , ZLANG_OBJECT_int,NULL , NULL ) ;
		if( func == NULL )
			return NULL;
		
		/* long.Sqrt() */
		func = AddFunctionAndParametersInObject( rt , long_obj , "Sqrt" , "Sqrt()" , ZlangInvokeFunction_integernumber_Sqrt , ZLANG_OBJECT_long , NULL ) ;
		if( func == NULL )
			return NULL;
		
		/* long.Abs() */
		func = AddFunctionAndParametersInObject( rt , long_obj , "Abs" , "Abs()" , ZlangInvokeFunction_integernumber_Abs , ZLANG_OBJECT_long , NULL ) ;
		if( func == NULL )
			return NULL;
	}
	if( float_obj )
	{
		/* float.Sin() */
		func = AddFunctionAndParametersInObject( rt , float_obj , "Sin" , "Sin()" , ZlangInvokeFunction_floatingnumber_Sin , ZLANG_OBJECT_float , NULL ) ;
		if( func == NULL )
			return NULL;
		
		/* float.Cos() */
		func = AddFunctionAndParametersInObject( rt , float_obj , "Cos" , "Cos()" , ZlangInvokeFunction_floatingnumber_Cos , ZLANG_OBJECT_float , NULL ) ;
		if( func == NULL )
			return NULL;
		
		/* float.Tan() */
		func = AddFunctionAndParametersInObject( rt , float_obj , "Tan" , "Tan()" , ZlangInvokeFunction_floatingnumber_Tan , ZLANG_OBJECT_float , NULL ) ;
		if( func == NULL )
			return NULL;
		
		/* float.ASin() */
		func = AddFunctionAndParametersInObject( rt , float_obj , "ASin" , "ASin()" , ZlangInvokeFunction_floatingnumber_ASin , ZLANG_OBJECT_float , NULL ) ;
		if( func == NULL )
			return NULL;
		
		/* float.ACos() */
		func = AddFunctionAndParametersInObject( rt , float_obj , "ACos" , "ACos()" , ZlangInvokeFunction_floatingnumber_ACos , ZLANG_OBJECT_float , NULL ) ;
		if( func == NULL )
			return NULL;
		
		/* float.ATan() */
		func = AddFunctionAndParametersInObject( rt , float_obj , "ATan" , "ATan()" , ZlangInvokeFunction_floatingnumber_ATan , ZLANG_OBJECT_float , NULL ) ;
		if( func == NULL )
			return NULL;
		
		/* float.SinH() */
		func = AddFunctionAndParametersInObject( rt , float_obj , "SinH" , "SinH()" , ZlangInvokeFunction_floatingnumber_SinH , ZLANG_OBJECT_float , NULL ) ;
		if( func == NULL )
			return NULL;
		
		/* float.CosH() */
		func = AddFunctionAndParametersInObject( rt , float_obj , "CosH" , "CosH()" , ZlangInvokeFunction_floatingnumber_CosH , ZLANG_OBJECT_float , NULL ) ;
		if( func == NULL )
			return NULL;
		
		/* float.TanH() */
		func = AddFunctionAndParametersInObject( rt , float_obj , "TanH" , "TanH()" , ZlangInvokeFunction_floatingnumber_TanH , ZLANG_OBJECT_float , NULL ) ;
		if( func == NULL )
			return NULL;
		
		/* float.Log() */
		func = AddFunctionAndParametersInObject( rt , float_obj , "Log" , "Log()" , ZlangInvokeFunction_floatingnumber_Log , ZLANG_OBJECT_float , NULL ) ;
		if( func == NULL )
			return NULL;
		
		/* float.Log10() */
		func = AddFunctionAndParametersInObject( rt , float_obj , "Log10" , "Log10()" , ZlangInvokeFunction_floatingnumber_Log10 , ZLANG_OBJECT_float , NULL ) ;
		if( func == NULL )
			return NULL;
		
		/* float.Pow(double) */
		func = AddFunctionAndParametersInObject( rt , float_obj , "Pow" , "Pow(double)" , ZlangInvokeFunction_floatingnumber_Pow_double , ZLANG_OBJECT_float , ZLANG_OBJECT_double , NULL ) ;
		if( func == NULL )
			return NULL;
		
		/* float.Exp() */
		func = AddFunctionAndParametersInObject( rt , float_obj , "Exp" , "Exp()" , ZlangInvokeFunction_floatingnumber_Exp , ZLANG_OBJECT_float , NULL ) ;
		if( func == NULL )
			return NULL;
		
		/* float.Sqrt() */
		func = AddFunctionAndParametersInObject( rt , float_obj , "Sqrt" , "Sqrt()" , ZlangInvokeFunction_floatingnumber_Sqrt , ZLANG_OBJECT_float , NULL ) ;
		if( func == NULL )
			return NULL;
		
		/* float.Ceil() */
		func = AddFunctionAndParametersInObject( rt , float_obj , "Ceil" , "Ceil()" , ZlangInvokeFunction_floatingnumber_Ceil , ZLANG_OBJECT_float , NULL ) ;
		if( func == NULL )
			return NULL;
		
		/* float.Floor() */
		func = AddFunctionAndParametersInObject( rt , float_obj , "Floor" , "Floor()" , ZlangInvokeFunction_floatingnumber_Floor , ZLANG_OBJECT_float , NULL ) ;
		if( func == NULL )
			return NULL;
		
		/* float.Abs() */
		func = AddFunctionAndParametersInObject( rt , float_obj , "Abs" , "Abs()" , ZlangInvokeFunction_floatingnumber_Abs , ZLANG_OBJECT_float , NULL ) ;
		if( func == NULL )
			return NULL;
	}
	if( double_obj )
	{
		/* double.Sin() */
		func = AddFunctionAndParametersInObject( rt , double_obj , "Sin" , "Sin()" , ZlangInvokeFunction_floatingnumber_Sin , ZLANG_OBJECT_double , NULL ) ;
		if( func == NULL )
			return NULL;
		
		/* double.Cos() */
		func = AddFunctionAndParametersInObject( rt , double_obj , "Cos" , "Cos()" , ZlangInvokeFunction_floatingnumber_Cos , ZLANG_OBJECT_double , NULL ) ;
		if( func == NULL )
			return NULL;
		
		/* double.Tan() */
		func = AddFunctionAndParametersInObject( rt , double_obj , "Tan" , "Tan()" , ZlangInvokeFunction_floatingnumber_Tan , ZLANG_OBJECT_double , NULL ) ;
		if( func == NULL )
			return NULL;
		
		/* double.ASin() */
		func = AddFunctionAndParametersInObject( rt , double_obj , "ASin" , "ASin()" , ZlangInvokeFunction_floatingnumber_ASin , ZLANG_OBJECT_double , NULL ) ;
		if( func == NULL )
			return NULL;
		
		/* double.ACos() */
		func = AddFunctionAndParametersInObject( rt , double_obj , "ACos" , "ACos()" , ZlangInvokeFunction_floatingnumber_ACos , ZLANG_OBJECT_double , NULL ) ;
		if( func == NULL )
			return NULL;
		
		/* double.ATan() */
		func = AddFunctionAndParametersInObject( rt , double_obj , "ATan" , "ATan()" , ZlangInvokeFunction_floatingnumber_ATan , ZLANG_OBJECT_double , NULL ) ;
		if( func == NULL )
			return NULL;
		
		/* double.SinH() */
		func = AddFunctionAndParametersInObject( rt , double_obj , "SinH" , "SinH()" , ZlangInvokeFunction_floatingnumber_SinH , ZLANG_OBJECT_double , NULL ) ;
		if( func == NULL )
			return NULL;
		
		/* double.CosH() */
		func = AddFunctionAndParametersInObject( rt , double_obj , "CosH" , "CosH()" , ZlangInvokeFunction_floatingnumber_CosH , ZLANG_OBJECT_double , NULL ) ;
		if( func == NULL )
			return NULL;
		
		/* double.TanH() */
		func = AddFunctionAndParametersInObject( rt , double_obj , "TanH" , "TanH()" , ZlangInvokeFunction_floatingnumber_TanH , ZLANG_OBJECT_double , NULL ) ;
		if( func == NULL )
			return NULL;
		
		/* double.Log() */
		func = AddFunctionAndParametersInObject( rt , double_obj , "Log" , "Log()" , ZlangInvokeFunction_floatingnumber_Log , ZLANG_OBJECT_double , NULL ) ;
		if( func == NULL )
			return NULL;
		
		/* double.Log10() */
		func = AddFunctionAndParametersInObject( rt , double_obj , "Log10" , "Log10()" , ZlangInvokeFunction_floatingnumber_Log10 , ZLANG_OBJECT_double , NULL ) ;
		if( func == NULL )
			return NULL;
		
		/* double.Pow(double) */
		func = AddFunctionAndParametersInObject( rt , double_obj , "Pow" , "Pow(double)" , ZlangInvokeFunction_floatingnumber_Pow_double , ZLANG_OBJECT_double , ZLANG_OBJECT_double , NULL ) ;
		if( func == NULL )
			return NULL;
		
		/* double.Exp() */
		func = AddFunctionAndParametersInObject( rt , double_obj , "Exp" , "Exp()" , ZlangInvokeFunction_floatingnumber_Exp , ZLANG_OBJECT_double , NULL ) ;
		if( func == NULL )
			return NULL;
		
		/* double.Sqrt() */
		func = AddFunctionAndParametersInObject( rt , double_obj , "Sqrt" , "Sqrt()" , ZlangInvokeFunction_floatingnumber_Sqrt , ZLANG_OBJECT_double , NULL ) ;
		if( func == NULL )
			return NULL;
		
		/* double.Ceil() */
		func = AddFunctionAndParametersInObject( rt , double_obj , "Ceil" , "Ceil()" , ZlangInvokeFunction_floatingnumber_Ceil , ZLANG_OBJECT_double , NULL ) ;
		if( func == NULL )
			return NULL;
		
		/* double.Floor() */
		func = AddFunctionAndParametersInObject( rt , double_obj , "Floor" , "Floor()" , ZlangInvokeFunction_floatingnumber_Floor , ZLANG_OBJECT_double , NULL ) ;
		if( func == NULL )
			return NULL;
		
		/* double.Abs() */
		func = AddFunctionAndParametersInObject( rt , double_obj , "Abs" , "Abs()" , ZlangInvokeFunction_floatingnumber_Abs , ZLANG_OBJECT_double , NULL ) ;
		if( func == NULL )
			return NULL;
	}
	
	return obj ;
}

