/*
 * libgus - Global Unique Sequence Generator
 * author	: calvin
 * email	: calvinwilliams@163.com
 *
 * Licensed under the LGPL v2.1, see the file LICENSE in base directory.
 */

#ifndef _H_LIBGUS_
#define _H_LIBGUS_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <inttypes.h>

#ifdef __cplusplus
extern "C" {
#endif

#if ( defined _WIN32 )
#ifndef _WINDLL_FUNC
#define _WINDLL_FUNC		_declspec(dllexport)
#endif
#elif ( defined __unix ) || ( defined _AIX ) || ( defined __linux__ ) || ( defined __hpux )
#ifndef _WINDLL_FUNC
#define _WINDLL_FUNC
#endif
#endif

#define GUS_SEQ_ID_LENGTH		32
#define GUS_IP_OR_NCMAC_STRING_LENGTH	17
#define GUS_TIMESTAMP_STRING_LENGTH	26
#define GUS_STRING_4BYTES_LENGTH	4
#define GUS_STRING_8BYTES_LENGTH	8

#define GUS_SUCCESS			0
#define GUS_ERROR_ALLOC			-8
#define GUS_ERROR_GETIFADDRS		-11
#define GUS_ERROR_GETIFADDRS_NOT_FOUND	-12
#define GUS_ERROR_SOCKET		-13
#define GUS_ERROR_IOCTL			-14
#define GUS_ERROR_SYSCALL		-21
#define GUS_ERROR_WSASTARTUP		-31
#define GUS_ERROR_SEQ_ID_INVALID	-71

#define GUS_GENERATE_OPTION_USE_NCMAC	1

/*
GUS序列号格式 :
	| NETADDR(46bits) | TID(30bits) | TIMESTAMP(49bits) | SEQNO(3bits) |
	|       46        +  18  |  12  +       49          +      3       |
	|     frame1(64bits)     |            frame2(64bits)               |

	NETADDR : 14位全1(0x3FFF)+IPv4地址的32位数字形式(非回绕地址；大端网络字节序)，或者 46位网卡MAC去掉第一个字节第一、二低位（全局本地标志和单播组播标志）
	TID : 30位整型，Linux里的TID可通过"syscall(SYS_gettid)"获得，类型为pid_t；<br /> WINDOWS里的ThreadId，通过GetCurrentThreadId()获得，取其1/4，类型为DWORD；小端字节序
	TIMESTAMP : 49位微秒戳，从2020年到现在的微秒数，可使用17年（至2037年）；小端字节序
	SEQNO : 3位微秒内序号，取值空间为"[0,7]"，每微秒最多排出8个序列号，如果计算机实在太强劲而超出了，由具体实现处置，建议原地沉睡到下一微秒；小端字节序
*/

/*
:!./seondstamp_20200101                                                                                                                               
--- NOW ---
tt[1604545540]
stime.tm_year[120]
stime.tm_mon[10]
stime.tm_mday[4]
stime.tm_hour[22]
stime.tm_min[5]
stime.tm_sec[40]
--- 2020-01-01 00:00:00 ---
tt[1577854800]
*/

_WINDLL_FUNC int GUSGenerate( char seq_id[GUS_SEQ_ID_LENGTH+1] , uint32_t generate_options );
_WINDLL_FUNC int GUSExplain( char seq_id[GUS_SEQ_ID_LENGTH+1]
		, unsigned char *p_netaddr_is_ncmac , char ip_or_ncmac_str[GUS_IP_OR_NCMAC_STRING_LENGTH+1]
		, uint64_t *p_tid
		, char time_str[GUS_TIMESTAMP_STRING_LENGTH+1]
		, uint64_t *p_seqno );

#ifdef __cplusplus
}
#endif

#endif

