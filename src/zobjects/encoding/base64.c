/* Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "zobjects_encoding.h"

struct ZlangDirectProperty_base64
{
	int	dummy ;
} ;

ZlangInvokeFunction ZlangInvokeFunction_base64_Encode_string;
int ZlangInvokeFunction_base64_Encode_string( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangObject			*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject			*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	char					*from = NULL ;
	int32_t					from_len ;
	char					**to = NULL ;
	int32_t					*to_len ;
	int32_t					prepare_len ;
	int32_t					ret_len ;
	
	CallRuntimeFunction_string_GetStringValue( rt , in1 , & from , & from_len );
	
	CallRuntimeFunction_string_Clear( rt , out1 );
	if( from_len == 0 )
	{
		return 0;
	}
	prepare_len = ( from_len/3 + (from_len%3?1:0) ) * 4 ;
	CallRuntimeFunction_string_PrepareBuffer( rt , out1 , prepare_len );
	CallRuntimeFunction_string_GetDirectPropertiesPtr( rt , out1 , & to , NULL , & to_len );
	ret_len = EVP_EncodeBlock( (unsigned char *)(*to) , (unsigned char *)from , from_len ) ;
	if( ret_len == -1 )
	{
		UnreferObject( rt , out1 );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_EVP_ENCODEBLOCK_FAILED );
	}
	(*to_len) = ret_len ;
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_base64_Decode_string;
int ZlangInvokeFunction_base64_Decode_string( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangObject			*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject			*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	char					*from = NULL ;
	int32_t					from_len ;
	char					**to = NULL ;
	int32_t					*to_len ;
	int32_t					max_len ;
	int32_t					ret_len ;
	
	CallRuntimeFunction_string_GetStringValue( rt , in1 , & from , & from_len );
	
	CallRuntimeFunction_string_Clear( rt , out1 );
	if( ( from_len % 4 ) != 0 )
	{
		UnreferObject( rt , out1 );
		return 0;
	}
	max_len = from_len / 4 * 3 ;
	CallRuntimeFunction_string_PrepareBuffer( rt , out1 , max_len );
	CallRuntimeFunction_string_GetDirectPropertiesPtr( rt , out1 , & to , NULL , & to_len );
	ret_len = EVP_DecodeBlock( (unsigned char *)(*to) , (unsigned char *)from , from_len ) ;
	if( ret_len == -1 )
	{
		UnreferObject( rt , out1 );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_EVP_DECODEBLOCK_FAILED );
	}
	(*to_len) = ret_len ;
	
	return 0;
}

ZlangCreateDirectPropertyFunction ZlangCreateDirectProperty_base64;
void *ZlangCreateDirectProperty_base64( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_base64	*base64_direct_prop = NULL ;
	
	base64_direct_prop = (struct ZlangDirectProperty_base64 *)ZLMALLOC( sizeof(struct ZlangDirectProperty_base64) ) ;
	if( base64_direct_prop == NULL )
	{
		SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_ALLOC , "alloc memory for entity" )
		return NULL;
	}
	memset( base64_direct_prop , 0x00 , sizeof(struct ZlangDirectProperty_base64) );
	
	return base64_direct_prop;
}

ZlangDestroyDirectPropertyFunction ZlangDestroyDirectProperty_base64;
void ZlangDestroyDirectProperty_base64( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_base64	*base64_direct_prop = GetObjectDirectProperty(obj) ;
	
	ZLFREE( base64_direct_prop );
	
	return;
}

ZlangSummarizeDirectPropertySizeFunction ZlangSummarizeDirectPropertySize_base64;
void ZlangSummarizeDirectPropertySize_base64( struct ZlangRuntime *rt , struct ZlangObject *obj , size_t *summarized_obj_size , size_t *summarized_direct_prop_size )
{
	SUMMARIZE_SIZE( summarized_direct_prop_size , sizeof(struct ZlangDirectProperty_base64) )
	return;
}

static struct ZlangDirectFunctions direct_funcs_base64 =
	{
		ZLANG_OBJECT_base64 , /* char *ancestor_name */
		
		ZlangCreateDirectProperty_base64 , /* ZlangCreateDirectPropertyFunction *create_entity_func */
		ZlangDestroyDirectProperty_base64 , /* ZlangDestroyDirectPropertyFunction *destroy_entity_func */
		
		NULL , /* ZlangFromCharPtrFunction *from_char_ptr_func */
		NULL , /* ZlangToStringFunction *to_string_func */
		NULL , /* ZlangFromDataPtrFunction *from_data_ptr_func */
		NULL , /* ZlangGetDataPtrFunction *get_data_ptr_func */
		
		NULL , /* ZlangOperatorFunction *oper_PLUS_func */
		NULL , /* ZlangOperatorFunction *oper_MINUS_func */
		NULL , /* ZlangOperatorFunction *oper_MUL_func */
		NULL , /* ZlangOperatorFunction *oper_DIV_func */
		NULL , /* ZlangOperatorFunction *oper_MOD_func */
		
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_NEGATIVE_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_NOT_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_BIT_REVERSE_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_PLUS_PLUS_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_MINUS_MINUS_func */
		
		NULL , /* ZlangCompareFunction *comp_EGUAL_func */
		NULL , /* ZlangCompareFunction *comp_NOTEGUAL_func */
		NULL , /* ZlangCompareFunction *comp_LT_func */
		NULL , /* ZlangCompareFunction *comp_LE_func */
		NULL , /* ZlangCompareFunction *comp_GT_func */
		NULL , /* ZlangCompareFunction *comp_GE_func */
		
		NULL , /* ZlangLogicFunction *logic_AND_func */
		NULL , /* ZlangLogicFunction *logic_OR_func */
		
		NULL , /* ZlangLogicFunction *bit_AND_func */
		NULL , /* ZlangLogicFunction *bit_XOR_func */
		NULL , /* ZlangLogicFunction *bit_OR_func */
		NULL , /* ZlangLogicFunction *bit_MOVELEFT_func */
		NULL , /* ZlangLogicFunction *bit_MOVERIGHT_func */
		
		ZlangSummarizeDirectPropertySize_base64 , /* ZlangSummarizeDirectPropertySizeFunction *summarize_direct_prop_size_func */
	} ;

ZlangImportObjectFunction ZlangImportObject_base64;
struct ZlangObject *ZlangImportObject_base64( struct ZlangRuntime *rt )
{
	struct ZlangObject	*obj = NULL ;
	struct ZlangFunction	*func = NULL ;
	int			nret = 0 ;
	
	nret = ImportObject( rt , & obj , ZLANG_OBJECT_base64 , & direct_funcs_base64 , sizeof(struct ZlangDirectFunctions) , NULL ) ;
	if( nret )
	{
		SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_LINK_FUNC_TO_ENTITY , "import object to global objects heap" )
		return NULL;
	}
	
	/* base64.Encode(string) */
	func = AddFunctionAndParametersInObject( rt , obj , "Encode" , "Encode(string)" , ZlangInvokeFunction_base64_Encode_string , ZLANG_OBJECT_string , ZLANG_OBJECT_string,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* base64.Decode(string) */
	func = AddFunctionAndParametersInObject( rt , obj , "Decode" , "Decode(string)" , ZlangInvokeFunction_base64_Decode_string , ZLANG_OBJECT_string , ZLANG_OBJECT_string,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	return obj ;
}

