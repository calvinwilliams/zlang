/* Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "zobjects_encoding.h"

struct ZlangDirectProperty_gus_uuid
{
	int	dummy ;
} ;

ZlangInvokeFunction ZlangInvokeFunction_gus_uuid_Generate;
int ZlangInvokeFunction_gus_uuid_Generate( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangObject			*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	
	char					seq_id[ GUS_SEQ_ID_LENGTH + 1 ] ;
	
	int					nret = 0 ;
	
	nret = GUSGenerate( seq_id , 0 ) ;
	if( nret )
	{
		UnreferObject( rt , out1 );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_GUSGENERATE_FAILED );
	}
	
	CallRuntimeFunction_string_SetStringValue( rt , out1 , seq_id , GUS_SEQ_ID_LENGTH );
	
	return 0;
}

ZlangCreateDirectPropertyFunction ZlangCreateDirectProperty_gus_uuid;
void *ZlangCreateDirectProperty_gus_uuid( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_gus_uuid	*gus_uuid_direct_prop = NULL ;
	
	gus_uuid_direct_prop = (struct ZlangDirectProperty_gus_uuid *)ZLMALLOC( sizeof(struct ZlangDirectProperty_gus_uuid) ) ;
	if( gus_uuid_direct_prop == NULL )
	{
		SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_ALLOC , "alloc memory for entity" )
		return NULL;
	}
	memset( gus_uuid_direct_prop , 0x00 , sizeof(struct ZlangDirectProperty_gus_uuid) );
	
	return gus_uuid_direct_prop;
}

ZlangDestroyDirectPropertyFunction ZlangDestroyDirectProperty_gus_uuid;
void ZlangDestroyDirectProperty_gus_uuid( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_gus_uuid	*gus_uuid_direct_prop = GetObjectDirectProperty(obj) ;
	
	ZLFREE( gus_uuid_direct_prop );
	
	return;
}

ZlangSummarizeDirectPropertySizeFunction ZlangSummarizeDirectPropertySize_gus_uuid;
void ZlangSummarizeDirectPropertySize_gus_uuid( struct ZlangRuntime *rt , struct ZlangObject *obj , size_t *summarized_obj_size , size_t *summarized_direct_prop_size )
{
	SUMMARIZE_SIZE( summarized_direct_prop_size , sizeof(struct ZlangDirectProperty_gus_uuid) )
	return;
}

static struct ZlangDirectFunctions direct_funcs_gus_uuid =
	{
		ZLANG_OBJECT_gus_uuid , /* char *ancestor_name */
		
		ZlangCreateDirectProperty_gus_uuid , /* ZlangCreateDirectPropertyFunction *create_entity_func */
		ZlangDestroyDirectProperty_gus_uuid , /* ZlangDestroyDirectPropertyFunction *destroy_entity_func */
		
		NULL , /* ZlangFromCharPtrFunction *from_char_ptr_func */
		NULL , /* ZlangToStringFunction *to_string_func */
		NULL , /* ZlangFromDataPtrFunction *from_data_ptr_func */
		NULL , /* ZlangGetDataPtrFunction *get_data_ptr_func */
		
		NULL , /* ZlangOperatorFunction *oper_PLUS_func */
		NULL , /* ZlangOperatorFunction *oper_MINUS_func */
		NULL , /* ZlangOperatorFunction *oper_MUL_func */
		NULL , /* ZlangOperatorFunction *oper_DIV_func */
		NULL , /* ZlangOperatorFunction *oper_MOD_func */
		
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_NEGATIVE_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_NOT_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_BIT_REVERSE_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_PLUS_PLUS_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_MINUS_MINUS_func */
		
		NULL , /* ZlangCompareFunction *comp_EGUAL_func */
		NULL , /* ZlangCompareFunction *comp_NOTEGUAL_func */
		NULL , /* ZlangCompareFunction *comp_LT_func */
		NULL , /* ZlangCompareFunction *comp_LE_func */
		NULL , /* ZlangCompareFunction *comp_GT_func */
		NULL , /* ZlangCompareFunction *comp_GE_func */
		
		NULL , /* ZlangLogicFunction *logic_AND_func */
		NULL , /* ZlangLogicFunction *logic_OR_func */
		
		NULL , /* ZlangLogicFunction *bit_AND_func */
		NULL , /* ZlangLogicFunction *bit_XOR_func */
		NULL , /* ZlangLogicFunction *bit_OR_func */
		NULL , /* ZlangLogicFunction *bit_MOVELEFT_func */
		NULL , /* ZlangLogicFunction *bit_MOVERIGHT_func */
		
		ZlangSummarizeDirectPropertySize_gus_uuid , /* ZlangSummarizeDirectPropertySizeFunction *summarize_direct_prop_size_func */
	} ;

ZlangImportObjectFunction ZlangImportObject_gus_uuid;
struct ZlangObject *ZlangImportObject_gus_uuid( struct ZlangRuntime *rt )
{
	struct ZlangObject	*obj = NULL ;
	struct ZlangFunction	*func = NULL ;
	int			nret = 0 ;
	
	nret = ImportObject( rt , & obj , ZLANG_OBJECT_gus_uuid , & direct_funcs_gus_uuid , sizeof(struct ZlangDirectFunctions) , NULL ) ;
	if( nret )
	{
		SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_LINK_FUNC_TO_ENTITY , "import object to global objects heap" )
		return NULL;
	}
	
	/* gus_uuid.Generate() */
	func = AddFunctionAndParametersInObject( rt , obj , "Generate" , "Generate()" , ZlangInvokeFunction_gus_uuid_Generate , ZLANG_OBJECT_string , NULL ) ;
	if( func == NULL )
		return NULL;
	
	return obj ;
}

