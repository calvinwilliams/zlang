/* Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef _H_LIBZLANG_ENCODING_
#define _H_LIBZLANG_ENCODING_

#include "zlang.h"

#include <openssl/evp.h>

#include "libgus.h"

#define ZLANG_OBJECT_gus_uuid	"gus_uuid"
#define ZLANG_OBJECT_base64	"base64"
#define ZLANG_OBJECT_hexstr	"hexstr"
#define ZLANG_OBJECT_mbcs	"mbcs"

#define EXCEPTION_MESSAGE_EVP_ENCODEBLOCK_FAILED		"EVP_EncodeBlock failed"
#define EXCEPTION_MESSAGE_EVP_DECODEBLOCK_FAILED		"EVP_DecodeBlock failed"
#define EXCEPTION_MESSAGE_GUSGENERATE_FAILED			"GUSGenerate failed"
#define EXCEPTION_MESSAGE_DATA_LEN_INVALID			"data len invalid"
#define EXCEPTION_MESSAGE_CONVERT_STRING_ENCODING_FAILED	"convert string encoding failed"

DLLEXPORT ZlangImportObjectsFunction ZlangImportObjects;
int ZlangImportObjects( struct ZlangRuntime *rt );

#endif

