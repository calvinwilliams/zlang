/* Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "zobjects_encoding.h"

struct ZlangDirectProperty_hexstr
{
	int	dummy ;
} ;

ZlangInvokeFunction ZlangInvokeFunction_hexstr_HexExpand_string;
int ZlangInvokeFunction_hexstr_HexExpand_string( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangObject	*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject	*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	char			*from = NULL ;
	int32_t			from_len ;
	char			**to = NULL ;
	int32_t			*to_len ;
	char			*f = NULL ;
	char			*t = NULL ;
	int32_t			i ;
	int			left , right ;
	static char		charset[] = "0123456789ABCDEF" ;
	
	CallRuntimeFunction_string_GetStringValue( rt , in1 , & from , & from_len );
	
	CallRuntimeFunction_string_Clear( rt , out1 );
	if( from_len == 0 )
	{
		return 0;
	}
	CallRuntimeFunction_string_PrepareBuffer( rt , out1 , from_len*2 );
	CallRuntimeFunction_string_GetDirectPropertiesPtr( rt , out1 , & to , NULL , & to_len );
	t = (*to) ;
	for( i = 0 , f = from ; i < from_len ; i++ , f++ )
	{
		left = ((*f)>>4) & 0x0F ;
		right = (*f) & 0x0F ;
		(*t) = charset[left] ; t++;
		(*t) = charset[right] ; t++;
	}
	(*to_len) = t - (*to) ;
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_hexstr_HexFold_string;
int ZlangInvokeFunction_hexstr_HexFold_string( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangObject	*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject	*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	char			*from = NULL ;
	int32_t			from_len ;
	char			**to = NULL ;
	int32_t			*to_len ;
	char			*f = NULL ;
	char			*t = NULL ;
	int32_t			i ;
	char			left , right ;
	
	CallRuntimeFunction_string_GetStringValue( rt , in1 , & from , & from_len );
	if( from_len % 2 == 1 )
	{
		UnreferObject( rt , out1 );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_DATA_LEN_INVALID );
	}
	
	CallRuntimeFunction_string_Clear( rt , out1 );
	CallRuntimeFunction_string_PrepareBuffer( rt , out1 , from_len/2 );
	CallRuntimeFunction_string_GetDirectPropertiesPtr( rt , out1 , & to , NULL , & to_len );
	t = (*to) ;
	for( i = 0 , f = from ; i < from_len ; i += 2 , f += 2 )
	{
		left = (*f) ;
		right = *(f+1) ;
		
		if( '0'<=left && left<='9' )
			left -= '0' ;
		else if( 'a'<=left && left<='z' )
			left = left-'a' + 10 ;
		else if( 'A'<=left && left<='Z' )
			left = left-'A' + 10 ;
		
		if( '0'<=right && right<='9' )
			right -= '0' ;
		else if( 'a'<=right && right<='z' )
			right = right-'a' + 10 ;
		else if( 'A'<=right && right<='Z' )
			right = right-'A' + 10 ;
		
		(*t) = left<<4 | right ; t++;
	}
	(*to_len) = t - (*to) ;
	
	return 0;
}

ZlangCreateDirectPropertyFunction ZlangCreateDirectProperty_hexstr;
void *ZlangCreateDirectProperty_hexstr( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_hexstr	*hexstr_direct_prop = NULL ;
	
	hexstr_direct_prop = (struct ZlangDirectProperty_hexstr *)ZLMALLOC( sizeof(struct ZlangDirectProperty_hexstr) ) ;
	if( hexstr_direct_prop == NULL )
	{
		SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_ALLOC , "alloc memory for entity" )
		return NULL;
	}
	memset( hexstr_direct_prop , 0x00 , sizeof(struct ZlangDirectProperty_hexstr) );
	
	return hexstr_direct_prop;
}

ZlangDestroyDirectPropertyFunction ZlangDestroyDirectProperty_hexstr;
void ZlangDestroyDirectProperty_hexstr( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_hexstr	*hexstr_direct_prop = GetObjectDirectProperty(obj) ;
	
	ZLFREE( hexstr_direct_prop );
	
	return;
}

ZlangSummarizeDirectPropertySizeFunction ZlangSummarizeDirectPropertySize_hexstr;
void ZlangSummarizeDirectPropertySize_hexstr( struct ZlangRuntime *rt , struct ZlangObject *obj , size_t *summarized_obj_size , size_t *summarized_direct_prop_size )
{
	SUMMARIZE_SIZE( summarized_direct_prop_size , sizeof(struct ZlangDirectProperty_hexstr) )
	return;
}

static struct ZlangDirectFunctions direct_funcs_hexstr =
	{
		ZLANG_OBJECT_hexstr , /* char *ancestor_name */
		
		ZlangCreateDirectProperty_hexstr , /* ZlangCreateDirectPropertyFunction *create_entity_func */
		ZlangDestroyDirectProperty_hexstr , /* ZlangDestroyDirectPropertyFunction *destroy_entity_func */
		
		NULL , /* ZlangFromCharPtrFunction *from_char_ptr_func */
		NULL , /* ZlangToStringFunction *to_string_func */
		NULL , /* ZlangFromDataPtrFunction *from_data_ptr_func */
		NULL , /* ZlangGetDataPtrFunction *get_data_ptr_func */
		
		NULL , /* ZlangOperatorFunction *oper_PLUS_func */
		NULL , /* ZlangOperatorFunction *oper_MINUS_func */
		NULL , /* ZlangOperatorFunction *oper_MUL_func */
		NULL , /* ZlangOperatorFunction *oper_DIV_func */
		NULL , /* ZlangOperatorFunction *oper_MOD_func */
		
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_NEGATIVE_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_NOT_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_BIT_REVERSE_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_PLUS_PLUS_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_MINUS_MINUS_func */
		
		NULL , /* ZlangCompareFunction *comp_EGUAL_func */
		NULL , /* ZlangCompareFunction *comp_NOTEGUAL_func */
		NULL , /* ZlangCompareFunction *comp_LT_func */
		NULL , /* ZlangCompareFunction *comp_LE_func */
		NULL , /* ZlangCompareFunction *comp_GT_func */
		NULL , /* ZlangCompareFunction *comp_GE_func */
		
		NULL , /* ZlangLogicFunction *logic_AND_func */
		NULL , /* ZlangLogicFunction *logic_OR_func */
		
		NULL , /* ZlangLogicFunction *bit_AND_func */
		NULL , /* ZlangLogicFunction *bit_XOR_func */
		NULL , /* ZlangLogicFunction *bit_OR_func */
		NULL , /* ZlangLogicFunction *bit_MOVELEFT_func */
		NULL , /* ZlangLogicFunction *bit_MOVERIGHT_func */
		
		ZlangSummarizeDirectPropertySize_hexstr , /* ZlangSummarizeDirectPropertySizeFunction *summarize_direct_prop_size_func */
	} ;

ZlangImportObjectFunction ZlangImportObject_hexstr;
struct ZlangObject *ZlangImportObject_hexstr( struct ZlangRuntime *rt )
{
	struct ZlangObject	*obj = NULL ;
	struct ZlangFunction	*func = NULL ;
	int			nret = 0 ;
	
	nret = ImportObject( rt , & obj , ZLANG_OBJECT_hexstr , & direct_funcs_hexstr , sizeof(struct ZlangDirectFunctions) , NULL ) ;
	if( nret )
	{
		SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_LINK_FUNC_TO_ENTITY , "import object to global objects heap" )
		return NULL;
	}
	
	/* hexstr.HexExpand(string) */
	func = AddFunctionAndParametersInObject( rt , obj , "HexExpand" , "HexExpand(string)" , ZlangInvokeFunction_hexstr_HexExpand_string , ZLANG_OBJECT_string , ZLANG_OBJECT_string,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* hexstr.HexFold(string) */
	func = AddFunctionAndParametersInObject( rt , obj , "HexFold" , "HexFold(string)" , ZlangInvokeFunction_hexstr_HexFold_string , ZLANG_OBJECT_string , ZLANG_OBJECT_string,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	return obj ;
}

