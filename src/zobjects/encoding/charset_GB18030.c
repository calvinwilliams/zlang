/* Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

static struct ZlangCharsetAlias		g_zlang_charset_aliases_GB18030[] = {
		{ ZLANG_CHARSET_GB18030 , "生成" , "Generate" } ,
		{ ZLANG_CHARSET_GB18030 , "编码" , "Encode" } ,
		{ ZLANG_CHARSET_GB18030 , "解码" , "Decode" } ,
		{ ZLANG_CHARSET_GB18030 , "十六进制字符串" , "hexstr" } ,
		{ ZLANG_CHARSET_GB18030 , "十六进制展开" , "HexExpand" } ,
		{ ZLANG_CHARSET_GB18030 , "十六进制合拢" , "HexFold" } ,
		{ ZLANG_CHARSET_GB18030 , "转换字符编码" , "ConvertEncoding" } ,
		{ 0 , NULL , NULL } ,
	} ;

