/* Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "zobjects_encoding.h"

struct ZlangDirectProperty_mbcs
{
	int	dummy ;
} ;

ZlangInvokeFunction ZlangInvokeFunction_mbcs_ConvertEncoding_string_string_string;
int ZlangInvokeFunction_mbcs_ConvertEncoding_string_string_string( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangObject	*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject	*in2 = GetInputParameterInLocalObjectStack(rt,2) ;
	struct ZlangObject	*in3 = GetInputParameterInLocalObjectStack(rt,3) ;
	struct ZlangObject	*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	char			*from_encoding = NULL ;
	char			*to_encoding = NULL ;
	char			*str = NULL ;
	int32_t			str_len ;
	char			*cvt_str = NULL ;
	size_t			cvt_str_len ;
	int			nret = 0 ;
	
	CallRuntimeFunction_string_GetStringValue( rt , in1 , & from_encoding , NULL );
	CallRuntimeFunction_string_GetStringValue( rt , in2 , & to_encoding , NULL );
	CallRuntimeFunction_string_GetStringValue( rt , in3 , & str , & str_len );
	
	nret = ConvertStringEncoding( from_encoding , to_encoding , str , str_len , & cvt_str , NULL , & cvt_str_len ) ;
	if( nret )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "ConvertStringEncoding failed[%d]" , nret )
		UnreferObject( rt , out1 );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_CONVERT_STRING_ENCODING_FAILED );
	}
	
	CallRuntimeFunction_string_SetStringValue( rt , out1 , cvt_str , (int32_t)cvt_str_len );
	free( cvt_str );
	return 0;
}

ZlangCreateDirectPropertyFunction ZlangCreateDirectProperty_mbcs;
void *ZlangCreateDirectProperty_mbcs( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_mbcs	*mbcs_direct_prop = NULL ;
	
	mbcs_direct_prop = (struct ZlangDirectProperty_mbcs *)ZLMALLOC( sizeof(struct ZlangDirectProperty_mbcs) ) ;
	if( mbcs_direct_prop == NULL )
	{
		SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_ALLOC , "alloc memory for entity" )
		return NULL;
	}
	memset( mbcs_direct_prop , 0x00 , sizeof(struct ZlangDirectProperty_mbcs) );
	
	return mbcs_direct_prop;
}

ZlangDestroyDirectPropertyFunction ZlangDestroyDirectProperty_mbcs;
void ZlangDestroyDirectProperty_mbcs( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_mbcs	*mbcs_direct_prop = GetObjectDirectProperty(obj) ;
	
	ZLFREE( mbcs_direct_prop );
	
	return;
}

ZlangSummarizeDirectPropertySizeFunction ZlangSummarizeDirectPropertySize_mbcs;
void ZlangSummarizeDirectPropertySize_mbcs( struct ZlangRuntime *rt , struct ZlangObject *obj , size_t *summarized_obj_size , size_t *summarized_direct_prop_size )
{
	SUMMARIZE_SIZE( summarized_direct_prop_size , sizeof(struct ZlangDirectProperty_mbcs) )
	return;
}

static struct ZlangDirectFunctions direct_funcs_mbcs =
	{
		ZLANG_OBJECT_mbcs , /* char *ancestor_name */
		
		ZlangCreateDirectProperty_mbcs , /* ZlangCreateDirectPropertyFunction *create_entity_func */
		ZlangDestroyDirectProperty_mbcs , /* ZlangDestroyDirectPropertyFunction *destroy_entity_func */
		
		NULL , /* ZlangFromCharPtrFunction *from_char_ptr_func */
		NULL , /* ZlangToStringFunction *to_string_func */
		NULL , /* ZlangFromDataPtrFunction *from_data_ptr_func */
		NULL , /* ZlangGetDataPtrFunction *get_data_ptr_func */
		
		NULL , /* ZlangOperatorFunction *oper_PLUS_func */
		NULL , /* ZlangOperatorFunction *oper_MINUS_func */
		NULL , /* ZlangOperatorFunction *oper_MUL_func */
		NULL , /* ZlangOperatorFunction *oper_DIV_func */
		NULL , /* ZlangOperatorFunction *oper_MOD_func */
		
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_NEGATIVE_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_NOT_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_BIT_REVERSE_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_PLUS_PLUS_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_MINUS_MINUS_func */
		
		NULL , /* ZlangCompareFunction *comp_EGUAL_func */
		NULL , /* ZlangCompareFunction *comp_NOTEGUAL_func */
		NULL , /* ZlangCompareFunction *comp_LT_func */
		NULL , /* ZlangCompareFunction *comp_LE_func */
		NULL , /* ZlangCompareFunction *comp_GT_func */
		NULL , /* ZlangCompareFunction *comp_GE_func */
		
		NULL , /* ZlangLogicFunction *logic_AND_func */
		NULL , /* ZlangLogicFunction *logic_OR_func */
		
		NULL , /* ZlangLogicFunction *bit_AND_func */
		NULL , /* ZlangLogicFunction *bit_XOR_func */
		NULL , /* ZlangLogicFunction *bit_OR_func */
		NULL , /* ZlangLogicFunction *bit_MOVELEFT_func */
		NULL , /* ZlangLogicFunction *bit_MOVERIGHT_func */
		
		ZlangSummarizeDirectPropertySize_mbcs , /* ZlangSummarizeDirectPropertySizeFunction *summarize_direct_prop_size_func */
	} ;

ZlangImportObjectFunction ZlangImportObject_mbcs;
struct ZlangObject *ZlangImportObject_mbcs( struct ZlangRuntime *rt )
{
	struct ZlangObject	*obj = NULL ;
	struct ZlangFunction	*func = NULL ;
	int			nret = 0 ;
	
	nret = ImportObject( rt , & obj , ZLANG_OBJECT_mbcs , & direct_funcs_mbcs , sizeof(struct ZlangDirectFunctions) , NULL ) ;
	if( nret )
	{
		SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_LINK_FUNC_TO_ENTITY , "import object to global objects heap" )
		return NULL;
	}
	
	/* mbcs.ConvertEncoding(string,string,string) */
	func = AddFunctionAndParametersInObject( rt , obj , "ConvertEncoding" , "ConvertEncoding(string,string,string)" , ZlangInvokeFunction_mbcs_ConvertEncoding_string_string_string , ZLANG_OBJECT_string , ZLANG_OBJECT_string,NULL , ZLANG_OBJECT_string,NULL , ZLANG_OBJECT_string,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	return obj ;
}

