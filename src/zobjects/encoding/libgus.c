/*
 * libgus - Global Unique Sequence Generator
 * author	: calvin
 * email	: calvinwilliams@163.com
 *
 * Licensed under the LGPL v2.1, see the file LICENSE in base directory.
 */

#if ( defined __unix ) || ( defined _AIX ) || ( defined __linux__ ) || ( defined __hpux )
#include <netinet/in.h>
#include <ifaddrs.h>
#include <net/if.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <pthread.h>
#include <sys/syscall.h>
#include <arpa/inet.h>
#include <sys/time.h>
#elif ( defined _WIN32 )
#include<winsock2.h>
#include <windows.h>
#include <stdio.h>
#include <time.h>
#include <Iphlpapi.h>
// #include <ws2tcpip.h>
// #include <winsock2.h>
#endif

#include "libgus.h"

char	__GUS_C_VERSION_0_0_5_0[] = "0.0.5.0" ;
char	*__GUS_C_VERSION = __GUS_C_VERSION_0_0_5_0 ;

#define __DEBUG		0

#if ( defined _WIN32 )
static union{ uint32_t mylong; unsigned char c[4]; } __endian_test = { 0xFF886600 };
#define ENDIANNESS  (__endian_test.c[0])

#define _swpe16(data)	((((data) & 0xff00) >> 8) | (((data) & 0x00ff) << 8))
#define _swpe32(data)	((((data) & 0xff000000) >> 24)			  \
				| (((data) & 0x00ff0000) >> 16)		  \
				| (((data) & 0x0000ff00) << 16)		  \
				| (((data) & 0x000000ff) << 24))
#define _swpe64(data)	((((data) & (uint64_t)(0xff00000000000000)) >> 56)	   \
				| (((data) & (uint64_t)(0x00ff000000000000)) >> 40)  \
				| (((data) & (uint64_t)(0x0000ff0000000000)) >> 24)  \
				| (((data) & (uint64_t)(0x000000ff00000000)) >> 16)  \
				| (((data) & (uint64_t)(0x00000000ff000000)) << 16)  \
				| (((data) & (uint64_t)(0x0000000000ff0000)) << 24)  \
				| (((data) & (uint64_t)(0x000000000000ff00)) << 40)  \
				| (((data) & (uint64_t)(0x00000000000000ff)) << 56))

#define be16toh(data)	( (ENDIANNESS) ? (data) : _swpe16(data))
#define be32toh(data)	( (ENDIANNESS) ? (data) : _swpe32(data))
#define be64toh(data)	( (ENDIANNESS) ? (data) : _swpe64(data))
#define htobe16(data)	(be16toh(data))
#define htobe32(data)	(be32toh(data))
#define htobe64(data)	(be64toh(data))

#define le16toh(data)	((!ENDIANNESS) ? (data) : _swpe16(data))
#define le32toh(data)	((!ENDIANNESS) ? (data) : _swpe32(data))
#define le64toh(data)	((!ENDIANNESS) ? (data) : _swpe64(data))
#define htole16(data)	(le16toh(data))
#define htole32(data)	(le32toh(data))
#define htole64(data)	(le64toh(data))

#define hton16(data)	(htobe16(data))
#define hton32(data)	(htobe32(data))
#define hton64(data)	(htobe64(data))
#define ntoh16(data)	(be16toh(data))
#define ntoh32(data)	(be32toh(data))
#define ntoh64(data)	(be64toh(data))
#endif

#if ( defined _WIN32 )
__declspec(thread)	unsigned char	is_call_WSAStartup = 0 ;
#endif

#if ( defined __unix ) || ( defined _AIX ) || ( defined __linux__ ) || ( defined __hpux )
__thread		unsigned char	netaddr_is_ncmac = 0 ;
__thread		uint64_t	netaddr = 0 ;
__thread		uint64_t	tid = 0 ;
__thread		uint64_t	timestamp = 0 ;
__thread		uint64_t	seqno = 0 ;
#elif ( defined _WIN32 )
__declspec(thread)	unsigned char	netaddr_is_ncmac = 0 ;
__declspec(thread)	DWORD64		netaddr = 0 ;
__declspec(thread)	DWORD64		tid = 0 ;
__declspec(thread)	DWORD64		timestamp = 0 ;
__declspec(thread)	DWORD64		seqno = 0 ;
#endif

static const char	sg_base16_charset[] = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ" ;

#if ( defined _WIN32 )
void USleep( uint64_t usec )
{
	LARGE_INTEGER perfCnt, start, now;

	QueryPerformanceFrequency(&perfCnt);
	QueryPerformanceCounter(&start);

	do {
		QueryPerformanceCounter((LARGE_INTEGER*)&now);
	} while ((now.QuadPart - start.QuadPart) * 1000 * 1000 / (perfCnt.QuadPart) < (LONGLONG)usec);

	return;
}
#endif

#define GUS_GENERATE_32BITS(_u32_,_gus_id_offset_) \
	{ \
		(_gus_id_offset_)[0] = sg_base16_charset[( ((_u32_)&0xF0000000) >> 28 )] ; \
		(_gus_id_offset_)[1] = sg_base16_charset[( ((_u32_)&0x0F000000) >> 24 )] ; \
		(_gus_id_offset_)[2] = sg_base16_charset[( ((_u32_)&0x00F00000) >> 20 )] ; \
		(_gus_id_offset_)[3] = sg_base16_charset[( ((_u32_)&0x000F0000) >> 16 )] ; \
		(_gus_id_offset_)[4] = sg_base16_charset[( ((_u32_)&0x0000F000) >> 12 )] ; \
		(_gus_id_offset_)[5] = sg_base16_charset[( ((_u32_)&0x00000F00) >> 8 )] ; \
		(_gus_id_offset_)[6] = sg_base16_charset[( ((_u32_)&0x000000F0) >> 4 )] ; \
		(_gus_id_offset_)[7] = sg_base16_charset[( ((_u32_)&0x0000000F) )] ; \
	} \

#define GUS_GENERATE_48BITS(_u64_,_gus_id_offset_) \
	{ \
		(_gus_id_offset_)[0]  = sg_base16_charset[( ((_u64_)&0x0000F00000000000) >> 44 )] ; \
		(_gus_id_offset_)[1]  = sg_base16_charset[( ((_u64_)&0x00000F0000000000) >> 40 )] ; \
		(_gus_id_offset_)[2]  = sg_base16_charset[( ((_u64_)&0x000000F000000000) >> 36 )] ; \
		(_gus_id_offset_)[3]  = sg_base16_charset[( ((_u64_)&0x0000000F00000000) >> 32 )] ; \
		(_gus_id_offset_)[4]  = sg_base16_charset[( ((_u64_)&0x00000000F0000000) >> 28 )] ; \
		(_gus_id_offset_)[5]  = sg_base16_charset[( ((_u64_)&0x000000000F000000) >> 24 )] ; \
		(_gus_id_offset_)[6] = sg_base16_charset[( ((_u64_)&0x0000000000F00000) >> 20 )] ; \
		(_gus_id_offset_)[7] = sg_base16_charset[( ((_u64_)&0x00000000000F0000) >> 16 )] ; \
		(_gus_id_offset_)[8] = sg_base16_charset[( ((_u64_)&0x000000000000F000) >> 12 )] ; \
		(_gus_id_offset_)[9] = sg_base16_charset[( ((_u64_)&0x0000000000000F00) >> 8 )] ; \
		(_gus_id_offset_)[10] = sg_base16_charset[( ((_u64_)&0x00000000000000F0) >> 4 )] ; \
		(_gus_id_offset_)[11] = sg_base16_charset[( ((_u64_)&0x000000000000000F) )] ; \
	} \

#define GUS_GENERATE_64BITS(_u64_,_gus_id_offset_) \
	{ \
		(_gus_id_offset_)[0]  = sg_base16_charset[( ((_u64_)&0xF000000000000000) >> 60 )] ; \
		(_gus_id_offset_)[1]  = sg_base16_charset[( ((_u64_)&0x0F00000000000000) >> 56 )] ; \
		(_gus_id_offset_)[2]  = sg_base16_charset[( ((_u64_)&0x00F0000000000000) >> 52 )] ; \
		(_gus_id_offset_)[3]  = sg_base16_charset[( ((_u64_)&0x000F000000000000) >> 48 )] ; \
		(_gus_id_offset_)[4]  = sg_base16_charset[( ((_u64_)&0x0000F00000000000) >> 44 )] ; \
		(_gus_id_offset_)[5]  = sg_base16_charset[( ((_u64_)&0x00000F0000000000) >> 40 )] ; \
		(_gus_id_offset_)[6]  = sg_base16_charset[( ((_u64_)&0x000000F000000000) >> 36 )] ; \
		(_gus_id_offset_)[7]  = sg_base16_charset[( ((_u64_)&0x0000000F00000000) >> 32 )] ; \
		(_gus_id_offset_)[8]  = sg_base16_charset[( ((_u64_)&0x00000000F0000000) >> 28 )] ; \
		(_gus_id_offset_)[9]  = sg_base16_charset[( ((_u64_)&0x000000000F000000) >> 24 )] ; \
		(_gus_id_offset_)[10] = sg_base16_charset[( ((_u64_)&0x0000000000F00000) >> 20 )] ; \
		(_gus_id_offset_)[11] = sg_base16_charset[( ((_u64_)&0x00000000000F0000) >> 16 )] ; \
		(_gus_id_offset_)[12] = sg_base16_charset[( ((_u64_)&0x000000000000F000) >> 12 )] ; \
		(_gus_id_offset_)[13] = sg_base16_charset[( ((_u64_)&0x0000000000000F00) >> 8 )] ; \
		(_gus_id_offset_)[14] = sg_base16_charset[( ((_u64_)&0x00000000000000F0) >> 4 )] ; \
		(_gus_id_offset_)[15] = sg_base16_charset[( ((_u64_)&0x000000000000000F) )] ; \
	} \

int GUSGenerate( char seq_id[GUS_SEQ_ID_LENGTH+1] , uint32_t generate_options )
{
#if ( defined __unix ) || ( defined _AIX ) || ( defined __linux__ ) || ( defined __hpux )
	struct timeval	now_timeval ;
#endif
	uint64_t	now_u64 ;
	
	uint64_t	netaddr_le64 ;
	uint64_t	tid_le64 ;
	uint64_t	timestamp_le64 ;
	uint64_t	seqno_le64 ;
	
	uint64_t	gus_id_frame1_64bits ;
	uint64_t	gus_id_frame2_64bits ;
	
	int		nret = 0 ;
	
#if ( defined _WIN32 )
	if( is_call_WSAStartup == 0 )
	{
		WSADATA			wsd ;

		if( WSAStartup( MAKEWORD(2,2) , &wsd ) != 0 )
			return GUS_ERROR_WSASTARTUP;

		is_call_WSAStartup = 1 ;
	}
#endif

	if( netaddr == 0 )
	{
#if ( defined __unix ) || ( defined _AIX ) || ( defined __linux__ ) || ( defined __hpux )
		struct ifaddrs	*ifa1 = NULL ;
		struct ifaddrs	*ifa = NULL ;
		struct ifreq	ifr ;
		int		sock ;
		
		nret = getifaddrs( & ifa1 ) ;
		if( nret == -1 )
			return GUS_ERROR_GETIFADDRS;
		
		ifa = ifa1 ;
		while( ifa )
		{
			if( ! (ifa->ifa_flags&IFF_LOOPBACK) )
			{
				if( ifa->ifa_addr && ifa->ifa_addr->sa_family == AF_INET )
				{
					if( ! (generate_options&GUS_GENERATE_OPTION_USE_NCMAC) )
					{
						netaddr = ((struct sockaddr_in *)(ifa->ifa_addr))->sin_addr.s_addr ;
						netaddr |= 0x3FFF00000000 ;
						netaddr_is_ncmac = 0 ;
					}
					else
					{
						memset( & ifr , 0x00 , sizeof(struct ifreq) );
						strncpy( ifr.ifr_name , ifa->ifa_name , sizeof(ifr.ifr_name)-1 );
						sock = socket( AF_INET , SOCK_STREAM , 0 ) ;
						if( sock == -1 )
							return GUS_ERROR_SOCKET;
						
						nret = ioctl( sock , SIOCGIFHWADDR , & ifr ) ;
						if( nret == -1 )
							return GUS_ERROR_IOCTL;
						
						/*
						printf( "NAME[%s] MAC[%02X:%02X:%02X:%02X:%02X:%02X]\n"
							, ifa->ifa_name
							, (unsigned char)(ifr.ifr_hwaddr.sa_data[0])
							, (unsigned char)(ifr.ifr_hwaddr.sa_data[1])
							, (unsigned char)(ifr.ifr_hwaddr.sa_data[2])
							, (unsigned char)(ifr.ifr_hwaddr.sa_data[3])
							, (unsigned char)(ifr.ifr_hwaddr.sa_data[4])
							, (unsigned char)(ifr.ifr_hwaddr.sa_data[5]) );
						*/
						
						netaddr = ifr.ifr_hwaddr.sa_data[0] >> 2 ; /* 去掉全局标志和单播标志 */
						netaddr = ( netaddr << 8 ) + (unsigned char)(ifr.ifr_hwaddr.sa_data[1]) ;
						netaddr = ( netaddr << 8 ) + (unsigned char)(ifr.ifr_hwaddr.sa_data[2]) ;
						netaddr = ( netaddr << 8 ) + (unsigned char)(ifr.ifr_hwaddr.sa_data[3]) ;
						netaddr = ( netaddr << 8 ) + (unsigned char)(ifr.ifr_hwaddr.sa_data[4]) ;
						netaddr = ( netaddr << 8 ) + (unsigned char)(ifr.ifr_hwaddr.sa_data[5]) ;
						netaddr_is_ncmac = 1 ;
					}
					
					break;
				}
			}
			
			ifa = ifa->ifa_next ;
		}
		freeifaddrs( ifa1 );
		if( ifa == NULL )
			return GUS_ERROR_GETIFADDRS_NOT_FOUND;
#elif ( defined _WIN32 )
		IP_ADAPTER_INFO		*pAdapterInfo = NULL ;
		IP_ADAPTER_INFO		*pAdapter = NULL ;
		ULONG			ulOutBufLen = sizeof(IP_ADAPTER_INFO) ;

		pAdapterInfo = (IP_ADAPTER_INFO *)malloc(sizeof(IP_ADAPTER_INFO)) ;
		if( pAdapterInfo == NULL )
			return GUS_ERROR_ALLOC;
		if( GetAdaptersInfo(pAdapterInfo , & ulOutBufLen ) == ERROR_BUFFER_OVERFLOW )
		{
			free( pAdapterInfo );
			pAdapterInfo = (IP_ADAPTER_INFO *)malloc(ulOutBufLen) ;
			if( pAdapterInfo == NULL )
				return GUS_ERROR_ALLOC;
		}
		if( ( GetAdaptersInfo( pAdapterInfo , & ulOutBufLen ) ) == NO_ERROR )
		{
			pAdapter = pAdapterInfo ;
			while( pAdapter )
			{
				if( pAdapter->Type != MIB_IF_TYPE_LOOPBACK )
				{
					if( ! (generate_options&GUS_GENERATE_OPTION_USE_NCMAC) )
					{
						SOCKADDR_IN	addr ;
						int		addrlen = sizeof(SOCKADDR) ;
						WSAStringToAddressA( pAdapter->IpAddressList.IpAddress.String , AF_INET , NULL , (SOCKADDR*) & addr , & addrlen );
						netaddr = (uint64_t)(addr.sin_addr.S_un.S_addr) ;
						netaddr |= 0x3FFF00000000 ;
						netaddr_is_ncmac = 0 ;
					}
					else
					{
						netaddr = pAdapter->Address[0] >> 2 ; /* 去掉全局标志和单播标志 */
						netaddr = ( netaddr << 8 ) + (unsigned char)(pAdapter->Address[1]) ;
						netaddr = ( netaddr << 8 ) + (unsigned char)(pAdapter->Address[2]) ;
						netaddr = ( netaddr << 8 ) + (unsigned char)(pAdapter->Address[3]) ;
						netaddr = ( netaddr << 8 ) + (unsigned char)(pAdapter->Address[4]) ;
						netaddr = ( netaddr << 8 ) + (unsigned char)(pAdapter->Address[5]) ;
						netaddr_is_ncmac = 1 ;
					}
					break;
				}

				pAdapter = pAdapter->Next ;
			}
		}
		free( pAdapterInfo );
#endif
	}
	
	if( tid == 0 )
	{
#if ( defined __unix ) || ( defined _AIX ) || ( defined __linux__ ) || ( defined __hpux )
		tid = syscall(SYS_gettid) ;
		if( tid == -1 )
		{
			tid = 0 ;
			return GUS_ERROR_SYSCALL;
		}
#elif ( defined _WIN32 )
		tid = GetCurrentThreadId() >> 2 ;
#endif
	}
	
_GOTO_NEXT_SECOND :
#if ( defined __unix ) || ( defined _AIX ) || ( defined __linux__ ) || ( defined __hpux )
	gettimeofday( & now_timeval , NULL );
	now_u64 = now_timeval.tv_sec ;
	now_u64 = (now_u64-1577854800)*1000000 + now_timeval.tv_usec ;
#elif ( defined _WIN32 )
	time_t		ttNow ;
	SYSTEMTIME	stNow ;
	ttNow = time( NULL ) ;
	GetLocalTime( & stNow );
	now_u64 = (ttNow-1577854800)*1000000 + stNow.wMilliseconds ;
#endif

	if( timestamp == 0 || now_u64 != timestamp )
	{
		seqno = 0 ;
		timestamp = now_u64 ;
	}
	else if( seqno == (2<<3)-1 )
	{
#if ( defined __unix ) || ( defined _AIX ) || ( defined __linux__ ) || ( defined __hpux )
		// useconds_t	us = ( 1000000 - now_timeval.tv_usec ) ;
		useconds_t	us = 1 ;
#if _DEBUG
		printf( "DEBUG - now_u64[%"PRIu64"] - usleep[%u]\n" , now_u64 , us );
#endif
		usleep( us );
		goto _GOTO_NEXT_SECOND;
#elif ( defined _WIN32 )
		// uint64_t	us = ( 1000000 - stNow.wMilliseconds ) ;
		uint64_t	us = 1 ;
#if __DEBUG
		printf( "DEBUG - now_u64[%I64u] - usleep[%I64u]\n" , now_u64 , us );
#endif
		USleep( us );
		goto _GOTO_NEXT_SECOND;
#endif
	}
	else
	{
		seqno++;
	}
	
#if ( defined __unix ) || ( defined _AIX ) || ( defined __linux__ ) || ( defined __hpux )
#if _DEBUG
	printf( "DEBUG - now_u64[%"PRIu64"] seqno[%"PRIu64"]\n" , now_u64 , seqno );
#endif
#elif ( defined _WIN32 )
#if __DEBUG
	printf( "DEBUG - now_u64[%I64u] seqno[%I64u]\n" , now_u64 , seqno );
#endif
#endif

	netaddr_le64 = htole64(netaddr) ;
	tid_le64 = htole64(tid) ;
	timestamp_le64 = htole64(timestamp) ;
	seqno_le64 = htole64(seqno) ;
	
	gus_id_frame1_64bits = ( ( netaddr_le64 & 0x3FFFFFFFFFFF ) << 18 ) | ( tid_le64 >> 12 ) ;
	gus_id_frame2_64bits = ( ( tid_le64 & 0xFFF ) << 52 ) | ( ( timestamp_le64 & 0x1FFFFFFFFFFFF ) << 3 ) | ( seqno_le64 & 0x7 ) ;
	
	GUS_GENERATE_64BITS( gus_id_frame1_64bits , seq_id )
	GUS_GENERATE_64BITS( gus_id_frame2_64bits , seq_id+16 )
	
	return GUS_SUCCESS;
}

static void GUSIpToString( uint64_t ip , char ip_str[GUS_IP_OR_NCMAC_STRING_LENGTH+1] )
{
#if ( defined __unix ) || ( defined _AIX ) || ( defined __linux__ ) || ( defined __hpux )
	uint32_t	ip_32bits = (uint32_t)ip ;

	memset( ip_str , 0x00 , GUS_IP_OR_NCMAC_STRING_LENGTH+1 );
	inet_ntop( AF_INET , & ip_32bits , ip_str , GUS_IP_OR_NCMAC_STRING_LENGTH+1 );
	ip_str[GUS_IP_OR_NCMAC_STRING_LENGTH] = '\0' ;
#elif ( defined _WIN32 )
	SOCKADDR_IN	ip_32bits ;
	DWORD		len = GUS_IP_OR_NCMAC_STRING_LENGTH+1 ;

	memset( & ip_32bits , 0x00 , sizeof(SOCKADDR_IN) );
	ip_32bits.sin_family = AF_INET ;
	ip_32bits.sin_addr.S_un.S_addr = (ULONG)ip ;
	memset( ip_str , 0x00 , GUS_IP_OR_NCMAC_STRING_LENGTH+1 );
	WSAAddressToStringA( (SOCKADDR*) & ip_32bits , sizeof(struct sockaddr_in) , NULL , ip_str , & len );
#endif
	
	return;
}

static void GUSNcMacToString( uint64_t ncmac , char ncmac_str[GUS_IP_OR_NCMAC_STRING_LENGTH+1] )
{
	int	i = 0 ;
	memset( ncmac_str , 0x00 , GUS_IP_OR_NCMAC_STRING_LENGTH+1 );
	ncmac_str[i++] = sg_base16_charset[(ncmac>>44)&0xF] ;
	ncmac_str[i++] = sg_base16_charset[(ncmac>>40)&0xF] ;
	ncmac_str[i++] = ':' ;
	ncmac_str[i++] = sg_base16_charset[(ncmac>>36)&0xF] ;
	ncmac_str[i++] = sg_base16_charset[(ncmac>>32)&0xF] ;
	ncmac_str[i++] = ':' ;
	ncmac_str[i++] = sg_base16_charset[(ncmac>>28)&0xF] ;
	ncmac_str[i++] = sg_base16_charset[(ncmac>>24)&0xF] ;
	ncmac_str[i++] = ':' ;
	ncmac_str[i++] = sg_base16_charset[(ncmac>>20)&0xF] ;
	ncmac_str[i++] = sg_base16_charset[(ncmac>>16)&0xF] ;
	ncmac_str[i++] = ':' ;
	ncmac_str[i++] = sg_base16_charset[(ncmac>>12)&0xF] ;
	ncmac_str[i++] = sg_base16_charset[(ncmac>>8)&0xF] ;
	ncmac_str[i++] = ':' ;
	ncmac_str[i++] = sg_base16_charset[(ncmac>>4)&0xF] ;
	ncmac_str[i++] = sg_base16_charset[ncmac&0xF] ;
	ncmac_str[GUS_IP_OR_NCMAC_STRING_LENGTH] = '\0' ;
	
	return;
}

static void GUSTimeStampToString( uint64_t timestamp , char time_str[GUS_TIMESTAMP_STRING_LENGTH+1] )
{
	time_t		tt ;
	struct tm	stime ;
	
	tt = (time_t)((timestamp/1000000+1577854800)) ;
	memset( & stime , 0x00 , sizeof(struct tm) );
#if ( defined __unix ) || ( defined _AIX ) || ( defined __linux__ ) || ( defined __hpux )
	localtime_r( & tt , & stime );
#elif ( defined _WIN32 )
	localtime_s( & stime , & tt );
#endif
	memset( time_str , 0x00 , GUS_TIMESTAMP_STRING_LENGTH+1 );
	strftime( time_str , GUS_TIMESTAMP_STRING_LENGTH+1 , "%Y-%m-%d %H:%M:%S" , & stime );
	snprintf( time_str+19 , GUS_TIMESTAMP_STRING_LENGTH+1-19 , ".%06"PRIu64 , timestamp%1000000 );
	time_str[GUS_TIMESTAMP_STRING_LENGTH] = '\0' ;
	
	return;
}

#define GUS_EXPLAIN_8BITS(_gus_id_offset_,_u8_) \
	{ \
		if( '0' <= (_gus_id_offset_)[0] && (_gus_id_offset_)[0] <= '9' ) \
			(_u8_) = (_gus_id_offset_)[0] - '0' ; \
		else if( 'A' <= (_gus_id_offset_)[0] && (_gus_id_offset_)[0] <= 'Z' ) \
			(_u8_) = (_gus_id_offset_)[0] - 'A' + 10 ; \
		else if( 'a' <= (_gus_id_offset_)[0] && (_gus_id_offset_)[0] <= 'z' ) \
			(_u8_) = (_gus_id_offset_)[0] - 'a' + 10 ; \
		else \
			return GUS_ERROR_SEQ_ID_INVALID; \
		\
		if( '0' <= (_gus_id_offset_)[1] && (_gus_id_offset_)[1] <= '9' ) \
			(_u8_) = ((_u8_)<<4) + (_gus_id_offset_)[1] - '0' ; \
		else if( 'A' <= (_gus_id_offset_)[1] && (_gus_id_offset_)[1] <= 'Z' ) \
			(_u8_) = ((_u8_)<<4) + (_gus_id_offset_)[1] - 'A' + 10 ; \
		else if( 'a' <= (_gus_id_offset_)[1] && (_gus_id_offset_)[1] <= 'z' ) \
			(_u8_) = ((_u8_)<<4) + (_gus_id_offset_)[1] - 'a' + 10 ; \
		else \
			return GUS_ERROR_SEQ_ID_INVALID; \
	} \

#define GUS_EXPLAIN_32BITS(_gus_id_offset_,_u32_) \
	{ \
		uint8_t	u8 ; \
		GUS_EXPLAIN_8BITS( (_gus_id_offset_) , u8 ) (_u32_) = u8 ; \
		GUS_EXPLAIN_8BITS( (_gus_id_offset_)+2 , u8 ) (_u32_) = ((_u32_)<<8) | u8 ; \
		GUS_EXPLAIN_8BITS( (_gus_id_offset_)+4 , u8 ) (_u32_) = ((_u32_)<<8) | u8 ; \
		GUS_EXPLAIN_8BITS( (_gus_id_offset_)+6 , u8 ) (_u32_) = ((_u32_)<<8) | u8 ; \
	} \

#define GUS_EXPLAIN_64BITS(_gus_id_offset_,_u64_) \
	{ \
		uint8_t	u8 ; \
		GUS_EXPLAIN_8BITS( (_gus_id_offset_) , u8 ) (_u64_) = u8 ; \
		GUS_EXPLAIN_8BITS( (_gus_id_offset_)+2 , u8 ) (_u64_) = ((_u64_)<<8) | u8 ; \
		GUS_EXPLAIN_8BITS( (_gus_id_offset_)+4 , u8 ) (_u64_) = ((_u64_)<<8) | u8 ; \
		GUS_EXPLAIN_8BITS( (_gus_id_offset_)+6 , u8 ) (_u64_) = ((_u64_)<<8) | u8 ; \
		GUS_EXPLAIN_8BITS( (_gus_id_offset_)+8 , u8 ) (_u64_) = ((_u64_)<<8) | u8 ; \
		GUS_EXPLAIN_8BITS( (_gus_id_offset_)+10 , u8 ) (_u64_) = ((_u64_)<<8) | u8 ; \
		GUS_EXPLAIN_8BITS( (_gus_id_offset_)+12 , u8 ) (_u64_) = ((_u64_)<<8) | u8 ; \
		GUS_EXPLAIN_8BITS( (_gus_id_offset_)+14 , u8 ) (_u64_) = ((_u64_)<<8) | u8 ; \
	} \

int GUSExplain( char seq_id[GUS_SEQ_ID_LENGTH+1]
		, unsigned char *p_netaddr_is_ncmac , char ip_or_ncmac_str[GUS_IP_OR_NCMAC_STRING_LENGTH+1]
		, uint64_t *p_tid
		, char time_str[GUS_TIMESTAMP_STRING_LENGTH+1]
		, uint64_t *p_seqno )
{
	uint64_t	gus_id_frame1_64bits ;
	uint64_t	gus_id_frame2_64bits ;
	
	uint64_t	netaddr_le64 ;
	uint64_t	tid_le64 ;
	uint64_t	timestamp_le64 ;
	uint64_t	seqno_le64 ;
	
	uint64_t	netaddr ;
	uint64_t	timestamp ;
	
#if ( defined _WIN32 )
	if( is_call_WSAStartup == 0 )
	{
		WSADATA			wsd ;

		if( WSAStartup( MAKEWORD(2,2) , &wsd ) != 0 )
			return GUS_ERROR_WSASTARTUP;

		is_call_WSAStartup = 1 ;
	}
#endif

	GUS_EXPLAIN_64BITS( seq_id , gus_id_frame1_64bits )
	GUS_EXPLAIN_64BITS( seq_id+16 , gus_id_frame2_64bits )
	
	netaddr_le64 = ( ( gus_id_frame1_64bits >> 18 ) & 0x3FFFFFFFFFFF ) ;
	tid_le64 = ( gus_id_frame1_64bits & 0x3FFFF ) | ( ( gus_id_frame2_64bits >> 52 ) & 0xFFF ) ;
	timestamp_le64 = ( ( gus_id_frame2_64bits >> 3 ) & 0x1FFFFFFFFFFFF ) ;
	seqno_le64 = ( gus_id_frame2_64bits & 0x7 ) ;
	
	if( ip_or_ncmac_str )
	{
		if( ( netaddr_le64 & 0x3FFF00000000 ) == 0x3FFF00000000 )
		{
			// int i = ENDIANNESS ;

			netaddr_le64 &= 0xFFFFFFFF ;
			netaddr = le64toh( netaddr_le64 ) ;
			GUSIpToString( netaddr , ip_or_ncmac_str );
			if( p_netaddr_is_ncmac )
				(*p_netaddr_is_ncmac) = 0 ;
		}
		else
		{
			netaddr_le64 = ( netaddr_le64 >> 40 << 42 ) | ( netaddr_le64 & 0xFFFFFFFFFF ) ;
			netaddr = le64toh( netaddr_le64 ) ; /* 补上了全局标志和单播标志 */
			GUSNcMacToString( netaddr , ip_or_ncmac_str );
			if( p_netaddr_is_ncmac )
				(*p_netaddr_is_ncmac) = 1 ;
			
		}
	}
	
	if( p_tid )
	{
		(*p_tid) = le64toh( tid_le64 ) ;
	}
	
	if( time_str )
	{
		timestamp = le64toh( timestamp_le64 ) ;
		GUSTimeStampToString( timestamp , time_str );
	}
	
	if( p_seqno )
	{
		(*p_seqno) = le64toh( seqno_le64 ) ;
	}
	
	return 0;
}

