/* Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "zobjects_system.h"

struct ZlangDirectProperty_system
{
	int		dummy ;
} ;

ZlangInvokeFunction ZlangInvokeFunction_system_SleepSeconds;
int ZlangInvokeFunction_system_SleepSeconds( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangObject			*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	int32_t					seconds ;
	
	CallRuntimeFunction_int_GetIntValue( rt , in1 , & seconds );
	
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "begin sleep(%"PRIi32") seconds" , seconds )
	sleep( (unsigned int)seconds );
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "end sleep(%"PRIi32") seconds" , seconds )
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_system_SleepMilliseconds;
int ZlangInvokeFunction_system_SleepMilliseconds( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangObject			*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	int32_t					milliseconds ;
	
	CallRuntimeFunction_int_GetIntValue( rt , in1 , & milliseconds );
	
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "begin usleep(%"PRIi32"*1000) milliseconds" , milliseconds )
	usleep( (useconds_t)(milliseconds*1000) );
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "end usleep(%"PRIi32"*1000) milliseconds" , milliseconds )
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_system_SleepMicroseconds;
int ZlangInvokeFunction_system_SleepMicroseconds( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangObject			*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	int32_t					microseconds ;
	
	CallRuntimeFunction_int_GetIntValue( rt , in1 , & microseconds );
	
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "begin usleep(%"PRIi32") microseconds" , microseconds )
	usleep( (useconds_t)microseconds );
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "end usleep(%"PRIi32") microseconds" , microseconds )
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_system_SleepNanoseconds;
int ZlangInvokeFunction_system_SleepNanoseconds( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangObject			*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	int32_t					nanoseconds ;
	struct timespec				ts ;
	
	CallRuntimeFunction_int_GetIntValue( rt , in1 , & nanoseconds );
	
	ts.tv_sec = nanoseconds / 1000000000 ;
	ts.tv_nsec = nanoseconds % 1000000000 ;
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "begin nanosleep(%ld.%09ld,NULL) microseconds" , (long)(ts.tv_sec) , ts.tv_nsec )
	nanosleep( & ts , NULL );
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "end nanosleep(%ld.%09ld,NULL) microseconds" , (long)(ts.tv_sec) , ts.tv_nsec )
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_system_GetEnvironmentVar_string;
int ZlangInvokeFunction_system_GetEnvironmentVar_string( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangObject			*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject			*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	char					*env_var_name = NULL ;
	char					*env_var_value = NULL ;
	
	CallRuntimeFunction_string_GetStringValue( rt , in1 , & env_var_name , NULL );
	
	env_var_value = getenv( env_var_name ) ;
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "[%s] = getenv( [%s] ) ;" , env_var_value , env_var_name )
	if( env_var_value )
	{
		CallRuntimeFunction_string_SetStringValue( rt , out1 , env_var_value , (int32_t)strlen(env_var_value) );
		return 0;
	}
	else
	{
		UnreferObject( rt , out1 );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_GETENV_VALUE_NULL );
	}
}

ZlangInvokeFunction ZlangInvokeFunction_system_SetEnvironmentVar_string_string;
int ZlangInvokeFunction_system_SetEnvironmentVar_string_string( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangObject			*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject			*in2 = GetInputParameterInLocalObjectStack(rt,2) ;
	struct ZlangObject			*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	char					*env_var_name = NULL ;
	char					*env_var_value = NULL ;
	int					nret = 0 ;
	
	CallRuntimeFunction_string_GetStringValue( rt , in1 , & env_var_name , NULL );
	CallRuntimeFunction_string_GetStringValue( rt , in2 , & env_var_value , NULL );
	
	nret = setenv( env_var_name , env_var_value , 1 ) ;
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "setenv( [%s] , [%s] , 1 ) ;" , env_var_name , env_var_value )
	if( nret )
	{
		CallRuntimeFunction_bool_SetBoolValue( rt , out1 , FALSE );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_SETENV_FAILED );
	}
	else
	{
		CallRuntimeFunction_bool_SetBoolValue( rt , out1 , TRUE );
		return 0;
	}
}

#if defined(__linux__)
ZlangInvokeFunction ZlangInvokeFunction_system_DeleteEnvironmentVar_string;
int ZlangInvokeFunction_system_DeleteEnvironmentVar_string( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangObject			*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject			*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	char					*env_var_name = NULL ;
	int					nret = 0 ;
	
	CallRuntimeFunction_string_GetStringValue( rt , in1 , & env_var_name , NULL );
	
	nret = unsetenv( env_var_name ) ;
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "unsetenv( [%s] ) ;" , env_var_name )
	if( nret )
	{
		CallRuntimeFunction_bool_SetBoolValue( rt , out1 , FALSE );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_UNSETENV_FAILED );
	}
	else
	{
		CallRuntimeFunction_bool_SetBoolValue( rt , out1 , TRUE );
		return 0;
	}
}
#endif

ZlangCreateDirectPropertyFunction ZlangCreateDirectProperty_system;
void *ZlangCreateDirectProperty_system( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_system	*system_prop = NULL ;
	
	system_prop = (struct ZlangDirectProperty_system *)ZLMALLOC( sizeof(struct ZlangDirectProperty_system) ) ;
	if( system_prop == NULL )
	{
		SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_ALLOC , "alloc memory for entity" )
		return NULL;
	}
	memset( system_prop , 0x00 , sizeof(struct ZlangDirectProperty_system) );
	
	return system_prop;
}

ZlangDestroyDirectPropertyFunction ZlangDestroyDirectProperty_system;
void ZlangDestroyDirectProperty_system( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_system	*system_direct_prop = GetObjectDirectProperty(obj) ;
	
	ZLFREE( system_direct_prop );
	
	return;
}

ZlangSummarizeDirectPropertySizeFunction ZlangSummarizeDirectPropertySize_system;
void ZlangSummarizeDirectPropertySize_system( struct ZlangRuntime *rt , struct ZlangObject *obj , size_t *summarized_obj_size , size_t *summarized_direct_prop_size )
{
	SUMMARIZE_SIZE( summarized_direct_prop_size , sizeof(struct ZlangDirectProperty_system) )
	return;
}

static struct ZlangDirectFunctions direct_funcs_system =
	{
		ZLANG_OBJECT_system , /* char *ancestor_name */
		
		ZlangCreateDirectProperty_system , /* ZlangCreateDirectPropertyFunction *create_entity_func */
		ZlangDestroyDirectProperty_system , /* ZlangDestroyDirectPropertyFunction *destroy_entity_func */
		
		NULL , /* ZlangFromCharPtrFunction *from_char_ptr_func */
		NULL , /* ZlangToStringFunction *to_string_func */
		NULL , /* ZlangFromDataPtrFunction *from_data_ptr_func */
		NULL , /* ZlangGetDataPtrFunction *get_data_ptr_func */
		
		NULL , /* ZlangOperatorFunction *oper_PLUS_func */
		NULL , /* ZlangOperatorFunction *oper_MINUS_func */
		NULL , /* ZlangOperatorFunction *oper_MUL_func */
		NULL , /* ZlangOperatorFunction *oper_DIV_func */
		NULL , /* ZlangOperatorFunction *oper_MOD_func */
		
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_NEGATIVE_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_NOT_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_BIT_REVERSE_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_PLUS_PLUS_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_MINUS_MINUS_func */
		
		NULL , /* ZlangCompareFunction *comp_EGUAL_func */
		NULL , /* ZlangCompareFunction *comp_NOTEGUAL_func */
		NULL , /* ZlangCompareFunction *comp_LT_func */
		NULL , /* ZlangCompareFunction *comp_LE_func */
		NULL , /* ZlangCompareFunction *comp_GT_func */
		NULL , /* ZlangCompareFunction *comp_GE_func */
		
		NULL , /* ZlangLogicFunction *logic_AND_func */
		NULL , /* ZlangLogicFunction *logic_OR_func */
		
		NULL , /* ZlangLogicFunction *bit_AND_func */
		NULL , /* ZlangLogicFunction *bit_XOR_func */
		NULL , /* ZlangLogicFunction *bit_OR_func */
		NULL , /* ZlangLogicFunction *bit_MOVELEFT_func */
		NULL , /* ZlangLogicFunction *bit_MOVERIGHT_func */
		
		ZlangSummarizeDirectPropertySize_system , /* ZlangSummarizeDirectPropertySizeFunction *summarize_direct_prop_size_func */
	} ;

ZlangImportObjectFunction ZlangImportObject_system;
struct ZlangObject *ZlangImportObject_system( struct ZlangRuntime *rt )
{
	struct ZlangObject	*obj = NULL ;
	struct ZlangFunction	*func = NULL ;
	int			nret = 0 ;
	
	nret = ImportObject( rt , & obj , ZLANG_OBJECT_system , & direct_funcs_system , sizeof(struct ZlangDirectFunctions) , NULL ) ;
	if( nret )
	{
		SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_LINK_FUNC_TO_ENTITY , "import object to global objects heap" )
		return NULL;
	}
	
	/* system.SleepSeconds() */
	func = AddFunctionAndParametersInObject( rt , obj , "SleepSeconds" , "SleepSeconds(int)" , ZlangInvokeFunction_system_SleepSeconds , ZLANG_OBJECT_void , ZLANG_OBJECT_int,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* system.SleepMilliseconds() */
	func = AddFunctionAndParametersInObject( rt , obj , "SleepMilliseconds" , "SleepMilliseconds(int)" , ZlangInvokeFunction_system_SleepMilliseconds , ZLANG_OBJECT_void , ZLANG_OBJECT_int,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* system.SleepMicroseconds() */
	func = AddFunctionAndParametersInObject( rt , obj , "SleepMicroseconds" , "SleepMicroseconds(int)" , ZlangInvokeFunction_system_SleepMicroseconds , ZLANG_OBJECT_void , ZLANG_OBJECT_int,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* system.SleepNanoseconds() */
	func = AddFunctionAndParametersInObject( rt , obj , "SleepNanoseconds" , "SleepNanoseconds(int)" , ZlangInvokeFunction_system_SleepNanoseconds , ZLANG_OBJECT_void , ZLANG_OBJECT_int,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* system.GetEnvironmentVar(string) */
	func = AddFunctionAndParametersInObject( rt , obj , "GetEnvironmentVar" , "GetEnvironmentVar(string)" , ZlangInvokeFunction_system_GetEnvironmentVar_string , ZLANG_OBJECT_string , ZLANG_OBJECT_string,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* system.SetEnvironmentVar(string,string) */
	func = AddFunctionAndParametersInObject( rt , obj , "SetEnvironmentVar" , "SetEnvironmentVar(string,string)" , ZlangInvokeFunction_system_SetEnvironmentVar_string_string , ZLANG_OBJECT_bool , ZLANG_OBJECT_string , ZLANG_OBJECT_string,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
#if defined(__linux__)
	/* system.DeleteEnvironmentVar(string) */
	func = AddFunctionAndParametersInObject( rt , obj , "DeleteEnvironmentVar" , "DeleteEnvironmentVar(string)" , ZlangInvokeFunction_system_DeleteEnvironmentVar_string , ZLANG_OBJECT_bool , ZLANG_OBJECT_string,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
#endif
	
	return obj ;
}

