/* Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef _H_LIBZLANG_SYSTEM_
#define _H_LIBZLANG_SYSTEM_

#include "zlang.h"

#if defined(__linux__)
#include <sys/time.h>
#elif defined(_WIN32)
#include <windows.h>
#endif

#define ZLANG_OBJECT_system	"system"

#define EXCEPTION_MESSAGE_GETENV_VALUE_NULL	"getenv value null"
#define EXCEPTION_MESSAGE_SETENV_FAILED		"setenv failed"
#define EXCEPTION_MESSAGE_UNSETENV_FAILED	"unsetenv failed"

DLLEXPORT ZlangImportObjectsFunction ZlangImportObjects;
int ZlangImportObjects( struct ZlangRuntime *rt );

#endif

