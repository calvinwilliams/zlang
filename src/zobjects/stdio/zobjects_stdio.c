/* Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "zobjects_stdio.h"

ZlangImportObjectFunction ZlangImportObject_stdin;
ZlangImportObjectFunction ZlangImportObject_stdout;
ZlangImportObjectFunction ZlangImportObject_stderr;
ZlangImportObjectFunction ZlangImportObject_stdfile;
ZlangImportObjectFunction ZlangImportObject_execmd;

#include "charset_GB18030.c"
#include "charset_UTF8.c"

int ZlangImportObjects( struct ZlangRuntime *rt )
{
	struct ZlangObject	*obj = NULL ;
	
	obj = ZlangImportObject_stdin( rt ) ;
	if( obj == NULL )
		return 1;
	
	obj = ZlangImportObject_stdout( rt ) ;
	if( obj == NULL )
		return 1;
	
	obj = ZlangImportObject_stderr( rt ) ;
	if( obj == NULL )
		return 1;
	
	obj = ZlangImportObject_stdfile( rt ) ;
	if( obj == NULL )
		return 1;
	
#if defined(__linux__)
	obj = ZlangImportObject_execmd( rt ) ;
	if( obj == NULL )
		return 1;
#endif

	ImportCharsetAlias( rt , g_zlang_charset_aliases_GB18030 );
	ImportCharsetAlias( rt , g_zlang_charset_aliases_UTF8 );
	
	return 0;
}

