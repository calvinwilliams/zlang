/* Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "zobjects_stdio.h"

struct ZlangDirectProperty_stdin
{
	char		dummy ;
} ;

static TLS char	_g_zlang_string_buffer[ 4096 ] = "" ;

ZlangInvokeFunction ZlangInvokeFunction_stdin_Scan_string;
int ZlangInvokeFunction_stdin_Scan_string( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	FSCANF_STRING_AND_SET_LENGTH_OUTPUT( stdin )
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_stdin_Scan_short;
int ZlangInvokeFunction_stdin_Scan_short( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	FSCANF_SHORT_AND_SET_LENGTH_OUTPUT( stdin )
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_stdin_Scan_ushort;
int ZlangInvokeFunction_stdin_Scan_ushort( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	FSCANF_USHORT_AND_SET_LENGTH_OUTPUT( stdin )
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_stdin_Scan_int;
int ZlangInvokeFunction_stdin_Scan_int( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	FSCANF_INT_AND_SET_LENGTH_OUTPUT( stdin )
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_stdin_Scan_uint;
int ZlangInvokeFunction_stdin_Scan_uint( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	FSCANF_UINT_AND_SET_LENGTH_OUTPUT( stdin )
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_stdin_Scan_long;
int ZlangInvokeFunction_stdin_Scan_long( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	FSCANF_LONG_AND_SET_LENGTH_OUTPUT( stdin )
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_stdin_Scan_ulong;
int ZlangInvokeFunction_stdin_Scan_ulong( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	FSCANF_ULONG_AND_SET_LENGTH_OUTPUT( stdin )
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_stdin_Scan_float;
int ZlangInvokeFunction_stdin_Scan_float( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	FSCANF_FLOAT_AND_SET_LENGTH_OUTPUT( stdin )
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_stdin_Scan_double;
int ZlangInvokeFunction_stdin_Scan_double( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	FSCANF_DOUBLE_AND_SET_LENGTH_OUTPUT( stdin )
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_stdin_Scanln_string;
int ZlangInvokeFunction_stdin_Scanln_string( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	FGETS_STRING_AND_SET_LENGTH_OUTPUT( stdin )
	return 0;
}

ZlangCreateDirectPropertyFunction ZlangCreateDirectProperty_stdin;
void *ZlangCreateDirectProperty_stdin( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_stdin	*stdin_prop = NULL ;
	
	stdin_prop = (struct ZlangDirectProperty_stdin *)ZLMALLOC( sizeof(struct ZlangDirectProperty_stdin) ) ;
	if( stdin_prop == NULL )
	{
		SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_ALLOC , "alloc memory for entity" )
		return NULL;
	}
	memset( stdin_prop , 0x00 , sizeof(struct ZlangDirectProperty_stdin) );
	
	return stdin_prop;
}

ZlangDestroyDirectPropertyFunction ZlangDestroyDirectProperty_stdin;
void ZlangDestroyDirectProperty_stdin( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_stdin	*stdin_direct_prop = GetObjectDirectProperty(obj) ;
	
	ZLFREE( stdin_direct_prop );
	
	return;
}

ZlangSummarizeDirectPropertySizeFunction ZlangSummarizeDirectPropertySize_stdin;
void ZlangSummarizeDirectPropertySize_stdin( struct ZlangRuntime *rt , struct ZlangObject *obj , size_t *summarized_obj_size , size_t *summarized_direct_prop_size )
{
	SUMMARIZE_SIZE( summarized_direct_prop_size , sizeof(struct ZlangDirectProperty_stdin) )
	return;
}

static struct ZlangDirectFunctions direct_funcs_stdin =
	{
		ZLANG_OBJECT_stdin , /* char *ancestor_name */
		
		ZlangCreateDirectProperty_stdin , /* ZlangCreateDirectPropertyFunction *create_entity_func */
		ZlangDestroyDirectProperty_stdin , /* ZlangDestroyDirectPropertyFunction *destroy_entity_func */
		
		NULL , /* ZlangFromCharPtrFunction *from_char_ptr_func */
		NULL , /* ZlangToStringFunction *to_string_func */
		NULL , /* ZlangFromDataPtrFunction *from_data_ptr_func */
		NULL , /* ZlangGetDataPtrFunction *get_data_ptr_func */
		
		NULL , /* ZlangOperatorFunction *oper_PLUS_func */
		NULL , /* ZlangOperatorFunction *oper_MINUS_func */
		NULL , /* ZlangOperatorFunction *oper_MUL_func */
		NULL , /* ZlangOperatorFunction *oper_DIV_func */
		NULL , /* ZlangOperatorFunction *oper_MOD_func */
		
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_NEGATIVE_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_NOT_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_BIT_REVERSE_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_PLUS_PLUS_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_MINUS_MINUS_func */
		
		NULL , /* ZlangCompareFunction *comp_EGUAL_func */
		NULL , /* ZlangCompareFunction *comp_NOTEGUAL_func */
		NULL , /* ZlangCompareFunction *comp_LT_func */
		NULL , /* ZlangCompareFunction *comp_LE_func */
		NULL , /* ZlangCompareFunction *comp_GT_func */
		NULL , /* ZlangCompareFunction *comp_GE_func */
		
		NULL , /* ZlangLogicFunction *logic_AND_func */
		NULL , /* ZlangLogicFunction *logic_OR_func */
		
		NULL , /* ZlangLogicFunction *bit_AND_func */
		NULL , /* ZlangLogicFunction *bit_XOR_func */
		NULL , /* ZlangLogicFunction *bit_OR_func */
		NULL , /* ZlangLogicFunction *bit_MOVELEFT_func */
		NULL , /* ZlangLogicFunction *bit_MOVERIGHT_func */
		
		ZlangSummarizeDirectPropertySize_stdin , /* ZlangSummarizeDirectPropertySizeFunction *summarize_direct_prop_size_func */
	} ;

ZlangImportObjectFunction ZlangImportObject_stdin;
struct ZlangObject *ZlangImportObject_stdin( struct ZlangRuntime *rt )
{
	struct ZlangObject	*obj = NULL ;
	struct ZlangFunction	*func = NULL ;
	int			nret = 0 ;
	
	nret = ImportObject( rt , & obj , ZLANG_OBJECT_stdin , & direct_funcs_stdin , sizeof(struct ZlangDirectFunctions) , NULL ) ;
	if( nret )
	{
		SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_LINK_FUNC_TO_ENTITY , "import object to global objects heap" )
		return NULL;
	}
	
	/* stdin.Scan(string) */
	func = AddFunctionAndParametersInObject( rt , obj , "Scan" , "Scan(string)" , ZlangInvokeFunction_stdin_Scan_string , ZLANG_OBJECT_int , ZLANG_OBJECT_string,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* stdin.Scan(short) */
	func = AddFunctionAndParametersInObject( rt , obj , "Scan" , "Scan(short)" , ZlangInvokeFunction_stdin_Scan_short , ZLANG_OBJECT_int , ZLANG_OBJECT_short,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* stdin.Scan(ushort) */
	func = AddFunctionAndParametersInObject( rt , obj , "Scan" , "Scan(ushort)" , ZlangInvokeFunction_stdin_Scan_ushort , ZLANG_OBJECT_int , ZLANG_OBJECT_ushort,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* stdin.Scan(int) */
	func = AddFunctionAndParametersInObject( rt , obj , "Scan" , "Scan(int)" , ZlangInvokeFunction_stdin_Scan_int , ZLANG_OBJECT_int , ZLANG_OBJECT_int,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* stdin.Scan(uint) */
	func = AddFunctionAndParametersInObject( rt , obj , "Scan" , "Scan(uint)" , ZlangInvokeFunction_stdin_Scan_uint , ZLANG_OBJECT_int , ZLANG_OBJECT_uint,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* stdin.Scan(long) */
	func = AddFunctionAndParametersInObject( rt , obj , "Scan" , "Scan(long)" , ZlangInvokeFunction_stdin_Scan_long , ZLANG_OBJECT_int , ZLANG_OBJECT_long,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* stdin.Scan(ulong) */
	func = AddFunctionAndParametersInObject( rt , obj , "Scan" , "Scan(ulong)" , ZlangInvokeFunction_stdin_Scan_ulong , ZLANG_OBJECT_int , ZLANG_OBJECT_ulong,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* stdin.Scan(float) */
	func = AddFunctionAndParametersInObject( rt , obj , "Scan" , "Scan(float)" , ZlangInvokeFunction_stdin_Scan_float , ZLANG_OBJECT_int , ZLANG_OBJECT_float,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* stdin.Scan(double) */
	func = AddFunctionAndParametersInObject( rt , obj , "Scan" , "Scan(double)" , ZlangInvokeFunction_stdin_Scan_double , ZLANG_OBJECT_int , ZLANG_OBJECT_double,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* stdin.Scanln(string) */
	func = AddFunctionAndParametersInObject( rt , obj , "Scanln" , "Scanln(string)" , ZlangInvokeFunction_stdin_Scanln_string , ZLANG_OBJECT_int , ZLANG_OBJECT_string,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	return obj ;
}

