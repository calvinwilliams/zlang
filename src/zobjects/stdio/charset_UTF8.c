﻿/* Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

static struct ZlangCharsetAlias		g_zlang_charset_aliases_UTF8[] = {
		{ ZLANG_CHARSET_UTF8 , "标准输入" , "stdin" } ,
		{ ZLANG_CHARSET_UTF8 , "读入" , "Scan" } ,
		{ ZLANG_CHARSET_UTF8 , "读入行" , "Scanln" } ,
		{ ZLANG_CHARSET_UTF8 , "标准输出" , "stdout" } ,
		{ ZLANG_CHARSET_UTF8 , "显示" , "Print" } ,
		{ ZLANG_CHARSET_UTF8 , "显示并换行" , "Println" } ,
		{ ZLANG_CHARSET_UTF8 , "格式化显示" , "FormatPrint" } ,
                { ZLANG_CHARSET_UTF8 , "格式化显示并换行" , "FormatPrintln" } ,
		{ ZLANG_CHARSET_UTF8 , "标准错误" , "stderr" } ,
		{ ZLANG_CHARSET_UTF8 , "标准文件" , "stdfile" } ,
		{ ZLANG_CHARSET_UTF8 , "打开" , "Open" } ,
		{ ZLANG_CHARSET_UTF8 , "关闭" , "Close" } ,
		{ ZLANG_CHARSET_UTF8 , "读" , "Read" } ,
		{ ZLANG_CHARSET_UTF8 , "写" , "Write" } ,
		{ ZLANG_CHARSET_UTF8 , "读文件到字符串" , "ReadFileToString" } ,
		{ ZLANG_CHARSET_UTF8 , "外部命令" , "execmd" } ,
		{ ZLANG_CHARSET_UTF8 , "关闭写" , "CloseWrite" } ,
		{ 0 , NULL , NULL } ,
	} ;

