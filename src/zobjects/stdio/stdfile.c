/* Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "zobjects_stdio.h"

struct ZlangDirectProperty_stdfile
{
	FILE		*fp ;
} ;

ZlangInvokeFunction ZlangInvokeFunction_stdfile_Open_string_string;
int ZlangInvokeFunction_stdfile_Open_string_string( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_stdfile	*direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject			*in2 = GetInputParameterInLocalObjectStack(rt,2) ;
	struct ZlangObject			*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	char					*path = NULL ;
	char					*mode = NULL ;
	int32_t					errno_return ;
	
	if( direct_prop->fp )
		return ThrowFatalException( rt , ZLANG_FATAL_INVOKE_METHOD_RETURN , EXCEPTION_MESSAGE_GENERAL_ERROR );
	
	GetDataPtr( rt , in1 , (void**) & path , NULL );
	GetDataPtr( rt , in2 , (void**) & mode , NULL );
	direct_prop->fp = fopen( path , mode ) ;
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "fp[%p] = fopen( \"%s\" , \"%s\" ) ;" , direct_prop->fp , path , mode )
	if( direct_prop->fp == NULL )
	{
		errno_return = errno ;
	}
	else
	{
		errno_return = 0 ;
	}
	FromDataPtr( rt , out1 , (void**) & errno_return , sizeof(int32_t) );
	if( errno_return )
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_OPEN_FILE_FAILED );
	else
		return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_stdfile_Close;
int ZlangInvokeFunction_stdfile_Close( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_stdfile	*direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	int32_t					fclose_return ;
	
	if( direct_prop->fp == NULL )
		return ThrowFatalException( rt , ZLANG_FATAL_INVOKE_METHOD_RETURN , EXCEPTION_MESSAGE_GENERAL_ERROR );
	
	fclose_return = fclose( direct_prop->fp ) ;
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "[%d] = fclose( \"%p\" ) ;" , fclose_return , direct_prop->fp )
	FromDataPtr( rt , out1 , (void**) & fclose_return , sizeof(int32_t) );
	
	direct_prop->fp = NULL ;
	
	return 0;
}

static TLS char	_g_zlang_string_buffer[ 4096 ] = "" ;

ZlangInvokeFunction ZlangInvokeFunction_stdfile_Scan_string;
int ZlangInvokeFunction_stdfile_Scan_string( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_stdfile	*direct_prop = GetObjectDirectProperty(obj) ;
	
	if( direct_prop->fp == NULL )
		return ThrowFatalException( rt , ZLANG_FATAL_INVOKE_METHOD_RETURN , EXCEPTION_MESSAGE_GENERAL_ERROR );
	
	FSCANF_STRING_AND_SET_LENGTH_OUTPUT( direct_prop->fp )
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_stdfile_Scan_short;
int ZlangInvokeFunction_stdfile_Scan_short( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_stdfile	*direct_prop = GetObjectDirectProperty(obj) ;
	
	if( direct_prop->fp == NULL )
		return ThrowFatalException( rt , ZLANG_FATAL_INVOKE_METHOD_RETURN , EXCEPTION_MESSAGE_GENERAL_ERROR );
	
	FSCANF_SHORT_AND_SET_LENGTH_OUTPUT( direct_prop->fp )
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_stdfile_Scan_ushort;
int ZlangInvokeFunction_stdfile_Scan_ushort( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_stdfile	*direct_prop = GetObjectDirectProperty(obj) ;
	
	if( direct_prop->fp == NULL )
		return ThrowFatalException( rt , ZLANG_FATAL_INVOKE_METHOD_RETURN , EXCEPTION_MESSAGE_GENERAL_ERROR );
	
	FSCANF_USHORT_AND_SET_LENGTH_OUTPUT( direct_prop->fp )
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_stdfile_Scan_int;
int ZlangInvokeFunction_stdfile_Scan_int( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_stdfile	*direct_prop = GetObjectDirectProperty(obj) ;
	
	if( direct_prop->fp == NULL )
		return ThrowFatalException( rt , ZLANG_FATAL_INVOKE_METHOD_RETURN , EXCEPTION_MESSAGE_GENERAL_ERROR );
	
	FSCANF_INT_AND_SET_LENGTH_OUTPUT( direct_prop->fp )
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_stdfile_Scan_uint;
int ZlangInvokeFunction_stdfile_Scan_uint( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_stdfile	*direct_prop = GetObjectDirectProperty(obj) ;
	
	if( direct_prop->fp == NULL )
		return ThrowFatalException( rt , ZLANG_FATAL_INVOKE_METHOD_RETURN , EXCEPTION_MESSAGE_GENERAL_ERROR );
	
	FSCANF_UINT_AND_SET_LENGTH_OUTPUT( direct_prop->fp )
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_stdfile_Scan_long;
int ZlangInvokeFunction_stdfile_Scan_long( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_stdfile	*direct_prop = GetObjectDirectProperty(obj) ;
	
	if( direct_prop->fp == NULL )
		return ThrowFatalException( rt , ZLANG_FATAL_INVOKE_METHOD_RETURN , EXCEPTION_MESSAGE_GENERAL_ERROR );
	
	FSCANF_LONG_AND_SET_LENGTH_OUTPUT( direct_prop->fp )
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_stdfile_Scan_ulong;
int ZlangInvokeFunction_stdfile_Scan_ulong( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_stdfile	*direct_prop = GetObjectDirectProperty(obj) ;
	
	if( direct_prop->fp == NULL )
		return ThrowFatalException( rt , ZLANG_FATAL_INVOKE_METHOD_RETURN , EXCEPTION_MESSAGE_GENERAL_ERROR );
	
	FSCANF_ULONG_AND_SET_LENGTH_OUTPUT( direct_prop->fp )
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_stdfile_Scan_float;
int ZlangInvokeFunction_stdfile_Scan_float( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_stdfile	*direct_prop = GetObjectDirectProperty(obj) ;
	
	if( direct_prop->fp == NULL )
		return ThrowFatalException( rt , ZLANG_FATAL_INVOKE_METHOD_RETURN , EXCEPTION_MESSAGE_GENERAL_ERROR );
	
	FSCANF_FLOAT_AND_SET_LENGTH_OUTPUT( direct_prop->fp )
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_stdfile_Scan_double;
int ZlangInvokeFunction_stdfile_Scan_double( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_stdfile	*direct_prop = GetObjectDirectProperty(obj) ;
	
	if( direct_prop->fp == NULL )
		return ThrowFatalException( rt , ZLANG_FATAL_INVOKE_METHOD_RETURN , EXCEPTION_MESSAGE_GENERAL_ERROR );
	
	FSCANF_DOUBLE_AND_SET_LENGTH_OUTPUT( direct_prop->fp )
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_stdfile_Scanln_string;
int ZlangInvokeFunction_stdfile_Scanln_string( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_stdfile	*direct_prop = GetObjectDirectProperty(obj) ;
	
	if( direct_prop->fp == NULL )
		return ThrowFatalException( rt , ZLANG_FATAL_INVOKE_METHOD_RETURN , EXCEPTION_MESSAGE_GENERAL_ERROR );
	
	FGETS_STRING_AND_SET_LENGTH_OUTPUT( direct_prop->fp )
	return 0;
}

static int Print( struct ZlangRuntime *rt , struct ZlangObject *obj , struct ZlangObject *str_obj , struct ZlangObject *str_len_obj )
{
	struct ZlangDirectProperty_stdfile	*direct_prop = GetObjectDirectProperty(obj) ;
	char					*data = NULL ;
	int32_t					data_len ;
	
	if( direct_prop->fp == NULL )
		return ThrowFatalException( rt , ZLANG_FATAL_INVOKE_METHOD_RETURN , EXCEPTION_MESSAGE_GENERAL_ERROR );
	
	GetDataPtr( rt , str_obj , (void**) & data , & data_len );
	if( data == NULL )
		fprintf( direct_prop->fp , "%s" , ZLANG_null_STRING );
	else
		fprintf( direct_prop->fp , "%.*s" , data_len,data );
	
	FromDataPtr( rt , str_len_obj , & data_len , sizeof(int32_t) );
	
	return 0;
}

static int Println( struct ZlangRuntime *rt , struct ZlangObject *obj , struct ZlangObject *str_obj , struct ZlangObject *str_len_obj )
{
	struct ZlangDirectProperty_stdfile	*direct_prop = GetObjectDirectProperty(obj) ;
	int					nret = 0 ;
	
	nret = Print( rt , obj , str_obj , str_len_obj ) ;
	if( nret )
		return ThrowFatalException( rt , nret , EXCEPTION_MESSAGE_GENERAL_ERROR );
	
	fprintf( direct_prop->fp , NEWLINE );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_stdfile_Print_string;
int ZlangInvokeFunction_stdfile_Print_string( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangObject	*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject	*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	int			nret = 0 ;
	
	nret = Print( rt , obj , in1 , out1 ) ;
	if( nret )
		return ThrowFatalException( rt , nret , EXCEPTION_MESSAGE_GENERAL_ERROR );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_stdfile_Println_string;
int ZlangInvokeFunction_stdfile_Println_string( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangObject	*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject	*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	int			nret = 0 ;
	
	nret = Println( rt , obj , in1 , out1 ) ;
	if( nret )
		return ThrowFatalException( rt , nret , EXCEPTION_MESSAGE_GENERAL_ERROR );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_stdfile_Print_basetype;
int ZlangInvokeFunction_stdfile_Print_basetype( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangObject	*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject	*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject	*tostr = NULL ;
	int			nret = 0 ;
	
	nret = ToString( rt , in1 , & tostr ) ;
	if( nret )
		return ThrowFatalException( rt , nret , EXCEPTION_MESSAGE_GENERAL_ERROR );
	
	nret = Print( rt , obj , tostr , out1 ) ;
	if( nret )
		return ThrowFatalException( rt , nret , EXCEPTION_MESSAGE_GENERAL_ERROR );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_stdfile_Println_basetype;
int ZlangInvokeFunction_stdfile_Println_basetype( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangObject	*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject	*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject	*tostr = NULL ;
	int			nret = 0 ;
	
	nret = ToString( rt , in1 , & tostr ) ;
	if( nret )
		return ThrowFatalException( rt , nret , EXCEPTION_MESSAGE_GENERAL_ERROR );
	
	nret = Println( rt , obj , tostr , out1 ) ;
	if( nret )
		return ThrowFatalException( rt , nret , EXCEPTION_MESSAGE_GENERAL_ERROR );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_stdfile_Print_null;
int ZlangInvokeFunction_stdfile_Print_null( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangObject	*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject	*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	int			nret = 0 ;
	
	nret = Print( rt , obj , in1 , out1 ) ;
	if( nret )
		return ThrowFatalException( rt , nret , EXCEPTION_MESSAGE_GENERAL_ERROR );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_stdfile_Println_null;
int ZlangInvokeFunction_stdfile_Println_null( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangObject	*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject	*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	int			nret = 0 ;
	
	nret = Println( rt , obj , in1 , out1 ) ;
	if( nret )
		return ThrowFatalException( rt , nret , EXCEPTION_MESSAGE_GENERAL_ERROR );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_stdfile_FormatPrint_vargs;
int ZlangInvokeFunction_stdfile_FormatPrint_vargs( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_stdfile	*direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*buf_obj = NULL ;
	char					*str = NULL ;
	int32_t					str_len ;
	
	int					nret = 0 ;
	
	if( direct_prop->fp == NULL )
		return ThrowFatalException( rt , nret , EXCEPTION_MESSAGE_GENERAL_ERROR );
	
	buf_obj = CloneStringObjectInTmpStack( rt , NULL ) ;
	if( buf_obj == NULL )
		return ThrowFatalException( rt , nret , EXCEPTION_MESSAGE_GENERAL_ERROR );
	
	nret = CallRuntimeFunction_string_AppendFormatFromArgsStack( rt , buf_obj ) ;
	if( nret )
		return ThrowFatalException( rt , nret , EXCEPTION_MESSAGE_GENERAL_ERROR );
	
	GetDataPtr( rt , buf_obj , (void**) & str , & str_len );
	
	fprintf( direct_prop->fp , "%.*s" , str_len,str );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_stdfile_FormatPrintln_vargs;
int ZlangInvokeFunction_stdfile_FormatPrintln_vargs( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_stdfile	*direct_prop = GetObjectDirectProperty(obj) ;
	int					nret = 0 ;
	
	nret = ZlangInvokeFunction_stdfile_FormatPrint_vargs( rt , obj ) ;
	if( nret )
		return nret;
	
	fprintf( direct_prop->fp , NEWLINE );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_stdfile_Write_basetype;
int ZlangInvokeFunction_stdfile_Write_basetype( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_stdfile	*direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject			*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	int16_t					*s = NULL ;
	uint16_t				*us = NULL ;
	int32_t					*i = NULL ;
	uint32_t				*ui = NULL ;
	int64_t					*l = NULL ;
	uint64_t				*ul = NULL ;
	float					*f = NULL ;
	double					*d = NULL ;
	unsigned char				*b = NULL ;
	int32_t					nret = 0 ;
	
	if( direct_prop->fp == NULL )
		return ThrowFatalException( rt , ZLANG_FATAL_INVOKE_METHOD_RETURN , EXCEPTION_MESSAGE_GENERAL_ERROR );
	
	if( IsTypeOf(rt,in1,GetShortObjectInRuntimeObjectsHeap(rt)) )
	{
		GetDataPtr( rt , in1 , (void**) & s , NULL ) ;
		nret = fwrite( (void*) s , sizeof(int16_t) , 1 ,direct_prop->fp );
	}
	else if( IsTypeOf(rt,in1,GetUShortObjectInRuntimeObjectsHeap(rt)) )
	{
		GetDataPtr( rt , in1 , (void**) & us , NULL ) ;
		nret = fwrite( (void*) us , sizeof(uint16_t) , 1 ,direct_prop->fp );
	}
	else if( IsTypeOf(rt,in1,GetIntObjectInRuntimeObjectsHeap(rt)) )
	{
		GetDataPtr( rt , in1 , (void**) & i , NULL ) ;
		nret = fwrite( (void*) i , sizeof(int32_t) , 1 ,direct_prop->fp );
	}
	else if( IsTypeOf(rt,in1,GetUIntObjectInRuntimeObjectsHeap(rt)) )
	{
		GetDataPtr( rt , in1 , (void**) & ui , NULL ) ;
		nret = fwrite( (void*) ui , sizeof(uint32_t) , 1 ,direct_prop->fp );
	}
	else if( IsTypeOf(rt,in1,GetLongObjectInRuntimeObjectsHeap(rt)) )
	{
		GetDataPtr( rt , in1 , (void**) & l , NULL ) ;
		nret = fwrite( (void*) l , sizeof(int64_t) , 1 ,direct_prop->fp );
	}
	else if( IsTypeOf(rt,in1,GetULongObjectInRuntimeObjectsHeap(rt)) )
	{
		GetDataPtr( rt , in1 , (void**) & ul , NULL ) ;
		nret = fwrite( (void*) ul , sizeof(uint64_t) , 1 ,direct_prop->fp );
	}
	else if( IsTypeOf(rt,in1,GetFloatObjectInRuntimeObjectsHeap(rt)) )
	{
		GetDataPtr( rt , in1 , (void**) & f , NULL ) ;
		nret = fwrite( (void*) f , sizeof(float) , 1 ,direct_prop->fp );
	}
	else if( IsTypeOf(rt,in1,GetDoubleObjectInRuntimeObjectsHeap(rt)) )
	{
		GetDataPtr( rt , in1 , (void**) & d , NULL ) ;
		nret = fwrite( (void*) d , sizeof(double) , 1 ,direct_prop->fp );
	}
	else if( IsTypeOf(rt,in1,GetBoolObjectInRuntimeObjectsHeap(rt)) )
	{
		GetDataPtr( rt , in1 , (void**) & b , NULL ) ;
		nret = fwrite( (void*) b , sizeof(unsigned char) , 1 ,direct_prop->fp );
	}
	else
	{
		SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_OBJECT_TYPE_NOT_MATCHED , "parameter type not matched" )
		return ThrowFatalException( rt , ZLANG_FATAL_INVOKE_METHOD_RETURN , EXCEPTION_MESSAGE_GENERAL_ERROR );
	}
	
	CallRuntimeFunction_int_SetIntValue( rt , out1 , nret );
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_stdfile_Read_basetype;
int ZlangInvokeFunction_stdfile_Read_basetype( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_stdfile	*direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject			*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	int16_t					s ;
	uint16_t				us ;
	int32_t					i ;
	uint32_t				ui ;
	int64_t					l ;
	uint64_t				ul ;
	float					f ;
	double					d ;
	unsigned char				b ;
	int32_t					nret = 0 ;
	
	if( direct_prop->fp == NULL )
		return ThrowFatalException( rt , ZLANG_FATAL_INVOKE_METHOD_RETURN , EXCEPTION_MESSAGE_GENERAL_ERROR );
	
	if( IsTypeOf(rt,in1,GetShortObjectInRuntimeObjectsHeap(rt)) )
	{
		nret = fread( (void*) & s , sizeof(int16_t) , 1 ,direct_prop->fp );
		FromDataPtr( rt , in1 , (void*) & s , sizeof(int16_t) ) ;
	}
	else if( IsTypeOf(rt,in1,GetUShortObjectInRuntimeObjectsHeap(rt)) )
	{
		nret = fread( (void*) & us , sizeof(uint16_t) , 1 ,direct_prop->fp );
		FromDataPtr( rt , in1 , (void*) & us , sizeof(uint16_t) ) ;
	}
	else if( IsTypeOf(rt,in1,GetIntObjectInRuntimeObjectsHeap(rt)) )
	{
		nret = fread( (void*) & i , sizeof(int32_t) , 1 ,direct_prop->fp );
		FromDataPtr( rt , in1 , (void*) & i , sizeof(int32_t) ) ;
	}
	else if( IsTypeOf(rt,in1,GetUIntObjectInRuntimeObjectsHeap(rt)) )
	{
		nret = fread( (void*) & ui , sizeof(uint32_t) , 1 ,direct_prop->fp );
		FromDataPtr( rt , in1 , (void*) & ui , sizeof(uint32_t) ) ;
	}
	else if( IsTypeOf(rt,in1,GetLongObjectInRuntimeObjectsHeap(rt)) )
	{
		nret = fread( (void*) & l , sizeof(int64_t) , 1 ,direct_prop->fp );
		FromDataPtr( rt , in1 , (void*) & l , sizeof(int64_t) ) ;
	}
	else if( IsTypeOf(rt,in1,GetULongObjectInRuntimeObjectsHeap(rt)) )
	{
		nret = fread( (void*) & ul , sizeof(uint64_t) , 1 ,direct_prop->fp );
		FromDataPtr( rt , in1 , (void*) & ul , sizeof(uint64_t) ) ;
	}
	else if( IsTypeOf(rt,in1,GetFloatObjectInRuntimeObjectsHeap(rt)) )
	{
		nret = fread( (void*) & f , sizeof(float) , 1 ,direct_prop->fp );
		FromDataPtr( rt , in1 , (void*) & f , sizeof(float) ) ;
	}
	else if( IsTypeOf(rt,in1,GetDoubleObjectInRuntimeObjectsHeap(rt)) )
	{
		nret = fread( (void*) & d , sizeof(double) , 1 ,direct_prop->fp );
		FromDataPtr( rt , in1 , (void*) & d , sizeof(double) ) ;
	}
	else if( IsTypeOf(rt,in1,GetBoolObjectInRuntimeObjectsHeap(rt)) )
	{
		nret = fread( (void*) & b , sizeof(unsigned char) , 1 ,direct_prop->fp );
		FromDataPtr( rt , in1 , (void*) & b , sizeof(unsigned char) ) ;
	}
	else
	{
		SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_OBJECT_TYPE_NOT_MATCHED , "parameter type not matched" )
		return ThrowFatalException( rt , ZLANG_FATAL_INVOKE_METHOD_RETURN , EXCEPTION_MESSAGE_GENERAL_ERROR );
	}
	
	CallRuntimeFunction_int_SetIntValue( rt , out1 , nret );
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_stdfile_ReadFileToString_string;
int ZlangInvokeFunction_stdfile_ReadFileToString_string( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangObject			*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject			*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	char					*filename = NULL ;
	struct stat				file_stat ;
	char					**buf = NULL ;
	int32_t					*buf_len = NULL ;
	FILE					*fp = NULL ;
	int					nret = 0 ;
	
	CallRuntimeFunction_string_GetStringValue( rt , in1 , & filename , NULL );
	
	nret = stat( filename , & file_stat ) ;
	if( nret )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "CallRuntimeFunction_string_PrepareBuffer failed[%d]" , nret )
		UnreferObject( rt , out1 );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_STAT_FILE_FAILED );
	}
	
	nret = CallRuntimeFunction_string_PrepareBuffer( rt , out1 , file_stat.st_size ) ;
	if( nret )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "CallRuntimeFunction_string_PrepareBuffer failed[%d]" , nret )
		UnreferObject( rt , out1 );
		return ThrowFatalException( rt , nret , EXCEPTION_MESSAGE_GENERAL_ERROR );
	}
	
	CallRuntimeFunction_string_GetDirectPropertiesPtr( rt , out1 , & buf , NULL , & buf_len );
	fp = fopen( filename , "rb" ) ;
	if( fp == NULL )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "fopen failed , errno[%d]" , errno )
		UnreferObject( rt , out1 );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_OPEN_FILE_FAILED );
	}
	nret = (int)fread( *buf , file_stat.st_size , 1 , fp );
	if( nret != 1 )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "fread failed , errno[%d]" , errno )
		fclose( fp );
		UnreferObject( rt , out1 );
		return ThrowFatalException( rt , nret , EXCEPTION_MESSAGE_GENERAL_ERROR );
	}
	(*buf)[file_stat.st_size] = '\0' ;
	(*buf_len) = file_stat.st_size ;
	fclose( fp );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_stdfile_WriteStringToFile_string_string;
int ZlangInvokeFunction_stdfile_WriteStringToFile_string_string( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangObject			*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject			*in2 = GetInputParameterInLocalObjectStack(rt,2) ;
	struct ZlangObject			*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	char					*filename = NULL ;
	struct stat				file_stat ;
	char					*buf = NULL ;
	int32_t					buf_len ;
	FILE					*fp = NULL ;
	int					nret = 0 ;
	
	CallRuntimeFunction_string_GetStringValue( rt , in1 , & filename , NULL );
	CallRuntimeFunction_string_GetStringValue( rt , in2 , & buf , & buf_len ) ;
	
	fp = fopen( filename , "wb" ) ;
	if( fp == NULL )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "fopen failed , errno[%d]" , errno )
		CallRuntimeFunction_bool_SetBoolValue( rt , out1 , FALSE );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_OPEN_FILE_FAILED );
	}
	
	memset( & file_stat , 0x00 , sizeof(struct stat) );
	nret = (int)fwrite( buf , file_stat.st_size , 1 , fp );
	if( nret )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "fwrite failed , errno[%d]" , errno )
		fclose( fp );
		CallRuntimeFunction_bool_SetBoolValue( rt , out1 , FALSE );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_WRITE_FILE_FAILED );
	}
	fclose( fp );
	
	CallRuntimeFunction_bool_SetBoolValue( rt , out1 , TRUE );
	return 0;
}

ZlangCreateDirectPropertyFunction ZlangCreateDirectProperty_stdfile;
void *ZlangCreateDirectProperty_stdfile( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_stdfile	*stdfile_prop = NULL ;
	
	stdfile_prop = (struct ZlangDirectProperty_stdfile *)ZLMALLOC( sizeof(struct ZlangDirectProperty_stdfile) ) ;
	if( stdfile_prop == NULL )
	{
		SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_ALLOC , "alloc memory for entity" )
		return NULL;
	}
	memset( stdfile_prop , 0x00 , sizeof(struct ZlangDirectProperty_stdfile) );
	
	return stdfile_prop;
}

ZlangDestroyDirectPropertyFunction ZlangDestroyDirectProperty_stdfile;
void ZlangDestroyDirectProperty_stdfile( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_stdfile	*stdfile_direct_prop = GetObjectDirectProperty(obj) ;
	int					fclose_return ;
	
	if( stdfile_direct_prop->fp )
	{
		fclose_return = fclose( stdfile_direct_prop->fp ) ;
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "[%d] = fclose( \"%p\" ) ;" , fclose_return , stdfile_direct_prop->fp )
		stdfile_direct_prop->fp = NULL ;
	}
	
	ZLFREE( stdfile_direct_prop );
	
	return;
}

ZlangSummarizeDirectPropertySizeFunction ZlangSummarizeDirectPropertySize_stdfile;
void ZlangSummarizeDirectPropertySize_stdfile( struct ZlangRuntime *rt , struct ZlangObject *obj , size_t *summarized_obj_size , size_t *summarized_direct_prop_size )
{
	SUMMARIZE_SIZE( summarized_direct_prop_size , sizeof(struct ZlangDirectProperty_stdfile) )
	return;
}

static struct ZlangDirectFunctions direct_funcs_stdfile =
	{
		ZLANG_OBJECT_stdfile , /* char *ancestor_name */
		
		ZlangCreateDirectProperty_stdfile , /* ZlangCreateDirectPropertyFunction *create_entity_func */
		ZlangDestroyDirectProperty_stdfile , /* ZlangDestroyDirectPropertyFunction *destroy_entity_func */
		
		NULL , /* ZlangFromCharPtrFunction *from_char_ptr_func */
		NULL , /* ZlangToStringFunction *to_string_func */
		NULL , /* ZlangFromDataPtrFunction *from_data_ptr_func */
		NULL , /* ZlangGetDataPtrFunction *get_data_ptr_func */
		
		NULL , /* ZlangOperatorFunction *oper_PLUS_func */
		NULL , /* ZlangOperatorFunction *oper_MINUS_func */
		NULL , /* ZlangOperatorFunction *oper_MUL_func */
		NULL , /* ZlangOperatorFunction *oper_DIV_func */
		NULL , /* ZlangOperatorFunction *oper_MOD_func */
		
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_NEGATIVE_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_NOT_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_BIT_REVERSE_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_PLUS_PLUS_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_MINUS_MINUS_func */
		
		NULL , /* ZlangCompareFunction *comp_EGUAL_func */
		NULL , /* ZlangCompareFunction *comp_NOTEGUAL_func */
		NULL , /* ZlangCompareFunction *comp_LT_func */
		NULL , /* ZlangCompareFunction *comp_LE_func */
		NULL , /* ZlangCompareFunction *comp_GT_func */
		NULL , /* ZlangCompareFunction *comp_GE_func */
		
		NULL , /* ZlangLogicFunction *logic_AND_func */
		NULL , /* ZlangLogicFunction *logic_OR_func */
		
		NULL , /* ZlangLogicFunction *bit_AND_func */
		NULL , /* ZlangLogicFunction *bit_XOR_func */
		NULL , /* ZlangLogicFunction *bit_OR_func */
		NULL , /* ZlangLogicFunction *bit_MOVELEFT_func */
		NULL , /* ZlangLogicFunction *bit_MOVERIGHT_func */
		
		ZlangSummarizeDirectPropertySize_stdfile , /* ZlangSummarizeDirectPropertySizeFunction *summarize_direct_prop_size_func */
	} ;

ZlangImportObjectFunction ZlangImportObject_stdfile;
struct ZlangObject *ZlangImportObject_stdfile( struct ZlangRuntime *rt )
{
	struct ZlangObject	*obj = NULL ;
	struct ZlangFunction	*func = NULL ;
	int			nret = 0 ;
	
	nret = ImportObject( rt , & obj , ZLANG_OBJECT_stdfile , & direct_funcs_stdfile , sizeof(struct ZlangDirectFunctions) , NULL ) ;
	if( nret )
	{
		SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_LINK_FUNC_TO_ENTITY , "import object to global objects heap" )
		return NULL;
	}
	
	/* stdfile.Open(string,string) */
	func = AddFunctionAndParametersInObject( rt , obj , "Open" , "Open(string,string)" , ZlangInvokeFunction_stdfile_Open_string_string , ZLANG_OBJECT_int , ZLANG_OBJECT_string,NULL , ZLANG_OBJECT_string,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* stdfile.Close() */
	func = AddFunctionAndParametersInObject( rt , obj , "Close" , "Close()" , ZlangInvokeFunction_stdfile_Close , ZLANG_OBJECT_int , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* stdfile.Scan(string) */
	func = AddFunctionAndParametersInObject( rt , obj , "Scan" , "Scan(string)" , ZlangInvokeFunction_stdfile_Scan_string , ZLANG_OBJECT_int , ZLANG_OBJECT_string,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* stdfile.Scan(short) */
	func = AddFunctionAndParametersInObject( rt , obj , "Scan" , "Scan(short)" , ZlangInvokeFunction_stdfile_Scan_short , ZLANG_OBJECT_int , ZLANG_OBJECT_short,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* stdfile.Scan(ushort) */
	func = AddFunctionAndParametersInObject( rt , obj , "Scan" , "Scan(ushort)" , ZlangInvokeFunction_stdfile_Scan_ushort , ZLANG_OBJECT_int , ZLANG_OBJECT_ushort,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* stdfile.Scan(int) */
	func = AddFunctionAndParametersInObject( rt , obj , "Scan" , "Scan(int)" , ZlangInvokeFunction_stdfile_Scan_int , ZLANG_OBJECT_int , ZLANG_OBJECT_int,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* stdfile.Scan(uint) */
	func = AddFunctionAndParametersInObject( rt , obj , "Scan" , "Scan(uint)" , ZlangInvokeFunction_stdfile_Scan_uint , ZLANG_OBJECT_int , ZLANG_OBJECT_uint,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* stdfile.Scan(long) */
	func = AddFunctionAndParametersInObject( rt , obj , "Scan" , "Scan(long)" , ZlangInvokeFunction_stdfile_Scan_long , ZLANG_OBJECT_int , ZLANG_OBJECT_long,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* stdfile.Scan(ulong) */
	func = AddFunctionAndParametersInObject( rt , obj , "Scan" , "Scan(ulong)" , ZlangInvokeFunction_stdfile_Scan_ulong , ZLANG_OBJECT_int , ZLANG_OBJECT_ulong,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* stdfile.Scan(float) */
	func = AddFunctionAndParametersInObject( rt , obj , "Scan" , "Scan(float)" , ZlangInvokeFunction_stdfile_Scan_float , ZLANG_OBJECT_int , ZLANG_OBJECT_float,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* stdfile.Scan(double) */
	func = AddFunctionAndParametersInObject( rt , obj , "Scan" , "Scan(double)" , ZlangInvokeFunction_stdfile_Scan_double , ZLANG_OBJECT_int , ZLANG_OBJECT_double,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* stdfile.Scanln(string) */
	func = AddFunctionAndParametersInObject( rt , obj , "Scanln" , "Scanln(string)" , ZlangInvokeFunction_stdfile_Scanln_string , ZLANG_OBJECT_int , ZLANG_OBJECT_string,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* stdfile.Print(string) */
	func = AddFunctionAndParametersInObject( rt , obj , "Print" , "Print(string)" , ZlangInvokeFunction_stdfile_Print_string , ZLANG_OBJECT_int , ZLANG_OBJECT_string,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* stdfile.Println(string) */
	func = AddFunctionAndParametersInObject( rt , obj , "Println" , "Println(string)" , ZlangInvokeFunction_stdfile_Println_string , ZLANG_OBJECT_int , ZLANG_OBJECT_string,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* stdfile.Print(bool) */
	func = AddFunctionAndParametersInObject( rt , obj , "Print" , "Print(bool)" , ZlangInvokeFunction_stdfile_Print_basetype , ZLANG_OBJECT_int , ZLANG_OBJECT_bool,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* stdfile.Println(bool) */
	func = AddFunctionAndParametersInObject( rt , obj , "Println" , "Println(bool)" , ZlangInvokeFunction_stdfile_Println_basetype , ZLANG_OBJECT_int , ZLANG_OBJECT_bool,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* stdfile.Print(short) */
	func = AddFunctionAndParametersInObject( rt , obj , "Print" , "Print(short)" , ZlangInvokeFunction_stdfile_Print_basetype , ZLANG_OBJECT_int , ZLANG_OBJECT_short,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* stdfile.Println(short) */
	func = AddFunctionAndParametersInObject( rt , obj , "Println" , "Println(short)" , ZlangInvokeFunction_stdfile_Println_basetype , ZLANG_OBJECT_int , ZLANG_OBJECT_short,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* stdfile.Print(ushort) */
	func = AddFunctionAndParametersInObject( rt , obj , "Print" , "Print(ushort)" , ZlangInvokeFunction_stdfile_Print_basetype , ZLANG_OBJECT_int , ZLANG_OBJECT_ushort,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* stdfile.Println(ushort) */
	func = AddFunctionAndParametersInObject( rt , obj , "Println" , "Println(ushort)" , ZlangInvokeFunction_stdfile_Println_basetype , ZLANG_OBJECT_int , ZLANG_OBJECT_ushort,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* stdfile.Print(int) */
	func = AddFunctionAndParametersInObject( rt , obj , "Print" , "Print(int)" , ZlangInvokeFunction_stdfile_Print_basetype , ZLANG_OBJECT_int , ZLANG_OBJECT_int,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* stdfile.Println(int) */
	func = AddFunctionAndParametersInObject( rt , obj , "Println" , "Println(int)" , ZlangInvokeFunction_stdfile_Println_basetype , ZLANG_OBJECT_int , ZLANG_OBJECT_int,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* stdfile.Print(uint) */
	func = AddFunctionAndParametersInObject( rt , obj , "Print" , "Print(uint)" , ZlangInvokeFunction_stdfile_Print_basetype , ZLANG_OBJECT_int , ZLANG_OBJECT_uint,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* stdfile.Println(uint) */
	func = AddFunctionAndParametersInObject( rt , obj , "Println" , "Println(uint)" , ZlangInvokeFunction_stdfile_Println_basetype , ZLANG_OBJECT_int , ZLANG_OBJECT_uint,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* stdfile.Print(long) */
	func = AddFunctionAndParametersInObject( rt , obj , "Print" , "Print(long)" , ZlangInvokeFunction_stdfile_Print_basetype , ZLANG_OBJECT_int , ZLANG_OBJECT_long,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* stdfile.Println(long) */
	func = AddFunctionAndParametersInObject( rt , obj , "Println" , "Println(long)" , ZlangInvokeFunction_stdfile_Println_basetype , ZLANG_OBJECT_int , ZLANG_OBJECT_long,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* stdfile.Print(ulong) */
	func = AddFunctionAndParametersInObject( rt , obj , "Print" , "Print(ulong)" , ZlangInvokeFunction_stdfile_Print_basetype , ZLANG_OBJECT_int , ZLANG_OBJECT_ulong,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* stdfile.Println(ulong) */
	func = AddFunctionAndParametersInObject( rt , obj , "Println" , "Println(ulong)" , ZlangInvokeFunction_stdfile_Println_basetype , ZLANG_OBJECT_int , ZLANG_OBJECT_ulong,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* stdfile.Print(float) */
	func = AddFunctionAndParametersInObject( rt , obj , "Print" , "Print(float)" , ZlangInvokeFunction_stdfile_Print_basetype , ZLANG_OBJECT_int , ZLANG_OBJECT_float,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* stdfile.Println(float) */
	func = AddFunctionAndParametersInObject( rt , obj , "Println" , "Println(float)" , ZlangInvokeFunction_stdfile_Println_basetype , ZLANG_OBJECT_int , ZLANG_OBJECT_float,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* stdfile.Print(double) */
	func = AddFunctionAndParametersInObject( rt , obj , "Print" , "Print(double)" , ZlangInvokeFunction_stdfile_Print_basetype , ZLANG_OBJECT_int , ZLANG_OBJECT_double,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* stdfile.Println(double) */
	func = AddFunctionAndParametersInObject( rt , obj , "Println" , "Println(double)" , ZlangInvokeFunction_stdfile_Println_basetype , ZLANG_OBJECT_int , ZLANG_OBJECT_double,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* stdfile.Print(null) */
	func = AddFunctionAndParametersInObject( rt , obj , "Print" , "Print((null))" , ZlangInvokeFunction_stdfile_Print_null , ZLANG_OBJECT_int , ZLANG_OBJECT_object,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* stdfile.Println(null) */
	func = AddFunctionAndParametersInObject( rt , obj , "Println" , "Println((null))" , ZlangInvokeFunction_stdfile_Println_null , ZLANG_OBJECT_int , ZLANG_OBJECT_object,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* stdfile.Print(...) */
	func = AddFunctionAndParametersInObject( rt , obj , "Print" , "Print(...)" , ZlangInvokeFunction_stdfile_Print_basetype , ZLANG_OBJECT_int , ZLANG_OBJECT_vargs,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* stdfile.Println(...) */
	func = AddFunctionAndParametersInObject( rt , obj , "Println" , "Println(...)" , ZlangInvokeFunction_stdfile_Println_basetype , ZLANG_OBJECT_int , ZLANG_OBJECT_vargs,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* stdfile.FormatPrint(...) */
	func = AddFunctionAndParametersInObject( rt , obj , "FormatPrint" , "FormatPrint(...)" , ZlangInvokeFunction_stdfile_FormatPrint_vargs , ZLANG_OBJECT_int , ZLANG_OBJECT_vargs,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* stdfile.FormatPrintln(...) */
	func = AddFunctionAndParametersInObject( rt , obj , "FormatPrintln" , "FormatPrintln(...)" , ZlangInvokeFunction_stdfile_FormatPrintln_vargs , ZLANG_OBJECT_int , ZLANG_OBJECT_vargs,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* stdfile.Write(short) */
	func = AddFunctionAndParametersInObject( rt , obj , "Write" , "Write(short)" , ZlangInvokeFunction_stdfile_Write_basetype , ZLANG_OBJECT_int , ZLANG_OBJECT_short,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* stdfile.Write(ushort) */
	func = AddFunctionAndParametersInObject( rt , obj , "Write" , "Write(ushort)" , ZlangInvokeFunction_stdfile_Write_basetype , ZLANG_OBJECT_int , ZLANG_OBJECT_ushort,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* stdfile.Write(int) */
	func = AddFunctionAndParametersInObject( rt , obj , "Write" , "Write(int)" , ZlangInvokeFunction_stdfile_Write_basetype , ZLANG_OBJECT_int , ZLANG_OBJECT_int,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* stdfile.Write(uint) */
	func = AddFunctionAndParametersInObject( rt , obj , "Write" , "Write(uint)" , ZlangInvokeFunction_stdfile_Write_basetype , ZLANG_OBJECT_int , ZLANG_OBJECT_uint,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* stdfile.Write(long) */
	func = AddFunctionAndParametersInObject( rt , obj , "Write" , "Write(long)" , ZlangInvokeFunction_stdfile_Write_basetype , ZLANG_OBJECT_int , ZLANG_OBJECT_long,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* stdfile.Write(ulong) */
	func = AddFunctionAndParametersInObject( rt , obj , "Write" , "Write(ulong)" , ZlangInvokeFunction_stdfile_Write_basetype , ZLANG_OBJECT_int , ZLANG_OBJECT_ulong,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* stdfile.Write(float) */
	func = AddFunctionAndParametersInObject( rt , obj , "Write" , "Write(float)" , ZlangInvokeFunction_stdfile_Write_basetype , ZLANG_OBJECT_int , ZLANG_OBJECT_float,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* stdfile.Write(double) */
	func = AddFunctionAndParametersInObject( rt , obj , "Write" , "Write(double)" , ZlangInvokeFunction_stdfile_Write_basetype , ZLANG_OBJECT_int , ZLANG_OBJECT_double,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* stdfile.Write(bool) */
	func = AddFunctionAndParametersInObject( rt , obj , "Write" , "Write(bool)" , ZlangInvokeFunction_stdfile_Write_basetype , ZLANG_OBJECT_int , ZLANG_OBJECT_bool,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* stdfile.Read(short) */
	func = AddFunctionAndParametersInObject( rt , obj , "Read" , "Read(short)" , ZlangInvokeFunction_stdfile_Read_basetype , ZLANG_OBJECT_int , ZLANG_OBJECT_short,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* stdfile.Read(ushort) */
	func = AddFunctionAndParametersInObject( rt , obj , "Read" , "Read(ushort)" , ZlangInvokeFunction_stdfile_Read_basetype , ZLANG_OBJECT_int , ZLANG_OBJECT_ushort,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* stdfile.Read(int) */
	func = AddFunctionAndParametersInObject( rt , obj , "Read" , "Read(int)" , ZlangInvokeFunction_stdfile_Read_basetype , ZLANG_OBJECT_int , ZLANG_OBJECT_int,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* stdfile.Read(uint) */
	func = AddFunctionAndParametersInObject( rt , obj , "Read" , "Read(uint)" , ZlangInvokeFunction_stdfile_Read_basetype , ZLANG_OBJECT_int , ZLANG_OBJECT_uint,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* stdfile.Read(long) */
	func = AddFunctionAndParametersInObject( rt , obj , "Read" , "Read(long)" , ZlangInvokeFunction_stdfile_Read_basetype , ZLANG_OBJECT_int , ZLANG_OBJECT_long,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* stdfile.Read(ulong) */
	func = AddFunctionAndParametersInObject( rt , obj , "Read" , "Read(ulong)" , ZlangInvokeFunction_stdfile_Read_basetype , ZLANG_OBJECT_int , ZLANG_OBJECT_ulong,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* stdfile.Read(float) */
	func = AddFunctionAndParametersInObject( rt , obj , "Read" , "Read(float)" , ZlangInvokeFunction_stdfile_Read_basetype , ZLANG_OBJECT_int , ZLANG_OBJECT_float,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* stdfile.Read(double) */
	func = AddFunctionAndParametersInObject( rt , obj , "Read" , "Read(double)" , ZlangInvokeFunction_stdfile_Read_basetype , ZLANG_OBJECT_int , ZLANG_OBJECT_double,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* stdfile.Read(bool) */
	func = AddFunctionAndParametersInObject( rt , obj , "Read" , "Read(bool)" , ZlangInvokeFunction_stdfile_Read_basetype , ZLANG_OBJECT_int , ZLANG_OBJECT_bool,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* stdfile.ReadFileToString(string) */
	func = AddFunctionAndParametersInObject( rt , obj , "ReadFileToString" , "ReadFileToString(string)" , ZlangInvokeFunction_stdfile_ReadFileToString_string , ZLANG_OBJECT_string , ZLANG_OBJECT_string,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* stdfile.WriteStringToFile(string,string) */
	func = AddFunctionAndParametersInObject( rt , obj , "WriteStringToFile" , "WriteStringToFile(string,string)" , ZlangInvokeFunction_stdfile_WriteStringToFile_string_string , ZLANG_OBJECT_bool , ZLANG_OBJECT_string,NULL , ZLANG_OBJECT_string,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	return obj ;
}

