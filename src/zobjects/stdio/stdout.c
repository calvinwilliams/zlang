/* Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "zobjects_stdio.h"

struct ZlangDirectProperty_stdout
{
	unsigned char		is_buffer_enable ;
} ;

ZlangInvokeFunction ZlangInvokeFunction_stdout_EnableBuffer_bool;
int ZlangInvokeFunction_stdout_EnableBuffer_bool( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_stdout	*stdout_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	
	CallRuntimeFunction_bool_GetBoolValue( rt , in1 , & (stdout_direct_prop->is_buffer_enable) );
	
	return 0;
}

static int Print( struct ZlangRuntime *rt , struct ZlangObject *str_obj , struct ZlangObject *str_len_obj )
{
	char			*data = NULL ;
	int32_t			data_len ;
	
	if( GetObjectDirectFunctions(str_obj) == NULL || GetObjectPropertiesEntity(str_obj) == NULL )
	{
		data = NULL ;
	}
	else
	{
		GetDataPtr( rt , str_obj , (void**) & data , & data_len );
	}
	
	if( data == NULL )
	{
		printf( "%s" , ZLANG_null_STRING );
		data_len = sizeof(ZLANG_null_STRING) - 1 ;
	}
	else
	{
		printf( "%.*s" , data_len,data );
	}
	
	FromDataPtr( rt , str_len_obj , & data_len , sizeof(int32_t) );
	
	return 0;
}

static int Println( struct ZlangRuntime *rt , struct ZlangObject *str_obj , struct ZlangObject *str_len_obj )
{
	int		nret = 0 ;
	
	if( str_obj )
	{
		nret = Print( rt , str_obj , str_len_obj ) ;
		if( nret )
			return nret;
	}
	
	printf( NEWLINE );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_stdout_Print_string;
int ZlangInvokeFunction_stdout_Print_string( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_stdout	*stdout_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject			*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	int					nret = 0 ;
	
	nret = Print( rt , in1 , out1 ) ;
	if( nret )
		return ThrowFatalException( rt , ZLANG_FATAL_INVOKE_METHOD_RETURN , EXCEPTION_MESSAGE_ALLOC_FAILED );
	
	if( stdout_direct_prop->is_buffer_enable != TRUE )
		fflush(stdout);
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_stdout_Println_string;
int ZlangInvokeFunction_stdout_Println_string( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_stdout	*stdout_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject			*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	int					nret = 0 ;
	
	nret = Println( rt , in1 , out1 ) ;
	if( nret )
		return ThrowFatalException( rt , ZLANG_FATAL_INVOKE_METHOD_RETURN , EXCEPTION_MESSAGE_ALLOC_FAILED );
	
	if( stdout_direct_prop->is_buffer_enable != TRUE )
		fflush(stdout);
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_stdout_Print_basetype;
int ZlangInvokeFunction_stdout_Print_basetype( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_stdout	*stdout_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject			*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject			*tostr = NULL ;
	int				nret = 0 ;
	
	nret = ToString( rt , in1 , & tostr ) ;
	if( nret )
		return ThrowFatalException( rt , ZLANG_FATAL_INVOKE_METHOD_RETURN , EXCEPTION_MESSAGE_ALLOC_FAILED );
	
	nret = Print( rt , tostr , out1 ) ;
	if( nret )
		return ThrowFatalException( rt , ZLANG_FATAL_INVOKE_METHOD_RETURN , EXCEPTION_MESSAGE_ALLOC_FAILED );
	
	if( stdout_direct_prop->is_buffer_enable != TRUE )
		fflush(stdout);
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_stdout_Println_basetype;
int ZlangInvokeFunction_stdout_Println_basetype( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_stdout	*stdout_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject			*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject			*tostr = NULL ;
	int					nret = 0 ;
	
	nret = ToString( rt , in1 , & tostr ) ;
	if( nret )
		return ThrowFatalException( rt , ZLANG_FATAL_INVOKE_METHOD_RETURN , EXCEPTION_MESSAGE_ALLOC_FAILED );
	
	nret = Println( rt , tostr , out1 ) ;
	if( nret )
		return ThrowFatalException( rt , ZLANG_FATAL_INVOKE_METHOD_RETURN , EXCEPTION_MESSAGE_ALLOC_FAILED );
	
	if( stdout_direct_prop->is_buffer_enable != TRUE )
		fflush(stdout);
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_stdout_Print_null;
int ZlangInvokeFunction_stdout_Print_null( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_stdout	*stdout_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject			*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	int					nret = 0 ;
	
	nret = Print( rt , in1 , out1 ) ;
	if( nret )
		return ThrowFatalException( rt , ZLANG_FATAL_INVOKE_METHOD_RETURN , EXCEPTION_MESSAGE_ALLOC_FAILED );
	
	if( stdout_direct_prop->is_buffer_enable != TRUE )
		fflush(stdout);
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_stdout_Println_null;
int ZlangInvokeFunction_stdout_Println_null( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_stdout	*stdout_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject			*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	int					nret = 0 ;
	
	nret = Println( rt , in1 , out1 ) ;
	if( nret )
		return ThrowFatalException( rt , ZLANG_FATAL_INVOKE_METHOD_RETURN , EXCEPTION_MESSAGE_ALLOC_FAILED );
	
	if( stdout_direct_prop->is_buffer_enable != TRUE )
		fflush(stdout);
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_stdout_Println;
int ZlangInvokeFunction_stdout_Println( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_stdout	*stdout_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	int					nret = 0 ;
	
	nret = Println( rt , NULL , out1 ) ;
	if( nret )
		return ThrowFatalException( rt , ZLANG_FATAL_INVOKE_METHOD_RETURN , EXCEPTION_MESSAGE_ALLOC_FAILED );
	
	if( stdout_direct_prop->is_buffer_enable != TRUE )
		fflush(stdout);
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_stdout_FormatPrint_vargs;
int ZlangInvokeFunction_stdout_FormatPrint_vargs( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_stdout	*stdout_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*buf_obj = NULL ;
	char					*str = NULL ;
	int32_t					str_len ;
	
	int					nret = 0 ;
	
	buf_obj = CloneStringObjectInTmpStack( rt , NULL ) ;
	if( buf_obj == NULL )
		return ThrowFatalException( rt , ZLANG_FATAL_INVOKE_METHOD_RETURN , EXCEPTION_MESSAGE_ALLOC_FAILED );

	nret = CallRuntimeFunction_string_AppendFormatFromArgsStack( rt , buf_obj ) ;
	if( nret )
		return ThrowFatalException( rt , ZLANG_FATAL_INVOKE_METHOD_RETURN , EXCEPTION_MESSAGE_ALLOC_FAILED );
	
	GetDataPtr( rt , buf_obj , (void**) & str , & str_len );
	
	printf( "%.*s" , str_len,str );
	if( stdout_direct_prop->is_buffer_enable != TRUE )
		fflush(stdout);
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_stdout_FormatPrintln_vargs;
int ZlangInvokeFunction_stdout_FormatPrintln_vargs( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_stdout	*stdout_direct_prop = GetObjectDirectProperty(obj) ;
	int					nret = 0 ;
	
	nret = ZlangInvokeFunction_stdout_FormatPrint_vargs( rt , obj ) ;
	if( nret )
		return ThrowFatalException( rt , ZLANG_FATAL_INVOKE_METHOD_RETURN , EXCEPTION_MESSAGE_ALLOC_FAILED );
	
	printf( NEWLINE );
	if( stdout_direct_prop->is_buffer_enable != TRUE )
		fflush(stdout);
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_stdout_Write_string;
int ZlangInvokeFunction_stdout_Write_string( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_stdout	*stdout_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	char					*data = NULL ;
	int32_t					data_len ;
	
	if( GetObjectDirectFunctions(in1) == NULL || GetObjectPropertiesEntity(in1) == NULL )
		return 0;
	
	GetDataPtr( rt , in1 , (void**) & data , & data_len );
	WRITE( 1 , data , data_len );
	
	if( stdout_direct_prop->is_buffer_enable != TRUE )
		fflush(stdout);
	
	return 0;
}

ZlangCreateDirectPropertyFunction ZlangCreateDirectProperty_stdout;
void *ZlangCreateDirectProperty_stdout( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_stdout	*stdout_prop = NULL ;
	
	stdout_prop = (struct ZlangDirectProperty_stdout *)ZLMALLOC( sizeof(struct ZlangDirectProperty_stdout) ) ;
	if( stdout_prop == NULL )
	{
		SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_ALLOC , "alloc memory for entity" )
		return NULL;
	}
	memset( stdout_prop , 0x00 , sizeof(struct ZlangDirectProperty_stdout) );
	
	stdout_prop->is_buffer_enable = TRUE ;
	
	return stdout_prop;
}

ZlangDestroyDirectPropertyFunction ZlangDestroyDirectProperty_stdout;
void ZlangDestroyDirectProperty_stdout( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_stdout	*stdout_direct_prop = GetObjectDirectProperty(obj) ;
	
	ZLFREE( stdout_direct_prop );
	
	return;
}

ZlangSummarizeDirectPropertySizeFunction ZlangSummarizeDirectPropertySize_stdout;
void ZlangSummarizeDirectPropertySize_stdout( struct ZlangRuntime *rt , struct ZlangObject *obj , size_t *summarized_obj_size , size_t *summarized_direct_prop_size )
{
	SUMMARIZE_SIZE( summarized_direct_prop_size , sizeof(struct ZlangDirectProperty_stdout) )
	return;
}

static struct ZlangDirectFunctions direct_funcs_stdout =
	{
		ZLANG_OBJECT_stdout , /* char *ancestor_name */
		
		ZlangCreateDirectProperty_stdout , /* ZlangCreateDirectPropertyFunction *create_entity_func */
		ZlangDestroyDirectProperty_stdout , /* ZlangDestroyDirectPropertyFunction *destroy_entity_func */
		
		NULL , /* ZlangFromCharPtrFunction *from_char_ptr_func */
		NULL , /* ZlangToStringFunction *to_string_func */
		NULL , /* ZlangFromDataPtrFunction *from_data_ptr_func */
		NULL , /* ZlangGetDataPtrFunction *get_data_ptr_func */
		
		NULL , /* ZlangOperatorFunction *oper_PLUS_func */
		NULL , /* ZlangOperatorFunction *oper_MINUS_func */
		NULL , /* ZlangOperatorFunction *oper_MUL_func */
		NULL , /* ZlangOperatorFunction *oper_DIV_func */
		NULL , /* ZlangOperatorFunction *oper_MOD_func */
		
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_NEGATIVE_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_NOT_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_BIT_REVERSE_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_PLUS_PLUS_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_MINUS_MINUS_func */
		
		NULL , /* ZlangCompareFunction *comp_EGUAL_func */
		NULL , /* ZlangCompareFunction *comp_NOTEGUAL_func */
		NULL , /* ZlangCompareFunction *comp_LT_func */
		NULL , /* ZlangCompareFunction *comp_LE_func */
		NULL , /* ZlangCompareFunction *comp_GT_func */
		NULL , /* ZlangCompareFunction *comp_GE_func */
		
		NULL , /* ZlangLogicFunction *logic_AND_func */
		NULL , /* ZlangLogicFunction *logic_OR_func */
		
		NULL , /* ZlangLogicFunction *bit_AND_func */
		NULL , /* ZlangLogicFunction *bit_XOR_func */
		NULL , /* ZlangLogicFunction *bit_OR_func */
		NULL , /* ZlangLogicFunction *bit_MOVELEFT_func */
		NULL , /* ZlangLogicFunction *bit_MOVERIGHT_func */
		
		ZlangSummarizeDirectPropertySize_stdout , /* ZlangSummarizeDirectPropertySizeFunction *summarize_direct_prop_size_func */
	} ;

ZlangImportObjectFunction ZlangImportObject_stdout;
struct ZlangObject *ZlangImportObject_stdout( struct ZlangRuntime *rt )
{
	struct ZlangObject	*obj = NULL ;
	struct ZlangFunction	*func = NULL ;
	int			nret = 0 ;
	
	nret = ImportObject( rt , & obj , ZLANG_OBJECT_stdout , & direct_funcs_stdout , sizeof(struct ZlangDirectFunctions) , NULL ) ;
	if( nret )
	{
		SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_LINK_FUNC_TO_ENTITY , "import object to global objects heap" )
		return NULL;
	}
	
	/* stdout.EnableBuffer(bool) */
	func = AddFunctionAndParametersInObject( rt , obj , "EnableBuffer" , "EnableBuffer(bool)" , ZlangInvokeFunction_stdout_EnableBuffer_bool , ZLANG_OBJECT_void , ZLANG_OBJECT_bool,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* stdout.Print(string) */
	func = AddFunctionAndParametersInObject( rt , obj , "Print" , "Print(string)" , ZlangInvokeFunction_stdout_Print_string , ZLANG_OBJECT_int , ZLANG_OBJECT_string,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* stdout.Println(string) */
	func = AddFunctionAndParametersInObject( rt , obj , "Println" , "Println(string)" , ZlangInvokeFunction_stdout_Println_string , ZLANG_OBJECT_int , ZLANG_OBJECT_string,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* stdout.Print(bool) */
	func = AddFunctionAndParametersInObject( rt , obj , "Print" , "Print(bool)" , ZlangInvokeFunction_stdout_Print_basetype , ZLANG_OBJECT_int , ZLANG_OBJECT_bool,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* stdout.Println(bool) */
	func = AddFunctionAndParametersInObject( rt , obj , "Println" , "Println(bool)" , ZlangInvokeFunction_stdout_Println_basetype , ZLANG_OBJECT_int , ZLANG_OBJECT_bool,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* stdout.Print(short) */
	func = AddFunctionAndParametersInObject( rt , obj , "Print" , "Print(short)" , ZlangInvokeFunction_stdout_Print_basetype , ZLANG_OBJECT_int , ZLANG_OBJECT_short,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* stdout.Println(short) */
	func = AddFunctionAndParametersInObject( rt , obj , "Println" , "Println(short)" , ZlangInvokeFunction_stdout_Println_basetype , ZLANG_OBJECT_int , ZLANG_OBJECT_short,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* stdout.Print(ushort) */
	func = AddFunctionAndParametersInObject( rt , obj , "Print" , "Print(ushort)" , ZlangInvokeFunction_stdout_Print_basetype , ZLANG_OBJECT_int , ZLANG_OBJECT_ushort,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* stdout.Println(ushort) */
	func = AddFunctionAndParametersInObject( rt , obj , "Println" , "Println(ushort)" , ZlangInvokeFunction_stdout_Println_basetype , ZLANG_OBJECT_int , ZLANG_OBJECT_ushort,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* stdout.Print(int) */
	func = AddFunctionAndParametersInObject( rt , obj , "Print" , "Print(int)" , ZlangInvokeFunction_stdout_Print_basetype , ZLANG_OBJECT_int , ZLANG_OBJECT_int,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* stdout.Println(int) */
	func = AddFunctionAndParametersInObject( rt , obj , "Println" , "Println(int)" , ZlangInvokeFunction_stdout_Println_basetype , ZLANG_OBJECT_int , ZLANG_OBJECT_int,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* stdout.Print(uint) */
	func = AddFunctionAndParametersInObject( rt , obj , "Print" , "Print(uint)" , ZlangInvokeFunction_stdout_Print_basetype , ZLANG_OBJECT_int , ZLANG_OBJECT_uint,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* stdout.Println(uint) */
	func = AddFunctionAndParametersInObject( rt , obj , "Println" , "Println(uint)" , ZlangInvokeFunction_stdout_Println_basetype , ZLANG_OBJECT_int , ZLANG_OBJECT_uint,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* stdout.Print(long) */
	func = AddFunctionAndParametersInObject( rt , obj , "Print" , "Print(long)" , ZlangInvokeFunction_stdout_Print_basetype , ZLANG_OBJECT_int , ZLANG_OBJECT_long,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* stdout.Println(long) */
	func = AddFunctionAndParametersInObject( rt , obj , "Println" , "Println(long)" , ZlangInvokeFunction_stdout_Println_basetype , ZLANG_OBJECT_int , ZLANG_OBJECT_long,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* stdout.Print(ulong) */
	func = AddFunctionAndParametersInObject( rt , obj , "Print" , "Print(ulong)" , ZlangInvokeFunction_stdout_Print_basetype , ZLANG_OBJECT_int , ZLANG_OBJECT_ulong,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* stdout.Println(ulong) */
	func = AddFunctionAndParametersInObject( rt , obj , "Println" , "Println(ulong)" , ZlangInvokeFunction_stdout_Println_basetype , ZLANG_OBJECT_int , ZLANG_OBJECT_ulong,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* stdout.Print(float) */
	func = AddFunctionAndParametersInObject( rt , obj , "Print" , "Print(float)" , ZlangInvokeFunction_stdout_Print_basetype , ZLANG_OBJECT_int , ZLANG_OBJECT_float,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* stdout.Println(float) */
	func = AddFunctionAndParametersInObject( rt , obj , "Println" , "Println(float)" , ZlangInvokeFunction_stdout_Println_basetype , ZLANG_OBJECT_int , ZLANG_OBJECT_float,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* stdout.Print(double) */
	func = AddFunctionAndParametersInObject( rt , obj , "Print" , "Print(double)" , ZlangInvokeFunction_stdout_Print_basetype , ZLANG_OBJECT_int , ZLANG_OBJECT_double,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* stdout.Println(double) */
	func = AddFunctionAndParametersInObject( rt , obj , "Println" , "Println(double)" , ZlangInvokeFunction_stdout_Println_basetype , ZLANG_OBJECT_int , ZLANG_OBJECT_double,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* stdout.Print(null) */
	func = AddFunctionAndParametersInObject( rt , obj , "Print" , "Print((null))" , ZlangInvokeFunction_stdout_Print_null , ZLANG_OBJECT_int , ZLANG_OBJECT_object,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* stdout.Println(null) */
	func = AddFunctionAndParametersInObject( rt , obj , "Println" , "Println((null))" , ZlangInvokeFunction_stdout_Println_null , ZLANG_OBJECT_int , ZLANG_OBJECT_object,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* stdout.Println() */
	func = AddFunctionAndParametersInObject( rt , obj , "Println" , "Println()" , ZlangInvokeFunction_stdout_Println , ZLANG_OBJECT_int , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* stdout.Print(...) */
	func = AddFunctionAndParametersInObject( rt , obj , "Print" , "Print(...)" , ZlangInvokeFunction_stdout_Print_basetype , ZLANG_OBJECT_int , ZLANG_OBJECT_vargs,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* stdout.Println(...) */
	func = AddFunctionAndParametersInObject( rt , obj , "Println" , "Println(...)" , ZlangInvokeFunction_stdout_Println_basetype , ZLANG_OBJECT_int , ZLANG_OBJECT_vargs,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* stdout.FormatPrint(...) */
	func = AddFunctionAndParametersInObject( rt , obj , "FormatPrint" , "FormatPrint(...)" , ZlangInvokeFunction_stdout_FormatPrint_vargs , ZLANG_OBJECT_int , ZLANG_OBJECT_vargs,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* stdout.FormatPrintln(...) */
	func = AddFunctionAndParametersInObject( rt , obj , "FormatPrintln" , "FormatPrintln(...)" , ZlangInvokeFunction_stdout_FormatPrintln_vargs , ZLANG_OBJECT_int , ZLANG_OBJECT_vargs,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* stdout.Write(string) */
	func = AddFunctionAndParametersInObject( rt , obj , "Write" , "Write(string)" , ZlangInvokeFunction_stdout_Write_string , ZLANG_OBJECT_int , ZLANG_OBJECT_string,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	return obj ;
}

