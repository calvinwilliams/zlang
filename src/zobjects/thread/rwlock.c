/* Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "zobjects_thread.h"

ZlangFromDataPtrFunction ZlangFromDataPtr_rwlock;

#define LOCKTYPE_NONE		0
#define LOCKTYPE_RDLOCK		1
#define LOCKTYPE_WRLOCK		2

struct ZlangDirectProperty_rwlock
{
#if defined(__linux__)
	pthread_rwlock_t	rwlock ;
#elif defined(_WIN32)
	SRWLOCK			rwlock ;
	int			lock_type ;
#endif
} ;

ZlangInvokeFunction ZlangInvokeFunction_rwlock_LockRead;
int ZlangInvokeFunction_rwlock_LockRead( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_rwlock	*rwlock_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;

#if defined(__linux__)
	int					nret = 0 ;
	
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "pthread_rwlock_rdlock ..." )
	nret = pthread_rwlock_rdlock( & (rwlock_direct_prop->rwlock) ) ;
	if( nret )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "pthread_rwlock_rdlock failed[%d]" , nret )
		CallRuntimeFunction_bool_SetBoolValue( rt , out1 , FALSE );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_RDLOCK_RWLOCK_FAILED );
	}
	
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "pthread_rwlock_rdlock ok" )
	CallRuntimeFunction_bool_SetBoolValue( rt , out1 , TRUE );
#elif defined(_WIN32)
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "AcquireSRWLockShared ..." )
	AcquireSRWLockShared( & (rwlock_direct_prop->rwlock) ) ;

	rwlock_direct_prop->lock_type = LOCKTYPE_RDLOCK ;

	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "AcquireSRWLockShared ok" )
	CallRuntimeFunction_bool_SetBoolValue( rt , out1 , TRUE );
#endif
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_rwlock_LockWrite;
int ZlangInvokeFunction_rwlock_LockWrite( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_rwlock	*rwlock_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;

#if defined(__linux__)
	int					nret = 0 ;
	
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "pthread_rwlock_wrlock ..." )
	nret = pthread_rwlock_wrlock( & (rwlock_direct_prop->rwlock) ) ;
	if( nret )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "pthread_rwlock_wrlock failed[%d]" , nret )
		CallRuntimeFunction_bool_SetBoolValue( rt , out1 , FALSE );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_WRLOCK_RWLOCK_FAILED );
	}
	
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "pthread_rwlock_wrlock ok" )
	CallRuntimeFunction_bool_SetBoolValue( rt , out1 , TRUE );
#elif defined(_WIN32)
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "AcquireSRWLockExclusive ..." )
	AcquireSRWLockExclusive( & (rwlock_direct_prop->rwlock) ) ;

	rwlock_direct_prop->lock_type = LOCKTYPE_WRLOCK ;

	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "AcquireSRWLockExclusive ok" )
	CallRuntimeFunction_bool_SetBoolValue( rt , out1 , TRUE );
#endif
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_rwlock_TryLockRead;
int ZlangInvokeFunction_rwlock_TryLockRead( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_rwlock	*rwlock_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;

#if defined(__linux__)
	int					nret = 0 ;
	
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "pthread_rwlock_tryrdlock ..." )
	nret = pthread_rwlock_tryrdlock( & (rwlock_direct_prop->rwlock) ) ;
	if( nret == EBUSY )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "pthread_rwlock_tryrdlock is busy" )
		UnreferObject( rt , out1 );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_TRYRDLOCK_RWLOCK_BUSY );
	}
	else if( nret )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "pthread_rwlock_tryrdlock failed[%d]" , nret )
		CallRuntimeFunction_bool_SetBoolValue( rt , out1 , FALSE );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_TRYRDLOCK_RWLOCK_FAILED );
	}
	
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "pthread_rwlock_tryrdlock ok" )
	CallRuntimeFunction_bool_SetBoolValue( rt , out1 , TRUE );
#elif defined(_WIN32)
	BOOL					bret = FALSE ;
	
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "TryAcquireSRWLockShared ..." )
	bret = TryAcquireSRWLockShared( & (rwlock_direct_prop->rwlock) ) ;
	if( bret == FALSE )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "TryAcquireSRWLockShared failed" )
		UnreferObject( rt , out1 );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_TRYRDLOCK_RWLOCK_FAILED );
	}
	
	rwlock_direct_prop->lock_type = LOCKTYPE_RDLOCK ;

	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "TryAcquireSRWLockShared ok" )
	CallRuntimeFunction_bool_SetBoolValue( rt , out1 , TRUE );
#endif
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_rwlock_TryLockWrite;
int ZlangInvokeFunction_rwlock_TryLockWrite( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_rwlock	*rwlock_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;

#if defined(__linux__)
	int					nret = 0 ;
	
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "pthread_rwlock_trywrlock ..." )
	nret = pthread_rwlock_trywrlock( & (rwlock_direct_prop->rwlock) ) ;
	if( nret == EBUSY )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "pthread_rwlock_trywrlock is busy" )
		UnreferObject( rt , out1 );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_TRYWRLOCK_RWLOCK_BUSY );
	}
	else if( nret )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "pthread_rwlock_trywrlock failed[%d]" , nret )
		CallRuntimeFunction_bool_SetBoolValue( rt , out1 , FALSE );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_TRYWRLOCK_RWLOCK_FAILED );
	}
	
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "pthread_rwlock_trywrlock ok" )
	CallRuntimeFunction_bool_SetBoolValue( rt , out1 , TRUE );
#elif defined(_WIN32)
	BOOL					bret = FALSE ;
	
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "TryAcquireSRWLockExclusive ..." )
	bret = TryAcquireSRWLockExclusive( & (rwlock_direct_prop->rwlock) ) ;
	if( bret == FALSE )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "TryAcquireSRWLockExclusive failed" )
		UnreferObject( rt , out1 );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_TRYWRLOCK_RWLOCK_FAILED );
	}
	
	rwlock_direct_prop->lock_type = LOCKTYPE_WRLOCK ;

	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "TryAcquireSRWLockExclusive ok" )
	CallRuntimeFunction_bool_SetBoolValue( rt , out1 , TRUE );
#endif
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_rwlock_Unlock;
int ZlangInvokeFunction_rwlock_Unlock( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_rwlock	*rwlock_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
#if defined(__linux__)
	int					nret = 0 ;
	
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "pthread_rwlock_unlock ..." )
	nret = pthread_rwlock_unlock( & (rwlock_direct_prop->rwlock) ) ;
	if( nret )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "pthread_rwlock_unlock failed[%d]" , nret )
		CallRuntimeFunction_bool_SetBoolValue( rt , out1 , FALSE );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_UNLOCK_RWLOCK_FAILED );
	}
	
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "pthread_rwlock_unlock ok" )
	CallRuntimeFunction_bool_SetBoolValue( rt , out1 , TRUE );
#elif defined(_WIN32)
	if( rwlock_direct_prop->lock_type == LOCKTYPE_RDLOCK )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "ReleaseSRWLockShared ..." )
		ReleaseSRWLockShared( & (rwlock_direct_prop->rwlock) ) ;

		rwlock_direct_prop->lock_type = LOCKTYPE_NONE ;

		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "ReleaseSRWLockShared ok" )
		CallRuntimeFunction_bool_SetBoolValue( rt , out1 , TRUE );
	}
	else if( rwlock_direct_prop->lock_type == LOCKTYPE_WRLOCK )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "ReleaseSRWLockExclusive ..." )
		ReleaseSRWLockExclusive( & (rwlock_direct_prop->rwlock) ) ;

		rwlock_direct_prop->lock_type = LOCKTYPE_NONE ;

		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "ReleaseSRWLockExclusive ok" )
		CallRuntimeFunction_bool_SetBoolValue( rt , out1 , TRUE );
	}
#endif
	return 0;
}

ZlangCreateDirectPropertyFunction ZlangCreateDirectProperty_rwlock;
void *ZlangCreateDirectProperty_rwlock( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_rwlock	*rwlock_prop = NULL ;
	
	rwlock_prop = (struct ZlangDirectProperty_rwlock *)ZLMALLOC( sizeof(struct ZlangDirectProperty_rwlock) ) ;
	if( rwlock_prop == NULL )
	{
		SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_ALLOC , "alloc memory for entity" )
		return NULL;
	}
	memset( rwlock_prop , 0x00 , sizeof(struct ZlangDirectProperty_rwlock) );
	
#if defined(__linux__)
	pthread_rwlock_init( & (rwlock_prop->rwlock) , NULL );
#elif defined(_WIN32)
	InitializeSRWLock( & (rwlock_prop->rwlock) );
#endif
	
	return rwlock_prop;
}

ZlangDestroyDirectPropertyFunction ZlangDestroyDirectProperty_rwlock;
void ZlangDestroyDirectProperty_rwlock( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_rwlock	*rwlock_direct_prop = GetObjectDirectProperty(obj) ;
	
#if defined(__linux__)
	pthread_rwlock_destroy( &(rwlock_direct_prop->rwlock) );
#elif defined(_WIN32)
#endif
	
	ZLFREE( rwlock_direct_prop );
	
	return;
}

ZlangSummarizeDirectPropertySizeFunction ZlangSummarizeDirectPropertySize_rwlock;
void ZlangSummarizeDirectPropertySize_rwlock( struct ZlangRuntime *rt , struct ZlangObject *obj , size_t *summarized_obj_size , size_t *summarized_direct_prop_size )
{
	SUMMARIZE_SIZE( summarized_direct_prop_size , sizeof(struct ZlangDirectProperty_rwlock) )
	return;
}

static struct ZlangDirectFunctions direct_funcs_rwlock =
	{
		ZLANG_OBJECT_rwlock , /* char *tpye_name */
		
		ZlangCreateDirectProperty_rwlock , /* ZlangCreateDirectPropertyFunction *create_entity_func */
		ZlangDestroyDirectProperty_rwlock , /* ZlangDestroyDirectPropertyFunction *destroy_entity_func */
		
		NULL , /* ZlangFromCharPtrFunction *from_char_ptr_func */
		NULL , /* ZlangToStringFunction *to_string_func */
		NULL , /* ZlangFromDataPtrFunction *from_data_ptr_func */
		NULL , /* ZlangGetDataPtrFunction *get_data_ptr_func */
		
		NULL , /* ZlangOperatorFunction *oper_PLUS_func */
		NULL , /* ZlangOperatorFunction *oper_MINUS_func */
		NULL , /* ZlangOperatorFunction *oper_MUL_func */
		NULL , /* ZlangOperatorFunction *oper_DIV_func */
		NULL , /* ZlangOperatorFunction *oper_MOD_func */
		
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_NEGATIVE_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_NOT_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_BIT_REVERSE_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_PLUS_PLUS_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_MINUS_MINUS_func */
		
		NULL , /* ZlangCompareFunction *comp_EGUAL_func */
		NULL , /* ZlangCompareFunction *comp_NOTEGUAL_func */
		NULL , /* ZlangCompareFunction *comp_LT_func */
		NULL , /* ZlangCompareFunction *comp_LE_func */
		NULL , /* ZlangCompareFunction *comp_GT_func */
		NULL , /* ZlangCompareFunction *comp_GE_func */
		
		NULL , /* ZlangLogicFunction *logic_AND_func */
		NULL , /* ZlangLogicFunction *logic_OR_func */
		
		NULL , /* ZlangLogicFunction *bit_AND_func */
		NULL , /* ZlangLogicFunction *bit_XOR_func */
		NULL , /* ZlangLogicFunction *bit_OR_func */
		NULL , /* ZlangLogicFunction *bit_MOVELEFT_func */
		NULL , /* ZlangLogicFunction *bit_MOVERIGHT_func */
		
		ZlangSummarizeDirectPropertySize_rwlock , /* ZlangSummarizeDirectPropertySizeFunction *summarize_direct_prop_size_func */
	} ;

ZlangImportObjectFunction ZlangImportObject_rwlock;
struct ZlangObject *ZlangImportObject_rwlock( struct ZlangRuntime *rt )
{
	struct ZlangObject	*obj = NULL ;
	struct ZlangFunction	*func = NULL ;
	int			nret = 0 ;
	
	nret = ImportObject( rt , & obj , ZLANG_OBJECT_rwlock , & direct_funcs_rwlock , sizeof(struct ZlangDirectFunctions) , NULL ) ;
	if( nret )
	{
		SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_LINK_FUNC_TO_ENTITY , "import object to global objects heap" )
		return NULL;
	}
	
	/* rwlock.LockRead() */
	func = AddFunctionAndParametersInObject( rt , obj , "LockRead" , "LockRead()" , ZlangInvokeFunction_rwlock_LockRead , ZLANG_OBJECT_bool , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* rwlock.LockWrite() */
	func = AddFunctionAndParametersInObject( rt , obj , "LockWrite" , "LockWrite()" , ZlangInvokeFunction_rwlock_LockWrite , ZLANG_OBJECT_bool , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* rwlock.TryLockRead() */
	func = AddFunctionAndParametersInObject( rt , obj , "TryLockRead" , "TryLockRead()" , ZlangInvokeFunction_rwlock_TryLockRead , ZLANG_OBJECT_bool , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* rwlock.TryLockWrite() */
	func = AddFunctionAndParametersInObject( rt , obj , "TryLockWrite" , "TryLockWrite()" , ZlangInvokeFunction_rwlock_TryLockWrite , ZLANG_OBJECT_bool , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* rwlock.Unlock() */
	func = AddFunctionAndParametersInObject( rt , obj , "Unlock" , "Unlock()" , ZlangInvokeFunction_rwlock_Unlock , ZLANG_OBJECT_bool , NULL ) ;
	if( func == NULL )
		return NULL;
	
	return obj ;
}

