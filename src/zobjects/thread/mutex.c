/* Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "zobjects_thread.h"

ZlangFromDataPtrFunction ZlangFromDataPtr_mutex;

struct ZlangDirectProperty_mutex
{
	MUTEX		mutex ;
} ;

ZlangInvokeFunction ZlangInvokeFunction_mutex_Lock;
int ZlangInvokeFunction_mutex_Lock( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_mutex	*mutex_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;

#if defined(__linux__)
	int					nret = 0 ;
	
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "pthread_mutex_lock ..." )
	nret = pthread_mutex_lock( & (mutex_direct_prop->mutex) ) ;
	if( nret )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "pthread_mutex_lock failed[%d]" , nret )
		CallRuntimeFunction_bool_SetBoolValue( rt , out1 , FALSE );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_LOCK_MUTEX_FAILED );
	}
	
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "pthread_mutex_lock ok" )
	CallRuntimeFunction_bool_SetBoolValue( rt , out1 , TRUE );
#elif defined(_WIN32)
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "EnterCriticalSection ..." )
	EnterCriticalSection( & (mutex_direct_prop->mutex) ) ;
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "EnterCriticalSection ok" )
	CallRuntimeFunction_bool_SetBoolValue( rt , out1 , TRUE );
#endif
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_mutex_TryLock;
int ZlangInvokeFunction_mutex_TryLock( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_mutex	*mutex_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	
#if defined(__linux__)
	int					nret = 0 ;

	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "pthread_mutex_trylock ..." )
	nret = pthread_mutex_trylock( & (mutex_direct_prop->mutex) ) ;
	if( nret == EBUSY )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "pthread_mutex_trylock is busy" )
		UnreferObject( rt , out1 );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_TRYLOCK_MUTEX_BUSY );
	}
	else if( nret )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "pthread_mutex_trylock failed[%d]" , nret )
		CallRuntimeFunction_bool_SetBoolValue( rt , out1 , FALSE );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_TRYLOCK_MUTEX_FAILED );
	}
	
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "pthread_mutex_trylock ok" )
	CallRuntimeFunction_bool_SetBoolValue( rt , out1 , TRUE );
#elif defined(_WIN32)
	BOOL			bret = FALSE ;

	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "TryEnterCriticalSection ..." )
	bret = TryEnterCriticalSection( & (mutex_direct_prop->mutex) ) ;
	if( bret == FALSE )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "TryEnterCriticalSection is busy" )
		CallRuntimeFunction_bool_SetBoolValue( rt , out1 , FALSE );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_TRYLOCK_MUTEX_FAILED );
	}
	
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "TryEnterCriticalSection ok" )
	CallRuntimeFunction_bool_SetBoolValue( rt , out1 , TRUE );
#endif
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_mutex_Unlock;
int ZlangInvokeFunction_mutex_Unlock( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_mutex	*mutex_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;

#if defined(__linux__)
	int					nret = 0 ;
	
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "pthread_mutex_unlock ..." )
	nret = pthread_mutex_unlock( & (mutex_direct_prop->mutex) ) ;
	if( nret )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "pthread_mutex_unlock failed[%d]" , nret )
		CallRuntimeFunction_bool_SetBoolValue( rt , out1 , FALSE );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_UNLOCK_MUTEX_FAILED );
	}
	
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "pthread_mutex_unlock ok" )
	CallRuntimeFunction_bool_SetBoolValue( rt , out1 , TRUE );
#elif defined(_WIN32)
	int					nret = 0 ;
	
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "LeaveCriticalSection ..." )
	LeaveCriticalSection( & (mutex_direct_prop->mutex) ) ;
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "LeaveCriticalSection ok" )
	CallRuntimeFunction_bool_SetBoolValue( rt , out1 , TRUE );
#endif
	return 0;
}

ZlangCreateDirectPropertyFunction ZlangCreateDirectProperty_mutex;
void *ZlangCreateDirectProperty_mutex( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_mutex	*mutex_prop = NULL ;
	
	mutex_prop = (struct ZlangDirectProperty_mutex *)ZLMALLOC( sizeof(struct ZlangDirectProperty_mutex) ) ;
	if( mutex_prop == NULL )
	{
		SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_ALLOC , "alloc memory for entity" )
		return NULL;
	}
	memset( mutex_prop , 0x00 , sizeof(struct ZlangDirectProperty_mutex) );
	
#if defined(__linux__)
	pthread_mutex_init( & (mutex_prop->mutex) , NULL );
#elif defined(_WIN32)
	InitializeCriticalSection( & (mutex_prop->mutex) );
#endif
	
	return mutex_prop;
}

ZlangDestroyDirectPropertyFunction ZlangDestroyDirectProperty_mutex;
void ZlangDestroyDirectProperty_mutex( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_mutex	*mutex_direct_prop = GetObjectDirectProperty(obj) ;
	
#if defined(__linux__)
	pthread_mutex_destroy( & (mutex_direct_prop->mutex) );
#elif defined(_WIN32)
	DeleteCriticalSection( & (mutex_direct_prop->mutex) );
#endif
	
	ZLFREE( mutex_direct_prop );
	
	return;
}

ZlangSummarizeDirectPropertySizeFunction ZlangSummarizeDirectPropertySize_mutex;
void ZlangSummarizeDirectPropertySize_mutex( struct ZlangRuntime *rt , struct ZlangObject *obj , size_t *summarized_obj_size , size_t *summarized_direct_prop_size )
{
	SUMMARIZE_SIZE( summarized_direct_prop_size , sizeof(struct ZlangDirectProperty_mutex) )
	return;
}

static struct ZlangDirectFunctions direct_funcs_mutex =
	{
		ZLANG_OBJECT_mutex , /* char *tpye_name */
		
		ZlangCreateDirectProperty_mutex , /* ZlangCreateDirectPropertyFunction *create_entity_func */
		ZlangDestroyDirectProperty_mutex , /* ZlangDestroyDirectPropertyFunction *destroy_entity_func */
		
		NULL , /* ZlangFromCharPtrFunction *from_char_ptr_func */
		NULL , /* ZlangToStringFunction *to_string_func */
		NULL , /* ZlangFromDataPtrFunction *from_data_ptr_func */
		NULL , /* ZlangGetDataPtrFunction *get_data_ptr_func */
		
		NULL , /* ZlangOperatorFunction *oper_PLUS_func */
		NULL , /* ZlangOperatorFunction *oper_MINUS_func */
		NULL , /* ZlangOperatorFunction *oper_MUL_func */
		NULL , /* ZlangOperatorFunction *oper_DIV_func */
		NULL , /* ZlangOperatorFunction *oper_MOD_func */
		
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_NEGATIVE_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_NOT_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_BIT_REVERSE_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_PLUS_PLUS_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_MINUS_MINUS_func */
		
		NULL , /* ZlangCompareFunction *comp_EGUAL_func */
		NULL , /* ZlangCompareFunction *comp_NOTEGUAL_func */
		NULL , /* ZlangCompareFunction *comp_LT_func */
		NULL , /* ZlangCompareFunction *comp_LE_func */
		NULL , /* ZlangCompareFunction *comp_GT_func */
		NULL , /* ZlangCompareFunction *comp_GE_func */
		
		NULL , /* ZlangLogicFunction *logic_AND_func */
		NULL , /* ZlangLogicFunction *logic_OR_func */
		
		NULL , /* ZlangLogicFunction *bit_AND_func */
		NULL , /* ZlangLogicFunction *bit_XOR_func */
		NULL , /* ZlangLogicFunction *bit_OR_func */
		NULL , /* ZlangLogicFunction *bit_MOVELEFT_func */
		NULL , /* ZlangLogicFunction *bit_MOVERIGHT_func */
		
		ZlangSummarizeDirectPropertySize_mutex , /* ZlangSummarizeDirectPropertySizeFunction *summarize_direct_prop_size_func */
	} ;

ZlangDirectFunction_mutex_GetMutexPtr mutex_GetMutexPtr;
int mutex_GetMutexPtr( struct ZlangRuntime *rt , struct ZlangObject *obj , MUTEX **mutex )
{
	struct ZlangDirectProperty_mutex	*mutex_direct_prop = GetObjectDirectProperty(obj) ;
	
	if( IsObjectPropertiesEntityNull(obj) )
		(*mutex) = NULL ;
	else
		(*mutex) = & (mutex_direct_prop->mutex) ;
	
	return 0;
}

ZlangImportObjectFunction ZlangImportObject_mutex;
struct ZlangObject *ZlangImportObject_mutex( struct ZlangRuntime *rt )
{
	struct ZlangObject	*obj = NULL ;
	struct ZlangFunction	*func = NULL ;
	int			nret = 0 ;
	
	nret = ImportObject( rt , & obj , ZLANG_OBJECT_mutex , & direct_funcs_mutex , sizeof(struct ZlangDirectFunctions) , NULL ) ;
	if( nret )
	{
		SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_LINK_FUNC_TO_ENTITY , "import object to global objects heap" )
		return NULL;
	}
	
	/* mutex.Lock() */
	func = AddFunctionAndParametersInObject( rt , obj , "Lock" , "Lock()" , ZlangInvokeFunction_mutex_Lock , ZLANG_OBJECT_bool , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* mutex.TryLock() */
	func = AddFunctionAndParametersInObject( rt , obj , "TryLock" , "TryLock()" , ZlangInvokeFunction_mutex_TryLock , ZLANG_OBJECT_bool , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* mutex.Unlock() */
	func = AddFunctionAndParametersInObject( rt , obj , "Unlock" , "Unlock()" , ZlangInvokeFunction_mutex_Unlock , ZLANG_OBJECT_bool , NULL ) ;
	if( func == NULL )
		return NULL;
	
	SetRuntimeFunction_mutex_GetMutexPtr( rt , mutex_GetMutexPtr );
	
	return obj ;
}

