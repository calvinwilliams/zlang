/* Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "zobjects_thread.h"

#if defined(__linux__)
#elif defined(_WIN32)
#include <process.h>
#endif

ZlangFromDataPtrFunction ZlangFromDataPtr_thread;

struct ZlangDirectProperty_thread
{
	struct ZlangObject	*funcp_obj ;
	unsigned char		is_daemon ;
	
	struct ZlangRuntime	*rt ;
	
	THREAD			tid ;
#if defined(__linux__)
	int			ret_code ;
#elif defined(_WIN32)
	DWORD			ret_code ;
#endif
} ;

struct ThreadEntryData
{
	int32_t					is_daemon ;
	
	struct ZlangRuntime			*rt ;
	struct ZlangFunction			*func ;
	int					recursive_depth ;
	struct ZlangDirectProperty_thread	*thread_direct_prop ;
} ;

#if defined(__linux__)
void *ThreadEntryFunc( void *p )
#elif defined(_WIN32)
unsigned __stdcall ThreadEntryFunc( void *p )
#endif
{
	struct ThreadEntryData	*entry_data = (struct ThreadEntryData *) p ;
	
	SetRuntimeRecursiveDepth( entry_data->rt , entry_data->recursive_depth );
	
#if defined(__linux__)
	if( entry_data->is_daemon == 1 )
	{
		pthread_detach( pthread_self() );
	}
#endif
	
	TEST_RUNTIME_DEBUG_THEN_PRINT( entry_data->rt , "InvokeEntryFunction ..." );
	entry_data->thread_direct_prop->ret_code = InvokeEntryFunction( entry_data->rt , NULL , entry_data->func ) ;
	TEST_RUNTIME_DEBUG_THEN_PRINT( entry_data->rt , "InvokeEntryFunction return[%d]" , entry_data->thread_direct_prop->ret_code );
	
	if( entry_data->is_daemon == 1 )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( entry_data->rt , "FreeRuntime" );
		FreeRuntime( entry_data->rt ); entry_data->rt = NULL ;
	}
	
	free( entry_data );
	
#if defined(__linux__)
	return NULL;
#elif defined(_WIN32)
	_endthreadex( 0 );
	return 0;
#endif
}

ZlangInvokeFunction ZlangInvokeFunction_thread_SetFunctionEntry;
int ZlangInvokeFunction_thread_SetFunctionEntry( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_thread	*thread_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangFunction			*func = NULL ;
	
	thread_direct_prop->funcp_obj = CloneFunctionPtrObject( rt , NULL ) ;
	if( thread_direct_prop->funcp_obj == NULL )
		return ThrowFatalException( rt , ZLANG_ERROR_INTERNAL , EXCEPTION_MESSAGE_INTERNAL_ERROR );
	
	CallRuntimeFunction_functionptr_GetFunctionPtr( rt , in1 , & func );
	CallRuntimeFunction_functionptr_SetFunctionPtr( rt , thread_direct_prop->funcp_obj , func );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_thread_SetDaemon_bool;
int ZlangInvokeFunction_thread_SetDaemon_bool( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_thread	*thread_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	
	CallRuntimeFunction_bool_GetBoolValue( rt , in1 , & ( thread_direct_prop->is_daemon ) );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_thread_Start;
int ZlangInvokeFunction_thread_Start( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_thread	*thread_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	struct ZlangFunction			*func = NULL ;
	struct ThreadEntryData			*entry_data = NULL ;
#if defined(__linux__)
	int32_t					pthread_create_return ;
#endif
	int					nret = 0 ;
	
	if( thread_direct_prop->funcp_obj == NULL )
	{
		SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_FATAL_INVOKE_METHOD_RETURN , "need call SetFunctionEntry before call Start or StartThread" )
		return ThrowFatalException( rt , ZLANG_FATAL_INVOKE_METHOD_RETURN , EXCEPTION_MESSAGE_GENERAL_ERROR );
	}
	
	CallRuntimeFunction_functionptr_GetFunctionPtr( rt , thread_direct_prop->funcp_obj , & func );
	
	nret = CheckInputParamterStackAndFunctionParameters( rt , func ) ;
	if( nret )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "CheckInputParamterStackAndFunctionParameters ok" )
		return ThrowFatalException( rt , nret , EXCEPTION_MESSAGE_GENERAL_ERROR );
	}
	
	thread_direct_prop->rt = DuplicateRuntime( rt , func ) ;
	if( thread_direct_prop->rt == NULL )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "DuplicateRuntime failed[%d]" , GetRuntimeErrorNo(rt) )
		UnreferObject( rt , out1 );
		return ThrowFatalException( rt , ZLANG_FATAL_INVOKE_METHOD_RETURN , EXCEPTION_MESSAGE_GENERAL_ERROR );
	}
	
	entry_data = (struct ThreadEntryData *)malloc( sizeof(struct ThreadEntryData) ) ;
	if( entry_data == NULL )
	{
		SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_ALLOC , "alloc thread entry data failed , errno[%d]" , errno )
		return ThrowFatalException( rt , ZLANG_ERROR_ALLOC , EXCEPTION_MESSAGE_ALLOC_FAILED );
	}
	memset( entry_data , 0x00 , sizeof(struct ThreadEntryData) );
	entry_data->is_daemon = thread_direct_prop->is_daemon ;
	entry_data->rt = thread_direct_prop->rt ;
	entry_data->func = func ;
	entry_data->recursive_depth = GetRuntimeRecursiveDepth(rt) ;
	entry_data->thread_direct_prop = thread_direct_prop ;
	
#if defined(__linux__)
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "pthread_create ..." );
	pthread_create_return = pthread_create( & (thread_direct_prop->tid) , NULL , ThreadEntryFunc , entry_data ) ;
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "pthread_create return[%"PRIi32"] , new thread[%010lu]" , pthread_create_return , thread_direct_prop->tid );
	CallRuntimeFunction_int_SetIntValue( rt , out1 , pthread_create_return );
	if( pthread_create_return == -1 )
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_CREATE_THREAD_FAILED );
#elif defined(_WIN32)
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "_beginthread ..." );
	thread_direct_prop->tid = (HANDLE)_beginthreadex( NULL , 0 , ThreadEntryFunc , entry_data , 0 , NULL ) ;
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "_beginthread return , new thread[%p]" , thread_direct_prop->tid );
	CallRuntimeFunction_int_SetIntValue( rt , out1 , 0 );
#endif
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_thread_Join;
int ZlangInvokeFunction_thread_Join( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_thread	*thread_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	int					param_count ;
	int					param_index ;
	struct ZlangObject			*out = NULL ;
	struct ZlangObject			*in = NULL ;
	void					*value = NULL ;
	int32_t					value_len ;
	
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "thread_direct_prop->is_daemon[%"PRIi32"]" , thread_direct_prop->is_daemon );
	if( thread_direct_prop->is_daemon == 1 )
	{
		CallRuntimeFunction_int_SetIntValue( rt , out1 , -1 );
		return 0;
	}
	
#if defined(__linux__)
	int32_t					pthread_join_return ;

	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "pthread_join(%010lu) ..." , thread_direct_prop->tid );
	pthread_join_return = pthread_join( thread_direct_prop->tid , NULL ) ;
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "pthread_join return[%"PRIi32"] ret_code[%d]" , pthread_join_return , thread_direct_prop->ret_code );
	CallRuntimeFunction_int_SetIntValue( rt , out1 , pthread_join_return );
	if( pthread_join_return == -1 )
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_JOIN_THREAD_FAILED );
	if( thread_direct_prop->ret_code )
	{
		if( thread_direct_prop->ret_code != ZLANG_INFO_EXIT )
		{
			return ThrowFatalException( rt , ZLANG_FATAL_INVOKE_METHOD_RETURN , EXCEPTION_MESSAGE_GENERAL_ERROR );
		}
	}
#elif defined(_WIN32)
	int32_t					WaitForSingleObject_return ;

	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "WaitForSingleObject(%p) ..." , thread_direct_prop->tid );
	WaitForSingleObject_return = WaitForSingleObject( thread_direct_prop->tid , INFINITE ) ;
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "WaitForSingleObject return[%ld] ret_code[%d]" , WaitForSingleObject_return , thread_direct_prop->ret_code );
	CloseHandle( thread_direct_prop->tid );
	CallRuntimeFunction_int_SetIntValue( rt , out1 , WaitForSingleObject_return );
	if( WaitForSingleObject_return == WAIT_FAILED )
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_JOIN_THREAD_FAILED );
	if( thread_direct_prop->ret_code )
	{
		if( thread_direct_prop->ret_code != ZLANG_INFO_EXIT )
		{
			return ThrowFatalException( rt , ZLANG_FATAL_INVOKE_METHOD_RETURN , EXCEPTION_MESSAGE_GENERAL_ERROR );
		}
	}
#endif
	
	param_count = GetOutputParameterCountInLocalObjectStack( thread_direct_prop->rt );
	param_index = 0 ;
	for( ; ; )
	{
		out = GetOutputParameterInLocalObjectStack( thread_direct_prop->rt , param_index+1 ) ;
		if( out == NULL )
			break;
		TEST_RUNTIME_DEBUG( rt ) { PRINT_TABS(rt) printf( "get dup stack out param index[%d] , " , param_index+1 ); DebugPrintObject( rt , out ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE }
		
		param_index++;
		
		in = GetInputParameterInLocalObjectStack( rt , param_index ) ;
		if( in == NULL )
			break;
		
		if( STRCMP( GetCloneObjectName(out) , != , GetCloneObjectName(in) ) )
		{
			SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_FUNC_PARAMETER_TYPE_NOT_MATCHED , "out-param ancestor name[%s] not matched with func-out-param parent name[%s]" , GetCloneObjectName(out) , GetCloneObjectName(in) )
			return ThrowFatalException( rt , ZLANG_FATAL_INVOKE_METHOD_RETURN , EXCEPTION_MESSAGE_GENERAL_ERROR );
		}
		
		GetDataPtr( rt , out , & value , & value_len );
		FromDataPtr( rt , in , value , value_len );
		TEST_RUNTIME_DEBUG( rt ) { PRINT_TABS(rt) printf( "put stack in param index[%d] , " , param_index ); DebugPrintObject( rt , in ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE }
	}
	if( param_index != param_count )
	{
		SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_FUNC_PARAMETERS_COUNT_NOT_MATCHED , "thread func out parameters count not matched with thread.Join in parameters" )
		return ThrowFatalException( rt , ZLANG_FATAL_INVOKE_METHOD_RETURN , EXCEPTION_MESSAGE_GENERAL_ERROR );
	}
	
	if( thread_direct_prop->is_daemon == 0 )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "FreeRuntime" );
		FreeRuntime( thread_direct_prop->rt ); thread_direct_prop->rt = NULL ;
	}
	
	return 0;
}

ZlangCreateDirectPropertyFunction ZlangCreateDirectProperty_thread;
void *ZlangCreateDirectProperty_thread( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_thread	*thread_prop = NULL ;
	
	thread_prop = (struct ZlangDirectProperty_thread *)ZLMALLOC( sizeof(struct ZlangDirectProperty_thread) ) ;
	if( thread_prop == NULL )
	{
		SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_ALLOC , "alloc memory for entity" )
		return NULL;
	}
	memset( thread_prop , 0x00 , sizeof(struct ZlangDirectProperty_thread) );
	
	return thread_prop;
}

ZlangDestroyDirectPropertyFunction ZlangDestroyDirectProperty_thread;
void ZlangDestroyDirectProperty_thread( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_thread	*thread_direct_prop = GetObjectDirectProperty(obj) ;
	
	if( thread_direct_prop->funcp_obj )
	{
		DestroyObject( rt , thread_direct_prop->funcp_obj ); thread_direct_prop->funcp_obj = NULL ;
	}
	
	ZLFREE( thread_direct_prop );
	
	return;
}

ZlangSummarizeDirectPropertySizeFunction ZlangSummarizeDirectPropertySize_thread;
void ZlangSummarizeDirectPropertySize_thread( struct ZlangRuntime *rt , struct ZlangObject *obj , size_t *summarized_obj_size , size_t *summarized_direct_prop_size )
{
	struct ZlangDirectProperty_thread	*thread_direct_prop = GetObjectDirectProperty(obj) ;
	
	if( thread_direct_prop->funcp_obj )
		SummarizeObjectSize( rt , thread_direct_prop->funcp_obj , summarized_obj_size , summarized_direct_prop_size );
	
	SUMMARIZE_SIZE( summarized_direct_prop_size , sizeof(struct ZlangDirectProperty_thread) )
	
	return;
}

static struct ZlangDirectFunctions direct_funcs_thread =
	{
		ZLANG_OBJECT_thread , /* char *tpye_name */
		
		ZlangCreateDirectProperty_thread , /* ZlangCreateDirectPropertyFunction *create_entity_func */
		ZlangDestroyDirectProperty_thread , /* ZlangDestroyDirectPropertyFunction *destroy_entity_func */
		
		NULL , /* ZlangFromCharPtrFunction *from_char_ptr_func */
		NULL , /* ZlangToStringFunction *to_string_func */
		NULL , /* ZlangFromDataPtrFunction *from_data_ptr_func */
		NULL , /* ZlangGetDataPtrFunction *get_data_ptr_func */
		
		NULL , /* ZlangOperatorFunction *oper_PLUS_func */
		NULL , /* ZlangOperatorFunction *oper_MINUS_func */
		NULL , /* ZlangOperatorFunction *oper_MUL_func */
		NULL , /* ZlangOperatorFunction *oper_DIV_func */
		NULL , /* ZlangOperatorFunction *oper_MOD_func */
		
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_NEGATIVE_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_NOT_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_BIT_REVERSE_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_PLUS_PLUS_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_MINUS_MINUS_func */
		
		NULL , /* ZlangCompareFunction *comp_EGUAL_func */
		NULL , /* ZlangCompareFunction *comp_NOTEGUAL_func */
		NULL , /* ZlangCompareFunction *comp_LT_func */
		NULL , /* ZlangCompareFunction *comp_LE_func */
		NULL , /* ZlangCompareFunction *comp_GT_func */
		NULL , /* ZlangCompareFunction *comp_GE_func */
		
		NULL , /* ZlangLogicFunction *logic_AND_func */
		NULL , /* ZlangLogicFunction *logic_OR_func */
		
		NULL , /* ZlangLogicFunction *bit_AND_func */
		NULL , /* ZlangLogicFunction *bit_XOR_func */
		NULL , /* ZlangLogicFunction *bit_OR_func */
		NULL , /* ZlangLogicFunction *bit_MOVELEFT_func */
		NULL , /* ZlangLogicFunction *bit_MOVERIGHT_func */
		
		ZlangSummarizeDirectPropertySize_thread , /* ZlangSummarizeDirectPropertySizeFunction *summarize_direct_prop_size_func */
	} ;

ZlangImportObjectFunction ZlangImportObject_thread;
struct ZlangObject *ZlangImportObject_thread( struct ZlangRuntime *rt )
{
	struct ZlangObject	*obj = NULL ;
	struct ZlangObject	*prop = NULL ;
	struct ZlangFunction	*func = NULL ;
	int			nret = 0 ;
	
	nret = ImportObject( rt , & obj , ZLANG_OBJECT_thread , & direct_funcs_thread , sizeof(struct ZlangDirectFunctions) , NULL ) ;
	if( nret )
	{
		SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_LINK_FUNC_TO_ENTITY , "import object to global objects heap" )
		return NULL;
	}
	
	/* bool thread.DAEMON_THREAD */
	prop = AddPropertyInObject( rt , obj , QueryObjectByObjectName(rt,"bool") , "DAEMON_THREAD") ;
	if( prop == NULL )
		return NULL;
	CallRuntimeFunction_bool_SetBoolValue( rt , prop , TRUE );
	SetConstantObject( prop );
	
	/* bool thread.JOIN_THREAD */
	prop = AddPropertyInObject( rt , obj , QueryObjectByObjectName(rt,"bool") , "JOIN_THREAD") ;
	if( prop == NULL )
		return NULL;
	CallRuntimeFunction_bool_SetBoolValue( rt , prop , FALSE );
	SetConstantObject( prop );
	
	/* thread.SetFunctionEntry() */
	func = AddFunctionAndParametersInObject( rt , obj , "SetFunctionEntry" , "SetFunctionEntry(functionptr)" , ZlangInvokeFunction_thread_SetFunctionEntry , ZLANG_OBJECT_int , ZLANG_OBJECT_functionptr,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* thread.SetDaemon() */
	func = AddFunctionAndParametersInObject( rt , obj , "SetDaemon" , "SetDaemon(bool)" , ZlangInvokeFunction_thread_SetDaemon_bool , ZLANG_OBJECT_void , ZLANG_OBJECT_bool,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* thread.Start() */
	func = AddFunctionAndParametersInObject( rt , obj , "Start" , "Start(...)" , ZlangInvokeFunction_thread_Start , ZLANG_OBJECT_int , ZLANG_OBJECT_vargs,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* thread.Join() */
	func = AddFunctionAndParametersInObject( rt , obj , "Join" , "Join(...)" , ZlangInvokeFunction_thread_Join , ZLANG_OBJECT_int , ZLANG_OBJECT_vargs,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	return obj ;
}

