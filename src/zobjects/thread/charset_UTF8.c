﻿/* Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

static struct ZlangCharsetAlias		g_zlang_charset_aliases_UTF8[] = {
		{ ZLANG_CHARSET_UTF8 , "线程" , "thread" } ,
		{ ZLANG_CHARSET_UTF8 , "守护线程" , "DAEMON_THREAD" } ,
		{ ZLANG_CHARSET_UTF8 , "可合并线程" , "JOIN_THREAD" } ,
		{ ZLANG_CHARSET_UTF8 , "设置线程主函数" , "SetFunctionEntry" } ,
		{ ZLANG_CHARSET_UTF8 , "设置为守护线程" , "SetDaemon" } ,
		{ ZLANG_CHARSET_UTF8 , "开始线程" , "Start" } ,
		{ ZLANG_CHARSET_UTF8 , "合并线程" , "Join" } ,
		{ 0 , NULL , NULL } ,
	} ;

