/* Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef _H_LIBZLANG_THREAD_
#define _H_LIBZLANG_THREAD_

#include "zlang.h"

#if defined(__linux__)
#include <pthread.h>
#elif defined(_WIN32)
#include <windows.h>
#endif

struct ZlangObject_thread;

#define ZLANG_OBJECT_thread	"thread"

#define ZLANG_OBJECT_rwlock	"rwlock"
#define ZLANG_OBJECT_barrier	"barrier"

#define EXCEPTION_MESSAGE_CREATE_THREAD_FAILED	"create thread failed"
#define EXCEPTION_MESSAGE_JOIN_THREAD_FAILED	"join thread failed"
#define EXCEPTION_MESSAGE_INIT_BARRIER_FAILED	"init barrier failed"
#define EXCEPTION_MESSAGE_BARRIER_NOT_INIT	"barrier not init"
#define EXCEPTION_MESSAGE_WAIT_BARRIER_FAILED	"wait barrier failed"
#define EXCEPTION_MESSAGE_LOCK_MUTEX_FAILED	"lock mutex failed"
#define EXCEPTION_MESSAGE_TRYLOCK_MUTEX_FAILED	"trylock mutex failed"
#define EXCEPTION_MESSAGE_TRYLOCK_MUTEX_BUSY	"trylock mutex busy"
#define EXCEPTION_MESSAGE_UNLOCK_MUTEX_FAILED	"unlock mutex failed"
#define EXCEPTION_MESSAGE_WAIT_CONDITION_FAILED	"wait condition failed"
#define EXCEPTION_MESSAGE_TIMEWAIT_CONDITION_TIMEOUT	"timewait condition timeout"
#define EXCEPTION_MESSAGE_TIMEWAIT_CONDITION_FAILED	"timewait condition failed"
#define EXCEPTION_MESSAGE_SIGNAL_CONDITION_FAILED	"signal condition failed"
#define EXCEPTION_MESSAGE_BROADCAST_CONDITION_FAILED	"broadcast condition failed"
#define EXCEPTION_MESSAGE_RDLOCK_RWLOCK_FAILED		"rdlock rwlock failed"
#define EXCEPTION_MESSAGE_WRLOCK_RWLOCK_FAILED		"wrlock rwlock failed"
#define EXCEPTION_MESSAGE_TRYRDLOCK_RWLOCK_BUSY		"tryrdlock rwlock busy"
#define EXCEPTION_MESSAGE_TRYRDLOCK_RWLOCK_FAILED	"tryrdlock rwlock failed"
#define EXCEPTION_MESSAGE_TRYWRLOCK_RWLOCK_BUSY		"trywrlock rwlock busy"
#define EXCEPTION_MESSAGE_TRYWRLOCK_RWLOCK_FAILED	"trywrlock rwlock failed"
#define EXCEPTION_MESSAGE_UNLOCK_RWLOCK_FAILED		"unlock rwlock failed"

DLLEXPORT ZlangImportObjectsFunction ZlangImportObjects;
int ZlangImportObjects( struct ZlangRuntime *rt );

#endif

