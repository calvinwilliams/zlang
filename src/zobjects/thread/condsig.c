/* Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "zobjects_thread.h"

ZlangFromDataPtrFunction ZlangFromDataPtr_condsig;

struct ZlangDirectProperty_condsig
{
#if defined(__linux__)
	pthread_cond_t		cond ;
	MUTEX			mutex ;
#elif defined(_WIN32)
	CONDITION_VARIABLE	cond ;
	CRITICAL_SECTION	mutex ;
#endif
} ;

ZlangInvokeFunction ZlangInvokeFunction_condsig_Lock;
int ZlangInvokeFunction_condsig_Lock( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_condsig	*condsig_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	int					nret = 0 ;
	
#if defined(__linux__)
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "pthread_mutex_lock ..." )
	nret = pthread_mutex_lock( & (condsig_direct_prop->mutex) ) ;
	if( nret )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "pthread_mutex_lock failed[%d]" , nret )
		CallRuntimeFunction_bool_SetBoolValue( rt , out1 , FALSE );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_LOCK_MUTEX_FAILED );
	}
#elif defined(_WIN32)
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "EnterCriticalSection ..." )
	EnterCriticalSection( & (condsig_direct_prop->mutex) ) ;
#endif
	
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "pthread_mutex_lock ok" )
	CallRuntimeFunction_bool_SetBoolValue( rt , out1 , TRUE );
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_condsig_Unlock;
int ZlangInvokeFunction_condsig_Unlock( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_condsig	*condsig_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	int					nret = 0 ;
	
#if defined(__linux__)
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "pthread_mutex_unlock ..." )
	nret = pthread_mutex_unlock( & (condsig_direct_prop->mutex) ) ;
	if( nret )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "pthread_mutex_unlock failed[%d]" , nret )
		CallRuntimeFunction_bool_SetBoolValue( rt , out1 , FALSE );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_UNLOCK_MUTEX_FAILED );
	}
#elif defined(_WIN32)
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "LeaveCriticalSection ..." )
	LeaveCriticalSection( & (condsig_direct_prop->mutex) ) ;
#endif

	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "pthread_mutex_unlock ok" )
	CallRuntimeFunction_bool_SetBoolValue( rt , out1 , TRUE );
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_condsig_Wait;
int ZlangInvokeFunction_condsig_Wait( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_condsig	*condsig_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	
#if defined(__linux__)
	int					nret = 0 ;

	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "pthread_cond_wait ..." )
	nret = pthread_cond_wait( & (condsig_direct_prop->cond) , & (condsig_direct_prop->mutex) ) ;
	if( nret )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "pthread_cond_wait failed[%d]" , nret )
		CallRuntimeFunction_bool_SetBoolValue( rt , out1 , FALSE );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_WAIT_CONDITION_FAILED );
	}
#elif defined(_WIN32)
	BOOL					bret = FALSE ;

	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "SleepConditionVariableCS ..." )
	bret = SleepConditionVariableCS( & (condsig_direct_prop->cond) , & (condsig_direct_prop->mutex) , INFINITE ) ;
	if( bret != TRUE )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "SleepConditionVariableCS failed" )
		CallRuntimeFunction_bool_SetBoolValue( rt , out1 , FALSE );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_WAIT_CONDITION_FAILED );
	}
#endif
	
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "pthread_cond_wait ok" )
	CallRuntimeFunction_bool_SetBoolValue( rt , out1 , TRUE );
	return 0;
}

#if defined(__linux__)
inline long long ts_to_ns( struct timespec ts )
{
	return ts.tv_sec * 1000000000 + ts.tv_nsec ;
}

inline struct timespec ns_to_ts( long long ns )
{
	struct timespec		ts ;
	
	memset( & ts , 0x00 , sizeof(struct timespec) );
	ts.tv_sec = ns / 1000000000 ;
	ts.tv_nsec = ns % 1000000000 ;
	
	return ts;
}
#endif

ZlangInvokeFunction ZlangInvokeFunction_condsig_TimedWait;
int ZlangInvokeFunction_condsig_TimedWait( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_condsig	*condsig_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject			*in2 = GetInputParameterInLocalObjectStack(rt,2) ;
	struct ZlangObject			*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	int32_t					seconds ;
	int64_t					nano_seconds ;
#if defined(__linux__)
	int					nret = 0 ;
#elif defined(_WIN32)
#endif
	
	CallRuntimeFunction_int_GetIntValue( rt , in1 , & seconds );
	CallRuntimeFunction_long_GetLongValue( rt , in2 , & nano_seconds );
	
#if defined(__linux__)
	struct timespec				timeout_ts ;

	clock_gettime( CLOCK_MONOTONIC , & timeout_ts );
	timeout_ts.tv_sec += seconds ;
	timeout_ts.tv_nsec += nano_seconds ;
	if( nano_seconds >= 1000000000 )
	{
		timeout_ts.tv_sec += nano_seconds / 1000000000;
		timeout_ts.tv_nsec = nano_seconds % 1000000000 ;
	}
	
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "pthread_cond_timedwait ..." )
	nret = pthread_cond_timedwait( & (condsig_direct_prop->cond) , & (condsig_direct_prop->mutex) , & timeout_ts ) ;
	if( nret == ETIMEDOUT )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "pthread_cond_timedwait timeout" )
		UnreferObject( rt , out1 );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_TIMEWAIT_CONDITION_TIMEOUT );
	}
	else if( nret )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "pthread_cond_timedwait failed[%d]" , nret )
		CallRuntimeFunction_bool_SetBoolValue( rt , out1 , FALSE );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_TIMEWAIT_CONDITION_FAILED );
	}
#elif defined(_WIN32)
	DWORD					dwMilliseconds ;
	BOOL					bret = FALSE ;

	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "SleepConditionVariableCS ..." )
	dwMilliseconds = seconds*1000 + nano_seconds/1000000 ;
	bret = SleepConditionVariableCS( & (condsig_direct_prop->cond) , & (condsig_direct_prop->mutex) , dwMilliseconds ) ;
	if( bret != TRUE )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "SleepConditionVariableCS failed" )
		CallRuntimeFunction_bool_SetBoolValue( rt , out1 , FALSE );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_TIMEWAIT_CONDITION_FAILED );
	}
#endif
	
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "pthread_cond_timedwait ok" )
	CallRuntimeFunction_bool_SetBoolValue( rt , out1 , TRUE );
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_condsig_Signal;
int ZlangInvokeFunction_condsig_Signal( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_condsig	*condsig_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
#if defined(__linux__)
	int					nret = 0 ;
	
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "pthread_cond_signal ..." )
	nret = pthread_cond_signal( & (condsig_direct_prop->cond) ) ;
	if( nret )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "pthread_cond_signal failed[%d]" , nret )
		CallRuntimeFunction_bool_SetBoolValue( rt , out1 , FALSE );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_SIGNAL_CONDITION_FAILED );
	}
	else
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "pthread_cond_signal ok" )
		CallRuntimeFunction_bool_SetBoolValue( rt , out1 , TRUE );
	}
#elif defined(_WIN32)
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "WakeConditionVariable ..." )
	WakeConditionVariable( & (condsig_direct_prop->cond) ) ;
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "WakeConditionVariable ok" )
	CallRuntimeFunction_bool_SetBoolValue( rt , out1 , TRUE );
#endif
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_condsig_Broadcast;
int ZlangInvokeFunction_condsig_Broadcast( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_condsig	*condsig_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;

#if defined(__linux__)
	int					nret = 0 ;
	
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "pthread_cond_broadcast ..." )
	nret = pthread_cond_broadcast( & (condsig_direct_prop->cond) ) ;
	if( nret )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "pthread_cond_broadcast failed[%d]" , nret )
		CallRuntimeFunction_bool_SetBoolValue( rt , out1 , FALSE );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_BROADCAST_CONDITION_FAILED );
	}
	else
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "pthread_cond_broadcast ok" )
		CallRuntimeFunction_bool_SetBoolValue( rt , out1 , TRUE );
	}
#elif defined(_WIN32)
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "WakeAllConditionVariable ..." )
	WakeAllConditionVariable( & (condsig_direct_prop->cond) ) ;
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "WakeAllConditionVariable ok" )
	CallRuntimeFunction_bool_SetBoolValue( rt , out1 , TRUE );
#endif

	return 0;
}

ZlangCreateDirectPropertyFunction ZlangCreateDirectProperty_condsig;
void *ZlangCreateDirectProperty_condsig( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_condsig	*condsig_prop = NULL ;
#if defined(__linux__)
	pthread_condattr_t			attr ;
#endif
	
	condsig_prop = (struct ZlangDirectProperty_condsig *)ZLMALLOC( sizeof(struct ZlangDirectProperty_condsig) ) ;
	if( condsig_prop == NULL )
	{
		SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_ALLOC , "alloc memory for entity" )
		return NULL;
	}
	memset( condsig_prop , 0x00 , sizeof(struct ZlangDirectProperty_condsig) );
	
#if defined(__linux__)
	pthread_condattr_init( & attr );
	pthread_condattr_setclock( & attr , CLOCK_MONOTONIC );
	pthread_cond_init( & (condsig_prop->cond) , & attr );
	pthread_condattr_destroy( & attr );

	pthread_mutex_init( & (condsig_prop->mutex) , NULL );
#elif defined(_WIN32)
	InitializeConditionVariable( & (condsig_prop->cond) );

	InitializeCriticalSection( & (condsig_prop->mutex) );
#endif
	
	return condsig_prop;
}

ZlangDestroyDirectPropertyFunction ZlangDestroyDirectProperty_condsig;
void ZlangDestroyDirectProperty_condsig( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_condsig	*condsig_direct_prop = GetObjectDirectProperty(obj) ;
	
#if defined(__linux__)
	pthread_cond_destroy( & (condsig_direct_prop->cond) );
	pthread_mutex_destroy( & (condsig_direct_prop->mutex) );
#elif defined(_WIN32)
#endif

	ZLFREE( condsig_direct_prop );
	
	return;
}

ZlangSummarizeDirectPropertySizeFunction ZlangSummarizeDirectPropertySize_condsig;
void ZlangSummarizeDirectPropertySize_condsig( struct ZlangRuntime *rt , struct ZlangObject *obj , size_t *summarized_obj_size , size_t *summarized_direct_prop_size )
{
	SUMMARIZE_SIZE( summarized_direct_prop_size , sizeof(struct ZlangDirectProperty_condsig) )
	return;
}

static struct ZlangDirectFunctions direct_funcs_condsig =
	{
		ZLANG_OBJECT_condsig , /* char *tpye_name */
		
		ZlangCreateDirectProperty_condsig , /* ZlangCreateDirectPropertyFunction *create_entity_func */
		ZlangDestroyDirectProperty_condsig , /* ZlangDestroyDirectPropertyFunction *destroy_entity_func */
		
		NULL , /* ZlangFromCharPtrFunction *from_char_ptr_func */
		NULL , /* ZlangToStringFunction *to_string_func */
		NULL , /* ZlangFromDataPtrFunction *from_data_ptr_func */
		NULL , /* ZlangGetDataPtrFunction *get_data_ptr_func */
		
		NULL , /* ZlangOperatorFunction *oper_PLUS_func */
		NULL , /* ZlangOperatorFunction *oper_MINUS_func */
		NULL , /* ZlangOperatorFunction *oper_MUL_func */
		NULL , /* ZlangOperatorFunction *oper_DIV_func */
		NULL , /* ZlangOperatorFunction *oper_MOD_func */
		
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_NEGATIVE_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_NOT_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_BIT_REVERSE_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_PLUS_PLUS_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_MINUS_MINUS_func */
		
		NULL , /* ZlangCompareFunction *comp_EGUAL_func */
		NULL , /* ZlangCompareFunction *comp_NOTEGUAL_func */
		NULL , /* ZlangCompareFunction *comp_LT_func */
		NULL , /* ZlangCompareFunction *comp_LE_func */
		NULL , /* ZlangCompareFunction *comp_GT_func */
		NULL , /* ZlangCompareFunction *comp_GE_func */
		
		NULL , /* ZlangLogicFunction *logic_AND_func */
		NULL , /* ZlangLogicFunction *logic_OR_func */
		
		NULL , /* ZlangLogicFunction *bit_AND_func */
		NULL , /* ZlangLogicFunction *bit_XOR_func */
		NULL , /* ZlangLogicFunction *bit_OR_func */
		NULL , /* ZlangLogicFunction *bit_MOVELEFT_func */
		NULL , /* ZlangLogicFunction *bit_MOVERIGHT_func */
		
		ZlangSummarizeDirectPropertySize_condsig , /* ZlangSummarizeDirectPropertySizeFunction *summarize_direct_prop_size_func */
	} ;

ZlangDirectFunction_condsig_GetMutexPtr mutex_GetMutexPtr;
int condsig_GetMutexPtr( struct ZlangRuntime *rt , struct ZlangObject *obj , MUTEX **mutex )
{
	struct ZlangDirectProperty_condsig	*condsig_direct_prop = GetObjectDirectProperty(obj) ;
	
	if( IsObjectPropertiesEntityNull(obj) )
		(*mutex) = NULL ;
	else
		(*mutex) = & (condsig_direct_prop->mutex) ;
	
	return 0;
}

ZlangImportObjectFunction ZlangImportObject_condsig;
struct ZlangObject *ZlangImportObject_condsig( struct ZlangRuntime *rt )
{
	struct ZlangObject	*obj = NULL ;
	struct ZlangFunction	*func = NULL ;
	int			nret = 0 ;
	
	nret = ImportObject( rt , & obj , ZLANG_OBJECT_condsig , & direct_funcs_condsig , sizeof(struct ZlangDirectFunctions) , NULL ) ;
	if( nret )
	{
		SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_LINK_FUNC_TO_ENTITY , "import object to global objects heap" )
		return NULL;
	}
	
	/* condsig.Lock() */
	func = AddFunctionAndParametersInObject( rt , obj , "Lock" , "Lock()" , ZlangInvokeFunction_condsig_Lock , ZLANG_OBJECT_bool , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* condsig.Unlock() */
	func = AddFunctionAndParametersInObject( rt , obj , "Unlock" , "Unlock()" , ZlangInvokeFunction_condsig_Unlock , ZLANG_OBJECT_bool , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* condsig.Wait() */
	func = AddFunctionAndParametersInObject( rt , obj , "Wait" , "Wait()" , ZlangInvokeFunction_condsig_Wait , ZLANG_OBJECT_bool , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* condsig.TimedWait() */
	func = AddFunctionAndParametersInObject( rt , obj , "TimedWait" , "TimedWait(int,long)" , ZlangInvokeFunction_condsig_TimedWait , ZLANG_OBJECT_bool , ZLANG_OBJECT_int,NULL , ZLANG_OBJECT_long,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* condsig.Signal() */
	func = AddFunctionAndParametersInObject( rt , obj , "Signal" , "Signal()" , ZlangInvokeFunction_condsig_Signal , ZLANG_OBJECT_bool , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* condsig.Broadcast() */
	func = AddFunctionAndParametersInObject( rt , obj , "Broadcast" , "Broadcast()" , ZlangInvokeFunction_condsig_Broadcast , ZLANG_OBJECT_bool , NULL ) ;
	if( func == NULL )
		return NULL;
	
	SetRuntimeFunction_condsig_GetMutexPtr( rt , condsig_GetMutexPtr );
	
	return obj ;
}

