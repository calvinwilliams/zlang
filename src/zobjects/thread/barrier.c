/* Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "zobjects_thread.h"

ZlangFromDataPtrFunction ZlangFromDataPtr_barrier;

struct ZlangDirectProperty_barrier
{
	int32_t				wait_count ;
	unsigned char			init_flag ;
#if defined(__linux__)
	pthread_barrier_t		barrier ;
#elif defined(_WIN32)
	SYNCHRONIZATION_BARRIER		barrier ;
#endif
} ;

ZlangInvokeFunction ZlangInvokeFunction_barrier_SetWaitCount_int;
int ZlangInvokeFunction_barrier_SetWaitCount_int( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_barrier	*barrier_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject			*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	int32_t					wait_count ;
#if defined(__linux__)
	int					nret = 0 ;
#elif defined(_WIN32)
	BOOL					bret = FALSE ;
#endif
	
	CallRuntimeFunction_int_GetIntValue( rt , in1 , & wait_count );
	
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "pthread_barrier_init ..." )
#if defined(__linux__)
	nret = pthread_barrier_init( & (barrier_direct_prop->barrier) , NULL , wait_count ) ;
	if( nret )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "pthread_barrier_init failed[%d]" , nret )
		CallRuntimeFunction_bool_SetBoolValue( rt , out1 , FALSE );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_INIT_BARRIER_FAILED );
	}
#elif defined(_WIN32)
	bret = InitializeSynchronizationBarrier( & (barrier_direct_prop->barrier) , wait_count , 0 ) ;
	if( bret != TRUE )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "InitializeSynchronizationBarrier failed" )
		CallRuntimeFunction_bool_SetBoolValue( rt , out1 , FALSE );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_INIT_BARRIER_FAILED );
	}
#endif
	
	barrier_direct_prop->init_flag = 1 ;

	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "pthread_barrier_init ok" )
	CallRuntimeFunction_bool_SetBoolValue( rt , out1 , TRUE );
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_barrier_Wait;
int ZlangInvokeFunction_barrier_Wait( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_barrier	*barrier_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	int					nret = 0 ;
	
	if( barrier_direct_prop->init_flag == 0 )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "barrier_direct_prop->init_flag[%d]" , barrier_direct_prop->init_flag )
		CallRuntimeFunction_bool_SetBoolValue( rt , out1 , FALSE );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_BARRIER_NOT_INIT );
	}

#if defined(__linux__)
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "pthread_barrier_wait ..." )
	nret = pthread_barrier_wait( & (barrier_direct_prop->barrier) ) ;
	if( nret )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "pthread_barrier_wait failed[%d]" , nret )
		CallRuntimeFunction_bool_SetBoolValue( rt , out1 , FALSE );
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , EXCEPTION_MESSAGE_WAIT_BARRIER_FAILED );
	}
#elif defined(_WIN32)
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "EnterSynchronizationBarrier ..." )
	EnterSynchronizationBarrier( & (barrier_direct_prop->barrier) , SYNCHRONIZATION_BARRIER_FLAGS_BLOCK_ONLY ) ;
#endif
	
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "pthread_barrier_wait ok" )
	CallRuntimeFunction_bool_SetBoolValue( rt , out1 , TRUE );
	return 0;
}

void *ZlangCreateDirectProperty_barrier( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_barrier	*barrier_prop = NULL ;
	
	barrier_prop = (struct ZlangDirectProperty_barrier *)ZLMALLOC( sizeof(struct ZlangDirectProperty_barrier) ) ;
	if( barrier_prop == NULL )
	{
		SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_ALLOC , "alloc memory for entity" )
		return NULL;
	}
	memset( barrier_prop , 0x00 , sizeof(struct ZlangDirectProperty_barrier) );
	barrier_prop->init_flag = 0 ;
	
	return barrier_prop;
}

ZlangDestroyDirectPropertyFunction ZlangDestroyDirectProperty_barrier;
void ZlangDestroyDirectProperty_barrier( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_barrier	*barrier_direct_prop = GetObjectDirectProperty(obj) ;
	
	if( barrier_direct_prop->init_flag == 1 )
	{
#if defined(__linux__)
		pthread_barrier_destroy( & (barrier_direct_prop->barrier) );
#elif defined(_WIN32)
		DeleteSynchronizationBarrier( & (barrier_direct_prop->barrier) );
#endif
	}
	
	ZLFREE( barrier_direct_prop );
	
	return;
}

ZlangSummarizeDirectPropertySizeFunction ZlangSummarizeDirectPropertySize_barrier;
void ZlangSummarizeDirectPropertySize_barrier( struct ZlangRuntime *rt , struct ZlangObject *obj , size_t *summarized_obj_size , size_t *summarized_direct_prop_size )
{
	SUMMARIZE_SIZE( summarized_direct_prop_size , sizeof(struct ZlangDirectProperty_barrier) )
	return;
}

static struct ZlangDirectFunctions direct_funcs_barrier =
	{
		ZLANG_OBJECT_barrier , /* char *tpye_name */
		
		ZlangCreateDirectProperty_barrier , /* ZlangCreateDirectPropertyFunction *create_entity_func */
		ZlangDestroyDirectProperty_barrier , /* ZlangDestroyDirectPropertyFunction *destroy_entity_func */
		
		NULL , /* ZlangFromCharPtrFunction *from_char_ptr_func */
		NULL , /* ZlangToStringFunction *to_string_func */
		NULL , /* ZlangFromDataPtrFunction *from_data_ptr_func */
		NULL , /* ZlangGetDataPtrFunction *get_data_ptr_func */
		
		NULL , /* ZlangOperatorFunction *oper_PLUS_func */
		NULL , /* ZlangOperatorFunction *oper_MINUS_func */
		NULL , /* ZlangOperatorFunction *oper_MUL_func */
		NULL , /* ZlangOperatorFunction *oper_DIV_func */
		NULL , /* ZlangOperatorFunction *oper_MOD_func */
		
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_NEGATIVE_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_NOT_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_BIT_REVERSE_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_PLUS_PLUS_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_MINUS_MINUS_func */
		
		NULL , /* ZlangCompareFunction *comp_EGUAL_func */
		NULL , /* ZlangCompareFunction *comp_NOTEGUAL_func */
		NULL , /* ZlangCompareFunction *comp_LT_func */
		NULL , /* ZlangCompareFunction *comp_LE_func */
		NULL , /* ZlangCompareFunction *comp_GT_func */
		NULL , /* ZlangCompareFunction *comp_GE_func */
		
		NULL , /* ZlangLogicFunction *logic_AND_func */
		NULL , /* ZlangLogicFunction *logic_OR_func */
		
		NULL , /* ZlangLogicFunction *bit_AND_func */
		NULL , /* ZlangLogicFunction *bit_XOR_func */
		NULL , /* ZlangLogicFunction *bit_OR_func */
		NULL , /* ZlangLogicFunction *bit_MOVELEFT_func */
		NULL , /* ZlangLogicFunction *bit_MOVERIGHT_func */
		
		ZlangSummarizeDirectPropertySize_barrier , /* ZlangSummarizeDirectPropertySizeFunction *summarize_direct_prop_size_func */
	} ;

ZlangImportObjectFunction ZlangImportObject_barrier;
struct ZlangObject *ZlangImportObject_barrier( struct ZlangRuntime *rt )
{
	struct ZlangObject	*obj = NULL ;
	struct ZlangFunction	*func = NULL ;
	int			nret = 0 ;
	
	nret = ImportObject( rt , & obj , ZLANG_OBJECT_barrier , & direct_funcs_barrier , sizeof(struct ZlangDirectFunctions) , NULL ) ;
	if( nret )
	{
		SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_LINK_FUNC_TO_ENTITY , "import object to global objects heap" )
		return NULL;
	}
	
	/* barrier.SetWaitCount(int) */
	func = AddFunctionAndParametersInObject( rt , obj , "SetWaitCount" , "SetWaitCount(int)" , ZlangInvokeFunction_barrier_SetWaitCount_int , ZLANG_OBJECT_bool , ZLANG_OBJECT_int,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* barrier.Wait() */
	func = AddFunctionAndParametersInObject( rt , obj , "Wait" , "Wait()" , ZlangInvokeFunction_barrier_Wait , ZLANG_OBJECT_bool , NULL ) ;
	if( func == NULL )
		return NULL;
	
	return obj ;
}

