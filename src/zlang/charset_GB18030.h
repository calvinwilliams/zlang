/* Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define ZLANG_FULLFUNCNAME_MAIN_GB18030		"主函数(array)"

#define CHARSET_GB18030_TRUE		"真"
#define CHARSET_GB18030_FALSE		"假"
#define CHARSET_GB18030_NULL		"空"
#define CHARSET_GB18030_VOID		"无参"
#define CHARSET_GB18030_INCLUDE		"包含文件"
#define CHARSET_GB18030_NEW		"新的"
#define CHARSET_GB18030_IF		"如果"
#define CHARSET_GB18030_ELSE		"否则"
#define CHARSET_GB18030_SWITCH		"分支判断"
#define CHARSET_GB18030_CASE		"分支匹配"
#define CHARSET_GB18030_DEFAULT		"分支缺省"
#define CHARSET_GB18030_DO		"做循环"
#define CHARSET_GB18030_WHILE		"循环判断"
#define CHARSET_GB18030_FOR		"循环"
#define CHARSET_GB18030_IN		"在里面"
#define CHARSET_GB18030_FROM		"从"
#define CHARSET_GB18030_TO		"到"
#define CHARSET_GB18030_FOREACH		"循环每一个"
#define CHARSET_GB18030_CONTINUE	"继续"
#define CHARSET_GB18030_BREAK		"中断"
#define CHARSET_GB18030_IMPORT		"导入"
#define CHARSET_GB18030_FUNCTION	"函数"
#define CHARSET_GB18030_OBJECT		"对象"
#define CHARSET_GB18030_EXTENDS		"继承"
#define CHARSET_GB18030_INTERFACE	"接口"
#define CHARSET_GB18030_IMPLEMENTS	"实现"
#define CHARSET_GB18030_PUBLIC		"公共的"
#define CHARSET_GB18030_PRIVATE		"私有的"
#define CHARSET_GB18030_THIS		"本对象"
#define CHARSET_GB18030_INTERCEPT	"拦截器"
#define CHARSET_GB18030_BEFORE		"在之前"
#define CHARSET_GB18030_AFTER		"在之后"
#define CHARSET_GB18030_SET		"设置"
#define CHARSET_GB18030_GET		"得到"
#define CHARSET_GB18030_DEFER		"延后"
#define CHARSET_GB18030_RETURN		"返回"
#define CHARSET_GB18030_EXIT		"退出"
#define CHARSET_GB18030_SYNC		"同步"
#define CHARSET_GB18030_ATOMIC		"原子的"
#define CHARSET_GB18030_CONST		"常量"
#define CHARSET_GB18030_TRY		"异常监视"
#define CHARSET_GB18030_THROW		"抛出异常"
#define CHARSET_GB18030_CATCH		"捕获异常"
#define CHARSET_GB18030_UNCATCH		"不捕获异常"
#define CHARSET_GB18030_FINALLY		"异常监视后"

