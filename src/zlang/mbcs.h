/* Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef _H_MBCS_
#define _H_MBCS_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#if defined(__linux__)
#define DLLEXPORT	extern
#elif defined(_WIN32)
#define DLLEXPORT	__declspec(dllexport)
#endif

#define MBCS_ERROR_ALLOC			-1
#define MBCS_ERROR_INVALID_ENCODING		-11
#define MBCS_ERROR_INVALID_INPUT		-12
#define MBCS_ERROR_INPUT_BUFFER_TOO_SMALL	-13
#define MBCS_ERROR_UNKNOW			-19

DLLEXPORT int ConvertStringEncoding( char *from_encoding , char *to_encoding , char *str , size_t str_len , char **encoding_buf , size_t *encoding_buf_size , size_t *encoding_buf_len );

#endif

