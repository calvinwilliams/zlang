/* Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "zlang_in.h"

#define GET_LEFT_VALUE		1
#define GET_RIGHT_VALUE		2

static int InterpretExpression_L15( struct ZlangRuntime *rt , struct ZlangInterpretStatementContext *interp_stat_ctx , struct ZlangInterpretExpressionContext *interp_expr_ctx , struct ZlangObject **result_obj );
static int InterpretExpression_L14( struct ZlangRuntime *rt , struct ZlangInterpretStatementContext *interp_stat_ctx , struct ZlangInterpretExpressionContext *interp_expr_ctx , struct ZlangObject **result_obj );
static int InterpretExpression_L12_L13( struct ZlangRuntime *rt , struct ZlangInterpretStatementContext *interp_stat_ctx , struct ZlangInterpretExpressionContext *interp_expr_ctx , struct ZlangObject **result_obj );
static int InterpretExpression_L9_L10_L11( struct ZlangRuntime *rt , struct ZlangInterpretStatementContext *interp_stat_ctx , struct ZlangInterpretExpressionContext *interp_expr_ctx , struct ZlangObject **result_obj );
static int InterpretExpression_L7_L8( struct ZlangRuntime *rt , struct ZlangInterpretStatementContext *interp_stat_ctx , struct ZlangInterpretExpressionContext *interp_expr_ctx , struct ZlangObject **result_obj );
static int InterpretExpression_L6( struct ZlangRuntime *rt , struct ZlangInterpretStatementContext *interp_stat_ctx , struct ZlangInterpretExpressionContext *interp_expr_ctx , struct ZlangObject **result_obj );
static int InterpretExpression_L5( struct ZlangRuntime *rt , struct ZlangInterpretStatementContext *interp_stat_ctx , struct ZlangInterpretExpressionContext *interp_expr_ctx , struct ZlangObject **result_obj );
static int InterpretExpression_L4( struct ZlangRuntime *rt , struct ZlangInterpretStatementContext *interp_stat_ctx , struct ZlangInterpretExpressionContext *interp_expr_ctx , struct ZlangObject **result_obj );
static int InterpretExpression_L2( struct ZlangRuntime *rt , struct ZlangInterpretStatementContext *interp_stat_ctx , struct ZlangInterpretExpressionContext *interp_expr_ctx , struct ZlangObject **result_obj );
static int InterpretExpression_L1( struct ZlangRuntime *rt , struct ZlangInterpretStatementContext *interp_stat_ctx , struct ZlangInterpretExpressionContext *interp_expr_ctx , struct ZlangObject **result_obj );
static int InterpretExpression_L0( struct ZlangRuntime *rt , struct ZlangInterpretStatementContext *interp_stat_ctx , struct ZlangInterpretExpressionContext *interp_expr_ctx , struct ZlangObject **result_obj );

#define INTERCEPT_BEFORE_CALL_FUNCTION \
	if( rt->in_func && ! rt->in_func->is_interceptor ) \
	{ \
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "InterceptBeforeCallFunction ..." ) \
		nret2 = InterceptBeforeCallFunction( rt ) ; \
		if( nret2 ) \
		{ \
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "InterceptBeforeCallFunction failed[%d]" , nret2 ) \
			return nret2; \
		} \
		else \
		{ \
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "InterceptBeforeCallFunction ok" ) \
		} \
	} \

#define INTERCEPT_AFTER_CALL_FUNCTION \
	if( rt->in_func && ! rt->in_func->is_interceptor ) \
	{ \
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "InterceptAfterCallFunction ..." ) \
		nret2 = InterceptAfterCallFunction( rt ) ; \
		if( nret2 ) \
		{ \
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "InterceptAfterCallFunction failed[%d]" , nret2 ) \
			return nret2; \
		} \
		else \
		{ \
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "InterceptAfterCallFunction ok" ) \
		} \
	} \

#define IF_SYNCHRONIZE_FUNCTION_THEN_LOCK(_func_) \
	if( (_func_) && IsSynchronizeFunction(_func_) ) \
	{ \
		nret = LockSynchronizeFunction( _func_ ) ; \
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "LockSynchronizeFunction return[%d]" , nret ) \
	} \

#define IF_SYNCHRONIZE_FUNCTION_THEN_UNLOCK(_func_) \
	if( (_func_) && IsSynchronizeFunction(_func_) ) \
	{ \
		nret = UnlockSynchronizeFunction( _func_ ) ; \
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "UnlockSynchronizeFunction return[%d]" , nret ) \
	} \

int InterpretStatementSegment( struct ZlangRuntime *rt , struct ZlangInterpretStatementContext *previous_interp_stat_ctx )
{
	struct ZlangTokenDataUnitHeader	*token_info1 = NULL ;
	char				*token1 = NULL ;
	
	int				nret = 0 ;
	int				nret2 = 0 ;
	
	CHECK_ALART_STACK_DEPTH( rt->zlang_stack_bottom,&rt )
	
	TEST_RUNTIME_DEBUG_THEN_PRINT_ENTER_FUNCTION(rt)
	
	SetObjectsStackCalleeTokenInfo( rt );
	
	IF_SYNCHRONIZE_FUNCTION_THEN_LOCK( rt->in_func )
	
	INTERCEPT_BEFORE_CALL_FUNCTION
	
	TEST_RUNTIME_DEBUG( rt )
	{
		if( rt->in_func )
		{
			DebugBeforeCallFunction( rt , rt->in_func );
		}
	}
	
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "InterpretStatementSegment in_obj[%p][%s] in_func[%s][%s]" , rt->in_obj , rt->in_obj?rt->in_obj->obj_name:NULL , rt->in_func?rt->in_func->func_name:NULL , rt->in_func?rt->in_func->full_func_name:NULL )
	if( rt->in_obj )
	{
		TEST_RUNTIME_DEBUG( rt ) { PRINT_TABS(rt) printf( "in_obj " ); DebugPrintObject( rt , rt->in_obj ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE }
	}
	
	if( rt->in_func && rt->in_func->invoke_func )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "INVOKE obj[%s].func[%s][%s]" , rt->in_obj?rt->in_obj->obj_name:NULL , rt->in_func->func_name,rt->in_func->full_func_name )
		nret = rt->in_func->invoke_func( rt , rt->in_obj ) ;
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "INVOKE obj[%s].func[%s][%s] return[%d] error[%d] fatal[%d]" , rt->in_obj?rt->in_obj->obj_name:NULL , rt->in_func->func_name,rt->in_func->full_func_name , nret , HaveErrorException(rt) , HaveFatalException(rt) )
		if( HaveFatalException(rt) )
		{
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "HaveFatalException" )
			INTERCEPT_AFTER_CALL_FUNCTION
			IF_SYNCHRONIZE_FUNCTION_THEN_UNLOCK( rt->in_func )
			TEST_RUNTIME_DEBUG_THEN_PRINT_LEAVE_FUNCTION(rt)
			return ZLANG_INFO_THROW;
		}
		if( HaveErrorException(rt) )
		{
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "HaveErrorException , is_in_try[%d] is_in_uncatch[%d]" , IsPreviousLocalObjectsStackFrameInTry( rt ) , previous_interp_stat_ctx->is_in_uncatch )
			if( IsPreviousLocalObjectsStackFrameInTry( rt )
				&&
				(
					previous_interp_stat_ctx == NULL
					||
					(
						previous_interp_stat_ctx != NULL
						&&
						previous_interp_stat_ctx->is_in_uncatch == FALSE
					)
				)
			)
			{
				TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "in try in previous local objects stack frame" )
				INTERCEPT_AFTER_CALL_FUNCTION
				IF_SYNCHRONIZE_FUNCTION_THEN_UNLOCK( rt->in_func )
				TEST_RUNTIME_DEBUG_THEN_PRINT_LEAVE_FUNCTION(rt)
				return ZLANG_INFO_THROW;
			}
			else
			{
				TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "not in try in previous local objects stack frame" )
				CleanErrorException(rt);
			}
		}
		if( nret == ZLANG_INFO_EXIT )
		{
			INTERCEPT_AFTER_CALL_FUNCTION
			IF_SYNCHRONIZE_FUNCTION_THEN_UNLOCK( rt->in_func )
			TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
			return nret;
		}
		else if( nret == ZLANG_FATAL_INVOKE_METHOD_RETURN )
		{
			SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_FATAL_INVOKE_METHOD_RETURN , "INVOKE obj[%s].func[%s][%s] return[%d]" , rt->in_obj?rt->in_obj->obj_name:NULL , rt->in_func->func_name,rt->in_func->full_func_name , nret )
			INTERCEPT_AFTER_CALL_FUNCTION
			IF_SYNCHRONIZE_FUNCTION_THEN_UNLOCK( rt->in_func )
			exit(-nret);
		}
		else if( nret == ZLANG_ERROR_INVOKE_METHOD_RETURN )
		{
			SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_INVOKE_METHOD_RETURN , "INVOKE obj[%s].func[%s][%s] return[%d]" , rt->in_obj?rt->in_obj->obj_name:NULL , rt->in_func->func_name,rt->in_func->full_func_name , nret )
			INTERCEPT_AFTER_CALL_FUNCTION
			IF_SYNCHRONIZE_FUNCTION_THEN_UNLOCK( rt->in_func )
			TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
			return nret;
		}
		else if( nret )
		{
			SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , nret , "INVOKE obj[%s].func[%s][%s] return[%d]" , rt->in_obj?rt->in_obj->obj_name:NULL , rt->in_func->func_name,rt->in_func->full_func_name , nret )
			INTERCEPT_AFTER_CALL_FUNCTION
			IF_SYNCHRONIZE_FUNCTION_THEN_UNLOCK( rt->in_func )
			TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
			return nret;
		}
	}
	else
	{
		for( ; ; )
		{
			PEEKTOKEN_AND_SAVEINFO( rt , token_info1 , token1 , rt->debug_recursive_depth -= 3 )
			TEST_RUNTIME_DEBUG( rt )
			{
				TOKENTYPE_TO_STRING( token_info1->token_type , _zlang_token_type_str )
				PRINT_TABS_AND_FORMAT( rt , "StatementSegment TOKEN-R%d begin from source[%s][%"PRIi32",%"PRIi32"] token[%s][%s] p[%p][%p]%s%s" , token_info1->source_row , token_info1->source_filename,token_info1->source_row,token_info1->source_col , _zlang_token_type_str,token1 , token_info1->p1,token_info1->p1 , (HaveFatalException(rt)?" HaveFatalException":"") , (HaveErrorException(rt)?" HaveErrorException":"") )
			}
			
			if( token_info1->token_type == TOKEN_TYPE_END_OF_STATEMENT_SEGMENT )
			{
				NEXTTOKEN( rt , )
				TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "found TOKEN_TYPE_END_OF_STATEMENT_SEGMENT in statement iteration" )
				break;
			}
			
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "CALL InterpretStatement" )
			nret = InterpretStatement( rt ) ;
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "InterpretStatement return[%d]" , nret )
			if( nret == ZLANG_INFO_END_OF_STATEMENT )
			{
				;
			}
			else if( nret == ZLANG_INFO_CONTINUE )
			{
				TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "CONTINUE" )
				INTERCEPT_AFTER_CALL_FUNCTION
				IF_SYNCHRONIZE_FUNCTION_THEN_UNLOCK( rt->in_func )
				TEST_RUNTIME_DEBUG_THEN_PRINT_LEAVE_FUNCTION(rt)
				return nret;
			}
			else if( nret == ZLANG_INFO_BREAK )
			{
				TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "BREAK" )
				INTERCEPT_AFTER_CALL_FUNCTION
				IF_SYNCHRONIZE_FUNCTION_THEN_UNLOCK( rt->in_func )
				TEST_RUNTIME_DEBUG_THEN_PRINT_LEAVE_FUNCTION(rt)
				return nret;
			}
			else if( nret == ZLANG_INFO_THROW )
			{
				TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "THROW" )
				INTERCEPT_AFTER_CALL_FUNCTION
				IF_SYNCHRONIZE_FUNCTION_THEN_UNLOCK( rt->in_func )
				TEST_RUNTIME_DEBUG_THEN_PRINT_LEAVE_FUNCTION(rt)
				return nret;
			}
			else if( nret == ZLANG_INFO_RETURN )
			{
				break;
			}
			else if( nret == ZLANG_INFO_EXIT )
			{
				break;
			}
			else if( nret )
			{
				INTERCEPT_AFTER_CALL_FUNCTION
				IF_SYNCHRONIZE_FUNCTION_THEN_UNLOCK( rt->in_func )
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return nret;
			}
		}
	}
	
	TEST_RUNTIME_DEBUG( rt )
	{
		if( rt->in_func )
		{
			DebugAfterCallFunction( rt , rt->in_func );
		}
	}
	
	INTERCEPT_AFTER_CALL_FUNCTION
	
	IF_SYNCHRONIZE_FUNCTION_THEN_UNLOCK( rt->in_func )
	
	TEST_RUNTIME_DEBUG_THEN_PRINT_LEAVE_FUNCTION(rt)
	
	if( nret == ZLANG_INFO_EXIT )
	{
		return nret;
	}
	else if( nret == ZLANG_INFO_RETURN )
	{
		if( rt->in_func == NULL )
		{
			return nret;
		}
		else
		{
			return ZLANG_INFO_END_OF_STATEMENT_SEGMENT;
		}
	}
	else
	{
		return ZLANG_INFO_END_OF_STATEMENT_SEGMENT;
	}
}

int InterpretStatement( struct ZlangRuntime *rt )
{
	struct ZlangTokenDataUnitHeader		*token_info1 = NULL ;
	char					*token1 = NULL ;
	struct ZlangInterpretStatementContext	interp_stat_ctx ;
	struct ZlangObject			*result_obj = NULL ;
	
	int					nret = 0 ;
	
	CHECK_ALART_STACK_DEPTH( rt->zlang_stack_bottom,&rt )
	
	rt->debug_recursive_depth += 1 ;
	TEST_RUNTIME_DEBUG_THEN_PRINT_ENTER_FUNCTION(rt)
	
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "InterpretStatement in_obj[%p][%s] in_func[%s][%s]" , rt->in_obj , rt->in_obj?rt->in_obj->obj_name:NULL , rt->in_func?rt->in_func->func_name:NULL , rt->in_func?rt->in_func->full_func_name:NULL )
	if( rt->in_obj )
	{
		TEST_RUNTIME_DEBUG( rt ) { PRINT_TABS(rt) printf( "in_obj " ); DebugPrintObject( rt , rt->in_obj ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE }
	}
	
	TEST_RUNTIME_DEBUG( rt )
	{
		DebugStack( rt );
	}
	
	PEEKTOKEN_AND_SAVEINFO( rt , token_info1 , token1 , rt->debug_recursive_depth -= 1 )
	TEST_RUNTIME_DEBUG( rt )
	{
		TOKENTYPE_TO_STRING( token_info1->token_type , _zlang_token_type_str )
		PRINT_TABS_AND_FORMAT( rt , "Statement TOKEN-R%d=C%d begin from source[%s][%"PRIi32",%"PRIi32"] token[%s][%s] p[%p][%p] , error[%d] fatal[%d]" , token_info1->source_row,token_info1->source_col , token_info1->source_filename,token_info1->source_row,token_info1->source_col , _zlang_token_type_str,token1 , token_info1->p1,token_info1->p2 , HaveErrorException(rt) , HaveFatalException(rt) )
	}
	
	TEST_RUNTIME_DEBUG( rt )
	{
		PRINT_TABS_AND_FORMAT( rt , "CALL InterpretExpression" )
	}
	memset( & interp_stat_ctx , 0x00 , sizeof(struct ZlangInterpretStatementContext) );
	interp_stat_ctx.token_of_expression_end1 = TOKEN_TYPE_END_OF_STATEMENT ;
	nret = InterpretExpression( rt , & interp_stat_ctx , & result_obj ) ;
	TEST_RUNTIME_DEBUG( rt )
	{
		PRINT_TABS(rt) printf( "InterpretExpression return[%d] error[%d] fatal[%d] " , nret , HaveErrorException(rt) , HaveFatalException(rt) ); DebugPrintObject(rt,result_obj); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE
		DebugStack( rt );
	}
	
	if( nret == ZLANG_INFO_END_OF_EXPRESSION )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "END_OF_EXPRESSION" )
		TEST_RUNTIME_DEBUG_THEN_PRINT_LEAVE_FUNCTION(rt)
		rt->debug_recursive_depth -= 1 ;
		return ZLANG_INFO_END_OF_STATEMENT;
	}
	else if( nret == ZLANG_INFO_CONTINUE )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "CONTINUE" )
		TEST_RUNTIME_DEBUG_THEN_PRINT_LEAVE_FUNCTION(rt)
		rt->debug_recursive_depth -= 1 ;
		return nret;
	}
	else if( nret == ZLANG_INFO_BREAK )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "BREAK" )
		TEST_RUNTIME_DEBUG_THEN_PRINT_LEAVE_FUNCTION(rt)
		rt->debug_recursive_depth -= 1 ;
		return nret;
	}
	else if( nret == ZLANG_INFO_THROW )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "THROW , error[%d] fatal[%d]" , HaveErrorException(rt) , HaveFatalException(rt) )
		TEST_RUNTIME_DEBUG_THEN_PRINT_LEAVE_FUNCTION(rt)
		rt->debug_recursive_depth -= 1 ;
		return nret;
	}
	else if( nret == ZLANG_INFO_RETURN )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "RETURN" )
		TEST_RUNTIME_DEBUG_THEN_PRINT_LEAVE_FUNCTION(rt)
		rt->debug_recursive_depth -= 1 ;
		return nret;
	}
	else if( nret == ZLANG_INFO_EXIT )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "EXIT" )
		TEST_RUNTIME_DEBUG_THEN_PRINT_LEAVE_FUNCTION(rt)
		rt->debug_recursive_depth -= 1 ;
		return nret;
	}
	else if( nret )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
		rt->debug_recursive_depth -= 1 ;
		return nret;
	}
	
	if( interp_stat_ctx.has_const_decorated == TRUE )
	{
		SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_UNEXPECT_DECORATION_CONST , "unexpect decoration 'const'" )
		TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
		rt->debug_recursive_depth -= 1 ;
		return ZLANG_ERROR_UNEXPECT_DECORATION_CONST;
	}
	
	if( HaveFatalException(rt) )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "HaveFatalException" )
		TEST_RUNTIME_DEBUG_THEN_PRINT_LEAVE_FUNCTION(rt)
		rt->debug_recursive_depth -= 1 ;
		return ZLANG_INFO_THROW;
	}
	if( HaveErrorException(rt) )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "HaveErrorException , is_in_try[%d] is_in_throw[%d]" , IsLocalObjectsStackFrameInTry( rt ) , interp_stat_ctx.is_in_throw )
		if( IsLocalObjectsStackFrameInTry( rt ) && interp_stat_ctx.is_in_throw == TRUE )
		{
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "in try in local objects stack frame" )
			TEST_RUNTIME_DEBUG_THEN_PRINT_LEAVE_FUNCTION(rt)
			rt->debug_recursive_depth -= 1 ;
			return ZLANG_INFO_THROW;
		}
		else
		{
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "not in try in local objects stack frame" )
			CleanErrorException(rt);
		}
	}
	
	TEST_RUNTIME_DEBUG_THEN_PRINT_LEAVE_FUNCTION(rt)
	rt->debug_recursive_depth -= 1 ;
	return ZLANG_INFO_END_OF_STATEMENT;
}

int InterpretExpression( struct ZlangRuntime *rt , struct ZlangInterpretStatementContext *interp_stat_ctx , struct ZlangObject **result_obj )
{
	struct ZlangTokenDataUnitHeader		*token_info1 = NULL ;
	char					*token1 = NULL ;
	struct ZlangInterpretExpressionContext	interp_expr_ctx ;
	struct ZlangTokenDataUnitHeader		*peek_token_info = NULL ;
	char					*peek_token = NULL ;
	
	int					nret = 0 ;
	
	CHECK_ALART_STACK_DEPTH( rt->zlang_stack_bottom,&rt )

	TEST_RUNTIME_DEBUG_THEN_PRINT_ENTER_FUNCTION(rt)
	
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "InterpretStatement in_obj[%p][%s] in_func[%s][%s]" , rt->in_obj , rt->in_obj?rt->in_obj->obj_name:NULL , rt->in_func?rt->in_func->func_name:NULL , rt->in_func?rt->in_func->full_func_name:NULL )
	if( rt->in_obj )
	{
		TEST_RUNTIME_DEBUG( rt ) { PRINT_TABS(rt) printf( "in_obj " ); DebugPrintObject( rt , rt->in_obj ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE }
	}
	
	TEST_RUNTIME_DEBUG( rt )
	{
		TOKENTYPE_TO_STRING( interp_stat_ctx->token_of_expression_end1 , _zlang_token_type_str )
		TOKENTYPE_TO_STRING( interp_stat_ctx->token_of_expression_end2 , _zlang_token_type_str2 )
		PRINT_TABS_AND_FORMAT( rt , "token of expression end[%s][%s]" , _zlang_token_type_str,_zlang_token_type_str2 )
	}
	
	PEEKTOKEN_AND_SAVEINFO( rt , token_info1 , token1 )
	// PEEKTOKEN_AND_SAVEINFO( rt , token_info1 , token1 , rt->debug_recursive_depth -= 1 )
	TEST_RUNTIME_DEBUG( rt )
	{
		TOKENTYPE_TO_STRING( token_info1->token_type , _zlang_token_type_str )
		PRINT_TABS_AND_FORMAT( rt , "Expression TOKEN-R%d-C%d begin from source[%s][%"PRIi32",%"PRIi32"] token[%s][%s] p[%p][%p]" , token_info1->source_row,token_info1->source_col , token_info1->source_filename,token_info1->source_row,token_info1->source_col , _zlang_token_type_str,token1 , token_info1->p1,token_info1->p2 )
	}
	
	TEST_RUNTIME_DEBUG( rt )
	{
		PRINT_TABS_AND_FORMAT( rt , "CALL InterpretExpression_L15" )
	}
	memset( & interp_expr_ctx , 0x00 , sizeof(struct ZlangInterpretExpressionContext) );
	interp_expr_ctx.get_expr_left_or_right = GET_EXPRESSION_LEFT ;
	nret = InterpretExpression_L15( rt , interp_stat_ctx , & interp_expr_ctx , result_obj ) ;
	TEST_RUNTIME_DEBUG( rt )
	{
		PRINT_TABS(rt) printf( "InterpretExpression_L15 return[%d] " , nret ); DebugPrintObject(rt,*result_obj); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE
	}
	
	if( nret == ZLANG_INFO_END_OF_EXPRESSION )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT_LEAVE_FUNCTION(rt)
		return nret;
	}
	else if( nret == ZLANG_INFO_CONTINUE )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "CONTINUE" )
		TEST_RUNTIME_DEBUG_THEN_PRINT_LEAVE_FUNCTION(rt)
		return nret;
	}
	else if( nret == ZLANG_INFO_BREAK )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "BREAK" )
		TEST_RUNTIME_DEBUG_THEN_PRINT_LEAVE_FUNCTION(rt)
		return nret;
	}
	else if( nret == ZLANG_INFO_RETURN )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "RETURN" )
		TEST_RUNTIME_DEBUG_THEN_PRINT_LEAVE_FUNCTION(rt)
		return nret;
	}
	else if( nret == ZLANG_INFO_EXIT )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "EXIT" )
		TEST_RUNTIME_DEBUG_THEN_PRINT_LEAVE_FUNCTION(rt)
		return nret;
	}
	else if( nret )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
		return nret;
	}
	
	if( 	rt->travel_token_info
		&&
		(
			( interp_stat_ctx->token_of_expression_end1 && rt->travel_token_info->token_type == interp_stat_ctx->token_of_expression_end1 )
			||
			( interp_stat_ctx->token_of_expression_end2 && rt->travel_token_info->token_type == interp_stat_ctx->token_of_expression_end2 )
			||
			( interp_stat_ctx->token_of_expression_end3 && rt->travel_token_info->token_type == interp_stat_ctx->token_of_expression_end3 )
		)
	)
	{
		TOKENTYPE_TO_STRING( interp_stat_ctx->token_of_expression_end1 , _zlang_token_type_str1 )
		TOKENTYPE_TO_STRING( interp_stat_ctx->token_of_expression_end2 , _zlang_token_type_str2 )
		TOKENTYPE_TO_STRING( interp_stat_ctx->token_of_expression_end3 , _zlang_token_type_str3 )
		TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "token of expression end[%s][%s][%s] found[%s] at last" , _zlang_token_type_str1,_zlang_token_type_str2,_zlang_token_type_str3 , _zlang_token_type_str )
		NEXTTOKEN( rt )
		PEEKTOKEN( rt , peek_token_info , peek_token )
		TEST_RUNTIME_DEBUG_THEN_PRINT_LEAVE_FUNCTION(rt)
		return ZLANG_INFO_END_OF_EXPRESSION;
	}
	else
	{
		TOKENTYPE_TO_STRING( interp_stat_ctx->token_of_expression_end1 , _zlang_token_type_str1 )
		TOKENTYPE_TO_STRING( interp_stat_ctx->token_of_expression_end2 , _zlang_token_type_str2 )
		TOKENTYPE_TO_STRING( interp_stat_ctx->token_of_expression_end3 , _zlang_token_type_str3 )
		TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
		SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_SYNTAX , "token of expression end[%s][%s][%s] not found at last , last found[%s]" , _zlang_token_type_str1,_zlang_token_type_str2,_zlang_token_type_str3 , _zlang_token_type_str )
		TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
		return ZLANG_ERROR_SYNTAX;
	}
}

static int InterpretExpression_L15( struct ZlangRuntime *rt , struct ZlangInterpretStatementContext *interp_stat_ctx , struct ZlangInterpretExpressionContext *interp_expr_ctx , struct ZlangObject **result_obj )
{
	struct ZlangObject		*obj2 = NULL ;
	struct ZlangObject		*obj3 = NULL ;
	struct ZlangTokenDataUnitHeader	*token_info2 = NULL ;
	char				*token2 = NULL ;
	
	int				nret = 0 ;
	
	CHECK_ALART_STACK_DEPTH( rt->zlang_stack_bottom,&rt )

	TEST_RUNTIME_DEBUG_THEN_PRINT_ENTER_FUNCTION(rt)
	
	TEST_RUNTIME_DEBUG( rt )
	{
		TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
		PRINT_TABS_AND_FORMAT( rt , "CALL InterpretExpression_L14 last_token[%s][%s]" , _zlang_token_type_str , rt->travel_token )
	}
	nret = InterpretExpression_L14( rt , interp_stat_ctx , interp_expr_ctx , result_obj ) ;
	TEST_RUNTIME_DEBUG( rt )
	{
		TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
		TEST_RUNTIME_DEBUG( rt ) { PRINT_TABS(rt) printf( "InterpretExpression_L14 return[%d] last_token[%s][%s] " , nret , _zlang_token_type_str , rt->travel_token ); DebugPrintObject( rt , (*result_obj) ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE }
	}
	if( nret == ZLANG_INFO_END_OF_EXPRESSION )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT_LEAVE_FUNCTION(rt)
		return nret;
	}
	else if( nret == ZLANG_INFO_CONTINUE )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "CONTINUE" )
		TEST_RUNTIME_DEBUG_THEN_PRINT_LEAVE_FUNCTION(rt)
		return nret;
	}
	else if( nret == ZLANG_INFO_BREAK )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "BREAK" )
		TEST_RUNTIME_DEBUG_THEN_PRINT_LEAVE_FUNCTION(rt)
		return nret;
	}
	else if( nret == ZLANG_INFO_RETURN )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "RETURN" )
		TEST_RUNTIME_DEBUG_THEN_PRINT_LEAVE_FUNCTION(rt)
		return nret;
	}
	else if( nret == ZLANG_INFO_EXIT )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "EXIT" )
		TEST_RUNTIME_DEBUG_THEN_PRINT_LEAVE_FUNCTION(rt)
		return nret;
	}
	else if( nret )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
		return nret;
	}
	
	for( ; ; )
	{
		PEEKTOKEN_AND_SAVEINFO( rt , token_info2 , token2 )
		if( token_info2->token_type == TOKEN_TYPE_EVAL )
		{
			NEXTTOKEN( rt )
			
			IF_ATOMIC_OBJECT_THEN_LOCK(*result_obj)
			
			TEST_RUNTIME_DEBUG( rt )
			{
				TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
				PRINT_TABS_AND_FORMAT( rt , "CALL InterpretExpression_L14 , last_token[%s][%s]" , _zlang_token_type_str , rt->travel_token )
			}
			interp_expr_ctx->get_expr_left_or_right = GET_EXPRESSION_RIGHT ;
			nret = InterpretExpression_L14( rt , interp_stat_ctx , interp_expr_ctx , & obj2 ) ;
			TEST_RUNTIME_DEBUG( rt )
			{
				TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
				PRINT_TABS(rt) printf( "InterpretExpression_L14 return[%d] last_token[%s][%s] " , nret , _zlang_token_type_str , rt->travel_token ); DebugPrintObject( rt , obj2 ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE
			}
			if( nret == ZLANG_INFO_END_OF_EXPRESSION )
			{
				SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_EXPECT_OBJECT , "expect object" )
				IF_ATOMIC_OBJECT_THEN_UNLOCK(*result_obj)
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return ZLANG_ERROR_EXPECT_OBJECT;
			}
			else if( nret )
			{
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				IF_ATOMIC_OBJECT_THEN_UNLOCK(*result_obj)
				return nret;
			}
			
			TEST_RUNTIME_DEBUG( rt )
			{
				PRINT_TABS(rt) printf( "PROCESS operator TOKEN_TYPE_EVAL " ); DebugPrintObject( rt , (*result_obj) ); printf( " from " ); DebugPrintObject( rt , obj2 ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE
			}
			
			if( (*result_obj) == NULL )
			{
				SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_INTERNAL , "left object is null" )
				IF_ATOMIC_OBJECT_THEN_UNLOCK(*result_obj)
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return ZLANG_ERROR_INTERNAL;
			}
			if( obj2 == NULL )
			{
				SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_INTERNAL , "right object is null" )
				IF_ATOMIC_OBJECT_THEN_UNLOCK(*result_obj)
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return ZLANG_ERROR_INTERNAL;
			}
			
			TEST_RUNTIME_DEBUG( rt ) { PRINT_TABS(rt) printf( "EvalObject " ); DebugPrintObject(rt,obj2); printf( " to " ); DebugPrintObject(rt,(*result_obj)); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE }
			nret = EvalObject( rt , (*result_obj) , obj2 ) ;
			if( nret )
			{
				TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "EvalObject failed[%d]" , nret )
				IF_ATOMIC_OBJECT_THEN_UNLOCK(*result_obj)
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return nret;
			}
			
			TEST_RUNTIME_DEBUG( rt )
			{
				PRINT_TABS(rt) printf( "PROCESS-RESULT operator TOKEN_TYPE_EVAL - (*result_obj) " ); DebugPrintObject( rt , (*result_obj) ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE
			}
			
			IF_ATOMIC_OBJECT_THEN_UNLOCK(*result_obj)
		}
		else if( token_info2->token_type == TOKEN_TYPE_REFER )
		{
			NEXTTOKEN( rt )
			
			IF_ATOMIC_OBJECT_THEN_LOCK(*result_obj)
			
			TEST_RUNTIME_DEBUG( rt )
			{
				TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
				PRINT_TABS_AND_FORMAT( rt , "CALL InterpretExpression_L14 , last_token[%s][%s]" , _zlang_token_type_str , rt->travel_token )
			}
			interp_expr_ctx->get_expr_left_or_right = GET_EXPRESSION_RIGHT ;
			nret = InterpretExpression_L14( rt , interp_stat_ctx , interp_expr_ctx , & obj2 ) ;
			TEST_RUNTIME_DEBUG( rt )
			{
				TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
				PRINT_TABS(rt) printf( "InterpretExpression_L14 return[%d] last_token[%s][%s] " , nret , _zlang_token_type_str , rt->travel_token ); DebugPrintObject( rt , obj2 ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE
			}
			if( nret == ZLANG_INFO_END_OF_EXPRESSION )
			{
				SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_EXPECT_OBJECT , "expect object" )
				IF_ATOMIC_OBJECT_THEN_UNLOCK(*result_obj)
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return ZLANG_ERROR_EXPECT_OBJECT;
			}
			else if( nret )
			{
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				IF_ATOMIC_OBJECT_THEN_UNLOCK(*result_obj)
				return nret;
			}
			
			TEST_RUNTIME_DEBUG( rt )
			{
				PRINT_TABS(rt) printf( "PROCESS operator TOKEN_TYPE_REFER - (*result_obj) " ); DebugPrintObject( rt , (*result_obj) ); printf( " from obj2 " ); DebugPrintObject( rt , obj2 ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE
			}
			
			if( (*result_obj) == NULL )
			{
				SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_INTERNAL , "left object is null" )
				IF_ATOMIC_OBJECT_THEN_UNLOCK(*result_obj)
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return ZLANG_ERROR_INTERNAL;
			}
			if( obj2 == NULL )
			{
				SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_INTERNAL , "right object is null" )
				IF_ATOMIC_OBJECT_THEN_UNLOCK(*result_obj)
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return ZLANG_ERROR_INTERNAL;
			}
			
			if( GetObjectFunctionsEntity(*result_obj) == NULL )
			{
				TEST_RUNTIME_DEBUG( rt ) { PRINT_TABS(rt) printf( "ReferObject " ); DebugPrintObject(rt,obj2); printf( " to " ); DebugPrintObject(rt,(*result_obj)); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE }
				nret = ReferObject( rt , (*result_obj) , obj2 ) ;
				if( nret )
				{
					TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "ReferObject failed[%d]" , nret )
					IF_ATOMIC_OBJECT_THEN_UNLOCK(*result_obj)
					TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
					return nret;
				}
				TEST_RUNTIME_DEBUG( rt ) { PRINT_TABS(rt) printf( "(*result_obj) " ); DebugPrintObject(rt,(*result_obj)); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE }
			}
			else
			{
				// if( GetCloneObjectName(*result_obj) && ! IsObjectPropertiesEntityNull(*result_obj) && ! IsObjectPropertiesEntityNull(obj2) && ! IsTypeOf( rt , (*result_obj) , obj2 ) )
				if( GetCloneObjectName(*result_obj) && ! IsObjectPropertiesEntityNull(obj2) && ! IsTypeOf( rt , (*result_obj) , obj2 ) )
				{
					nret = TryAscensionType( rt , *result_obj , & obj2 ) ;
					if( nret == 0 )
					{
						SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_OPERATOR_TYPE_NOT_MATCHED , "left obj[%s]->type_name[%s] type not matched with right obj[%s]->type_name[%s] type" , GetObjectName(*result_obj) , GetCloneObjectName(*result_obj) , GetObjectName(obj2) , GetCloneObjectName(obj2) )
						IF_ATOMIC_OBJECT_THEN_UNLOCK(*result_obj)
						return ZLANG_ERROR_OPERATOR_TYPE_NOT_MATCHED;
					}
					else if( nret == 2 )
					{
						TEST_RUNTIME_DEBUG( rt ) { PRINT_TABS(rt) printf( "TryAscensionType " ); DebugPrintObject(rt,obj2); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE }
					}
					else
					{
						TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "TryAscensionType obj[%s] and obj[%s] failed" , GetObjectName(obj2) , GetObjectName(*result_obj) )
						IF_ATOMIC_OBJECT_THEN_UNLOCK(*result_obj)
						return GET_RUNTIME_ERROR_NO(rt);
					}
				}
				
				TEST_RUNTIME_DEBUG( rt ) { PRINT_TABS(rt) printf( "ReferObjectPropertiesEntity " ); DebugPrintObject(rt,obj2); printf( " to " ); DebugPrintObject(rt,(*result_obj)); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE }
				nret = ReferObjectPropertiesEntity( rt , (*result_obj) , obj2 ) ;
				if( nret )
				{
					TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "ReferObjectPropertiesEntity failed[%d]" , nret )
					TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
					return nret;
				}
			}
			
			TEST_RUNTIME_DEBUG( rt )
			{
				PRINT_TABS(rt) printf( "PROCESS-RESULT operator TOKEN_TYPE_REFER - (*result_obj) " ); DebugPrintObject( rt , (*result_obj) ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE
			}
			
			IF_ATOMIC_OBJECT_THEN_UNLOCK(*result_obj)
		}
		else if( token_info2->token_type == TOKEN_TYPE_CAST_REFER )
		{
			NEXTTOKEN( rt )
			
			IF_ATOMIC_OBJECT_THEN_LOCK(*result_obj)
			
			TEST_RUNTIME_DEBUG( rt )
			{
				TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
				PRINT_TABS_AND_FORMAT( rt , "CALL InterpretExpression_L14 , last_token[%s][%s]" , _zlang_token_type_str , rt->travel_token )
			}
			interp_expr_ctx->get_expr_left_or_right = GET_EXPRESSION_RIGHT ;
			nret = InterpretExpression_L14( rt , interp_stat_ctx , interp_expr_ctx , & obj2 ) ;
			TEST_RUNTIME_DEBUG( rt )
			{
				TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
				PRINT_TABS(rt) printf( "InterpretExpression_L14 return[%d] last_token[%s][%s] " , nret , _zlang_token_type_str , rt->travel_token ); DebugPrintObject( rt , obj2 ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE
			}
			if( nret == ZLANG_INFO_END_OF_EXPRESSION )
			{
				SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_EXPECT_OBJECT , "expect object" )
				IF_ATOMIC_OBJECT_THEN_UNLOCK(*result_obj)
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return ZLANG_ERROR_EXPECT_OBJECT;
			}
			else if( nret )
			{
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				IF_ATOMIC_OBJECT_THEN_UNLOCK(*result_obj)
				return nret;
			}
			
			TEST_RUNTIME_DEBUG( rt )
			{
				PRINT_TABS(rt) printf( "PROCESS operator TOKEN_TYPE_CAST_REFER - (*result_obj) " ); DebugPrintObject( rt , (*result_obj) ); printf( " from obj2 " ); DebugPrintObject( rt , obj2 ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE
			}
			
			if( (*result_obj) == NULL )
			{
				SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_INTERNAL , "left object is null" )
				IF_ATOMIC_OBJECT_THEN_UNLOCK(*result_obj)
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return ZLANG_ERROR_INTERNAL;
			}
			if( obj2 == NULL )
			{
				SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_INTERNAL , "right object is null" )
				IF_ATOMIC_OBJECT_THEN_UNLOCK(*result_obj)
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return ZLANG_ERROR_INTERNAL;
			}
			
			nret = UnreferObject( rt , (*result_obj) ) ;
			if( nret )
			{
				TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "UnreferObject failed[%d]" , nret )
				IF_ATOMIC_OBJECT_THEN_UNLOCK(*result_obj)
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return nret;
			}
			TEST_RUNTIME_DEBUG( rt ) { PRINT_TABS(rt) printf( "UnreferObject " ); DebugPrintObject(rt,(*result_obj)); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE }
			
			nret = ReferObject( rt , (*result_obj) , obj2 ) ;
			if( nret )
			{
				TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "ReferObject failed[%d]" , nret )
				IF_ATOMIC_OBJECT_THEN_UNLOCK(*result_obj)
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return nret;
			}
			TEST_RUNTIME_DEBUG( rt ) { PRINT_TABS(rt) printf( "ReferObject " ); DebugPrintObject(rt,(*result_obj)); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE }
			
			TEST_RUNTIME_DEBUG( rt )
			{
				PRINT_TABS(rt) printf( "PROCESS-RESULT operator TOKEN_TYPE_CAST_REFER - (*result_obj) " ); DebugPrintObject( rt , (*result_obj) ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE
			}
			
			IF_ATOMIC_OBJECT_THEN_UNLOCK(*result_obj)
		}
		else if( token_info2->token_type == TOKEN_TYPE_OPERATOR_PLUS_EVAL )
		{
			NEXTTOKEN( rt )
			
			IF_ATOMIC_OBJECT_THEN_LOCK(*result_obj)
			
			TEST_RUNTIME_DEBUG( rt )
			{
				TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
				PRINT_TABS_AND_FORMAT( rt , "CALL InterpretExpression_L14 , last_token[%s][%s]" , _zlang_token_type_str , rt->travel_token )
			}
			interp_expr_ctx->get_expr_left_or_right = GET_EXPRESSION_RIGHT ;
			nret = InterpretExpression_L14( rt , interp_stat_ctx , interp_expr_ctx , & obj2 ) ;
			TEST_RUNTIME_DEBUG( rt )
			{
				TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
				PRINT_TABS(rt) printf( "InterpretExpression_L14 return[%d] last_token[%s][%s] " , nret , _zlang_token_type_str , rt->travel_token ); DebugPrintObject( rt , obj2 ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE
			}
			if( nret == ZLANG_INFO_END_OF_EXPRESSION )
			{
				SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_EXPECT_OBJECT , "expect object" )
				IF_ATOMIC_OBJECT_THEN_UNLOCK(*result_obj)
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return ZLANG_ERROR_EXPECT_OBJECT;
			}
			else if( nret )
			{
				IF_ATOMIC_OBJECT_THEN_UNLOCK(*result_obj)
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return nret;
			}
			
			TEST_RUNTIME_DEBUG( rt )
			{
				PRINT_TABS(rt) printf( "PROCESS operator TOKEN_TYPE_OPERATOR_PLUS_EVAL - (*result_obj) " ); DebugPrintObject( rt , (*result_obj) ); printf( " and obj2 " ); DebugPrintObject( rt , obj2 ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE
			}
			
			nret = Operator_PLUS( rt , (*result_obj) , obj2 , & obj3 ) ;
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "INVOKE Operator_PLUS return[%d]" , nret )
			if( nret )
			{
				IF_ATOMIC_OBJECT_THEN_UNLOCK(*result_obj)
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return nret;
			}
			
			TEST_RUNTIME_DEBUG( rt ) { PRINT_TABS(rt) printf( "EvalObject " ); DebugPrintObject(rt,obj3); printf( " to " ); DebugPrintObject(rt,(*result_obj)); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE }
			nret = EvalObject( rt , (*result_obj) , obj3 ) ;
			if( nret < 0 )
			{
				TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "EvalObject failed[%d]" , nret )
				IF_ATOMIC_OBJECT_THEN_UNLOCK(*result_obj)
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return nret;
			}
			
			TEST_RUNTIME_DEBUG( rt )
			{
				PRINT_TABS(rt) printf( "PROCESS-RESULT operator TOKEN_TYPE_OPERATOR_PLUS_EVAL - (*result_obj) " ); DebugPrintObject( rt , (*result_obj) ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE
			}
			
			IF_ATOMIC_OBJECT_THEN_UNLOCK(*result_obj)
		}
		else if( token_info2->token_type == TOKEN_TYPE_OPERATOR_MINUS_EVAL )
		{
			NEXTTOKEN( rt )
			
			IF_ATOMIC_OBJECT_THEN_LOCK(*result_obj)
			
			TEST_RUNTIME_DEBUG( rt )
			{
				TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
				PRINT_TABS_AND_FORMAT( rt , "CALL InterpretExpression_L14 , last_token[%s][%s]" , _zlang_token_type_str , rt->travel_token )
			}
			interp_expr_ctx->get_expr_left_or_right = GET_EXPRESSION_RIGHT ;
			nret = InterpretExpression_L14( rt , interp_stat_ctx , interp_expr_ctx , & obj2 ) ;
			TEST_RUNTIME_DEBUG( rt )
			{
				TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
				PRINT_TABS(rt) printf( "InterpretExpression_L14 return[%d] last_token[%s][%s] " , nret , _zlang_token_type_str , rt->travel_token ); DebugPrintObject( rt , obj2 ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE
			}
			if( nret == ZLANG_INFO_END_OF_EXPRESSION )
			{
				SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_EXPECT_OBJECT , "expect object" )
				IF_ATOMIC_OBJECT_THEN_UNLOCK(*result_obj)
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return ZLANG_ERROR_EXPECT_OBJECT;
			}
			else if( nret )
			{
				IF_ATOMIC_OBJECT_THEN_UNLOCK(*result_obj)
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return nret;
			}
			
			TEST_RUNTIME_DEBUG( rt )
			{
				PRINT_TABS(rt) printf( "PROCESS operator TOKEN_TYPE_OPERATOR_MINUS_EVAL - (*result_obj) " ); DebugPrintObject( rt , (*result_obj) ); printf( " and obj2 " ); DebugPrintObject( rt , obj2 ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE
			}
			
			nret = Operator_MINUS( rt , (*result_obj) , obj2 , & obj3 ) ;
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "INVOKE Operator_MINUS return[%d]" , nret )
			if( nret )
			{
				IF_ATOMIC_OBJECT_THEN_UNLOCK(*result_obj)
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return nret;
			}
			
			TEST_RUNTIME_DEBUG( rt ) { PRINT_TABS(rt) printf( "EvalObject " ); DebugPrintObject(rt,obj3); printf( " to " ); DebugPrintObject(rt,(*result_obj)); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE }
			nret = EvalObject( rt , (*result_obj) , obj3 ) ;
			if( nret < 0 )
			{
				TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "EvalObject failed[%d]" , nret )
				IF_ATOMIC_OBJECT_THEN_UNLOCK(*result_obj)
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return nret;
			}
			
			TEST_RUNTIME_DEBUG( rt )
			{
				PRINT_TABS(rt) printf( "PROCESS-RESULT operator TOKEN_TYPE_OPERATOR_MINUS_EVAL - (*result_obj) " ); DebugPrintObject( rt , (*result_obj) ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE
			}
			
			IF_ATOMIC_OBJECT_THEN_UNLOCK(*result_obj)
		}
		else if( token_info2->token_type == TOKEN_TYPE_OPERATOR_MUL_EVAL )
		{
			NEXTTOKEN( rt )
			
			IF_ATOMIC_OBJECT_THEN_LOCK(*result_obj)
			
			TEST_RUNTIME_DEBUG( rt )
			{
				TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
				PRINT_TABS_AND_FORMAT( rt , "CALL InterpretExpression_L14 , last_token[%s][%s]" , _zlang_token_type_str , rt->travel_token )
			}
			interp_expr_ctx->get_expr_left_or_right = GET_EXPRESSION_RIGHT ;
			nret = InterpretExpression_L14( rt , interp_stat_ctx , interp_expr_ctx , & obj2 ) ;
			TEST_RUNTIME_DEBUG( rt )
			{
				TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
				PRINT_TABS(rt) printf( "InterpretExpression_L14 return[%d] last_token[%s][%s] " , nret , _zlang_token_type_str , rt->travel_token ); DebugPrintObject( rt , obj2 ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE
			}
			if( nret == ZLANG_INFO_END_OF_EXPRESSION )
			{
				SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_EXPECT_OBJECT , "expect object" )
				IF_ATOMIC_OBJECT_THEN_UNLOCK(*result_obj)
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return ZLANG_ERROR_EXPECT_OBJECT;
			}
			else if( nret )
			{
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				IF_ATOMIC_OBJECT_THEN_UNLOCK(*result_obj)
				return nret;
			}
			
			TEST_RUNTIME_DEBUG( rt )
			{
				PRINT_TABS(rt) printf( "PROCESS operator TOKEN_TYPE_OPERATOR_MUL_EVAL - (*result_obj) " ); DebugPrintObject( rt , (*result_obj) ); printf( " and obj2 " ); DebugPrintObject( rt , obj2 ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE
			}
			
			nret = Operator_MUL( rt , (*result_obj) , obj2 , & obj3 ) ;
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "INVOKE Operator_MUL return[%d]" , nret )
			if( nret )
			{
				IF_ATOMIC_OBJECT_THEN_UNLOCK(*result_obj)
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return nret;
			}
			
			TEST_RUNTIME_DEBUG( rt ) { PRINT_TABS(rt) printf( "EvalObject " ); DebugPrintObject(rt,obj3); printf( " to " ); DebugPrintObject(rt,(*result_obj)); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE }
			nret = EvalObject( rt , (*result_obj) , obj3 ) ;
			if( nret < 0 )
			{
				TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "EvalObject failed[%d]" , nret )
				IF_ATOMIC_OBJECT_THEN_UNLOCK(*result_obj)
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return nret;
			}
			
			TEST_RUNTIME_DEBUG( rt )
			{
				PRINT_TABS(rt) printf( "PROCESS-RESULT operator TOKEN_TYPE_OPERATOR_MUL_EVAL - (*result_obj) " ); DebugPrintObject( rt , (*result_obj) ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE
			}
			
			IF_ATOMIC_OBJECT_THEN_UNLOCK(*result_obj)
		}
		else if( token_info2->token_type == TOKEN_TYPE_OPERATOR_DIV_EVAL )
		{
			NEXTTOKEN( rt )
			
			IF_ATOMIC_OBJECT_THEN_LOCK(*result_obj)
			
			TEST_RUNTIME_DEBUG( rt )
			{
				TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
				PRINT_TABS_AND_FORMAT( rt , "CALL InterpretExpression_L14 , last_token[%s][%s]" , _zlang_token_type_str , rt->travel_token )
			}
			interp_expr_ctx->get_expr_left_or_right = GET_EXPRESSION_RIGHT ;
			nret = InterpretExpression_L14( rt , interp_stat_ctx , interp_expr_ctx , & obj2 ) ;
			TEST_RUNTIME_DEBUG( rt )
			{
				TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
				PRINT_TABS(rt) printf( "InterpretExpression_L14 return[%d] last_token[%s][%s] " , nret , _zlang_token_type_str , rt->travel_token ); DebugPrintObject( rt , obj2 ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE
			}
			if( nret == ZLANG_INFO_END_OF_EXPRESSION )
			{
				SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_EXPECT_OBJECT , "expect object" )
				IF_ATOMIC_OBJECT_THEN_UNLOCK(*result_obj)
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return ZLANG_ERROR_EXPECT_OBJECT;
			}
			else if( nret )
			{
				IF_ATOMIC_OBJECT_THEN_UNLOCK(*result_obj)
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return nret;
			}
			
			TEST_RUNTIME_DEBUG( rt )
			{
				PRINT_TABS(rt) printf( "PROCESS operator TOKEN_TYPE_OPERATOR_DIV_EVAL - (*result_obj) " ); DebugPrintObject( rt , (*result_obj) ); printf( " and obj2 " ); DebugPrintObject( rt , obj2 ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE
			}
			
			nret = Operator_DIV( rt , (*result_obj) , obj2 , & obj3 ) ;
			if( HaveFatalException(rt) )
			{
				TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "INVOKE Operator_DIV throw fatal exception[%d]" , HaveFatalException(rt) )
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return ZLANG_INFO_THROW;
			}
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "INVOKE Operator_DIV return[%d]" , nret )
			if( nret )
			{
				IF_ATOMIC_OBJECT_THEN_UNLOCK(*result_obj)
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return nret;
			}
			
			TEST_RUNTIME_DEBUG( rt ) { PRINT_TABS(rt) printf( "EvalObject " ); DebugPrintObject(rt,obj3); printf( " to " ); DebugPrintObject(rt,(*result_obj)); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE }
			nret = EvalObject( rt , (*result_obj) , obj3 ) ;
			if( nret < 0 )
			{
				TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "EvalObject failed[%d]" , nret )
				IF_ATOMIC_OBJECT_THEN_UNLOCK(*result_obj)
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return nret;
			}
			
			TEST_RUNTIME_DEBUG( rt )
			{
				PRINT_TABS(rt) printf( "PROCESS-RESULT operator TOKEN_TYPE_OPERATOR_DIV_EVAL - (*result_obj) " ); DebugPrintObject( rt , (*result_obj) ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE
			}
			
			IF_ATOMIC_OBJECT_THEN_UNLOCK(*result_obj)
		}
		else if( token_info2->token_type == TOKEN_TYPE_OPERATOR_MOD_EVAL )
		{
			NEXTTOKEN( rt )
			
			IF_ATOMIC_OBJECT_THEN_LOCK(*result_obj)
			
			TEST_RUNTIME_DEBUG( rt )
			{
				TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
				PRINT_TABS_AND_FORMAT( rt , "CALL InterpretExpression_L14 , last_token[%s][%s]" , _zlang_token_type_str , rt->travel_token )
			}
			interp_expr_ctx->get_expr_left_or_right = GET_EXPRESSION_RIGHT ;
			nret = InterpretExpression_L14( rt , interp_stat_ctx , interp_expr_ctx , & obj2 ) ;
			TEST_RUNTIME_DEBUG( rt )
			{
				TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
				PRINT_TABS(rt) printf( "InterpretExpression_L14 return[%d] last_token[%s][%s] " , nret , _zlang_token_type_str , rt->travel_token ); DebugPrintObject( rt , obj2 ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE
			}
			if( nret == ZLANG_INFO_END_OF_EXPRESSION )
			{
				SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_EXPECT_OBJECT , "expect object" )
				IF_ATOMIC_OBJECT_THEN_UNLOCK(*result_obj)
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return ZLANG_ERROR_EXPECT_OBJECT;
			}
			else if( nret )
			{
				IF_ATOMIC_OBJECT_THEN_UNLOCK(*result_obj)
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return nret;
			}
			
			TEST_RUNTIME_DEBUG( rt )
			{
				PRINT_TABS(rt) printf( "PROCESS operator TOKEN_TYPE_OPERATOR_MOD_EVAL - (*result_obj) " ); DebugPrintObject( rt , (*result_obj) ); printf( " and obj2 " ); DebugPrintObject( rt , obj2 ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE
			}
			
			nret = Operator_MOD( rt , (*result_obj) , obj2 , & obj3 ) ;
			if( HaveFatalException(rt) )
			{
				TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "INVOKE Operator_DIV throw fatal exception[%d]" , HaveFatalException(rt) )
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return ZLANG_INFO_THROW;
			}
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "INVOKE Operator_MOD return[%d]" , nret )
			if( nret )
			{
				IF_ATOMIC_OBJECT_THEN_UNLOCK(*result_obj)
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return nret;
			}
			
			TEST_RUNTIME_DEBUG( rt ) { PRINT_TABS(rt) printf( "EvalObject " ); DebugPrintObject(rt,obj3); printf( " to " ); DebugPrintObject(rt,(*result_obj)); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE }
			nret = EvalObject( rt , (*result_obj) , obj3 ) ;
			if( nret < 0 )
			{
				TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "EvalObject failed[%d]" , nret )
				IF_ATOMIC_OBJECT_THEN_UNLOCK(*result_obj)
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return nret;
			}
			
			TEST_RUNTIME_DEBUG( rt )
			{
				PRINT_TABS(rt) printf( "PROCESS-RESULT operator TOKEN_TYPE_OPERATOR_MOD_EVAL - (*result_obj) " ); DebugPrintObject( rt , (*result_obj) ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE
			}
			
			IF_ATOMIC_OBJECT_THEN_UNLOCK(*result_obj)
		}
		else if( token_info2->token_type == TOKEN_TYPE_BIT_AND_EVAL )
		{
			NEXTTOKEN( rt )
			
			IF_ATOMIC_OBJECT_THEN_LOCK(*result_obj)
			
			TEST_RUNTIME_DEBUG( rt )
			{
				TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
				PRINT_TABS_AND_FORMAT( rt , "CALL InterpretExpression_L14 , last_token[%s][%s]" , _zlang_token_type_str , rt->travel_token )
			}
			interp_expr_ctx->get_expr_left_or_right = GET_EXPRESSION_RIGHT ;
			nret = InterpretExpression_L14( rt , interp_stat_ctx , interp_expr_ctx , & obj2 ) ;
			TEST_RUNTIME_DEBUG( rt )
			{
				TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
				PRINT_TABS(rt) printf( "InterpretExpression_L14 return[%d] last_token[%s][%s] " , nret , _zlang_token_type_str , rt->travel_token ); DebugPrintObject( rt , obj2 ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE
			}
			if( nret == ZLANG_INFO_END_OF_EXPRESSION )
			{
				SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_EXPECT_OBJECT , "expect object" )
				IF_ATOMIC_OBJECT_THEN_UNLOCK(*result_obj)
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return ZLANG_ERROR_EXPECT_OBJECT;
			}
			else if( nret )
			{
				IF_ATOMIC_OBJECT_THEN_UNLOCK(*result_obj)
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return nret;
			}
			
			TEST_RUNTIME_DEBUG( rt )
			{
				PRINT_TABS(rt) printf( "PROCESS operator TOKEN_TYPE_BIT_AND_EVAL - (*result_obj) " ); DebugPrintObject( rt , (*result_obj) ); printf( " and obj2 " ); DebugPrintObject( rt , obj2 ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE
			}
			
			nret = Bit_AND( rt , (*result_obj) , obj2 , & obj3 ) ;
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "INVOKE Bit_AND return[%d]" , nret )
			if( nret )
			{
				IF_ATOMIC_OBJECT_THEN_UNLOCK(*result_obj)
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return nret;
			}
			
			TEST_RUNTIME_DEBUG( rt ) { PRINT_TABS(rt) printf( "EvalObject " ); DebugPrintObject(rt,obj3); printf( " to " ); DebugPrintObject(rt,(*result_obj)); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE }
			nret = EvalObject( rt , (*result_obj) , obj3 ) ;
			if( nret < 0 )
			{
				TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "EvalObject failed[%d]" , nret )
				IF_ATOMIC_OBJECT_THEN_UNLOCK(*result_obj)
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return nret;
			}
			
			TEST_RUNTIME_DEBUG( rt )
			{
				PRINT_TABS(rt) printf( "PROCESS-RESULT operator TOKEN_TYPE_BIT_AND_EVAL - (*result_obj) " ); DebugPrintObject( rt , (*result_obj) ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE
			}
			
			IF_ATOMIC_OBJECT_THEN_UNLOCK(*result_obj)
		}
		else if( token_info2->token_type == TOKEN_TYPE_BIT_XOR_EVAL )
		{
			NEXTTOKEN( rt )
			
			IF_ATOMIC_OBJECT_THEN_LOCK(*result_obj)
			
			TEST_RUNTIME_DEBUG( rt )
			{
				TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
				PRINT_TABS_AND_FORMAT( rt , "CALL InterpretExpression_L14 , last_token[%s][%s]" , _zlang_token_type_str , rt->travel_token )
			}
			interp_expr_ctx->get_expr_left_or_right = GET_EXPRESSION_RIGHT ;
			nret = InterpretExpression_L14( rt , interp_stat_ctx , interp_expr_ctx , & obj2 ) ;
			TEST_RUNTIME_DEBUG( rt )
			{
				TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
				PRINT_TABS(rt) printf( "InterpretExpression_L14 return[%d] last_token[%s][%s] " , nret , _zlang_token_type_str , rt->travel_token ); DebugPrintObject( rt , obj2 ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE
			}
			if( nret == ZLANG_INFO_END_OF_EXPRESSION )
			{
				SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_EXPECT_OBJECT , "expect object" )
				IF_ATOMIC_OBJECT_THEN_UNLOCK(*result_obj)
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return ZLANG_ERROR_EXPECT_OBJECT;
			}
			else if( nret )
			{
				IF_ATOMIC_OBJECT_THEN_UNLOCK(*result_obj)
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return nret;
			}
			
			TEST_RUNTIME_DEBUG( rt )
			{
				PRINT_TABS(rt) printf( "PROCESS operator TOKEN_TYPE_BIT_XOR_EVAL - (*result_obj) " ); DebugPrintObject( rt , (*result_obj) ); printf( " and obj2 " ); DebugPrintObject( rt , obj2 ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE
			}
			
			nret = Bit_XOR( rt , (*result_obj) , obj2 , & obj3 ) ;
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "INVOKE Bit_XOR return[%d]" , nret )
			if( nret )
			{
				IF_ATOMIC_OBJECT_THEN_UNLOCK(*result_obj)
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return nret;
			}
			
			TEST_RUNTIME_DEBUG( rt ) { PRINT_TABS(rt) printf( "EvalObject " ); DebugPrintObject(rt,obj3); printf( " to " ); DebugPrintObject(rt,(*result_obj)); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE }
			nret = EvalObject( rt , (*result_obj) , obj3 ) ;
			if( nret < 0 )
			{
				TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "EvalObject failed[%d]" , nret )
				IF_ATOMIC_OBJECT_THEN_UNLOCK(*result_obj)
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return nret;
			}
			
			TEST_RUNTIME_DEBUG( rt )
			{
				PRINT_TABS(rt) printf( "PROCESS-RESULT operator TOKEN_TYPE_BIT_XOR_EVAL - (*result_obj) " ); DebugPrintObject( rt , (*result_obj) ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE
			}
			
			IF_ATOMIC_OBJECT_THEN_UNLOCK(*result_obj)
		}
		else if( token_info2->token_type == TOKEN_TYPE_BIT_OR_EVAL )
		{
			NEXTTOKEN( rt )
			
			IF_ATOMIC_OBJECT_THEN_LOCK(*result_obj)
			
			TEST_RUNTIME_DEBUG( rt )
			{
				TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
				PRINT_TABS_AND_FORMAT( rt , "CALL InterpretExpression_L14 , last_token[%s][%s]" , _zlang_token_type_str , rt->travel_token )
			}
			interp_expr_ctx->get_expr_left_or_right = GET_EXPRESSION_RIGHT ;
			nret = InterpretExpression_L14( rt , interp_stat_ctx , interp_expr_ctx , & obj2 ) ;
			TEST_RUNTIME_DEBUG( rt )
			{
				TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
				PRINT_TABS(rt) printf( "InterpretExpression_L14 return[%d] last_token[%s][%s] " , nret , _zlang_token_type_str , rt->travel_token ); DebugPrintObject( rt , obj2 ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE
			}
			if( nret == ZLANG_INFO_END_OF_EXPRESSION )
			{
				SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_EXPECT_OBJECT , "expect object" )
				IF_ATOMIC_OBJECT_THEN_UNLOCK(*result_obj)
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return ZLANG_ERROR_EXPECT_OBJECT;
			}
			else if( nret )
			{
				IF_ATOMIC_OBJECT_THEN_UNLOCK(*result_obj)
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return nret;
			}
			
			TEST_RUNTIME_DEBUG( rt )
			{
				PRINT_TABS(rt) printf( "PROCESS operator TOKEN_TYPE_BIT_OR_EVAL - (*result_obj) " ); DebugPrintObject( rt , (*result_obj) ); printf( " and obj2 " ); DebugPrintObject( rt , obj2 ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE
			}
			
			nret = Bit_OR( rt , (*result_obj) , obj2 , & obj3 ) ;
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "INVOKE Bit_OR return[%d]" , nret )
			if( nret )
			{
				IF_ATOMIC_OBJECT_THEN_UNLOCK(*result_obj)
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return nret;
			}
			
			TEST_RUNTIME_DEBUG( rt ) { PRINT_TABS(rt) printf( "EvalObject " ); DebugPrintObject(rt,obj3); printf( " to " ); DebugPrintObject(rt,(*result_obj)); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE }
			nret = EvalObject( rt , (*result_obj) , obj3 ) ;
			if( nret < 0 )
			{
				TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "EvalObject failed[%d]" , nret )
				IF_ATOMIC_OBJECT_THEN_UNLOCK(*result_obj)
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return nret;
			}
			
			TEST_RUNTIME_DEBUG( rt )
			{
				PRINT_TABS(rt) printf( "PROCESS-RESULT operator TOKEN_TYPE_BIT_OR_EVAL - (*result_obj) " ); DebugPrintObject( rt , (*result_obj) ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE
			}
			
			IF_ATOMIC_OBJECT_THEN_UNLOCK(*result_obj)
		}
		else if( token_info2->token_type == TOKEN_TYPE_BIT_MOVELEFT_EVAL )
		{
			NEXTTOKEN( rt )
			
			IF_ATOMIC_OBJECT_THEN_LOCK(*result_obj)
			
			TEST_RUNTIME_DEBUG( rt )
			{
				TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
				PRINT_TABS_AND_FORMAT( rt , "CALL InterpretExpression_L14 , last_token[%s][%s]" , _zlang_token_type_str , rt->travel_token )
			}
			interp_expr_ctx->get_expr_left_or_right = GET_EXPRESSION_RIGHT ;
			nret = InterpretExpression_L14( rt , interp_stat_ctx , interp_expr_ctx , & obj2 ) ;
			TEST_RUNTIME_DEBUG( rt )
			{
				TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
				PRINT_TABS(rt) printf( "InterpretExpression_L14 return[%d] last_token[%s][%s] " , nret , _zlang_token_type_str , rt->travel_token ); DebugPrintObject( rt , obj2 ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE
			}
			if( nret == ZLANG_INFO_END_OF_EXPRESSION )
			{
				SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_EXPECT_OBJECT , "expect object" )
				IF_ATOMIC_OBJECT_THEN_UNLOCK(*result_obj)
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return ZLANG_ERROR_EXPECT_OBJECT;
			}
			else if( nret )
			{
				IF_ATOMIC_OBJECT_THEN_UNLOCK(*result_obj)
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return nret;
			}
			
			TEST_RUNTIME_DEBUG( rt )
			{
				PRINT_TABS(rt) printf( "PROCESS operator TOKEN_TYPE_BIT_MOVELEFT_EVAL - (*result_obj) " ); DebugPrintObject( rt , (*result_obj) ); printf( " and obj2 " ); DebugPrintObject( rt , obj2 ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE
			}
			
			nret = Bit_MOVELEFT( rt , (*result_obj) , obj2 , & obj3 ) ;
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "INVOKE Bit_MOVELEFT return[%d]" , nret )
			if( nret )
			{
				IF_ATOMIC_OBJECT_THEN_UNLOCK(*result_obj)
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return nret;
			}
			
			TEST_RUNTIME_DEBUG( rt ) { PRINT_TABS(rt) printf( "EvalObject " ); DebugPrintObject(rt,obj3); printf( " to " ); DebugPrintObject(rt,(*result_obj)); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE }
			nret = EvalObject( rt , (*result_obj) , obj3 ) ;
			if( nret < 0 )
			{
				TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "EvalObject failed[%d]" , nret )
				IF_ATOMIC_OBJECT_THEN_UNLOCK(*result_obj)
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return nret;
			}
			
			TEST_RUNTIME_DEBUG( rt )
			{
				PRINT_TABS(rt) printf( "PROCESS-RESULT operator TOKEN_TYPE_BIT_MOVELEFT_EVAL - (*result_obj) " ); DebugPrintObject( rt , (*result_obj) ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE
			}
			
			IF_ATOMIC_OBJECT_THEN_UNLOCK(*result_obj)
		}
		else if( token_info2->token_type == TOKEN_TYPE_BIT_MOVERIGHT_EVAL )
		{
			NEXTTOKEN( rt )
			
			IF_ATOMIC_OBJECT_THEN_LOCK(*result_obj)
			
			TEST_RUNTIME_DEBUG( rt )
			{
				TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
				PRINT_TABS_AND_FORMAT( rt , "CALL InterpretExpression_L14 , last_token[%s][%s]" , _zlang_token_type_str , rt->travel_token )
			}
			interp_expr_ctx->get_expr_left_or_right = GET_EXPRESSION_RIGHT ;
			nret = InterpretExpression_L14( rt , interp_stat_ctx , interp_expr_ctx , & obj2 ) ;
			TEST_RUNTIME_DEBUG( rt )
			{
				TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
				PRINT_TABS(rt) printf( "InterpretExpression_L14 return[%d] last_token[%s][%s] " , nret , _zlang_token_type_str , rt->travel_token ); DebugPrintObject( rt , obj2 ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE
			}
			if( nret == ZLANG_INFO_END_OF_EXPRESSION )
			{
				SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_EXPECT_OBJECT , "expect object" )
				IF_ATOMIC_OBJECT_THEN_UNLOCK(*result_obj)
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return ZLANG_ERROR_EXPECT_OBJECT;
			}
			else if( nret )
			{
				IF_ATOMIC_OBJECT_THEN_UNLOCK(*result_obj)
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return nret;
			}
			
			TEST_RUNTIME_DEBUG( rt )
			{
				PRINT_TABS(rt) printf( "PROCESS operator TOKEN_TYPE_BIT_MOVERIGHT_EVAL - (*result_obj) " ); DebugPrintObject( rt , (*result_obj) ); printf( " and obj2 " ); DebugPrintObject( rt , obj2 ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE
			}
			
			nret = Bit_MOVERIGHT( rt , (*result_obj) , obj2 , & obj3 ) ;
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "INVOKE Bit_MOVERIGHT return[%d]" , nret )
			if( nret )
			{
				IF_ATOMIC_OBJECT_THEN_UNLOCK(*result_obj)
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return nret;
			}
			
			TEST_RUNTIME_DEBUG( rt ) { PRINT_TABS(rt) printf( "EvalObject " ); DebugPrintObject(rt,obj3); printf( " to " ); DebugPrintObject(rt,(*result_obj)); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE }
			nret = EvalObject( rt , (*result_obj) , obj3 ) ;
			if( nret < 0 )
			{
				TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "EvalObject failed[%d]" , nret )
				IF_ATOMIC_OBJECT_THEN_UNLOCK(*result_obj)
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return nret;
			}
			
			TEST_RUNTIME_DEBUG( rt )
			{
				PRINT_TABS(rt) printf( "PROCESS-RESULT operator TOKEN_TYPE_BIT_MOVERIGHT_EVAL - (*result_obj) " ); DebugPrintObject( rt , (*result_obj) ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE
			}
			
			IF_ATOMIC_OBJECT_THEN_UNLOCK(*result_obj)
		}
		else
		{
			break;
		}
	}
	
	TEST_RUNTIME_DEBUG_THEN_PRINT_LEAVE_FUNCTION(rt)
	
	return 0;
}

static int InterpretExpression_L14( struct ZlangRuntime *rt , struct ZlangInterpretStatementContext *interp_stat_ctx , struct ZlangInterpretExpressionContext *interp_expr_ctx , struct ZlangObject **result_obj )
{
	struct ZlangObject			*obj2 = NULL ;
	struct ZlangTokenDataUnitHeader		*token_info2 = NULL ;
	char					*token2 = NULL ;
	unsigned char				logic_result ;
	
	struct ZlangInterpretStatementContext	new_interp_stat_ctx ;
	
	int					nret = 0 ;
	
	CHECK_ALART_STACK_DEPTH( rt->zlang_stack_bottom,&rt )

	TEST_RUNTIME_DEBUG_THEN_PRINT_ENTER_FUNCTION(rt)
	
	TEST_RUNTIME_DEBUG( rt )
	{
		TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
		PRINT_TABS_AND_FORMAT( rt , "CALL InterpretExpression_L12_L13 last_token[%s][%s]" , _zlang_token_type_str , rt->travel_token )
	}
	nret = InterpretExpression_L12_L13( rt , interp_stat_ctx , interp_expr_ctx , result_obj ) ;
	TEST_RUNTIME_DEBUG( rt )
	{
		TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
		TEST_RUNTIME_DEBUG( rt ) { PRINT_TABS(rt) printf( "InterpretExpression_L12_L13 return[%d] last_token[%s][%s] " , nret , _zlang_token_type_str , rt->travel_token ); DebugPrintObject( rt , (*result_obj) ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE }
	}
	if( nret == ZLANG_INFO_END_OF_EXPRESSION )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT_LEAVE_FUNCTION(rt)
		return nret;
	}
	else if( nret == ZLANG_INFO_CONTINUE )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "CONTINUE" )
		TEST_RUNTIME_DEBUG_THEN_PRINT_LEAVE_FUNCTION(rt)
		return nret;
	}
	else if( nret == ZLANG_INFO_BREAK )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "BREAK" )
		TEST_RUNTIME_DEBUG_THEN_PRINT_LEAVE_FUNCTION(rt)
		return nret;
	}
	else if( nret == ZLANG_INFO_RETURN )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "RETURN" )
		TEST_RUNTIME_DEBUG_THEN_PRINT_LEAVE_FUNCTION(rt)
		return nret;
	}
	else if( nret == ZLANG_INFO_EXIT )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "EXIT" )
		TEST_RUNTIME_DEBUG_THEN_PRINT_LEAVE_FUNCTION(rt)
		return nret;
	}
	else if( nret )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
		return nret;
	}
	
	for( ; ; )
	{
		PEEKTOKEN_AND_SAVEINFO( rt , token_info2 , token2 )
		if( token_info2->token_type == TOKEN_TYPE_JUDGE )
		{
			NEXTTOKEN( rt )
			
			TEST_RUNTIME_DEBUG( rt )
			{
				PRINT_TABS(rt) printf( "PROCESS operator TOKEN_TYPE_JUDGE - (*result_obj) " ); DebugPrintObject( rt , (*result_obj) ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE
			}
			
			nret = IsObjectTrueValue( rt , (*result_obj) , & logic_result ) ;
			if( nret )
			{
				TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "IsObjectTrueValue return[%d]" , nret )
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return nret;
			}
			
			if( logic_result )
			{
				TEST_RUNTIME_DEBUG( rt )
				{
					PRINT_TABS_AND_FORMAT( rt , "CALL InterpretExpression" )
				}
				memset( & new_interp_stat_ctx , 0x00 , sizeof(struct ZlangInterpretStatementContext) );
				new_interp_stat_ctx.token_of_expression_end1 = TOKEN_TYPE_ANOTHER_SIDE ;
				nret = InterpretExpression( rt , & new_interp_stat_ctx , & obj2 ) ;
				TEST_RUNTIME_DEBUG( rt )
				{
					PRINT_TABS(rt) printf( "InterpretExpression return[%d] " , nret ); DebugPrintObject(rt,obj2); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE
					DebugStack( rt );
				}
				
				if( nret == ZLANG_INFO_END_OF_EXPRESSION && rt->travel_token_info->token_type == TOKEN_TYPE_ANOTHER_SIDE )
				{
					nret = SkipExpression( rt , TOKEN_TYPE_PART_OF_STATEMENT , TOKEN_TYPE_END_OF_STATEMENT , TOKEN_TYPE_END_OF_SUB_EXPRESSION ) ;
					if( nret != ZLANG_INFO_END_OF_EXPRESSION )
					{
						TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "SkipExpression return[%d]" , nret )
						TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
						return nret;
					}
				}
				else
				{
					TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "unexpect return[%d]" , nret )
					TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
					return nret;
				}
			}
			else
			{
				nret = SkipExpression( rt , TOKEN_TYPE_ANOTHER_SIDE , 0 , 0 ) ;
				if( nret != ZLANG_INFO_END_OF_EXPRESSION )
				{
					TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "SkipExpression return[%d]" , nret )
					TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
					return nret;
				}
				
				TEST_RUNTIME_DEBUG( rt )
				{
					PRINT_TABS_AND_FORMAT( rt , "CALL InterpretExpression" )
				}
				memset( & new_interp_stat_ctx , 0x00 , sizeof(struct ZlangInterpretStatementContext) );
				new_interp_stat_ctx.token_of_expression_end1 = TOKEN_TYPE_PART_OF_STATEMENT ;
				new_interp_stat_ctx.token_of_expression_end2 = TOKEN_TYPE_END_OF_STATEMENT ;
				new_interp_stat_ctx.token_of_expression_end3 = TOKEN_TYPE_END_OF_SUB_EXPRESSION ;
				nret = InterpretExpression( rt , & new_interp_stat_ctx , & obj2 ) ;
				TEST_RUNTIME_DEBUG( rt )
				{
					PRINT_TABS(rt) printf( "InterpretExpression return[%d] " , nret ); DebugPrintObject(rt,obj2); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE
					DebugStack( rt );
				}
				
				if( nret != ZLANG_INFO_END_OF_EXPRESSION )
				{
					TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "unexpect return[%d]" , nret )
					TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
					return nret;
				}
			}
			
			(*result_obj) = obj2 ;
			
			return ZLANG_INFO_END_OF_EXPRESSION;
		}
		else
		{
			break;
		}
	}
	
	TEST_RUNTIME_DEBUG_THEN_PRINT_LEAVE_FUNCTION(rt)
	
	return 0;
}

static int InterpretExpression_L12_L13( struct ZlangRuntime *rt , struct ZlangInterpretStatementContext *interp_stat_ctx , struct ZlangInterpretExpressionContext *interp_expr_ctx , struct ZlangObject **result_obj )
{
	struct ZlangObject		*obj2 = NULL ;
	struct ZlangObject		*obj3 = NULL ;
	struct ZlangTokenDataUnitHeader	*token_info2 = NULL ;
	char				*token2 = NULL ;
	
	int				nret = 0 ;
	
	CHECK_ALART_STACK_DEPTH( rt->zlang_stack_bottom,&rt )

	TEST_RUNTIME_DEBUG_THEN_PRINT_ENTER_FUNCTION(rt)
	
	TEST_RUNTIME_DEBUG( rt )
	{
		TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
		PRINT_TABS_AND_FORMAT( rt , "CALL InterpretExpression_L9_L10_L11 , last_token[%s][%s]" , _zlang_token_type_str , rt->travel_token )
	}
	nret = InterpretExpression_L9_L10_L11( rt , interp_stat_ctx , interp_expr_ctx , result_obj ) ;
	TEST_RUNTIME_DEBUG( rt )
	{
		TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
		TEST_RUNTIME_DEBUG( rt ) { PRINT_TABS(rt) printf( "InterpretExpression_L9_L10_L11 return[%d] last_token[%s][%s] " , nret , _zlang_token_type_str , rt->travel_token ); DebugPrintObject( rt , (*result_obj) ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE }
	}
	if( nret == ZLANG_INFO_END_OF_EXPRESSION )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT_LEAVE_FUNCTION(rt)
		return nret;
	}
	else if( nret == ZLANG_INFO_CONTINUE )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "CONTINUE" )
		TEST_RUNTIME_DEBUG_THEN_PRINT_LEAVE_FUNCTION(rt)
		return nret;
	}
	else if( nret == ZLANG_INFO_BREAK )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "BREAK" )
		TEST_RUNTIME_DEBUG_THEN_PRINT_LEAVE_FUNCTION(rt)
		return nret;
	}
	else if( nret == ZLANG_INFO_RETURN )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "RETURN" )
		TEST_RUNTIME_DEBUG_THEN_PRINT_LEAVE_FUNCTION(rt)
		return nret;
	}
	else if( nret == ZLANG_INFO_EXIT )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "EXIT" )
		TEST_RUNTIME_DEBUG_THEN_PRINT_LEAVE_FUNCTION(rt)
		return nret;
	}
	else if( nret )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
		return nret;
	}
	
	for( ; ; )
	{
		PEEKTOKEN_AND_SAVEINFO( rt , token_info2 , token2 )
		if( token_info2->token_type == TOKEN_TYPE_LOGIC_AND )
		{
			unsigned char		b ;
			
			NEXTTOKEN( rt )
			
			IsObjectTrueValue( rt , (*result_obj) , & b );
			if( b == FALSE )
			{
				TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "left value is false" )
				
				nret = SkipExpression( rt , TOKEN_TYPE_END_OF_SUB_EXPRESSION , 0 , 0 ) ;
				if( nret != ZLANG_INFO_END_OF_EXPRESSION )
				{
					SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_SKIP_EXPRESSION , "SkipExpression failed" )
					TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
					return ZLANG_ERROR_SKIP_EXPRESSION;
				}
				
				TEST_RUNTIME_DEBUG_THEN_PRINT_LEAVE_FUNCTION(rt)
				return ZLANG_INFO_END_OF_EXPRESSION;
			}
			else
			{
				TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "left value is true" )
			}
			
			TEST_RUNTIME_DEBUG( rt )
			{
				TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
				PRINT_TABS_AND_FORMAT( rt , "CALL InterpretExpression_L9_L10_L11 , last_token[%s][%s]" , _zlang_token_type_str , rt->travel_token )
			}
			nret = InterpretExpression_L9_L10_L11( rt , interp_stat_ctx , interp_expr_ctx , & obj2 ) ;
			TEST_RUNTIME_DEBUG( rt )
			{
				TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
				TEST_RUNTIME_DEBUG( rt ) { PRINT_TABS(rt) printf( "InterpretExpression_L9_L10_L11 return[%d] last_token[%s][%s] " , nret , _zlang_token_type_str , rt->travel_token ); DebugPrintObject( rt , obj2 ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE }
			}
			if( nret == ZLANG_INFO_END_OF_EXPRESSION )
			{
				SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_EXPECT_OBJECT , "expect object" )
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return ZLANG_ERROR_EXPECT_OBJECT;
			}
			else if( nret )
			{
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return nret;
			}
			
			TEST_RUNTIME_DEBUG( rt )
			{
				PRINT_TABS(rt) printf( "PROCESS operator TOKEN_TYPE_LOGIC_AND - (*result_obj) " ); DebugPrintObject( rt , (*result_obj) ); printf( " and obj2 " ); DebugPrintObject( rt , obj2 ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE
			}
			
			if( (*result_obj) == NULL )
			{
				SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_INTERNAL , "left object is null" )
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return ZLANG_ERROR_INTERNAL;
			}
			if( ! IsTypeOf( rt , (*result_obj) , GetBoolObjectInRuntimeObjectsHeap(rt) ) )
			{
				SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_OBJECT_TYPE_ISNOT_BOOL , "left obj type is not bool" )
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return ZLANG_ERROR_OBJECT_TYPE_ISNOT_BOOL;
			}
			
			if( obj2 == NULL )
			{
				SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_INTERNAL , "right object is null" )
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return ZLANG_ERROR_INTERNAL;
			}
			if( ! IsTypeOf( rt , obj2 , GetBoolObjectInRuntimeObjectsHeap(rt) ) )
			{
				SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_OBJECT_TYPE_ISNOT_BOOL , "right obj type is not bool" )
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return ZLANG_ERROR_OBJECT_TYPE_ISNOT_BOOL;
			}
			
			nret = Logic_AND( rt , (*result_obj) , obj2 , & obj3 ) ;
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "INVOKE Logic_AND return[%d]" , nret )
			if( nret )
			{
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return nret;
			}
			
			(*result_obj) = obj3 ;
			
			TEST_RUNTIME_DEBUG( rt )
			{
				PRINT_TABS(rt) printf( "PROCESS-RESULT operator TOKEN_TYPE_LOGIC_AND - (*result_obj) " ); DebugPrintObject( rt , (*result_obj) ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE
			}
		}
		else if( token_info2->token_type == TOKEN_TYPE_LOGIC_OR )
		{
			unsigned char	b ;
			
			NEXTTOKEN( rt )
			
			IsObjectTrueValue( rt , (*result_obj) , & b );
			if( b == TRUE )
			{
				TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "left value is true" )
				
				nret = SkipExpression( rt , TOKEN_TYPE_END_OF_SUB_EXPRESSION , 0 , 0 ) ;
				if( nret != ZLANG_INFO_END_OF_EXPRESSION )
				{
					SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_SKIP_EXPRESSION , "SkipExpression failed" )
					TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
					return ZLANG_ERROR_SKIP_EXPRESSION;
				}
				
				TEST_RUNTIME_DEBUG_THEN_PRINT_LEAVE_FUNCTION(rt)
				return ZLANG_INFO_END_OF_EXPRESSION;
			}
			else
			{
				TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "left value is false" )
			}
			
			TEST_RUNTIME_DEBUG( rt )
			{
				TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
				PRINT_TABS_AND_FORMAT( rt , "CALL InterpretExpression_L9_L10_L11 , last_token[%s][%s]" , _zlang_token_type_str , rt->travel_token )
			}
			nret = InterpretExpression_L9_L10_L11( rt , interp_stat_ctx , interp_expr_ctx , & obj2 ) ;
			TEST_RUNTIME_DEBUG( rt )
			{
				TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
				TEST_RUNTIME_DEBUG( rt ) { PRINT_TABS(rt) printf( "InterpretExpression_L9_L10_L11 return[%d] last_token[%s][%s] " , nret , _zlang_token_type_str , rt->travel_token ); DebugPrintObject( rt , obj2 ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE }
			}
			if( nret == ZLANG_INFO_END_OF_EXPRESSION )
			{
				SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_EXPECT_OBJECT , "expect object" )
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return ZLANG_ERROR_EXPECT_OBJECT;
			}
			else if( nret )
			{
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return nret;
			}
			
			TEST_RUNTIME_DEBUG( rt )
			{
				PRINT_TABS(rt) printf( "PROCESS operator TOKEN_TYPE_LOGIC_OR - (*result_obj) " ); DebugPrintObject( rt , (*result_obj) ); printf( " and obj2 " ); DebugPrintObject( rt , obj2 ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE
			}
			
			if( (*result_obj) == NULL )
			{
				SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_INTERNAL , "left object is null" )
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return ZLANG_ERROR_INTERNAL;
			}
			if( ! IsTypeOf( rt , (*result_obj) , GetBoolObjectInRuntimeObjectsHeap(rt) ) )
			{
				SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_OBJECT_TYPE_ISNOT_BOOL , "left obj type is not bool" )
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return ZLANG_ERROR_OBJECT_TYPE_ISNOT_BOOL;
			}
			
			if( obj2 == NULL )
			{
				SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_INTERNAL , "right object is null" )
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return ZLANG_ERROR_INTERNAL;
			}
			if( ! IsTypeOf( rt , obj2 , GetBoolObjectInRuntimeObjectsHeap(rt) ) )
			{
				SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_OBJECT_TYPE_ISNOT_BOOL , "right obj type is not bool" )
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return ZLANG_ERROR_OBJECT_TYPE_ISNOT_BOOL;
			}
			
			nret = Logic_OR( rt , (*result_obj) , obj2 , & obj3 ) ;
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "INVOKE Logic_OR return[%d]" , nret )
			if( nret )
			{
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return nret;
			}
			
			(*result_obj) = obj3 ;
			
			TEST_RUNTIME_DEBUG( rt )
			{
				PRINT_TABS(rt) printf( "PROCESS-RESULT operator TOKEN_TYPE_LOGIC_OR - (*result_obj) " ); DebugPrintObject( rt , (*result_obj) ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE
			}
		}
		else
		{
			break;
		}
	}
	
	TEST_RUNTIME_DEBUG_THEN_PRINT_LEAVE_FUNCTION(rt)
	
	return 0;
}

static int InterpretExpression_L9_L10_L11( struct ZlangRuntime *rt , struct ZlangInterpretStatementContext *interp_stat_ctx , struct ZlangInterpretExpressionContext *interp_expr_ctx , struct ZlangObject **result_obj )
{
	struct ZlangObject		*obj2 = NULL ;
	struct ZlangObject		*obj3 = NULL ;
	struct ZlangTokenDataUnitHeader	*token_info2 = NULL ;
	char				*token2 = NULL ;
	
	int				nret = 0 ;
	
	CHECK_ALART_STACK_DEPTH( rt->zlang_stack_bottom,&rt )

	TEST_RUNTIME_DEBUG_THEN_PRINT_ENTER_FUNCTION(rt)
	
	TEST_RUNTIME_DEBUG( rt )
	{
		TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
		PRINT_TABS_AND_FORMAT( rt , "CALL InterpretExpression_L7_L8 , last_token[%s][%s]" , _zlang_token_type_str , rt->travel_token )
	}
	nret = InterpretExpression_L7_L8( rt , interp_stat_ctx , interp_expr_ctx , result_obj ) ;
	TEST_RUNTIME_DEBUG( rt )
	{
		TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
		TEST_RUNTIME_DEBUG( rt ) { PRINT_TABS(rt) printf( "InterpretExpression_L7_L8 return[%d] last_token[%s][%s] " , nret , _zlang_token_type_str , rt->travel_token ); DebugPrintObject( rt , (*result_obj) ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE }
	}
	if( nret == ZLANG_INFO_END_OF_EXPRESSION )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT_LEAVE_FUNCTION(rt)
		return nret;
	}
	else if( nret == ZLANG_INFO_CONTINUE )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "CONTINUE" )
		TEST_RUNTIME_DEBUG_THEN_PRINT_LEAVE_FUNCTION(rt)
		return nret;
	}
	else if( nret == ZLANG_INFO_BREAK )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "BREAK" )
		TEST_RUNTIME_DEBUG_THEN_PRINT_LEAVE_FUNCTION(rt)
		return nret;
	}
	else if( nret == ZLANG_INFO_RETURN )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "RETURN" )
		TEST_RUNTIME_DEBUG_THEN_PRINT_LEAVE_FUNCTION(rt)
		return nret;
	}
	else if( nret == ZLANG_INFO_EXIT )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "EXIT" )
		TEST_RUNTIME_DEBUG_THEN_PRINT_LEAVE_FUNCTION(rt)
		return nret;
	}
	else if( nret )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
		return nret;
	}
	
	for( ; ; )
	{
		PEEKTOKEN_AND_SAVEINFO( rt , token_info2 , token2 )
		if( token_info2->token_type == TOKEN_TYPE_BIT_AND )
		{
			NEXTTOKEN( rt )
			
			TEST_RUNTIME_DEBUG( rt )
			{
				TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
				PRINT_TABS_AND_FORMAT( rt , "CALL InterpretExpression_L7_L8 , last_token[%s][%s]" , _zlang_token_type_str , rt->travel_token )
			}
			nret = InterpretExpression_L7_L8( rt , interp_stat_ctx , interp_expr_ctx , & obj2 ) ;
			TEST_RUNTIME_DEBUG( rt )
			{
				TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
				TEST_RUNTIME_DEBUG( rt ) { PRINT_TABS(rt) printf( "InterpretExpression_L7_L8 return[%d] last_token[%s][%s] " , nret , _zlang_token_type_str , rt->travel_token ); DebugPrintObject( rt , obj2 ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE }
			}
			if( nret == ZLANG_INFO_END_OF_EXPRESSION )
			{
				SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_EXPECT_OBJECT , "expect object" )
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return ZLANG_ERROR_EXPECT_OBJECT;
			}
			else if( nret )
			{
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return nret;
			}
			
			TEST_RUNTIME_DEBUG( rt )
			{
				PRINT_TABS(rt) printf( "PROCESS operator TOKEN_TYPE_BIT_AND - (*result_obj) " ); DebugPrintObject( rt , (*result_obj) ); printf( " and obj2 " ); DebugPrintObject( rt , obj2 ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE
			}
			
			nret = Bit_AND( rt , (*result_obj) , obj2 , & obj3 ) ;
			TEST_RUNTIME_DEBUG( rt ) { PRINT_TABS(rt) printf( "INVOKE Bit_AND return[%d] " , nret ); DebugPrintObject( rt , obj3 ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE }
			if( nret )
			{
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return nret;
			}
			
			(*result_obj) = obj3 ;
			
			TEST_RUNTIME_DEBUG( rt )
			{
				PRINT_TABS(rt) printf( "PROCESS-RESULT operator TOKEN_TYPE_BIT_AND - (*result_obj) " ); DebugPrintObject( rt , (*result_obj) ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE
			}
		}
		else if( token_info2->token_type == TOKEN_TYPE_BIT_XOR )
		{
			NEXTTOKEN( rt )
			
			TEST_RUNTIME_DEBUG( rt )
			{
				TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
				PRINT_TABS_AND_FORMAT( rt , "CALL InterpretExpression_L7_L8 , last_token[%s][%s]" , _zlang_token_type_str , rt->travel_token )
			}
			nret = InterpretExpression_L7_L8( rt , interp_stat_ctx , interp_expr_ctx , & obj2 ) ;
			TEST_RUNTIME_DEBUG( rt )
			{
				TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
				TEST_RUNTIME_DEBUG( rt ) { PRINT_TABS(rt) printf( "InterpretExpression_L7_L8 return[%d] last_token[%s][%s] " , nret , _zlang_token_type_str , rt->travel_token ); DebugPrintObject( rt , obj2 ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE }
			}
			if( nret == ZLANG_INFO_END_OF_EXPRESSION )
			{
				SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_EXPECT_OBJECT , "expect object" )
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return ZLANG_ERROR_EXPECT_OBJECT;
			}
			else if( nret )
			{
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return nret;
			}
			
			TEST_RUNTIME_DEBUG( rt )
			{
				PRINT_TABS(rt) printf( "PROCESS operator TOKEN_TYPE_BIT_XOR - (*result_obj) " ); DebugPrintObject( rt , (*result_obj) ); printf( " and obj2 " ); DebugPrintObject( rt , obj2 ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE
			}
			
			nret = Bit_XOR( rt , (*result_obj) , obj2 , & obj3 ) ;
			TEST_RUNTIME_DEBUG( rt ) { PRINT_TABS(rt) printf( "INVOKE Bit_XOR return[%d] " , nret ); DebugPrintObject( rt , obj3 ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE }
			if( nret )
			{
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return nret;
			}
			
			(*result_obj) = obj3 ;
			
			TEST_RUNTIME_DEBUG( rt )
			{
				PRINT_TABS(rt) printf( "PROCESS-RESULT operator TOKEN_TYPE_BIT_XOR - (*result_obj) " ); DebugPrintObject( rt , (*result_obj) ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE
			}
		}
		else if( token_info2->token_type == TOKEN_TYPE_BIT_OR )
		{
			NEXTTOKEN( rt )
			
			TEST_RUNTIME_DEBUG( rt )
			{
				TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
				PRINT_TABS_AND_FORMAT( rt , "CALL InterpretExpression_L7_L8 , last_token[%s][%s]" , _zlang_token_type_str , rt->travel_token )
			}
			nret = InterpretExpression_L7_L8( rt , interp_stat_ctx , interp_expr_ctx , & obj2 ) ;
			TEST_RUNTIME_DEBUG( rt )
			{
				TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
				TEST_RUNTIME_DEBUG( rt ) { PRINT_TABS(rt) printf( "InterpretExpression_L7_L8 return[%d] last_token[%s][%s] " , nret , _zlang_token_type_str , rt->travel_token ); DebugPrintObject( rt , obj2 ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE }
			}
			if( nret == ZLANG_INFO_END_OF_EXPRESSION )
			{
				SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_EXPECT_OBJECT , "expect object" )
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return ZLANG_ERROR_EXPECT_OBJECT;
			}
			else if( nret )
			{
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return nret;
			}
			
			TEST_RUNTIME_DEBUG( rt )
			{
				PRINT_TABS(rt) printf( "PROCESS operator TOKEN_TYPE_BIT_OR - (*result_obj) " ); DebugPrintObject( rt , (*result_obj) ); printf( " and obj2 " ); DebugPrintObject( rt , obj2 ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE
			}
			
			nret = Bit_OR( rt , (*result_obj) , obj2 , & obj3 ) ;
			TEST_RUNTIME_DEBUG( rt ) { PRINT_TABS(rt) printf( "INVOKE Bit_OR return[%d] " , nret ); DebugPrintObject( rt , obj3 ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE }
			if( nret )
			{
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return nret;
			}
			
			(*result_obj) = obj3 ;
			
			TEST_RUNTIME_DEBUG( rt )
			{
				PRINT_TABS(rt) printf( "PROCESS-RESULT operator TOKEN_TYPE_BIT_OR - (*result_obj) " ); DebugPrintObject( rt , (*result_obj) ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE
			}
		}
		else
		{
			break;
		}
	}
	
	TEST_RUNTIME_DEBUG_THEN_PRINT_LEAVE_FUNCTION(rt)
	
	return 0;
}

static int InterpretExpression_L7_L8( struct ZlangRuntime *rt , struct ZlangInterpretStatementContext *interp_stat_ctx , struct ZlangInterpretExpressionContext *interp_expr_ctx , struct ZlangObject **result_obj )
{
	struct ZlangObject		*obj2 = NULL ;
	struct ZlangObject		*obj3 = NULL ;
	struct ZlangTokenDataUnitHeader	*token_info2 = NULL ;
	char				*token2 = NULL ;
	struct ZlangTokenDataUnitHeader	*token_info3 = NULL ;
	char				*token3 = NULL ;
	struct ZlangTokenDataPageHeader	*embellish_declare_token_datapage_header = NULL ;
	char				*embellish_declare_token_dataunit = NULL ;
	struct ZlangPropertiesEntity	*prop1_enti = NULL , *prop2_enti = NULL ;
	char				*type1_name = NULL , *type2_name = NULL ;
	
	int				nret = 0 ;
	
	CHECK_ALART_STACK_DEPTH( rt->zlang_stack_bottom,&rt )

	TEST_RUNTIME_DEBUG_THEN_PRINT_ENTER_FUNCTION(rt)
	
	TEST_RUNTIME_DEBUG( rt )
	{
		TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
		PRINT_TABS_AND_FORMAT( rt , "CALL InterpretExpression_L6 , last_token[%s][%s]" , _zlang_token_type_str , rt->travel_token )
	}
	nret = InterpretExpression_L6( rt , interp_stat_ctx , interp_expr_ctx , result_obj ) ;
	TEST_RUNTIME_DEBUG( rt )
	{
		TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
		TEST_RUNTIME_DEBUG( rt ) { PRINT_TABS(rt) printf( "InterpretExpression_L6 return[%d] last_token[%s][%s] " , nret , _zlang_token_type_str , rt->travel_token ); DebugPrintObject( rt , (*result_obj) ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE }
	}
	if( nret == ZLANG_INFO_END_OF_EXPRESSION )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT_LEAVE_FUNCTION(rt)
		return nret;
	}
	else if( nret == ZLANG_INFO_CONTINUE )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "CONTINUE" )
		TEST_RUNTIME_DEBUG_THEN_PRINT_LEAVE_FUNCTION(rt)
		return nret;
	}
	else if( nret == ZLANG_INFO_BREAK )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "BREAK" )
		TEST_RUNTIME_DEBUG_THEN_PRINT_LEAVE_FUNCTION(rt)
		return nret;
	}
	else if( nret == ZLANG_INFO_RETURN )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "RETURN" )
		TEST_RUNTIME_DEBUG_THEN_PRINT_LEAVE_FUNCTION(rt)
		return nret;
	}
	else if( nret == ZLANG_INFO_EXIT )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "EXIT" )
		TEST_RUNTIME_DEBUG_THEN_PRINT_LEAVE_FUNCTION(rt)
		return nret;
	}
	else if( nret )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
		return nret;
	}
	
	for( ; ; )
	{
		embellish_declare_token_datapage_header = rt->travel_token_datapage_header ;
		embellish_declare_token_dataunit = rt->travel_token_dataunit ;
		
		PEEKTOKEN_AND_SAVEINFO( rt , token_info2 , token2 )
		if( token_info2->token_type == TOKEN_TYPE_EQUAL )
		{
			NEXTTOKEN( rt )
			
			TEST_RUNTIME_DEBUG( rt )
			{
				TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
				PRINT_TABS_AND_FORMAT( rt , "CALL InterpretExpression_L6 , last_token[%s][%s]" , _zlang_token_type_str , rt->travel_token )
			}
			nret = InterpretExpression_L6( rt , interp_stat_ctx , interp_expr_ctx , & obj2 ) ;
			TEST_RUNTIME_DEBUG( rt )
			{
				TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
				TEST_RUNTIME_DEBUG( rt ) { PRINT_TABS(rt) printf( "InterpretExpression_L6 return[%d] last_token[%s][%s] " , nret , _zlang_token_type_str , rt->travel_token ); DebugPrintObject( rt , obj2 ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE }
			}
			if( nret == ZLANG_INFO_END_OF_EXPRESSION )
			{
				SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_EXPECT_OBJECT , "expect object" )
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return ZLANG_ERROR_EXPECT_OBJECT;
			}
			else if( nret )
			{
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return nret;
			}
			
			TEST_RUNTIME_DEBUG( rt )
			{
				PRINT_TABS(rt) printf( "PROCESS operator TOKEN_TYPE_EQUAL - (*result_obj) " ); DebugPrintObject( rt , (*result_obj) ); printf( " and obj2 " ); DebugPrintObject( rt , obj2 ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE
			}
			
			nret = Compare_EQUAL( rt , (*result_obj) , obj2 , & obj3 ) ;
			TEST_RUNTIME_DEBUG( rt ) { PRINT_TABS(rt) printf( "INVOKE Compare_EQUAL return[%d] " , nret ); DebugPrintObject( rt , obj3 ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE }
			if( nret )
			{
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return nret;
			}
			
			(*result_obj) = obj3 ;
			
			TEST_RUNTIME_DEBUG( rt )
			{
				PRINT_TABS(rt) printf( "PROCESS-RESULT operator TOKEN_TYPE_EQUAL - (*result_obj) " ); DebugPrintObject( rt , (*result_obj) ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE
			}
		}
		else if( token_info2->token_type == TOKEN_TYPE_NOT_EQUAL )
		{
			NEXTTOKEN( rt )
			
			TEST_RUNTIME_DEBUG( rt )
			{
				TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
				PRINT_TABS_AND_FORMAT( rt , "CALL InterpretExpression_L6 , last_token[%s][%s]" , _zlang_token_type_str , rt->travel_token )
			}
			nret = InterpretExpression_L6( rt , interp_stat_ctx , interp_expr_ctx , & obj2 ) ;
			TEST_RUNTIME_DEBUG( rt )
			{
				TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
				TEST_RUNTIME_DEBUG( rt ) { PRINT_TABS(rt) printf( "InterpretExpression_L6 return[%d] last_token[%s][%s] " , nret , _zlang_token_type_str , rt->travel_token ); DebugPrintObject( rt , obj2 ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE }
			}
			if( nret == ZLANG_INFO_END_OF_EXPRESSION )
			{
				SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_EXPECT_OBJECT , "expect object" )
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return ZLANG_ERROR_EXPECT_OBJECT;
			}
			else if( nret )
			{
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return nret;
			}
			
			TEST_RUNTIME_DEBUG( rt )
			{
				PRINT_TABS(rt) printf( "PROCESS operator TOKEN_TYPE_NOT_EQUAL - (*result_obj) " ); DebugPrintObject( rt , (*result_obj) ); printf( " and obj2 " ); DebugPrintObject( rt , obj2 ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE
			}
			
			nret = Compare_NOTEQUAL( rt , (*result_obj) , obj2 , & obj3 ) ;
			TEST_RUNTIME_DEBUG( rt ) { PRINT_TABS(rt) printf( "INVOKE Compare_NOTEQUAL return[%d] " , nret ); DebugPrintObject( rt , obj3 ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE }
			if( nret )
			{
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return nret;
			}
			
			(*result_obj) = obj3 ;
			
			TEST_RUNTIME_DEBUG( rt )
			{
				PRINT_TABS(rt) printf( "PROCESS-RESULT operator TOKEN_TYPE_NOT_EQUAL - (*result_obj) " ); DebugPrintObject( rt , (*result_obj) ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE
			}
		}
		else if( token_info2->token_type == TOKEN_TYPE_COMPARE_LT )
		{
			NEXTTOKEN( rt )
			
			TEST_RUNTIME_DEBUG( rt )
			{
				TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
				PRINT_TABS_AND_FORMAT( rt , "CALL InterpretExpression_L6 , last_token[%s][%s]" , _zlang_token_type_str , rt->travel_token )
			}
			nret = InterpretExpression_L6( rt , interp_stat_ctx , interp_expr_ctx , & obj2 ) ;
			TEST_RUNTIME_DEBUG( rt )
			{
				TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
				TEST_RUNTIME_DEBUG( rt ) { PRINT_TABS(rt) printf( "InterpretExpression_L6 return[%d] last_token[%s][%s] " , nret , _zlang_token_type_str , rt->travel_token ); DebugPrintObject( rt , obj2 ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE }
			}
			if( nret == ZLANG_INFO_END_OF_EXPRESSION )
			{
				SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_EXPECT_OBJECT , "expect object" )
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return ZLANG_ERROR_EXPECT_OBJECT;
			}
			else if( nret )
			{
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return nret;
			}
			
			PEEKTOKEN_AND_SAVEINFO( rt , token_info3 , token3 )
			if( token_info3->token_type == TOKEN_TYPE_PART_OF_STATEMENT || token_info3->token_type == TOKEN_TYPE_COMPARE_GT )
			{
				rt->travel_token_datapage_header = embellish_declare_token_datapage_header ;
				rt->travel_token_dataunit = embellish_declare_token_dataunit ;
				
				TEST_RUNTIME_DEBUG( rt )
				{
					TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
					PRINT_TABS_AND_FORMAT( rt , "CALL InterpretExpression_DeclareObjects , last_token[%s][%s]" , _zlang_token_type_str , rt->travel_token )
				}
				nret = InterpretExpression_DeclareObjects( rt , interp_stat_ctx , interp_expr_ctx , NULL , (*result_obj) , result_obj ) ;
				interp_stat_ctx->create_new_object_use_existed_name = 0 ;
				TEST_RUNTIME_DEBUG( rt )
				{
					TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
					TEST_RUNTIME_DEBUG( rt ) { PRINT_TABS(rt) printf( "InterpretExpression_DeclareObjects return[%d] last_token[%s][%s] " , nret , _zlang_token_type_str , rt->travel_token ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE }
				}
				if( nret == ZLANG_INFO_END_OF_EXPRESSION )
				{
					TEST_RUNTIME_DEBUG( rt )
					{
						DebugStack( rt );
					}
					TEST_RUNTIME_DEBUG_THEN_PRINT_LEAVE_FUNCTION(rt)
					return nret;
				}
				else if( nret )
				{
					TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
					return nret;
				}
				else
				{
					SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_INTERNAL , "internal error" )
					TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
					return ZLANG_ERROR_INTERNAL;
				}
			}
			
			TEST_RUNTIME_DEBUG( rt )
			{
				PRINT_TABS(rt) printf( "PROCESS operator TOKEN_TYPE_COMPARE_LT - (*result_obj) " ); DebugPrintObject( rt , (*result_obj) ); printf( " and obj2 " ); DebugPrintObject( rt , obj2 ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE
			}
			
			nret = Compare_LT( rt , (*result_obj) , obj2 , & obj3 ) ;
			TEST_RUNTIME_DEBUG( rt ) { PRINT_TABS(rt) printf( "INVOKE Compare_LT return[%d] " , nret ); DebugPrintObject( rt , obj3 ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE }
			if( nret )
			{
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return nret;
			}
			
			(*result_obj) = obj3 ;
			
			TEST_RUNTIME_DEBUG( rt )
			{
				PRINT_TABS(rt) printf( "PROCESS-RESULT operator TOKEN_TYPE_COMPARE_LT - (*result_obj) " ); DebugPrintObject( rt , (*result_obj) ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE
			}
		}
		else if( token_info2->token_type == TOKEN_TYPE_COMPARE_LE )
		{
			NEXTTOKEN( rt )
			
			TEST_RUNTIME_DEBUG( rt )
			{
				TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
				PRINT_TABS_AND_FORMAT( rt , "CALL InterpretExpression_L6 , last_token[%s][%s]" , _zlang_token_type_str , rt->travel_token )
			}
			nret = InterpretExpression_L6( rt , interp_stat_ctx , interp_expr_ctx , & obj2 ) ;
			TEST_RUNTIME_DEBUG( rt )
			{
				TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
				TEST_RUNTIME_DEBUG( rt ) { PRINT_TABS(rt) printf( "InterpretExpression_L6 return[%d] last_token[%s][%s] " , nret , _zlang_token_type_str , rt->travel_token ); DebugPrintObject( rt , obj2 ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE }
			}
			if( nret == ZLANG_INFO_END_OF_EXPRESSION )
			{
				SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_EXPECT_OBJECT , "expect object" )
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return ZLANG_ERROR_EXPECT_OBJECT;
			}
			else if( nret )
			{
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return nret;
			}
			
			TEST_RUNTIME_DEBUG( rt )
			{
				PRINT_TABS(rt) printf( "PROCESS operator TOKEN_TYPE_COMPARE_LE - (*result_obj) " ); DebugPrintObject( rt , (*result_obj) ); printf( " and obj2 " ); DebugPrintObject( rt , obj2 ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE
			}
			
			nret = Compare_LE( rt , (*result_obj) , obj2 , & obj3 ) ;
			TEST_RUNTIME_DEBUG( rt ) { PRINT_TABS(rt) printf( "INVOKE Compare_LE return[%d] " , nret ); DebugPrintObject( rt , obj3 ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE }
			if( nret )
			{
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return nret;
			}
			
			(*result_obj) = obj3 ;
			
			TEST_RUNTIME_DEBUG( rt )
			{
				PRINT_TABS(rt) printf( "PROCESS-RESULT operator TOKEN_TYPE_COMPARE_LE - (*result_obj) " ); DebugPrintObject( rt , (*result_obj) ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE
			}
		}
		else if( token_info2->token_type == TOKEN_TYPE_COMPARE_GT )
		{
			NEXTTOKEN( rt )
			
			TEST_RUNTIME_DEBUG( rt )
			{
				TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
				PRINT_TABS_AND_FORMAT( rt , "CALL InterpretExpression_L6 , last_token[%s][%s]" , _zlang_token_type_str , rt->travel_token )
			}
			nret = InterpretExpression_L6( rt , interp_stat_ctx , interp_expr_ctx , & obj2 ) ;
			TEST_RUNTIME_DEBUG( rt )
			{
				TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
				TEST_RUNTIME_DEBUG( rt ) { PRINT_TABS(rt) printf( "InterpretExpression_L6 return[%d] last_token[%s][%s] " , nret , _zlang_token_type_str , rt->travel_token ); DebugPrintObject( rt , obj2 ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE }
			}
			if( nret == ZLANG_INFO_END_OF_EXPRESSION )
			{
				SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_EXPECT_OBJECT , "expect object" )
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return ZLANG_ERROR_EXPECT_OBJECT;
			}
			else if( nret )
			{
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return nret;
			}
			
			TEST_RUNTIME_DEBUG( rt )
			{
				PRINT_TABS(rt) printf( "PROCESS operator TOKEN_TYPE_COMPARE_GT - (*result_obj) " ); DebugPrintObject( rt , (*result_obj) ); printf( " and obj2 " ); DebugPrintObject( rt , obj2 ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE
			}
			
			nret = Compare_GT( rt , (*result_obj) , obj2 , & obj3 ) ;
			TEST_RUNTIME_DEBUG( rt ) { PRINT_TABS(rt) printf( "INVOKE Compare_GT return[%d] " , nret ); DebugPrintObject( rt , obj3 ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE }
			if( nret )
			{
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return nret;
			}
			
			(*result_obj) = obj3 ;
			
			TEST_RUNTIME_DEBUG( rt )
			{
				PRINT_TABS(rt) printf( "PROCESS-RESULT operator TOKEN_TYPE_COMPARE_GT - (*result_obj) " ); DebugPrintObject( rt , (*result_obj) ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE
			}
		}
		else if( token_info2->token_type == TOKEN_TYPE_COMPARE_GE )
		{
			NEXTTOKEN( rt )
			
			TEST_RUNTIME_DEBUG( rt )
			{
				TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
				PRINT_TABS_AND_FORMAT( rt , "CALL InterpretExpression_L6 , last_token[%s][%s]" , _zlang_token_type_str , rt->travel_token )
			}
			nret = InterpretExpression_L6( rt , interp_stat_ctx , interp_expr_ctx , & obj2 ) ;
			TEST_RUNTIME_DEBUG( rt )
			{
				TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
				TEST_RUNTIME_DEBUG( rt ) { PRINT_TABS(rt) printf( "InterpretExpression_L6 return[%d] last_token[%s][%s] " , nret , _zlang_token_type_str , rt->travel_token ); DebugPrintObject( rt , obj2 ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE }
			}
			if( nret == ZLANG_INFO_END_OF_EXPRESSION )
			{
				SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_EXPECT_OBJECT , "expect object" )
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return ZLANG_ERROR_EXPECT_OBJECT;
			}
			else if( nret )
			{
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return nret;
			}
			
			TEST_RUNTIME_DEBUG( rt )
			{
				PRINT_TABS(rt) printf( "PROCESS operator TOKEN_TYPE_COMPARE_GE - (*result_obj) " ); DebugPrintObject( rt , (*result_obj) ); printf( " and obj2 " ); DebugPrintObject( rt , obj2 ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE
			}
			
			nret = Compare_GE( rt , (*result_obj) , obj2 , & obj3 ) ;
			TEST_RUNTIME_DEBUG( rt ) { PRINT_TABS(rt) printf( "INVOKE Compare_GE return[%d] " , nret ); DebugPrintObject( rt , obj3 ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE }
			if( nret )
			{
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return nret;
			}
			
			(*result_obj) = obj3 ;
			
			TEST_RUNTIME_DEBUG( rt )
			{
				PRINT_TABS(rt) printf( "PROCESS-RESULT operator TOKEN_TYPE_COMPARE_GE - (*result_obj) " ); DebugPrintObject( rt , (*result_obj) ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE
			}
		}
		else if( token_info2->token_type == TOKEN_TYPE_REFER_EQUAL )
		{
			NEXTTOKEN( rt )
			
			TEST_RUNTIME_DEBUG( rt )
			{
				TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
				PRINT_TABS_AND_FORMAT( rt , "CALL InterpretExpression_L6 , last_token[%s][%s]" , _zlang_token_type_str , rt->travel_token )
			}
			nret = InterpretExpression_L6( rt , interp_stat_ctx , interp_expr_ctx , & obj2 ) ;
			TEST_RUNTIME_DEBUG( rt )
			{
				TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
				TEST_RUNTIME_DEBUG( rt ) { PRINT_TABS(rt) printf( "InterpretExpression_L6 return[%d] last_token[%s][%s] " , nret , _zlang_token_type_str , rt->travel_token ); DebugPrintObject( rt , obj2 ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE }
			}
			if( nret == ZLANG_INFO_END_OF_EXPRESSION )
			{
				SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_EXPECT_OBJECT , "expect object" )
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return ZLANG_ERROR_EXPECT_OBJECT;
			}
			else if( nret )
			{
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return nret;
			}
			
			TEST_RUNTIME_DEBUG( rt )
			{
				PRINT_TABS(rt) printf( "PROCESS operator TOKEN_TYPE_REFER_EQUAL - (*result_obj) " ); DebugPrintObject( rt , (*result_obj) ); printf( " and obj2 " ); DebugPrintObject( rt , obj2 ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE
			}
			
			obj3 = CloneBoolObjectInTmpStack( rt , NULL ) ;
			if( obj3 == NULL )
			{
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return GET_RUNTIME_ERROR_NO(rt);
			}
			
			prop1_enti = GetObjectPropertiesEntity( (*result_obj) ) ;
			prop2_enti = GetObjectPropertiesEntity( obj2 ) ;
			if( prop1_enti == NULL && prop2_enti == NULL )
			{
				CallRuntimeFunction_bool_SetBoolValue( rt , obj3 , TRUE );
			}
			else if( prop1_enti == NULL || prop2_enti == NULL )
			{
				CallRuntimeFunction_bool_SetBoolValue( rt , obj3 , FALSE );
			}
			else
			{
				if( prop1_enti == prop2_enti )
					CallRuntimeFunction_bool_SetBoolValue( rt , obj3 , TRUE );
				else
					CallRuntimeFunction_bool_SetBoolValue( rt , obj3 , FALSE );
			}
			
			(*result_obj) = obj3 ;
			
			TEST_RUNTIME_DEBUG( rt )
			{
				PRINT_TABS(rt) printf( "PROCESS-RESULT operator TOKEN_TYPE_REFER_EQUAL - (*result_obj) " ); DebugPrintObject( rt , (*result_obj) ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE
			}
		}
		else if( token_info2->token_type == TOKEN_TYPE_REFER_NOT_EQUAL )
		{
			NEXTTOKEN( rt )
			
			TEST_RUNTIME_DEBUG( rt )
			{
				TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
				PRINT_TABS_AND_FORMAT( rt , "CALL InterpretExpression_L6 , last_token[%s][%s]" , _zlang_token_type_str , rt->travel_token )
			}
			nret = InterpretExpression_L6( rt , interp_stat_ctx , interp_expr_ctx , & obj2 ) ;
			TEST_RUNTIME_DEBUG( rt )
			{
				TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
				TEST_RUNTIME_DEBUG( rt ) { PRINT_TABS(rt) printf( "InterpretExpression_L6 return[%d] last_token[%s][%s] " , nret , _zlang_token_type_str , rt->travel_token ); DebugPrintObject( rt , obj2 ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE }
			}
			if( nret == ZLANG_INFO_END_OF_EXPRESSION )
			{
				SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_EXPECT_OBJECT , "expect object" )
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return ZLANG_ERROR_EXPECT_OBJECT;
			}
			else if( nret )
			{
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return nret;
			}
			
			TEST_RUNTIME_DEBUG( rt )
			{
				PRINT_TABS(rt) printf( "PROCESS operator TOKEN_TYPE_REFER_NOT_EQUAL " ); DebugPrintObject( rt , (*result_obj) ); printf( " and " ); DebugPrintObject( rt , obj2 ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE
			}
			
			obj3 = CloneBoolObjectInTmpStack( rt , NULL ) ;
			if( obj3 == NULL )
			{
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return GET_RUNTIME_ERROR_NO(rt);
			}
			
			prop1_enti = GetObjectPropertiesEntity( (*result_obj) ) ;
			prop2_enti = GetObjectPropertiesEntity( obj2 ) ;
			if( prop1_enti == NULL && prop2_enti == NULL )
			{
				CallRuntimeFunction_bool_SetBoolValue( rt , obj3 , FALSE );
			}
			else if( prop1_enti == NULL || prop2_enti == NULL )
			{
				CallRuntimeFunction_bool_SetBoolValue( rt , obj3 , TRUE );
			}
			else
			{
				if( prop1_enti == prop2_enti )
					CallRuntimeFunction_bool_SetBoolValue( rt , obj3 , FALSE );
				else
					CallRuntimeFunction_bool_SetBoolValue( rt , obj3 , TRUE );
			}
			
			(*result_obj) = obj3 ;
			
			TEST_RUNTIME_DEBUG( rt )
			{
				PRINT_TABS(rt) printf( "PROCESS-RESULT operator TOKEN_TYPE_REFER_NOT_EQUAL - (*result_obj) " ); DebugPrintObject( rt , (*result_obj) ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE
			}
		}
		else if( token_info2->token_type == TOKEN_TYPE_ANCESTOR_EQUAL )
		{
			NEXTTOKEN( rt )
			
			TEST_RUNTIME_DEBUG( rt )
			{
				TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
				PRINT_TABS_AND_FORMAT( rt , "CALL InterpretExpression_L6 , last_token[%s][%s]" , _zlang_token_type_str , rt->travel_token )
			}
			nret = InterpretExpression_L6( rt , interp_stat_ctx , interp_expr_ctx , & obj2 ) ;
			TEST_RUNTIME_DEBUG( rt )
			{
				TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
				TEST_RUNTIME_DEBUG( rt ) { PRINT_TABS(rt) printf( "InterpretExpression_L6 return[%d] last_token[%s][%s] " , nret , _zlang_token_type_str , rt->travel_token ); DebugPrintObject( rt , obj2 ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE }
			}
			if( nret == ZLANG_INFO_END_OF_EXPRESSION )
			{
				SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_EXPECT_OBJECT , "expect object" )
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return ZLANG_ERROR_EXPECT_OBJECT;
			}
			else if( nret )
			{
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return nret;
			}
			
			TEST_RUNTIME_DEBUG( rt )
			{
				PRINT_TABS(rt) printf( "PROCESS operator TOKEN_TYPE_ANCESTOR_EQUAL - (*result_obj) " ); DebugPrintObject( rt , (*result_obj) ); printf( " and obj2 " ); DebugPrintObject( rt , obj2 ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE
			}
			
			obj3 = CloneBoolObjectInTmpStack( rt , NULL ) ;
			if( obj3 == NULL )
			{
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return GET_RUNTIME_ERROR_NO(rt);
			}
			
			type1_name = GetCloneObjectName( (*result_obj) ) ;
			type2_name = GetCloneObjectName( obj2 ) ;
			if( type1_name == NULL && type2_name == NULL )
			{
				CallRuntimeFunction_bool_SetBoolValue( rt , obj3 , TRUE );
			}
			else if( type1_name == NULL || type2_name == NULL )
			{
				CallRuntimeFunction_bool_SetBoolValue( rt , obj3 , FALSE );
			}
			else
			{
				if( STRCMP( type1_name , == , type2_name ) )
					CallRuntimeFunction_bool_SetBoolValue( rt , obj3 , TRUE );
				else
					CallRuntimeFunction_bool_SetBoolValue( rt , obj3 , FALSE );
			}
			
			(*result_obj) = obj3 ;
			
			TEST_RUNTIME_DEBUG( rt )
			{
				PRINT_TABS(rt) printf( "PROCESS-RESULT operator TOKEN_TYPE_ANCESTOR_EQUAL - (*result_obj) " ); DebugPrintObject( rt , (*result_obj) ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE
			}
		}
		else if( token_info2->token_type == TOKEN_TYPE_ANCESTOR_NOT_EQUAL )
		{
			NEXTTOKEN( rt )
			
			TEST_RUNTIME_DEBUG( rt )
			{
				TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
				PRINT_TABS_AND_FORMAT( rt , "CALL InterpretExpression_L6 , last_token[%s][%s]" , _zlang_token_type_str , rt->travel_token )
			}
			nret = InterpretExpression_L6( rt , interp_stat_ctx , interp_expr_ctx , & obj2 ) ;
			TEST_RUNTIME_DEBUG( rt )
			{
				TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
				TEST_RUNTIME_DEBUG( rt ) { PRINT_TABS(rt) printf( "InterpretExpression_L6 return[%d] last_token[%s][%s] " , nret , _zlang_token_type_str , rt->travel_token ); DebugPrintObject( rt , obj2 ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE }
			}
			if( nret == ZLANG_INFO_END_OF_EXPRESSION )
			{
				SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_EXPECT_OBJECT , "expect object" )
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return ZLANG_ERROR_EXPECT_OBJECT;
			}
			else if( nret )
			{
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return nret;
			}
			
			TEST_RUNTIME_DEBUG( rt )
			{
				PRINT_TABS(rt) printf( "PROCESS operator TOKEN_TYPE_ANCESTOR_NOT_EQUAL " ); DebugPrintObject( rt , (*result_obj) ); printf( " and " ); DebugPrintObject( rt , obj2 ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE
			}
			
			obj3 = CloneBoolObjectInTmpStack( rt , NULL ) ;
			if( obj3 == NULL )
			{
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return GET_RUNTIME_ERROR_NO(rt);
			}
			
			type1_name = GetCloneObjectName( (*result_obj) ) ;
			type2_name = GetCloneObjectName( obj2 ) ;
			if( type1_name == NULL && type2_name == NULL )
			{
				CallRuntimeFunction_bool_SetBoolValue( rt , obj3 , FALSE );
			}
			else if( type1_name == NULL || type2_name == NULL )
			{
				CallRuntimeFunction_bool_SetBoolValue( rt , obj3 , TRUE );
			}
			else
			{
				if( STRCMP( type1_name , == , type2_name ) )
					CallRuntimeFunction_bool_SetBoolValue( rt , obj3 , FALSE );
				else
					CallRuntimeFunction_bool_SetBoolValue( rt , obj3 , TRUE );
			}
			
			(*result_obj) = obj3 ;
			
			TEST_RUNTIME_DEBUG( rt )
			{
				PRINT_TABS(rt) printf( "PROCESS-RESULT operator TOKEN_TYPE_ANCESTOR_NOT_EQUAL - (*result_obj) " ); DebugPrintObject( rt , (*result_obj) ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE
			}
		}
		else
		{
			break;
		}
	}
	
	TEST_RUNTIME_DEBUG_THEN_PRINT_LEAVE_FUNCTION(rt)
	
	return 0;
}

static int InterpretExpression_L6( struct ZlangRuntime *rt , struct ZlangInterpretStatementContext *interp_stat_ctx , struct ZlangInterpretExpressionContext *interp_expr_ctx , struct ZlangObject **result_obj )
{
	struct ZlangObject		*obj2 = NULL ;
	struct ZlangObject		*obj3 = NULL ;
	struct ZlangTokenDataUnitHeader	*token_info2 = NULL ;
	char				*token2 = NULL ;
	
	int				nret = 0 ;
	
	CHECK_ALART_STACK_DEPTH( rt->zlang_stack_bottom,&rt )

	TEST_RUNTIME_DEBUG_THEN_PRINT_ENTER_FUNCTION(rt)
	
	TEST_RUNTIME_DEBUG( rt )
	{
		TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
		PRINT_TABS_AND_FORMAT( rt , "CALL InterpretExpression_L5 , last_token[%s][%s]" , _zlang_token_type_str , rt->travel_token )
	}
	nret = InterpretExpression_L5( rt , interp_stat_ctx , interp_expr_ctx , result_obj ) ;
	TEST_RUNTIME_DEBUG( rt )
	{
		TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
		TEST_RUNTIME_DEBUG( rt ) { PRINT_TABS(rt) printf( "InterpretExpression_L5 return[%d] last_token[%s][%s] " , nret , _zlang_token_type_str , rt->travel_token ); DebugPrintObject( rt , (*result_obj) ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE }
	}
	if( nret == ZLANG_INFO_END_OF_EXPRESSION )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT_LEAVE_FUNCTION(rt)
		return nret;
	}
	else if( nret == ZLANG_INFO_CONTINUE )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "CONTINUE" )
		TEST_RUNTIME_DEBUG_THEN_PRINT_LEAVE_FUNCTION(rt)
		return nret;
	}
	else if( nret == ZLANG_INFO_BREAK )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "BREAK" )
		TEST_RUNTIME_DEBUG_THEN_PRINT_LEAVE_FUNCTION(rt)
		return nret;
	}
	else if( nret == ZLANG_INFO_RETURN )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "RETURN" )
		TEST_RUNTIME_DEBUG_THEN_PRINT_LEAVE_FUNCTION(rt)
		return nret;
	}
	else if( nret == ZLANG_INFO_EXIT )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "EXIT" )
		TEST_RUNTIME_DEBUG_THEN_PRINT_LEAVE_FUNCTION(rt)
		return nret;
	}
	else if( nret )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
		return nret;
	}
	
	for( ; ; )
	{
		PEEKTOKEN_AND_SAVEINFO( rt , token_info2 , token2 )
		if( token_info2->token_type == TOKEN_TYPE_BIT_MOVELEFT )
		{
			NEXTTOKEN( rt )
			
			TEST_RUNTIME_DEBUG( rt )
			{
				TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
				PRINT_TABS_AND_FORMAT( rt , "CALL InterpretExpression_L5 , last_token[%s][%s]" , _zlang_token_type_str , rt->travel_token )
			}
			nret = InterpretExpression_L5( rt , interp_stat_ctx , interp_expr_ctx , & obj2 ) ;
			TEST_RUNTIME_DEBUG( rt )
			{
				TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
				TEST_RUNTIME_DEBUG( rt ) { PRINT_TABS(rt) printf( "InterpretExpression_L5 return[%d] last_token[%s][%s] " , nret , _zlang_token_type_str , rt->travel_token ); DebugPrintObject( rt , obj2 ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE }
			}
			if( nret == ZLANG_INFO_END_OF_EXPRESSION )
			{
				SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_EXPECT_OBJECT , "expect object" )
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return ZLANG_ERROR_EXPECT_OBJECT;
			}
			else if( nret )
			{
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return nret;
			}
			
			TEST_RUNTIME_DEBUG( rt )
			{
				PRINT_TABS(rt) printf( "PROCESS operator TOKEN_TYPE_BIT_MOVELEFT - (*result_obj) " ); DebugPrintObject( rt , (*result_obj) ); printf( " and obj2 " ); DebugPrintObject( rt , obj2 ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE
			}
			
			nret = Bit_MOVELEFT( rt , (*result_obj) , obj2 , & obj3 ) ;
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "INVOKE Bit_MOVELEFT return[%d]" , nret )
			if( nret )
			{
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return nret;
			}
			
			(*result_obj) = obj3 ;
			
			TEST_RUNTIME_DEBUG( rt )
			{
				PRINT_TABS(rt) printf( "PROCESS-RESULT operator TOKEN_TYPE_BIT_MOVELEFT - (*result_obj) " ); DebugPrintObject( rt , (*result_obj) ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE
			}
		}
		else if( token_info2->token_type == TOKEN_TYPE_BIT_MOVERIGHT )
		{
			NEXTTOKEN( rt )
			
			TEST_RUNTIME_DEBUG( rt )
			{
				TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
				PRINT_TABS_AND_FORMAT( rt , "CALL InterpretExpression_L5 , last_token[%s][%s]" , _zlang_token_type_str , rt->travel_token )
			}
			nret = InterpretExpression_L5( rt , interp_stat_ctx , interp_expr_ctx , & obj2 ) ;
			TEST_RUNTIME_DEBUG( rt )
			{
				TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
				TEST_RUNTIME_DEBUG( rt ) { PRINT_TABS(rt) printf( "InterpretExpression_L5 return[%d] last_token[%s][%s] " , nret , _zlang_token_type_str , rt->travel_token ); DebugPrintObject( rt , obj2 ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE }
			}
			if( nret == ZLANG_INFO_END_OF_EXPRESSION )
			{
				SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_EXPECT_OBJECT , "expect object" )
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return ZLANG_ERROR_EXPECT_OBJECT;
			}
			else if( nret )
			{
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return nret;
			}
			
			TEST_RUNTIME_DEBUG( rt )
			{
				PRINT_TABS(rt) printf( "PROCESS operator TOKEN_TYPE_BIT_MOVERIGHT - (*result_obj) " ); DebugPrintObject( rt , (*result_obj) ); printf( " and obj2 " ); DebugPrintObject( rt , obj2 ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE
			}
			
			nret = Bit_MOVERIGHT( rt , (*result_obj) , obj2 , & obj3 ) ;
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "INVOKE Bit_MOVERIGHT return[%d]" , nret )
			if( nret )
			{
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return nret;
			}
			
			(*result_obj) = obj3 ;
			
			TEST_RUNTIME_DEBUG( rt )
			{
				PRINT_TABS(rt) printf( "PROCESS-RESULT operator TOKEN_TYPE_BIT_MOVERIGHT - (*result_obj) " ); DebugPrintObject( rt , (*result_obj) ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE
			}
		}
		else
		{
			break;
		}
	}
	
	TEST_RUNTIME_DEBUG_THEN_PRINT_LEAVE_FUNCTION(rt)
	
	return 0;
}

static int InterpretExpression_L5( struct ZlangRuntime *rt , struct ZlangInterpretStatementContext *interp_stat_ctx , struct ZlangInterpretExpressionContext *interp_expr_ctx , struct ZlangObject **result_obj )
{
	struct ZlangObject		*obj2 = NULL ;
	struct ZlangObject		*obj3 = NULL ;
	struct ZlangTokenDataUnitHeader	*token_info2 = NULL ;
	char				*token2 = NULL ;
	
	int				nret = 0 ;
	
	CHECK_ALART_STACK_DEPTH( rt->zlang_stack_bottom,&rt )

	TEST_RUNTIME_DEBUG_THEN_PRINT_ENTER_FUNCTION(rt)
	
	TEST_RUNTIME_DEBUG( rt )
	{
		TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
		PRINT_TABS_AND_FORMAT( rt , "CALL InterpretExpression_L4 , last_token[%s][%s]" , _zlang_token_type_str , rt->travel_token )
	}
	nret = InterpretExpression_L4( rt , interp_stat_ctx , interp_expr_ctx , result_obj ) ;
	TEST_RUNTIME_DEBUG( rt )
	{
		TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
		TEST_RUNTIME_DEBUG( rt ) { PRINT_TABS(rt) printf( "InterpretExpression_L4 return[%d] last_token[%s][%s] " , nret , _zlang_token_type_str , rt->travel_token ); DebugPrintObject( rt , (*result_obj) ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE }
	}
	if( nret == ZLANG_INFO_END_OF_EXPRESSION )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT_LEAVE_FUNCTION(rt)
		return nret;
	}
	else if( nret == ZLANG_INFO_CONTINUE )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "CONTINUE" )
		TEST_RUNTIME_DEBUG_THEN_PRINT_LEAVE_FUNCTION(rt)
		return nret;
	}
	else if( nret == ZLANG_INFO_BREAK )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "BREAK" )
		TEST_RUNTIME_DEBUG_THEN_PRINT_LEAVE_FUNCTION(rt)
		return nret;
	}
	else if( nret == ZLANG_INFO_RETURN )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "RETURN" )
		TEST_RUNTIME_DEBUG_THEN_PRINT_LEAVE_FUNCTION(rt)
		return nret;
	}
	else if( nret == ZLANG_INFO_EXIT )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "EXIT" )
		TEST_RUNTIME_DEBUG_THEN_PRINT_LEAVE_FUNCTION(rt)
		return nret;
	}
	else if( nret )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
		return nret;
	}
	
	for( ; ; )
	{
		PEEKTOKEN_AND_SAVEINFO( rt , token_info2 , token2 )
		if( token_info2->token_type == TOKEN_TYPE_OPERATOR_PLUS )
		{
			NEXTTOKEN( rt )
			
			TEST_RUNTIME_DEBUG( rt )
			{
				TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
				PRINT_TABS_AND_FORMAT( rt , "CALL InterpretExpression_L4 , last_token[%s][%s]" , _zlang_token_type_str , rt->travel_token )
			}
			nret = InterpretExpression_L4( rt , interp_stat_ctx , interp_expr_ctx , & obj2 ) ;
			TEST_RUNTIME_DEBUG( rt )
			{
				TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
				TEST_RUNTIME_DEBUG( rt ) { PRINT_TABS(rt) printf( "InterpretExpression_L4 return[%d] last_token[%s][%s] " , nret , _zlang_token_type_str , rt->travel_token ); DebugPrintObject( rt , obj2 ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE }
			}
			if( nret == ZLANG_INFO_END_OF_EXPRESSION )
			{
				SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_EXPECT_OBJECT , "expect object" )
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return ZLANG_ERROR_EXPECT_OBJECT;
			}
			else if( nret )
			{
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return nret;
			}
			
			TEST_RUNTIME_DEBUG( rt )
			{
				PRINT_TABS(rt) printf( "PROCESS operator TOKEN_TYPE_OPERATOR_PLUS - (*result_obj) " ); DebugPrintObject( rt , (*result_obj) ); printf( " and obj2 " ); DebugPrintObject( rt , obj2 ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE
			}
			
			nret = Operator_PLUS( rt , (*result_obj) , obj2 , & obj3 ) ;
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "INVOKE Operator_PLUS return[%d]" , nret )
			if( nret )
			{
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return nret;
			}
			
			(*result_obj) = obj3 ;
			
			TEST_RUNTIME_DEBUG( rt )
			{
				PRINT_TABS(rt) printf( "PROCESS-RESULT operator TOKEN_TYPE_OPERATOR_PLUS - (*result_obj) " ); DebugPrintObject( rt , (*result_obj) ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE
			}
		}
		else if( token_info2->token_type == TOKEN_TYPE_OPERATOR_MINUS )
		{
			NEXTTOKEN( rt )
			
			TEST_RUNTIME_DEBUG( rt )
			{
				TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
				PRINT_TABS_AND_FORMAT( rt , "CALL InterpretExpression_L4 , last_token[%s][%s]" , _zlang_token_type_str , rt->travel_token )
			}
			nret = InterpretExpression_L4( rt , interp_stat_ctx , interp_expr_ctx , & obj2 ) ;
			TEST_RUNTIME_DEBUG( rt )
			{
				TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
				TEST_RUNTIME_DEBUG( rt ) { PRINT_TABS(rt) printf( "InterpretExpression_L4 return[%d] last_token[%s][%s] " , nret , _zlang_token_type_str , rt->travel_token ); DebugPrintObject( rt , obj2 ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE }
			}
			if( nret == ZLANG_INFO_END_OF_EXPRESSION )
			{
				SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_EXPECT_OBJECT , "expect object" )
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return ZLANG_ERROR_EXPECT_OBJECT;
			}
			else if( nret )
			{
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return nret;
			}
			
			TEST_RUNTIME_DEBUG( rt )
			{
				PRINT_TABS(rt) printf( "PROCESS operator TOKEN_TYPE_OPERATOR_MINUS - (*result_obj) " ); DebugPrintObject( rt , (*result_obj) ); printf( " and obj2 " ); DebugPrintObject( rt , obj2 ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE
			}
			
			nret = Operator_MINUS( rt , (*result_obj) , obj2 , & obj3 ) ;
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "INVOKE Operator_MINUS return[%d]" , nret )
			if( nret )
			{
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return nret;
			}
			
			(*result_obj) = obj3 ;
			
			TEST_RUNTIME_DEBUG( rt )
			{
				PRINT_TABS(rt) printf( "PROCESS-RESULT operator TOKEN_TYPE_OPERATOR_MINUS - (*result_obj) " ); DebugPrintObject( rt , (*result_obj) ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE
			}
		}
		else
		{
			break;
		}
	}
	
	TEST_RUNTIME_DEBUG_THEN_PRINT_LEAVE_FUNCTION(rt)
	
	return 0;
}

static int InterpretExpression_L4( struct ZlangRuntime *rt , struct ZlangInterpretStatementContext *interp_stat_ctx , struct ZlangInterpretExpressionContext *interp_expr_ctx , struct ZlangObject **result_obj )
{
	struct ZlangObject		*obj2 = NULL ;
	struct ZlangObject		*obj3 = NULL ;
	struct ZlangTokenDataUnitHeader	*token_info2 = NULL ;
	char				*token2 = NULL ;
	
	int				nret = 0 ;
	
	CHECK_ALART_STACK_DEPTH( rt->zlang_stack_bottom,&rt )

	TEST_RUNTIME_DEBUG_THEN_PRINT_ENTER_FUNCTION(rt)
	
	TEST_RUNTIME_DEBUG( rt )
	{
		TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
		PRINT_TABS_AND_FORMAT( rt , "CALL InterpretExpression_L2 , last_token[%s][%s]" , _zlang_token_type_str , rt->travel_token )
	}
	nret = InterpretExpression_L2( rt , interp_stat_ctx , interp_expr_ctx , result_obj ) ;
	TEST_RUNTIME_DEBUG( rt )
	{
		TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
		TEST_RUNTIME_DEBUG( rt ) { PRINT_TABS(rt) printf( "InterpretExpression_L2 return[%d] last_token[%s][%s] " , nret , _zlang_token_type_str , rt->travel_token ); DebugPrintObject( rt , (*result_obj) ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE }
	}
	if( nret == ZLANG_INFO_END_OF_EXPRESSION )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT_LEAVE_FUNCTION(rt)
		return nret;
	}
	else if( nret == ZLANG_INFO_CONTINUE )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "CONTINUE" )
		TEST_RUNTIME_DEBUG_THEN_PRINT_LEAVE_FUNCTION(rt)
		return nret;
	}
	else if( nret == ZLANG_INFO_BREAK )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "BREAK" )
		TEST_RUNTIME_DEBUG_THEN_PRINT_LEAVE_FUNCTION(rt)
		return nret;
	}
	else if( nret == ZLANG_INFO_RETURN )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "RETURN" )
		TEST_RUNTIME_DEBUG_THEN_PRINT_LEAVE_FUNCTION(rt)
		return nret;
	}
	else if( nret == ZLANG_INFO_EXIT )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "EXIT" )
		TEST_RUNTIME_DEBUG_THEN_PRINT_LEAVE_FUNCTION(rt)
		return nret;
	}
	else if( nret )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
		return nret;
	}
	
	for( ; ; )
	{
		PEEKTOKEN_AND_SAVEINFO( rt , token_info2 , token2 )
		if( token_info2->token_type == TOKEN_TYPE_OPERATOR_MUL )
		{
			NEXTTOKEN( rt )
			
			TEST_RUNTIME_DEBUG( rt )
			{
				TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
				PRINT_TABS_AND_FORMAT( rt , "CALL InterpretExpression_L2 , last_token[%s][%s]" , _zlang_token_type_str , rt->travel_token )
			}
			nret = InterpretExpression_L2( rt , interp_stat_ctx , interp_expr_ctx , & obj2 ) ;
			TEST_RUNTIME_DEBUG( rt )
			{
				TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
				TEST_RUNTIME_DEBUG( rt ) { PRINT_TABS(rt) printf( "InterpretExpression_L2 return[%d] last_token[%s][%s] " , nret , _zlang_token_type_str , rt->travel_token ); DebugPrintObject( rt , obj2 ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE }
			}
			if( nret == ZLANG_INFO_END_OF_EXPRESSION )
			{
				SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_EXPECT_OBJECT , "expect object" )
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return ZLANG_ERROR_EXPECT_OBJECT;
			}
			else if( nret )
			{
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return nret;
			}
			
			TEST_RUNTIME_DEBUG( rt )
			{
				PRINT_TABS(rt) printf( "PROCESS operator TOKEN_TYPE_OPERATOR_MUL - (*result_obj) " ); DebugPrintObject( rt , (*result_obj) ); printf( " and obj2 " ); DebugPrintObject( rt , obj2 ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE
			}
			
			nret = Operator_MUL( rt , (*result_obj) , obj2 , & obj3 ) ;
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "INVOKE Operator_MUL return[%d]" , nret )
			if( nret )
			{
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return nret;
			}
			
			(*result_obj) = obj3 ;
			
			TEST_RUNTIME_DEBUG( rt )
			{
				PRINT_TABS(rt) printf( "PROCESS-RESULT operator TOKEN_TYPE_OPERATOR_MUL - (*result_obj) " ); DebugPrintObject( rt , (*result_obj) ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE
			}
		}
		else if( token_info2->token_type == TOKEN_TYPE_OPERATOR_DIV )
		{
			NEXTTOKEN( rt )
			
			TEST_RUNTIME_DEBUG( rt )
			{
				TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
				PRINT_TABS_AND_FORMAT( rt , "CALL InterpretExpression_L2 , last_token[%s][%s]" , _zlang_token_type_str , rt->travel_token )
			}
			nret = InterpretExpression_L2( rt , interp_stat_ctx , interp_expr_ctx , & obj2 ) ;
			TEST_RUNTIME_DEBUG( rt )
			{
				TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
				TEST_RUNTIME_DEBUG( rt ) { PRINT_TABS(rt) printf( "InterpretExpression_L2 return[%d] result_toe[%s] " , nret , _zlang_token_type_str ); DebugPrintObject( rt , obj2 ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE }
			}
			if( nret == ZLANG_INFO_END_OF_EXPRESSION )
			{
				SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_EXPECT_OBJECT , "expect object" )
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return ZLANG_ERROR_EXPECT_OBJECT;
			}
			else if( nret )
			{
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return nret;
			}
			
			TEST_RUNTIME_DEBUG( rt )
			{
				PRINT_TABS(rt) printf( "PROCESS operator TOKEN_TYPE_OPERATOR_DIV - (*result_obj) " ); DebugPrintObject( rt , (*result_obj) ); printf( " and obj2 " ); DebugPrintObject( rt , obj2 ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE
			}
			
			nret = Operator_DIV( rt , (*result_obj) , obj2 , & obj3 ) ;
			if( HaveFatalException(rt) )
			{
				TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "INVOKE Operator_DIV throw fatal exception[%d]" , HaveFatalException(rt) )
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return ZLANG_INFO_THROW;
			}
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "INVOKE Operator_DIV return[%d]" , nret )
			if( nret )
			{
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return nret;
			}
			
			(*result_obj) = obj3 ;
			
			TEST_RUNTIME_DEBUG( rt )
			{
				PRINT_TABS(rt) printf( "PROCESS-RESULT operator TOKEN_TYPE_OPERATOR_DIV - (*result_obj) " ); DebugPrintObject( rt , (*result_obj) ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE
			}
		}
		else if( token_info2->token_type == TOKEN_TYPE_OPERATOR_MOD )
		{
			NEXTTOKEN( rt )
			
			TEST_RUNTIME_DEBUG( rt )
			{
				TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
				PRINT_TABS_AND_FORMAT( rt , "CALL InterpretExpression_L2 , last_token[%s][%s]" , _zlang_token_type_str , rt->travel_token )
			}
			nret = InterpretExpression_L2( rt , interp_stat_ctx , interp_expr_ctx , & obj2 ) ;
			TEST_RUNTIME_DEBUG( rt )
			{
				TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
				TEST_RUNTIME_DEBUG( rt ) { PRINT_TABS(rt) printf( "InterpretExpression_L2 return[%d] result_toe[%s] " , nret , _zlang_token_type_str ); DebugPrintObject( rt , obj2 ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE }
			}
			if( nret == ZLANG_INFO_END_OF_EXPRESSION )
			{
				SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_EXPECT_OBJECT , "expect object" )
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return ZLANG_ERROR_EXPECT_OBJECT;
			}
			else if( nret )
			{
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return nret;
			}
			
			TEST_RUNTIME_DEBUG( rt )
			{
				PRINT_TABS(rt) printf( "PROCESS operator TOKEN_TYPE_OPERATOR_MOD - (*result_obj) " ); DebugPrintObject( rt , (*result_obj) ); printf( " and obj2 " ); DebugPrintObject( rt , obj2 ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE
			}
			
			nret = Operator_MOD( rt , (*result_obj) , obj2 , & obj3 ) ;
			if( HaveFatalException(rt) )
			{
				TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "INVOKE Operator_DIV throw fatal exception[%d]" , HaveFatalException(rt) )
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return ZLANG_INFO_THROW;
			}
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "INVOKE Operator_MOD return[%d]" , nret )
			if( nret )
			{
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return nret;
			}
			
			(*result_obj) = obj3 ;
			
			TEST_RUNTIME_DEBUG( rt )
			{
				PRINT_TABS(rt) printf( "PROCESS-RESULT operator TOKEN_TYPE_OPERATOR_MOD - (*result_obj) " ); DebugPrintObject( rt , (*result_obj) ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE
			}
		}
		else
		{
			break;
		}
	}
	
	TEST_RUNTIME_DEBUG_THEN_PRINT_LEAVE_FUNCTION(rt)
	
	return 0;
}

static int InterpretExpression_L2( struct ZlangRuntime *rt , struct ZlangInterpretStatementContext *interp_stat_ctx , struct ZlangInterpretExpressionContext *interp_expr_ctx , struct ZlangObject **result_obj )
{
	struct ZlangObject		*obj2 = NULL ;
	struct ZlangObject		*obj3 = NULL ;
	struct ZlangTokenDataUnitHeader	*token_info2 = NULL ;
	char				*token2 = NULL ;
	
	int				nret = 0 ;
	
	CHECK_ALART_STACK_DEPTH( rt->zlang_stack_bottom,&rt )
	
	TEST_RUNTIME_DEBUG_THEN_PRINT_ENTER_FUNCTION(rt)
	
	PEEKTOKEN_AND_SAVEINFO( rt , token_info2 , token2 )
	if( token_info2->token_type == TOKEN_TYPE_OPERATOR_MINUS )
	{
		NEXTTOKEN( rt )
		
		TEST_RUNTIME_DEBUG( rt )
		{
			TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
			PRINT_TABS_AND_FORMAT( rt , "CALL InterpretExpression_L1 , last_token[%s][%s]" , _zlang_token_type_str , rt->travel_token )
		}
		nret = InterpretExpression_L1( rt , interp_stat_ctx , interp_expr_ctx , & obj2 ) ;
		TEST_RUNTIME_DEBUG( rt )
		{
			TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
			TEST_RUNTIME_DEBUG( rt ) { PRINT_TABS(rt) printf( "InterpretExpression_L1 return[%d] last_token[%s][%s] " , nret , _zlang_token_type_str , rt->travel_token ); DebugPrintObject( rt , obj2 ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE }
		}
		if( nret == ZLANG_INFO_END_OF_EXPRESSION )
		{
			SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_EXPECT_OBJECT , "expect object" )
			TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
			return ZLANG_ERROR_EXPECT_OBJECT;
		}
		else if( nret )
		{
			TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
			return nret;
		}
		
		IF_ATOMIC_OBJECT_THEN_LOCK(obj2)
		
		TEST_RUNTIME_DEBUG( rt )
		{
			PRINT_TABS(rt) printf( "PROCESS operator UNARY-TOKEN_TYPE_OPERATOR_MINUS - (*result_obj) " ); DebugPrintObject( rt , (*result_obj) ); printf( " and obj2 " ); DebugPrintObject( rt , obj2 ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE
		}
		
		obj3 = CloneObjectInTmpStack( rt , NULL , obj2 ) ;
		if( obj3 == NULL )
		{
			IF_ATOMIC_OBJECT_THEN_UNLOCK(obj2)
			TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
			return GET_RUNTIME_ERROR_NO(rt);
		}
		
		nret = UnaryOperator_NEGATIVE( rt , obj3 ) ;
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "INVOKE UnaryOperator_NEGATIVE return[%d]" , nret )
		if( nret )
		{
			IF_ATOMIC_OBJECT_THEN_UNLOCK(obj2)
			TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
			return nret;
		}
		
		(*result_obj) = obj3 ;
		
		TEST_RUNTIME_DEBUG( rt )
		{
			PRINT_TABS(rt) printf( "PROCESS-RESULT operator UNARY-TOKEN_TYPE_OPERATOR_MINUS - (*result_obj) " ); DebugPrintObject( rt , (*result_obj) ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE
		}
		
		IF_ATOMIC_OBJECT_THEN_UNLOCK(obj2)
	}
	else if( token_info2->token_type == TOKEN_TYPE_NOT )
	{
		NEXTTOKEN( rt )
		
		TEST_RUNTIME_DEBUG( rt )
		{
			TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
			PRINT_TABS_AND_FORMAT( rt , "CALL InterpretExpression_L1 , last_token[%s][%s]" , _zlang_token_type_str , rt->travel_token )
		}
		nret = InterpretExpression_L1( rt , interp_stat_ctx , interp_expr_ctx , & obj2 ) ;
		TEST_RUNTIME_DEBUG( rt )
		{
			TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
			TEST_RUNTIME_DEBUG( rt ) { PRINT_TABS(rt) printf( "InterpretExpression_L1 return[%d] last_token[%s][%s] " , nret , _zlang_token_type_str , rt->travel_token ); DebugPrintObject( rt , obj2 ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE }
		}
		if( nret == ZLANG_INFO_END_OF_EXPRESSION )
		{
			SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_EXPECT_OBJECT , "expect object" )
			TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
			return ZLANG_ERROR_EXPECT_OBJECT;
		}
		else if( nret )
		{
			TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
			return nret;
		}
		
		IF_ATOMIC_OBJECT_THEN_LOCK(obj2)
		
		TEST_RUNTIME_DEBUG( rt )
		{
			PRINT_TABS(rt) printf( "PROCESS operator UNARY-TOKEN_TYPE_NOT - (*result_obj) " ); DebugPrintObject( rt , (*result_obj) ); printf( " and obj2 " ); DebugPrintObject( rt , obj2 ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE
		}
		
		obj3 = CloneObjectInTmpStack( rt , NULL , obj2 ) ;
		if( obj3 == NULL )
		{
			IF_ATOMIC_OBJECT_THEN_UNLOCK(*result_obj)
			TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
			return GET_RUNTIME_ERROR_NO(rt);
		}
		
		nret = UnaryOperator_NOT( rt , obj3 ) ;
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "INVOKE UnaryOperator_NOT return[%d]" , nret )
		if( nret )
		{
			IF_ATOMIC_OBJECT_THEN_UNLOCK(*result_obj)
			TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
			return nret;
		}
		
		(*result_obj) = obj3 ;
		
		TEST_RUNTIME_DEBUG( rt )
		{
			PRINT_TABS(rt) printf( "PROCESS-RESULT operator UNARY-TOKEN_TYPE_NOT - (*result_obj) " ); DebugPrintObject( rt , (*result_obj) ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE
		}
		
		IF_ATOMIC_OBJECT_THEN_UNLOCK(*result_obj)
	}
	else if( token_info2->token_type == TOKEN_TYPE_BIT_REVERSE )
	{
		NEXTTOKEN( rt )
		
		TEST_RUNTIME_DEBUG( rt )
		{
			TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
			PRINT_TABS_AND_FORMAT( rt , "CALL InterpretExpression_L1 , last_token[%s][%s]" , _zlang_token_type_str , rt->travel_token )
		}
		nret = InterpretExpression_L1( rt , interp_stat_ctx , interp_expr_ctx , & obj2 ) ;
		TEST_RUNTIME_DEBUG( rt )
		{
			TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
			TEST_RUNTIME_DEBUG( rt ) { PRINT_TABS(rt) printf( "InterpretExpression_L1 return[%d] last_token[%s][%s] " , nret , _zlang_token_type_str , rt->travel_token ); DebugPrintObject( rt , obj2 ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE }
		}
		if( nret == ZLANG_INFO_END_OF_EXPRESSION )
		{
			SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_EXPECT_OBJECT , "expect object" )
			TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
			return ZLANG_ERROR_EXPECT_OBJECT;
		}
		else if( nret )
		{
			TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
			return nret;
		}
		
		IF_ATOMIC_OBJECT_THEN_LOCK(obj2)
		
		TEST_RUNTIME_DEBUG( rt )
		{
			PRINT_TABS(rt) printf( "PROCESS operator UNARY-TOKEN_TYPE_BIT_REVERSE - (*result_obj) " ); DebugPrintObject( rt , (*result_obj) ); printf( " and obj2 " ); DebugPrintObject( rt , obj2 ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE
		}
		
		obj3 = CloneObjectInTmpStack( rt , NULL , obj2 ) ;
		if( obj3 == NULL )
		{
			IF_ATOMIC_OBJECT_THEN_UNLOCK(obj2)
			TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
			return GET_RUNTIME_ERROR_NO(rt);
		}
		
		nret = UnaryOperator_BIT_REVERSE( rt , obj3 ) ;
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "INVOKE UnaryOperator_BIT_REVERSE return[%d]" , nret )
		if( nret )
		{
			IF_ATOMIC_OBJECT_THEN_UNLOCK(obj2)
			TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
			return nret;
		}
		
		(*result_obj) = obj3 ;
		
		TEST_RUNTIME_DEBUG( rt )
		{
			PRINT_TABS(rt) printf( "PROCESS-RESULT operator UNARY-TOKEN_TYPE_BIT_REVERSE - (*result_obj) " ); DebugPrintObject( rt , (*result_obj) ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE
		}
		
		IF_ATOMIC_OBJECT_THEN_UNLOCK(obj2)
	}
	else if( token_info2->token_type == TOKEN_TYPE_OPERATOR_PLUS_PLUS ) /* ++var */
	{
		NEXTTOKEN( rt )
		
		TEST_RUNTIME_DEBUG( rt )
		{
			TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
			PRINT_TABS_AND_FORMAT( rt , "CALL InterpretExpression_L1 , last_token[%s][%s]" , _zlang_token_type_str , rt->travel_token )
		}
		nret = InterpretExpression_L1( rt , interp_stat_ctx , interp_expr_ctx , & obj2 ) ;
		TEST_RUNTIME_DEBUG( rt )
		{
			TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
			TEST_RUNTIME_DEBUG( rt ) { PRINT_TABS(rt) printf( "InterpretExpression_L1 return[%d] last_token[%s][%s] " , nret , _zlang_token_type_str , rt->travel_token ); DebugPrintObject( rt , obj2 ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE }
		}
		if( nret == ZLANG_INFO_END_OF_EXPRESSION )
		{
			SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_EXPECT_OBJECT , "expect object" )
			TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
			return ZLANG_ERROR_EXPECT_OBJECT;
		}
		else if( nret )
		{
			TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
			return nret;
		}
	
		TEST_RUNTIME_DEBUG( rt )
		{
			PRINT_TABS(rt) printf( "PROCESS operator ++TOKEN_TYPE_OPERATOR_PLUS_PLUS - (*result_obj) " ); DebugPrintObject( rt , (*result_obj) ); printf( " and obj2 " ); DebugPrintObject( rt , obj2 ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE
		}
		
		IF_ATOMIC_OBJECT_THEN_LOCK(obj2)
		
		nret = UnaryOperator_PLUS_PLUS( rt , obj2 ) ;
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "INVOKE UnaryOperator_PLUS_PLUS return[%d]" , nret )
		if( nret )
		{
			IF_ATOMIC_OBJECT_THEN_UNLOCK(obj2)
			TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
			return nret;
		}
		
		(*result_obj) = obj2 ;
		
		TEST_RUNTIME_DEBUG( rt )
		{
			PRINT_TABS(rt) printf( "PROCESS-RESULT operator ++TOKEN_TYPE_OPERATOR_PLUS_PLUS - (*result_obj) " ); DebugPrintObject( rt , (*result_obj) ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE
		}
		
		IF_ATOMIC_OBJECT_THEN_UNLOCK(obj2)
	}
	else if( token_info2->token_type == TOKEN_TYPE_OPERATOR_MINUS_MINUS ) /* --var */
	{
		NEXTTOKEN( rt )
		
		TEST_RUNTIME_DEBUG( rt )
		{
			TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
			PRINT_TABS_AND_FORMAT( rt , "CALL InterpretExpression_L1 , last_token[%s][%s]" , _zlang_token_type_str , rt->travel_token )
		}
		nret = InterpretExpression_L1( rt , interp_stat_ctx , interp_expr_ctx , & obj2 ) ;
		TEST_RUNTIME_DEBUG( rt )
		{
			TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
			TEST_RUNTIME_DEBUG( rt ) { PRINT_TABS(rt) printf( "InterpretExpression_L1 return[%d] last_token[%s][%s] " , nret , _zlang_token_type_str , rt->travel_token ); DebugPrintObject( rt , obj2 ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE }
		}
		if( nret == ZLANG_INFO_END_OF_EXPRESSION )
		{
			SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_EXPECT_OBJECT , "expect object" )
			TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
			return ZLANG_ERROR_EXPECT_OBJECT;
		}
		else if( nret )
		{
			TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
			return nret;
		}
		
		TEST_RUNTIME_DEBUG( rt )
		{
			PRINT_TABS(rt) printf( "PROCESS operator --TOKEN_TYPE_OPERATOR_MINUS_MINUS - (*result_obj) " ); DebugPrintObject( rt , (*result_obj) ); printf( " and obj2 " ); DebugPrintObject( rt , obj2 ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE
		}
		
		IF_ATOMIC_OBJECT_THEN_LOCK(obj2)
		
		nret = UnaryOperator_MINUS_MINUS( rt , obj2 ) ;
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "INVOKE UnaryOperator_MINUS_MINUS return[%d]" , nret )
		if( nret )
		{
			IF_ATOMIC_OBJECT_THEN_UNLOCK(obj2)
			TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
			return nret;
		}
		
		(*result_obj) = obj2 ;
		
		TEST_RUNTIME_DEBUG( rt )
		{
			PRINT_TABS(rt) printf( "PROCESS-RESULT operator --TOKEN_TYPE_OPERATOR_MINUS_MINUS - (*result_obj) " ); DebugPrintObject( rt , (*result_obj) ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE
		}
		
		IF_ATOMIC_OBJECT_THEN_UNLOCK(obj2)
	}
	else
	{
		TEST_RUNTIME_DEBUG( rt )
		{
			TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
			PRINT_TABS_AND_FORMAT( rt , "THROUGH-CALL InterpretExpression_L1 , last_token[%s][%s]" , _zlang_token_type_str , rt->travel_token )
		}
		nret = InterpretExpression_L1( rt , interp_stat_ctx , interp_expr_ctx , result_obj ) ;
		TEST_RUNTIME_DEBUG( rt )
		{
			TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
			TEST_RUNTIME_DEBUG( rt ) { PRINT_TABS(rt) printf( "InterpretExpression_L1 return[%d] last_token[%s][%s] " , nret , _zlang_token_type_str , rt->travel_token ); DebugPrintObject( rt , (*result_obj) ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE }
		}
		if( nret == ZLANG_INFO_END_OF_EXPRESSION )
		{
			TEST_RUNTIME_DEBUG_THEN_PRINT_LEAVE_FUNCTION(rt)
			return nret;
		}
		else if( nret == ZLANG_INFO_CONTINUE )
		{
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "CONTINUE" )
			TEST_RUNTIME_DEBUG_THEN_PRINT_LEAVE_FUNCTION(rt)
			return nret;
		}
		else if( nret == ZLANG_INFO_BREAK )
		{
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "BREAK" )
			TEST_RUNTIME_DEBUG_THEN_PRINT_LEAVE_FUNCTION(rt)
			return nret;
		}
		else if( nret == ZLANG_INFO_RETURN )
		{
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "RETURN" )
			TEST_RUNTIME_DEBUG_THEN_PRINT_LEAVE_FUNCTION(rt)
			return nret;
		}
		else if( nret == ZLANG_INFO_EXIT )
		{
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "EXIT" )
			TEST_RUNTIME_DEBUG_THEN_PRINT_LEAVE_FUNCTION(rt)
			return nret;
		}
		else if( nret )
		{
			TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
			return nret;
		}
		else
		{
			TEST_RUNTIME_DEBUG_THEN_PRINT_LEAVE_FUNCTION(rt)
			return 0;
		}
	}
	
	TEST_RUNTIME_DEBUG_THEN_PRINT_LEAVE_FUNCTION(rt)
	
	return 0;
}

static int InterpretExpression_L1( struct ZlangRuntime *rt , struct ZlangInterpretStatementContext *interp_stat_ctx , struct ZlangInterpretExpressionContext *interp_expr_ctx , struct ZlangObject **result_obj )
{
	struct ZlangTokenDataUnitHeader	*token_info2 = NULL ;
	char				*token2 = NULL ;
	
	int				nret = 0 ;
	
	CHECK_ALART_STACK_DEPTH( rt->zlang_stack_bottom,&rt )

	TEST_RUNTIME_DEBUG_THEN_PRINT_ENTER_FUNCTION(rt)
	
	TEST_RUNTIME_DEBUG( rt )
	{
		TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
		PRINT_TABS_AND_FORMAT( rt , "CALL InterpretExpression_L0 , last_token[%s][%s]" , _zlang_token_type_str , rt->travel_token )
	}
	nret = InterpretExpression_L0( rt , interp_stat_ctx , interp_expr_ctx , result_obj ) ;
	TEST_RUNTIME_DEBUG( rt )
	{
		TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
		TEST_RUNTIME_DEBUG( rt ) { PRINT_TABS(rt) printf( "InterpretExpression_L0 return[%d] last_token[%s][%s] " , nret , _zlang_token_type_str , rt->travel_token ); DebugPrintObject( rt , (*result_obj) ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE }
	}
	if( nret == ZLANG_INFO_END_OF_EXPRESSION )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT_LEAVE_FUNCTION(rt)
		return nret;
	}
	else if( nret == ZLANG_INFO_CONTINUE )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "CONTINUE" )
		TEST_RUNTIME_DEBUG_THEN_PRINT_LEAVE_FUNCTION(rt)
		return nret;
	}
	else if( nret == ZLANG_INFO_BREAK )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "BREAK" )
		TEST_RUNTIME_DEBUG_THEN_PRINT_LEAVE_FUNCTION(rt)
		return nret;
	}
	else if( nret == ZLANG_INFO_RETURN )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "RETURN" )
		TEST_RUNTIME_DEBUG_THEN_PRINT_LEAVE_FUNCTION(rt)
		return nret;
	}
	else if( nret == ZLANG_INFO_EXIT )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "EXIT" )
		TEST_RUNTIME_DEBUG_THEN_PRINT_LEAVE_FUNCTION(rt)
		return nret;
	}
	else if( nret )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
		return nret;
	}
	
	for( ; ; )
	{
		PEEKTOKEN_AND_SAVEINFO( rt , token_info2 , token2 )
		if( token_info2->token_type == TOKEN_TYPE_OPERATOR_PLUS_PLUS ) /* var++ */
		{
			NEXTTOKEN( rt )
			
			TEST_RUNTIME_DEBUG( rt )
			{
				PRINT_TABS(rt) printf( "PROCESS operator TOKEN_TYPE_OPERATOR_PLUS_PLUS++ - (*result_obj) " ); DebugPrintObject( rt , (*result_obj) ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE
			}
			
			IF_ATOMIC_OBJECT_THEN_LOCK(*result_obj)
			
			nret = UnaryOperator_PLUS_PLUS( rt , (*result_obj) ) ;
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "INVOKE UnaryOperator_PLUS_PLUS return[%d]" , nret )
			if( nret )
			{
				IF_ATOMIC_OBJECT_THEN_UNLOCK(*result_obj)
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return nret;
			}
			
			TEST_RUNTIME_DEBUG( rt )
			{
				PRINT_TABS(rt) printf( "PROCESS-RESULT operator TOKEN_TYPE_OPERATOR_PLUS_PLUS++ - (*result_obj) " ); DebugPrintObject( rt , (*result_obj) ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE
			}
			
			IF_ATOMIC_OBJECT_THEN_UNLOCK(*result_obj)
		}
		else if( token_info2->token_type == TOKEN_TYPE_OPERATOR_MINUS_MINUS ) /* var-- */
		{
			NEXTTOKEN( rt )
			
			TEST_RUNTIME_DEBUG( rt )
			{
				PRINT_TABS(rt) printf( "PROCESS operator TOKEN_TYPE_OPERATOR_MINUS_MINUS-- - (*result_obj) " ); DebugPrintObject( rt , (*result_obj) ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE
			}
			
			IF_ATOMIC_OBJECT_THEN_LOCK(*result_obj)
			
			nret = UnaryOperator_MINUS_MINUS( rt , (*result_obj) ) ;
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "INVOKE UnaryOperator_MINUS_MINUS return[%d]" , nret )
			if( nret )
			{
				IF_ATOMIC_OBJECT_THEN_UNLOCK(*result_obj)
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return nret;
			}
			
			TEST_RUNTIME_DEBUG( rt )
			{
				PRINT_TABS(rt) printf( "PROCESS-RESULT operator TOKEN_TYPE_OPERATOR_MINUS_MINUS-- - (*result_obj) " ); DebugPrintObject( rt , (*result_obj) ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE
			}
			
			IF_ATOMIC_OBJECT_THEN_UNLOCK(*result_obj)
		}
		else if( token_info2->token_type == TOKEN_TYPE_MEMBER_OF ) /* . */
		{
			struct ZlangObject		*member_obj = NULL ;
			
			NEXTTOKEN( rt )
			
			TEST_RUNTIME_DEBUG( rt )
			{
				PRINT_TABS(rt) printf( "PROCESS operator TOKEN_TYPE_MEMBER_OF - (*result_obj) " ); DebugPrintObject( rt , (*result_obj) ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE
			}
			
			TEST_RUNTIME_DEBUG( rt )
			{
				TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
				PRINT_TABS_AND_FORMAT( rt , "CALL InterpretExpression_MemberOf , last_token[%s][%s]" , _zlang_token_type_str , rt->travel_token )
			}
			nret = InterpretExpression_MemberOf( rt , token_info2->token_type , interp_stat_ctx , (*result_obj) , & member_obj ) ;
			TEST_RUNTIME_DEBUG( rt )
			{
				TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
				PRINT_TABS(rt) printf( "InterpretExpression_MemberOf return[%d] last_token[%s][%s] " , nret , _zlang_token_type_str , rt->travel_token ); DebugPrintObject( rt , member_obj ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE
			}
			if( nret )
			{
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return nret;
			}
			
			(*result_obj) = member_obj ;
			
			TEST_RUNTIME_DEBUG( rt )
			{
				PRINT_TABS(rt) printf( "PROCESS-RESULT operator TOKEN_TYPE_MEMBER_OF - (*result_obj) " ); DebugPrintObject( rt , (*result_obj) ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE
			}
		}
		else if( token_info2->token_type == TOKEN_TYPE_MEMBER_OF_NOTNULL ) /* ?. */
		{
			struct ZlangObject		*member_obj = NULL ;
			
			NEXTTOKEN( rt )
			
			TEST_RUNTIME_DEBUG( rt )
			{
				PRINT_TABS(rt) printf( "PROCESS operator TOKEN_TYPE_MEMBER_OF_NOTNULL - (*result_obj) " ); DebugPrintObject( rt , (*result_obj) ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE
			}
			
			TEST_RUNTIME_DEBUG( rt )
			{
				TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
				PRINT_TABS_AND_FORMAT( rt , "CALL InterpretExpression_MemberOf Notnull , last_token[%s][%s]" , _zlang_token_type_str , rt->travel_token )
			}
			nret = InterpretExpression_MemberOf( rt , token_info2->token_type , interp_stat_ctx , (*result_obj) , & member_obj ) ;
			TEST_RUNTIME_DEBUG( rt )
			{
				TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
				PRINT_TABS(rt) printf( "InterpretExpression_MemberOf Notnull return[%d] last_token[%s][%s] " , nret , _zlang_token_type_str , rt->travel_token ); DebugPrintObject( rt , member_obj ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE
			}
			if( nret )
			{
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return nret;
			}
			
			(*result_obj) = member_obj ;
			
			TEST_RUNTIME_DEBUG( rt )
			{
				PRINT_TABS(rt) printf( "PROCESS-RESULT operator TOKEN_TYPE_MEMBER_OF_NOTNULL - (*result_obj) " ); DebugPrintObject( rt , (*result_obj) ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE
			}
		}
		else if( token_info2->token_type == TOKEN_TYPE_IDENTIFICATION ) /* IDENTIFICATION */
		{
			TEST_RUNTIME_DEBUG( rt )
			{
				PRINT_TABS(rt) printf( "PROCESS TOKEN_TYPE_IDENTIFICATION - (*result_obj) " ); DebugPrintObject( rt , (*result_obj) ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE
			}
			
			if( interp_expr_ctx->get_expr_left_or_right == GET_LEFT_VALUE )
			{
_GOTO_DECLAREOBJECTS:
				TEST_RUNTIME_DEBUG( rt )
				{
					TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
					PRINT_TABS_AND_FORMAT( rt , "CALL InterpretExpression_DeclareObjects , last_token[%s][%s]" , _zlang_token_type_str , rt->travel_token )
				}
				nret = InterpretExpression_DeclareObjects( rt , interp_stat_ctx , interp_expr_ctx , NULL , (*result_obj) , result_obj ) ;
				interp_stat_ctx->create_new_object_use_existed_name = 0 ;
				TEST_RUNTIME_DEBUG( rt )
				{
					TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
					TEST_RUNTIME_DEBUG( rt ) { PRINT_TABS(rt) printf( "InterpretExpression_DeclareObjects return[%d] last_token[%s][%s] " , nret , _zlang_token_type_str , rt->travel_token ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE }
				}
				if( nret == ZLANG_INFO_END_OF_EXPRESSION )
				{
					TEST_RUNTIME_DEBUG( rt )
					{
						DebugStack( rt );
					}
					TEST_RUNTIME_DEBUG_THEN_PRINT_LEAVE_FUNCTION(rt)
					return nret;
				}
				else if( nret )
				{
					TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
					return nret;
				}
				else
				{
					SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_INTERNAL , "internal error" )
					TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
					return ZLANG_ERROR_INTERNAL;
				}
			}
			else
			{
				SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_SYNTAX , "unexpect '%s'" , token2 )
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return ZLANG_ERROR_SYNTAX;
			}
		}
		else if( token_info2->token_type == TOKEN_TYPE_NEW ) /* new */
		{
			TEST_RUNTIME_DEBUG( rt )
			{
				PRINT_TABS(rt) printf( "PROCESS TOKEN_TYPE_NEW - (*result_obj) " ); DebugPrintObject( rt , (*result_obj) ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE
			}
			
			if( interp_expr_ctx->get_expr_left_or_right != GET_LEFT_VALUE )
			{
				SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_SYNTAX , "unexpect '%s'" , token2 )
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return ZLANG_ERROR_SYNTAX;
			}
			
			NEXTTOKEN( rt )
			
			if( result_obj == NULL )
			{
				SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_SYNTAX , "unexpect left object before 'new'" )
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return ZLANG_ERROR_SYNTAX;
			}
			
			PEEKTOKEN_AND_SAVEINFO( rt , token_info2 , token2 )
			if( token_info2->token_type != TOKEN_TYPE_IDENTIFICATION ) /* IDENTIFICATION */
			{
				SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_SYNTAX , "unexpect right identification after 'new'" )
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return ZLANG_ERROR_SYNTAX;
			}
			
			interp_stat_ctx->create_new_object_use_existed_name = 1 ;
			goto _GOTO_DECLAREOBJECTS;
		}
		else
		{
			interp_stat_ctx->create_new_object_use_existed_name = 0 ;
			break;
		}
	}
	
	TEST_RUNTIME_DEBUG_THEN_PRINT_LEAVE_FUNCTION(rt)
	
	return 0;
}

static int InterpretExpression_L0( struct ZlangRuntime *rt , struct ZlangInterpretStatementContext *interp_stat_ctx , struct ZlangInterpretExpressionContext *interp_expr_ctx , struct ZlangObject **result_obj )
{
	struct ZlangObject		*obj = NULL ;
	struct ZlangTokenDataUnitHeader	*token_info1 = NULL ;
	char				*token1 = NULL ;
	struct ZlangTokenDataUnitHeader	*token_info2 = NULL ;
	char				*token2 = NULL ;
	struct ZlangDefer		*defer = NULL ;
	char				numeric_buffer[ 100 ] ;
	int32_t				numeric_buffer_len ;
	
	int				nret = 0 ;
	
	CHECK_ALART_STACK_DEPTH( rt->zlang_stack_bottom,&rt )

	TEST_RUNTIME_DEBUG_THEN_PRINT_ENTER_FUNCTION(rt)
	
_GOTO_REDO_TRAVEL_TOKEN :
	TRAVELTOKEN_AND_SAVEINFO( rt , token_info1 , token1 )
	if( token_info1->token_type == TOKEN_TYPE_DOUBLE_QUOTES_STRING || token_info1->token_type == TOKEN_TYPE_TRIPLE_QUOTES_STRING ) /* STRING */
	{
		char		**buf = NULL ;
		int32_t		*buf_len = NULL ;
		
		obj = CloneStringObjectInTmpStack( rt , NULL ) ;
		if( obj == NULL )
		{
			TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
			return GET_RUNTIME_ERROR_NO(rt);
		}
		
		nret = FromCharPtr( rt , obj , token1 , token_info1->token_len ) ;
		if( nret )
		{
			TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
			return nret;
		}
		
		if( token_info1->token_type == TOKEN_TYPE_DOUBLE_QUOTES_STRING )
		{
			CallRuntimeFunction_string_GetDirectPropertiesPtr( rt , obj , & buf , NULL , & buf_len );
			(*buf_len) = EscapeString( *buf ) ;
		}
		
		TEST_RUNTIME_DEBUG( rt ) { PRINT_TABS(rt) printf( "literal convert to " ); DebugPrintObject( rt , obj ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE }
		
		(*result_obj) = obj ;
	}
	else if( token_info1->token_type == TOKEN_TYPE_TRIPLE_BACK_QUOTES_STRING ) /* ```STRING``` */
	{
		char		*str = NULL ;
		int32_t		str_len ;
		
		obj = QueryObjectByObjectName( rt , token1 ) ;
		if( obj == NULL )
		{
			SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_OBJECT_NOT_IMPORTED_OR_DECLARED , "object '%s' not imported or declared" , token1 )
			TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
			return ZLANG_ERROR_OBJECT_NOT_IMPORTED_OR_DECLARED;
		}
		
		if( ! IsTypeOf( rt , obj , GetStringObjectInRuntimeObjectsHeap(rt) ) )
		{
			SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_OBJECT_TYPE_ISNOT_STRING , "object '%s' isn't string type" , token1 )
			TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
			return ZLANG_ERROR_OBJECT_TYPE_ISNOT_STRING;
		}
		
		nret = CallRuntimeFunction_string_GetStringValue( rt , obj , & str , & str_len ) ;
		if( nret )
		{
			TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
			return nret;
		}
		str[str_len] = '\0' ;
		
		obj = QueryObjectByObjectName( rt , str ) ;
		if( obj == NULL )
		{
			SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_OBJECT_NOT_IMPORTED_OR_DECLARED , "object '%s' not imported or declared" , str )
			TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
			return ZLANG_ERROR_OBJECT_NOT_IMPORTED_OR_DECLARED;
		}
		
		TEST_RUNTIME_DEBUG( rt ) { PRINT_TABS(rt) printf( "query object[%s] from object[%s]'s value[%s] , " , str , token1 , str ); DebugPrintObject( rt , obj ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE }
		
		(*result_obj) = obj ;
	}
	else if( token_info1->token_type == TOKEN_TYPE_NUMBER ) /* NUMBER */
	{
		PEEKTOKEN_AND_SAVEINFO( rt , token_info2 , token2 )
		if( token_info2->token_type == TOKEN_TYPE_SHORT_NUMBER )
		{
			NEXTTOKEN( rt )
			
			obj = CloneShortObjectInTmpStack( rt , NULL ) ;
			if( obj == NULL )
			{
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return GET_RUNTIME_ERROR_NO(rt);
			}
		}
		else if( token_info2->token_type == TOKEN_TYPE_USHORT_NUMBER )
		{
			NEXTTOKEN( rt )
			
			obj = CloneUShortObjectInTmpStack( rt , NULL ) ;
			if( obj == NULL )
			{
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return GET_RUNTIME_ERROR_NO(rt);
			}
		}
		else if( token_info2->token_type == TOKEN_TYPE_INT_NUMBER )
		{
			NEXTTOKEN( rt )
			
			obj = CloneIntObjectInTmpStack( rt , NULL ) ;
			if( obj == NULL )
			{
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return GET_RUNTIME_ERROR_NO(rt);
			}
		}
		else if( token_info2->token_type == TOKEN_TYPE_UINT_NUMBER )
		{
			NEXTTOKEN( rt )
			
			obj = CloneUIntObjectInTmpStack( rt , NULL ) ;
			if( obj == NULL )
			{
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return GET_RUNTIME_ERROR_NO(rt);
			}
		}
		else if( token_info2->token_type == TOKEN_TYPE_LONG_NUMBER )
		{
			NEXTTOKEN( rt )
			
			obj = CloneLongObjectInTmpStack( rt , NULL ) ;
			if( obj == NULL )
			{
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return GET_RUNTIME_ERROR_NO(rt);
			}
		}
		else if( token_info2->token_type == TOKEN_TYPE_ULONG_NUMBER )
		{
			NEXTTOKEN( rt )
			
			obj = CloneULongObjectInTmpStack( rt , NULL ) ;
			if( obj == NULL )
			{
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return GET_RUNTIME_ERROR_NO(rt);
			}
		}
		else
		{
			obj = CloneIntObjectInTmpStack( rt , NULL ) ;
			if( obj == NULL )
			{
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return GET_RUNTIME_ERROR_NO(rt);
			}
		}
		
		if( token_info1->token_len > sizeof(numeric_buffer)-1 )
		{
			SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_NUMERIC_LITERAL_TOO_LONG , "numeric literal '%.*s' too long" , token_info1->token_len,token1 )
			TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
			return ZLANG_ERROR_OBJECT_NOT_IMPORTED_OR_DECLARED;
		}
		
		numeric_buffer_len = TrimNumericLiteralUnderlineUnsafely( token1 , token_info1->token_len , numeric_buffer ) ;
		nret = FromCharPtr( rt , obj , numeric_buffer , numeric_buffer_len ) ;
		if( nret )
		{
			TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
			return nret;
		}
		
		TEST_RUNTIME_DEBUG( rt ) { PRINT_TABS(rt) printf( "literal convert to " ); DebugPrintObject( rt , obj ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE }
		
		(*result_obj) = obj ;
	}
	else if( token_info1->token_type == TOKEN_TYPE_FLOAT_NUMBER ) /* FLOAT-NUMBER */
	{
		PEEKTOKEN_AND_SAVEINFO( rt , token_info2 , token2 )
		if( token_info2->token_type == TOKEN_TYPE_SINGLE_FLOAT_NUMBER )
		{
			NEXTTOKEN( rt )
			
			obj = CloneFloatObjectInTmpStack( rt , NULL ) ;
			if( obj == NULL )
			{
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return GET_RUNTIME_ERROR_NO(rt);
			}
		}
		else if( token_info2->token_type == TOKEN_TYPE_DOUBLE_FLOAT_NUMBER )
		{
			NEXTTOKEN( rt )
			
			obj = CloneDoubleObjectInTmpStack( rt , NULL ) ;
			if( obj == NULL )
			{
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return GET_RUNTIME_ERROR_NO(rt);
			}
		}
		else
		{
			obj = CloneDoubleObjectInTmpStack( rt , NULL ) ;
			if( obj == NULL )
			{
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return GET_RUNTIME_ERROR_NO(rt);
			}
		}
		
		numeric_buffer_len = TrimNumericLiteralUnderlineUnsafely( token1 , token_info1->token_len , numeric_buffer ) ;
		nret = FromCharPtr( rt , obj , numeric_buffer , numeric_buffer_len ) ;
		if( nret )
		{
			TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
			return nret;
		}
		
		TEST_RUNTIME_DEBUG( rt ) { PRINT_TABS(rt) printf( "literal convert to " ); DebugPrintObject( rt , obj ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE }
		
		(*result_obj) = obj ;
	}
	else if( token_info1->token_type == TOKEN_TYPE_TRUE ) /* true */
	{
		obj = CloneBoolObjectInTmpStack( rt , NULL ) ;
		if( obj == NULL )
		{
			TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
			return GET_RUNTIME_ERROR_NO(rt);
		}
		
		CallRuntimeFunction_bool_SetBoolValue( rt , obj , TRUE );
		
		TEST_RUNTIME_DEBUG( rt ) { PRINT_TABS(rt) printf( "literal convert to " ); DebugPrintObject( rt , obj ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE }
		
		(*result_obj) = obj ;
	}
	else if( token_info1->token_type == TOKEN_TYPE_FALSE ) /* false */
	{
		obj = CloneBoolObjectInTmpStack( rt , NULL ) ;
		if( obj == NULL )
		{
			TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
			return GET_RUNTIME_ERROR_NO(rt);
		}
		
		CallRuntimeFunction_bool_SetBoolValue( rt , obj , FALSE );
		
		TEST_RUNTIME_DEBUG( rt ) { PRINT_TABS(rt) printf( "literal convert to " ); DebugPrintObject( rt , obj ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE }
		
		(*result_obj) = obj ;
	}
	else if( token_info1->token_type == TOKEN_TYPE_NULL ) /* null */
	{
		obj = CloneObjectInTmpStack( rt , NULL , NULL ) ;
		if( obj == NULL )
		{
			TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
			return GET_RUNTIME_ERROR_NO(rt);
		}
		
		TEST_RUNTIME_DEBUG( rt ) { PRINT_TABS(rt) printf( "literal convert to " ); DebugPrintObject( rt , obj ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE }
		
		(*result_obj) = obj ;
	}
	else if( token_info1->token_type == TOKEN_TYPE_IDENTIFICATION ) /* IDENTIFICATION */
	{
		QueryCharsetAliasAndChangeTokenInfo( rt , token_info1 , & token1 );
		
		PEEKTOKEN_AND_SAVEINFO( rt , token_info2 , token2 )
		if( token_info2->token_type == TOKEN_TYPE_BEGIN_OF_SUB_EXPRESSION ) /* ( */
		{
			NEXTTOKEN( rt )
			
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "CALL GetExpression_InvokeFunction ..." )
			nret = InterpretExpression_InvokeFunction( rt , TOKEN_TYPE_MEMBER_OF , interp_stat_ctx , NULL , token1 , result_obj ) ;
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "GetExpression_InvokeFunction return[%d]" , nret )
			if( nret )
			{
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return nret;
			}
		}
		else
		{
			obj = QueryObjectByObjectName( rt , token1 ) ;
			if( obj == NULL )
			{
				if
				(
					interp_expr_ctx->get_expr_left_or_right == GET_EXPRESSION_LEFT
					&&
					(
						token_info2->token_type == TOKEN_TYPE_EVAL
						||
						token_info2->token_type == TOKEN_TYPE_REFER
					)
				)
				{
					obj = CloneObjectInLocalStack( rt , token1 , NULL ) ;
					if( obj == NULL )
					{
						TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
						return GET_RUNTIME_ERROR_NO(rt);
					}
					else
					{
						TEST_RUNTIME_DEBUG( rt ) { PRINT_TABS(rt) printf( "CloneObjectInLocalStack ok , " ); DebugPrintObject( rt , obj ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE }
					}
				}
				else
				{
					SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_OBJECT_NOT_IMPORTED_OR_DECLARED , "object '%s' not imported or declared" , token1 )
					TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
					return ZLANG_ERROR_OBJECT_NOT_IMPORTED_OR_DECLARED;
				}
			}
			else
			{
				TEST_RUNTIME_DEBUG( rt ) { PRINT_TABS(rt) printf( "QueryObjectByObjectName[%s] ok , " , token1 ); DebugPrintObject( rt , obj ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE }
			}
			
			(*result_obj) = obj ;
		}
	}
	else if( token_info1->token_type == TOKEN_TYPE_THIS ) /* this */
	{
		if( rt->in_obj == NULL )
		{
			SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_THIS_NO_ASSOCIATED_OBJECT , "'this' no associated object" )
			TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
			return ZLANG_ERROR_OBJECT_NOT_IMPORTED_OR_DECLARED;
		}
		
		TEST_RUNTIME_DEBUG( rt ) { PRINT_TABS(rt) printf( "'this' associated object " ); DebugPrintObject( rt , rt->in_obj ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE }
		(*result_obj) = rt->in_obj ;
	}
	else if( token_info1->token_type == TOKEN_TYPE_BEGIN_OF_SUB_EXPRESSION ) /* ( */
	{
		struct ZlangInterpretStatementContext	interp_stat_ctx ;
		
		TEST_RUNTIME_DEBUG( rt )
		{
			TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
			PRINT_TABS_AND_FORMAT( rt , "CALL InterpretExpression , last_token[%s][%s]" , _zlang_token_type_str , rt->travel_token )
		}
		memset( & interp_stat_ctx , 0x00 , sizeof(struct ZlangInterpretStatementContext) );
		interp_stat_ctx.token_of_expression_end1 = TOKEN_TYPE_END_OF_SUB_EXPRESSION ;
		nret = InterpretExpression( rt , & interp_stat_ctx , result_obj ) ;
		TEST_RUNTIME_DEBUG( rt )
		{
			TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
			TEST_RUNTIME_DEBUG( rt ) { PRINT_TABS(rt) printf( "InterpretExpression return[%d] last_token[%s][%s] " , nret , _zlang_token_type_str , rt->travel_token ); DebugPrintObject(rt,*result_obj); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE }
		}
		if( nret != ZLANG_INFO_END_OF_EXPRESSION )
		{
			TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
			return nret;
		}
	}
	else if( token_info1->token_type == TOKEN_TYPE_BEGIN_OF_STATEMENT_SEGMENT ) /* { */
	{
		struct ZlangObject	*bak_in_obj = NULL ;
		struct ZlangFunction	*bak_in_func = NULL ;
		
		if( interp_expr_ctx->get_expr_left_or_right != GET_EXPRESSION_LEFT )
		{
			SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_SYNTAX , "unexpect '%s'" , token1 )
			TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
			return ZLANG_ERROR_SYNTAX;
		}
		
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "CALL InterpretStatementSegment" )
		IncreaseStackFrame( rt , NULL );
		bak_in_obj = rt->in_obj ;
		bak_in_func = rt->in_func ;
		rt->in_obj = NULL ;
		rt->in_func = NULL ;
		nret = InterpretStatementSegment( rt , interp_stat_ctx ) ;
		rt->in_obj = bak_in_obj ;
		rt->in_func = bak_in_func ;
		DecreaseStackFrame( rt );
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "InterpretStatementSegment return[%d]" , nret )
		if( nret != ZLANG_INFO_END_OF_STATEMENT_SEGMENT )
		{
			TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
			return nret;
		}
		
		TEST_RUNTIME_DEBUG_THEN_PRINT_LEAVE_FUNCTION(rt)
		return 0;
	}
	else if( token_info1->token_type == TOKEN_TYPE_IF ) /* if */
	{
		if( interp_expr_ctx->get_expr_left_or_right != GET_EXPRESSION_LEFT )
		{
			SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_SYNTAX , "unexpect '%s'" , token1 )
			TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
			return ZLANG_ERROR_SYNTAX;
		}
		
		TEST_RUNTIME_DEBUG( rt )
		{
			TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
			PRINT_TABS_AND_FORMAT( rt , "CALL Keyword_if , last_token[%s][%s]" , _zlang_token_type_str , rt->travel_token )
		}
		nret = Keyword_if( rt ) ;
		TEST_RUNTIME_DEBUG( rt )
		{
			TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
			PRINT_TABS_AND_FORMAT( rt , "Keyword_if return[%d] last_token[%s][%s]" , nret , _zlang_token_type_str , rt->travel_token )
			(*result_obj) = NULL ;
		}
		if( nret == ZLANG_INFO_END_OF_STATEMENT )
		{
			TEST_RUNTIME_DEBUG_THEN_PRINT_LEAVE_FUNCTION(rt)
			return nret;
		}
		else if( nret )
		{
			TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
			return nret;
		}
		else
		{
			SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_INTERNAL , "internal error" )
			TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
			return ZLANG_ERROR_INTERNAL;
		}
	}
	else if( token_info1->token_type == TOKEN_TYPE_WHILE ) /* while */
	{
		if( interp_expr_ctx->get_expr_left_or_right != GET_EXPRESSION_LEFT )
		{
			SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_SYNTAX , "unexpect '%s'" , token1 )
			TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
			return ZLANG_ERROR_SYNTAX;
		}
		
		TEST_RUNTIME_DEBUG( rt )
		{
			TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
			PRINT_TABS_AND_FORMAT( rt , "CALL Keyword_while , last_token[%s][%s]" , _zlang_token_type_str , rt->travel_token )
		}
		nret = Keyword_while( rt ) ;
		TEST_RUNTIME_DEBUG( rt )
		{
			TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
			PRINT_TABS_AND_FORMAT( rt , "Keyword_while return[%d] last_token[%s][%s]" , nret , _zlang_token_type_str , rt->travel_token )
			(*result_obj) = NULL ;
		}
		if( nret == ZLANG_INFO_END_OF_STATEMENT )
		{
			TEST_RUNTIME_DEBUG_THEN_PRINT_LEAVE_FUNCTION(rt)
			return nret;
		}
		else if( nret )
		{
			TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
			return nret;
		}
		else
		{
			SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_INTERNAL , "internal error" )
			TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
			return ZLANG_ERROR_INTERNAL;
		}
	}
	else if( token_info1->token_type == TOKEN_TYPE_DO ) /* do while */
	{
		if( interp_expr_ctx->get_expr_left_or_right != GET_EXPRESSION_LEFT )
		{
			SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_SYNTAX , "unexpect '%s'" , token1 )
			TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
			return ZLANG_ERROR_SYNTAX;
		}
		
		TEST_RUNTIME_DEBUG( rt )
		{
			TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
			PRINT_TABS_AND_FORMAT( rt , "CALL Keyword_do_while , last_token[%s][%s]" , _zlang_token_type_str , rt->travel_token )
		}
		nret = Keyword_do_while( rt ) ;
		TEST_RUNTIME_DEBUG( rt )
		{
			TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
			PRINT_TABS_AND_FORMAT( rt , "Keyword_do_while return[%d] last_token[%s][%s]" , nret , _zlang_token_type_str , rt->travel_token )
			(*result_obj) = NULL ;
		}
		if( nret == ZLANG_INFO_END_OF_STATEMENT )
		{
			TEST_RUNTIME_DEBUG_THEN_PRINT_LEAVE_FUNCTION(rt)
			return nret;
		}
		else if( nret )
		{
			TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
			return nret;
		}
		else
		{
			SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_INTERNAL , "internal error" )
			TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
			return ZLANG_ERROR_INTERNAL;
		}
	}
	else if( token_info1->token_type == TOKEN_TYPE_FOR ) /* for */
	{
		if( interp_expr_ctx->get_expr_left_or_right != GET_EXPRESSION_LEFT )
		{
			SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_SYNTAX , "unexpect '%s'" , token1 )
			TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
			return ZLANG_ERROR_SYNTAX;
		}
		
		TEST_RUNTIME_DEBUG( rt )
		{
			TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
			PRINT_TABS_AND_FORMAT( rt , "CALL Keyword_for , last_token[%s][%s]" , _zlang_token_type_str , rt->travel_token )
		}
		nret = Keyword_for( rt ) ;
		TEST_RUNTIME_DEBUG( rt )
		{
			TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
			PRINT_TABS_AND_FORMAT( rt , "Keyword_for return[%d] last_token[%s][%s]" , nret , _zlang_token_type_str , rt->travel_token )
			(*result_obj) = NULL ;
		}
		if( nret == ZLANG_INFO_END_OF_STATEMENT )
		{
			TEST_RUNTIME_DEBUG_THEN_PRINT_LEAVE_FUNCTION(rt)
			return nret;
		}
		else if( nret )
		{
			TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
			return nret;
		}
		else
		{
			SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_INTERNAL , "internal error" )
			TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
			return ZLANG_ERROR_INTERNAL;
		}
	}
	else if( token_info1->token_type == TOKEN_TYPE_FOREACH ) /* foreach */
	{
		if( interp_expr_ctx->get_expr_left_or_right != GET_EXPRESSION_LEFT )
		{
			SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_SYNTAX , "unexpect '%s'" , token1 )
			TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
			return ZLANG_ERROR_SYNTAX;
		}
		
		TEST_RUNTIME_DEBUG( rt )
		{
			TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
			PRINT_TABS_AND_FORMAT( rt , "CALL Keyword_foreach , last_token[%s][%s]" , _zlang_token_type_str , rt->travel_token )
		}
		nret = Keyword_foreach( rt ) ;
		TEST_RUNTIME_DEBUG( rt )
		{
			TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
			PRINT_TABS_AND_FORMAT( rt , "Keyword_foreach return[%d] last_token[%s][%s]" , nret , _zlang_token_type_str , rt->travel_token )
			(*result_obj) = NULL ;
		}
		if( nret == ZLANG_INFO_END_OF_STATEMENT )
		{
			TEST_RUNTIME_DEBUG_THEN_PRINT_LEAVE_FUNCTION(rt)
			return nret;
		}
		else if( nret )
		{
			TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
			return nret;
		}
		else
		{
			SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_INTERNAL , "internal error" )
			TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
			return ZLANG_ERROR_INTERNAL;
		}
	}
	else if( token_info1->token_type == TOKEN_TYPE_SWITCH ) /* switch */
	{
		if( interp_expr_ctx->get_expr_left_or_right != GET_EXPRESSION_LEFT )
		{
			SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_SYNTAX , "unexpect '%s'" , token1 )
			TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
			return ZLANG_ERROR_SYNTAX;
		}
		
		TEST_RUNTIME_DEBUG( rt )
		{
			TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
			PRINT_TABS_AND_FORMAT( rt , "CALL Keyword_switch , last_token[%s][%s]" , _zlang_token_type_str , rt->travel_token )
		}
		nret = Keyword_switch( rt ) ;
		TEST_RUNTIME_DEBUG( rt )
		{
			TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
			PRINT_TABS_AND_FORMAT( rt , "Keyword_switch return[%d] last_token[%s][%s]" , nret , _zlang_token_type_str , rt->travel_token )
			(*result_obj) = NULL ;
		}
		if( nret == ZLANG_INFO_END_OF_STATEMENT )
		{
			TEST_RUNTIME_DEBUG_THEN_PRINT_LEAVE_FUNCTION(rt)
			return nret;
		}
		else if( nret )
		{
			TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
			return nret;
		}
		else
		{
			SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_INTERNAL , "internal error" )
			TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
			return ZLANG_ERROR_INTERNAL;
		}
	}
	else if( token_info1->token_type == TOKEN_TYPE_CASE ) /* case */
	{
		if( interp_expr_ctx->get_expr_left_or_right != GET_EXPRESSION_LEFT )
		{
			SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_SYNTAX , "unexpect '%s'" , token1 )
			TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
			return ZLANG_ERROR_SYNTAX;
		}
		
		TEST_RUNTIME_DEBUG( rt )
		{
			TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
			PRINT_TABS_AND_FORMAT( rt , "CALL SkipStatement_case , last_token[%s][%s]" , _zlang_token_type_str , rt->travel_token )
		}
		nret = SkipStatement_case( rt ) ;
		TEST_RUNTIME_DEBUG( rt )
		{
			TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
			PRINT_TABS_AND_FORMAT( rt , "SkipStatement_case return[%d] last_token[%s][%s]" , nret , _zlang_token_type_str , rt->travel_token )
			(*result_obj) = NULL ;
		}
		if( nret == ZLANG_INFO_END_OF_STATEMENT )
		{
			TEST_RUNTIME_DEBUG_THEN_PRINT_LEAVE_FUNCTION(rt)
			return nret;
		}
		else if( nret )
		{
			TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
			return nret;
		}
		else
		{
			SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_INTERNAL , "internal error" )
			TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
			return ZLANG_ERROR_INTERNAL;
		}
	}
	else if( token_info1->token_type == TOKEN_TYPE_DEFAULT ) /* default */
	{
		if( interp_expr_ctx->get_expr_left_or_right != GET_EXPRESSION_LEFT )
		{
			SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_SYNTAX , "unexpect '%s'" , token1 )
			TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
			return ZLANG_ERROR_SYNTAX;
		}
		
		TEST_RUNTIME_DEBUG( rt )
		{
			TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
			PRINT_TABS_AND_FORMAT( rt , "CALL SkipStatement_default , last_token[%s][%s]" , _zlang_token_type_str , rt->travel_token )
		}
		nret = SkipStatement_default( rt ) ;
		TEST_RUNTIME_DEBUG( rt )
		{
			TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
			PRINT_TABS_AND_FORMAT( rt , "SkipStatement_default return[%d] last_token[%s][%s]" , nret , _zlang_token_type_str , rt->travel_token )
			(*result_obj) = NULL ;
		}
		if( nret == ZLANG_INFO_END_OF_STATEMENT )
		{
			TEST_RUNTIME_DEBUG_THEN_PRINT_LEAVE_FUNCTION(rt)
			return nret;
		}
		else if( nret )
		{
			TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
			return nret;
		}
		else
		{
			SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_INTERNAL , "internal error" )
			TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
			return ZLANG_ERROR_INTERNAL;
		}
	}
	else if( token_info1->token_type == TOKEN_TYPE_TRY ) /* try */
	{
		if( interp_expr_ctx->get_expr_left_or_right != GET_EXPRESSION_LEFT )
		{
			SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_SYNTAX , "unexpect '%s'" , token1 )
			TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
			return ZLANG_ERROR_SYNTAX;
		}
		
		TEST_RUNTIME_DEBUG( rt )
		{
			TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
			PRINT_TABS_AND_FORMAT( rt , "CALL Keyword_try , last_token[%s][%s]" , _zlang_token_type_str , rt->travel_token )
		}
		nret = Keyword_try( rt ) ;
		TEST_RUNTIME_DEBUG( rt )
		{
			TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
			PRINT_TABS_AND_FORMAT( rt , "Keyword_try return[%d] last_token[%s][%s]" , nret , _zlang_token_type_str , rt->travel_token )
			(*result_obj) = NULL ;
		}
		if( nret == ZLANG_INFO_END_OF_STATEMENT )
		{
			TEST_RUNTIME_DEBUG_THEN_PRINT_LEAVE_FUNCTION(rt)
			return nret;
		}
		else if( nret )
		{
			TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
			return nret;
		}
		else
		{
			SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_INTERNAL , "internal error" )
			TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
			return ZLANG_ERROR_INTERNAL;
		}
	}
	else if( token_info1->token_type == TOKEN_TYPE_CATCH ) /* catch */
	{
		if( interp_expr_ctx->get_expr_left_or_right != GET_EXPRESSION_LEFT )
		{
			SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_SYNTAX , "unexpect '%s'" , token1 )
			TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
			return ZLANG_ERROR_SYNTAX;
		}
		
		TEST_RUNTIME_DEBUG( rt )
		{
			TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
			PRINT_TABS_AND_FORMAT( rt , "CALL SkipStatement_catch , last_token[%s][%s]" , _zlang_token_type_str , rt->travel_token )
		}
		nret = SkipStatement_catch( rt ) ;
		TEST_RUNTIME_DEBUG( rt )
		{
			TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
			PRINT_TABS_AND_FORMAT( rt , "SkipStatement_catch return[%d] last_token[%s][%s]" , nret , _zlang_token_type_str , rt->travel_token )
			(*result_obj) = NULL ;
		}
		if( nret == ZLANG_INFO_END_OF_STATEMENT )
		{
			TEST_RUNTIME_DEBUG_THEN_PRINT_LEAVE_FUNCTION(rt)
			return nret;
		}
		else if( nret )
		{
			TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
			return nret;
		}
		else
		{
			SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_INTERNAL , "internal error" )
			TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
			return ZLANG_ERROR_INTERNAL;
		}
	}
	else if( token_info1->token_type == TOKEN_TYPE_FINALLY ) /* finally */
	{
		if( interp_expr_ctx->get_expr_left_or_right != GET_EXPRESSION_LEFT )
		{
			SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_SYNTAX , "unexpect '%s'" , token1 )
			TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
			return ZLANG_ERROR_SYNTAX;
		}
		
		TEST_RUNTIME_DEBUG( rt )
		{
			TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
			PRINT_TABS_AND_FORMAT( rt , "CALL SkipStatement_finally , last_token[%s][%s]" , _zlang_token_type_str , rt->travel_token )
		}
		nret = SkipStatement_finally( rt ) ;
		TEST_RUNTIME_DEBUG( rt )
		{
			TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
			PRINT_TABS_AND_FORMAT( rt , "SkipStatement_finally return[%d] last_token[%s][%s]" , nret , _zlang_token_type_str , rt->travel_token )
			(*result_obj) = NULL ;
		}
		if( nret == ZLANG_INFO_END_OF_STATEMENT )
		{
			TEST_RUNTIME_DEBUG_THEN_PRINT_LEAVE_FUNCTION(rt)
			return nret;
		}
		else if( nret )
		{
			TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
			return nret;
		}
		else
		{
			SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_INTERNAL , "internal error" )
			TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
			return ZLANG_ERROR_INTERNAL;
		}
	}
	else if( token_info1->token_type == TOKEN_TYPE_THROW ) /* throw */
	{
		if( interp_expr_ctx->get_expr_left_or_right != GET_EXPRESSION_LEFT )
		{
			SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_SYNTAX , "unexpect '%s'" , token1 )
			TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
			return ZLANG_ERROR_SYNTAX;
		}
		
		interp_stat_ctx->is_in_throw = TRUE ;
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "interp_stat_ctx->is_in_throw[%d]" , interp_stat_ctx->is_in_throw )
		
		goto _GOTO_REDO_TRAVEL_TOKEN;
	}
	else if( token_info1->token_type == TOKEN_TYPE_UNCATCH ) /* uncatch */
	{
		if( interp_expr_ctx->get_expr_left_or_right != GET_EXPRESSION_LEFT )
		{
			SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_SYNTAX , "unexpect '%s'" , token1 )
			TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
			return ZLANG_ERROR_SYNTAX;
		}
		
		interp_stat_ctx->is_in_uncatch = TRUE ;
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "interp_stat_ctx->is_in_uncatch[%d]" , interp_stat_ctx->is_in_uncatch )
		
		goto _GOTO_REDO_TRAVEL_TOKEN;
	}
	else if( token_info1->token_type == TOKEN_TYPE_CONTINUE ) /* continue */
	{
		if( interp_expr_ctx->get_expr_left_or_right != GET_EXPRESSION_LEFT )
		{
			SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_SYNTAX , "unexpect '%s'" , token1 )
			TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
			return ZLANG_ERROR_SYNTAX;
		}
		
		TRAVELTOKEN_AND_SAVEINFO( rt , token_info1 , token1 )
		if( token_info1->token_type != TOKEN_TYPE_END_OF_STATEMENT ) /* ; */
		{
			SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_SYNTAX , "expect ';' but '%s'" , rt->travel_token )
			TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
			return ZLANG_ERROR_SYNTAX;
		}
		
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "CONTINUE" )
		TEST_RUNTIME_DEBUG_THEN_PRINT_LEAVE_FUNCTION(rt)
		return ZLANG_INFO_CONTINUE;
	}
	else if( token_info1->token_type == TOKEN_TYPE_BREAK ) /* break */
	{
		if( interp_expr_ctx->get_expr_left_or_right != GET_EXPRESSION_LEFT )
		{
			SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_SYNTAX , "unexpect '%s'" , token1 )
			TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
			return ZLANG_ERROR_SYNTAX;
		}
		
		TRAVELTOKEN_AND_SAVEINFO( rt , token_info1 , token1 )
		if( token_info1->token_type != TOKEN_TYPE_END_OF_STATEMENT ) /* ; */
		{
			SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_SYNTAX , "expect ';' but '%s'" , rt->travel_token )
			TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
			return ZLANG_ERROR_SYNTAX;
		}
		
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "BRERK" )
		TEST_RUNTIME_DEBUG_THEN_PRINT_LEAVE_FUNCTION(rt)
		return ZLANG_INFO_BREAK;
	}
	else if( token_info1->token_type == TOKEN_TYPE_DEFER ) /* defer */
	{
		if( interp_expr_ctx->get_expr_left_or_right != GET_EXPRESSION_LEFT )
		{
			SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_SYNTAX , "unexpect '%s'" , token1 )
			TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
			return ZLANG_ERROR_SYNTAX;
		}
		
		defer = PushLocalDefersStack( rt , & (rt->defers_stack) , GetCurrentDefersStackFrame(rt) ) ;
		if( defer == NULL )
		{
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "PushLocalDefersStack failed[%d]" , nret )
			TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
			return GetRuntimeErrorNo(rt);
		}
		
		defer->defer_begin_token_datapage_header = rt->travel_token_datapage_header ;
		defer->defer_begin_token_dataunit = rt->travel_token_dataunit ;
		
		nret = SkipStatement( rt ) ;
		if( nret != ZLANG_INFO_END_OF_STATEMENT )
		{
			TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
			return nret;
		}
		
		goto _GOTO_REDO_TRAVEL_TOKEN;
	}
	else if( token_info1->token_type == TOKEN_TYPE_RETURN ) /* return */
	{
		if( interp_expr_ctx->get_expr_left_or_right != GET_EXPRESSION_LEFT )
		{
			SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_SYNTAX , "unexpect '%s'" , token1 )
			TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
			return ZLANG_ERROR_SYNTAX;
		}
		
		TEST_RUNTIME_DEBUG( rt )
		{
			TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
			PRINT_TABS_AND_FORMAT( rt , "CALL InterpretExpression_ReturnFunctionStack , last_token[%s][%s]" , _zlang_token_type_str , rt->travel_token )
		}
		nret = InterpretExpression_ReturnFunctionStack( rt ) ;
		TEST_RUNTIME_DEBUG( rt )
		{
			TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
			TEST_RUNTIME_DEBUG( rt ) { PRINT_TABS_AND_FORMAT( rt , "InterpretExpression_ReturnFunctionStack return[%d] last_token[%s][%s]" , nret , _zlang_token_type_str , rt->travel_token ); }
			(*result_obj) = NULL ;
		}
		if( nret == ZLANG_INFO_RETURN )
		{
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "RETURN" )
			TEST_RUNTIME_DEBUG_THEN_PRINT_LEAVE_FUNCTION(rt)
			return nret;
		}
		else if( nret )
		{
			TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
			return nret;
		}
		else
		{
			SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_INTERNAL , "internal error" )
			TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
			return ZLANG_ERROR_INTERNAL;
		}
	}
	else if( token_info1->token_type == TOKEN_TYPE_EXIT ) /* exit */
	{
		if( interp_expr_ctx->get_expr_left_or_right != GET_EXPRESSION_LEFT )
		{
			SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_SYNTAX , "unexpect '%s'" , token1 )
			TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
			return ZLANG_ERROR_SYNTAX;
		}
		
		PEEKTOKEN_AND_SAVEINFO( rt , token_info2 , token2 )
		if( token_info2->token_type == TOKEN_TYPE_BEGIN_OF_SUB_EXPRESSION ) /* ( */
		{
			NEXTTOKEN( rt )
			
			TEST_RUNTIME_DEBUG( rt )
			{
				TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
				PRINT_TABS_AND_FORMAT( rt , "CALL InterpretExpression_InvokeFunction 'exit' , last_token[%s][%s]" , _zlang_token_type_str , rt->travel_token )
			}
			nret = InterpretExpression_InvokeFunction( rt , TOKEN_TYPE_MEMBER_OF , interp_stat_ctx , NULL , token1 , result_obj ) ;
			TEST_RUNTIME_DEBUG( rt )
			{
				TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
				TEST_RUNTIME_DEBUG( rt ) { PRINT_TABS_AND_FORMAT( rt , "InterpretExpression_InvokeFunction 'exit' return[%d] last_token[%s][%s]" , nret , _zlang_token_type_str , rt->travel_token ); }
				(*result_obj) = NULL ;
			}
			if( nret == ZLANG_INFO_EXIT )
			{
				TEST_RUNTIME_DEBUG_THEN_PRINT_LEAVE_FUNCTION(rt)
				return nret;
			}
			else
			{
				SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_INTERNAL , "internal error" )
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return ZLANG_ERROR_INTERNAL;
			}
		}
		else
		{
			SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_SYNTAX , "unexpect token '%s'" , token2 )
			TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
			return ZLANG_ERROR_SYNTAX;
		}
	}
	else if( token_info1->token_type == TOKEN_TYPE_SYNC ) /* sync */
	{
		if( interp_expr_ctx->get_expr_left_or_right != GET_EXPRESSION_LEFT )
		{
			SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_SYNTAX , "unexpect '%s'" , token1 )
			TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
			return ZLANG_ERROR_SYNTAX;
		}
		
		TEST_RUNTIME_DEBUG( rt )
		{
			TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
			PRINT_TABS_AND_FORMAT( rt , "CALL Keyword_sync , last_token[%s][%s]" , _zlang_token_type_str , rt->travel_token )
		}
		nret = Keyword_sync( rt ) ;
		TEST_RUNTIME_DEBUG( rt )
		{
			TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
			PRINT_TABS_AND_FORMAT( rt , "Keyword_sync return[%d] last_token[%s][%s]" , nret , _zlang_token_type_str , rt->travel_token )
			(*result_obj) = NULL ;
		}
		if( nret == ZLANG_INFO_END_OF_STATEMENT )
		{
			TEST_RUNTIME_DEBUG_THEN_PRINT_LEAVE_FUNCTION(rt)
			return nret;
		}
		else if( nret )
		{
			TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
			return nret;
		}
		else
		{
			SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_INTERNAL , "internal error" )
			TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
			return ZLANG_ERROR_INTERNAL;
		}
	}
	else if( token_info1->token_type == TOKEN_TYPE_CONST ) /* const */
	{
		interp_stat_ctx->has_const_decorated = TRUE ;
		
		goto _GOTO_REDO_TRAVEL_TOKEN;
	}
	else if( token_info1->token_type == TOKEN_TYPE_ATOMIC ) /* atomic */
	{
		interp_stat_ctx->has_atomic_decorated = TRUE ;
		
		goto _GOTO_REDO_TRAVEL_TOKEN;
	}
	else if( interp_stat_ctx->token_of_expression_end1 && token_info1->token_type == interp_stat_ctx->token_of_expression_end1 )
	{
		if( interp_expr_ctx->get_expr_left_or_right == GET_EXPRESSION_LEFT )
		{
			TOKENTYPE_TO_STRING( interp_stat_ctx->token_of_expression_end1 , _zlang_token_type_str )
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "TRAVEL EXPRESSION END[%s]" , _zlang_token_type_str )
			(*result_obj) = NULL ;
			TEST_RUNTIME_DEBUG_THEN_PRINT_LEAVE_FUNCTION(rt)
			return ZLANG_INFO_END_OF_EXPRESSION;
		}
	}
	else if( interp_stat_ctx->token_of_expression_end2 && token_info1->token_type == interp_stat_ctx->token_of_expression_end2 )
	{
		if( interp_expr_ctx->get_expr_left_or_right == GET_EXPRESSION_LEFT )
		{
			TOKENTYPE_TO_STRING( interp_stat_ctx->token_of_expression_end2 , _zlang_token_type_str )
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "TRAVEL EXPRESSION END[%s]" , _zlang_token_type_str )
			(*result_obj) = NULL ;
			TEST_RUNTIME_DEBUG_THEN_PRINT_LEAVE_FUNCTION(rt)
			return ZLANG_INFO_END_OF_EXPRESSION;
		}
	}
	else if( interp_stat_ctx->token_of_expression_end3 && token_info1->token_type == interp_stat_ctx->token_of_expression_end3 )
	{
		if( interp_expr_ctx->get_expr_left_or_right == GET_EXPRESSION_LEFT )
		{
			TOKENTYPE_TO_STRING( interp_stat_ctx->token_of_expression_end3 , _zlang_token_type_str )
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "TRAVEL EXPRESSION END[%s]" , _zlang_token_type_str )
			(*result_obj) = NULL ;
			TEST_RUNTIME_DEBUG_THEN_PRINT_LEAVE_FUNCTION(rt)
			return ZLANG_INFO_END_OF_EXPRESSION;
		}
	}
	else
	{
		SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_UNRECOGNIZED_OBJECT , "unrecognized token '%s'" , token1 )
		TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
		return ZLANG_ERROR_UNRECOGNIZED_OBJECT;
	}
	
	TEST_RUNTIME_DEBUG_THEN_PRINT_LEAVE_FUNCTION(rt)
	return 0;
}

int SkipExpression( struct ZlangRuntime *rt , enum TokenType token_of_expression_end1 , enum TokenType token_of_expression_end2 , enum TokenType token_of_expression_end3 )
{
	struct ZlangTokenDataUnitHeader	*token_info2 = NULL ;
	char				*token2 = NULL ;
	
	int				nret = 0 ;
	
	TEST_RUNTIME_DEBUG_THEN_PRINT_ENTER_FUNCTION(rt)
	
	for( ; ; )
	{
		TRAVELTOKEN_AND_SAVEINFO( rt , token_info2 , token2 )
		if( token_info2->token_type == TOKEN_TYPE_BEGIN_OF_SUB_EXPRESSION )
		{
			nret = SkipExpression( rt , TOKEN_TYPE_END_OF_SUB_EXPRESSION , 0 , 0 ) ;
			if( nret != ZLANG_INFO_END_OF_EXPRESSION )
			{
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return nret;
			}
		}
		else if(
			( token_of_expression_end1 && token_info2->token_type == token_of_expression_end1 )
			||
			( token_of_expression_end2 && token_info2->token_type == token_of_expression_end2 )
			||
			( token_of_expression_end3 && token_info2->token_type == token_of_expression_end3 )
		)
		{
			break;
		}
	}
	
	TEST_RUNTIME_DEBUG_THEN_PRINT_LEAVE_FUNCTION(rt)
	
	return ZLANG_INFO_END_OF_EXPRESSION;
}

int SkipStatement( struct ZlangRuntime *rt )
{
	struct ZlangTokenDataUnitHeader	*token_info1 = NULL ;
	char				*token1 = NULL ;
	
	int				nret = 0 ;
	
	TEST_RUNTIME_DEBUG_THEN_PRINT_ENTER_FUNCTION(rt)
	
	PEEKTOKEN_AND_SAVEINFO( rt , token_info1 , token1 )
	if( token_info1->token_type == TOKEN_TYPE_IF )
	{
		NEXTTOKEN( rt )
		
		nret = SkipStatement_if( rt ) ;
		if( nret != ZLANG_INFO_END_OF_STATEMENT )
		{
			TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
			return ZLANG_ERROR_SYNTAX;
		}
	}
	else if( token_info1->token_type == TOKEN_TYPE_WHILE )
	{
		NEXTTOKEN( rt )
		
		nret = SkipStatement_while( rt ) ;
		if( nret != ZLANG_INFO_END_OF_STATEMENT )
		{
			TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
			return ZLANG_ERROR_SYNTAX;
		}
	}
	else if( token_info1->token_type == TOKEN_TYPE_DO )
	{
		NEXTTOKEN( rt )
		
		nret = SkipStatement_do_while( rt ) ;
		if( nret != ZLANG_INFO_END_OF_STATEMENT )
		{
			TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
			return ZLANG_ERROR_SYNTAX;
		}
	}
	else if( token_info1->token_type == TOKEN_TYPE_FOR )
	{
		NEXTTOKEN( rt )
		
		nret = SkipStatement_for( rt ) ;
		if( nret != ZLANG_INFO_END_OF_STATEMENT )
		{
			TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
			return ZLANG_ERROR_SYNTAX;
		}
	}
	else if( token_info1->token_type == TOKEN_TYPE_FOREACH )
	{
		NEXTTOKEN( rt )
		
		nret = SkipStatement_foreach( rt ) ;
		if( nret != ZLANG_INFO_END_OF_STATEMENT )
		{
			TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
			return ZLANG_ERROR_SYNTAX;
		}
	}
	else if( token_info1->token_type == TOKEN_TYPE_SYNC )
	{
		NEXTTOKEN( rt )
		
		nret = SkipStatement_sync( rt ) ;
		if( nret != ZLANG_INFO_END_OF_STATEMENT )
		{
			TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
			return ZLANG_ERROR_SYNTAX;
		}
	}
	else if( token_info1->token_type == TOKEN_TYPE_TRY )
	{
		NEXTTOKEN( rt )
		
		nret = SkipStatement_try( rt ) ;
		if( nret != ZLANG_INFO_END_OF_STATEMENT )
		{
			TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
			return ZLANG_ERROR_SYNTAX;
		}
	}
	else
	{
		nret = SkipExpression( rt , TOKEN_TYPE_END_OF_STATEMENT , 0 , 0 ) ;
		if( nret != ZLANG_INFO_END_OF_EXPRESSION )
		{
			TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
			return nret;
		}
	}
	
	TEST_RUNTIME_DEBUG_THEN_PRINT_LEAVE_FUNCTION(rt)
	
	return ZLANG_INFO_END_OF_STATEMENT;
}

int SkipStatementSegment( struct ZlangRuntime *rt )
{
	struct ZlangTokenDataUnitHeader	*token_info2 = NULL ;
	char				*token2 = NULL ;
	
	int				nret = 0 ;
	
	TEST_RUNTIME_DEBUG_THEN_PRINT_ENTER_FUNCTION(rt)
	
	for( ; ; )
	{
		PEEKTOKEN_AND_SAVEINFO( rt , token_info2 , token2 )
		if( token_info2->token_type == TOKEN_TYPE_BEGIN_OF_STATEMENT_SEGMENT )
		{
			nret = SkipStatementSegment( rt ) ;
			if( nret != ZLANG_INFO_END_OF_STATEMENT_SEGMENT )
			{
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return nret;
			}
		}
		else if( token_info2->token_type == TOKEN_TYPE_END_OF_STATEMENT_SEGMENT )
		{
			NEXTTOKEN( rt )
			break;
		}
		else
		{
			nret = SkipStatement( rt ) ;
			if( nret != ZLANG_INFO_END_OF_STATEMENT )
			{
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return nret;
			}
		}
	}
	
	TEST_RUNTIME_DEBUG_THEN_PRINT_LEAVE_FUNCTION(rt)
	
	return ZLANG_INFO_END_OF_STATEMENT_SEGMENT;
}

