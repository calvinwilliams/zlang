/* Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef _H_WBYTE_
#define _H_WBYTE_

#if defined(__linux__)
#define DLLEXPORT	extern
#elif defined(_WIN32)
#define DLLEXPORT	__declspec(dllexport)
#endif

#define WBYTE_CHARSET_GB18030	54936
#define WBYTE_CHARSET_UTF8	65001

DLLEXPORT int EvaluateOneWbyteLength( uint16_t charset , unsigned char *begin , unsigned char *end );

#endif

