#include "zlang_in.h"

struct ZlangDirectProperty_zruntime
{
	int		dummy ;
} ;

ZlangInvokeFunction ZlangInvokeFunction_zruntime_GetInputParametersCount;
int ZlangInvokeFunction_zruntime_GetInputParametersCount( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangObject			*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObjectsStackFrame		*previous_function_local_objs_stack_frame = NULL ;
	int32_t					param_count ;
	
	previous_function_local_objs_stack_frame = GetPreviousFunctionObjectsStackFrame( rt ) ;
	
	param_count = (int)GetInputParameterCountInStackFrame( rt , previous_function_local_objs_stack_frame ) ;
	CallRuntimeFunction_int_SetIntValue( rt , out1 , param_count );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_zruntime_GetInputParameter;
int ZlangInvokeFunction_zruntime_GetInputParameter( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangObject			*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject			*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObjectsStackFrame		*previous_function_local_objs_stack_frame = NULL ;
	int32_t					param_no ;
	struct ZlangObject			*param_obj = NULL ;
	int					nret = 0 ;
	
	previous_function_local_objs_stack_frame = GetPreviousFunctionObjectsStackFrame( rt ) ;
	
	CallRuntimeFunction_int_GetIntValue( rt , in1 , & param_no );
	param_obj = GetInputParameterInStackFrame( rt , previous_function_local_objs_stack_frame , (int)param_no ) ;
	if( param_obj == NULL )
	{
		UnreferObject( rt , out1 ) ;
		return ThrowFatalException( rt , nret , EXCEPTION_MESSAGE_GENERAL_ERROR );
	}
	nret = ReferObject( rt , out1 , param_obj ) ;
	if( nret )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "ReferObject failed[%d]" , nret )
		return ThrowFatalException( rt , nret , EXCEPTION_MESSAGE_GENERAL_ERROR );
	}
	
	return 0;
}

ZlangCreateDirectPropertyFunction ZlangCreateDirectProperty_zruntime;
void *ZlangCreateDirectProperty_zruntime( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_zruntime	*obj_direct_prop = NULL ;
	
	obj_direct_prop = (struct ZlangDirectProperty_zruntime *)ZLMALLOC( sizeof(struct ZlangDirectProperty_zruntime) ) ;
	if( obj_direct_prop == NULL )
	{
		SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_ALLOC , "alloc memory for entity" )
		return NULL;
	}
	memset( obj_direct_prop , 0x00 , sizeof(struct ZlangDirectProperty_zruntime) );
	
	return (void *)obj_direct_prop;
}

ZlangDestroyDirectPropertyFunction ZlangDestroyDirectProperty_zruntime;
void ZlangDestroyDirectProperty_zruntime( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_zruntime	*obj_direct_prop = GetObjectDirectProperty(obj) ;
	
	ZLFREE( obj_direct_prop );
	
	return;
}

static struct ZlangDirectFunctions direct_funcs_zruntime =
	{
		ZLANG_OBJECT_zruntime ,
		
		ZlangCreateDirectProperty_zruntime ,
		ZlangDestroyDirectProperty_zruntime ,
		
		NULL ,
		NULL ,
		NULL ,
		NULL ,
		
		NULL ,
		NULL ,
		NULL ,
		NULL ,
		NULL ,
		
		NULL ,
		NULL ,
		NULL ,
		NULL ,
		NULL ,
		
		NULL ,
		NULL ,
		NULL ,
		NULL ,
		NULL ,
		NULL ,
		
		NULL ,
		NULL ,
		
		NULL ,
		NULL ,
		NULL ,
		NULL ,
		NULL ,
		
		NULL ,
	} ;

ZlangImportObjectFunction ZlangImportObject_zruntime;
struct ZlangObject *ZlangImportObject_zruntime( struct ZlangRuntime *rt )
{
	struct ZlangObject	*obj = NULL ;
	struct ZlangFunction	*func = NULL ;
	int			nret = 0 ;
	
	nret = ImportObject( rt , & obj , ZLANG_OBJECT_zruntime , & direct_funcs_zruntime , sizeof(struct ZlangDirectFunctions) , NULL ) ;
	if( nret )
	{
		SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_LINK_FUNC_TO_ENTITY , "import object to global objects heap" )
		return NULL;
	}
	
	/* zruntime.GetInputParametersCount() */
	func = AddFunctionAndParametersInObject( rt , obj , "GetInputParametersCount" , "GetInputParametersCount()" , ZlangInvokeFunction_zruntime_GetInputParametersCount , ZLANG_OBJECT_int , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* zruntime.GetInputParameter(int) */
	func = AddFunctionAndParametersInObject( rt , obj , "GetInputParameter" , "GetInputParameter(int)" , ZlangInvokeFunction_zruntime_GetInputParameter , ZLANG_OBJECT_object , ZLANG_OBJECT_int,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	return obj ;
}

