/* Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "zlang_in.h"

struct ZlangInterface *CreateInterface( struct ZlangRuntime *rt , char *interf_name )
{
	struct ZlangInterface	*interf = NULL ;
	int			nret = 0 ;
	
	interf = (struct ZlangInterface *)ZLMALLOC( sizeof(struct ZlangInterface) ) ;
	if( interf == NULL )
		return NULL;
	memset( interf , 0x00 , sizeof(struct ZlangInterface) );
	
	interf->inner = AllocObject( rt ) ;
	if( interf->inner == NULL )
	{
		DestroyInterface( rt , interf );
		return NULL;
	}
	memset( interf->inner , 0x00 , sizeof(struct ZlangObject) );
	
	nret = InitObject( rt , interf->inner , interf_name , NULL , 0 ) ;
	if( nret )
	{
		DestroyInterface( rt , interf );
		return NULL;
	}
	
	return interf;
}

void DestroyInterface( struct ZlangRuntime *rt , struct ZlangInterface *interf )
{
	if( interf )
	{
		if( interf->inner )
		{
			DestroyObject( rt , interf->inner );
		}
		
		ZLFREE( interf );
	}
	
	return;
}

struct ZlangInterface *QueryGlobalInterfaceByInterfaceName( struct ZlangRuntime *rt , char *interf_name )
{
	struct ZlangInterface	i ;
	
	memset( & i , 0x00 , sizeof(struct ZlangInterface) );
	i.interf_name = interf_name ;
	return QueryInterfaceInRuntimeInterfacesHeapByInterfaceName( rt , & i );
}

char *GetInterfaceName( struct ZlangInterface *interf )
{
	if( interf == NULL )
		return NULL;
	
	return interf->interf_name;
}

struct ZlangObject *GetInterfaceInner( struct ZlangInterface *interf )
{
	if( interf == NULL )
		return NULL;
	
	return interf->inner;
}

void DebugPrintInterface( struct ZlangRuntime *rt , struct ZlangInterface *interf )
{
	if( interf == NULL )
		return;
	
	PRINT_TABS(rt) printf( " interf[%p][%s] " , interf , GetObjectName(interf->inner) ); DebugPrintObject(rt,interf->inner); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE
	
	DebugPrintObjectFunctions( rt , interf->inner );
	
	return;
}

