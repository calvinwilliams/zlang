#include "zlang_in.h"

#include "zobject_exception.tpl.c"

exception_THROWEXCEPTION( error )
ZLANGINVOKEFUNCTION_exception_THROWEXCEPTION( error )

exception_HAVEEXCEPTION( error )
ZLANGINVOKEFUNCTION_exception_HAVEEXCEPTION( error )

exception_GETCODE( error )
ZLANGINVOKEFUNCTION_exception_GETCODE( error )

exception_GETMESSAGE( error )
ZLANGINVOKEFUNCTION_exception_GETMESSAGE( error )

exception_GETEXCEPTIONSOURCEFILENAME( error )
ZLANGINVOKEFUNCTION_exception_GETEXCEPTIONSOURCEFILENAME( error )

exception_GETEXCEPTIONSOURCEROW( error )
ZLANGINVOKEFUNCTION_exception_GETEXCEPTIONSOURCEROW( error )

exception_GETEXCEPTIONSOURCECOLUMN( error )
ZLANGINVOKEFUNCTION_exception_GETEXCEPTIONSOURCECOLUMN( error )

exception_GETEXCEPTIONOBJECTNAME( error )
ZLANGINVOKEFUNCTION_exception_GETEXCEPTIONOBJECTNAME( error )

exception_GETEXCEPTIONFUNCTIONNAME( error )
ZLANGINVOKEFUNCTION_exception_GETEXCEPTIONFUNCTIONNAME( error )

exception_GETSTACKTRACE( error )
ZLANGINVOKEFUNCTION_exception_GETSTACKTRACE( error )

exception_CLEANTHROWN( error )
ZLANGINVOKEFUNCTION_exception_CLEANTHROWN( error )

ZlangCreateDirectPropertyFunction ZlangCreateDirectProperty_error;
void *ZlangCreateDirectProperty_error( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_error	*obj_direct_prop = NULL ;
	
	obj_direct_prop = (struct ZlangDirectProperty_error *)ZLMALLOC( sizeof(struct ZlangDirectProperty_error) ) ;
	if( obj_direct_prop == NULL )
	{
		SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_ALLOC , "alloc memory for entity" )
		return NULL;
	}
	memset( obj_direct_prop , 0x00 , sizeof(struct ZlangDirectProperty_error) );
	
	obj_direct_prop->message_obj = CloneStringObject( rt , NULL ) ;
	if( obj_direct_prop->message_obj == NULL )
	{
		ZLFREE( obj_direct_prop );
		SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_ALLOC , "alloc memory for entity" )
		return NULL;
	}
	
	obj_direct_prop->stack_trace_obj = CloneStringObject( rt , NULL ) ;
	if( obj_direct_prop->stack_trace_obj == NULL )
	{
		DestroyObject( rt , obj_direct_prop->message_obj );
		ZLFREE( obj_direct_prop );
		SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_ALLOC , "alloc memory for entity" )
		return NULL;
	}
	
	return (void *)obj_direct_prop;
}

ZlangDestroyDirectPropertyFunction ZlangDestroyDirectProperty_error;
void ZlangDestroyDirectProperty_error( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_error	*obj_direct_prop = GetObjectDirectProperty(obj) ;
	
	error_CleanException( rt , obj );
	DestroyObject( rt , obj_direct_prop->message_obj );
	if( obj_direct_prop->exception_obj_name )
	{
		ZLFREE( obj_direct_prop->exception_obj_name );
	}
	if( obj_direct_prop->exception_func_name )
	{
		ZLFREE( obj_direct_prop->exception_func_name );
	}
	DestroyObject( rt , obj_direct_prop->stack_trace_obj );
	
	ZLFREE( obj_direct_prop );
	
	return;
}

static struct ZlangDirectFunctions direct_funcs_error =
	{
		ZLANG_OBJECT_error ,
		
		ZlangCreateDirectProperty_error ,
		ZlangDestroyDirectProperty_error ,
		
		NULL ,
		NULL ,
		NULL ,
		NULL ,
		
		NULL ,
		NULL ,
		NULL ,
		NULL ,
		NULL ,
		
		NULL ,
		NULL ,
		NULL ,
		NULL ,
		NULL ,
		
		NULL ,
		NULL ,
		NULL ,
		NULL ,
		NULL ,
		NULL ,
		
		NULL ,
		NULL ,
		
		NULL ,
		NULL ,
		NULL ,
		NULL ,
		NULL ,
		
		NULL ,
	} ;

ZlangImportObjectFunction ZlangImportObject_error;
struct ZlangObject *ZlangImportObject_error( struct ZlangRuntime *rt )
{
	struct ZlangObject	*obj = NULL ;
	struct ZlangFunction	*func = NULL ;
	int			nret = 0 ;
	
	nret = ImportObject( rt , & obj , ZLANG_OBJECT_error , & direct_funcs_error , sizeof(struct ZlangDirectFunctions) , NULL ) ;
	if( nret )
	{
		SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_LINK_FUNC_TO_ENTITY , "import object to global objects heap" )
		return NULL;
	}
	
	/* error.ThrowException(int,string) */
	func = AddFunctionAndParametersInObject( rt , obj , "ThrowException" , "ThrowException(int,string)" , ZlangInvokeFunction_error_ThrowException_int_string , ZLANG_OBJECT_int , ZLANG_OBJECT_int,NULL , ZLANG_OBJECT_string,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* error.HaveException() */
	func = AddFunctionAndParametersInObject( rt , obj , "HaveException" , "HaveException()" , ZlangInvokeFunction_error_HaveException , ZLANG_OBJECT_bool , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* error.GetCode() */
	func = AddFunctionAndParametersInObject( rt , obj , "GetCode" , "GetCode()" , ZlangInvokeFunction_error_GetCode , ZLANG_OBJECT_int , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* error.GetMessage() */
	func = AddFunctionAndParametersInObject( rt , obj , "GetMessage" , "GetMessage()" , ZlangInvokeFunction_error_GetMessage , ZLANG_OBJECT_string , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* error.GetExceptionSourceFilename() */
	func = AddFunctionAndParametersInObject( rt , obj , "GetExceptionSourceFilename" , "GetExceptionSourceFilename()" , ZlangInvokeFunction_error_GetExceptionSourceFilename , ZLANG_OBJECT_string , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* error.GetExceptionSourceRow() */
	func = AddFunctionAndParametersInObject( rt , obj , "GetExceptionSourceRow" , "GetExceptionSourceRow()" , ZlangInvokeFunction_error_GetExceptionSourceRow , ZLANG_OBJECT_int , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* error.GetExceptionSourceColumn() */
	func = AddFunctionAndParametersInObject( rt , obj , "GetExceptionSourceColumn" , "GetExceptionSourceColumn()" , ZlangInvokeFunction_error_GetExceptionSourceColumn , ZLANG_OBJECT_int , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* error.GetExceptionObjectName() */
	func = AddFunctionAndParametersInObject( rt , obj , "GetExceptionObjectName" , "GetExceptionObjectName()" , ZlangInvokeFunction_error_GetExceptionObjectName , ZLANG_OBJECT_string , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* error.GetExceptionFunctionName() */
	func = AddFunctionAndParametersInObject( rt , obj , "GetExceptionFunctionName" , "GetExceptionFunctionName()" , ZlangInvokeFunction_error_GetExceptionFunctionName , ZLANG_OBJECT_string , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* error.GetStackTrace() */
	func = AddFunctionAndParametersInObject( rt , obj , "GetStackTrace" , "GetStackTrace()" , ZlangInvokeFunction_error_GetStackTrace , ZLANG_OBJECT_string , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* error.CleanException() */
	func = AddFunctionAndParametersInObject( rt , obj , "CleanException" , "CleanException()" , ZlangInvokeFunction_error_CleanException , ZLANG_OBJECT_void , NULL ) ;
	if( func == NULL )
		return NULL;
	
	SetRuntimeFunction_error_ThrowException( rt , error_ThrowException );
	SetRuntimeFunction_error_HaveException( rt , error_HaveException );
	SetRuntimeFunction_error_GetMessage( rt , error_GetMessage );
	SetRuntimeFunction_error_GetExceptionSourceFilename( rt , error_GetExceptionSourceFilename );
	SetRuntimeFunction_error_GetExceptionSourceRow( rt , error_GetExceptionSourceRow );
	SetRuntimeFunction_error_GetExceptionSourceColumn( rt , error_GetExceptionSourceColumn );
	SetRuntimeFunction_error_GetExceptionObjectName( rt , error_GetExceptionObjectName );
	SetRuntimeFunction_error_GetExceptionFunctionName( rt , error_GetExceptionFunctionName );
	SetRuntimeFunction_error_GetStackTrace( rt , error_GetStackTrace );
	SetRuntimeFunction_error_CleanException( rt , error_CleanException );
	
	return obj ;
}

