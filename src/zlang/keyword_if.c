/* Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "zlang_in.h"

int Keyword_if( struct ZlangRuntime *rt )
{
	struct ZlangInterpretStatementContext	if_expr_interpret_statement_ctx ;
	struct ZlangObject			*logic_obj = NULL ;
	unsigned char				logic_result ;
	struct ZlangTokenDataUnitHeader		*token_info1 = NULL ;
	char					*token1 = NULL ;
	struct ZlangTokenDataUnitHeader		*token_info2 = NULL ;
	char					*token2 = NULL ;
	struct ZlangTokenDataUnitHeader		*token_info3 = NULL ;
	char					*token3 = NULL ;
	struct ZlangTokenDataUnitHeader		*token_info4 = NULL ;
	char					*token4 = NULL ;
	/*
	struct ZlangObject			*bak_in_obj = NULL ;
	*/
	struct ZlangFunction			*bak_in_func = NULL ;
	
	int					nret = 0 ;
	
	TEST_RUNTIME_DEBUG_THEN_PRINT_ENTER_FUNCTION(rt)
	
_GOTO_RETRY_IF_CONDITION :
	TRAVELTOKEN_AND_SAVEINFO( rt , token_info1 , token1 )
	if( token_info1->token_type != TOKEN_TYPE_BEGIN_OF_SUB_EXPRESSION ) /* ( */
	{
		SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_SYNTAX , "expect '(' but '%s'" , rt->travel_token )
		TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
		return ZLANG_ERROR_SYNTAX;
	}
	
	TEST_RUNTIME_DEBUG( rt )
	{
		TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
		PRINT_TABS_AND_FORMAT( rt , "CALL InterpretExpression , last_token[%s][%s]" , _zlang_token_type_str , rt->travel_token )
	}
	memset( & if_expr_interpret_statement_ctx , 0x00 , sizeof(struct ZlangInterpretStatementContext) );
	if_expr_interpret_statement_ctx.token_of_expression_end1 = TOKEN_TYPE_END_OF_SUB_EXPRESSION ;
	nret = InterpretExpression( rt , & if_expr_interpret_statement_ctx , & logic_obj ) ;
	TEST_RUNTIME_DEBUG( rt )
	{
		TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
		TEST_RUNTIME_DEBUG( rt ) { PRINT_TABS(rt) printf( "InterpretExpression return[%d] last_token[%s][%s] " , nret , _zlang_token_type_str , rt->travel_token ); DebugPrintObject(rt,logic_obj); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE }
	}
	if( nret != ZLANG_INFO_END_OF_EXPRESSION )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
		return nret;
	}
	
	nret = IsObjectTrueValue( rt , logic_obj , & logic_result ) ;
	if( nret )
	{
		SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , nret , "IsObjectTrueValue failed" )
		return nret;
	}
	
	if( logic_result )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "if ( ture ) ..." )
		
		PEEKTOKEN_AND_SAVEINFO( rt , token_info2 , token2 )
		if( token_info2->token_type == TOKEN_TYPE_BEGIN_OF_STATEMENT_SEGMENT )
		{
			NEXTTOKEN( rt )
			
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "CALL InterpretStatementSegment" )
			IncreaseStackFrame( rt , NULL );
			/*
			bak_in_obj = rt->in_obj ;
			rt->in_obj = NULL ;
			*/
			bak_in_func = rt->in_func ;
			rt->in_func = NULL ;
			nret = InterpretStatementSegment( rt , NULL ) ;
			/*
			rt->in_obj = bak_in_obj ;
			*/
			rt->in_func = bak_in_func ;
			DecreaseStackFrame( rt );
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "InterpretStatementSegment return[%d]" , nret )
			if( nret != ZLANG_INFO_END_OF_STATEMENT_SEGMENT )
			{
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return nret;
			}
		}
		else
		{
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "CALL InterpretStatement" )
			nret = InterpretStatement( rt ) ;
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "InterpretStatement return[%d]" , nret )
			if( nret != ZLANG_INFO_END_OF_STATEMENT )
			{
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return nret;
			}
		}
		
_GOTO_SKIP_ELSE :
		PEEKTOKEN_AND_SAVEINFO( rt , token_info2 , token2 )
		if( token_info2->token_type == TOKEN_TYPE_ELSE )
		{
			NEXTTOKEN( rt )
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "if ( ture ) statement_or_segment else ..." )
			
			PEEKTOKEN_AND_SAVEINFO( rt , token_info3 , token3 )
			if( token_info3->token_type == TOKEN_TYPE_IF )
			{
				NEXTTOKEN( rt )
				TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "if ( true ) statement_or_segment else if ..." )
				
				TRAVELTOKEN_AND_SAVEINFO( rt , token_info4 , token4 )
				if( token_info4->token_type != TOKEN_TYPE_BEGIN_OF_SUB_EXPRESSION )
				{
					SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_SYNTAX , "expect '(' but '%s'" , token4 )
					TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
					return ZLANG_ERROR_SYNTAX;
				}
				
				nret = SkipExpression( rt , TOKEN_TYPE_END_OF_SUB_EXPRESSION , 0 , 0 ) ;
				if( nret != ZLANG_INFO_END_OF_EXPRESSION )
				{
					TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
					return nret;
				}
				
				PEEKTOKEN_AND_SAVEINFO( rt , token_info4 , token4 )
				if( token_info4->token_type == TOKEN_TYPE_BEGIN_OF_STATEMENT_SEGMENT )
				{
					NEXTTOKEN( rt )
					TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "if ( true ) statement_or_segment else if ( ... ) { ... }" )
					
					nret = SkipStatementSegment( rt ) ;
					if( nret != ZLANG_INFO_END_OF_STATEMENT_SEGMENT )
					{
						TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
						return nret;
					}
				}
				else
				{
					NEXTTOKEN( rt )
					TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "if ( true ) statement_or_segment else if ( ... ) ..." )
					
					nret = SkipStatement( rt ) ;
					if( nret != ZLANG_INFO_END_OF_STATEMENT )
					{
						TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
						return nret;
					}
				}
				
				TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "goto skip else" )
				goto _GOTO_SKIP_ELSE;
			}
			else
			{
				TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "if ( true ) statement_or_segment else !if..." )
				
				if( token_info3->token_type == TOKEN_TYPE_BEGIN_OF_STATEMENT_SEGMENT )
				{
					NEXTTOKEN( rt )
					TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "if ( true ) statement_or_segment else { statement_segment... }" )
					
					nret = SkipStatementSegment( rt ) ;
					if( nret != ZLANG_INFO_END_OF_STATEMENT_SEGMENT )
					{
						TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
						return nret;
					}
				}
				else
				{
					NEXTTOKEN( rt )
					TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "if ( true ) statement_or_segment else statement..." )
					
					nret = SkipStatement( rt ) ;
					if( nret != ZLANG_INFO_END_OF_STATEMENT )
					{
						TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
						return nret;
					}
				}
			}
		}
	}
	else
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "if ( false ) ..." )
		
		PEEKTOKEN_AND_SAVEINFO( rt , token_info2 , token2 )
		if( token_info2->token_type == TOKEN_TYPE_BEGIN_OF_STATEMENT_SEGMENT )
		{
			NEXTTOKEN( rt )
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "if ( false ) { statement_segment }" )
			
			nret = SkipStatementSegment( rt ) ;
			if( nret != ZLANG_INFO_END_OF_STATEMENT_SEGMENT )
			{
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return nret;
			}
		}
		else
		{
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "if ( false ) statement" )
			
			nret = SkipStatement( rt ) ;
			if( nret != ZLANG_INFO_END_OF_STATEMENT )
			{
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return nret;
			}
		}
		
		PEEKTOKEN_AND_SAVEINFO( rt , token_info2 , token2 )
		if( token_info2->token_type == TOKEN_TYPE_ELSE )
		{
			NEXTTOKEN( rt )
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "if ( false ) statement_or_segment else ..." )
			
			PEEKTOKEN_AND_SAVEINFO( rt , token_info3 , token3 )
			if( token_info3->token_type == TOKEN_TYPE_IF )
			{
				NEXTTOKEN( rt )
				TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "if ( false ) statement_or_segment else if ..." )
				TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "goto retry if condition" )
				goto _GOTO_RETRY_IF_CONDITION;
			}
			else
			{
				TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "if ( false ) statement_or_segment else ..." )
				
				PEEKTOKEN_AND_SAVEINFO( rt , token_info4 , token4 )
				if( token_info4->token_type == TOKEN_TYPE_BEGIN_OF_STATEMENT_SEGMENT )
				{
					NEXTTOKEN( rt )
					TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "if ( false ) statement_or_segment else { statement_segment... }" )
					
					TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "CALL InterpretStatementSegment" )
					IncreaseStackFrame( rt , NULL );
					/*
					bak_in_obj = rt->in_obj ;
					rt->in_obj = NULL ;
					*/
					bak_in_func = rt->in_func ;
					rt->in_func = NULL ;
					nret = InterpretStatementSegment( rt , NULL ) ;
					/*
					rt->in_obj = bak_in_obj ;
					*/
					rt->in_func = bak_in_func ;
					DecreaseStackFrame( rt );
					TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "InterpretStatementSegment return[%d]" , nret )
					if( nret != ZLANG_INFO_END_OF_STATEMENT_SEGMENT )
					{
						TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
						return nret;
					}
				}
				else
				{
					TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "if ( false ) statement_or_segment else statement..." )
					
					TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "CALL InterpretStatement" )
					nret = InterpretStatement( rt ) ;
					TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "InterpretStatement return[%d]" , nret )
					if( nret != ZLANG_INFO_END_OF_STATEMENT )
					{
						TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
						return nret;
					}
				}
				
			}
		}
	}
	
	TEST_RUNTIME_DEBUG_THEN_PRINT_LEAVE_FUNCTION(rt)
	return ZLANG_INFO_END_OF_STATEMENT;
}

int SkipStatement_if( struct ZlangRuntime *rt )
{
	struct ZlangTokenDataUnitHeader	*token_info1 = NULL ;
	char				*token1 = NULL ;
	struct ZlangTokenDataUnitHeader	*token_info2 = NULL ;
	char				*token2 = NULL ;
	struct ZlangTokenDataUnitHeader	*token_info3 = NULL ;
	char				*token3 = NULL ;
	struct ZlangTokenDataUnitHeader	*token_info4 = NULL ;
	char				*token4 = NULL ;
	
	int				nret = 0 ;
	
	TEST_RUNTIME_DEBUG_THEN_PRINT_ENTER_FUNCTION(rt)
	
_GOTO_RETRY_IF_CONDITION :
	PEEKTOKEN_AND_SAVEINFO( rt , token_info1 , token1 )
	if( token_info1->token_type != TOKEN_TYPE_BEGIN_OF_SUB_EXPRESSION ) /* if ( */
	{
		SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_SYNTAX , "unexpect '%s'" , token1 )
		TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
		return ZLANG_ERROR_SYNTAX;
	}
	
	NEXTTOKEN( rt )
	
	nret = SkipExpression( rt , TOKEN_TYPE_END_OF_SUB_EXPRESSION , 0 , 0 ) ; /* if ( ... ) */
	if( nret != ZLANG_INFO_END_OF_EXPRESSION )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
		return nret;
	}
	
	PEEKTOKEN_AND_SAVEINFO( rt , token_info2 , token2 )
	if( token_info2->token_type == TOKEN_TYPE_BEGIN_OF_STATEMENT_SEGMENT )
	{
		NEXTTOKEN( rt )
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "if ( ... ) { statement_segment... }" )
		
		nret = SkipStatementSegment( rt ) ;
		if( nret != ZLANG_INFO_END_OF_STATEMENT_SEGMENT )
		{
			TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
			return nret;
		}
	}
	else
	{
		NEXTTOKEN( rt )
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "if ( ... ) statement..." )
		
		nret = SkipStatement( rt ) ;
		if( nret != ZLANG_INFO_END_OF_STATEMENT )
		{
			TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
			return nret;
		}
	}
	
	PEEKTOKEN_AND_SAVEINFO( rt , token_info2 , token2 )
	if( token_info2->token_type == TOKEN_TYPE_ELSE )
	{
		NEXTTOKEN( rt )
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "if ( ... ) ... else ..." )
		
		PEEKTOKEN_AND_SAVEINFO( rt , token_info3 , token3 )
		if( token_info3->token_type == TOKEN_TYPE_IF )
		{
			NEXTTOKEN( rt )
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "if ( ... ) ... else if ..." )
			goto _GOTO_RETRY_IF_CONDITION;
		}
		else
		{
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "if ( ... ) ... else ..." )
			
			PEEKTOKEN_AND_SAVEINFO( rt , token_info4 , token4 )
			if( token_info4->token_type == TOKEN_TYPE_BEGIN_OF_STATEMENT_SEGMENT )
			{
				NEXTTOKEN( rt )
				TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "if ( ... ) ... else { statement_segment... }" )
				
				nret = SkipStatementSegment( rt ) ;
				if( nret != ZLANG_INFO_END_OF_STATEMENT_SEGMENT )
				{
					TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
					return nret;
				}
			}
			else
			{
				NEXTTOKEN( rt )
				TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "if ( ... ) ... else statement..." )
				
				nret = SkipStatement( rt ) ;
				if( nret != ZLANG_INFO_END_OF_STATEMENT )
				{
					TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
					return nret;
				}
			}
		}
	}
	
	TEST_RUNTIME_DEBUG_THEN_PRINT_LEAVE_FUNCTION(rt)
	return ZLANG_INFO_END_OF_STATEMENT;
}

