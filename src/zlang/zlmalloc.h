#ifndef _H_ZLMALLOC_
#define _H_ZLMALLOC_

#include <stdlib.h>
#include <string.h>

#if defined(__linux__)
#define DLLEXPORT	extern
#elif defined(_WIN32)
#define DLLEXPORT	__declspec(dllexport)
#endif

#ifdef __USE_ZLMALLOC
#define ZLMALLOC(_size_)		_zlmalloc("malloc",__FILE__,__LINE__,_size_)
#define ZLFREE(_ptr_)			_zlfree("free",__FILE__,__LINE__,_ptr_)
#define ZLCALLOC(_nmemb_,_size_)	_zlcalloc("calloc",__FILE__,__LINE__,_nmemb_,_size_)
#define ZLREALLOC(_ptr_,_size_)		_zlrealloc("realloc",__FILE__,__LINE__,_ptr_,_size_)
#define ZLSTRDUP(_str_)			_zlstrdup("strdup",__FILE__,__LINE__,_str_)
#define ZLSTRNDUP(_str_,_str_len_)	_zlstrndup("strndup",__FILE__,__LINE__,_str_,_str_len_)
#else
#define ZLMALLOC(_size_)		malloc(_size_)
#define ZLFREE(_ptr_)			free(_ptr_)
#define ZLCALLOC(_nmemb_,_size_)	calloc(_nmemb_,_size_)
#define ZLREALLOC(_ptr_,_size_)		realloc(_ptr_,_size_)
#define ZLSTRDUP(_str_)			strdup(_str_)
#define ZLSTRNDUP(_str_,_str_len_)	strndup(_str_,_str_len_)
#endif

DLLEXPORT void *_zlmalloc( char *action , char *file , size_t line , size_t size );
DLLEXPORT void _zlfree( char *action , char *file , size_t line , void *ptr );
DLLEXPORT void *_zlcalloc( char *action , char *file , size_t line , size_t nmemb , size_t size );
DLLEXPORT void *_zlrealloc( char *action , char *file , size_t line , void *ptr , size_t size );
DLLEXPORT char *_zlstrdup( char *action , char *file , size_t line , char *s );
DLLEXPORT char *_zlstrndup( char *action , char *file , size_t line , char *s , size_t n );

struct ZlMemBlocks ;
struct ZlMemBlock ;

DLLEXPORT size_t ZlGetUsingMemory();
DLLEXPORT size_t ZlGetUsingBlocks();
DLLEXPORT size_t ZlGetUsingTotalMemory();
DLLEXPORT struct ZlMemBlocks *ZlGetMemBlocks();

DLLEXPORT struct ZlMemBlock *ZlTravelMemoryBlock( struct ZlMemBlocks *memblocks , struct ZlMemBlock *memblock );
DLLEXPORT char *ZlGetMemblockBase( struct ZlMemBlock *memblock );
DLLEXPORT char *ZlGetMemblockAction( struct ZlMemBlock *memblock );
DLLEXPORT size_t ZlGetMemblockSize( struct ZlMemBlock *memblock );
DLLEXPORT char *ZlGetMemblockSourceFile( struct ZlMemBlock *memblock );
DLLEXPORT size_t ZlGetMemblockSourceLine( struct ZlMemBlock *memblock );

DLLEXPORT int ZlWriteLockMemblocks();
DLLEXPORT int ZlReadLockMemblocks();
DLLEXPORT int ZlUnlockMemblocks();

DLLEXPORT unsigned char ZlUseStdlib( unsigned char enable );

#endif

