#include "zlang_in.h"

#include "zobject_exception.tpl.c"

exception_THROWEXCEPTION( fatal )
ZLANGINVOKEFUNCTION_exception_THROWEXCEPTION( fatal )

exception_HAVEEXCEPTION( fatal )
ZLANGINVOKEFUNCTION_exception_HAVEEXCEPTION( fatal )

exception_GETCODE( fatal )
ZLANGINVOKEFUNCTION_exception_GETCODE( fatal )

exception_GETMESSAGE( fatal )
ZLANGINVOKEFUNCTION_exception_GETMESSAGE( fatal )

exception_GETEXCEPTIONSOURCEFILENAME( fatal )
ZLANGINVOKEFUNCTION_exception_GETEXCEPTIONSOURCEFILENAME( fatal )

exception_GETEXCEPTIONSOURCEROW( fatal )
ZLANGINVOKEFUNCTION_exception_GETEXCEPTIONSOURCEROW( fatal )

exception_GETEXCEPTIONSOURCECOLUMN( fatal )
ZLANGINVOKEFUNCTION_exception_GETEXCEPTIONSOURCECOLUMN( fatal )

exception_GETEXCEPTIONOBJECTNAME( fatal )
ZLANGINVOKEFUNCTION_exception_GETEXCEPTIONOBJECTNAME( fatal )

exception_GETEXCEPTIONFUNCTIONNAME( fatal )
ZLANGINVOKEFUNCTION_exception_GETEXCEPTIONFUNCTIONNAME( fatal )

exception_GETSTACKTRACE( fatal )
ZLANGINVOKEFUNCTION_exception_GETSTACKTRACE( fatal )

exception_CLEANTHROWN( fatal )
ZLANGINVOKEFUNCTION_exception_CLEANTHROWN( fatal )

ZlangCreateDirectPropertyFunction ZlangCreateDirectProperty_fatal;
void *ZlangCreateDirectProperty_fatal( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_fatal	*obj_direct_prop = NULL ;
	
	obj_direct_prop = (struct ZlangDirectProperty_fatal *)ZLMALLOC( sizeof(struct ZlangDirectProperty_fatal) ) ;
	if( obj_direct_prop == NULL )
	{
		SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_ALLOC , "alloc memory for entity" )
		return NULL;
	}
	memset( obj_direct_prop , 0x00 , sizeof(struct ZlangDirectProperty_fatal) );
	
	obj_direct_prop->message_obj = CloneStringObject( rt , NULL ) ;
	if( obj_direct_prop->message_obj == NULL )
	{
		ZLFREE( obj_direct_prop );
		SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_ALLOC , "alloc memory for entity" )
		return NULL;
	}
	
	obj_direct_prop->stack_trace_obj = CloneStringObject( rt , NULL ) ;
	if( obj_direct_prop->stack_trace_obj == NULL )
	{
		DestroyObject( rt , obj_direct_prop->message_obj );
		ZLFREE( obj_direct_prop );
		SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_ALLOC , "alloc memory for entity" )
		return NULL;
	}
	
	return (void *)obj_direct_prop;
}

ZlangDestroyDirectPropertyFunction ZlangDestroyDirectProperty_fatal;
void ZlangDestroyDirectProperty_fatal( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_fatal	*obj_direct_prop = GetObjectDirectProperty(obj) ;
	
	fatal_CleanException( rt , obj );
	DestroyObject( rt , obj_direct_prop->message_obj );
	DestroyObject( rt , obj_direct_prop->stack_trace_obj );
	
	ZLFREE( obj_direct_prop );
	
	return;
}

static struct ZlangDirectFunctions direct_funcs_fatal =
	{
		ZLANG_OBJECT_fatal ,
		
		ZlangCreateDirectProperty_fatal ,
		ZlangDestroyDirectProperty_fatal ,
		
		NULL ,
		NULL ,
		NULL ,
		NULL ,
		
		NULL ,
		NULL ,
		NULL ,
		NULL ,
		NULL ,
		
		NULL ,
		NULL ,
		NULL ,
		NULL ,
		NULL ,
		
		NULL ,
		NULL ,
		NULL ,
		NULL ,
		NULL ,
		NULL ,
		
		NULL ,
		NULL ,
		
		NULL ,
		NULL ,
		NULL ,
		NULL ,
		NULL ,
		
		NULL ,
	} ;

ZlangImportObjectFunction ZlangImportObject_fatal;
struct ZlangObject *ZlangImportObject_fatal( struct ZlangRuntime *rt )
{
	struct ZlangObject	*obj = NULL ;
	struct ZlangFunction	*func = NULL ;
	int			nret = 0 ;
	
	nret = ImportObject( rt , & obj , ZLANG_OBJECT_fatal , & direct_funcs_fatal , sizeof(struct ZlangDirectFunctions) , NULL ) ;
	if( nret )
	{
		SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_LINK_FUNC_TO_ENTITY , "import object to global objects heap" )
		return NULL;
	}
	
	/* fatal.ThrowException(int,string) */
	func = AddFunctionAndParametersInObject( rt , obj , "ThrowException" , "ThrowException(int,string)" , ZlangInvokeFunction_fatal_ThrowException_int_string , ZLANG_OBJECT_int , ZLANG_OBJECT_int,NULL , ZLANG_OBJECT_string,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* fatal.HaveException() */
	func = AddFunctionAndParametersInObject( rt , obj , "HaveException" , "HaveException()" , ZlangInvokeFunction_fatal_HaveException , ZLANG_OBJECT_bool , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* fatal.GetCode() */
	func = AddFunctionAndParametersInObject( rt , obj , "GetCode" , "GetCode()" , ZlangInvokeFunction_fatal_GetCode , ZLANG_OBJECT_int , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* fatal.GetMessage() */
	func = AddFunctionAndParametersInObject( rt , obj , "GetMessage" , "GetMessage()" , ZlangInvokeFunction_fatal_GetMessage , ZLANG_OBJECT_string , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* fatal.GetExceptionSourceFilename() */
	func = AddFunctionAndParametersInObject( rt , obj , "GetExceptionSourceFilename" , "GetExceptionSourceFilename()" , ZlangInvokeFunction_fatal_GetExceptionSourceFilename , ZLANG_OBJECT_string , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* fatal.GetExceptionSourceRow() */
	func = AddFunctionAndParametersInObject( rt , obj , "GetExceptionSourceRow" , "GetExceptionSourceRow()" , ZlangInvokeFunction_fatal_GetExceptionSourceRow , ZLANG_OBJECT_int , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* fatal.GetExceptionSourceColumn() */
	func = AddFunctionAndParametersInObject( rt , obj , "GetExceptionSourceColumn" , "GetExceptionSourceColumn()" , ZlangInvokeFunction_fatal_GetExceptionSourceColumn , ZLANG_OBJECT_int , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* fatal.GetExceptionObjectName() */
	func = AddFunctionAndParametersInObject( rt , obj , "GetExceptionObjectName" , "GetExceptionObjectName()" , ZlangInvokeFunction_fatal_GetExceptionObjectName , ZLANG_OBJECT_string , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* fatal.GetExceptionFunctionName() */
	func = AddFunctionAndParametersInObject( rt , obj , "GetExceptionFunctionName" , "GetExceptionFunctionName()" , ZlangInvokeFunction_fatal_GetExceptionFunctionName , ZLANG_OBJECT_string , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* fatal.GetStackTrace() */
	func = AddFunctionAndParametersInObject( rt , obj , "GetStackTrace" , "GetStackTrace()" , ZlangInvokeFunction_fatal_GetStackTrace , ZLANG_OBJECT_string , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* fatal.CleanException() */
	func = AddFunctionAndParametersInObject( rt , obj , "CleanException" , "CleanException()" , ZlangInvokeFunction_fatal_CleanException , ZLANG_OBJECT_void , NULL ) ;
	if( func == NULL )
		return NULL;
	
	SetRuntimeFunction_fatal_ThrowException( rt , fatal_ThrowException );
	SetRuntimeFunction_fatal_HaveException( rt , fatal_HaveException );
	SetRuntimeFunction_fatal_GetMessage( rt , fatal_GetMessage );
	SetRuntimeFunction_fatal_GetExceptionSourceFilename( rt , fatal_GetExceptionSourceFilename );
	SetRuntimeFunction_fatal_GetExceptionSourceRow( rt , fatal_GetExceptionSourceRow );
	SetRuntimeFunction_fatal_GetExceptionSourceColumn( rt , fatal_GetExceptionSourceColumn );
	SetRuntimeFunction_fatal_GetExceptionObjectName( rt , fatal_GetExceptionObjectName );
	SetRuntimeFunction_fatal_GetExceptionFunctionName( rt , fatal_GetExceptionFunctionName );
	SetRuntimeFunction_fatal_GetStackTrace( rt , fatal_GetStackTrace );
	SetRuntimeFunction_fatal_CleanException( rt , fatal_CleanException );
	
	return obj ;
}

