#include "commonpool.h"

#include <time.h>
#if defined(__linux__)
#include <unistd.h>
#include <pthread.h>
#elif defined(_WIN32)
#include <windows.h>
#endif

#define _ASSISTANT_THREAD		2

struct TestSessionContent
{
	int	dummy ;
} ;

int InitSessionContext( struct CommonPool *pool , void *session_ctx )
{
	struct TestSessionContent	*ctx = (struct TestSessionContent *) session_ctx ;
	
	printf( "TID[%ld] | test_commonpool : InitSessionContext : ctx[%p]\n" , pthread_self() , ctx );
	
	return 0;
}

int WatchingSessions( struct CommonPool *pool )
{
	struct TestSessionContent	*ctx = NULL ;
	
	printf( "TID[%ld] | test_commonpool : enter WatchingSessions\n" , pthread_self() );
	
	LockBeforeTravelIdleCommonPool( pool );
	
	ctx = TravelIdleCommonPoolSession( pool , NULL ) ;
	while( ctx )
	{
		printf( "TID[%ld] | test_commonpool : TravelIdleCommonPoolSession : ctx[%p]\n" , pthread_self() , ctx );
		
		ctx = TravelIdleCommonPoolSession( pool , ctx ) ;
	}
	
	UnlockAfterTravelIdleCommonPool( pool );
	
	printf( "TID[%ld] | test_commonpool : WatchingSessions\n" , pthread_self() );
	
	return 0;
}

int CleanSessionContext( struct CommonPool *pool , void *session_ctx )
{
	struct TestSessionContent	*ctx = (struct TestSessionContent *) session_ctx ;
	
	printf( "TID[%ld] | test_commonpool : CleanSessionContext : ctx[%p]\n" , pthread_self() , ctx );
	
	return 0;
}

struct CommonPoolCallback	g_test_callback =
	{
		sizeof(struct TestSessionContent)
		, & InitSessionContext
#if _ASSISTANT_THREAD
		, & WatchingSessions
#else
		, NULL
#endif
		, & CleanSessionContext
	} ;

int test_commonpool()
{
	struct CommonPool	*pool = NULL ;
	void			*context = NULL ;
	void			*context2 = NULL ;
	int			nret = 0 ;
	
	pool = CreateCommonPool( & g_test_callback , sizeof(struct CommonPoolCallback) ) ;
	if( pool == NULL )
	{
		printf( "TID[%ld] | CreateCommonPool failed\n" , pthread_self() );
		return -1;
	}
	
#if _ASSISTANT_THREAD
	SetCommonPoolMaxIdleTimeval( pool , 1 );
#else
	SetCommonPoolMaxIdleTimeval( pool , 0 );
#endif
	
	SetCommonPoolAssistantSleepTimeval( pool , 1 );
	
	nret = StartCommonPool( pool ) ;
	if( nret )
	{
		printf( "TID[%ld] | StartCommonPool failed[%d]\n" , pthread_self() , nret );
		return -1;
	}
	
	usleep( 1500*1000 );
	
	nret = FetchCommonPoolSession( pool , & context ) ;
	if( nret )
	{
		printf( "TID[%ld] | FetchCommonPoolSession failed[%d]\n" , pthread_self() , nret );
		return -1;
	}
	
	usleep( 1000*1000 );
	
	nret = FetchCommonPoolSession( pool , & context2 ) ;
	if( nret )
	{
		printf( "TID[%ld] | FetchCommonPoolSession failed[%d]\n" , pthread_self() , nret );
		return -1;
	}
	
	usleep( 1000*1000 );
	
	nret = GivebackCommonPoolSession( pool , context ) ;
	if( nret )
	{
		printf( "TID[%ld] | GivebackCommonPoolSession failed[%d]\n" , pthread_self() , nret );
		return -1;
	}
	
	nret = GivebackCommonPoolSession( pool , context2 ) ;
	if( nret )
	{
		printf( "TID[%ld] | GivebackCommonPoolSession failed[%d]\n" , pthread_self() , nret );
		return -1;
	}
	
	usleep( 1000*1000 );
	
	nret = StopCommonPool( pool ) ;
	if( nret )
	{
		printf( "TID[%ld] | StopCommonPool failed[%d]\n" , pthread_self() , nret );
		return -1;
	}
	
	nret = DestroyCommonPool( pool ) ;
	if( nret )
	{
		printf( "TID[%ld] | DestroyCommonPool failed[%d]\n" , pthread_self() , nret );
		return -1;
	}
	
	return 0;
}

int main()
{
	return -test_commonpool();
}

