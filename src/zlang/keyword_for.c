/* Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "zlang_in.h"

int Keyword_for( struct ZlangRuntime *rt )
{
	struct ZlangObject			*init_obj = NULL ;
	struct ZlangInterpretStatementContext	for_expr_interpret_statement_ctx ;
	struct ZlangObject			*logic_obj = NULL ;
	struct ZlangObject			*step_obj = NULL ;
	unsigned char				logic_result ;
	struct ZlangTokenDataUnitHeader		*token_info1 = NULL ;
	char					*token1 = NULL ;
	struct ZlangTokenDataUnitHeader		*token_info2 = NULL ;
	char					*token2 = NULL ;
	struct ZlangTokenDataPageHeader		*for_2_condition_token_datapage_header = NULL ;
	char					*for_2_condition_token_dataunit = NULL ;
	struct ZlangTokenDataPageHeader		*for_3_condition_token_datapage_header = NULL ;
	char					*for_3_condition_token_dataunit = NULL ;
	struct ZlangTokenDataPageHeader		*for_body_token_datapage_header = NULL ;
	char					*for_body_token_dataunit = NULL ;
	struct ZlangTokenDataUnitHeader		*for_body_token_dataunit_header = NULL ;
	/*
	struct ZlangObject			*bak_in_obj = NULL ;
	*/
	struct ZlangFunction			*bak_in_func = NULL ;
	
	int					nret = 0 ;
	
	TEST_RUNTIME_DEBUG_THEN_PRINT_ENTER_FUNCTION(rt)
	
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "for( ... ; . ; . )" )
	TRAVELTOKEN_AND_SAVEINFO( rt , token_info1 , token1 )
	if( token_info1->token_type != TOKEN_TYPE_BEGIN_OF_SUB_EXPRESSION )
	{
		SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_SYNTAX , "expect '(' but '%s'" , rt->travel_token )
		TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
		return ZLANG_ERROR_SYNTAX;
	}
	
	TEST_RUNTIME_DEBUG( rt )
	{
		TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
		PRINT_TABS_AND_FORMAT( rt , "CALL InterpretExpression for first expression , last_token[%s][%s]" , _zlang_token_type_str , rt->travel_token )
	}
	memset( & for_expr_interpret_statement_ctx , 0x00 , sizeof(struct ZlangInterpretStatementContext) );
	for_expr_interpret_statement_ctx.token_of_expression_end1 = TOKEN_TYPE_END_OF_STATEMENT ;
	nret = InterpretExpression( rt , & for_expr_interpret_statement_ctx , & init_obj ) ;
	TEST_RUNTIME_DEBUG( rt )
	{
		TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
		TEST_RUNTIME_DEBUG( rt ) { PRINT_TABS(rt) printf( "InterpretExpression for first expression return[%d] last_token[%s][%s] " , nret , _zlang_token_type_str , rt->travel_token ); DebugPrintObject(rt,logic_obj); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE }
	}
	if( nret != ZLANG_INFO_END_OF_EXPRESSION )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
		return nret;
	}
	
	for_2_condition_token_datapage_header = rt->travel_token_datapage_header ;
	for_2_condition_token_dataunit = rt->travel_token_dataunit ;
	
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "for( . ; ... ; . )" )
	TEST_RUNTIME_DEBUG( rt )
	{
		TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
		PRINT_TABS_AND_FORMAT( rt , "CALL InterpretExpression for second expression , last_token[%s][%s]" , _zlang_token_type_str , rt->travel_token )
	}
	memset( & for_expr_interpret_statement_ctx , 0x00 , sizeof(struct ZlangInterpretStatementContext) );
	for_expr_interpret_statement_ctx.token_of_expression_end1 = TOKEN_TYPE_END_OF_STATEMENT ;
	nret = InterpretExpression( rt , & for_expr_interpret_statement_ctx , & logic_obj ) ;
	TEST_RUNTIME_DEBUG( rt )
	{
		TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
		TEST_RUNTIME_DEBUG( rt ) { PRINT_TABS(rt) printf( "InterpretExpression for second expression return[%d] last_token[%s][%s] " , nret , _zlang_token_type_str , rt->travel_token ); DebugPrintObject(rt,logic_obj); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE }
	}
	if( nret != ZLANG_INFO_END_OF_EXPRESSION )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
		return nret;
	}
	
	for_3_condition_token_datapage_header = rt->travel_token_datapage_header ;
	for_3_condition_token_dataunit = rt->travel_token_dataunit ;
	
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "for( . ; . ; skip... )" )
	nret = SkipExpression( rt , TOKEN_TYPE_END_OF_SUB_EXPRESSION , 0 , 0 ) ; /* for( . , true , ... ) */
	if( nret != ZLANG_INFO_END_OF_EXPRESSION )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
		return nret;
	}
	
	for_body_token_datapage_header = rt->travel_token_datapage_header ;
	for_body_token_dataunit = rt->travel_token_dataunit ;
	
_GOTO_RETRY_JUDGE_FOR_SECOND_CONDITION :
	if( logic_obj )
	{
		nret = IsObjectTrueValue( rt , logic_obj , & logic_result ) ;
		if( nret )
		{
			SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , nret , "IsObjectTrueValue failed" )
			return nret;
		}
	}
	else
	{
		logic_result = 1 ;
	}
	
	if( logic_result )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "for( . ; ture ; . ) ..." )
		
		PEEKTOKEN_AND_SAVEINFO( rt , token_info2 , token2 )
		if( token_info2->token_type == TOKEN_TYPE_BEGIN_OF_STATEMENT_SEGMENT )
		{
			for_body_token_dataunit_header = token_info2 ;
			NEXTTOKEN( rt )
			
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "CALL InterpretStatementSegment" )
			IncreaseStackFrame( rt , NULL );
			/*
			bak_in_obj = rt->in_obj ;
			rt->in_obj = NULL ;
			*/
			bak_in_func = rt->in_func ;
			rt->in_func = NULL ;
			nret = InterpretStatementSegment( rt , NULL ) ;
			/*
			rt->in_obj = bak_in_obj ;
			*/
			rt->in_func = bak_in_func ;
			DecreaseStackFrame( rt );
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "InterpretStatementSegment return[%d]" , nret )
			if( nret == ZLANG_INFO_END_OF_STATEMENT_SEGMENT )
			{
				;
			}
			else if( nret == ZLANG_INFO_CONTINUE )
			{
				rt->travel_token_datapage_header = for_body_token_dataunit_header->p1 ;
				rt->travel_token_dataunit = for_body_token_dataunit_header->p2 ;
				NEXTTOKEN( rt )
			}
			else if( nret == ZLANG_INFO_BREAK )
			{
				rt->travel_token_datapage_header = for_body_token_dataunit_header->p1 ;
				rt->travel_token_dataunit = for_body_token_dataunit_header->p2 ;
				NEXTTOKEN( rt )
				
				TEST_RUNTIME_DEBUG_THEN_PRINT_LEAVE_FUNCTION(rt)
				return ZLANG_INFO_END_OF_STATEMENT;
			}
			else
			{
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return nret;
			}
		}
		else
		{
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "CALL InterpretStatement" )
			nret = InterpretStatement( rt ) ;
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "InterpretStatement return[%d]" , nret )
			if( nret == ZLANG_INFO_END_OF_STATEMENT )
			{
				;
			}
			else if( nret == ZLANG_INFO_CONTINUE )
			{
				rt->travel_token_datapage_header = for_body_token_datapage_header ;
				rt->travel_token_dataunit = for_body_token_dataunit ;
				nret = SkipStatement( rt ) ;
				if( nret != ZLANG_INFO_END_OF_STATEMENT )
				{
					TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
					return nret;
				}
			}
			else if( nret == ZLANG_INFO_BREAK )
			{
				rt->travel_token_datapage_header = for_body_token_datapage_header ;
				rt->travel_token_dataunit = for_body_token_dataunit ;
				nret = SkipStatement( rt ) ;
				if( nret != ZLANG_INFO_END_OF_STATEMENT )
				{
					TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
					return nret;
				}
				
				TEST_RUNTIME_DEBUG_THEN_PRINT_LEAVE_FUNCTION(rt)
				return ZLANG_INFO_END_OF_STATEMENT;
			}
			else
			{
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return nret;
			}
		}
		
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "rego for( . ; . ; ... )" )
		rt->travel_token_datapage_header = for_3_condition_token_datapage_header ;
		rt->travel_token_dataunit = for_3_condition_token_dataunit ;
		
		TEST_RUNTIME_DEBUG( rt )
		{
			TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
			PRINT_TABS_AND_FORMAT( rt , "CALL InterpretExpression for third expression , last_token[%s][%s]" , _zlang_token_type_str , rt->travel_token )
		}
		memset( & for_expr_interpret_statement_ctx , 0x00 , sizeof(struct ZlangInterpretStatementContext) );
		for_expr_interpret_statement_ctx.token_of_expression_end1 = TOKEN_TYPE_END_OF_SUB_EXPRESSION ;
		nret = InterpretExpression( rt , & for_expr_interpret_statement_ctx , & step_obj ) ;
		TEST_RUNTIME_DEBUG( rt )
		{
			TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
			TEST_RUNTIME_DEBUG( rt ) { PRINT_TABS(rt) printf( "InterpretExpression for third expression return[%d] last_token[%s][%s] " , nret , _zlang_token_type_str , rt->travel_token ); DebugPrintObject(rt,step_obj); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE }
		}
		if( nret != ZLANG_INFO_END_OF_EXPRESSION )
		{
			TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
			return nret;
		}
		
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "rego for( . ; ... ; . )" )
		rt->travel_token_datapage_header = for_2_condition_token_datapage_header ;
		rt->travel_token_dataunit = for_2_condition_token_dataunit ;
		
		TEST_RUNTIME_DEBUG( rt )
		{
			TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
			PRINT_TABS_AND_FORMAT( rt , "CALL InterpretExpression for second expression , last_token[%s][%s]" , _zlang_token_type_str , rt->travel_token )
		}
		memset( & for_expr_interpret_statement_ctx , 0x00 , sizeof(struct ZlangInterpretStatementContext) );
		for_expr_interpret_statement_ctx.token_of_expression_end1 = TOKEN_TYPE_END_OF_STATEMENT ;
		nret = InterpretExpression( rt , & for_expr_interpret_statement_ctx , & logic_obj ) ;
		TEST_RUNTIME_DEBUG( rt )
		{
			TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
			TEST_RUNTIME_DEBUG( rt ) { PRINT_TABS(rt) printf( "InterpretExpression for second expression return[%d] last_token[%s][%s] " , nret , _zlang_token_type_str , rt->travel_token ); DebugPrintObject(rt,logic_obj); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE }
		}
		if( nret != ZLANG_INFO_END_OF_EXPRESSION )
		{
			TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
			return nret;
		}
		
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "rego for( . ; . ; . ) ..." )
		rt->travel_token_datapage_header = for_body_token_datapage_header ;
		rt->travel_token_dataunit = for_body_token_dataunit ;
		
		goto _GOTO_RETRY_JUDGE_FOR_SECOND_CONDITION;
	}
	else
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "for( . ; false ; . ) skip..." )
		
		PEEKTOKEN_AND_SAVEINFO( rt , token_info2 , token2 )
		if( token_info2->token_type == TOKEN_TYPE_BEGIN_OF_STATEMENT_SEGMENT )
		{
			NEXTTOKEN( rt )
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "for( . ; false ; . ) { skip.statement_segment }" )
			
			nret = SkipStatementSegment( rt ) ;
			if( nret != ZLANG_INFO_END_OF_STATEMENT_SEGMENT )
			{
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return nret;
			}
		}
		else
		{
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "for( . ; false ; . ) skip.statement" )
			
			nret = SkipStatement( rt ) ;
			if( nret != ZLANG_INFO_END_OF_STATEMENT )
			{
				TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
				return nret;
			}
		}
	}
	
	TEST_RUNTIME_DEBUG_THEN_PRINT_LEAVE_FUNCTION(rt)
	return ZLANG_INFO_END_OF_STATEMENT;
}

int SkipStatement_for( struct ZlangRuntime *rt )
{
	struct ZlangTokenDataUnitHeader	*token_info1 = NULL ;
	char				*token1 = NULL ;
	struct ZlangTokenDataUnitHeader	*token_info2 = NULL ;
	char				*token2 = NULL ;
	
	int				nret = 0 ;
	
	TEST_RUNTIME_DEBUG_THEN_PRINT_ENTER_FUNCTION(rt)
	
	TRAVELTOKEN_AND_SAVEINFO( rt , token_info1 , token1 )
	if( token_info1->token_type != TOKEN_TYPE_BEGIN_OF_SUB_EXPRESSION )
	{
		SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_SYNTAX , "expect '(' but '%s'" , token1 )
		TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
		return ZLANG_ERROR_SYNTAX;
	}
	
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "for( ... ; . ; . )" )
	nret = SkipExpression( rt , TOKEN_TYPE_END_OF_STATEMENT , 0 , 0 ) ;
	if( nret != ZLANG_INFO_END_OF_EXPRESSION )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
		return nret;
	}
	
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "for( . ; ... ; . )" )
	nret = SkipExpression( rt , TOKEN_TYPE_END_OF_STATEMENT , 0 , 0 ) ;
	if( nret != ZLANG_INFO_END_OF_EXPRESSION )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
		return nret;
	}
	
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "for( . ; . ; ... )" )
	nret = SkipExpression( rt , TOKEN_TYPE_END_OF_SUB_EXPRESSION , 0 , 0 ) ;
	if( nret != ZLANG_INFO_END_OF_EXPRESSION )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
		return nret;
	}
	
	PEEKTOKEN_AND_SAVEINFO( rt , token_info2 , token2 )
	if( token_info2->token_type == TOKEN_TYPE_BEGIN_OF_STATEMENT_SEGMENT )
	{
		NEXTTOKEN( rt )
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "for( . ; . ; . ) { skip.statement_segment... }" )
		
		nret = SkipStatementSegment( rt ) ;
		if( nret != ZLANG_INFO_END_OF_STATEMENT_SEGMENT )
		{
			TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
			return nret;
		}
	}
	else
	{
		NEXTTOKEN( rt )
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "for( . ; . ; . ) skip.statement..." )
		
		nret = SkipStatement( rt ) ;
		if( nret != ZLANG_INFO_END_OF_STATEMENT )
		{
			TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
			return nret;
		}
	}
	
	TEST_RUNTIME_DEBUG_THEN_PRINT_LEAVE_FUNCTION(rt)
	return ZLANG_INFO_END_OF_STATEMENT;
}

