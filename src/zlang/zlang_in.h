/* Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef _H_ZLANG_IN_
#define _H_ZLANG_IN_

/*
printf( "LIHUA - %d:%zu - %s:%d:%s\n" , getpid() , (size_t)pthread_self() , __FILE__,__LINE__,__FUNCTION__ ); fflush(stdout);
{ struct ZlangTokenDataUnitHeader *token_info = NULL ; char *token = NULL ; PEEKTOKEN( token_info , token ) } fflush(stdout);
*/

#include "list.h"
#include "rbtree.h"

#include "token.h"
#include "aodatapage.h"

#include "zlang.h"

#include "charset_GB18030.h"
#include "charset_UTF8.h"

extern TLS  char	_zlang_token_type_str1[4+1] ;
extern TLS  char	_zlang_token_type_str2[4+1] ;
extern TLS  char	_zlang_token_type_str3[4+1] ;
extern TLS  char	_zlang_token_type_str[4+1] ;

extern TLS struct ZlangRuntime		*_zlang_rt ;

extern TLS struct StackFrameInfo	*_alart_stack_frames_info ;
extern TLS size_t			_alart_stack_frames_count ;
extern TLS size_t			_alart_stack_frames_no ;
extern TLS signed long			_alart_stack_depth ;

struct ZlangMutexLock
{
	volatile long		refer_count ; /* win32 needed 'long' type */
	MUTEX			lock ;
} ;

struct ZlangSourceFileDataPageHeader
{
	char			*datapage ;
	struct list_head	this_source_file_datapage ;
} ;

#define ZLANG_TOKEN_IDENTIFICATION_NO_ALIASED	(void*)1

struct ZlangTokenDataUnitHeader
{
	char			*source_filename ;
	int32_t			source_row ;
	int32_t			source_col ;
	
	enum TokenType		token_type ;
	int32_t			token_len ;
	
	void			*p1 ;
	void			*p2 ;
	int			n3 ;
	void			*p7 ;
	void			*p8 ;
} ;

struct ZlangTokenDataPageHeader
{
	char			*datapage ;
	struct list_head	this_token_datapage ;
} ;

struct ZlangFunctionParameter
{
	char				*parent_obj_name ;
	char				*obj_name ;
	
	struct list_head		this_param ;
} ;

#define ZLANG_FUNCNAME_ADDPROPERTY		"AddProperty"
#define ZLANG_FULLFUNCNAME_ADDPROPERTY		"AddProperty(object,string)"
#define ZLANG_FUNCNAME_REFERPROPERTY		"ReferProperty"
#define ZLANG_FULLFUNCNAME_REFERPROPERTY	"ReferProperty(string)"
#define ZLANG_FUNCNAME_REMOVEPROPERTY		"RemoveProperty"
#define ZLANG_FULLFUNCNAME_REMOVEPROPERTY	"RemoveProperty(string)"

#define FUNC_IS_SYNCHRONIZE_INIT_AND_ENABLE	1
#define FUNC_IS_SYNCHRONIZE_ENABLE		2

struct ZlangFunction
{
	char				*func_name ;
	char				*full_func_name ;
	struct list_head		in_params ; /* struct ZlangFunctionParameter */
	struct ZlangFunctionParameter	*out_param ;
	
	unsigned char			access_qualifier ;
	unsigned char			is_interceptor ;
	
	ZlangInvokeFunction		*invoke_func ;
	
	struct ZlangTokenDataPageHeader	*func_begin_token_datapage_header ;
	char				*func_begin_token_dataunit ;
	struct ZlangTokenDataPageHeader	*func_end_over_token_datapage_header ;
	char				*func_end_over_token_dataunit ;
	
	struct ZlangMutexLock		*synchronize_lock ;
	
	struct rb_node			this_func ;
} ;

struct ZlangFunctionsEntity
{
	struct ZlangFunctionsEntity	*ancestor_funcs_enti ;
	
	struct ZlangDirectFunctions	*direct_funcs ;
	struct rb_root			funcs ; /* struct ZlangFunction */
	
	volatile long			refer_count ; /* win32 needed 'long' type */
	
	struct ZlangFunction		*constractor_cache ;
	struct ZlangFunction		*destractor_cache ;
	
	struct list_head		funcs_enti_cache_node ;
} ;

struct ZlangPropertiesEntity
{
	void				*direct_prop ;
	struct rb_root			props ; /* tree node for struct ZlangObject.struct ZlangObject.obj_rbtree_node */
	struct list_head		props_list ; /* list node for struct ZlangObject.obj_list_node */
	
	volatile long			refer_count ; /* win32 needed 'long' type */
	
	unsigned char			had_constracted ;
	unsigned char			had_destracted ;
	
	struct list_head		props_enti_cache_node ;
} ;

struct ZlangObject
{
	char				*clone_obj_name ;
	char				*obj_name ;
	
	char				*embellish_name ;
	char				*embellish2_name ;
	
	unsigned char			access_qualifier ;
	unsigned char			is_constant ;
	struct ZlangIntercept		*intercept_set ;
	struct ZlangIntercept		*intercept_get ;
	
	struct ZlangMutexLock		*atomic_lock ;
	
	struct ZlangFunctionsEntity	*funcs_enti ;
	struct ZlangPropertiesEntity	*props_enti ;
	
	struct rb_node			obj_rbtree_node ; /* tree node for struct ZlangRuntime.global_objs_heap or struct ZlangPropertiesEntity.props */
	struct list_head		obj_list_node ; /* list node for struct ZlangPropertiesEntity.props_list */
	
	struct list_head		obj_order_imported_node ; /* list node for ZlangRuntime.global_order_imported_objs_list */
	struct list_head		obj_cache_node ; /* list node for ZlangRuntime.objs_cache */
} ;

struct ZlangObjectsStack
{
	struct ZlangObject		*objs_stack ;
	int				objs_stack_size ;
	int				increase_size ;
} ;

/*
                                                               LOCAL int c <- println: stack_in_params_top
                                                               OUT int     <- println: stack_bottom
                                                               IN int b
              <- main: stack_in_params_top (stack_length)      IN int a    <- println: stack_out_params_top
OUT int       <- main: stack_bottom
IN Array args <- main: stack_out_params_top
*/

#define ZLANG_FULLFUNCNAME_MAIN		"main(array)"

#define ZLANG_FUNCNAME_EXIT		"exit"
#define ZLANG_FULLFUNCNAME_EXIT		"exit(int)"

#define ZLANG_FUNCNAME_EXITTHREAD	"ExitThread"
#define ZLANG_FULLFUNCNAME_EXITTHREAD	"ExitThread(...)"

struct ZlangObjectsStackFrame
{
	int				stack_local_var_top ;
	int				stack_out_params_top ;
	int				stack_in_params_top ;
	int				stack_bottom ;
	
	struct ZlangTokenDataUnitHeader	*caller_token_info ;
	struct ZlangTokenDataUnitHeader	*callee_token_info ;
	unsigned char			is_in_try ;
	
	char				*in_obj_name ;
	char				*full_func_name ;
} ;

struct ZlangIncludeFile
{
	char				include_filename[ PATH_MAX ] ;
	
	struct list_head		this_include_file ;
} ;

struct ZlangImportFile
{
	char				import_filename[ PATH_MAX ] ;
#if ( defined __linux ) || ( defined __unix )
	void				*open_handler ;
#elif ( defined _WIN32 )
	HMODULE				open_handler ;
#endif

	struct list_head		this_import_file ;
} ;

#define ZLANG_LOCAL_OBJECTS_STACK_SIZE_DEFAULT			256
#define ZLANG_LOCAL_OBJECTS_STACK_INCREASE_SIZE			256
#define ZLANG_LOCAL_OBJECTS_STACK_INFO_ARRAY_COUNT_DEFAULT	16
#define ZLANG_LOCAL_OBJECTS_STACK_INFO_ARRAY_INCREASE_COUNT	16

#define ZLANG_TMP_OBJECTS_STACK_SIZE_DEFAULT			256
#define ZLANG_TMP_OBJECTS_STACK_INCREASE_SIZE			256
#define ZLANG_TMP_OBJECTS_STACK_INFO_ARRAY_COUNT_DEFAULT	16
#define ZLANG_TMP_OBJECTS_STACK_INFO_ARRAY_INCREASE_COUNT	16

#define ZLANG_DEFERS_STACK_SIZE_DEFAULT				32
#define ZLANG_DEFERS_STACK_INCREASE_SIZE			32
#define ZLANG_DEFERS_STACK_INFO_ARRAY_COUNT_DEFAULT		16
#define ZLANG_DEFERS_STACK_INFO_ARRAY_INCREASE_COUNT		16

#define GET_RUNTIME_ERROR_NO(_rt_ptr_)				( (_rt_ptr_)->runtime_error_no )

#define PRINT_RUNTIME_ERROR(_rt_ptr_,_runtime_error_level_) \
	TEST_RUNTIME_LEVEL(_rt_ptr_,_runtime_error_level_) \
	{ \
		printf( "*** %s %s:%"PRIi32",%"PRIi32":'%s' %d: %s" \
			, _zlang_runtime_error_level[(_rt_ptr_)->runtime_error_level] \
			, (_rt_ptr_)->error_token_info ? (_rt_ptr_)->error_token_info->source_filename : "_" \
			, (_rt_ptr_)->error_token_info ? (_rt_ptr_)->error_token_info->source_row : 0 \
			, (_rt_ptr_)->error_token_info ? (_rt_ptr_)->error_token_info->source_col : 0 \
			, (_rt_ptr_)->error_token ? (_rt_ptr_)->error_token : "" \
			, (_rt_ptr_)->runtime_error_no , (_rt_ptr_)->runtime_error_string ); \
		if( (_rt_ptr_)->debug_error_level == RUNTIME_DEBUG ) \
			printf( " (%s:%d)" \
				, (_rt_ptr_)->debug_internal_file \
				, (_rt_ptr_)->debug_internal_lineno ); \
		printf( "\n" ); \
	} \

#define ZLANG_RUNTIME_ERROR_STRING_SIZE		256

struct ZlangInterpretStatementContext
{
	enum TokenType	token_of_expression_end1 ;
	enum TokenType	token_of_expression_end2 ;
	enum TokenType	token_of_expression_end3 ;
	unsigned char	has_const_decorated ;
	unsigned char	has_access_qualifier ;
	unsigned char	create_new_object_use_existed_name ;
	unsigned char	has_atomic_decorated ;
	unsigned char	has_sync_decorated ;
	unsigned char	is_in_throw ;
	unsigned char	is_in_uncatch ;
} ;

enum GetExpressionLeftOrRight
{
	GET_EXPRESSION_LEFT = 1 ,
	GET_EXPRESSION_RIGHT = 1 ,
} ;

struct ZlangInterpretExpressionContext
{
	enum GetExpressionLeftOrRight	get_expr_left_or_right ;
} ;

struct ZlangRuntimeFrequentObject
{
	struct ZlangObject	*zobject_obj ;
	struct ZlangObject	*zruntime_obj ;
	struct ZlangObject	*error_obj ;
	struct ZlangObject	*fatal_obj ;
	
	struct ZlangObject	*string_obj ;
	
	struct ZlangObject	*bool_obj ;
	
	struct ZlangObject	*short_obj ;
	struct ZlangObject	*ushort_obj ;
	struct ZlangObject	*int_obj ;
	struct ZlangObject	*uint_obj ;
	struct ZlangObject	*long_obj ;
	struct ZlangObject	*ulong_obj ;
	
	struct ZlangObject	*float_obj ;
	struct ZlangObject	*double_obj ;
	
	struct ZlangObject	*array_obj ;
	struct ZlangObject	*list_obj ;
	struct ZlangObject	*list_node_obj ;
	struct ZlangObject	*map_obj ;
	struct ZlangObject	*iterator_obj ;
	struct ZlangObject	*functionptr_obj ;
} ;

struct ZlangRuntimeInvokeFunctions
{
	ZlangInvokeFunction					*string_AppendFormatFromArgsStack_func ;
} ;

struct ZlangRuntimeDirectFunctions
{
	ZlangDirectFunction_string_Clear			*string_Clear_func ;
	ZlangDirectFunction_string_PrepareBuffer		*string_PrepareBuffer_func ;
	ZlangDirectFunction_string_GetDirectPropertiesPtr	*string_GetDirectPropertiesPtr_func ;
	ZlangDirectFunction_array_Append			*array_Append_func ;
	ZlangDirectFunction_array_InsertBefore			*array_InsertBefore_func ;
	ZlangDirectFunction_array_Remove			*array_Remove_func ;
	ZlangDirectFunction_array_RemoveAll			*array_RemoveAll_func ;
	ZlangDirectFunction_array_Length			*array_Length_func ;
	ZlangDirectFunction_array_Get				*array_Get_func ;
	ZlangDirectFunction_array_Set				*array_Set_func ;
	
	ZlangDirectFunction_list_AddHead			*list_AddHead_func ;
	ZlangDirectFunction_list_AddTail			*list_AddTail_func ;
	ZlangDirectFunction_list_RemoveAll			*list_RemoveAll_func ;
	ZlangDirectFunction_list_GetHead			*list_GetHead_func ;
	ZlangDirectFunction_list_GetTail			*list_GetTail_func ;
	ZlangDirectFunction_list_Length				*list_Length_func ;
	ZlangDirectFunction_list_IsEmpty			*list_IsEmpty_func ;
	ZlangDirectFunction_list_IsNotEmpty			*list_IsNotEmpty_func ;
	ZlangDirectFunction_list_node_TravelNext		*list_node_TravelNext_func ;
	ZlangDirectFunction_list_node_TravelPrev		*list_node_TravelPrev_func ;
	ZlangDirectFunction_list_node_IsTravelOver		*list_node_IsTravelOver_func ;
	ZlangDirectFunction_list_node_Remove			*list_node_Remove_func ;
	ZlangDirectFunction_list_node_GetMember			*list_node_GetMember_func ;
	
	ZlangDirectFunction_map_Put				*map_Put_func ;
	ZlangDirectFunction_map_Get				*map_Get_func ;
	ZlangDirectFunction_map_TravelNextKey			*map_TravelNextKey_func ;
	ZlangDirectFunction_map_TravelPrevKey			*map_TravelPrevKey_func ;
	ZlangDirectFunction_map_UpdateKey			*map_UpdateKey_func ;
	ZlangDirectFunction_map_Remove				*map_Remove_func ;
	ZlangDirectFunction_map_RemoveAll			*map_RemoveAll_func ;
	ZlangDirectFunction_map_Length				*map_Length_func ;
	
	ZlangDirectFunction_iterator_TravelFirst		*iterator_TravelFirst_func ;
	ZlangDirectFunction_iterator_TravelLast			*iterator_TravelLast_func ;
	ZlangDirectFunction_iterator_TravelNext			*iterator_TravelNext_func ;
	ZlangDirectFunction_iterator_TravelPrev			*iterator_TravelPrev_func ;
	ZlangDirectFunction_iterator_IsTravelOver		*iterator_IsTravelOver_func ;
	ZlangDirectFunction_iterator_GetElement			*iterator_GetElement_func ;
	ZlangDirectFunction_iterator_Remove			*iterator_Remove_func ;
	ZlangDirectFunction_iterator_Length			*iterator_Length_func ;
	
	ZlangDirectFunction_json_StringToObject			*json_StringToObject_func ;
	ZlangDirectFunction_json_StringToEntityObject		*json_StringToEntityObject_func ;
	ZlangDirectFunction_json_ObjectToString			*json_ObjectToString_func ;
	
	ZlangDirectFunction_xml_StringToObject			*xml_StringToObject_func ;
	ZlangDirectFunction_xml_StringToEntityObject		*xml_StringToEntityObject_func ;
	ZlangDirectFunction_xml_ObjectToString			*xml_ObjectToString_func ;
	
	ZlangDirectFunction_string_SetStringValue		*string_SetStringValue_func ;
	ZlangDirectFunction_string_FormatStringValue		*string_FormatStringValue_func ;
	ZlangDirectFunction_string_AppendStringValue		*string_AppendStringValue_func ;
	ZlangDirectFunction_string_AppendFormatStringValue	*string_AppendFormatStringValue_func ;
	ZlangDirectFunction_string_ExpandEnvironmentVar		*string_ExpandEnvironmentVar_func ;
	ZlangDirectFunction_string_GetStringValue		*string_GetStringValue_func ;
	ZlangDirectFunction_bool_SetBoolValue			*bool_SetBoolValue_func ;
	ZlangDirectFunction_bool_GetBoolValue			*bool_GetBoolValue_func ;
	ZlangDirectFunction_short_SetShortValue			*short_SetShortValue_func ;
	ZlangDirectFunction_short_GetShortValue			*short_GetShortValue_func ;
	ZlangDirectFunction_ushort_SetUShortValue		*ushort_SetUShortValue_func ;
	ZlangDirectFunction_ushort_GetUShortValue		*ushort_GetUShortValue_func ;
	ZlangDirectFunction_int_SetIntValue			*int_SetIntValue_func ;
	ZlangDirectFunction_int_GetIntValue			*int_GetIntValue_func ;
	ZlangDirectFunction_uint_SetUIntValue			*uint_SetUIntValue_func ;
	ZlangDirectFunction_uint_GetUIntValue			*uint_GetUIntValue_func ;
	ZlangDirectFunction_long_SetLongValue			*long_SetLongValue_func ;
	ZlangDirectFunction_long_GetLongValue			*long_GetLongValue_func ;
	ZlangDirectFunction_ulong_SetULongValue			*ulong_SetULongValue_func ;
	ZlangDirectFunction_ulong_GetULongValue			*ulong_GetULongValue_func ;
	ZlangDirectFunction_float_SetFloatValue			*float_SetFloatValue_func ;
	ZlangDirectFunction_float_GetFloatValue			*float_GetFloatValue_func ;
	ZlangDirectFunction_double_SetDoubleValue		*double_SetDoubleValue_func ;
	ZlangDirectFunction_double_GetDoubleValue		*double_GetDoubleValue_func ;
	ZlangDirectFunction_functionptr_SetFunctionPtr		*functionptr_SetFunctionPtr_func ;
	ZlangDirectFunction_functionptr_GetFunctionPtr		*functionptr_GetFunctionPtr_func ;
	ZlangDirectFunction_functionptr_SetObjectPtr		*functionptr_SetObjectPtr_func ;
	ZlangDirectFunction_functionptr_GetObjectPtr		*functionptr_GetObjectPtr_func ;
	
	ZlangDirectFunction_mutex_GetMutexPtr			*mutex_GetMutexPtr_func ;
	ZlangDirectFunction_condsig_GetMutexPtr			*condsig_GetMutexPtr_func ;
	
	ZlangDirectFunction_error_ThrowException		*error_ThrowException_func ;
	ZlangDirectFunction_error_HaveException			*error_HaveException_func ;
	ZlangDirectFunction_error_GetCode			*error_GetCode_func ;
	ZlangDirectFunction_error_GetMessage			*error_GetMessage_func ;
	ZlangDirectFunction_error_GetExceptionSourceFilename	*error_GetExceptionSourceFilename_func ;
	ZlangDirectFunction_error_GetExceptionSourceRow		*error_GetExceptionSourceRow_func ;
	ZlangDirectFunction_error_GetExceptionSourceColumn	*error_GetExceptionSourceColumn_func ;
	ZlangDirectFunction_error_GetExceptionObjectName	*error_GetExceptionObjectName_func ;
	ZlangDirectFunction_error_GetExceptionFunctionName	*error_GetExceptionFunctionName_func ;
	ZlangDirectFunction_error_GetStackTrace			*error_GetStackTrace_func ;
	ZlangDirectFunction_error_CleanException		*error_CleanException_func ;
	
	ZlangDirectFunction_fatal_ThrowException		*fatal_ThrowException_func ;
	ZlangDirectFunction_fatal_HaveException			*fatal_HaveException_func ;
	ZlangDirectFunction_fatal_GetCode			*fatal_GetCode_func ;
	ZlangDirectFunction_fatal_GetMessage			*fatal_GetMessage_func ;
	ZlangDirectFunction_fatal_GetExceptionSourceFilename	*fatal_GetExceptionSourceFilename_func ;
	ZlangDirectFunction_fatal_GetExceptionSourceRow		*fatal_GetExceptionSourceRow_func ;
	ZlangDirectFunction_fatal_GetExceptionSourceColumn	*fatal_GetExceptionSourceColumn_func ;
	ZlangDirectFunction_fatal_GetExceptionObjectName	*fatal_GetExceptionObjectName_func ;
	ZlangDirectFunction_fatal_GetExceptionFunctionName	*fatal_GetExceptionFunctionName_func ;
	ZlangDirectFunction_fatal_GetStackTrace			*fatal_GetStackTrace_func ;
	ZlangDirectFunction_fatal_CleanException		*fatal_CleanException_func ;
} ;

#define ZLANG_INTERCEPT_BEFORE		1
#define ZLANG_INTERCEPT_AFTER		2
#define ZLANG_INTERCEPT_SET		3
#define ZLANG_INTERCEPT_GET		4
#define ZLANG_before_STRING		"before"
#define ZLANG_after_STRING		"after"
#define ZLANG_set_STRING		"set"
#define ZLANG_get_STRING		"get"
extern char *_zlang_opportunity[5] ;

struct ZlangIntercept
{
	unsigned char			opportunity ;
	
	unsigned char			need_obj_matching ;
	unsigned char			need_func_matching ;
	char				*obj_name ;
	char				*func_name ;
	
	struct ZlangFunction		*func ;
	
	struct list_head		this_intercept ;
} ;

struct ZlangDefer
{
	struct ZlangTokenDataPageHeader	*defer_begin_token_datapage_header ;
	char				*defer_begin_token_dataunit ;
} ;

struct ZlangDefersStack
{
	struct ZlangDefer		*defers_stack ;
	int				defers_stack_size ;
	int				increase_size ;
} ;

struct ZlangDefersStackFrame
{
	int				stack_top ;
	int				stack_bottom ;
	
	struct ZlangTokenDataUnitHeader	*caller_token_info ;
	char				*in_obj_name ;
	char				*full_func_name ;
} ;

struct ZlangInterface
{
	char				*interf_name ;
	
	struct ZlangObject		*inner ;
	
	struct rb_node			interf_rbtree_node ; /* tree node for struct ZlangRuntime.global_interfaces */
} ;

#define ZLANG_MAX_STACK_SIZE		"8388608"
// #define ZLANG_MAX_STACK_SIZE		"1000000"
// #define ZLANG_MAX_STACK_SIZE		"40000"

#if defined(__linux__)
#define INIT_STACK_FRAMES_CONTEXT	
#elif defined(_WIN32)
#define INIT_STACK_FRAMES_CONTEXT	memset( & thread_context , 0x00 , sizeof(CONTEXT) ); RtlCaptureContext( & thread_context );
#endif

#define CHECK_ALART_STACK_DEPTH(_stack_bottom_,_stack_top_)	\
	/* { \
		_alart_stack_depth = (signed long)((char*)(_stack_bottom_)-(char*)(_stack_top_)) ; \
		if( _alart_stack_depth > _zlang_rt->zlang_alart_stack_depth ) \
		{ \
			CONTEXT			thread_context ; \
			\
			printf( "zlang stack overflow\n" ); \
			\
			printf( "--- stack info ---------\n" ); \
			printf( "stack top[%ld] crossed alart value[%ld]\n" , _alart_stack_depth , _zlang_rt->zlang_alart_stack_depth ); \
			\
			INIT_STACK_FRAMES_CONTEXT \
			_alart_stack_frames_info = GetStackFramesInfo( & thread_context , & _alart_stack_frames_count ) ; \
			if( _alart_stack_frames_info == NULL ) \
			{ \
				printf( "GetStackFramesInfo failed\n" ); \
				exit(4); \
			} \
			\
			printf( "--- stack frames ---------\n" ); \
			for( _alart_stack_frames_no = 0 ; _alart_stack_frames_no < _alart_stack_frames_count ; _alart_stack_frames_no++ ) \
			{ \
				printf( "frame %zu : %s (%s:%zu) top[%ld] depth[%ld]\n" , _alart_stack_frames_no , _alart_stack_frames_info[_alart_stack_frames_no].func_name , _alart_stack_frames_info[_alart_stack_frames_no].source_filename , _alart_stack_frames_info[_alart_stack_frames_no].source_fileline , (signed long)(_stack_bottom_)-_alart_stack_frames_info[_alart_stack_frames_no].stack_top , _alart_stack_frames_info[_alart_stack_frames_no].stack_depth ); \
			} \
			\
			exit(4); \
		} \
	} */

struct ZlangRuntime
{
	char							*zlang_stack_bottom ;

	int							debug_error_level ;
	char							debug_error_level_str[ 5 + 1 ] ;
	int							debug_recursive_depth ;
	
	char							*charset_str ;
	uint16_t						charset ;
	struct rb_root						global_charset_alias ; /* struct ZlangCharsetAlias */
	
	char							first_z_file_path[ PATH_MAX ] ;
	
	uint64_t						source_row ;
	uint64_t						source_col ;
	
	struct ZlangObject					*in_obj ;
	struct ZlangFunction					*in_func ;
	struct ZlangTokenDataPageHeader				*travel_token_datapage_header ;
	char							*travel_token_dataunit ;
	struct ZlangTokenDataUnitHeader				*travel_token_info ;
	char							*travel_token ;
	
	struct ZlangTokenDataUnitHeader				*error_token_info ;
	char							*error_token ;
	int							runtime_error_level ;
	int							runtime_error_no ;
	char							runtime_error_string[ ZLANG_RUNTIME_ERROR_STRING_SIZE ] ;
	char							*debug_internal_file ;
	int							debug_internal_lineno ;
	
	struct list_head					source_file_datapage_list ; /* struct ZlangSourceFileDataPageHeader */
	struct ZlangSourceFileDataPageHeader			*curr_source_file_datapage ;
	struct list_head					token_datapage_list ; /* struct ZlangTokenDataPageHeader */
	struct ZlangTokenDataPageHeader				*curr_token_datapage ;
	
	struct list_head					include_files_list ; /* struct ZlangInclude */
	struct list_head					import_files_list ; /* struct ZlangImport */
	
	struct rb_root						global_objs_heap ; /* struct ZlangObject */
	struct list_head					global_order_imported_objs_list ; /* struct ZlangImport */
	
	struct ZlangRuntimeInvokeFunctions			invoke_funcs ;
	
	struct ZlangRuntimeDirectFunctions			direct_funcs ;
	
	struct rb_root						global_funcs ;
	
	struct list_head					global_intercepts ; /* struct ZlangIntercept */
	struct list_head					object_prop_intercepts ; /* struct ZlangIntercept */
	
	struct rb_root						global_interfaces ; /* struct ZlangInterface */
	
	struct ZlangRuntimeFrequentObject			frequent_objs ;
	
	struct ZlangObjectsStack				local_objs_stack ;
	struct ZlangObjectsStackFrame				*local_objs_stack_frame_array ;
	int							local_objs_stack_frame_array_allocount ;
	int							local_objs_stack_frame_array_curridx ;
	struct ZlangObjectsStack				tmp_objs_stack ;
	struct ZlangObjectsStackFrame				*tmp_objs_stack_frame_array ;
	int							tmp_objs_stack_frame_array_allocount ;
	int							tmp_objs_stack_frame_array_curridx ;
	struct ZlangDefersStack					defers_stack ;
	struct ZlangDefersStackFrame				*defers_stack_frame_array ;
	int							defers_stack_frame_array_allocount ;
	int							defers_stack_frame_array_curridx ;
	
	struct list_head					objs_cache ; /* struct ZlangObject */
	struct list_head					props_entis_cache ; /* struct ZlangPropertiesEntity */
	struct list_head					funcs_entis_cache ; /* struct ZlangFunctionsEntity */

	signed long						zlang_max_stack_size ;
	signed long						zlang_alart_stack_depth ;
} ;

struct ZlangDirectProperty_error
{
	unsigned char			thrown_flag ;
	int32_t				code ;
	struct ZlangObject		*message_obj ;
	char				*exception_source_filename ;
	int32_t				exception_source_row ;
	int32_t				exception_source_col ;
	char				*exception_obj_name ;
	char				*exception_func_name ;
	struct ZlangObject		*stack_trace_obj ;
} ;

struct ZlangDirectProperty_fatal
{
	unsigned char			thrown_flag ;
	int32_t				code ;
	struct ZlangObject		*message_obj ;
	char				*exception_source_filename ;
	int32_t				exception_source_row ;
	int32_t				exception_source_col ;
	char				*exception_obj_name ;
	char				*exception_func_name ;
	struct ZlangObject		*stack_trace_obj ;
} ;

DLLEXPORT int zlang( int argc , char *argv[] );

DLLEXPORT int LexicalAnalysisSourceFile( char *filename , struct ZlangRuntime *rt );
DLLEXPORT int LexicalAnalysisSource( char *source , char *source_begin , char *source_end , struct ZlangRuntime *rt );
DLLEXPORT void DebugLexicalAnalysis( struct ZlangRuntime *rt );
DLLEXPORT int PeekToken( struct ZlangRuntime *rt , struct ZlangTokenDataUnitHeader **token_dataunit_header , char **token_dataunit_body );
DLLEXPORT int NextToken( struct ZlangRuntime *rt );
DLLEXPORT int TravelToken( struct ZlangRuntime *rt , struct ZlangTokenDataUnitHeader **token_dataunit_header , char **token_dataunit_body );

#define TRAVELTOKEN(_rt_,_token_info_,_token_,...) \
	if( rt->travel_token_datapage_header ) \
	{ \
		int nret = TravelToken( (_rt_) , & (_token_info_) , & (_token_) ) ; \
		TEST_RUNTIME_DEBUG( (_rt_) ) \
		{ \
			if( (_token_info_) ) \
			{ \
				TOKENTYPE_TO_STRING( (_token_info_)->token_type , _zlang_token_type_str ) \
			} \
			else \
			{ \
				_zlang_token_type_str[0] = '\0' ; \
			} \
			TEST_RUNTIME_DEBUG( (_rt_) ) { PRINT_TABS_AND_FORMAT( rt , "TravelToken return[%d] type[%s] word[%s] source[%s:%"PRIi32",%"PRIi32"]" , nret , _zlang_token_type_str , (_token_) , (_token_info_)->source_filename,(_token_info_)->source_row,(_token_info_)->source_col ); } \
		} \
		__VA_ARGS__ \
		if( nret ) \
		{ \
			SET_RUNTIME_ERROR( (_rt_) , RUNTIME_ERROR , ZLANG_ERROR_SYNTAX , "syntax error after '%s' on traveling token" , (_token_) ) \
			TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt) \
			return ZLANG_ERROR_SYNTAX; \
		} \
	} \

#define TRAVELTOKEN_AND_SAVEINFO(_rt_,_token_info_,_token_,...) \
	{ \
		TRAVELTOKEN( (_rt_) , (_token_info_) , (_token_) , __VA_ARGS__ ) \
		(_rt_)->travel_token_info = (_token_info_) ; \
		(_rt_)->travel_token = (_token_) ; \
	} \

#define PEEKTOKEN(_rt_,_token_info_,_token_,...) \
	if( rt->travel_token_datapage_header ) \
	{ \
		int nret = PeekToken( (_rt_) , & (_token_info_) , & (_token_) ) ; \
		TEST_RUNTIME_DEBUG( (_rt_) ) \
		{ \
			if( (_token_info_) ) \
			{ \
				TOKENTYPE_TO_STRING( (_token_info_)->token_type , _zlang_token_type_str ) \
			} \
			else \
			{ \
				_zlang_token_type_str[0] = '\0' ; \
			} \
			TEST_RUNTIME_DEBUG( (_rt_) ) { PRINT_TABS_AND_FORMAT( rt , "PeekToken return[%d] type[%s] word[%s] source[%s:%"PRIi32",%"PRIi32"]" , nret , _zlang_token_type_str , (_token_) , (_token_info_)->source_filename,(_token_info_)->source_row,(_token_info_)->source_col ); } \
		} \
		if( nret ) \
		{ \
			__VA_ARGS__; \
			SET_RUNTIME_ERROR( (_rt_) , RUNTIME_ERROR , ZLANG_ERROR_SYNTAX , "syntax error after '%s' on peeking token" , (_token_) ) \
			TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt) \
			return ZLANG_ERROR_SYNTAX; \
		} \
	} \

#define PEEKTOKEN_AND_SAVEINFO(_rt_,_token_info_,_token_,...) \
	if( rt->travel_token_datapage_header ) \
	{ \
		PEEKTOKEN( (_rt_) , (_token_info_) , (_token_) , __VA_ARGS__ ) \
		(_rt_)->travel_token_info = (_token_info_) ; \
		(_rt_)->travel_token = (_token_) ; \
	} \

#define NEXTTOKEN(_rt_,...) \
	if( rt->travel_token_datapage_header ) \
	{ \
		int nret = NextToken( (_rt_) ) ; \
		TEST_RUNTIME_DEBUG_THEN_PRINT( (_rt_) , "NextToken return[%d]" , nret ) \
		if( nret ) \
		{ \
			__VA_ARGS__; \
			SET_RUNTIME_ERROR( (_rt_) , RUNTIME_ERROR , ZLANG_ERROR_SYNTAX , "syntax error after '%s' on nexting token" , rt->travel_token ) \
			TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt) \
			return nret; \
		} \
	} \

DLLEXPORT int CreateSourceFile_main( char *pathfilename );
DLLEXPORT int GitInstall( char *git_url , char *git_branch_name , char *makefile );

DLLEXPORT int SyntaxAnalysisScan_func( struct ZlangRuntime *rt , char *obj_name , struct ZlangFunction **pp_func );
DLLEXPORT int SyntaxAnalysisScan_object( struct ZlangRuntime *rt , struct ZlangObject **pp_obj );
DLLEXPORT int SyntaxAnalysisScan_intercept( struct ZlangRuntime *rt , struct ZlangIntercept **pp_intercept );
DLLEXPORT int SyntaxAnalysisScan_interf( struct ZlangRuntime *rt , struct ZlangInterface **pp_interf );
DLLEXPORT int SyntaxAnalysisScan( struct ZlangRuntime *rt );

/*
 * zobjects
 */

DLLEXPORT struct ZlangObject *ZlangImportObject_zobject( struct ZlangRuntime *rt );
DLLEXPORT struct ZlangObject *ZlangImportObject_zruntime( struct ZlangRuntime *rt );

DLLEXPORT struct ZlangObject *ZlangImportObject_error( struct ZlangRuntime *rt );
DLLEXPORT struct ZlangObject *ZlangImportObject_fatal( struct ZlangRuntime *rt );

/*
 * object
 */

DLLEXPORT void FreeObjectsCache( struct ZlangRuntime *rt );

DLLEXPORT int DestractObject( struct ZlangRuntime *rt , struct ZlangObject *obj );
DLLEXPORT unsigned char HadObjectConstracted( struct ZlangObject *obj );
DLLEXPORT unsigned char HadObjectDestracted( struct ZlangObject *obj );
DLLEXPORT void SetObjectConstracted( struct ZlangObject *obj );
DLLEXPORT void SetObjectDestracted( struct ZlangObject *obj );

DLLEXPORT struct ZlangPropertiesEntity *AllocPropertiesEntity( struct ZlangRuntime *rt );
DLLEXPORT int FreePropertiesEntity( struct ZlangRuntime *rt , struct ZlangObject *obj , struct ZlangPropertiesEntity *enti );
DLLEXPORT void FreePropertiesEntityCache( struct ZlangRuntime *rt );

DLLEXPORT struct ZlangFunctionsEntity *AllocFunctionsEntity( struct ZlangRuntime *rt );
DLLEXPORT int FreeFunctionsEntity( struct ZlangRuntime *rt , struct ZlangFunctionsEntity *funcs_enti );
DLLEXPORT void FreeFunctionsEntityCache( struct ZlangRuntime *rt );

DLLEXPORT int ReferObjectFunctionsEntity( struct ZlangRuntime *rt , struct ZlangObject *obj , struct ZlangObject *refer_obj );
DLLEXPORT int UnreferObjectFunctionsEntity( struct ZlangRuntime *rt , struct ZlangObject *obj );
DLLEXPORT int ReferObjectPropertiesEntity( struct ZlangRuntime *rt , struct ZlangObject *obj , struct ZlangObject *refer_obj );
DLLEXPORT int UnreferObjectPropertiesEntity( struct ZlangRuntime *rt , struct ZlangObject *obj );

DLLEXPORT int EvalObjectDirectProperty( struct ZlangRuntime *rt , struct ZlangObject *obj , struct ZlangObject *copy_obj );
DLLEXPORT int EvalObjectProperties( struct ZlangRuntime *rt , struct ZlangObject *obj , struct ZlangObject *eval_obj );
DLLEXPORT int CopyObjectProperties( struct ZlangRuntime *rt , struct ZlangObject *obj , struct ZlangObject *copy_obj );

DLLEXPORT int CreateObjectDirectProperty( struct ZlangRuntime *rt , struct ZlangObject *obj );
DLLEXPORT int DestroyObjectDirectProperty( struct ZlangRuntime *rt , struct ZlangObject *obj );

DLLEXPORT int AddFunctionToObjectFunctionsEntity( struct ZlangRuntime *rt , struct ZlangObject *obj , struct ZlangFunction *func );
DLLEXPORT int UnreferFunctionsEntity( struct ZlangRuntime *rt , struct ZlangFunctionsEntity *funcs_enti );

DLLEXPORT int Operator_PLUS( struct ZlangRuntime *rt , struct ZlangObject *in_obj1 , struct ZlangObject *in_obj2 , struct ZlangObject **out_obj );
DLLEXPORT int Operator_MINUS( struct ZlangRuntime *rt , struct ZlangObject *in_obj1 , struct ZlangObject *in_obj2 , struct ZlangObject **out_obj );
DLLEXPORT int Operator_MUL( struct ZlangRuntime *rt , struct ZlangObject *in_obj1 , struct ZlangObject *in_obj2 , struct ZlangObject **out_obj );
DLLEXPORT int Operator_DIV( struct ZlangRuntime *rt , struct ZlangObject *in_obj1 , struct ZlangObject *in_obj2 , struct ZlangObject **out_obj );
DLLEXPORT int Operator_MOD( struct ZlangRuntime *rt , struct ZlangObject *in_obj1 , struct ZlangObject *in_obj2 , struct ZlangObject **out_obj );
DLLEXPORT int UnaryOperator_NEGATIVE( struct ZlangRuntime *rt , struct ZlangObject *in_out_obj1 );
DLLEXPORT int UnaryOperator_NOT( struct ZlangRuntime *rt , struct ZlangObject *in_out_obj1 );
DLLEXPORT int UnaryOperator_BIT_REVERSE( struct ZlangRuntime *rt , struct ZlangObject *in_out_obj1 );
DLLEXPORT int UnaryOperator_PLUS_PLUS( struct ZlangRuntime *rt , struct ZlangObject *in_out_obj1 );
DLLEXPORT int UnaryOperator_MINUS_MINUS( struct ZlangRuntime *rt , struct ZlangObject *in_out_obj1 );
DLLEXPORT int Compare_EQUAL( struct ZlangRuntime *rt , struct ZlangObject *in_obj1 , struct ZlangObject *in_obj2 , struct ZlangObject **out_obj );
DLLEXPORT int Compare_NOTEQUAL( struct ZlangRuntime *rt , struct ZlangObject *in_obj1 , struct ZlangObject *in_obj2 , struct ZlangObject **out_obj );
DLLEXPORT int Compare_LT( struct ZlangRuntime *rt , struct ZlangObject *in_obj1 , struct ZlangObject *in_obj2 , struct ZlangObject **out_obj );
DLLEXPORT int Compare_LE( struct ZlangRuntime *rt , struct ZlangObject *in_obj1 , struct ZlangObject *in_obj2 , struct ZlangObject **out_obj );
DLLEXPORT int Compare_GT( struct ZlangRuntime *rt , struct ZlangObject *in_obj1 , struct ZlangObject *in_obj2 , struct ZlangObject **out_obj );
DLLEXPORT int Compare_GE( struct ZlangRuntime *rt , struct ZlangObject *in_obj1 , struct ZlangObject *in_obj2 , struct ZlangObject **out_obj );
DLLEXPORT int Logic_AND( struct ZlangRuntime *rt , struct ZlangObject *in_obj1 , struct ZlangObject *in_obj2 , struct ZlangObject **out_obj );
DLLEXPORT int Logic_OR( struct ZlangRuntime *rt , struct ZlangObject *in_obj1 , struct ZlangObject *in_obj2 , struct ZlangObject **out_obj );
DLLEXPORT int Bit_AND( struct ZlangRuntime *rt , struct ZlangObject *in_obj1 , struct ZlangObject *in_obj2 , struct ZlangObject **out_obj );
DLLEXPORT int Bit_XOR( struct ZlangRuntime *rt , struct ZlangObject *in_obj1 , struct ZlangObject *in_obj2 , struct ZlangObject **out_obj );
DLLEXPORT int Bit_OR( struct ZlangRuntime *rt , struct ZlangObject *in_obj1 , struct ZlangObject *in_obj2 , struct ZlangObject **out_obj );
DLLEXPORT int Bit_MOVELEFT( struct ZlangRuntime *rt , struct ZlangObject *in_obj1 , struct ZlangObject *in_obj2 , struct ZlangObject **out_obj );
DLLEXPORT int Bit_MOVERIGHT( struct ZlangRuntime *rt , struct ZlangObject *in_obj1 , struct ZlangObject *in_obj2 , struct ZlangObject **out_obj );
DLLEXPORT int TryAscensionType( struct ZlangRuntime *rt , struct ZlangObject *obj , struct ZlangObject **ascen_obj );
DLLEXPORT int TryAscensionType2( struct ZlangRuntime *rt , struct ZlangObject **ascen_obj , struct ZlangObject **ascen_obj2 );

DLLEXPORT int GetFunctionsEntityReferCount( struct ZlangFunctionsEntity *funcs_enti );

DLLEXPORT int CheckUnimplementFunctionInObject( struct ZlangRuntime *rt , struct ZlangObject *top_child_obj , struct ZlangFunctionsEntity *funcs_enti );

/*
 * stack
 */

DLLEXPORT int InitObjectStack( struct ZlangRuntime *rt , struct ZlangObjectsStack *objs_stack , int default_size , int increase_size );
DLLEXPORT void CleanObjectsStack( struct ZlangRuntime *rt , struct ZlangObjectsStack *objs_stack );
DLLEXPORT struct ZlangObject *PushObjectsStack( struct ZlangRuntime *rt , struct ZlangObjectsStack *objs_stack , struct ZlangObjectsStackFrame *objs_stack_frame );
DLLEXPORT int PopupObjectsStack( struct ZlangRuntime *rt , struct ZlangObjectsStack *objs_stack , struct ZlangObjectsStackFrame *objs_stack_frame );
DLLEXPORT int GetStackTop( struct ZlangRuntime *rt , struct ZlangObjectsStackFrame *objs_stack_frame );
DLLEXPORT struct ZlangObject *GetStackTopObject( struct ZlangRuntime *rt , struct ZlangObjectsStack *objs_stack );
DLLEXPORT struct ZlangObject *GetStackObject( struct ZlangRuntime *rt , struct ZlangObjectsStack *objs_stack , int stack_no );

DLLEXPORT int InitLocalObjecsStackFrameArray( struct ZlangRuntime *rt );
DLLEXPORT void CleanLocalObjecsStackFrameArray( struct ZlangRuntime *rt );
DLLEXPORT struct ZlangObjectsStackFrame *IncreaseLocalObjectsStackFrame( struct ZlangRuntime *rt , char *full_func_name );
DLLEXPORT struct ZlangObjectsStackFrame *DecreaseLocalObjectsStackFrame( struct ZlangRuntime *rt );
DLLEXPORT int PopupLocalObjectsStack( struct ZlangRuntime *rt );
DLLEXPORT void DebugLocalObjectsStack( struct ZlangRuntime *rt );

DLLEXPORT int InitTmpObjecsStackFrameArray( struct ZlangRuntime *rt );
DLLEXPORT void CleanTmpObjecsStackFrameArray( struct ZlangRuntime *rt );
DLLEXPORT struct ZlangObjectsStackFrame *IncreaseTmpObjectsStackFrame( struct ZlangRuntime *rt , char *full_func_name );
DLLEXPORT struct ZlangObjectsStackFrame *DecreaseTmpObjectsStackFrame( struct ZlangRuntime *rt );
DLLEXPORT int PopupTmpObjectsStack( struct ZlangRuntime *rt );
DLLEXPORT void DebugTmpObjectsStack( struct ZlangRuntime *rt );

DLLEXPORT int InitDefersStack( struct ZlangRuntime *rt , struct ZlangDefersStack *defers_stack , int default_size , int increase_size );
DLLEXPORT void CleanDefersStack( struct ZlangRuntime *rt , struct ZlangDefersStack *defers_stack );
DLLEXPORT struct ZlangDefer *PushLocalDefersStack( struct ZlangRuntime *rt , struct ZlangDefersStack *defers_stack , struct ZlangDefersStackFrame *defers_stack_frame );
DLLEXPORT int PopupDefesStack( struct ZlangRuntime *rt , struct ZlangDefersStack *defers_stack , struct ZlangDefersStackFrame *defers_stack_frame );
DLLEXPORT int PopupDefersStack( struct ZlangRuntime *rt );
DLLEXPORT int InitDefersStackFrameArray( struct ZlangRuntime *rt );
DLLEXPORT void CleanDefersStackFrameArray( struct ZlangRuntime *rt );
DLLEXPORT struct ZlangDefersStackFrame *IncreaseDefersStackFrame( struct ZlangRuntime *rt , char *full_func_name );
DLLEXPORT struct ZlangDefersStackFrame *DecreaseDefersStackFrame( struct ZlangRuntime *rt );
DLLEXPORT void SetDefersStackFullFuncName( struct ZlangDefersStackFrame *defers_stack_frame , char *full_func_name );
DLLEXPORT struct ZlangDefersStackFrame *GetCurrentDefersStackFrame( struct ZlangRuntime *rt );
DLLEXPORT int GetCurrentDefersStackFrameIndex( struct ZlangRuntime *rt );
DLLEXPORT struct ZlangDefersStackFrame *GetPreviousDefersStackFrame( struct ZlangRuntime *rt , struct ZlangDefersStackFrame *defers_stack_frame );
DLLEXPORT void DebugDefersStack( struct ZlangRuntime *rt );

DLLEXPORT void DebugStack( struct ZlangRuntime *rt );

/*
 * intercept
 */

DLLEXPORT struct ZlangIntercept *CreateIntercept( struct ZlangRuntime *rt , unsigned char opportunity , char *obj_and_func_name , struct ZlangFunction *func );
DLLEXPORT void DestroyIntercept( struct ZlangRuntime *rt , struct ZlangIntercept *intercept );

DLLEXPORT int InterceptBeforeCallFunction( struct ZlangRuntime *rt );
DLLEXPORT int InterceptAfterCallFunction( struct ZlangRuntime *rt );

#define CHECK_THEN_INTERCEPT_SET_PROPERTY(_obj_,_from_obj_,...) \
	if( (_obj_)->intercept_set && ! rt->in_func->is_interceptor ) \
	{ \
		TEST_RUNTIME_DEBUG( rt ) { PRINT_TABS(rt) printf( "intercept set property , to " ); DebugPrintObject( rt , (_obj_) ); printf( " from " ); DebugPrintObject( rt , (_from_obj_) ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE } \
		nret = InterceptSetProperty( rt , (_obj_)->intercept_set , & (_from_obj_) ) ; \
		if( nret ) \
		{ \
			__VA_ARGS__; \
		} \
	} \

#define CHECK_THEN_INTERCEPT_GET_PROPERTY(_obj_,...) \
	if( (_obj_)->intercept_get && ! rt->in_func->is_interceptor ) \
	{ \
		TEST_RUNTIME_DEBUG( rt ) { PRINT_TABS(rt) printf( "intercept get property , " ); DebugPrintObject( rt , (_obj_) ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE } \
		nret = InterceptGetProperty( rt , (_obj_)->intercept_get , & (_obj_) ) ; \
		if( nret ) \
		{ \
			__VA_ARGS__; \
		} \
	} \

DLLEXPORT int InterceptGetProperty( struct ZlangRuntime *rt , struct ZlangIntercept *intercept , struct ZlangObject **pp_property );
DLLEXPORT int InterceptSetProperty( struct ZlangRuntime *rt , struct ZlangIntercept *intercept , struct ZlangObject **pp_property );

DLLEXPORT void DebugPrintIntercept( struct ZlangRuntime *rt , struct ZlangIntercept *intercept );

/*
 * interface
 */

DLLEXPORT struct ZlangInterface *CreateInterface( struct ZlangRuntime *rt , char *interf_name );
DLLEXPORT void DestroyInterface( struct ZlangRuntime *rt , struct ZlangInterface *interf );

DLLEXPORT struct ZlangInterface *QueryGlobalInterfaceByInterfaceName( struct ZlangRuntime *rt , char *interf_name );

DLLEXPORT char *GetInterfaceName( struct ZlangInterface *interf );
DLLEXPORT struct ZlangObject *GetInterfaceInner( struct ZlangInterface *interf );

DLLEXPORT void DebugPrintInterface( struct ZlangRuntime *rt , struct ZlangInterface *interf );

/*
 * interpret
 */

DLLEXPORT int InterpretStatementSegment( struct ZlangRuntime *rt , struct ZlangInterpretStatementContext *previous_interp_stat_ctx );
DLLEXPORT int InterpretStatement( struct ZlangRuntime *rt );
DLLEXPORT int InterpretExpression( struct ZlangRuntime *rt , struct ZlangInterpretStatementContext *interp_stat_ctx , struct ZlangObject **result_obj );
DLLEXPORT int InterpretExpression_MemberOf( struct ZlangRuntime *rt , enum TokenType member_of_token_type , struct ZlangInterpretStatementContext *interp_stat_ctx , struct ZlangObject *master_obj , struct ZlangObject **result_obj );
DLLEXPORT int InterpretExpression_InvokeFunction( struct ZlangRuntime *rt , enum TokenType member_of_token_type , struct ZlangInterpretStatementContext *interp_stat_ctx , struct ZlangObject *master_obj , char *func_name , struct ZlangObject **result_obj );
DLLEXPORT int InterpretExpression_InvokeConstractorFunction( struct ZlangRuntime *rt , struct ZlangInterpretStatementContext *interp_stat_ctx , struct ZlangObject *obj );
#define IS_CONSTRACTOR		0
#define IS_DESTRACTOR		1
DLLEXPORT int InterpretExpression_InvokeDefaultConstractorOrDestractorFunction( struct ZlangRuntime *rt , struct ZlangInterpretStatementContext *interp_stat_ctx , unsigned char constractor_or_destractor , struct ZlangObject *var_obj );
DLLEXPORT int InterpretExpression_DeclareObjects( struct ZlangRuntime *rt , struct ZlangInterpretStatementContext *interp_stat_ctx , struct ZlangInterpretExpressionContext *interp_expr_ctx , struct ZlangObject *master_obj , struct ZlangObject *clone_obj , struct ZlangObject **result_obj );
DLLEXPORT int InterpretExpression_ReturnFunctionStack( struct ZlangRuntime *rt );

DLLEXPORT int SkipExpression( struct ZlangRuntime *rt , enum TokenType token_of_expression_end1 , enum TokenType token_of_expression_end2 , enum TokenType token_of_expression_end3 );
DLLEXPORT int SkipStatement( struct ZlangRuntime *rt );
DLLEXPORT int SkipStatementSegment( struct ZlangRuntime *rt );

/*
 * function
 */

DLLEXPORT int InitFunctionParamter( struct ZlangRuntime *rt , struct ZlangFunctionParameter *param , char *parent_obj_name , char *obj_name );
DLLEXPORT struct ZlangFunctionParameter *AllocFunctionParamter( struct ZlangRuntime *rt , char *parent_obj_name , char *obj_name );
DLLEXPORT void FreeFunctionParamter( struct ZlangFunctionParameter *func_param );
DLLEXPORT char *GenerateFullFunctionName( struct ZlangRuntime *rt , struct ZlangFunction *func , struct list_head *in_params , struct ZlangFunctionParameter *out_param );
DLLEXPORT struct ZlangFunction *CreateFunctionFromFunction( struct ZlangRuntime *rt , struct ZlangFunction *copy_func );

DLLEXPORT int SetSynchronizeFunction( struct ZlangFunction *func );
DLLEXPORT int UnsetSynchronizeFunction( struct ZlangFunction *func );
DLLEXPORT unsigned char IsSynchronizeFunction( struct ZlangFunction *func );
DLLEXPORT int LockSynchronizeFunction( struct ZlangFunction *func );
DLLEXPORT int UnlockSynchronizeFunction( struct ZlangFunction *func );
DLLEXPORT void CopySynchronizeFunction( struct ZlangFunction *func , struct ZlangFunction *copy_func );

DLLEXPORT void DebugPrintFunction( struct ZlangRuntime *rt , struct ZlangFunction *func );

DLLEXPORT void DebugBeforeCallFunction( struct ZlangRuntime *rt , struct ZlangFunction *func );
DLLEXPORT void DebugAfterCallFunction( struct ZlangRuntime *rt , struct ZlangFunction *func );

/*
 * keyword
 */

DLLEXPORT int Keyword_import( struct ZlangRuntime *rt );

DLLEXPORT int Keyword_if( struct ZlangRuntime *rt );
DLLEXPORT int SkipStatement_if( struct ZlangRuntime *rt );

DLLEXPORT int Keyword_while( struct ZlangRuntime *rt );
DLLEXPORT int SkipStatement_while( struct ZlangRuntime *rt );

DLLEXPORT int Keyword_do_while( struct ZlangRuntime *rt );
DLLEXPORT int SkipStatement_do_while( struct ZlangRuntime *rt );

DLLEXPORT int Keyword_for( struct ZlangRuntime *rt );
DLLEXPORT int SkipStatement_for( struct ZlangRuntime *rt );

DLLEXPORT int Keyword_foreach( struct ZlangRuntime *rt );
DLLEXPORT int SkipStatement_foreach( struct ZlangRuntime *rt );

DLLEXPORT int Keyword_switch( struct ZlangRuntime *rt );
DLLEXPORT int SkipStatement_switch( struct ZlangRuntime *rt );
DLLEXPORT int SkipStatement_case( struct ZlangRuntime *rt );
DLLEXPORT int SkipStatement_default( struct ZlangRuntime *rt );

DLLEXPORT int Keyword_sync( struct ZlangRuntime *rt );
DLLEXPORT int SkipStatement_sync( struct ZlangRuntime *rt );

DLLEXPORT int Keyword_try( struct ZlangRuntime *rt );
DLLEXPORT int SkipStatement_try( struct ZlangRuntime *rt );
DLLEXPORT int SkipStatement_catch( struct ZlangRuntime *rt );
DLLEXPORT int SkipStatement_finally( struct ZlangRuntime *rt );

/*
 * charset
 */

DLLEXPORT char *QueryCharsetAlias( struct ZlangRuntime *rt , char *alias_name );
DLLEXPORT int QueryCharsetAliasAndChangeTokenInfo( struct ZlangRuntime *rt , struct ZlangTokenDataUnitHeader *token_info , char **token );

/*
 * rbtree_ins
 */

DLLEXPORT int LinkObjectToRuntimeObjectsHeapByObjectName( struct ZlangRuntime *rt , struct ZlangObject *obj );
DLLEXPORT void UnlinkObjectFromRuntimeObjectsHeapNode( struct ZlangRuntime *rt , struct ZlangObject *obj );
DLLEXPORT struct ZlangObject *QueryObjectInRuntimeObjectsHeapByObjectName( struct ZlangRuntime *rt , struct ZlangObject *obj );
DLLEXPORT struct ZlangObject *TravelObjectInRuntimeObjectsHeapByObjectName( struct ZlangRuntime *rt , struct ZlangObject *obj );
DLLEXPORT void DestroyRuntimeObjectsHeap( struct ZlangRuntime *rt );

DLLEXPORT int LinkFunctionToRuntimeFunctionsTreeByFullFunctionName( struct ZlangRuntime *rt , struct ZlangFunction *func );
DLLEXPORT void UnlinkFunctionFromRuntimeFunctionsTree( struct ZlangRuntime *rt , struct ZlangFunction *func );
DLLEXPORT struct ZlangFunction *QueryFunctionInRuntimeFunctionsTreeByFunctionName( struct ZlangRuntime *rt , struct ZlangFunction *func );
DLLEXPORT struct ZlangFunction *QueryFunctionInRuntimeFunctionsTreeByFullFunctionName( struct ZlangRuntime *rt , struct ZlangFunction *func );
DLLEXPORT struct ZlangFunction *TravelFunctionInRuntimeFunctionsTreeByFullFunctionName( struct ZlangRuntime *rt , struct ZlangFunction *func );
DLLEXPORT struct ZlangFunction *TravelPrevFunctionInRuntimeFunctionsTreeByFullFunctionName( struct ZlangRuntime *rt , struct ZlangFunction *func );
DLLEXPORT void DestroyRuntimeFunctionsTree( struct ZlangRuntime *rt );

DLLEXPORT int LinkPropertyToEntityPropertiesTreeByPropertyName( struct ZlangPropertiesEntity *entity , struct ZlangObject *obj );
DLLEXPORT void UnlinkPropertyFromEntityPropertiesTree( struct ZlangPropertiesEntity *entity , struct ZlangObject *obj );
DLLEXPORT struct ZlangObject *QueryPropertyInEntityPropertiesTreeByPropertyName( struct ZlangPropertiesEntity *entity , struct ZlangObject *obj );
DLLEXPORT struct ZlangObject *TravelPropertyFromEntityPropertiesTreeByPropertyName( struct ZlangPropertiesEntity *entity , struct ZlangObject *obj );
DLLEXPORT void DestroyEntityPropertiesTree( struct ZlangPropertiesEntity *enti );

DLLEXPORT int LinkFunctionToFunctionsEntityFunctionsTreeByFullFunctionName( struct ZlangFunctionsEntity *funcs_enti , struct ZlangFunction *func );
DLLEXPORT void UnlinkFunctionFromFunctionsEntityFunctionsTree( struct ZlangFunctionsEntity *funcs_enti , struct ZlangFunction *func );
DLLEXPORT struct ZlangFunction *QueryFunctionInFunctionsEntityFunctionsTreeByFunctionName( struct ZlangFunctionsEntity *funcs_enti , struct ZlangFunction *func );
DLLEXPORT struct ZlangFunction *QueryFunctionInFunctionsEntityFunctionsTreeByFullFunctionName( struct ZlangFunctionsEntity *funcs_enti , struct ZlangFunction *func );
DLLEXPORT struct ZlangFunction *TravelFunctionFromFunctionsEntityFunctionsTreeByFullFunctionName( struct ZlangFunctionsEntity *funcs_enti , struct ZlangFunction *func );
DLLEXPORT struct ZlangFunction *TravelPrevFunctionFromFunctionsEntityFunctionsTreeByFullFunctionName( struct ZlangFunctionsEntity *funcs_enti , struct ZlangFunction *func );
DLLEXPORT void DestroyFunctionsEntityFunctionsTree( struct ZlangFunctionsEntity *funcs_enti );

DLLEXPORT int LinkAliasToRuntimeCharsetAliasByAliasName( struct ZlangRuntime *rt , struct ZlangCharsetAlias *alias );
DLLEXPORT void UnlinkAliasFromRuntimeCharsetAliasNode( struct ZlangRuntime *rt , struct ZlangCharsetAlias *alias );
DLLEXPORT struct ZlangCharsetAlias *QueryAliasInRuntimeCharsetAliasByAliasName( struct ZlangRuntime *rt , struct ZlangCharsetAlias *alias );
DLLEXPORT struct ZlangCharsetAlias *TravelAliasInRuntimeCharsetAliasByAliasName( struct ZlangRuntime *rt , struct ZlangCharsetAlias *alias );
DLLEXPORT void DestroyRuntimeCharsetAlias( struct ZlangRuntime *rt );

DLLEXPORT int LinkInterfaceToRuntimeInterfacesHeapByInterfaceName( struct ZlangRuntime *rt , struct ZlangInterface *interf );
DLLEXPORT void UnlinkInterfaceFromRuntimeInterfacesHeapNode( struct ZlangRuntime *rt , struct ZlangInterface *interf );
DLLEXPORT struct ZlangInterface *QueryInterfaceInRuntimeInterfacesHeapByInterfaceName( struct ZlangRuntime *rt , struct ZlangInterface *interf );
DLLEXPORT struct ZlangInterface *TravelInterfaceInRuntimeInterfacesHeapByInterfaceName( struct ZlangRuntime *rt , struct ZlangInterface *interf );
DLLEXPORT void DestroyRuntimeInterfacesHeap( struct ZlangRuntime *rt );

#endif

