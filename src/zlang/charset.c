/* Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "zlang_in.h"

int ImportCharsetAlias( struct ZlangRuntime *rt , struct ZlangCharsetAlias *aliases )
{
	struct ZlangCharsetAlias	*alias = NULL ;
	struct ZlangCharsetAlias	*a = NULL ;
	int				nret = 0 ;
	
	for( alias = aliases ; alias->charset > 0 && alias->alias_name ; alias++ )
	{
		if( alias->charset == 0 || alias->alias_name == NULL || alias->alias_name[0] == '\0' || alias->identification == NULL || alias->identification[0] == '\0' )
			continue;
		
		a = (struct ZlangCharsetAlias *)ZLMALLOC( sizeof(struct ZlangCharsetAlias) ) ;
		if( a == NULL )
		{
			SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_ALLOC , "alloc memory for charset failed" )
			return ZLANG_ERROR_ALLOC;
		}
		memset( a , 0x00 , sizeof(struct ZlangCharsetAlias) );
		
		a->charset = alias->charset ;
		a->alias_name = alias->alias_name ;
		a->identification = alias->identification ;
		nret = LinkAliasToRuntimeCharsetAliasByAliasName( rt , a ) ;
		if( nret )
		{
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "link charset alias[%d][%s][%s] in runtime failed" , a->charset , a->alias_name , a->identification )
			ZLFREE( a );
		}
		
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "import charset [%d][%s][%s]" , a->charset , a->alias_name , a->identification )
	}
	
	return 0;
}

char *QueryCharsetAlias( struct ZlangRuntime *rt , char *alias_name )
{
	struct ZlangCharsetAlias	a ;
	struct ZlangCharsetAlias	*alias ;
	
	if( rt->charset == 0 )
		return NULL;
	
	memset( & a , 0x00 , sizeof(struct ZlangCharsetAlias) );
	a.charset = rt->charset ;
	a.alias_name = alias_name ;
	alias = QueryAliasInRuntimeCharsetAliasByAliasName( rt , & a ) ;
	if( alias == NULL )
		return NULL;
	else
		return alias->identification;
}

int QueryCharsetAliasAndChangeTokenInfo( struct ZlangRuntime *rt , struct ZlangTokenDataUnitHeader *token_info , char **token )
{
	char		*new_token = NULL ;
	char		*p = NULL ;
	
	if( token == NULL || token_info == NULL )
		return -1;
	
	if( token_info->token_type != TOKEN_TYPE_IDENTIFICATION )
		return -1;
	
	if( token_info->p1 == ZLANG_TOKEN_IDENTIFICATION_NO_ALIASED )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "don't need replace alias token[%s]" , (*token) )
		return 1;
	}
	else if( token_info->p1 )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "replace alias[%s] to token[%s]" , (char*)(token_info->p1) , (*token) )
		(*token) = token_info->p1 ;
		token_info->token_len = token_info->n3 ;
		return 0;
	}
	
	new_token = (*token) ;
	for( ; ; )
	{
		p = QueryCharsetAlias( rt , new_token ) ;
		if( p == NULL )
			break;
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "QueryCharsetAlias[%s] return[%s]" , new_token , p )
		new_token = p ;
	}
	if( new_token == (*token) )
	{
		token_info->p1 = ZLANG_TOKEN_IDENTIFICATION_NO_ALIASED ;
		return 1;
	}
	else
	{
		token_info->p1 = new_token ;
		token_info->n3 = (int)strlen(new_token) ;
		(*token) = token_info->p1 ;
		token_info->token_len = token_info->n3 ;
		return 0;
	}
}

