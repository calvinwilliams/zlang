/* Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef _H_AODATAPAGE_
#define _H_AODATAPAGE_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <inttypes.h>

#ifndef TLS
#if ( defined __linux__ ) || ( defined __unix ) || ( defined _AIX )
#define TLS             __thread
#elif ( defined _WIN32 )
#define TLS             __declspec( thread )
#endif
#endif

#if defined(__linux__)
#define DLLEXPORT	extern
#elif defined(_WIN32)
#define DLLEXPORT	__declspec(dllexport)
#endif

#define AODATAPAGE_ERROR_ALLOC			-10
#define AODATAPAGE_ERROR_VERSION_NOT_MATCH	-100
#define AODATAPAGE_WARN_SPACE_ISNOT_ENOUGH	101
#define AODATAPAGE_INFO_TRAVEL_OUT		102

DLLEXPORT int GetAppendonlyDataPageLastError();

DLLEXPORT char *CreateAppendonlyDataPage( char magic[4] , uint64_t datapage_size , uint16_t datapage_user_header_size , uint16_t dataunit_user_header_size , uint16_t post_dataunit_reserved_len );
DLLEXPORT void DestroyAppendonlyDataPage( char *datapage );

DLLEXPORT void *GetAppendonlyDataPageUserHeader( char *datapage );

DLLEXPORT char *AddAppendonlyDataUnit( char *datapage , void *dataunit_user_header , void *dataunit_user_body , uint64_t dataunit_user_body_len );

DLLEXPORT char *PeekAppendonlyDataUnit( char *datapage , char *dataunit , void **dataunit_user_header , char **dataunit_user_body , uint64_t *dataunit_user_body_len );
DLLEXPORT char *NextAppendonlyDataUnit( char *datapage , char *dataunit );
DLLEXPORT char *TravelAppendonlyDataUnit( char *datapage , char *dataunit , void **dataunit_user_header , char **dataunit_user_body , uint64_t *dataunit_user_body_len );

DLLEXPORT void *GetAppendonlyDataUnitUserHeader( char *dataunit );
DLLEXPORT char *GetAppendonlyDataUnitUserBody( char *datapage , char *dataunit , uint64_t *dataunit_user_body_len );

#define DATAUNITUSERDEADER_TO_BODY(_dataunit_user_header_ptr_,_struct_of_dataunit_user_header_)		( (char*)(_dataunit_user_header_ptr_)+sizeof(_struct_of_dataunit_user_header_) )

#endif

