/* Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef _H_ZLANG_
#define _H_ZLANG_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <errno.h>
#include <sys/types.h>
#include <fcntl.h>
#include <inttypes.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <locale.h>
#include <time.h>
#if ( defined __linux ) || ( defined __unix )
#include <unistd.h>
#include <stdint.h>
#include <sys/mman.h>
#include <linux/limits.h>
#include <dlfcn.h>
#include <stdarg.h>
#include <sys/syscall.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <sys/time.h>
#define _XOPEN_SOURCE
#define __USE_XOPEN
#include <pthread.h>
#include <signal.h>
#elif ( defined _WIN32 )
#include <ws2tcpip.h>
#include <windows.h>
#include <io.h>
#include <stdio.h>
#include <string.h>
#include <winsock.h>
#include <winsock2.h>
#endif

#ifndef DLLEXPORT
#if defined(__linux__)
#define DLLEXPORT	extern
#elif defined(_WIN32)
#define DLLEXPORT	__declspec(dllexport)
#endif
#endif

#ifndef TLS
#if ( defined __linux__ ) || ( defined __unix ) || ( defined _AIX )
#define TLS             __thread
#elif ( defined _WIN32 )
#define TLS             __declspec(thread)
#endif
#endif

#if defined(__GNUC__) || defined(__clang__)
#define forceinline inline __attribute__((always_inline))
#elif (defined(__GNUC__) && !defined(__clang__))
#define inline inline
#elif defined(_MSC_VER)
#define inline __inline
#define forceinline __forceinline
#endif

#if defined(_WIN32)
#define PATH_MAX	MAX_PATH
#endif

#define __USE_ZLMALLOC
#include "zlmalloc.h"

#include "rbtree.h"

#include "mbcs.h"

#include "token.h"

#ifndef TRUE
#define TRUE		1
#endif

#ifndef FALSE
#define FALSE		0
#endif

#ifndef MIN
#define MIN(_a_,_b_)	( (_a_)<(_b_)?(_a_):(_b_) )
#endif

#ifndef MAX
#define MAX(_a_,_b_)	( (_a_)>(_b_)?(_a_):(_b_) )
#endif

#define FLOAT_EPSILON	1e-6
#define DOUBLE_EPSILON	1e-15

#if defined(_WIN32)
#define int32_t		__int32
#define int64_t		__int64
#define uint32_t	unsigned __int32
#define uint64_t	unsigned __int64
#ifndef PRIi32
#define PRIi32		"%d"
#define PRIi64		"%ld"
#define PRIu32		"%u"
#define PRIu64		"%lu"
#endif
#endif

#ifndef PRIptrdiff
#if defined(__linux__)
#define PRIptrdiff		"td"
#elif defined(_WIN32)
#define PRIptrdiff		"I64d"
#endif
#endif

#ifndef STRDUP
#if ( defined __linux ) || ( defined __unix )
#define STRDUP	strdup
#define STRNDUP	strndup
#elif ( defined _WIN32 )
#define STRDUP	_strdup
#define STRNDUP	_strndup
#endif
#endif

#ifndef STRCMP
#define STRCMP(_a_,_C_,_b_) ( strcmp(_a_,_b_) _C_ 0 )
#define STRNCMP(_a_,_C_,_b_,_n_) ( strncmp(_a_,_b_,_n_) _C_ 0 )
#define STRNNCMP(_a_,_an_,_C_,_b_,_bn_)	( \
						!! ( \
							( \
								(_an_) == 0 && (_bn_) == 0 \
							) \
							|| \
							( \
								(_an_) == (_bn_) && strncmp(_a_,_b_,_an_) == 0 \
							) \
						) \
					) _C_ 1
#endif

#if defined(_WIN32)
char *_stristr(const char* str, const char* substr);
#endif

#ifndef STRICMP
#if ( defined _WIN32 )
#define STRICMP(_a_,_C_,_b_) ( _stricmp(_a_,_b_) _C_ 0 )
#define STRNICMP(_a_,_C_,_b_,_n_) ( _strnicmp(_a_,_b_,_n_) _C_ 0 )
#define STRISTR(_a_,_b_) _stristr(_a_,_b_)
#elif ( defined __unix ) || ( defined _AIX ) || ( defined __linux__ )
#define STRICMP(_a_,_C_,_b_) ( strcasecmp(_a_,_b_) _C_ 0 )
#define STRNICMP(_a_,_C_,_b_,_n_) ( strncasecmp(_a_,_b_,_n_) _C_ 0 )
#define STRISTR(_a_,_b_) strcasestr(_a_,_b_)
char *strcasestr(const char *haystack, const char *needle);
#endif
#endif

#ifndef MEMCMP
#define MEMCMP(_a_,_C_,_b_,_n_) ( memcmp(_a_,_b_,_n_) _C_ 0 )
#endif

#ifndef SET_ATTRIBUTE
#define SET_ATTRIBUTE(attrs,attr)	(attrs)=(attr);
#endif

#ifndef ADD_ATTRIBUTE
#define ADD_ATTRIBUTE(attrs,attr)	(attrs)|=(attr);
#endif

#ifndef REMOVE_ATTRIBUTE
#define REMOVE_ATTRIBUTE(attrs,attr)	(attrs)&=~(attr);
#endif

#ifndef TEST_ATTRIBUTE
#define TEST_ATTRIBUTE(attrs,attr)	( ((attrs)&(attr))==(attr) )
#endif

#if ( defined __linux__ ) || ( defined __unix ) || ( defined _AIX )
#define NEWLINE		"\n"
#elif defined(_WIN32)
#define NEWLINE		"\r\n"
#endif

#if defined(__linux__)
#define DIRECTORY_SEPARATOR_CHAR		'/'
#elif defined(_WIN32)
#define DIRECTORY_SEPARATOR_CHAR		'\\'
#endif

#ifndef ADDRINFO
#if defined(__linux__)
#define ADDRINFO	struct addrinfo
#elif defined(_WIN32)
#define ADDRINFO	ADDRINFOA
#endif
#endif

#ifndef PATH_MAX
#define PATH_MAX	1024
#endif

#ifndef WRITE
#if ( defined __linux ) || ( defined __unix )
#define WRITE	write
#elif ( defined _WIN32 )
#define WRITE	_write
#endif
#endif

#ifndef READ
#if ( defined __linux ) || ( defined __unix )
#define READ	read
#elif ( defined _WIN32 )
#define READ	_read
#endif
#endif

#ifndef SEND
#if ( defined __linux ) || ( defined __unix )
#define SEND	send
#elif ( defined _WIN32 )
#define SEND	send
#endif
#endif

#ifndef RECV
#if ( defined __linux ) || ( defined __unix )
#define RECV	recv
#elif ( defined _WIN32 )
#define RECV	recv
#endif
#endif

#ifndef THREAD
#if defined(__linux__)
#define THREAD		pthread_t
#elif defined(_WIN32)
#define THREAD		HANDLE
#endif
#endif

#ifndef MUTEX
#if defined(__linux__)
#define MUTEX		pthread_mutex_t
#elif defined(_WIN32)
#define MUTEX		CRITICAL_SECTION
#endif
#endif

#ifndef GETCWD
#if defined(__linux__)
#define GETCWD		getcwd
#elif defined(_WIN32)
#define GETCWD		_getcwd
#endif
#endif

#ifndef ERRNO
#if defined(__linux__)
#define ERRNO		errno
#elif defined(_WIN32)
#define ERRNO		GetLastError()
#endif
#endif

#include "byteorder.h"

#define ZLANG_ERROR_FILE_NOT_FOUND		-4
#define ZLANG_ERROR_PARAMETER			-7
#define ZLANG_ERROR_INTERNAL			-9

#define ZLANG_ERROR_ALLOC			-10
#define ZLANG_ERROR_OPEN			-11
#define ZLANG_ERROR_STAT			-11
#define ZLANG_ERROR_DLOPEN			-13
#define ZLANG_ERROR_DLSYM			-14
#define ZLANG_ERROR_STRDUP			-15
#define ZLANG_ERROR_REALLOC			-16
#define ZLANG_ERROR_CHARSET			-21

#define ZLANG_ERROR_TOKEN			-101
#define ZLANG_ERROR_ADD_TOKEN			-102
#define ZLANG_INFO_TRAVEL_TOKEN_OUT		103
#define ZLANG_ERROR_TRAVEL_NULL_ADDRESS		-104

#define ZLANG_ERROR_SYNTAX				-200
#define ZLANG_ERROR_NO_MAIN				-201
#define ZLANG_ERROR_IMPORT_OBJECTS			-202
#define ZLANG_ERROR_CREATE_OBJECT			-203
#define ZLANG_ERROR_OBJECT_NOT_IMPORTED_OR_DECLARED	-204
#define ZLANG_ERROR_LINK_FUNC_TO_ENTITY			-205
#define ZLANG_ERROR_DATA_INVALID			-206
#define ZLANG_ERROR_OPERATOR_TYPE_NOT_MATCHED		-207
#define ZLANG_ERROR_FUNC_NOT_FOUND_IN_OBJECT		-208
#define ZLANG_ERROR_PROP_NOT_FOUND_IN_OBJECT		-209
#define ZLANG_ERROR_FROM_DATA_PTR			-210
#define ZLANG_ERROR_GET_DATA_PTR			-211
#define ZLANG_ERROR_DIVISION_BY_ZERO			-212
#define ZLANG_ERROR_EXPECT_OBJECT			-213
#define ZLANG_ERROR_OBJECT_TYPE_NOT_MATCHED		-214
#define ZLANG_ERROR_UNRECOGNIZED_OBJECT			-215
#define ZLANG_ERROR_OBJECT_TYPE_ISNOT_BOOL		-216
#define ZLANG_ERROR_TYPE_OF_OBJECT_NOT_MATCHED		-217
#define ZLANG_ERROR_FUNC_OUTPUT_PROTOTYPE_NOT_MATCHED	-218
#define ZLANG_ERROR_FORMAT_PARAMETERS_NOT_MATCHED	-219
#define ZLANG_ERROR_FORMAT_STRING_TOO_LONG		-220
#define ZLANG_ERROR_FORMAT_STRING_NOT_MATCHED_WITH_PARAMETER	-221
#define ZLANG_ERROR_FORMAT_STRING_NOT_COMPLETED		-222
#define ZLANG_ERROR_OBJECT_HAD_DECLARED			-223
#define ZLANG_ERROR_FUNC_NOT_FOUND			-224
#define ZLANG_ERROR_UNEXPECT_LOGIC_OBJECT_TYPE		-225
#define ZLANG_ERROR_OBJECT_TYPE_ISNOT_FUNCP		-226
#define ZLANG_ERROR_NO_FUNC_IN_FUNCP_OBJECT		-227
#define ZLANG_ERROR_FUNC_PARAMETER_TYPE_NOT_MATCHED		-228
#define ZLANG_ERROR_FUNC_PARAMETERS_COUNT_NOT_MATCHED		-229
#define ZLANG_ERROR_DIRECT_FUNCTIONS_VERSION_NOT_MATCHED	-230
#define ZLANG_ERROR_CANNOT_EVAL_NULL				-231
#define ZLANG_ERROR_CANNOT_REFER_WITH_OTHER_TYPE_VALUE		-232
#define ZLANG_ERROR_CANNOT_CREATE_WITH_OTHER_TYPE_VALUE		-233
#define ZLANG_ERROR_CANNOT_COPY_WITH_OTHER_TYPE_VALUE		-234
#define ZLANG_ERROR_EVAL_TYPE_NOT_MATCHED			-235
#define ZLANG_ERROR_REFER_TYPE_NOT_MATCHED			-236
#define ZLANG_ERROR_MAP_KEY_TYPE_NOT_SUPPORTED			-237
#define ZLANG_ERROR_WRONG_ORDER_ON_CALLING_FUNCTION_IN_OBJECT	-238
#define ZLANG_ERROR_LINK_PROP_TO_ENTITY				-239
#define ZLANG_ERROR_CANNOT_COPY_TO_A_CONSTANT			-240
#define ZLANG_ERROR_CANNOT_REFER_TO_A_CONSTANT			-241
#define ZLANG_ERROR_UNEXPECT_DECORATION_CONST			-242
#define ZLANG_ERROR_LITERAL_INVALID				-243
#define ZLANG_ERROR_PROPERTY_IS_NOT_PUBLIC			-244
#define ZLANG_ERROR_FUNCTION_IS_NOT_PUBLIC			-245
#define ZLANG_ERROR_NO_ENTITY_IN_OBJECT				-246
#define ZLANG_ERROR_OBJECT_NOT_IMPORTED_OR_DEFINED		-247
#define ZLANG_ERROR_DIRECT_PROPERTY_SIZE_NOT_MATCHED		-248
#define ZLANG_ERROR_NO_FUNCTIONS_ENTITY				-249
#define ZLANG_ERROR_ANCESTOR_NOT_FOUND				-250
#define ZLANG_ERROR_LINK_CHARSET_ALIAS				-251
#define ZLANG_ERROR_FORMAT_PLACEHOLDER_NOT_MATCHED_WITH_PARAMETER	-252
#define ZLANG_ERROR_THIS_NO_ASSOCIATED_OBJECT			-253
#define ZLANG_ERROR_UNIMPLEMET_INTERFACE_FUNCTION		-254
#define ZLANG_ERROR_NUMERIC_LITERAL_TOO_LONG			-255
#define ZLANG_ERROR_OBJECT_TYPE_INVALID				-256
#define ZLANG_ERROR_MUTEX_LOCK					-257
#define ZLANG_ERROR_MUTEX_TRYLOCK				-258
#define ZLANG_ERROR_MUTEX_UNLOCK				-259
#define ZLANG_ERROR_OBJECT_TYPE_ISNOT_STRING			-260
#define ZLANG_ERROR_FUNCTION_NULTIPLE_PROTOTYPE			-261

#define ZLANG_ERROR_NO_FROMCHARPTR_FUNC_IN_OBJECT		-300
#define ZLANG_ERROR_NO_TOSTRING_FUNC_IN_OBJECT			-301
#define ZLANG_ERROR_NO_FROMDATAPTR_FUNC_IN_OBJECT		-302
#define ZLANG_ERROR_NO_GETDATAPTR_FUNC_IN_OBJECT		-303
#define ZLANG_ERROR_NO_OPER_PLUS_FUNC_IN_OBJECT			-310
#define ZLANG_ERROR_NO_OPER_MINUS_FUNC_IN_OBJECT		-311
#define ZLANG_ERROR_NO_OPER_MUL_FUNC_IN_OBJECT			-312
#define ZLANG_ERROR_NO_OPER_DIV_FUNC_IN_OBJECT			-313
#define ZLANG_ERROR_NO_OPER_MOD_FUNC_IN_OBJECT			-314
#define ZLANG_ERROR_NO_OPER_NULL				-315
#define ZLANG_ERROR_NO_COMP_EGUAL_FUNC_IN_OBJECT		-320
#define ZLANG_ERROR_NO_COMP_NOTEGUAL_FUNC_IN_OBJECT		-321
#define ZLANG_ERROR_NO_COMP_NOT_FUNC_IN_OBJECT			-322
#define ZLANG_ERROR_NO_COMP_LT_FUNC_IN_OBJECT			-323
#define ZLANG_ERROR_NO_COMP_LE_FUNC_IN_OBJECT			-324
#define ZLANG_ERROR_NO_COMP_GT_FUNC_IN_OBJECT			-325
#define ZLANG_ERROR_NO_COMP_GE_FUNC_IN_OBJECT			-326
#define ZLANG_ERROR_NO_COMP_NULL				-327
#define ZLANG_ERROR_NO_LOGIC_AND_FUNC_IN_OBJECT			-330
#define ZLANG_ERROR_NO_LOGIC_OR_FUNC_IN_OBJECT			-331
#define ZLANG_ERROR_NO_LOGIC_NULL				-332
#define ZLANG_ERROR_NO_UNARYOPER_NEGATIVE_FUNC_IN_OBJECT	-340
#define ZLANG_ERROR_NO_UNARYOPER_NOT_FUNC_IN_OBJECT		-341
#define ZLANG_ERROR_NO_UNARYOPER_PLUS_PLUS_FUNC_IN_OBJECT	-342
#define ZLANG_ERROR_NO_UNARYOPER_MINUS_MINUS_FUNC_IN_OBJECT	-343
#define ZLANG_ERROR_NO_UNARYOPER_NULL				-344
#define ZLANG_ERROR_NO_BIT_AND_FUNC_IN_OBJECT			-345
#define ZLANG_ERROR_NO_BIT_XOR_FUNC_IN_OBJECT			-346
#define ZLANG_ERROR_NO_BIT_OR_FUNC_IN_OBJECT			-347
#define ZLANG_ERROR_NO_UNARYOPER_BIT_REVERSE_FUNC_IN_OBJECT	-348
#define ZLANG_ERROR_NO_BIT_MOVELEFT_FUNC_IN_OBJECT		-349
#define ZLANG_ERROR_NO_BIT_MOVERIGHT_FUNC_IN_OBJECT		-350
#define ZLANG_ERROR_FOREACH_FROM_IS_NULL			-351
#define ZLANG_ERROR_FOREACH_TO_IS_NULL				-352
#define ZLANG_ERROR_CLONE_OBJECT				-353
#define ZLANG_ERROR_SKIP_EXPRESSION				-354

#define ZLANG_ERROR_RUNTIME_DIRECT_FUNCTION_NOT_SETED		-381
#define ZLANG_ERROR_RUNTIME_INVOKE_FUNCTION_NOT_SETED		-382

#define ZLANG_ERROR_CONTINUE_NOT_WITHIN_LOOP_OR_SWITCH		-390
#define ZLANG_ERROR_BREAK_NOT_WITHIN_LOOP_OR_SWITCH		-391

#define ZLANG_ERROR_THROW_ERROR					-393
#define ZLANG_ERROR_THROW_FATAL					-394

#define ZLANG_WARN_INVOKE_METHOD_RETURN				-400
#define ZLANG_ERROR_INVOKE_METHOD_RETURN			-401
#define ZLANG_FATAL_INVOKE_METHOD_RETURN			-402
#define ZLANG_ERROR_CALL_RUNTIME_FUNCTION			-404

#define ZLANG_INFO_END_OF_EXPRESSION		411
#define ZLANG_INFO_END_OF_STATEMENT		412
#define ZLANG_INFO_END_OF_STATEMENT_SEGMENT	413
#define ZLANG_INFO_CONTINUE			421
#define ZLANG_INFO_BREAK			422
#define ZLANG_INFO_RETURN			423
#define ZLANG_INFO_EXIT				424
#define ZLANG_INFO_THROW			427

#define ZLANG_SOURCE_FILE_DATAPAGE_MAGIC	"ZLSP"
#define ZLANG_SOURCE_FILE_DATAPAGE_SIZE		4096

#define ZLANG_TOKEN_DATAPAGE_MAGIC		"ZLTP"
#define ZLANG_TOKEN_DATAPAGE_SIZE		4096*10

#define ZLANG_ACCESSQUALIFIER_PUBLIC	0
#define ZLANG_ACCESSQUALIFIER_PROTECT	1
#define ZLANG_ACCESSQUALIFIER_PRIVATE	2

#define RUNTIME_DEBUG		0
#define RUNTIME_INFO		1
#define RUNTIME_WARN		2
#define RUNTIME_ERROR		3
#define RUNTIME_FATAL		4
#define RUNTIME_NOLOG		9

#define TEST_RUNTIME_LEVEL(_rt_ptr_,_runtime_error_level_)		if( GetRuntimeDebugErrorLevel(_rt_ptr_) <= _runtime_error_level_ )

#define TEST_RUNTIME_DEBUG(_rt_ptr_)					TEST_RUNTIME_LEVEL(_rt_ptr_,RUNTIME_DEBUG)

#define TAB								". "
#if ( defined __linux__ ) || ( defined __unix ) || ( defined _AIX )
#define PRINT_TABS(_rt_ptr_)						{ struct timeval now; struct tm stime; char datetime_buf[10+1+8+1]; int i; gettimeofday(&now,NULL); localtime_r(&(now.tv_sec),&stime); strftime(datetime_buf,sizeof(datetime_buf),"%Y-%m-%d %H:%M:%S",&stime); printf("%s.%06ld | %010lu | %s:%03d,%03d | ",datetime_buf,(long)(now.tv_usec),(unsigned long)(pthread_self()),GetRuntimeSourceFilename(_rt_ptr_),GetRuntimeSourceRow(_rt_ptr_),GetRuntimeSourceColumn(_rt_ptr_)); int debug_recursive_depth=GetRuntimeRecursiveDepth(_rt_ptr_); for(i=0;i<debug_recursive_depth;i++) printf(TAB); }
#elif ( defined _WIN32 )
#define PRINT_TABS(_rt_ptr_)						{ SYSTEMTIME now; GetLocalTime( & now ); int i; printf("%04d-%02d-%02d %02d:%02d:%02d | %010d | %s:%03d,%03d | ",now.wYear,now.wMonth,now.wDay,now.wHour,now.wMinute,now.wSecond,GetCurrentThreadId(),GetRuntimeSourceFilename(_rt_ptr_),GetRuntimeSourceRow(_rt_ptr_),GetRuntimeSourceColumn(_rt_ptr_)); int debug_recursive_depth=GetRuntimeRecursiveDepth(_rt_ptr_); for(i=0;i<debug_recursive_depth;i++) printf(TAB); }
#endif
#define PRINT_SOURCE_FILE_LINE						{ printf(" (%s:%d:%s)",__FILE__,__LINE__,__FUNCTION__); }
#define PRINT_NEWLINE							{ printf("\n"); fflush(stdout); }
#define PRINT_TABS_AND_FORMAT(_rt_ptr_,...)				{ PRINT_TABS(_rt_ptr_) printf( __VA_ARGS__ ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE }
#define TEST_RUNTIME_DEBUG_THEN_PRINT(_rt_ptr_,...)			{ TEST_RUNTIME_DEBUG(_rt_ptr_) { PRINT_TABS_AND_FORMAT( _rt_ptr_ , __VA_ARGS__ ); } }
#define PRINT_HEXSTR(_b_,_b_len_)					{ \
	unsigned char	*line_base = (unsigned char *)(_b_) ; \
	int		line_base_offset = 0 ; \
	int		row , col ; \
	unsigned char	*p = NULL ; \
	\
	printf( "           | 0  1  2  3  4  5  6  7  8  9  A  B  C  D  E  F  | 0123456789ABCDEF\n" ); \
	printf( "-----------+-------------------------------------------------+-----------------\n" ); \
	\
	line_base_offset = 0 ; \
	row = 0 ; \
	for( ; ; ) \
	{ \
		printf( "0x%08X | " , row*16 ); \
		\
		for( col = 0 , p = line_base ; col < 16 ; col++ , p++ ) \
		{ \
			if( line_base_offset + col < (_b_len_) ) \
				printf( "%02X " , (*p) ); \
			else \
				printf( "   " ); \
		} \
		\
		printf( "| " ); \
		\
		for( col = 0 , p = line_base ; col < 16 ; col++ , p++ ) \
		{ \
			if( line_base_offset + col < (_b_len_) ) \
			{ \
				if( 32 <= (*p) && (*p) <= 127 ) \
					printf( "%c" , (*p) ); \
				else \
					printf( "." ); \
			} \
			else \
			{ \
				printf( " " ); \
			} \
		} \
		\
		printf( "\n" ); \
		\
		if( line_base_offset + 16 >= (_b_len_) ) \
			break; \
		\
		line_base += 16 ; \
		line_base_offset += 16 ; \
	} \
	\
	fflush(stdout); \
}
#define TEST_RUNTIME_DEBUG_THEN_PRINT_HEXSTR(_rt_ptr_,_buf_,_buf_len_)	{ TEST_RUNTIME_DEBUG(_rt_ptr_) { PRINT_HEXSTR( _buf_ , _buf_len_ ); } }
#define TEST_RUNTIME_DEBUG_THEN_PRINT_ENTER_FUNCTION(_rt_ptr_)		{ OffsetRuntimeRecursizeDepth(_rt_ptr_,1); TEST_RUNTIME_DEBUG_THEN_PRINT( _rt_ptr_ , "ENTER %s" , __FUNCTION__ ) }
#define TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(_rt_ptr_)	{ TEST_RUNTIME_DEBUG_THEN_PRINT( _rt_ptr_ , "INTERRUPT %s" , __FUNCTION__ ) OffsetRuntimeRecursizeDepth(_rt_ptr_,-1); }
#define TEST_RUNTIME_DEBUG_THEN_PRINT_LEAVE_FUNCTION(_rt_ptr_)		{ TEST_RUNTIME_DEBUG_THEN_PRINT( _rt_ptr_ , "LEAVE %s" , __FUNCTION__ ) OffsetRuntimeRecursizeDepth(_rt_ptr_,-1); }

#define SET_RUNTIME_ERROR(_rt_ptr_,_runtime_error_level_,_runtime_error_no_,...) \
	{ \
		SetRuntimeError( _rt_ptr_ , _runtime_error_level_ , _runtime_error_no_ , __FILE__ , __LINE__ , __VA_ARGS__ ); \
		\
		if( (_runtime_error_level_) == RUNTIME_WARN ) \
		{ \
			TEST_RUNTIME_DEBUG_THEN_PRINT( _rt_ptr_ , "*** WARN : "__VA_ARGS__ ); \
		} \
		else if( (_runtime_error_level_) == RUNTIME_ERROR ) \
		{ \
			TEST_RUNTIME_DEBUG_THEN_PRINT( _rt_ptr_ , "*** ERROR : "__VA_ARGS__ ); \
		} \
	} \

DLLEXPORT char				_zlang_runtime_error_level[10][5+1] ;

DLLEXPORT struct ZlangRuntime *GetZlangRuntime();

DLLEXPORT char				*_zlang_bool_value_str[2] ;

struct ZlangTokenDataUnitHeader;
struct ZlangProperty;
struct ZlangPropertyEntity;
struct ZlangFunctionParameter;
struct ZlangFunction;
struct ZlangObject;
struct ZlangObjectsStack;
struct ZlangObjectsStackFrame;
struct ZlangImport;
struct ZlangRuntime;

typedef struct ZlangObject *ZlangImportObjectFunction( struct ZlangRuntime *rt );
typedef int ZlangImportObjectsFunction( struct ZlangRuntime *rt );

typedef void *ZlangCreateDirectPropertyFunction( struct ZlangRuntime *rt , struct ZlangObject *obj );
typedef void ZlangDestroyDirectPropertyFunction( struct ZlangRuntime *rt , struct ZlangObject *obj );

typedef int ZlangFromCharPtrFunction( struct ZlangRuntime *rt , struct ZlangObject *obj , char *str , int32_t str_len );
typedef int ZlangToStringFunction( struct ZlangRuntime *rt , struct ZlangObject *obj , struct ZlangObject **str_obj );
typedef int ZlangFromDataPtrFunction( struct ZlangRuntime *rt , struct ZlangObject *obj , void *value , int32_t value_len );
typedef int ZlangGetDataPtrFunction( struct ZlangRuntime *rt , struct ZlangObject *obj , void **value , int32_t *value_len );

typedef int ZlangOperatorFunction( struct ZlangRuntime *rt , struct ZlangObject *in_obj1 , struct ZlangObject *in_obj2 , struct ZlangObject **out_obj );
typedef int ZlangUnaryOperatorFunction( struct ZlangRuntime *rt , struct ZlangObject *in_out_obj1 );

typedef int ZlangCompareFunction( struct ZlangRuntime *rt , struct ZlangObject *in_obj1 , struct ZlangObject *in_obj2 , struct ZlangObject *out_obj );

typedef int ZlangLogicFunction( struct ZlangRuntime *rt , struct ZlangObject *in_obj1 , struct ZlangObject *in_obj2 , struct ZlangObject *out_obj );

typedef int ZlangBitFunction( struct ZlangRuntime *rt , struct ZlangObject *in_obj1 , struct ZlangObject *in_obj2 , struct ZlangObject *out_obj );

typedef void ZlangSummarizeDirectPropertySizeFunction( struct ZlangRuntime *rt , struct ZlangObject *obj , size_t *summarized_obj_size , size_t *summarized_direct_prop_size );

typedef int ZlangInvokeFunction( struct ZlangRuntime *rt , struct ZlangObject *obj );

typedef int ZlangDirectFunction_string_Clear( struct ZlangRuntime *rt , struct ZlangObject *obj );
typedef int ZlangDirectFunction_string_PrepareBuffer( struct ZlangRuntime *rt , struct ZlangObject *obj , int32_t prepare_len );
typedef int ZlangDirectFunction_string_GetDirectPropertiesPtr( struct ZlangRuntime *rt , struct ZlangObject *obj , char ***buf , int32_t **buf_size , int32_t **buf_len );

typedef int ZlangDirectFunction_array_Append( struct ZlangRuntime *rt , struct ZlangObject *obj , struct ZlangObject *element , struct ZlangObject **append );
typedef int ZlangDirectFunction_array_InsertBefore( struct ZlangRuntime *rt , struct ZlangObject *obj , int32_t element_no , struct ZlangObject *element , struct ZlangObject **insert );
typedef int ZlangDirectFunction_array_Remove( struct ZlangRuntime *rt , struct ZlangObject *obj , int32_t element_no );
typedef int ZlangDirectFunction_array_RemoveAll( struct ZlangRuntime *rt , struct ZlangObject *obj );
typedef int ZlangDirectFunction_array_Length( struct ZlangRuntime *rt , struct ZlangObject *obj , int32_t *array_length );
typedef int ZlangDirectFunction_array_Get( struct ZlangRuntime *rt , struct ZlangObject *obj , int32_t element_no , struct ZlangObject **element );
typedef int ZlangDirectFunction_array_Set( struct ZlangRuntime *rt , struct ZlangObject *obj , int32_t element_no , struct ZlangObject *element );

typedef int ZlangDirectFunction_list_AddHead( struct ZlangRuntime *rt , struct ZlangObject *obj , struct ZlangObject *element , struct ZlangObject **add );
typedef int ZlangDirectFunction_list_AddTail( struct ZlangRuntime *rt , struct ZlangObject *obj , struct ZlangObject *element , struct ZlangObject **add );
typedef int ZlangDirectFunction_list_RemoveAll( struct ZlangRuntime *rt , struct ZlangObject *obj );
typedef int ZlangDirectFunction_list_GetHead( struct ZlangRuntime *rt , struct ZlangObject *obj , struct ZlangObject **list_node );
typedef int ZlangDirectFunction_list_GetTail( struct ZlangRuntime *rt , struct ZlangObject *obj , struct ZlangObject **list_node );
typedef int ZlangDirectFunction_list_Length( struct ZlangRuntime *rt , struct ZlangObject *obj , int32_t *list_length );
typedef int ZlangDirectFunction_list_IsEmpty( struct ZlangRuntime *rt , struct ZlangObject *obj , unsigned char *b );
typedef int ZlangDirectFunction_list_IsNotEmpty( struct ZlangRuntime *rt , struct ZlangObject *obj , unsigned char *b );
typedef int ZlangDirectFunction_list_node_TravelNext( struct ZlangRuntime *rt , struct ZlangObject *obj );
typedef int ZlangDirectFunction_list_node_TravelPrev( struct ZlangRuntime *rt , struct ZlangObject *obj );
typedef int ZlangDirectFunction_list_node_IsTravelOver( struct ZlangRuntime *rt , struct ZlangObject *obj );
typedef int ZlangDirectFunction_list_node_Remove( struct ZlangRuntime *rt , struct ZlangObject *obj );
typedef int ZlangDirectFunction_list_node_GetMember( struct ZlangRuntime *rt , struct ZlangObject *obj , struct ZlangObject **element );

typedef int ZlangDirectFunction_map_Put( struct ZlangRuntime *rt , struct ZlangObject *obj , struct ZlangObject *key , struct ZlangObject *value , struct ZlangObject **add );
typedef int ZlangDirectFunction_map_Get( struct ZlangRuntime *rt , struct ZlangObject *obj , struct ZlangObject *key , struct ZlangObject **value );
typedef int ZlangDirectFunction_map_TravelNextKey( struct ZlangRuntime *rt , struct ZlangObject *obj , struct ZlangObject **key );
typedef int ZlangDirectFunction_map_TravelPrevKey( struct ZlangRuntime *rt , struct ZlangObject *obj , struct ZlangObject **key );
typedef int ZlangDirectFunction_map_UpdateKey( struct ZlangRuntime *rt , struct ZlangObject *obj , struct ZlangObject *old_key , struct ZlangObject *new_key );
typedef int ZlangDirectFunction_map_Remove( struct ZlangRuntime *rt , struct ZlangObject *obj , struct ZlangObject **key );
typedef int ZlangDirectFunction_map_RemoveAll( struct ZlangRuntime *rt , struct ZlangObject *obj );
typedef int ZlangDirectFunction_map_Length( struct ZlangRuntime *rt , struct ZlangObject *obj , int32_t *map_length );

typedef int ZlangDirectFunction_iterator_TravelFirst( struct ZlangRuntime *rt , struct ZlangObject *obj , struct ZlangObject *collect_obj );
typedef int ZlangDirectFunction_iterator_TravelLast( struct ZlangRuntime *rt , struct ZlangObject *obj , struct ZlangObject *collect_obj );
typedef int ZlangDirectFunction_iterator_TravelNext( struct ZlangRuntime *rt , struct ZlangObject *obj );
typedef int ZlangDirectFunction_iterator_TravelPrev( struct ZlangRuntime *rt , struct ZlangObject *obj );
typedef int ZlangDirectFunction_iterator_IsTravelOver( struct ZlangRuntime *rt , struct ZlangObject *obj );
typedef int ZlangDirectFunction_iterator_GetElement( struct ZlangRuntime *rt , struct ZlangObject *obj , struct ZlangObject **element_obj );
typedef int ZlangDirectFunction_iterator_Remove( struct ZlangRuntime *rt , struct ZlangObject *obj );
typedef int ZlangDirectFunction_iterator_Length( struct ZlangRuntime *rt , struct ZlangObject *obj , int32_t *length );

typedef int ZlangDirectFunction_json_StringToObject( struct ZlangRuntime *rt , struct ZlangObject *obj , struct ZlangObject *t , struct ZlangObject *o );
typedef int ZlangDirectFunction_json_StringToEntityObject( struct ZlangRuntime *rt , struct ZlangObject *obj , struct ZlangObject *t , struct ZlangObject *o );
typedef int ZlangDirectFunction_json_ObjectToString( struct ZlangRuntime *rt , struct ZlangObject *obj , struct ZlangObject *o , struct ZlangObject *t , int32_t obj_to_str_style );

typedef int ZlangDirectFunction_xml_StringToObject( struct ZlangRuntime *rt , struct ZlangObject *obj , struct ZlangObject *t , struct ZlangObject *o );
typedef int ZlangDirectFunction_xml_StringToEntityObject( struct ZlangRuntime *rt , struct ZlangObject *obj , struct ZlangObject *t , struct ZlangObject *o );
typedef int ZlangDirectFunction_xml_ObjectToString( struct ZlangRuntime *rt , struct ZlangObject *obj , struct ZlangObject *o , struct ZlangObject *t , int32_t obj_to_str_style );

typedef int ZlangDirectFunction_string_SetStringValue( struct ZlangRuntime *rt , struct ZlangObject *obj , char *value , int32_t value_len );
typedef int ZlangDirectFunction_string_FormatStringValue( struct ZlangRuntime *rt , struct ZlangObject *obj , char *format , va_list valist );
typedef int ZlangDirectFunction_string_AppendStringValue( struct ZlangRuntime *rt , struct ZlangObject *obj , char *value , int32_t value_len );
typedef int ZlangDirectFunction_string_AppendFormatStringValue( struct ZlangRuntime *rt , struct ZlangObject *obj , char *format , va_list valist );
typedef int ZlangDirectFunction_string_ExpandEnvironmentVar( struct ZlangRuntime *rt , struct ZlangObject *obj );
typedef int ZlangDirectFunction_string_GetStringValue( struct ZlangRuntime *rt , struct ZlangObject *obj , char **value , int32_t *value_len );
typedef int ZlangDirectFunction_bool_SetBoolValue( struct ZlangRuntime *rt , struct ZlangObject *obj , unsigned char value );
typedef int ZlangDirectFunction_bool_GetBoolValue( struct ZlangRuntime *rt , struct ZlangObject *obj , unsigned char *value );
typedef int ZlangDirectFunction_short_SetShortValue( struct ZlangRuntime *rt , struct ZlangObject *obj , int16_t value );
typedef int ZlangDirectFunction_short_GetShortValue( struct ZlangRuntime *rt , struct ZlangObject *obj , int16_t *value );
typedef int ZlangDirectFunction_ushort_SetUShortValue( struct ZlangRuntime *rt , struct ZlangObject *obj , uint16_t value );
typedef int ZlangDirectFunction_ushort_GetUShortValue( struct ZlangRuntime *rt , struct ZlangObject *obj , uint16_t *value );
typedef int ZlangDirectFunction_int_SetIntValue( struct ZlangRuntime *rt , struct ZlangObject *obj , int32_t value );
typedef int ZlangDirectFunction_int_GetIntValue( struct ZlangRuntime *rt , struct ZlangObject *obj , int32_t *value );
typedef int ZlangDirectFunction_uint_SetUIntValue( struct ZlangRuntime *rt , struct ZlangObject *obj , uint32_t value );
typedef int ZlangDirectFunction_uint_GetUIntValue( struct ZlangRuntime *rt , struct ZlangObject *obj , uint32_t *value );
typedef int ZlangDirectFunction_long_SetLongValue( struct ZlangRuntime *rt , struct ZlangObject *obj , int64_t value );
typedef int ZlangDirectFunction_long_GetLongValue( struct ZlangRuntime *rt , struct ZlangObject *obj , int64_t *value );
typedef int ZlangDirectFunction_ulong_SetULongValue( struct ZlangRuntime *rt , struct ZlangObject *obj , uint64_t value );
typedef int ZlangDirectFunction_ulong_GetULongValue( struct ZlangRuntime *rt , struct ZlangObject *obj , uint64_t *value );
typedef int ZlangDirectFunction_float_SetFloatValue( struct ZlangRuntime *rt , struct ZlangObject *obj , float value );
typedef int ZlangDirectFunction_float_GetFloatValue( struct ZlangRuntime *rt , struct ZlangObject *obj , float *value );
typedef int ZlangDirectFunction_double_SetDoubleValue( struct ZlangRuntime *rt , struct ZlangObject *obj , double value );
typedef int ZlangDirectFunction_double_GetDoubleValue( struct ZlangRuntime *rt , struct ZlangObject *obj , double *value );
typedef int ZlangDirectFunction_functionptr_SetFunctionPtr( struct ZlangRuntime *rt , struct ZlangObject *obj , struct ZlangFunction *functionptr );
typedef int ZlangDirectFunction_functionptr_GetFunctionPtr( struct ZlangRuntime *rt , struct ZlangObject *obj , struct ZlangFunction **functionptr );
typedef int ZlangDirectFunction_functionptr_SetObjectPtr( struct ZlangRuntime *rt , struct ZlangObject *obj , struct ZlangObject *master_obj );
typedef int ZlangDirectFunction_functionptr_GetObjectPtr( struct ZlangRuntime *rt , struct ZlangObject *obj , struct ZlangObject **master_obj );

typedef int ZlangDirectFunction_mutex_GetMutexPtr( struct ZlangRuntime *rt , struct ZlangObject *obj , MUTEX **mutex );
typedef int ZlangDirectFunction_condsig_GetMutexPtr( struct ZlangRuntime *rt , struct ZlangObject *obj , MUTEX **mutex );

typedef int ZlangDirectFunction_error_ThrowException( struct ZlangRuntime *rt , struct ZlangObject *obj , int32_t code , char *message );
typedef int ZlangDirectFunction_error_HaveException( struct ZlangRuntime *rt , struct ZlangObject *obj , unsigned char *thrown_flag );
typedef int ZlangDirectFunction_error_GetCode( struct ZlangRuntime *rt , struct ZlangObject *obj , int32_t *code );
typedef int ZlangDirectFunction_error_GetMessage( struct ZlangRuntime *rt , struct ZlangObject *obj , char **message );
typedef int ZlangDirectFunction_error_GetExceptionSourceFilename( struct ZlangRuntime *rt , struct ZlangObject *obj , char **exception_source_filename );
typedef int ZlangDirectFunction_error_GetExceptionSourceRow( struct ZlangRuntime *rt , struct ZlangObject *obj , int32_t *exception_source_row );
typedef int ZlangDirectFunction_error_GetExceptionSourceColumn( struct ZlangRuntime *rt , struct ZlangObject *obj , int32_t *exception_source_col );
typedef int ZlangDirectFunction_error_GetExceptionObjectName( struct ZlangRuntime *rt , struct ZlangObject *obj , char **exception_obj_name );
typedef int ZlangDirectFunction_error_GetExceptionFunctionName( struct ZlangRuntime *rt , struct ZlangObject *obj , char **exception_func_name );
typedef int ZlangDirectFunction_error_GetStackTrace( struct ZlangRuntime *rt , struct ZlangObject *obj , char **stack_trace );
typedef int ZlangDirectFunction_error_CleanException( struct ZlangRuntime *rt , struct ZlangObject *obj );

typedef int ZlangDirectFunction_fatal_ThrowException( struct ZlangRuntime *rt , struct ZlangObject *obj , int32_t code , char *message );
typedef int ZlangDirectFunction_fatal_HaveException( struct ZlangRuntime *rt , struct ZlangObject *obj , unsigned char *thrown_flag );
typedef int ZlangDirectFunction_fatal_GetCode( struct ZlangRuntime *rt , struct ZlangObject *obj , int32_t *code );
typedef int ZlangDirectFunction_fatal_GetMessage( struct ZlangRuntime *rt , struct ZlangObject *obj , char **message );
typedef int ZlangDirectFunction_fatal_GetExceptionSourceFilename( struct ZlangRuntime *rt , struct ZlangObject *obj , char **exception_source_filename );
typedef int ZlangDirectFunction_fatal_GetExceptionSourceRow( struct ZlangRuntime *rt , struct ZlangObject *obj , int32_t *exception_source_row );
typedef int ZlangDirectFunction_fatal_GetExceptionSourceColumn( struct ZlangRuntime *rt , struct ZlangObject *obj , int32_t *exception_source_col );
typedef int ZlangDirectFunction_fatal_GetExceptionObjectName( struct ZlangRuntime *rt , struct ZlangObject *obj , char **exception_obj_name );
typedef int ZlangDirectFunction_fatal_GetExceptionFunctionName( struct ZlangRuntime *rt , struct ZlangObject *obj , char **exception_func_name );
typedef int ZlangDirectFunction_fatal_GetStackTrace( struct ZlangRuntime *rt , struct ZlangObject *obj , char **stack_trace );
typedef int ZlangDirectFunction_fatal_CleanException( struct ZlangRuntime *rt , struct ZlangObject *obj );

struct ZlangDirectFunctions
{
	char					*type_name ;
	
	ZlangCreateDirectPropertyFunction	*create_direct_prop_func ;
	ZlangDestroyDirectPropertyFunction	*destroy_direct_prop_func ;
	
	ZlangFromCharPtrFunction		*from_char_ptr_func ;
	ZlangToStringFunction			*to_string_func ;
	ZlangFromDataPtrFunction		*from_data_ptr_func ;
	ZlangGetDataPtrFunction			*get_data_ptr_func ;
	
	ZlangOperatorFunction			*oper_PLUS_func ;
	ZlangOperatorFunction			*oper_MINUS_func ;
	ZlangOperatorFunction			*oper_MUL_func ;
	ZlangOperatorFunction			*oper_DIV_func ;
	ZlangOperatorFunction			*oper_MOD_func ;
	
	ZlangUnaryOperatorFunction		*unaryoper_NEGATIVE_func ;
	ZlangUnaryOperatorFunction		*unaryoper_NOT_func ;
	ZlangUnaryOperatorFunction		*unaryoper_BIT_REVERSE_func ;
	ZlangUnaryOperatorFunction		*unaryoper_PLUS_PLUS_func ;
	ZlangUnaryOperatorFunction		*unaryoper_MINUS_MINUS_func ;
	
	ZlangCompareFunction			*comp_EGUAL_func ;
	ZlangCompareFunction			*comp_NOTEGUAL_func ;
	ZlangCompareFunction			*comp_LT_func ;
	ZlangCompareFunction			*comp_LE_func ;
	ZlangCompareFunction			*comp_GT_func ;
	ZlangCompareFunction			*comp_GE_func ;
	
	ZlangLogicFunction			*logic_AND_func ;
	ZlangLogicFunction			*logic_OR_func ;
	
	ZlangBitFunction			*bit_AND_func ;
	ZlangBitFunction			*bit_XOR_func ;
	ZlangBitFunction			*bit_OR_func ;
	ZlangBitFunction			*bit_MOVELEFT_func ;
	ZlangBitFunction			*bit_MOVERIGHT_func ;
	
	ZlangSummarizeDirectPropertySizeFunction	*summarize_direct_prop_size_func ;
} ;

#define ZLANG_CHARSET_GB18030		WBYTE_CHARSET_GB18030
#define ZLANG_CHARSET_UTF8		WBYTE_CHARSET_UTF8

struct ZlangCharsetAlias
{
	uint16_t	charset ;
	char		*alias_name ;
	char		*identification ;
	
	struct rb_node	alias_rbtree_node ;
} ;

#define ZLANG_OBJECT_void		"void"
#define ZLANG_OBJECT_vargs		"..."
#define ZLANG_OBJECT_null		"(null)"
#define ZLANG_OBJECT_object		"object"

#define ZLANG_OBJECT_zobject		"zobject"
#define ZLANG_OBJECT_zruntime		"zruntime"
#define ZLANG_OBJECT_error		"error"
#define ZLANG_OBJECT_fatal		"fatal"

#define ZLANG_OBJECT_bool_true		"true"
#define ZLANG_OBJECT_bool_false		"false"
#define ZLANG_null_STRING		"(null)"

#define ZLANG_OBJECT_object		"object"
#define ZLANG_OBJECT_string		"string"
#define ZLANG_OBJECT_bool		"bool"
#define ZLANG_OBJECT_short		"short"
#define ZLANG_OBJECT_ushort		"ushort"
#define ZLANG_OBJECT_int		"int"
#define ZLANG_OBJECT_uint		"uint"
#define ZLANG_OBJECT_long		"long"
#define ZLANG_OBJECT_ulong		"ulong"
#define ZLANG_OBJECT_float		"float"
#define ZLANG_OBJECT_double		"double"
#define ZLANG_OBJECT_array		"array"
#define ZLANG_OBJECT_list		"list"
#define ZLANG_OBJECT_list_node		"list_node"
#define ZLANG_OBJECT_stack		"stack"
#define ZLANG_OBJECT_queue		"queue"
#define ZLANG_OBJECT_map		"map"
#define ZLANG_OBJECT_iterator		"iterator"
#define ZLANG_OBJECT_functionptr	"functionptr"

#define ZLANG_OBJECT_mutex		"mutex"
#define ZLANG_OBJECT_condsig		"condsig"

/*
 * runtime
 */

DLLEXPORT void SetRuntimeError( struct ZlangRuntime *rt , int runtime_error_level , int runtime_error_no , char *debug_internal_file , int debug_internal_lineno , char *runtime_error_string_format , ... );
DLLEXPORT char *GetRuntimeErrorString( struct ZlangRuntime *rt );
DLLEXPORT void FillRuntimeErrorString( struct ZlangRuntime *rt , char *buf , size_t buf_size);
DLLEXPORT void CopyRuntimeError( struct ZlangRuntime *rt , struct ZlangRuntime *copy_rt );
DLLEXPORT int GetRuntimeErrorLevel( struct ZlangRuntime *rt );
DLLEXPORT int GetRuntimeErrorNo( struct ZlangRuntime *rt );
DLLEXPORT int GetRuntimeDebugErrorLevel( struct ZlangRuntime *rt );
DLLEXPORT char *GetRuntimeDebugErrorLevelStringPtr( struct ZlangRuntime *rt );

DLLEXPORT int16_t GetRuntimeCharset( struct ZlangRuntime *rt );
DLLEXPORT char *GetRuntimeCharsetString( struct ZlangRuntime *rt );

DLLEXPORT struct ZlangObject *GetRuntimeInObject( struct ZlangRuntime *rt );
DLLEXPORT struct ZlangFunction *GetRuntimeInFunction( struct ZlangRuntime *rt );

DLLEXPORT int ImportObject( struct ZlangRuntime *rt , struct ZlangObject **pp_obj , char *obj_name , struct ZlangDirectFunctions *direct_funcs , size_t sizeof_direct_funcs , struct ZlangObject *inherit_obj );
DLLEXPORT int ImportObjectToGlobalObjectsHeap( struct ZlangRuntime *rt , struct ZlangObject *obj , struct ZlangObject *inherit_obj );

DLLEXPORT struct ZlangObject *GetZObjectInRuntimeObjectsHeap( struct ZlangRuntime *rt );
DLLEXPORT struct ZlangObject *GetStringObjectInRuntimeObjectsHeap( struct ZlangRuntime *rt );
DLLEXPORT struct ZlangObject *GetBoolObjectInRuntimeObjectsHeap( struct ZlangRuntime *rt );
DLLEXPORT struct ZlangObject *GetShortObjectInRuntimeObjectsHeap( struct ZlangRuntime *rt );
DLLEXPORT struct ZlangObject *GetUShortObjectInRuntimeObjectsHeap( struct ZlangRuntime *rt );
DLLEXPORT struct ZlangObject *GetIntObjectInRuntimeObjectsHeap( struct ZlangRuntime *rt );
DLLEXPORT struct ZlangObject *GetUIntObjectInRuntimeObjectsHeap( struct ZlangRuntime *rt );
DLLEXPORT struct ZlangObject *GetLongObjectInRuntimeObjectsHeap( struct ZlangRuntime *rt );
DLLEXPORT struct ZlangObject *GetULongObjectInRuntimeObjectsHeap( struct ZlangRuntime *rt );
DLLEXPORT struct ZlangObject *GetFloatObjectInRuntimeObjectsHeap( struct ZlangRuntime *rt );
DLLEXPORT struct ZlangObject *GetDoubleObjectInRuntimeObjectsHeap( struct ZlangRuntime *rt );
DLLEXPORT struct ZlangObject *GetArrayObjectInRuntimeObjectsHeap( struct ZlangRuntime *rt );
DLLEXPORT struct ZlangObject *GetListObjectInRuntimeObjectsHeap( struct ZlangRuntime *rt );
DLLEXPORT struct ZlangObject *GetListNodeObjectInRuntimeObjectsHeap( struct ZlangRuntime *rt );
DLLEXPORT struct ZlangObject *GetMapObjectInRuntimeObjectsHeap( struct ZlangRuntime *rt );
DLLEXPORT struct ZlangObject *GetIteratorObjectInRuntimeObjectsHeap( struct ZlangRuntime *rt );
DLLEXPORT struct ZlangObject *GetFunctionPtrObjectInRuntimeObjectsHeap( struct ZlangRuntime *rt );

DLLEXPORT void SetRuntimeFunction_string_AppendFormatFromArgsStack( struct ZlangRuntime *rt , ZlangInvokeFunction *func );
DLLEXPORT void SetRuntimeFunction_string_Clear( struct ZlangRuntime *rt , ZlangDirectFunction_string_Clear *func );
DLLEXPORT void SetRuntimeFunction_string_PrepareBuffer( struct ZlangRuntime *rt , ZlangDirectFunction_string_PrepareBuffer *func );
DLLEXPORT void SetRuntimeFunction_string_GetDirectPropertiesPtr( struct ZlangRuntime *rt , ZlangDirectFunction_string_GetDirectPropertiesPtr *func );
DLLEXPORT int CallRuntimeFunction_string_AppendFormatFromArgsStack( struct ZlangRuntime *rt , struct ZlangObject *obj );
DLLEXPORT int CallRuntimeFunction_string_Clear( struct ZlangRuntime *rt , struct ZlangObject *obj );
DLLEXPORT int CallRuntimeFunction_string_PrepareBuffer( struct ZlangRuntime *rt , struct ZlangObject *obj , int32_t prepare_len );
DLLEXPORT int CallRuntimeFunction_string_GetDirectPropertiesPtr( struct ZlangRuntime *rt , struct ZlangObject *obj , char ***buf , int32_t **buf_size , int32_t **buf_len );

DLLEXPORT void SetRuntimeFunction_array_Append( struct ZlangRuntime *rt , ZlangDirectFunction_array_Append *func );
DLLEXPORT void SetRuntimeFunction_array_InsertBefore( struct ZlangRuntime *rt , ZlangDirectFunction_array_InsertBefore *func );
DLLEXPORT void SetRuntimeFunction_array_Remove( struct ZlangRuntime *rt , ZlangDirectFunction_array_Remove *func );
DLLEXPORT void SetRuntimeFunction_array_RemoveAll( struct ZlangRuntime *rt , ZlangDirectFunction_array_RemoveAll *func );
DLLEXPORT void SetRuntimeFunction_array_Length( struct ZlangRuntime *rt , ZlangDirectFunction_array_Length *func );
DLLEXPORT void SetRuntimeFunction_array_Get( struct ZlangRuntime *rt , ZlangDirectFunction_array_Get *func );
DLLEXPORT void SetRuntimeFunction_array_Set( struct ZlangRuntime *rt , ZlangDirectFunction_array_Set *func );
DLLEXPORT int CallRuntimeFunction_array_Append( struct ZlangRuntime *rt , struct ZlangObject *obj , struct ZlangObject *element , struct ZlangObject **append );
DLLEXPORT int CallRuntimeFunction_array_InsertBefore( struct ZlangRuntime *rt , struct ZlangObject *obj , int32_t element_no , struct ZlangObject *element , struct ZlangObject **insert );
DLLEXPORT int CallRuntimeFunction_array_Remove( struct ZlangRuntime *rt , struct ZlangObject *obj , int32_t element_no );
DLLEXPORT int CallRuntimeFunction_array_RemoveAll( struct ZlangRuntime *rt , struct ZlangObject *obj );
DLLEXPORT int CallRuntimeFunction_array_Length( struct ZlangRuntime *rt , struct ZlangObject *obj , int32_t *array_length );
DLLEXPORT int CallRuntimeFunction_array_Get( struct ZlangRuntime *rt , struct ZlangObject *obj , int32_t element_no , struct ZlangObject **element );
DLLEXPORT int CallRuntimeFunction_array_Set( struct ZlangRuntime *rt , struct ZlangObject *obj , int32_t element_no , struct ZlangObject *element );

DLLEXPORT void SetRuntimeFunction_list_AddHead( struct ZlangRuntime *rt , ZlangDirectFunction_list_AddHead *func );
DLLEXPORT void SetRuntimeFunction_list_AddTail( struct ZlangRuntime *rt , ZlangDirectFunction_list_AddTail *func );
DLLEXPORT void SetRuntimeFunction_list_RemoveAll( struct ZlangRuntime *rt , ZlangDirectFunction_list_RemoveAll *func );
DLLEXPORT void SetRuntimeFunction_list_GetHead( struct ZlangRuntime *rt , ZlangDirectFunction_list_GetHead *func );
DLLEXPORT void SetRuntimeFunction_list_GetTail( struct ZlangRuntime *rt , ZlangDirectFunction_list_GetTail *func );
DLLEXPORT void SetRuntimeFunction_list_Length( struct ZlangRuntime *rt , ZlangDirectFunction_list_Length *func );
DLLEXPORT void SetRuntimeFunction_list_IsEmpty( struct ZlangRuntime *rt , ZlangDirectFunction_list_IsEmpty *func );
DLLEXPORT void SetRuntimeFunction_list_IsNotEmpty( struct ZlangRuntime *rt , ZlangDirectFunction_list_IsNotEmpty *func );
DLLEXPORT void SetRuntimeFunction_list_node_TravelNext( struct ZlangRuntime *rt , ZlangDirectFunction_list_node_TravelNext *func );
DLLEXPORT void SetRuntimeFunction_list_node_TravelPrev( struct ZlangRuntime *rt , ZlangDirectFunction_list_node_TravelPrev *func );
DLLEXPORT void SetRuntimeFunction_list_node_IsTravelOver( struct ZlangRuntime *rt , ZlangDirectFunction_list_node_IsTravelOver *func );
DLLEXPORT void SetRuntimeFunction_list_node_Remove( struct ZlangRuntime *rt , ZlangDirectFunction_list_node_Remove *func );
DLLEXPORT void SetRuntimeFunction_list_node_GetMember( struct ZlangRuntime *rt , ZlangDirectFunction_list_node_GetMember *func );
DLLEXPORT int CallRuntimeFunction_list_AddHead( struct ZlangRuntime *rt , struct ZlangObject *obj , struct ZlangObject *element , struct ZlangObject **add );
DLLEXPORT int CallRuntimeFunction_list_AddTail( struct ZlangRuntime *rt , struct ZlangObject *obj , struct ZlangObject *element , struct ZlangObject **add );
DLLEXPORT int CallRuntimeFunction_list_RemoveAll( struct ZlangRuntime *rt , struct ZlangObject *obj );
DLLEXPORT int CallRuntimeFunction_list_GetHead( struct ZlangRuntime *rt , struct ZlangObject *obj , struct ZlangObject **list_node );
DLLEXPORT int CallRuntimeFunction_list_GetTail( struct ZlangRuntime *rt , struct ZlangObject *obj , struct ZlangObject **list_node );
DLLEXPORT int CallRuntimeFunction_list_Length( struct ZlangRuntime *rt , struct ZlangObject *obj , int32_t *list_length );
DLLEXPORT int CallRuntimeFunction_list_IsEmpty( struct ZlangRuntime *rt , struct ZlangObject *obj , unsigned char *b );
DLLEXPORT int CallRuntimeFunction_list_IsNotEmpty( struct ZlangRuntime *rt , struct ZlangObject *obj , unsigned char *b );
DLLEXPORT int CallRuntimeFunction_list_node_TravelNext( struct ZlangRuntime *rt , struct ZlangObject *obj );
DLLEXPORT int CallRuntimeFunction_list_node_TravelPrev( struct ZlangRuntime *rt , struct ZlangObject *obj );
DLLEXPORT int CallRuntimeFunction_list_node_IsTravelOver( struct ZlangRuntime *rt , struct ZlangObject *obj );
DLLEXPORT int CallRuntimeFunction_list_node_Remove( struct ZlangRuntime *rt , struct ZlangObject *obj );
DLLEXPORT int CallRuntimeFunction_list_node_GetMember( struct ZlangRuntime *rt , struct ZlangObject *obj , struct ZlangObject **element );

DLLEXPORT void SetRuntimeFunction_map_Put( struct ZlangRuntime *rt , ZlangDirectFunction_map_Put *func );
DLLEXPORT void SetRuntimeFunction_map_Get( struct ZlangRuntime *rt , ZlangDirectFunction_map_Get *func );
DLLEXPORT void SetRuntimeFunction_map_TravelNextKey( struct ZlangRuntime *rt , ZlangDirectFunction_map_TravelNextKey *func );
DLLEXPORT void SetRuntimeFunction_map_TravelPrevKey( struct ZlangRuntime *rt , ZlangDirectFunction_map_TravelPrevKey *func );
DLLEXPORT void SetRuntimeFunction_map_UpdateKey( struct ZlangRuntime *rt , ZlangDirectFunction_map_UpdateKey *func );
DLLEXPORT void SetRuntimeFunction_map_Remove( struct ZlangRuntime *rt , ZlangDirectFunction_map_Remove *func );
DLLEXPORT void SetRuntimeFunction_map_RemoveAll( struct ZlangRuntime *rt , ZlangDirectFunction_map_RemoveAll *func );
DLLEXPORT void SetRuntimeFunction_map_Length( struct ZlangRuntime *rt , ZlangDirectFunction_map_Length *func );
DLLEXPORT int CallRuntimeFunction_map_Put( struct ZlangRuntime *rt , struct ZlangObject *obj , struct ZlangObject *key , struct ZlangObject *value , struct ZlangObject **add );
DLLEXPORT int CallRuntimeFunction_map_Get( struct ZlangRuntime *rt , struct ZlangObject *obj , struct ZlangObject *key , struct ZlangObject **value );
DLLEXPORT int CallRuntimeFunction_map_TravelNextKey( struct ZlangRuntime *rt , struct ZlangObject *obj , struct ZlangObject **key );
DLLEXPORT int CallRuntimeFunction_map_TravelPrevKey( struct ZlangRuntime *rt , struct ZlangObject *obj , struct ZlangObject **key );
DLLEXPORT int CallRuntimeFunction_map_UpdateKey( struct ZlangRuntime *rt , struct ZlangObject *obj , struct ZlangObject *old_key , struct ZlangObject *new_value );
DLLEXPORT int CallRuntimeFunction_map_Remove( struct ZlangRuntime *rt , struct ZlangObject *obj , struct ZlangObject **key );
DLLEXPORT int CallRuntimeFunction_map_RemoveAll( struct ZlangRuntime *rt , struct ZlangObject *obj );
DLLEXPORT int CallRuntimeFunction_map_Length( struct ZlangRuntime *rt , struct ZlangObject *obj , int32_t *map_length );

DLLEXPORT void SetRuntimeFunction_iterator_TravelFirst( struct ZlangRuntime *rt , ZlangDirectFunction_iterator_TravelFirst *func );
DLLEXPORT void SetRuntimeFunction_iterator_TravelLast( struct ZlangRuntime *rt , ZlangDirectFunction_iterator_TravelLast *func );
DLLEXPORT void SetRuntimeFunction_iterator_TravelNext( struct ZlangRuntime *rt , ZlangDirectFunction_iterator_TravelNext *func );
DLLEXPORT void SetRuntimeFunction_iterator_TravelPrev( struct ZlangRuntime *rt , ZlangDirectFunction_iterator_TravelPrev *func );
DLLEXPORT void SetRuntimeFunction_iterator_IsTravelOver( struct ZlangRuntime *rt , ZlangDirectFunction_iterator_IsTravelOver *func );
DLLEXPORT void SetRuntimeFunction_iterator_GetElement( struct ZlangRuntime *rt , ZlangDirectFunction_iterator_GetElement *func );
DLLEXPORT void SetRuntimeFunction_iterator_Remove( struct ZlangRuntime *rt , ZlangDirectFunction_iterator_Remove *func );
DLLEXPORT void SetRuntimeFunction_iterator_Length( struct ZlangRuntime *rt , ZlangDirectFunction_iterator_Length *func );
DLLEXPORT int CallRuntimeFunction_iterator_TravelFirst( struct ZlangRuntime *rt , struct ZlangObject *obj , struct ZlangObject *collect_obj );
DLLEXPORT int CallRuntimeFunction_iterator_TravelLast( struct ZlangRuntime *rt , struct ZlangObject *obj , struct ZlangObject *collect_obj );
DLLEXPORT int CallRuntimeFunction_iterator_TravelNext( struct ZlangRuntime *rt , struct ZlangObject *obj );
DLLEXPORT int CallRuntimeFunction_iterator_TravelPrev( struct ZlangRuntime *rt , struct ZlangObject *obj );
DLLEXPORT int CallRuntimeFunction_iterator_IsTravelOver( struct ZlangRuntime *rt , struct ZlangObject *obj );
DLLEXPORT int CallRuntimeFunction_iterator_GetElement( struct ZlangRuntime *rt , struct ZlangObject *obj , struct ZlangObject **element_obj );
DLLEXPORT int CallRuntimeFunction_iterator_Remove( struct ZlangRuntime *rt , struct ZlangObject *obj );
DLLEXPORT int CallRuntimeFunction_iterator_Length( struct ZlangRuntime *rt , struct ZlangObject *obj , int32_t *length );

DLLEXPORT void SetRuntimeFunction_json_StringToObject( struct ZlangRuntime *rt , ZlangDirectFunction_json_StringToObject *func );
DLLEXPORT void SetRuntimeFunction_json_StringToEntityObject( struct ZlangRuntime *rt , ZlangDirectFunction_json_StringToEntityObject *func );
DLLEXPORT void SetRuntimeFunction_json_ObjectToString( struct ZlangRuntime *rt , ZlangDirectFunction_json_ObjectToString *func );
DLLEXPORT ZlangDirectFunction_json_StringToObject *GetRuntimeFunction_json_StringToObject( struct ZlangRuntime *rt );
DLLEXPORT ZlangDirectFunction_json_ObjectToString *GetRuntimeFunction_json_ObjectToString( struct ZlangRuntime *rt );
DLLEXPORT int CallRuntimeFunction_json_StringToObject( struct ZlangRuntime *rt , struct ZlangObject *obj , struct ZlangObject *t , struct ZlangObject *o );
DLLEXPORT int CallRuntimeFunction_json_StringToEntityObject( struct ZlangRuntime *rt , struct ZlangObject *obj , struct ZlangObject *t , struct ZlangObject *o );
DLLEXPORT int CallRuntimeFunction_json_ObjectToString( struct ZlangRuntime *rt , struct ZlangObject *obj , struct ZlangObject *o , struct ZlangObject *t , int32_t obj_to_str_style );

DLLEXPORT void SetRuntimeFunction_xml_StringToObject( struct ZlangRuntime *rt , ZlangDirectFunction_xml_StringToObject *func );
DLLEXPORT void SetRuntimeFunction_xml_StringToEntityObject( struct ZlangRuntime *rt , ZlangDirectFunction_xml_StringToEntityObject *func );
DLLEXPORT void SetRuntimeFunction_xml_ObjectToString( struct ZlangRuntime *rt , ZlangDirectFunction_xml_ObjectToString *func );
DLLEXPORT ZlangDirectFunction_xml_StringToObject *GetRuntimeFunction_xml_StringToObject( struct ZlangRuntime *rt );
DLLEXPORT ZlangDirectFunction_xml_ObjectToString *GetRuntimeFunction_xml_ObjectToString( struct ZlangRuntime *rt );
DLLEXPORT int CallRuntimeFunction_xml_StringToObject( struct ZlangRuntime *rt , struct ZlangObject *obj , struct ZlangObject *t , struct ZlangObject *o );
DLLEXPORT int CallRuntimeFunction_xml_StringToEntityObject( struct ZlangRuntime *rt , struct ZlangObject *obj , struct ZlangObject *t , struct ZlangObject *o );
DLLEXPORT int CallRuntimeFunction_xml_ObjectToString( struct ZlangRuntime *rt , struct ZlangObject *obj , struct ZlangObject *o , struct ZlangObject *t , int32_t obj_to_str_style );

DLLEXPORT void SetRuntimeFunction_string_SetStringValue( struct ZlangRuntime *rt , ZlangDirectFunction_string_SetStringValue *func );
DLLEXPORT void SetRuntimeFunction_string_FormatStringValue( struct ZlangRuntime *rt , ZlangDirectFunction_string_FormatStringValue *func );
DLLEXPORT void SetRuntimeFunction_string_AppendStringValue( struct ZlangRuntime *rt , ZlangDirectFunction_string_AppendStringValue *func );
DLLEXPORT void SetRuntimeFunction_string_AppendFormatStringValue( struct ZlangRuntime *rt , ZlangDirectFunction_string_AppendFormatStringValue *func );
DLLEXPORT void SetRuntimeFunction_string_ExpandEnvironmentVar( struct ZlangRuntime *rt , ZlangDirectFunction_string_ExpandEnvironmentVar *func );
DLLEXPORT void SetRuntimeFunction_string_GetStringValue( struct ZlangRuntime *rt , ZlangDirectFunction_string_GetStringValue *func );
#define STRLEN_OF_STRING		-1
DLLEXPORT int CallRuntimeFunction_string_SetStringValue( struct ZlangRuntime *rt , struct ZlangObject *obj , char *value , int32_t value_len );
DLLEXPORT int CallRuntimeFunction_string_FormatStringValue( struct ZlangRuntime *rt , struct ZlangObject *obj , char *format , ... );
DLLEXPORT int CallRuntimeFunction_string_AppendStringValue( struct ZlangRuntime *rt , struct ZlangObject *obj , char *value , int32_t value_len );
DLLEXPORT int CallRuntimeFunction_string_AppendFormatStringValue( struct ZlangRuntime *rt , struct ZlangObject *obj , char *format , ... );
DLLEXPORT int CallRuntimeFunction_string_ExpandEnvironmentVar( struct ZlangRuntime *rt , struct ZlangObject *obj );
DLLEXPORT int CallRuntimeFunction_string_GetStringValue( struct ZlangRuntime *rt , struct ZlangObject *obj , char **value , int32_t *value_len );
DLLEXPORT void SetRuntimeFunction_bool_SetBoolValue( struct ZlangRuntime *rt , ZlangDirectFunction_bool_SetBoolValue *func );
DLLEXPORT void SetRuntimeFunction_bool_GetBoolValue( struct ZlangRuntime *rt , ZlangDirectFunction_bool_GetBoolValue *func );
DLLEXPORT int CallRuntimeFunction_bool_SetBoolValue( struct ZlangRuntime *rt , struct ZlangObject *obj , unsigned char value );
DLLEXPORT int CallRuntimeFunction_bool_GetBoolValue( struct ZlangRuntime *rt , struct ZlangObject *obj , unsigned char *value );
DLLEXPORT void SetRuntimeFunction_short_SetShortValue( struct ZlangRuntime *rt , ZlangDirectFunction_short_SetShortValue *func );
DLLEXPORT void SetRuntimeFunction_short_GetShortValue( struct ZlangRuntime *rt , ZlangDirectFunction_short_GetShortValue *func );
DLLEXPORT int CallRuntimeFunction_short_SetShortValue( struct ZlangRuntime *rt , struct ZlangObject *obj , int16_t value );
DLLEXPORT int CallRuntimeFunction_short_GetShortValue( struct ZlangRuntime *rt , struct ZlangObject *obj , int16_t *value );
DLLEXPORT void SetRuntimeFunction_ushort_SetUShortValue( struct ZlangRuntime *rt , ZlangDirectFunction_ushort_SetUShortValue *func );
DLLEXPORT void SetRuntimeFunction_ushort_GetUShortValue( struct ZlangRuntime *rt , ZlangDirectFunction_ushort_GetUShortValue *func );
DLLEXPORT int CallRuntimeFunction_ushort_SetUShortValue( struct ZlangRuntime *rt , struct ZlangObject *obj , uint16_t value );
DLLEXPORT int CallRuntimeFunction_ushort_GetUShortValue( struct ZlangRuntime *rt , struct ZlangObject *obj , uint16_t *value );
DLLEXPORT void SetRuntimeFunction_int_SetIntValue( struct ZlangRuntime *rt , ZlangDirectFunction_int_SetIntValue *func );
DLLEXPORT void SetRuntimeFunction_int_GetIntValue( struct ZlangRuntime *rt , ZlangDirectFunction_int_GetIntValue *func );
DLLEXPORT int CallRuntimeFunction_int_SetIntValue( struct ZlangRuntime *rt , struct ZlangObject *obj , int32_t value );
DLLEXPORT int CallRuntimeFunction_int_GetIntValue( struct ZlangRuntime *rt , struct ZlangObject *obj , int32_t *value );
DLLEXPORT void SetRuntimeFunction_uint_SetUIntValue( struct ZlangRuntime *rt , ZlangDirectFunction_uint_SetUIntValue *func );
DLLEXPORT void SetRuntimeFunction_uint_GetUIntValue( struct ZlangRuntime *rt , ZlangDirectFunction_uint_GetUIntValue *func );
DLLEXPORT int CallRuntimeFunction_uint_SetUIntValue( struct ZlangRuntime *rt , struct ZlangObject *obj , uint32_t value );
DLLEXPORT int CallRuntimeFunction_uint_GetUIntValue( struct ZlangRuntime *rt , struct ZlangObject *obj , uint32_t *value );
DLLEXPORT void SetRuntimeFunction_long_SetLongValue( struct ZlangRuntime *rt , ZlangDirectFunction_long_SetLongValue *func );
DLLEXPORT void SetRuntimeFunction_long_GetLongValue( struct ZlangRuntime *rt , ZlangDirectFunction_long_GetLongValue *func );
DLLEXPORT int CallRuntimeFunction_long_SetLongValue( struct ZlangRuntime *rt , struct ZlangObject *obj , int64_t value );
DLLEXPORT int CallRuntimeFunction_long_GetLongValue( struct ZlangRuntime *rt , struct ZlangObject *obj , int64_t *value );
DLLEXPORT void SetRuntimeFunction_ulong_SetULongValue( struct ZlangRuntime *rt , ZlangDirectFunction_ulong_SetULongValue *func );
DLLEXPORT void SetRuntimeFunction_ulong_GetULongValue( struct ZlangRuntime *rt , ZlangDirectFunction_ulong_GetULongValue *func );
DLLEXPORT int CallRuntimeFunction_ulong_SetULongValue( struct ZlangRuntime *rt , struct ZlangObject *obj , uint64_t value );
DLLEXPORT int CallRuntimeFunction_ulong_GetULongValue( struct ZlangRuntime *rt , struct ZlangObject *obj , uint64_t *value );
DLLEXPORT void SetRuntimeFunction_float_SetFloatValue( struct ZlangRuntime *rt , ZlangDirectFunction_float_SetFloatValue *func );
DLLEXPORT void SetRuntimeFunction_float_GetFloatValue( struct ZlangRuntime *rt , ZlangDirectFunction_float_GetFloatValue *func );
DLLEXPORT int CallRuntimeFunction_float_SetFloatValue( struct ZlangRuntime *rt , struct ZlangObject *obj , float value );
DLLEXPORT int CallRuntimeFunction_float_GetFloatValue( struct ZlangRuntime *rt , struct ZlangObject *obj , float *value );
DLLEXPORT void SetRuntimeFunction_double_SetDoubleValue( struct ZlangRuntime *rt , ZlangDirectFunction_double_SetDoubleValue *func );
DLLEXPORT void SetRuntimeFunction_double_GetDoubleValue( struct ZlangRuntime *rt , ZlangDirectFunction_double_GetDoubleValue *func );
DLLEXPORT int CallRuntimeFunction_double_SetDoubleValue( struct ZlangRuntime *rt , struct ZlangObject *obj , double value );
DLLEXPORT int CallRuntimeFunction_double_GetDoubleValue( struct ZlangRuntime *rt , struct ZlangObject *obj , double *value );
DLLEXPORT void SetRuntimeFunction_functionptr_SetFunctionPtr( struct ZlangRuntime *rt , ZlangDirectFunction_functionptr_SetFunctionPtr *func );
DLLEXPORT void SetRuntimeFunction_functionptr_GetFunctionPtr( struct ZlangRuntime *rt , ZlangDirectFunction_functionptr_GetFunctionPtr *func );
DLLEXPORT int CallRuntimeFunction_functionptr_SetFunctionPtr( struct ZlangRuntime *rt , struct ZlangObject *obj , struct ZlangFunction *func );
DLLEXPORT int CallRuntimeFunction_functionptr_GetFunctionPtr( struct ZlangRuntime *rt , struct ZlangObject *obj , struct ZlangFunction **func );
DLLEXPORT void SetRuntimeFunction_functionptr_SetObjectPtr( struct ZlangRuntime *rt , ZlangDirectFunction_functionptr_SetObjectPtr *func );
DLLEXPORT void SetRuntimeFunction_functionptr_GetObjectPtr( struct ZlangRuntime *rt , ZlangDirectFunction_functionptr_GetObjectPtr *func );
DLLEXPORT int CallRuntimeFunction_functionptr_SetObjectPtr( struct ZlangRuntime *rt , struct ZlangObject *obj , struct ZlangObject *master_obj );
DLLEXPORT int CallRuntimeFunction_functionptr_GetObjectPtr( struct ZlangRuntime *rt , struct ZlangObject *obj , struct ZlangObject **master_obj );

DLLEXPORT void SetRuntimeFunction_mutex_GetMutexPtr( struct ZlangRuntime *rt , ZlangDirectFunction_mutex_GetMutexPtr *func );
DLLEXPORT int CallRuntimeFunction_mutex_GetMutexPtr( struct ZlangRuntime *rt , struct ZlangObject *obj , MUTEX **mutex );
DLLEXPORT void SetRuntimeFunction_condsig_GetMutexPtr( struct ZlangRuntime *rt , ZlangDirectFunction_condsig_GetMutexPtr *func );
DLLEXPORT int CallRuntimeFunction_condsig_GetMutexPtr( struct ZlangRuntime *rt , struct ZlangObject *obj , MUTEX **mutex );

DLLEXPORT void SetRuntimeFunction_error_ThrowException( struct ZlangRuntime *rt , ZlangDirectFunction_error_ThrowException *func );
DLLEXPORT void SetRuntimeFunction_error_HaveException( struct ZlangRuntime *rt , ZlangDirectFunction_error_HaveException *func );
DLLEXPORT void SetRuntimeFunction_error_GetCode( struct ZlangRuntime *rt , ZlangDirectFunction_error_GetCode *func );
DLLEXPORT void SetRuntimeFunction_error_GetMessage( struct ZlangRuntime *rt , ZlangDirectFunction_error_GetMessage *func );
DLLEXPORT void SetRuntimeFunction_error_GetExceptionSourceFilename( struct ZlangRuntime *rt , ZlangDirectFunction_error_GetExceptionSourceFilename *func );
DLLEXPORT void SetRuntimeFunction_error_GetExceptionSourceRow( struct ZlangRuntime *rt , ZlangDirectFunction_error_GetExceptionSourceRow *func );
DLLEXPORT void SetRuntimeFunction_error_GetExceptionSourceColumn( struct ZlangRuntime *rt , ZlangDirectFunction_error_GetExceptionSourceColumn *func );
DLLEXPORT void SetRuntimeFunction_error_GetExceptionObjectName( struct ZlangRuntime *rt , ZlangDirectFunction_error_GetExceptionObjectName *func );
DLLEXPORT void SetRuntimeFunction_error_GetExceptionFunctionName( struct ZlangRuntime *rt , ZlangDirectFunction_error_GetExceptionFunctionName *func );
DLLEXPORT void SetRuntimeFunction_error_GetStackTrace( struct ZlangRuntime *rt , ZlangDirectFunction_error_GetStackTrace *func );
DLLEXPORT void SetRuntimeFunction_error_CleanException( struct ZlangRuntime *rt , ZlangDirectFunction_error_CleanException *func );
DLLEXPORT int CallRuntimeFunction_error_ThrowException( struct ZlangRuntime *rt , struct ZlangObject *obj , int32_t code , char *message );
DLLEXPORT int CallRuntimeFunction_error_HaveException( struct ZlangRuntime *rt , struct ZlangObject *obj , unsigned char *thrown_flag );
DLLEXPORT int CallRuntimeFunction_error_GetCode( struct ZlangRuntime *rt , struct ZlangObject *obj , int32_t *code );
DLLEXPORT int CallRuntimeFunction_error_GetMessage( struct ZlangRuntime *rt , struct ZlangObject *obj , char **message );
DLLEXPORT int CallRuntimeFunction_error_GetExceptionSourceFilename( struct ZlangRuntime *rt , struct ZlangObject *obj , char **exception_source_filename );
DLLEXPORT int CallRuntimeFunction_error_GetExceptionSourceRow( struct ZlangRuntime *rt , struct ZlangObject *obj , int32_t *exception_source_row );
DLLEXPORT int CallRuntimeFunction_error_GetExceptionSourceColumn( struct ZlangRuntime *rt , struct ZlangObject *obj , int32_t *exception_source_col );
DLLEXPORT int CallRuntimeFunction_error_GetExceptionObjectName( struct ZlangRuntime *rt , struct ZlangObject *obj , char **exception_obj_name );
DLLEXPORT int CallRuntimeFunction_error_GetExceptionFunctionName( struct ZlangRuntime *rt , struct ZlangObject *obj , char **exception_func_name );
DLLEXPORT int CallRuntimeFunction_error_GetStackTrace( struct ZlangRuntime *rt , struct ZlangObject *obj , char **stack_trace );
DLLEXPORT int CallRuntimeFunction_error_CleanException( struct ZlangRuntime *rt , struct ZlangObject *obj );

DLLEXPORT void SetRuntimeFunction_fatal_ThrowException( struct ZlangRuntime *rt , ZlangDirectFunction_fatal_ThrowException *func );
DLLEXPORT void SetRuntimeFunction_fatal_HaveException( struct ZlangRuntime *rt , ZlangDirectFunction_fatal_HaveException *func );
DLLEXPORT void SetRuntimeFunction_fatal_GetCode( struct ZlangRuntime *rt , ZlangDirectFunction_fatal_GetCode *func );
DLLEXPORT void SetRuntimeFunction_fatal_GetMessage( struct ZlangRuntime *rt , ZlangDirectFunction_fatal_GetMessage *func );
DLLEXPORT void SetRuntimeFunction_fatal_GetExceptionSourceFilename( struct ZlangRuntime *rt , ZlangDirectFunction_fatal_GetExceptionSourceFilename *func );
DLLEXPORT void SetRuntimeFunction_fatal_GetExceptionSourceRow( struct ZlangRuntime *rt , ZlangDirectFunction_fatal_GetExceptionSourceRow *func );
DLLEXPORT void SetRuntimeFunction_fatal_GetExceptionSourceColumn( struct ZlangRuntime *rt , ZlangDirectFunction_fatal_GetExceptionSourceColumn *func );
DLLEXPORT void SetRuntimeFunction_fatal_GetExceptionObjectName( struct ZlangRuntime *rt , ZlangDirectFunction_fatal_GetExceptionObjectName *func );
DLLEXPORT void SetRuntimeFunction_fatal_GetExceptionFunctionName( struct ZlangRuntime *rt , ZlangDirectFunction_fatal_GetExceptionFunctionName *func );
DLLEXPORT void SetRuntimeFunction_fatal_GetStackTrace( struct ZlangRuntime *rt , ZlangDirectFunction_fatal_GetStackTrace *func );
DLLEXPORT void SetRuntimeFunction_fatal_CleanException( struct ZlangRuntime *rt , ZlangDirectFunction_fatal_CleanException *func );
DLLEXPORT int CallRuntimeFunction_fatal_ThrowException( struct ZlangRuntime *rt , struct ZlangObject *obj , int32_t code , char *message );
DLLEXPORT int CallRuntimeFunction_fatal_HaveException( struct ZlangRuntime *rt , struct ZlangObject *obj , unsigned char *thrown_flag );
DLLEXPORT int CallRuntimeFunction_fatal_GetCode( struct ZlangRuntime *rt , struct ZlangObject *obj , int32_t *code );
DLLEXPORT int CallRuntimeFunction_fatal_GetMessage( struct ZlangRuntime *rt , struct ZlangObject *obj , char **message );
DLLEXPORT int CallRuntimeFunction_fatal_GetExceptionSourceFilename( struct ZlangRuntime *rt , struct ZlangObject *obj , char **exception_source_filename );
DLLEXPORT int CallRuntimeFunction_fatal_GetExceptionSourceRow( struct ZlangRuntime *rt , struct ZlangObject *obj , int32_t *exception_source_row );
DLLEXPORT int CallRuntimeFunction_fatal_GetExceptionSourceColumn( struct ZlangRuntime *rt , struct ZlangObject *obj , int32_t *exception_source_col );
DLLEXPORT int CallRuntimeFunction_fatal_GetExceptionObjectName( struct ZlangRuntime *rt , struct ZlangObject *obj , char **exception_obj_name );
DLLEXPORT int CallRuntimeFunction_fatal_GetExceptionFunctionName( struct ZlangRuntime *rt , struct ZlangObject *obj , char **exception_func_name );
DLLEXPORT int CallRuntimeFunction_fatal_GetStackTrace( struct ZlangRuntime *rt , struct ZlangObject *obj , char **stack_trace );
DLLEXPORT int CallRuntimeFunction_fatal_CleanException( struct ZlangRuntime *rt , struct ZlangObject *obj );

#define EXCEPTION_CODE_GENERAL				0

#define EXCEPTION_MESSAGE_INTERNAL_ERROR		"internal error"
#define EXCEPTION_MESSAGE_GENERAL_ERROR			"general error"

#define EXCEPTION_MESSAGE_DIVISION_BY_ZERO		"division by zero"
#define EXCEPTION_MESSAGE_INDEX_OUT_OF_BOUNDS		"index out of bounds"
#define EXCEPTION_MESSAGE_ILLEGAL_ARGUMENT		"illegal argument"
#define EXCEPTION_MESSAGE_ILLEGAL_STATE			"illegal state"
#define EXCEPTION_MESSAGE_NOT_FOUND			"not found"
#define EXCEPTION_MESSAGE_FILE_NOT_FOUND		"file not found"
#define EXCEPTION_MESSAGE_OPEN_WRITE_FILE_FAILED	"open write file not found"
#define EXCEPTION_MESSAGE_PROPERTY_TYPE_NOT_MATCHED	"property type not matched"
#define EXCEPTION_MESSAGE_FORMAT_STRING_NOT_COMPLETED	"format string not completed"
#define EXCEPTION_MESSAGE_FORMAT_PLACEHOLDER_NOT_MATCHED_WITH_PARAMETER		"format place-holder not matched with parameter"
#define EXCEPTION_MESSAGE_FORMAT_NOT_MATCHED_WITH_PARAMETER	"format not matched with parameter"
#define EXCEPTION_MESSAGE_FORMAT_PARAMETERS_NOT_MATCHED		"format parameters not matched , index[%d] count"

#define EXCEPTION_MESSAGE_SET_ERROR_CODE		"set error code"
#define EXCEPTION_MESSAGE_ALLOC_FAILED 			"alloc failed"
#define EXCEPTION_MESSAGE_FORMCHARPTR_FAILED		"FormCharPtr failed"
#define EXCEPTION_MESSAGE_FUNC_PARAMETER_TYPE_NOT_MATCHED	"func parameter type not matched"
#define EXCEPTION_MESSAGE_PARAMETER_INVALID		"parameter invalid"
#define EXCEPTION_MESSAGE_INIT_ARRAY_FAILED		"init array failed"
#define EXCEPTION_MESSAGE_REFER_OBJECT_FAILED		"refer object failed"
#define EXCEPTION_MESSAGE_ARRAY_REMOVE_FAILED		"array remove failed"
#define EXCEPTION_MESSAGE_SET_FUNCTION			"set function"
#define EXCEPTION_MESSAGE_QUERY_GLOBAL_OBJECT_FAILED	"query global object failed"
#define EXCEPTION_MESSAGE_CLONE_OBJECT_FAILED		"clone object failed"
#define EXCEPTION_MESSAGE_CLONE_STRING_OBJECT_FAILED	"clone string object failed"
#define EXCEPTION_MESSAGE_PREPARE_STRING_BUFFER		"prepare string buffer failed"

DLLEXPORT int ThrowErrorException( struct ZlangRuntime *rt , int32_t code , char *message );
DLLEXPORT unsigned char HaveErrorException( struct ZlangRuntime *rt );
DLLEXPORT int GetErrorCode( struct ZlangRuntime *rt , int32_t *code );
DLLEXPORT int GetErrorMessage( struct ZlangRuntime *rt , char **message );
DLLEXPORT int GetErrorExceptionSourceFilename( struct ZlangRuntime *rt , char **exception_source_filename );
DLLEXPORT int GetErrorExceptionSourceRow( struct ZlangRuntime *rt , int32_t *exception_source_row );
DLLEXPORT int GetErrorExceptionSourceColumn( struct ZlangRuntime *rt , int32_t *exception_source_col );
DLLEXPORT int GetErrorExceptionObjectName( struct ZlangRuntime *rt , char **exception_obj_name );
DLLEXPORT int GetErrorExceptionFunctionName( struct ZlangRuntime *rt , char **exception_func_name );
DLLEXPORT int GetErrorStackTrace( struct ZlangRuntime *rt , char **stack_trace );
DLLEXPORT int CleanErrorException( struct ZlangRuntime *rt );

DLLEXPORT int ThrowFatalException( struct ZlangRuntime *rt , int32_t code , char *message );
DLLEXPORT unsigned char HaveFatalException( struct ZlangRuntime *rt );
DLLEXPORT int GetFatalCode( struct ZlangRuntime *rt , int32_t *code );
DLLEXPORT int GetFatalMessage( struct ZlangRuntime *rt , char **message );
DLLEXPORT int GetFatalExceptionSourceFilename( struct ZlangRuntime *rt , char **exception_source_filename );
DLLEXPORT int GetFatalExceptionSourceRow( struct ZlangRuntime *rt , int *exception_source_row );
DLLEXPORT int GetFatalExceptionSourceColumn( struct ZlangRuntime *rt , int *exception_source_col );
DLLEXPORT int GetFatalExceptionObjectName( struct ZlangRuntime *rt , char **exception_obj_name );
DLLEXPORT int GetFatalExceptionFunctionName( struct ZlangRuntime *rt , char **exception_func_name );
DLLEXPORT int GetFatalStackTrace( struct ZlangRuntime *rt , char **stack_trace );
DLLEXPORT int CleanFatalException( struct ZlangRuntime *rt );

DLLEXPORT void GetExceptionThrownFlags( struct ZlangRuntime *rt , unsigned char *error_thrown_flag , unsigned char *fatal_thrown_flag );
DLLEXPORT void SetExceptionThrownFlags( struct ZlangRuntime *rt , unsigned char error_thrown_flag , unsigned char fatal_thrown_flag );

DLLEXPORT int GetRuntimeRecursiveDepth( struct ZlangRuntime *rt );
DLLEXPORT void SetRuntimeRecursiveDepth( struct ZlangRuntime *rt , int recursive_depth );
DLLEXPORT void OffsetRuntimeRecursizeDepth( struct ZlangRuntime *rt , int recursive_depth_delta );
DLLEXPORT char *GetRuntimeSourceFilename( struct ZlangRuntime *rt );
DLLEXPORT int32_t GetRuntimeSourceRow( struct ZlangRuntime *rt );
DLLEXPORT int32_t GetRuntimeSourceColumn( struct ZlangRuntime *rt );

DLLEXPORT int CheckOutputParamterStackAndFunctionParameters( struct ZlangRuntime *rt , struct ZlangFunction *func );
DLLEXPORT int CheckInputParamterStackAndFunctionParameters( struct ZlangRuntime *rt , struct ZlangFunction *func );

DLLEXPORT struct ZlangRuntime *DuplicateRuntime( struct ZlangRuntime *parent_rt , struct ZlangFunction *func );
DLLEXPORT int InvokeEntryFunction( struct ZlangRuntime *rt , struct ZlangObject *in_obj , struct ZlangFunction *entry_func );
DLLEXPORT void FreeRuntime( struct ZlangRuntime *rt );

DLLEXPORT void DebugPrintRawMemory();
DLLEXPORT void DebugPrintStackMemory( struct ZlangRuntime *rt );
DLLEXPORT void DebugPrintHeapMemory( struct ZlangRuntime *rt );

/*
 * object
 */

#define IF_ATOMIC_OBJECT_THEN_LOCK(_obj_) \
	if( (_obj_) && IsAtomicObject(_obj_) ) \
	{ \
		nret = LockAtomicObject( _obj_ ) ; \
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "LockAtomicObject return[%d]" , nret ) \
	} \

#define IF_ATOMIC_OBJECT_THEN_UNLOCK(_obj_) \
	if( (_obj_) && IsAtomicObject(_obj_) ) \
	{ \
		nret = UnlockAtomicObject( _obj_ ) ; \
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "UnlockAtomicObject return[%d]" , nret ) \
	} \

#define IF_ATOMIC_OBJECT_THEN_LOCK_ANOTHER_OBJECT(_obj_,_obj2_) \
	if( (_obj_) && IsAtomicObject(_obj_) ) \
	{ \
		nret = LockAtomicObject( _obj2_ ) ; \
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "LockAtomicObject return[%d]" , nret ) \
	} \

#define IF_ATOMIC_OBJECT_THEN_UNLOCK_ANOTHER_OBJECT(_obj_,_obj2_) \
	if( (_obj_) && IsAtomicObject(_obj_) ) \
	{ \
		nret = UnlockAtomicObject( _obj2_ ) ; \
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "UnlockAtomicObject return[%d]" , nret ) \
	} \

DLLEXPORT int CloneObject( struct ZlangRuntime *rt , struct ZlangObject **pp_obj , char *obj_name , struct ZlangObject *clone_obj );

DLLEXPORT struct ZlangObject *CloneZObject( struct ZlangRuntime *rt , char *obj_name );
DLLEXPORT struct ZlangObject *CloneStringObject( struct ZlangRuntime *rt , char *obj_name );
DLLEXPORT struct ZlangObject *CloneBoolObject( struct ZlangRuntime *rt , char *obj_name );
DLLEXPORT struct ZlangObject *CloneShortObject( struct ZlangRuntime *rt , char *obj_name );
DLLEXPORT struct ZlangObject *CloneUShortObject( struct ZlangRuntime *rt , char *obj_name );
DLLEXPORT struct ZlangObject *CloneIntObject( struct ZlangRuntime *rt , char *obj_name );
DLLEXPORT struct ZlangObject *CloneUIntObject( struct ZlangRuntime *rt , char *obj_name );
DLLEXPORT struct ZlangObject *CloneLongObject( struct ZlangRuntime *rt , char *obj_name );
DLLEXPORT struct ZlangObject *CloneULongObject( struct ZlangRuntime *rt , char *obj_name );
DLLEXPORT struct ZlangObject *CloneFloatObject( struct ZlangRuntime *rt , char *obj_name );
DLLEXPORT struct ZlangObject *CloneDoubleObject( struct ZlangRuntime *rt , char *obj_name );
DLLEXPORT struct ZlangObject *CloneArrayObject( struct ZlangRuntime *rt , char *obj_name );
DLLEXPORT struct ZlangObject *CloneListObject( struct ZlangRuntime *rt , char *obj_name );
DLLEXPORT struct ZlangObject *CloneListNodeObject( struct ZlangRuntime *rt , char *obj_name );
DLLEXPORT struct ZlangObject *CloneMapObject( struct ZlangRuntime *rt , char *obj_name );
DLLEXPORT struct ZlangObject *CloneIteratorObject( struct ZlangRuntime *rt , char *obj_name );
DLLEXPORT struct ZlangObject *CloneFunctionPtrObject( struct ZlangRuntime *rt , char *obj_name );

DLLEXPORT struct ZlangObject *CloneNullObject( struct ZlangRuntime *rt , char *obj_name );

DLLEXPORT int EvalObject( struct ZlangRuntime *rt , struct ZlangObject *obj , struct ZlangObject *copy_obj );
DLLEXPORT int ReferObject( struct ZlangRuntime *rt , struct ZlangObject *obj , struct ZlangObject *refer_obj );
DLLEXPORT int UnreferObject( struct ZlangRuntime *rt , struct ZlangObject *obj );

DLLEXPORT struct ZlangObject *AllocObject( struct ZlangRuntime *rt );
DLLEXPORT int FreeObject( struct ZlangRuntime *rt , struct ZlangObject *obj );
#define ZLANG_INITOPTIONS_NO_CREATE_DIRECTPROPERTY	0x0000000000000001
DLLEXPORT int InitObject( struct ZlangRuntime *rt , struct ZlangObject *obj , char *obj_name , struct ZlangDirectFunctions *direct_funcs , uint64_t init_options );
DLLEXPORT void CleanObject( struct ZlangRuntime *rt , struct ZlangObject *obj );
DLLEXPORT void DestroyObject( struct ZlangRuntime *rt , struct ZlangObject *obj );

DLLEXPORT int FromCharPtr( struct ZlangRuntime *rt , struct ZlangObject *obj , char *str , int32_t str_len );
DLLEXPORT int ToString( struct ZlangRuntime *rt , struct ZlangObject *obj , struct ZlangObject **str_obj );
DLLEXPORT int FromDataPtr( struct ZlangRuntime *rt , struct ZlangObject *obj , void *value , int32_t value_len );
DLLEXPORT int GetDataPtr( struct ZlangRuntime *rt , struct ZlangObject *obj , void **value , int32_t *value_len );

/*
DLLEXPORT size_t GetObjectSize();
*/
DLLEXPORT struct ZlangPropertiesEntity *GetObjectPropertiesEntity( struct ZlangObject *obj );
DLLEXPORT void *GetObjectDirectProperty( struct ZlangObject *obj );
DLLEXPORT struct ZlangDirectFunctions *GetObjectDirectFunctions( struct ZlangObject *obj );
DLLEXPORT void SetObjectDirectFuncstions( struct ZlangObject *obj , struct ZlangDirectFunctions *direct_funcs );
DLLEXPORT struct ZlangFunctionsEntity *GetObjectFunctionsEntity( struct ZlangObject *obj );
DLLEXPORT int SetObjectFunctionsEntity( struct ZlangRuntime *rt , struct ZlangObject *obj , struct ZlangFunctionsEntity *set_funcs_enti );
DLLEXPORT struct ZlangFunctionsEntity *GetObjectAncestorFunctionsEntity( struct ZlangObject *obj );
DLLEXPORT int SetObjectAncestorFunctionsEntity( struct ZlangRuntime *rt , struct ZlangObject *obj , struct ZlangFunctionsEntity *set_funcs_enti );
DLLEXPORT struct ZlangFunctionsEntity *GetObjectAncestorAncestorFunctionsEntity( struct ZlangObject *obj );
DLLEXPORT int SetObjectAncestorAncestorFunctionsEntity( struct ZlangRuntime *rt , struct ZlangObject *obj , struct ZlangFunctionsEntity *set_funcs_enti );
DLLEXPORT unsigned char IsTypeOf( struct ZlangRuntime *rt , struct ZlangObject *obj , struct ZlangObject *type_obj );
#define OBJECTACCESSQUALIFIER_PUBLIC	ZLANG_ACCESSQUALIFIER_PUBLIC
#define OBJECTACCESSQUALIFIER_PROTECT	ZLANG_ACCESSQUALIFIER_PROTECT
#define OBJECTACCESSQUALIFIER_PRIVATE	ZLANG_ACCESSQUALIFIER_PRIVATE
DLLEXPORT void SetObjectAccessQualifier( struct ZlangObject *obj , unsigned char access_qualifier );
DLLEXPORT unsigned char GetObjectAccessQualifier( struct ZlangObject *obj );
DLLEXPORT void SetConstantObject( struct ZlangObject *obj );
DLLEXPORT void UnsetConstantObject( struct ZlangObject *obj );
DLLEXPORT unsigned char IsConstantObject( struct ZlangObject *obj );
DLLEXPORT int SetAtomicObject( struct ZlangObject *obj );
DLLEXPORT int UnsetAtomicObject( struct ZlangObject *obj );
DLLEXPORT unsigned char IsAtomicObject( struct ZlangObject *obj );
DLLEXPORT int LockAtomicObject( struct ZlangObject *obj );
DLLEXPORT int UnlockAtomicObject( struct ZlangObject *obj );
DLLEXPORT void CopyAtomicLock( struct ZlangObject *obj , struct ZlangObject *copy_obj );
DLLEXPORT int SetObjectName( struct ZlangRuntime *rt , struct ZlangObject *obj , char *obj_name );
DLLEXPORT int SetObjectEmbellishName( struct ZlangRuntime *rt , struct ZlangObject *obj , char *obj_name );
DLLEXPORT int SetObjectEmbellishName2( struct ZlangRuntime *rt , struct ZlangObject *obj , char *obj_name );
DLLEXPORT int SetCloneObjectName( struct ZlangRuntime *rt , struct ZlangObject *obj , char *clone_obj_name );
DLLEXPORT char *GetObjectName( struct ZlangObject *obj );
DLLEXPORT char *GetObjectEmbellishName( struct ZlangObject *obj );
DLLEXPORT char *GetObjectEmbellishName2( struct ZlangObject *obj );
DLLEXPORT char *GetCloneObjectName( struct ZlangObject *obj );
DLLEXPORT struct ZlangFunction *GetFunctionsEntityConstractorFunction( struct ZlangFunctionsEntity *funcs_enti );
DLLEXPORT struct ZlangFunction *GetFunctionsEntityDestractorFunction( struct ZlangFunctionsEntity *funcs_enti );
DLLEXPORT unsigned char IsObjectPropertiesEntityNull( struct ZlangObject *obj );
DLLEXPORT unsigned char IsObjectPropertiesEntityNotNull( struct ZlangObject *obj );
#if 0
DLLEXPORT unsigned char IsObjectNull( struct ZlangObject *obj );
#endif
DLLEXPORT int IsObjectTrueValue( struct ZlangRuntime *rt , struct ZlangObject *logic_obj , unsigned char *logic_result );

DLLEXPORT struct ZlangObject *AddPropertyInObject( struct ZlangRuntime *rt , struct ZlangObject *obj , struct ZlangObject *parent_obj , char *prop_name );
DLLEXPORT int RemovePropertyInObject( struct ZlangRuntime *rt , struct ZlangObject *obj , char *prop_name );
DLLEXPORT struct ZlangObject *AddNullPropertyInObject( struct ZlangRuntime *rt , struct ZlangObject *obj , struct ZlangObject *parent_obj , char *prop_name );
DLLEXPORT struct ZlangObject *QueryPropertyInObjectByPropertyName( struct ZlangRuntime *rt , struct ZlangObject *obj , char *prop_name );
DLLEXPORT struct ZlangObject *TravelPropertyInObject( struct ZlangRuntime *rt , struct ZlangObject *obj , struct ZlangObject *prop );

DLLEXPORT struct ZlangObject *QueryObjectByObjectName( struct ZlangRuntime *rt , char *obj_name );
DLLEXPORT struct ZlangObject *QueryObjectByObjectNameInLowerLocalObjectsStack( struct ZlangRuntime *rt , char *obj_name );
DLLEXPORT struct ZlangObject *QueryGlobalObjectByObjectName( struct ZlangRuntime *rt , char *obj_name );

#define SUMMARIZE_SIZE(_p_size_,_increment_size_) { if(_p_size_) *(_p_size_)+=(_increment_size_); }
DLLEXPORT void SummarizeObjectDirectPropertySize( struct ZlangRuntime *rt , struct ZlangObject *obj , size_t *summarized_obj_size , size_t *summarized_direct_prop_size );
DLLEXPORT void SummarizeFunctionSize( struct ZlangRuntime *rt , struct ZlangFunction *func , size_t *summarized_obj_size );
DLLEXPORT void SummarizeObjectFunctionsEntitySize( struct ZlangRuntime *rt , struct ZlangObject *obj , struct ZlangFunctionsEntity *funcs_enti , size_t *summarized_obj_size );
DLLEXPORT void SummarizeObjectPropertiesEntitySize( struct ZlangRuntime *rt , struct ZlangObject *obj , struct ZlangPropertiesEntity *props_enti , size_t *summarized_obj_size , size_t *summarized_direct_prop_size );
DLLEXPORT void SummarizeObjectSize( struct ZlangRuntime *rt , struct ZlangObject *obj , size_t *summarized_obj_size , size_t *summarized_direct_prop_size );

#define DEBUG_PRINT_STRING_LEN_MAX	2048
DLLEXPORT void DebugPrintObject( struct ZlangRuntime *rt , struct ZlangObject *obj );
DLLEXPORT void DebugPrintObjectFunctions( struct ZlangRuntime *rt , struct ZlangObject *obj );
DLLEXPORT void DebugPrintObjectProperties( struct ZlangRuntime *rt , struct ZlangObject *obj );

DLLEXPORT void DebugPrintFunctionsEntity( struct ZlangRuntime *rt , struct ZlangFunctionsEntity *funcs_enti );

/*
 * stack
 */

DLLEXPORT struct ZlangObject *CloneObjectInLocalStack( struct ZlangRuntime *rt , char *obj_name , struct ZlangObject *clone_obj );
DLLEXPORT struct ZlangObject *CloneStringObjectInLocalStack( struct ZlangRuntime *rt , char *obj_name );
DLLEXPORT struct ZlangObject *CloneBoolObjectInLocalStack( struct ZlangRuntime *rt , char *obj_name );
DLLEXPORT struct ZlangObject *CloneShortObjectInLocalStack( struct ZlangRuntime *rt , char *obj_name );
DLLEXPORT struct ZlangObject *CloneUShortObjectInLocalStack( struct ZlangRuntime *rt , char *obj_name );
DLLEXPORT struct ZlangObject *CloneIntObjectInLocalStack( struct ZlangRuntime *rt , char *obj_name );
DLLEXPORT struct ZlangObject *CloneUIntObjectInLocalStack( struct ZlangRuntime *rt , char *obj_name );
DLLEXPORT struct ZlangObject *CloneLongObjectInLocalStack( struct ZlangRuntime *rt , char *obj_name );
DLLEXPORT struct ZlangObject *CloneULongObjectInLocalStack( struct ZlangRuntime *rt , char *obj_name );
DLLEXPORT struct ZlangObject *CloneFloatObjectInLocalStack( struct ZlangRuntime *rt , char *obj_name );
DLLEXPORT struct ZlangObject *CloneDoubleObjectInLocalStack( struct ZlangRuntime *rt , char *obj_name );
DLLEXPORT struct ZlangObject *CloneArrayObjectInLocalStack( struct ZlangRuntime *rt , char *obj_name );
DLLEXPORT struct ZlangObject *CloneListObjectInLocalStack( struct ZlangRuntime *rt , char *obj_name );
DLLEXPORT struct ZlangObject *CloneListNodeObjectInLocalStack( struct ZlangRuntime *rt , char *obj_name );
DLLEXPORT struct ZlangObject *CloneMapObjectInLocalStack( struct ZlangRuntime *rt , char *obj_name );
DLLEXPORT struct ZlangObject *CloneIteratorObjectInLocalStack( struct ZlangRuntime *rt , char *obj_name );
DLLEXPORT struct ZlangObject *CloneFunctionPtrObjectInLocalStack( struct ZlangRuntime *rt , char *obj_name );

DLLEXPORT struct ZlangObject *ReferObjectInLocalStack( struct ZlangRuntime *rt , char *obj_name , struct ZlangObject *refer_obj );

DLLEXPORT struct ZlangObject *QueryObjectInStatementSegment( struct ZlangRuntime *rt , char *obj_name );
DLLEXPORT struct ZlangObject *QueryObjectInLocalObjectsStack( struct ZlangRuntime *rt , char *obj_name );
DLLEXPORT struct ZlangObject *QueryObjectInLowerLocalObjectsStack( struct ZlangRuntime *rt , char *obj_name );

DLLEXPORT int GetLocalObjectCountInStackFrame( struct ZlangRuntime *rt , struct ZlangObjectsStackFrame *local_objs_stack_frame );
DLLEXPORT struct ZlangObject *GetLocalObjectInStackFrame( struct ZlangRuntime *rt , struct ZlangObjectsStackFrame *local_objs_stack_frame , int local_obj_no );
DLLEXPORT int GetOutputParameterCountInStackFrame( struct ZlangRuntime *rt , struct ZlangObjectsStackFrame *local_objs_stack_frame );
DLLEXPORT struct ZlangObject *GetOutputParameterInStackFrame( struct ZlangRuntime *rt , struct ZlangObjectsStackFrame *local_objs_stack_frame , int out_param_no );
DLLEXPORT int GetInputParameterCountInStackFrame( struct ZlangRuntime *rt , struct ZlangObjectsStackFrame *local_objs_stack_frame );
DLLEXPORT struct ZlangObject *GetInputParameterInStackFrame( struct ZlangRuntime *rt , struct ZlangObjectsStackFrame *local_objs_stack_frame , int in_param_no );

DLLEXPORT int GetLocalObjectCountInLocalObjectStack( struct ZlangRuntime *rt );
DLLEXPORT struct ZlangObject *GetLocalObjectInLocalObjectStack( struct ZlangRuntime *rt , int local_obj_no );
DLLEXPORT int GetOutputParameterCountInLocalObjectStack( struct ZlangRuntime *rt );
DLLEXPORT struct ZlangObject *GetOutputParameterInLocalObjectStack( struct ZlangRuntime *rt , int out_param_no );
DLLEXPORT int GetInputParameterCountInLocalObjectStack( struct ZlangRuntime *rt );
DLLEXPORT struct ZlangObject *GetInputParameterInLocalObjectStack( struct ZlangRuntime *rt , int in_param_no );

DLLEXPORT struct ZlangObject *CloneObjectInTmpStack( struct ZlangRuntime *rt , char *obj_name , struct ZlangObject *clone_obj );
DLLEXPORT struct ZlangObject *CloneStringObjectInTmpStack( struct ZlangRuntime *rt , char *obj_name );
DLLEXPORT struct ZlangObject *CloneBoolObjectInTmpStack( struct ZlangRuntime *rt , char *obj_name );
DLLEXPORT struct ZlangObject *CloneShortObjectInTmpStack( struct ZlangRuntime *rt , char *obj_name );
DLLEXPORT struct ZlangObject *CloneUShortObjectInTmpStack( struct ZlangRuntime *rt , char *obj_name );
DLLEXPORT struct ZlangObject *CloneIntObjectInTmpStack( struct ZlangRuntime *rt , char *obj_name );
DLLEXPORT struct ZlangObject *CloneUIntObjectInTmpStack( struct ZlangRuntime *rt , char *obj_name );
DLLEXPORT struct ZlangObject *CloneLongObjectInTmpStack( struct ZlangRuntime *rt , char *obj_name );
DLLEXPORT struct ZlangObject *CloneULongObjectInTmpStack( struct ZlangRuntime *rt , char *obj_name );
DLLEXPORT struct ZlangObject *CloneFloatObjectInTmpStack( struct ZlangRuntime *rt , char *obj_name );
DLLEXPORT struct ZlangObject *CloneDoubleObjectInTmpStack( struct ZlangRuntime *rt , char *obj_name );
DLLEXPORT struct ZlangObject *CloneArrayObjectInTmpStack( struct ZlangRuntime *rt , char *obj_name );
DLLEXPORT struct ZlangObject *CloneListObjectInTmpStack( struct ZlangRuntime *rt , char *obj_name );
DLLEXPORT struct ZlangObject *CloneListNodeObjectInTmpStack( struct ZlangRuntime *rt , char *obj_name );
DLLEXPORT struct ZlangObject *CloneMapObjectInTmpStack( struct ZlangRuntime *rt , char *obj_name );
DLLEXPORT struct ZlangObject *CloneIteratorObjectInTmpStack( struct ZlangRuntime *rt , char *obj_name );
DLLEXPORT struct ZlangObject *CloneFunctionPtrObjectInTmpStack( struct ZlangRuntime *rt , char *obj_name );

DLLEXPORT struct ZlangObject *ReferObjectInTmpStack( struct ZlangRuntime *rt , struct ZlangObject *refer_obj );

DLLEXPORT int SetObjectsStackCallerTokenInfo( struct ZlangRuntime *rt );
DLLEXPORT int SetObjectsStackCalleeTokenInfo( struct ZlangRuntime *rt );
DLLEXPORT char *GetObjectsStackInObjectName( struct ZlangObjectsStackFrame *objs_stack_frame );
DLLEXPORT void SetObjectsStackFullFuncName( struct ZlangObjectsStackFrame *objs_stack_frame , char *full_func_name );
DLLEXPORT char *GetObjectsStackFullFuncName( struct ZlangObjectsStackFrame *objs_stack_frame );
DLLEXPORT void SetLocalObjectsStackFrameInTry( struct ZlangRuntime *rt );
DLLEXPORT unsigned char IsLocalObjectsStackFrameInTry( struct ZlangRuntime *rt );
DLLEXPORT unsigned char IsPreviousLocalObjectsStackFrameInTry( struct ZlangRuntime *rt );
DLLEXPORT struct ZlangObjectsStackFrame *GetCurrentLocalObjectsStackFrame( struct ZlangRuntime *rt );
DLLEXPORT int GetCurrentLocalObjectsStackFrameIndex( struct ZlangRuntime *rt );
DLLEXPORT void MarkInputParamtersTop( struct ZlangObjectsStackFrame *local_objs_stack_frame );
DLLEXPORT void MarkOutputParamtersTop( struct ZlangObjectsStackFrame *local_objs_stack_frame );
DLLEXPORT struct ZlangObjectsStackFrame *GetPreviousObjectsStackFrame( struct ZlangRuntime *rt , struct ZlangObjectsStackFrame *local_objs_stack_frame );
DLLEXPORT struct ZlangObjectsStackFrame *GetFunctionLocalObjectsStackFrame( struct ZlangRuntime *rt );
DLLEXPORT struct ZlangObjectsStackFrame *GetPreviousFunctionObjectsStackFrame( struct ZlangRuntime *rt );
DLLEXPORT struct ZlangObjectsStackFrame *GetFirstLocalObjectsStackFrame( struct ZlangRuntime *rt );

DLLEXPORT struct ZlangObjectsStackFrame *GetCurrentTmpObjectsStackFrame( struct ZlangRuntime *rt );
DLLEXPORT int GetCurrentTmpObjectsStackFrameIndex( struct ZlangRuntime *rt );

DLLEXPORT int IncreaseStackFrame( struct ZlangRuntime *rt , char *full_func_name );
DLLEXPORT int DecreaseStackFrame( struct ZlangRuntime *rt );
DLLEXPORT void SetStackTopFullFuncName( struct ZlangRuntime *rt , char *full_func_name );

/*
 * function
 */

DLLEXPORT size_t GetFunctionSize();
DLLEXPORT struct ZlangFunction *CreateFunction( struct ZlangRuntime *rt , char *func_name , char *full_func_name , ZlangInvokeFunction *invoke_func );
DLLEXPORT void FreeFunction( struct ZlangFunction *func );
DLLEXPORT struct ZlangFunctionParameter *SetFunctionOutputParameterInFunction( struct ZlangRuntime *rt , struct ZlangFunction *func , char *parent_obj_name );
DLLEXPORT struct ZlangFunctionParameter *AddFunctionInputParameterInFunction( struct ZlangRuntime *rt , struct ZlangFunction *func , char *parent_obj_name , char *obj_name );
DLLEXPORT struct ZlangFunctionParameter *TravelFunctionInputParameter( struct ZlangRuntime *rt , struct ZlangFunction *func , struct ZlangFunctionParameter *func_param );
DLLEXPORT char *GetFunctionParameterParentObjectName( struct ZlangFunctionParameter *func_param );
DLLEXPORT char *GetFunctionParameterObjectName( struct ZlangFunctionParameter *func_param );
DLLEXPORT int SetFunctionName( struct ZlangRuntime *rt , struct ZlangFunction *func , char *func_name );
DLLEXPORT int SetFunctionName2( struct ZlangRuntime *rt , struct ZlangFunction *func , char *prefix , char *func_name );
DLLEXPORT char *GetFunctionName( struct ZlangFunction *func );
DLLEXPORT void LinkFullFunctionName( struct ZlangFunction *func , char *full_func_name );
DLLEXPORT int SetFullFunctionName( struct ZlangRuntime *rt , struct ZlangFunction *func , char *full_func_name );
DLLEXPORT char *GetFullFunctionName( struct ZlangFunction *func );
DLLEXPORT struct ZlangFunctionParameter *GetFunctionOutParameter( struct ZlangFunction *func );
#define FUNCTIONACCESSQUALIFIER_PUBLIC	ZLANG_ACCESSQUALIFIER_PUBLIC
#define FUNCTIONACCESSQUALIFIER_PROTECT	ZLANG_ACCESSQUALIFIER_PROTECT
#define FUNCTIONACCESSQUALIFIER_PRIVATE	ZLANG_ACCESSQUALIFIER_PRIVATE
DLLEXPORT void SetFunctionAccessQuelifier( struct ZlangFunction *func , unsigned char access_qualifier );
DLLEXPORT unsigned char GetFunctionAccessQuelifier( struct ZlangFunction *func );
DLLEXPORT void RemoveFunctionInObject( struct ZlangRuntime *rt , struct ZlangObject *obj , struct ZlangFunction *func );

DLLEXPORT struct ZlangFunction *AddFunctionAndParametersInObject( struct ZlangRuntime *rt , struct ZlangObject *obj , char *func_name , char *full_func_name , ZlangInvokeFunction *invoke_func , char *out_param_parent_obj_name , ... );
DLLEXPORT struct ZlangFunction *QueryFunctionByFunctionNameInObject( struct ZlangRuntime *rt , struct ZlangObject *obj , char *func_name );
DLLEXPORT struct ZlangFunction *QueryFunctionByFullFunctionNameInObject( struct ZlangRuntime *rt , struct ZlangObject *obj , char *full_func_name );
DLLEXPORT struct ZlangFunction *TravelFunctionByFullFunctionNameInObject( struct ZlangRuntime *rt , struct ZlangObject *obj , struct ZlangFunction *func );
DLLEXPORT struct ZlangFunction *TravelPrevFunctionByFullFunctionNameInObject( struct ZlangRuntime *rt , struct ZlangObject *obj , struct ZlangFunction *func );

DLLEXPORT struct ZlangFunction *AddGlobalFunctionAndParameters( struct ZlangRuntime *rt , char *func_name , char *full_func_name , ZlangInvokeFunction *invoke_func , char *out_param_parent_obj_name , ... );
DLLEXPORT struct ZlangFunction *QueryGlobalFunctionByFunctionName( struct ZlangRuntime *rt , char *func_name );
DLLEXPORT struct ZlangFunction *QueryGlobalFunctionByFullFunctionName( struct ZlangRuntime *rt , char *full_func_name );
DLLEXPORT struct ZlangFunction *TravelGlobalFunctionByFullFunctionName( struct ZlangRuntime *rt , struct ZlangFunction *func );
DLLEXPORT struct ZlangFunction *TravelPrevGlobalFunctionByFullFunctionName( struct ZlangRuntime *rt , struct ZlangFunction *func );

/*
 * charset
 */

DLLEXPORT int ImportCharsetAlias( struct ZlangRuntime *rt , struct ZlangCharsetAlias *aliases );

/*
 * event
 */

DLLEXPORT int ThrowErrorEvent( struct ZlangRuntime *rt , struct ZlangObject *obj );
DLLEXPORT int ThrowFatalEvent( struct ZlangRuntime *rt , struct ZlangObject *obj );
DLLEXPORT void CleanEvent( struct ZlangRuntime *rt );

/*
 * util
 */

#if ( defined __linux ) || ( defined __unix )
#define ACCESS	access
#elif ( defined _WIN32 )
#define ACCESS	_access
#endif

#ifndef F_OK
#define F_OK	00
#endif

#ifndef R_OK
#define R_OK	04
#endif

#ifndef W_OK
#define W_OK	02
#endif

DLLEXPORT void ReplaceChar( char *str , char src , char dst );
DLLEXPORT char *strptime(const char *buf, const char *fmt, struct tm *tm);
#if ( defined _WIN32 )
DLLEXPORT char *strndup (const char *s, size_t n);
DLLEXPORT void *memrchr(const void *str, int c, size_t len);
DLLEXPORT unsigned int inet_atoi( const char *c_ipaddr );
DLLEXPORT int inet_aton(const char *cp, struct in_addr *ap);
// DLLEXPORT const char *inet_ntop(int af, const void *src, char *dst, socklen_t cnt);
DLLEXPORT int gettimeofday( struct timeval *tv , struct timezone *tz );
#endif
DLLEXPORT char *StrdupEntireFile( char *pathfilename , int *p_file_size );

struct Ipv4DnsResolution
{
	char		ip_str[ 15 + 1 ] ;
#if defined(__linux__)
	in_addr_t	ip_num ;
#elif defined(_WIN32)
	u_long		ip_num ;
#endif
} ;

DLLEXPORT int GetIpByDomainName( char *domain_name , size_t *ip_count , struct Ipv4DnsResolution **ip_array );

DLLEXPORT int IsMatchString(char *match_str, char *object_str, char match_much_characters, char match_one_character);

DLLEXPORT int EscapeString( char *str );

DLLEXPORT int32_t TrimNumericLiteralUnderlineUnsafely( char *token , int32_t token_len , char *numeric_buffer );

#define FILE_TYPE_DIRECTORY			1
#define FILE_TYPE_REGULAR			2
#define FILE_TYPE_CHARACTER			3
#define FILE_TYPE_BLOCK				4
#define FILE_TYPE_FIFO				5
#define FILE_TYPE_SOCKET			6
#define FILE_TYPE_LINK				7
#define FILE_TYPE_OTHER				8
DLLEXPORT int GetDirectoryOrFileType( char *pathfilename );

#define BEGIN_TIMEVAL				__elapse_begin_timeval
#if defined(__linux__)
#define GET_TIMEVAL(_timeval_)			gettimeofday( & (_timeval_) , NULL );
#elif defined(_WIN32)
#define GET_TIMEVAL(_timeval_)			{ SYSTEMTIME st ; (_timeval_).tv_sec = (long)time( NULL ) ; GetLocalTime( & st ); (_timeval_).tv_usec = st.wMilliseconds ; }
#endif
#define DECLARE_BEGIN_TIMEVAL			struct timeval	BEGIN_TIMEVAL

#define DECLARE_BEGIN_TIMEVAL_AND_INIT		DECLARE_BEGIN_TIMEVAL = { 0 } ;
#define GET_BEGIN_TIMEVAL			GET_TIMEVAL(BEGIN_TIMEVAL)
#define GET_END_TIMEVAL_AND_DIFF_ELAPSE(_elapse_diff_timeval_,_elapse_end_timeval_) \
	{ \
		struct timeval __elapse_end_timeval = { 0 } ; \
		GET_TIMEVAL(__elapse_end_timeval); \
		_elapse_diff_timeval_.tv_sec = __elapse_end_timeval.tv_sec - _elapse_end_timeval_.tv_sec ; \
		_elapse_diff_timeval_.tv_usec = __elapse_end_timeval.tv_usec - _elapse_end_timeval_.tv_usec; \
		while( _elapse_diff_timeval_.tv_usec < 0 ) \
		{ \
			_elapse_diff_timeval_.tv_usec += 1000000 ; \
			_elapse_diff_timeval_.tv_sec--; \
		} \
	}
#define CLEAR_ACCUMULATE_DIFF_ELAPSE(_accumulate_elapse_diff_timeval_)	{ _accumulate_elapse_diff_timeval_.tv_sec=0; _accumulate_elapse_diff_timeval_.tv_usec=0; }
#define ACCUMULATE_DIFF_ELAPSE(_accumulate_elapse_diff_timeval_,_elapse_diff_timeval_) \
	{ \
		_accumulate_elapse_diff_timeval_.tv_sec += _elapse_diff_timeval_.tv_sec ; \
		_accumulate_elapse_diff_timeval_.tv_usec += _elapse_diff_timeval_.tv_usec ; \
		while( _accumulate_elapse_diff_timeval_.tv_usec > 1000000 ) \
		{ \
			_accumulate_elapse_diff_timeval_.tv_usec -= 1000000 ; \
			_accumulate_elapse_diff_timeval_.tv_sec++; \
		} \
	} \

DLLEXPORT void GetDirectoryWithFile( char *filename , char *pathname , size_t pathname_size );

#if defined(_WIN32)
DLLEXPORT unsigned int sleep( unsigned int sec );
#define useconds_t	unsigned long
DLLEXPORT int usleep( useconds_t usec );
DLLEXPORT int nanosleep(const struct timespec *rqtp, struct timespec *rmtp);
#endif

#if defined(_WIN32)
// DLLEXPORT char *getenv( char *name );
DLLEXPORT int setenv(const char *name,const char * value,int overwrite);
#endif

#define STACKFRAME_MAX_FUNCTION_NAME		128
#define STACKFRAME_MAX_SOURCE_FILENAME		256

struct StackFrameInfo
{
	char		func_name[ STACKFRAME_MAX_FUNCTION_NAME+1 ] ;
	char		source_filename[ STACKFRAME_MAX_SOURCE_FILENAME+1 ] ;
	size_t		source_fileline ;
	signed long	stack_top ;
	signed long	stack_depth ;
} ;

#define STACKFRAME_MAX_STACK_FRAMES_INFO	256

#if defined(__linux__)
#define CONTEXT				char
#elif defined(_WIN32)
#endif

DLLEXPORT struct StackFrameInfo *GetStackFramesInfo( CONTEXT *p_context , size_t *p_stack_frame_count );

#endif
