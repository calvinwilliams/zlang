/*
 * rbtree_tpl
 * author	: calvin
 * email	: calvinwilliams@163.com
 *
 * Licensed under the LGPL v2.1, see the file LICENSE in base directory.
 */

#include "zlang_in.h"

#include "rbtree_tpl.h"

static void RbtreeFreeObject( void *pv )
{
	struct ZlangObject	*obj = (struct ZlangObject *) pv ;
	
	CleanObject( _zlang_rt , obj ) ;
	ZLFREE( obj );
	
	return;
}

LINK_RBTREENODE_STRING( LinkObjectToRuntimeObjectsHeapByObjectName , struct ZlangRuntime , global_objs_heap , struct ZlangObject , obj_rbtree_node , obj_name )
UNLINK_RBTREENODE( UnlinkObjectFromRuntimeObjectsHeapNode , struct ZlangRuntime , global_objs_heap , struct ZlangObject , obj_rbtree_node )
QUERY_RBTREENODE_STRING( QueryObjectInRuntimeObjectsHeapByObjectName , struct ZlangRuntime , global_objs_heap , struct ZlangObject , obj_rbtree_node , obj_name )
TRAVEL_RBTREENODE( TravelObjectInRuntimeObjectsHeapByObjectName , struct ZlangRuntime , global_objs_heap , struct ZlangObject , obj_rbtree_node )
DESTROY_RBTREE( DestroyRuntimeObjectsHeap , struct ZlangRuntime , global_objs_heap , struct ZlangObject , obj_rbtree_node , RbtreeFreeObject )

static void RbtreeFreeFunction( void *pv )
{
	struct ZlangFunction		*func = (struct ZlangFunction *) pv ;
	
	FreeFunction( func );
	
	return;
}

LINK_RBTREENODE_STRING( LinkFunctionToRuntimeFunctionsTreeByFullFunctionName , struct ZlangRuntime , global_funcs , struct ZlangFunction , this_func , full_func_name )
UNLINK_RBTREENODE( UnlinkFunctionFromRuntimeFunctionsTree , struct ZlangRuntime , global_funcs , struct ZlangFunction , this_func )
QUERY_RBTREENODE_STRING( QueryFunctionInRuntimeFunctionsTreeByFunctionName , struct ZlangRuntime , global_funcs , struct ZlangFunction , this_func , func_name )
QUERY_RBTREENODE_STRING( QueryFunctionInRuntimeFunctionsTreeByFullFunctionName , struct ZlangRuntime , global_funcs , struct ZlangFunction , this_func , full_func_name )
TRAVEL_RBTREENODE( TravelFunctionInRuntimeFunctionsTreeByFullFunctionName , struct ZlangRuntime , global_funcs , struct ZlangFunction , this_func )
TRAVELPREV_RBTREENODE( TravelPrevFunctionInRuntimeFunctionsTreeByFullFunctionName , struct ZlangRuntime , global_funcs , struct ZlangFunction , this_func )
DESTROY_RBTREE( DestroyRuntimeFunctionsTree , struct ZlangRuntime , global_funcs , struct ZlangFunction , this_func , RbtreeFreeFunction )

LINK_RBTREENODE_STRING( LinkPropertyToEntityPropertiesTreeByPropertyName , struct ZlangPropertiesEntity , props , struct ZlangObject , obj_rbtree_node , obj_name )
UNLINK_RBTREENODE( UnlinkPropertyFromEntityPropertiesTree , struct ZlangPropertiesEntity , props , struct ZlangObject , obj_rbtree_node )
QUERY_RBTREENODE_STRING( QueryPropertyInEntityPropertiesTreeByPropertyName , struct ZlangPropertiesEntity , props , struct ZlangObject , obj_rbtree_node , obj_name )
TRAVEL_RBTREENODE( TravelPropertyFromEntityPropertiesTreeByPropertyName , struct ZlangPropertiesEntity , props , struct ZlangObject , obj_rbtree_node )
DESTROY_RBTREE( DestroyEntityPropertiesTree , struct ZlangPropertiesEntity , props , struct ZlangObject , obj_rbtree_node , RbtreeFreeObject )

LINK_RBTREENODE_STRING( LinkFunctionToFunctionsEntityFunctionsTreeByFullFunctionName , struct ZlangFunctionsEntity , funcs , struct ZlangFunction , this_func , full_func_name )
UNLINK_RBTREENODE( UnlinkFunctionFromFunctionsEntityFunctionsTree , struct ZlangFunctionsEntity , funcs , struct ZlangFunction , this_func )
QUERY_RBTREENODE_STRING( QueryFunctionInFunctionsEntityFunctionsTreeByFunctionName , struct ZlangFunctionsEntity , funcs , struct ZlangFunction , this_func , func_name )
QUERY_RBTREENODE_STRING( QueryFunctionInFunctionsEntityFunctionsTreeByFullFunctionName , struct ZlangFunctionsEntity , funcs , struct ZlangFunction , this_func , full_func_name )
TRAVEL_RBTREENODE( TravelFunctionFromFunctionsEntityFunctionsTreeByFullFunctionName , struct ZlangFunctionsEntity , funcs , struct ZlangFunction , this_func )
TRAVELPREV_RBTREENODE( TravelPrevFunctionFromFunctionsEntityFunctionsTreeByFullFunctionName , struct ZlangFunctionsEntity , funcs , struct ZlangFunction , this_func )
DESTROY_RBTREE( DestroyFunctionsEntityFunctionsTree , struct ZlangFunctionsEntity , funcs , struct ZlangFunction , this_func , RbtreeFreeFunction )

static int RbTreeCompareCharsetAlias( void *node1 , void *node2 )
{
	struct ZlangCharsetAlias	*alias1 = (struct ZlangCharsetAlias *)node1 ;
	struct ZlangCharsetAlias	*alias2 = (struct ZlangCharsetAlias *)node2 ;
	
	if( alias1->charset < alias2->charset )
	{
		return -1;
	}
	else if( alias1->charset > alias2->charset )
	{
		return 1;
	}
	else
	{
		return strcmp( alias1->alias_name , alias2->alias_name );
	}
}

static void RbtreeFreeAlias( void *pv )
{
	struct ZlangCharsetAlias	*alias = (struct ZlangCharsetAlias *) pv ;
	
	ZLFREE( alias ) ;
	
	return;
}

LINK_RBTREENODE( LinkAliasToRuntimeCharsetAliasByAliasName , struct ZlangRuntime , global_charset_alias , struct ZlangCharsetAlias , alias_rbtree_node , RbTreeCompareCharsetAlias )
UNLINK_RBTREENODE( UnlinkAliasFromRuntimeCharsetAliasNode , struct ZlangRuntime , global_charset_alias , struct ZlangCharsetAlias , alias_rbtree_node )
QUERY_RBTREENODE( QueryAliasInRuntimeCharsetAliasByAliasName , struct ZlangRuntime , global_charset_alias , struct ZlangCharsetAlias , alias_rbtree_node , RbTreeCompareCharsetAlias )
TRAVEL_RBTREENODE( TravelAliasInRuntimeCharsetAliasByAliasName , struct ZlangRuntime , global_charset_alias , struct ZlangCharsetAlias , alias_rbtree_node )
DESTROY_RBTREE( DestroyRuntimeCharsetAlias , struct ZlangRuntime , global_charset_alias , struct ZlangCharsetAlias , alias_rbtree_node , RbtreeFreeAlias )

static void RbtreeFreeInterface( void *pv )
{
	struct ZlangInterface	*interf = (struct ZlangInterface *) pv ;
	
	DestroyInterface( _zlang_rt , interf ) ;
	
	return;
}

LINK_RBTREENODE_STRING( LinkInterfaceToRuntimeInterfacesHeapByInterfaceName , struct ZlangRuntime , global_interfaces , struct ZlangInterface , interf_rbtree_node , interf_name )
UNLINK_RBTREENODE( UnlinkInterfaceFromRuntimeInterfacesHeapNode , struct ZlangRuntime , global_interfaces , struct ZlangInterface , interf_rbtree_node )
QUERY_RBTREENODE_STRING( QueryInterfaceInRuntimeInterfacesHeapByInterfaceName , struct ZlangRuntime , global_interfaces , struct ZlangInterface , interf_rbtree_node , interf_name )
TRAVEL_RBTREENODE( TravelInterfaceInRuntimeInterfacesHeapByInterfaceName , struct ZlangRuntime , global_interfaces , struct ZlangInterface , interf_rbtree_node )
DESTROY_RBTREE( DestroyRuntimeInterfacesHeap , struct ZlangRuntime , global_interfaces , struct ZlangInterface , interf_rbtree_node , RbtreeFreeInterface )

