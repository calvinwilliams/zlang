/* Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "zlang_in.h"

int InitObjectStack( struct ZlangRuntime *rt , struct ZlangObjectsStack *objs_stack , int default_size , int increase_size )
{
	objs_stack->objs_stack_size = default_size ;
	objs_stack->objs_stack = (struct ZlangObject *)ZLMALLOC( sizeof(struct ZlangObject) * objs_stack->objs_stack_size ) ;
	if( objs_stack->objs_stack == NULL )
	{
		SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_ALLOC , "alloc memory for %s_objs_stack" , (objs_stack==&(rt->local_objs_stack)?"local":"tmp") )
		return ZLANG_ERROR_ALLOC;
	}
	memset( objs_stack->objs_stack , 0x00 , sizeof(struct ZlangObject) * objs_stack->objs_stack_size );
	objs_stack->increase_size = increase_size ;
	
	return 0;
}

void CleanObjectsStack( struct ZlangRuntime *rt , struct ZlangObjectsStack *objs_stack )
{
	ZLFREE( objs_stack->objs_stack );
	
	return;
}

static int TryIncreaseObjectsStack( struct ZlangRuntime *rt , struct ZlangObjectsStack *objs_stack , struct ZlangObjectsStackFrame *objs_stack_frame )
{
	if( objs_stack_frame->stack_local_var_top >= objs_stack->objs_stack_size )
	{
		int			new_objs_stack_size ;
		struct ZlangObject	*new_objs_stack = NULL ;
		
		new_objs_stack_size = objs_stack->objs_stack_size + objs_stack->increase_size ;
		new_objs_stack = (struct ZlangObject *)ZLREALLOC( objs_stack->objs_stack , sizeof(struct ZlangObject) * new_objs_stack_size ) ;
		if( new_objs_stack == NULL )
		{
			SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_ALLOC , "ZLREALLOC memory for %s_objs_stack" , (objs_stack==&(rt->tmp_objs_stack)?"tmp":"local") )
			return ZLANG_ERROR_ALLOC;
		}
		objs_stack->objs_stack = new_objs_stack ;
		objs_stack->objs_stack_size = new_objs_stack_size ;
	}
	
	return 0;
}

struct ZlangObject *PushLocalObjectsStack( struct ZlangRuntime *rt , struct ZlangObjectsStack *objs_stack , struct ZlangObjectsStackFrame *objs_stack_frame )
{
	struct ZlangObject		*obj = NULL ;
	int				nret = 0 ;
	
	nret = TryIncreaseObjectsStack( rt , objs_stack , objs_stack_frame ) ;
	if( nret )
		return NULL;
	
	obj = objs_stack->objs_stack + objs_stack_frame->stack_local_var_top ;
	memset( obj , 0x00 , sizeof(struct ZlangObject) );
	objs_stack_frame->stack_local_var_top++;
	
	return obj;
}

int PopupObjectsStack( struct ZlangRuntime *rt , struct ZlangObjectsStack *objs_stack , struct ZlangObjectsStackFrame *objs_stack_frame )
{
	int				stack_no ;
	struct ZlangObject		*obj = NULL ;
	
	if( objs_stack_frame->stack_local_var_top <= 0 )
		return ZLANG_ERROR_PARAMETER;
	
	stack_no = objs_stack_frame->stack_local_var_top-1 ;
	obj = objs_stack->objs_stack + stack_no ;
	TEST_RUNTIME_DEBUG( rt ) { PRINT_TABS(rt) printf( "    popup " ); DebugPrintObject(rt,obj); printf( " from %s_stack_no[%d]" , (objs_stack==&(rt->local_objs_stack)?"local":"tmp") , stack_no ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE }
	
	CleanObject( rt , obj );
	
	objs_stack_frame->stack_local_var_top--;
	
	return 0;
}

int GetStackTop( struct ZlangRuntime *rt , struct ZlangObjectsStackFrame *objs_stack_frame )
{
	return objs_stack_frame->stack_local_var_top;
}

struct ZlangObject *GetStackTopObject( struct ZlangRuntime *rt , struct ZlangObjectsStack *objs_stack )
{
	struct ZlangObjectsStackFrame	*local_objs_stack_frame = GetCurrentLocalObjectsStackFrame( rt ) ;
	
	return objs_stack->objs_stack + local_objs_stack_frame->stack_local_var_top-1;
}

struct ZlangObject *GetStackObject( struct ZlangRuntime *rt , struct ZlangObjectsStack *objs_stack , int stack_no )
{
	return objs_stack->objs_stack+stack_no;
}

struct ZlangObject *CloneObjectInLocalStack( struct ZlangRuntime *rt , char *obj_name , struct ZlangObject *clone_obj )
{
	struct ZlangObjectsStackFrame	*local_objs_stack_frame = GetCurrentLocalObjectsStackFrame( rt ) ;
	struct ZlangObject		*obj = NULL ;
	int				nret = 0 ;
	
	obj = PushLocalObjectsStack( rt , & (rt->local_objs_stack) , local_objs_stack_frame ) ;
	if( obj == NULL )
		return NULL;
	
	nret = CloneObject( rt , & obj , obj_name , clone_obj ) ;
	if( nret )
		return NULL;
	
	return obj;
}

struct ZlangObject *CloneZObjectInLocalStack( struct ZlangRuntime *rt , char *obj_name )
{
	return CloneObjectInLocalStack( rt , obj_name , GetZObjectInRuntimeObjectsHeap(rt) );
}

struct ZlangObject *CloneStringObjectInLocalStack( struct ZlangRuntime *rt , char *obj_name )
{
	return CloneObjectInLocalStack( rt , obj_name , GetStringObjectInRuntimeObjectsHeap(rt) );
}

struct ZlangObject *CloneBoolObjectInLocalStack( struct ZlangRuntime *rt , char *obj_name )
{
	return CloneObjectInLocalStack( rt , obj_name , GetBoolObjectInRuntimeObjectsHeap(rt) );
}

struct ZlangObject *CloneShortObjectInLocalStack( struct ZlangRuntime *rt , char *obj_name )
{
	return CloneObjectInLocalStack( rt , obj_name , GetShortObjectInRuntimeObjectsHeap(rt) );
}

struct ZlangObject *CloneUShortObjectInLocalStack( struct ZlangRuntime *rt , char *obj_name )
{
	return CloneObjectInLocalStack( rt , obj_name , GetUShortObjectInRuntimeObjectsHeap(rt) );
}

struct ZlangObject *CloneIntObjectInLocalStack( struct ZlangRuntime *rt , char *obj_name )
{
	return CloneObjectInLocalStack( rt , obj_name , GetIntObjectInRuntimeObjectsHeap(rt) );
}

struct ZlangObject *CloneUIntObjectInLocalStack( struct ZlangRuntime *rt , char *obj_name )
{
	return CloneObjectInLocalStack( rt , obj_name , GetUIntObjectInRuntimeObjectsHeap(rt) );
}

struct ZlangObject *CloneLongObjectInLocalStack( struct ZlangRuntime *rt , char *obj_name )
{
	return CloneObjectInLocalStack( rt , obj_name , GetLongObjectInRuntimeObjectsHeap(rt) );
}

struct ZlangObject *CloneULongObjectInLocalStack( struct ZlangRuntime *rt , char *obj_name )
{
	return CloneObjectInLocalStack( rt , obj_name , GetULongObjectInRuntimeObjectsHeap(rt) );
}

struct ZlangObject *CloneFloatObjectInLocalStack( struct ZlangRuntime *rt , char *obj_name )
{
	return CloneObjectInLocalStack( rt , obj_name , GetFloatObjectInRuntimeObjectsHeap(rt) );
}

struct ZlangObject *CloneDoubleObjectInLocalStack( struct ZlangRuntime *rt , char *obj_name )
{
	return CloneObjectInLocalStack( rt , obj_name , GetDoubleObjectInRuntimeObjectsHeap(rt) );
}

struct ZlangObject *CloneArrayObjectInLocalStack( struct ZlangRuntime *rt , char *obj_name )
{
	return CloneObjectInLocalStack( rt , obj_name , GetArrayObjectInRuntimeObjectsHeap(rt) );
}

struct ZlangObject *CloneListObjectInLocalStack( struct ZlangRuntime *rt , char *obj_name )
{
	return CloneObjectInLocalStack( rt , obj_name , GetListObjectInRuntimeObjectsHeap(rt) );
}

struct ZlangObject *CloneListNodeObjectInLocalStack( struct ZlangRuntime *rt , char *obj_name )
{
	return CloneObjectInLocalStack( rt , obj_name , GetListNodeObjectInRuntimeObjectsHeap(rt) );
}

struct ZlangObject *CloneMapObjectInLocalStack( struct ZlangRuntime *rt , char *obj_name )
{
	return CloneObjectInLocalStack( rt , obj_name , GetMapObjectInRuntimeObjectsHeap(rt) );
}

struct ZlangObject *CloneIteratorObjectInLocalStack( struct ZlangRuntime *rt , char *obj_name )
{
	return CloneObjectInLocalStack( rt , obj_name , GetIteratorObjectInRuntimeObjectsHeap(rt) );
}

struct ZlangObject *CloneFunctionPtrObjectInLocalStack( struct ZlangRuntime *rt , char *obj_name )
{
	return CloneObjectInLocalStack( rt , obj_name , GetFunctionPtrObjectInRuntimeObjectsHeap(rt) );
}

struct ZlangObject *PushInputObjectsStack( struct ZlangRuntime *rt , struct ZlangObjectsStack *objs_stack , struct ZlangObjectsStackFrame *objs_stack_frame )
{
	struct ZlangObject		*obj = NULL ;
	int				nret = 0 ;
	
	nret = TryIncreaseObjectsStack( rt , objs_stack , objs_stack_frame ) ;
	if( nret )
		return NULL;
	
	obj = objs_stack->objs_stack + objs_stack_frame->stack_in_params_top ;
	objs_stack_frame->stack_in_params_top++;
	
	return obj;
}

struct ZlangObject *ReferObjectInLocalStack( struct ZlangRuntime *rt , char *obj_name , struct ZlangObject *refer_obj )
{
	struct ZlangObjectsStackFrame	*local_objs_stack_frame = GetCurrentLocalObjectsStackFrame( rt ) ;
	struct ZlangObject		*obj = NULL ;
	int				nret = 0 ;
	
	obj = PushLocalObjectsStack( rt , & (rt->local_objs_stack) , local_objs_stack_frame ) ;
	if( obj == NULL )
		return NULL;
	
	nret = SetObjectName( rt , obj , obj_name ) ;
	if( nret )
		return NULL;
	
	nret = ReferObject( rt , obj , refer_obj ) ;
	if( nret )
		return NULL;
	
	return obj;
}

int InitLocalObjecsStackFrameArray( struct ZlangRuntime *rt )
{
	rt->local_objs_stack_frame_array_allocount = ZLANG_LOCAL_OBJECTS_STACK_INFO_ARRAY_COUNT_DEFAULT ;
	rt->local_objs_stack_frame_array = (struct ZlangObjectsStackFrame *)ZLMALLOC( sizeof(struct ZlangObjectsStackFrame) * rt->local_objs_stack_frame_array_allocount ) ;
	if( rt->local_objs_stack_frame_array == NULL )
	{
		SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_ALLOC , "alloc memory for local_objs_stack_frame_array" )
		return ZLANG_ERROR_ALLOC;
	}
	memset( rt->local_objs_stack_frame_array , 0x00 , sizeof(struct ZlangObjectsStackFrame) * rt->local_objs_stack_frame_array_allocount );
	
	return 0;
}

void CleanLocalObjecsStackFrameArray( struct ZlangRuntime *rt )
{
	ZLFREE( rt->local_objs_stack_frame_array );
	
	return;
}

static int TryIncreaseLocalObjecsStackFrameArray( struct ZlangRuntime *rt )
{
	if( rt->local_objs_stack_frame_array_curridx >= rt->local_objs_stack_frame_array_allocount-1 )
	{
		int				new_objs_stack_frame_array_allocount ;
		struct ZlangObjectsStackFrame	*new_objs_stack_frame_array = NULL ;
		
		new_objs_stack_frame_array_allocount = rt->local_objs_stack_frame_array_allocount + ZLANG_LOCAL_OBJECTS_STACK_INFO_ARRAY_INCREASE_COUNT ;
		new_objs_stack_frame_array = (struct ZlangObjectsStackFrame *)ZLREALLOC( rt->local_objs_stack_frame_array , sizeof(struct ZlangObjectsStackFrame) * new_objs_stack_frame_array_allocount ) ;
		if( new_objs_stack_frame_array == NULL )
		{
			SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_ALLOC , "ZLREALLOC memory for local_objs_stack_frame_array" )
			return ZLANG_ERROR_ALLOC;
		}
		rt->local_objs_stack_frame_array = new_objs_stack_frame_array ;
		rt->local_objs_stack_frame_array_allocount = new_objs_stack_frame_array_allocount ;
	}
	
	return 0;
}

struct ZlangObjectsStackFrame *IncreaseLocalObjectsStackFrame( struct ZlangRuntime *rt , char *full_func_name )
{
	struct ZlangObjectsStackFrame	*old_local_objs_stack_frame = GetCurrentLocalObjectsStackFrame( rt ) ;
	int				stack_local_var_top ;
	struct ZlangObjectsStackFrame	*new_local_objs_stack_frame = NULL ;
	
	stack_local_var_top = GetStackTop( rt , old_local_objs_stack_frame );
	
	TryIncreaseLocalObjecsStackFrameArray( rt );
	
	TEST_RUNTIME_DEBUG( rt )
	{
		struct ZlangObjectsStackFrame *local_objs_stack_frame = GetCurrentLocalObjectsStackFrame( rt ) ;
		PRINT_TABS_AND_FORMAT( rt , "before increase local stack : [%d][%d][%s][%s]-[%d][%d][%d][%d]" , rt->local_objs_stack_frame_array_curridx,local_objs_stack_frame->is_in_try,local_objs_stack_frame->in_obj_name,local_objs_stack_frame->full_func_name , local_objs_stack_frame->stack_bottom,local_objs_stack_frame->stack_in_params_top,local_objs_stack_frame->stack_out_params_top,local_objs_stack_frame->stack_local_var_top )
	}
	
	rt->local_objs_stack_frame_array_curridx++;
	new_local_objs_stack_frame = GetCurrentLocalObjectsStackFrame( rt ) ;
	new_local_objs_stack_frame->stack_local_var_top = stack_local_var_top ;
	new_local_objs_stack_frame->stack_out_params_top = stack_local_var_top ;
	new_local_objs_stack_frame->stack_in_params_top = stack_local_var_top ;
	new_local_objs_stack_frame->stack_bottom = stack_local_var_top ;
	SetObjectsStackCallerTokenInfo( rt );
	/*
	if( rt->in_obj )
		new_local_objs_stack_frame->in_obj_name = GetObjectName(rt->in_obj) ;
	else
		new_local_objs_stack_frame->in_obj_name = NULL ;
	*/
	if( full_func_name )
		new_local_objs_stack_frame->is_in_try = 0 ;
	else
		new_local_objs_stack_frame->is_in_try = old_local_objs_stack_frame->is_in_try ;
	new_local_objs_stack_frame->full_func_name = full_func_name ;
	
	TEST_RUNTIME_DEBUG( rt )
	{
		struct ZlangObjectsStackFrame *local_objs_stack_frame = GetCurrentLocalObjectsStackFrame( rt ) ;
		PRINT_TABS_AND_FORMAT( rt , "after increase local stack : [%d][%d][%s][%s]-[%d][%d][%d][%d]" , rt->local_objs_stack_frame_array_curridx,local_objs_stack_frame->is_in_try,local_objs_stack_frame->in_obj_name,local_objs_stack_frame->full_func_name , local_objs_stack_frame->stack_bottom,local_objs_stack_frame->stack_in_params_top,local_objs_stack_frame->stack_out_params_top,local_objs_stack_frame->stack_local_var_top )
	}
	
	return new_local_objs_stack_frame;
}

struct ZlangObjectsStackFrame *DecreaseLocalObjectsStackFrame( struct ZlangRuntime *rt )
{
	TEST_RUNTIME_DEBUG( rt )
	{
		struct ZlangObjectsStackFrame *local_objs_stack_frame = GetCurrentLocalObjectsStackFrame( rt ) ;
		PRINT_TABS_AND_FORMAT( rt , "before decrease local stack : [%d][%d][%s][%s]-[%d][%d][%d][%d]" , rt->local_objs_stack_frame_array_curridx,local_objs_stack_frame->is_in_try,local_objs_stack_frame->in_obj_name,local_objs_stack_frame->full_func_name , local_objs_stack_frame->stack_bottom,local_objs_stack_frame->stack_in_params_top,local_objs_stack_frame->stack_out_params_top,local_objs_stack_frame->stack_local_var_top )
	}
	
	PopupLocalObjectsStack( rt );
	
	memset( rt->local_objs_stack_frame_array+rt->local_objs_stack_frame_array_curridx , 0x00 , sizeof(struct ZlangObjectsStackFrame) );
	rt->local_objs_stack_frame_array_curridx--;
	
	TEST_RUNTIME_DEBUG( rt )
	{
		struct ZlangObjectsStackFrame *local_objs_stack_frame = GetCurrentLocalObjectsStackFrame( rt ) ;
		PRINT_TABS_AND_FORMAT( rt , "after decrease local stack : [%d][%d][%s][%s]-[%d][%d][%d][%d]" , rt->local_objs_stack_frame_array_curridx,local_objs_stack_frame->is_in_try,local_objs_stack_frame->in_obj_name,local_objs_stack_frame->full_func_name , local_objs_stack_frame->stack_bottom,local_objs_stack_frame->stack_in_params_top,local_objs_stack_frame->stack_out_params_top,local_objs_stack_frame->stack_local_var_top )
	}
	
	return GetCurrentLocalObjectsStackFrame( rt );
}

int SetObjectsStackCallerTokenInfo( struct ZlangRuntime *rt )
{
	struct ZlangObjectsStackFrame	*objs_stack_frame = NULL ;
	struct ZlangTokenDataUnitHeader	*token_info = NULL ;
	char				*token = NULL ;
	/*
	int				nret = 0 ;
	*/
	
	objs_stack_frame = GetCurrentLocalObjectsStackFrame( rt ) ;
	if( rt->travel_token_datapage_header && rt->travel_token_dataunit )
	{
		PEEKTOKEN( rt , token_info , token )
		objs_stack_frame->caller_token_info = token_info ;
	}
	else
	{
		objs_stack_frame->caller_token_info = NULL ;
	}
	
	return 0;
}

int SetObjectsStackCalleeTokenInfo( struct ZlangRuntime *rt )
{
	struct ZlangObjectsStackFrame	*objs_stack_frame = NULL ;
	struct ZlangTokenDataUnitHeader	*token_info = NULL ;
	char				*token = NULL ;
	/*
	int				nret = 0 ;
	*/
	
	objs_stack_frame = GetCurrentLocalObjectsStackFrame( rt ) ;
	if( rt->travel_token_datapage_header && rt->travel_token_dataunit )
	{
		PEEKTOKEN( rt , token_info , token )
		objs_stack_frame->callee_token_info = token_info ;
	}
	else
	{
		objs_stack_frame->callee_token_info = NULL ;
	}
	
	objs_stack_frame->in_obj_name = GetObjectName(rt->in_obj) ;
	
	return 0;
}

char *GetObjectsStackInObjectName( struct ZlangObjectsStackFrame *objs_stack_frame )
{
	return objs_stack_frame->in_obj_name;
}

void SetObjectsStackFullFuncName( struct ZlangObjectsStackFrame *objs_stack_frame , char *full_func_name )
{
	if( full_func_name )
		objs_stack_frame->is_in_try = 0 ;
	objs_stack_frame->full_func_name = full_func_name ;
	
	return;
}

char *GetObjectsStackFullFuncName( struct ZlangObjectsStackFrame *objs_stack_frame )
{
	return objs_stack_frame->full_func_name;
}

void SetLocalObjectsStackFrameInTry( struct ZlangRuntime *rt )
{
	rt->local_objs_stack_frame_array[rt->local_objs_stack_frame_array_curridx].is_in_try = TRUE ;
	return;
}

unsigned char IsLocalObjectsStackFrameInTry( struct ZlangRuntime *rt )
{
	return rt->local_objs_stack_frame_array[rt->local_objs_stack_frame_array_curridx].is_in_try;
}

unsigned char IsPreviousLocalObjectsStackFrameInTry( struct ZlangRuntime *rt )
{
	return rt->local_objs_stack_frame_array[rt->local_objs_stack_frame_array_curridx-1].is_in_try;
}

struct ZlangObjectsStackFrame *GetCurrentLocalObjectsStackFrame( struct ZlangRuntime *rt )
{
	return rt->local_objs_stack_frame_array+rt->local_objs_stack_frame_array_curridx;
}

int GetCurrentLocalObjectsStackFrameIndex( struct ZlangRuntime *rt )
{
	return rt->local_objs_stack_frame_array_curridx;
}

void MarkInputParamtersTop( struct ZlangObjectsStackFrame *local_objs_stack_frame )
{
	local_objs_stack_frame->stack_in_params_top = local_objs_stack_frame->stack_local_var_top ;
	return;
}

void MarkOutputParamtersTop( struct ZlangObjectsStackFrame *local_objs_stack_frame )
{
	local_objs_stack_frame->stack_out_params_top = local_objs_stack_frame->stack_local_var_top ;
	return;
}

struct ZlangObjectsStackFrame *GetPreviousObjectsStackFrame( struct ZlangRuntime *rt , struct ZlangObjectsStackFrame *local_objs_stack_frame )
{
	if( local_objs_stack_frame == rt->local_objs_stack_frame_array )
		return NULL;
	
	return local_objs_stack_frame-1;
}

struct ZlangObjectsStackFrame *GetFunctionLocalObjectsStackFrame( struct ZlangRuntime *rt )
{
	struct ZlangObjectsStackFrame	*local_objs_stack_frame = GetCurrentLocalObjectsStackFrame( rt ) ;
	
	while( local_objs_stack_frame->full_func_name == NULL )
	{
		local_objs_stack_frame = GetPreviousObjectsStackFrame( rt , local_objs_stack_frame ) ;
		if( local_objs_stack_frame == NULL )
			return NULL;
	}
	
	return local_objs_stack_frame;
}

struct ZlangObjectsStackFrame *GetPreviousFunctionObjectsStackFrame( struct ZlangRuntime *rt )
{
	struct ZlangObjectsStackFrame	*local_objs_stack_frame = GetPreviousObjectsStackFrame( rt , GetCurrentLocalObjectsStackFrame(rt) ) ;
	
	while( local_objs_stack_frame->full_func_name == NULL )
	{
		local_objs_stack_frame = GetPreviousObjectsStackFrame( rt , local_objs_stack_frame ) ;
		if( local_objs_stack_frame == NULL )
			return NULL;
	}
	
	return local_objs_stack_frame;
}

struct ZlangObjectsStackFrame *GetFirstLocalObjectsStackFrame( struct ZlangRuntime *rt )
{
	return rt->local_objs_stack_frame_array;
}

int PopupLocalObjectsStack( struct ZlangRuntime *rt )
{
	struct ZlangObjectsStackFrame	*local_objs_stack_frame = GetCurrentLocalObjectsStackFrame( rt ) ;
	int				nret = 0 ;
	
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "  PopupObjectsStack local_objs_stack_frame[%s][%s]-[%d][%d][%d][%d]" , local_objs_stack_frame->in_obj_name,local_objs_stack_frame->full_func_name , local_objs_stack_frame->stack_bottom,local_objs_stack_frame->stack_in_params_top,local_objs_stack_frame->stack_out_params_top,local_objs_stack_frame->stack_local_var_top )
	
	while( local_objs_stack_frame->stack_local_var_top > local_objs_stack_frame->stack_bottom )
	{
		nret = PopupObjectsStack( rt , & (rt->local_objs_stack) , local_objs_stack_frame ) ;
		if( nret )
			return nret;
	}
	
	local_objs_stack_frame->stack_local_var_top = local_objs_stack_frame->stack_bottom ;
	local_objs_stack_frame->stack_out_params_top = local_objs_stack_frame->stack_bottom ;
	local_objs_stack_frame->stack_in_params_top = local_objs_stack_frame->stack_bottom ;
	
	return 0;
}

struct ZlangObject *QueryObjectInStatementSegment( struct ZlangRuntime *rt , char *obj_name )
{
	struct ZlangObjectsStackFrame	*local_objs_stack_frame = GetCurrentLocalObjectsStackFrame( rt ) ;
	struct ZlangObject		*obj = NULL ;
	int				stack_no ;
	
	TEST_RUNTIME_DEBUG( rt )
	{
		DebugStack( rt );
	}
	
	if( obj_name == NULL || obj_name[0] == '\0' )
		return NULL;
	
	for( stack_no = local_objs_stack_frame->stack_local_var_top-1 ; stack_no >= local_objs_stack_frame->stack_out_params_top ; stack_no-- )
	{
		obj = rt->local_objs_stack.objs_stack + stack_no ;
		if( obj->obj_name && obj->obj_name[0] && STRCMP( obj->obj_name , == , obj_name ) )
			return obj;
	}
	
	return NULL;
}

struct ZlangObject *QueryObjectInLocalObjectsStack( struct ZlangRuntime *rt , char *obj_name )
{
	struct ZlangObjectsStackFrame	*local_objs_stack_frame = GetCurrentLocalObjectsStackFrame( rt ) ;
	int				local_objs_stack_frame_curridx = GetCurrentLocalObjectsStackFrameIndex( rt ) ;
	struct ZlangObject		*obj = NULL ;
	int				stack_no ;
	
	if( obj_name == NULL || obj_name[0] == '\0' )
		return NULL;
	
_GOTO_RETRY_QUERY :
	for( stack_no = local_objs_stack_frame->stack_local_var_top-1 ; stack_no >= local_objs_stack_frame->stack_bottom ; stack_no-- )
	{
		obj = rt->local_objs_stack.objs_stack + stack_no ;
		if( obj->obj_name && obj->obj_name[0] && STRCMP( obj->obj_name , == , obj_name ) )
		{
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "QueryObjectInLocalObjectsStack[%s] return obj[%p]" , obj_name , obj )
			return obj;
		}
	}
	
	if( local_objs_stack_frame->full_func_name == NULL && local_objs_stack_frame_curridx > 0 )
	{
		local_objs_stack_frame = GetPreviousObjectsStackFrame( rt , local_objs_stack_frame ) ;
		if( local_objs_stack_frame )
		{
			local_objs_stack_frame_curridx--;
			goto _GOTO_RETRY_QUERY;
		}
	}
	
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "QueryObjectInLocalObjectsStack[%s] return obj[%p]" , obj_name , NULL )
	return NULL;
}

struct ZlangObject *QueryObjectInLowerLocalObjectsStack( struct ZlangRuntime *rt , char *obj_name )
{
	struct ZlangObjectsStackFrame	*local_objs_stack_frame = GetCurrentLocalObjectsStackFrame( rt ) ;
	int				local_objs_stack_frame_curridx = GetCurrentLocalObjectsStackFrameIndex( rt ) ;
	struct ZlangObject		*obj = NULL ;
	int				stack_no ;
	
	if( obj_name == NULL || obj_name[0] == '\0' )
		return NULL;
	
	local_objs_stack_frame = GetPreviousObjectsStackFrame( rt , local_objs_stack_frame ) ;
	if( local_objs_stack_frame == NULL )
		return NULL;
	local_objs_stack_frame_curridx--;
	
_GOTO_RETRY_QUERY :
	for( stack_no = local_objs_stack_frame->stack_local_var_top-1 ; stack_no >= local_objs_stack_frame->stack_bottom ; stack_no-- )
	{
		obj = rt->local_objs_stack.objs_stack + stack_no ;
		if( obj->obj_name && obj->obj_name[0] && STRCMP( obj->obj_name , == , obj_name ) )
		{
			TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "QueryObjectInLocalObjectsStack[%s] return obj[%p]" , obj_name , obj )
			return obj;
		}
	}
	
	if( local_objs_stack_frame->full_func_name == NULL && local_objs_stack_frame_curridx > 0 )
	{
		local_objs_stack_frame = GetPreviousObjectsStackFrame( rt , local_objs_stack_frame ) ;
		if( local_objs_stack_frame )
		{
			local_objs_stack_frame_curridx--;
			goto _GOTO_RETRY_QUERY;
		}
	}
	
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "QueryObjectInLocalObjectsStack[%s] return obj[%p]" , obj_name , NULL )
	return NULL;
}

int GetLocalObjectCountInStackFrame( struct ZlangRuntime *rt , struct ZlangObjectsStackFrame *local_objs_stack_frame )
{
	return local_objs_stack_frame->stack_local_var_top-local_objs_stack_frame->stack_out_params_top;
}

struct ZlangObject *GetLocalObjectInStackFrame( struct ZlangRuntime *rt , struct ZlangObjectsStackFrame *local_objs_stack_frame , int local_obj_no )
{
	return rt->local_objs_stack.objs_stack+local_objs_stack_frame->stack_out_params_top+(local_obj_no-1);
}

int GetOutputParameterCountInStackFrame( struct ZlangRuntime *rt , struct ZlangObjectsStackFrame *local_objs_stack_frame )
{
	return local_objs_stack_frame->stack_out_params_top-local_objs_stack_frame->stack_in_params_top;
}

struct ZlangObject *GetOutputParameterInStackFrame( struct ZlangRuntime *rt , struct ZlangObjectsStackFrame *local_objs_stack_frame , int out_param_no )
{
	return rt->local_objs_stack.objs_stack+local_objs_stack_frame->stack_in_params_top+out_param_no-1;
}

int GetInputParameterCountInStackFrame( struct ZlangRuntime *rt , struct ZlangObjectsStackFrame *local_objs_stack_frame )
{
	return local_objs_stack_frame->stack_in_params_top-local_objs_stack_frame->stack_bottom;
}

struct ZlangObject *GetInputParameterInStackFrame( struct ZlangRuntime *rt , struct ZlangObjectsStackFrame *local_objs_stack_frame , int in_param_no )
{
	return rt->local_objs_stack.objs_stack+local_objs_stack_frame->stack_bottom+(in_param_no-1);
}

int GetLocalObjectCountInLocalObjectStack( struct ZlangRuntime *rt )
{
	struct ZlangObjectsStackFrame	*local_objs_stack_frame = GetCurrentLocalObjectsStackFrame( rt ) ;
	
	return GetLocalObjectCountInStackFrame(rt,local_objs_stack_frame);
}

struct ZlangObject *GetLocalObjectInLocalObjectStack( struct ZlangRuntime *rt , int local_obj_no )
{
	struct ZlangObjectsStackFrame	*local_objs_stack_frame = GetCurrentLocalObjectsStackFrame( rt ) ;
	
	return GetLocalObjectInStackFrame(rt,local_objs_stack_frame,local_obj_no);
}

int GetOutputParameterCountInLocalObjectStack( struct ZlangRuntime *rt )
{
	struct ZlangObjectsStackFrame	*local_objs_stack_frame = GetFunctionLocalObjectsStackFrame( rt ) ;
	
	return GetOutputParameterCountInStackFrame(rt,local_objs_stack_frame);
}

struct ZlangObject *GetOutputParameterInLocalObjectStack( struct ZlangRuntime *rt , int out_param_no )
{
	struct ZlangObjectsStackFrame	*local_objs_stack_frame = GetFunctionLocalObjectsStackFrame( rt ) ;
	
	if( out_param_no < 1 || out_param_no > local_objs_stack_frame->stack_out_params_top-local_objs_stack_frame->stack_in_params_top )
		return NULL;
	
	return GetOutputParameterInStackFrame(rt,local_objs_stack_frame,out_param_no);
}

int GetInputParameterCountInLocalObjectStack( struct ZlangRuntime *rt )
{
	struct ZlangObjectsStackFrame	*local_objs_stack_frame = GetFunctionLocalObjectsStackFrame( rt ) ;
	
	return GetInputParameterCountInStackFrame(rt,local_objs_stack_frame);
}

struct ZlangObject *GetInputParameterInLocalObjectStack( struct ZlangRuntime *rt , int in_param_no )
{
	struct ZlangObjectsStackFrame	*local_objs_stack_frame = GetFunctionLocalObjectsStackFrame( rt ) ;
	
	if( in_param_no < 1 || in_param_no > local_objs_stack_frame->stack_in_params_top-local_objs_stack_frame->stack_bottom )
		return NULL;
	
	return GetInputParameterInStackFrame(rt,local_objs_stack_frame,in_param_no);
}

void DebugLocalObjectsStack( struct ZlangRuntime *rt )
{
	int				local_objs_stack_frame_curridx = GetCurrentLocalObjectsStackFrameIndex( rt ) ;
	struct ZlangObjectsStackFrame	*local_objs_stack_frame = GetCurrentLocalObjectsStackFrame( rt ) ;
	int				stack_no ;
	struct ZlangObject		*obj = NULL ;
	
_GOTO_RETRY_DEBUG_OUTPUT :
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "LOCAL-OBJECTS-STACK - local_objs_stack_frame[%d][%d][%s][%s]-[%d][%d][%d][%d]" , local_objs_stack_frame_curridx,local_objs_stack_frame->is_in_try,local_objs_stack_frame->in_obj_name,local_objs_stack_frame->full_func_name , local_objs_stack_frame->stack_bottom,local_objs_stack_frame->stack_in_params_top,local_objs_stack_frame->stack_out_params_top,local_objs_stack_frame->stack_local_var_top );
	
	for( stack_no = local_objs_stack_frame->stack_local_var_top-1 ; stack_no >= local_objs_stack_frame->stack_out_params_top ; stack_no-- )
	{
		obj = rt->local_objs_stack.objs_stack + stack_no ;
		TEST_RUNTIME_DEBUG( rt ) { PRINT_TABS(rt) printf( TAB "local stack object - stack_no[%d] : local object : " , stack_no ); DebugPrintObject( rt , obj ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE }
	}
	
	for( stack_no = local_objs_stack_frame->stack_out_params_top-1 ; stack_no >= local_objs_stack_frame->stack_in_params_top ; stack_no-- )
	{
		obj = rt->local_objs_stack.objs_stack + stack_no ;
		TEST_RUNTIME_DEBUG( rt ) { PRINT_TABS(rt) printf( TAB "local stack object - stack_no[%d] : out param : " , stack_no ); DebugPrintObject( rt , obj ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE }
	}
	
	for( stack_no = local_objs_stack_frame->stack_in_params_top-1 ; stack_no >= local_objs_stack_frame->stack_bottom ; stack_no-- )
	{
		obj = rt->local_objs_stack.objs_stack + stack_no ;
		TEST_RUNTIME_DEBUG( rt ) { PRINT_TABS(rt) printf( TAB "local stack object - stack_no[%d] : in param : " , stack_no ); DebugPrintObject( rt , obj ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE }
	}
	
	if( local_objs_stack_frame->full_func_name == NULL && local_objs_stack_frame_curridx > 0 )
	{
		local_objs_stack_frame = GetPreviousObjectsStackFrame( rt , local_objs_stack_frame ) ;
		if( local_objs_stack_frame )
		{
			local_objs_stack_frame_curridx--;
			goto _GOTO_RETRY_DEBUG_OUTPUT;
		}
	}
	
	return;
}

struct ZlangObject *CloneObjectInTmpStack( struct ZlangRuntime *rt , char *obj_name , struct ZlangObject *clone_obj )
{
	struct ZlangObjectsStackFrame	*tmp_objs_stack_frame = GetCurrentTmpObjectsStackFrame( rt ) ;
	struct ZlangObject		*obj = NULL ;
	int				nret = 0 ;
	
	obj = PushLocalObjectsStack( rt , & (rt->tmp_objs_stack) , tmp_objs_stack_frame ) ;
	if( obj == NULL )
		return NULL;
	
	nret = CloneObject( rt , & obj , obj_name , clone_obj ) ;
	if( nret )
		return NULL;
	
	return obj;
}

struct ZlangObject *CloneZObjectInTmpStack( struct ZlangRuntime *rt , char *obj_name )
{
	return CloneObjectInTmpStack( rt , obj_name , GetZObjectInRuntimeObjectsHeap(rt) );
}

struct ZlangObject *CloneStringObjectInTmpStack( struct ZlangRuntime *rt , char *obj_name )
{
	return CloneObjectInTmpStack( rt , obj_name , GetStringObjectInRuntimeObjectsHeap(rt) );
}

struct ZlangObject *CloneBoolObjectInTmpStack( struct ZlangRuntime *rt , char *obj_name )
{
	return CloneObjectInTmpStack( rt , obj_name , GetBoolObjectInRuntimeObjectsHeap(rt) );
}

struct ZlangObject *CloneShortObjectInTmpStack( struct ZlangRuntime *rt , char *obj_name )
{
	return CloneObjectInTmpStack( rt , obj_name , GetShortObjectInRuntimeObjectsHeap(rt) );
}

struct ZlangObject *CloneUShortObjectInTmpStack( struct ZlangRuntime *rt , char *obj_name )
{
	return CloneObjectInTmpStack( rt , obj_name , GetUShortObjectInRuntimeObjectsHeap(rt) );
}

struct ZlangObject *CloneIntObjectInTmpStack( struct ZlangRuntime *rt , char *obj_name )
{
	return CloneObjectInTmpStack( rt , obj_name , GetIntObjectInRuntimeObjectsHeap(rt) );
}

struct ZlangObject *CloneUIntObjectInTmpStack( struct ZlangRuntime *rt , char *obj_name )
{
	return CloneObjectInTmpStack( rt , obj_name , GetUIntObjectInRuntimeObjectsHeap(rt) );
}

struct ZlangObject *CloneLongObjectInTmpStack( struct ZlangRuntime *rt , char *obj_name )
{
	return CloneObjectInTmpStack( rt , obj_name , GetLongObjectInRuntimeObjectsHeap(rt) );
}

struct ZlangObject *CloneULongObjectInTmpStack( struct ZlangRuntime *rt , char *obj_name )
{
	return CloneObjectInTmpStack( rt , obj_name , GetULongObjectInRuntimeObjectsHeap(rt) );
}

struct ZlangObject *CloneFloatObjectInTmpStack( struct ZlangRuntime *rt , char *obj_name )
{
	return CloneObjectInTmpStack( rt , obj_name , GetFloatObjectInRuntimeObjectsHeap(rt) );
}

struct ZlangObject *CloneDoubleObjectInTmpStack( struct ZlangRuntime *rt , char *obj_name )
{
	return CloneObjectInTmpStack( rt , obj_name , GetDoubleObjectInRuntimeObjectsHeap(rt) );
}

struct ZlangObject *CloneArrayObjectInTmpStack( struct ZlangRuntime *rt , char *obj_name )
{
	return CloneObjectInTmpStack( rt , obj_name , GetArrayObjectInRuntimeObjectsHeap(rt) );
}

struct ZlangObject *CloneListObjectInTmpStack( struct ZlangRuntime *rt , char *obj_name )
{
	return CloneObjectInTmpStack( rt , obj_name , GetListObjectInRuntimeObjectsHeap(rt) );
}

struct ZlangObject *CloneListNodeObjectInTmpStack( struct ZlangRuntime *rt , char *obj_name )
{
	return CloneObjectInTmpStack( rt , obj_name , GetListNodeObjectInRuntimeObjectsHeap(rt) );
}

struct ZlangObject *CloneMapObjectInTmpStack( struct ZlangRuntime *rt , char *obj_name )
{
	return CloneObjectInTmpStack( rt , obj_name , GetMapObjectInRuntimeObjectsHeap(rt) );
}

struct ZlangObject *CloneIteratorObjectInTmpStack( struct ZlangRuntime *rt , char *obj_name )
{
	return CloneObjectInTmpStack( rt , obj_name , GetIteratorObjectInRuntimeObjectsHeap(rt) );
}

struct ZlangObject *CloneFunctionPtrObjectInTmpStack( struct ZlangRuntime *rt , char *obj_name )
{
	return CloneObjectInTmpStack( rt , obj_name , GetFunctionPtrObjectInRuntimeObjectsHeap(rt) );
}

struct ZlangObject *ReferObjectInTmpStack( struct ZlangRuntime *rt , struct ZlangObject *refer_obj )
{
	struct ZlangObjectsStackFrame	*tmp_objs_stack_frame = GetCurrentTmpObjectsStackFrame( rt ) ;
	struct ZlangObject		*obj = NULL ;
	int				nret = 0 ;
	
	obj = PushLocalObjectsStack( rt , & (rt->tmp_objs_stack) , tmp_objs_stack_frame ) ;
	if( obj == NULL )
		return NULL;
	
	nret = ReferObject( rt , obj , refer_obj ) ;
	if( nret )
		return NULL;
	
	return obj;
}

int InitTmpObjecsStackFrameArray( struct ZlangRuntime *rt )
{
	rt->tmp_objs_stack_frame_array_allocount = ZLANG_TMP_OBJECTS_STACK_INFO_ARRAY_COUNT_DEFAULT ;
	rt->tmp_objs_stack_frame_array = (struct ZlangObjectsStackFrame *)ZLMALLOC( sizeof(struct ZlangObjectsStackFrame) * rt->tmp_objs_stack_frame_array_allocount ) ;
	if( rt->tmp_objs_stack_frame_array == NULL )
	{
		SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_ALLOC , "alloc memory for tmp_objs_stack_frame_array" )
		return ZLANG_ERROR_ALLOC;
	}
	memset( rt->tmp_objs_stack_frame_array , 0x00 , sizeof(struct ZlangObjectsStackFrame) * rt->tmp_objs_stack_frame_array_allocount );
	
	return 0;
}

void CleanTmpObjecsStackFrameArray( struct ZlangRuntime *rt )
{
	ZLFREE( rt->tmp_objs_stack_frame_array );
	
	return;
}

static int TryIncreaseTmpObjecsStackFrameArray( struct ZlangRuntime *rt )
{
	if( rt->tmp_objs_stack_frame_array_curridx >= rt->tmp_objs_stack_frame_array_allocount-1 )
	{
		int				new_objs_stack_frame_array_allocount ;
		struct ZlangObjectsStackFrame	*new_objs_stack_frame_array = NULL ;
		
		new_objs_stack_frame_array_allocount = rt->tmp_objs_stack_frame_array_allocount + ZLANG_TMP_OBJECTS_STACK_INFO_ARRAY_INCREASE_COUNT ;
		new_objs_stack_frame_array = (struct ZlangObjectsStackFrame *)ZLREALLOC( rt->tmp_objs_stack_frame_array , sizeof(struct ZlangObjectsStackFrame) * new_objs_stack_frame_array_allocount ) ;
		if( new_objs_stack_frame_array == NULL )
		{
			SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_ALLOC , "ZLREALLOC memory for tmp_objs_stack_frame_array" )
			return ZLANG_ERROR_ALLOC;
		}
		rt->tmp_objs_stack_frame_array = new_objs_stack_frame_array ;
		rt->tmp_objs_stack_frame_array_allocount = new_objs_stack_frame_array_allocount ;
	}
	
	return 0;
}

struct ZlangObjectsStackFrame *IncreaseTmpObjectsStackFrame( struct ZlangRuntime *rt , char *full_func_name )
{
	struct ZlangObjectsStackFrame	*old_tmp_objs_stack_frame = GetCurrentTmpObjectsStackFrame( rt ) ;
	int				stack_local_var_top ;
	struct ZlangObjectsStackFrame	*new_tmp_objs_stack_frame = NULL ;
	int				nret = 0 ;
	
	stack_local_var_top = GetStackTop( rt , old_tmp_objs_stack_frame );
	
	nret = TryIncreaseTmpObjecsStackFrameArray( rt ) ;
	if( nret )
		return NULL;
	
	TEST_RUNTIME_DEBUG( rt )
	{
		struct ZlangObjectsStackFrame *tmp_objs_stack_frame = GetCurrentTmpObjectsStackFrame( rt ) ;
		PRINT_TABS_AND_FORMAT( rt , "before increase tmp stack : [%d][%s][%s]-[%d][%d][%d][%d]" , rt->tmp_objs_stack_frame_array_curridx,tmp_objs_stack_frame->in_obj_name,tmp_objs_stack_frame->full_func_name , tmp_objs_stack_frame->stack_bottom,tmp_objs_stack_frame->stack_in_params_top,tmp_objs_stack_frame->stack_out_params_top,tmp_objs_stack_frame->stack_local_var_top )
	}
	
	rt->tmp_objs_stack_frame_array_curridx++;
	new_tmp_objs_stack_frame = GetCurrentTmpObjectsStackFrame( rt ) ;
	new_tmp_objs_stack_frame->stack_local_var_top = stack_local_var_top ;
	new_tmp_objs_stack_frame->stack_out_params_top = stack_local_var_top ;
	new_tmp_objs_stack_frame->stack_in_params_top = stack_local_var_top ;
	new_tmp_objs_stack_frame->stack_bottom = stack_local_var_top ;
	SetObjectsStackCallerTokenInfo( rt );
	if( rt->in_obj )
		new_tmp_objs_stack_frame->in_obj_name = GetObjectName(rt->in_obj) ;
	else
		new_tmp_objs_stack_frame->in_obj_name = NULL ;
	new_tmp_objs_stack_frame->full_func_name = full_func_name ;
	
	TEST_RUNTIME_DEBUG( rt )
	{
		struct ZlangObjectsStackFrame *tmp_objs_stack_frame = GetCurrentTmpObjectsStackFrame( rt ) ;
		PRINT_TABS_AND_FORMAT( rt , "after increase tmp stack : [%d][%s][%s]-[%d][%d][%d][%d]" , rt->tmp_objs_stack_frame_array_curridx,tmp_objs_stack_frame->in_obj_name,tmp_objs_stack_frame->full_func_name , tmp_objs_stack_frame->stack_bottom,tmp_objs_stack_frame->stack_in_params_top,tmp_objs_stack_frame->stack_out_params_top,tmp_objs_stack_frame->stack_local_var_top )
	}
	
	return new_tmp_objs_stack_frame;
}

struct ZlangObjectsStackFrame *DecreaseTmpObjectsStackFrame( struct ZlangRuntime *rt )
{
	TEST_RUNTIME_DEBUG( rt )
	{
		struct ZlangObjectsStackFrame *tmp_objs_stack_frame = GetCurrentTmpObjectsStackFrame( rt ) ;
		PRINT_TABS_AND_FORMAT( rt , "before decrease tmp stack : [%d][%s][%s]-[%d][%d][%d][%d]" , rt->tmp_objs_stack_frame_array_curridx,tmp_objs_stack_frame->in_obj_name,tmp_objs_stack_frame->full_func_name , tmp_objs_stack_frame->stack_bottom,tmp_objs_stack_frame->stack_in_params_top,tmp_objs_stack_frame->stack_out_params_top,tmp_objs_stack_frame->stack_local_var_top )
	}
	
	PopupTmpObjectsStack( rt );
	
	memset( rt->tmp_objs_stack_frame_array+rt->tmp_objs_stack_frame_array_curridx , 0x00 , sizeof(struct ZlangObjectsStackFrame) );
	rt->tmp_objs_stack_frame_array_curridx--;
	
	TEST_RUNTIME_DEBUG( rt )
	{
		struct ZlangObjectsStackFrame *tmp_objs_stack_frame = GetCurrentTmpObjectsStackFrame( rt ) ;
		PRINT_TABS_AND_FORMAT( rt , "after decrease tmp stack : [%d][%s][%s]-[%d][%d][%d][%d]" , rt->tmp_objs_stack_frame_array_curridx,tmp_objs_stack_frame->in_obj_name,tmp_objs_stack_frame->full_func_name , tmp_objs_stack_frame->stack_bottom,tmp_objs_stack_frame->stack_in_params_top,tmp_objs_stack_frame->stack_out_params_top,tmp_objs_stack_frame->stack_local_var_top )
	}
	
	return GetCurrentTmpObjectsStackFrame( rt );
}

struct ZlangObjectsStackFrame *GetCurrentTmpObjectsStackFrame( struct ZlangRuntime *rt )
{
	return rt->tmp_objs_stack_frame_array+rt->tmp_objs_stack_frame_array_curridx;
}

int PopupTmpObjectsStack( struct ZlangRuntime *rt )
{
	struct ZlangObjectsStackFrame	*tmp_objs_stack_frame = GetCurrentTmpObjectsStackFrame( rt ) ;
	int				nret = 0 ;
	
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "  PopupObjectsStack tmp_objs_stack_frame[%s][%s]-[%d][%d][%d][%d]" , tmp_objs_stack_frame->in_obj_name,tmp_objs_stack_frame->full_func_name , tmp_objs_stack_frame->stack_bottom,tmp_objs_stack_frame->stack_in_params_top,tmp_objs_stack_frame->stack_out_params_top,tmp_objs_stack_frame->stack_local_var_top )
	
	while( tmp_objs_stack_frame->stack_local_var_top > tmp_objs_stack_frame->stack_bottom )
	{
		nret = PopupObjectsStack( rt , & (rt->tmp_objs_stack) , tmp_objs_stack_frame ) ;
		if( nret )
			return nret;
	}
	
	tmp_objs_stack_frame->stack_local_var_top = tmp_objs_stack_frame->stack_bottom ;
	tmp_objs_stack_frame->stack_out_params_top = tmp_objs_stack_frame->stack_bottom ;
	tmp_objs_stack_frame->stack_in_params_top = tmp_objs_stack_frame->stack_bottom ;
	
	return 0;
}

void DebugTmpObjectsStack( struct ZlangRuntime *rt )
{
	int				tmp_objs_stack_frame_curridx = GetCurrentTmpObjectsStackFrameIndex( rt ) ;
	struct ZlangObjectsStackFrame	*tmp_objs_stack_frame = GetCurrentTmpObjectsStackFrame( rt ) ;
	int				stack_no ;
	struct ZlangObject		*obj = NULL ;
	
_GOTO_RETRY_DEBUG_OUTPUT :
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "TMP-OBJECTS-STACK - tmp_objs_stack_frame[%d][%s][%s]-[%d][%d][%d][%d]" , tmp_objs_stack_frame_curridx,tmp_objs_stack_frame->in_obj_name,tmp_objs_stack_frame->full_func_name , tmp_objs_stack_frame->stack_bottom,tmp_objs_stack_frame->stack_in_params_top,tmp_objs_stack_frame->stack_out_params_top,tmp_objs_stack_frame->stack_local_var_top );
	
	for( stack_no = tmp_objs_stack_frame->stack_local_var_top-1 ; stack_no >= tmp_objs_stack_frame->stack_bottom ; stack_no-- )
	{
		obj = rt->tmp_objs_stack.objs_stack + stack_no ;
		TEST_RUNTIME_DEBUG( rt ) { PRINT_TABS(rt) printf( TAB "tmp stack object - stack_no[%d] : tmp object : " , stack_no ); DebugPrintObject( rt , obj ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE }
	}
	
	if( tmp_objs_stack_frame->full_func_name == NULL && tmp_objs_stack_frame_curridx > 0 )
	{
		tmp_objs_stack_frame = GetPreviousObjectsStackFrame( rt , tmp_objs_stack_frame ) ;
		if( tmp_objs_stack_frame )
		{
			tmp_objs_stack_frame_curridx--;
			goto _GOTO_RETRY_DEBUG_OUTPUT;
		}
	}
	
	return;
}

int GetCurrentTmpObjectsStackFrameIndex( struct ZlangRuntime *rt )
{
	return rt->tmp_objs_stack_frame_array_curridx;
}

int InitDefersStack( struct ZlangRuntime *rt , struct ZlangDefersStack *defers_stack , int default_size , int increase_size )
{
	defers_stack->defers_stack_size = default_size ;
	defers_stack->defers_stack = (struct ZlangDefer *)ZLMALLOC( sizeof(struct ZlangDefer) * defers_stack->defers_stack_size ) ;
	if( defers_stack->defers_stack == NULL )
	{
		SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_ALLOC , "alloc memory for defers_stack" )
		return ZLANG_ERROR_ALLOC;
	}
	memset( defers_stack->defers_stack , 0x00 , sizeof(struct ZlangDefer) * defers_stack->defers_stack_size );
	defers_stack->increase_size = increase_size ;
	
	return 0;
}

void CleanDefersStack( struct ZlangRuntime *rt , struct ZlangDefersStack *defers_stack )
{
	ZLFREE( defers_stack->defers_stack );
	
	return;
}

static int TryIncreaseDefersStack( struct ZlangRuntime *rt , struct ZlangDefersStack *defers_stack , struct ZlangDefersStackFrame *defers_stack_frame )
{
	if( defers_stack_frame->stack_top >= defers_stack->defers_stack_size )
	{
		int			new_defers_stack_size ;
		struct ZlangDefer	*new_defers_stack = NULL ;
		
		new_defers_stack_size = defers_stack->defers_stack_size + defers_stack->increase_size ;
		new_defers_stack = (struct ZlangDefer *)ZLREALLOC( defers_stack->defers_stack , sizeof(struct ZlangDefer) * new_defers_stack_size ) ;
		if( new_defers_stack == NULL )
		{
			SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_ALLOC , "ZLREALLOC memory for defers_stack" )
			return ZLANG_ERROR_ALLOC;
		}
		defers_stack->defers_stack = new_defers_stack ;
		defers_stack->defers_stack_size = new_defers_stack_size ;
	}
	
	return 0;
}

struct ZlangDefer *PushLocalDefersStack( struct ZlangRuntime *rt , struct ZlangDefersStack *defers_stack , struct ZlangDefersStackFrame *defers_stack_frame )
{
	struct ZlangDefer		*defer = NULL ;
	int				nret = 0 ;
	
	nret = TryIncreaseDefersStack( rt , defers_stack , defers_stack_frame ) ;
	if( nret )
		return NULL;
	
	defer = defers_stack->defers_stack + defers_stack_frame->stack_top ;
	memset( defer , 0x00 , sizeof(struct ZlangDefer) );
	defers_stack_frame->stack_top++;
	
	return defer;
}

int PopupDeferStack( struct ZlangRuntime *rt , struct ZlangDefersStack *defers_stack , struct ZlangDefersStackFrame *defers_stack_frame )
{
	int				stack_no ;
	struct ZlangDefer		*defer = NULL ;
	struct ZlangTokenDataUnitHeader	*token_info = NULL ;
	char				*token = NULL ;
	int				nret = 0 ;
	
	if( defers_stack_frame->stack_top <= 0 )
		return ZLANG_ERROR_PARAMETER;
	
	stack_no = defers_stack_frame->stack_top-1 ;
	defer = defers_stack->defers_stack + stack_no ;
	token_info = (struct ZlangTokenDataUnitHeader *)GetAppendonlyDataUnitUserHeader(defer->defer_begin_token_dataunit) ;
	token = (char*)token_info + sizeof(struct ZlangTokenDataUnitHeader) ;
	TOKENTYPE_TO_STRING( token_info->token_type , _zlang_token_type_str )
	TEST_RUNTIME_DEBUG( rt ) { PRINT_TABS(rt) printf( "    popup defer from defer_stack_no[%d] begin[%s][%"PRIi32"][%"PRIi32"]-[%s][%"PRIi32"]-[%p][%p][%d]-[%.*s]" , stack_no , token_info->source_filename,token_info->source_row,token_info->source_col , _zlang_token_type_str,token_info->token_len , token_info->p1,token_info->p2,token_info->n3 , token_info->token_len,token ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE }
	
	rt->travel_token_datapage_header = defer->defer_begin_token_datapage_header ;
	rt->travel_token_dataunit = defer->defer_begin_token_dataunit ;
	
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "CALL InterpretStatement" )
	nret = InterpretStatement( rt ) ;
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "InterpretStatement return[%d]" , nret )
	if( nret != ZLANG_INFO_END_OF_STATEMENT )
	{
		return nret;
	}
	
	defers_stack_frame->stack_top--;
	
	return 0;
}

int PopupDefersStack( struct ZlangRuntime *rt )
{
	struct ZlangDefersStackFrame	*defers_stack_frame = GetCurrentDefersStackFrame( rt ) ;
	int				nret = 0 ;
	
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "  PopupDefersStack defers_stack_frame[%s]-[%d][%d]" , defers_stack_frame->full_func_name , defers_stack_frame->stack_bottom,defers_stack_frame->stack_top )
	
	while( defers_stack_frame->stack_top > defers_stack_frame->stack_bottom )
	{
		nret = PopupDeferStack( rt , & (rt->defers_stack) , defers_stack_frame ) ;
		if( nret )
			return nret;
	}
	
	defers_stack_frame->stack_top = defers_stack_frame->stack_bottom ;
	
	return 0;
}

int InitDefersStackFrameArray( struct ZlangRuntime *rt )
{
	rt->defers_stack_frame_array_allocount = ZLANG_DEFERS_STACK_INFO_ARRAY_COUNT_DEFAULT ;
	rt->defers_stack_frame_array = (struct ZlangDefersStackFrame *)ZLMALLOC( sizeof(struct ZlangDefersStackFrame) * rt->defers_stack_frame_array_allocount ) ;
	if( rt->defers_stack_frame_array == NULL )
	{
		SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_ALLOC , "alloc memory for defers_stack_frame_array" )
		return ZLANG_ERROR_ALLOC;
	}
	memset( rt->defers_stack_frame_array , 0x00 , sizeof(struct ZlangDefersStackFrame) * rt->defers_stack_frame_array_allocount );
	
	return 0;
}

void CleanDefersStackFrameArray( struct ZlangRuntime *rt )
{
	ZLFREE( rt->defers_stack_frame_array );
	
	return;
}

static int TryIncreaseDeferStackFrameArray( struct ZlangRuntime *rt )
{
	if( rt->defers_stack_frame_array_curridx >= rt->defers_stack_frame_array_allocount-1 )
	{
		int				new_defers_stack_frame_array_allocount ;
		struct ZlangDefersStackFrame	*new_defers_stack_frame_array = NULL ;
		
		new_defers_stack_frame_array_allocount = rt->defers_stack_frame_array_allocount + ZLANG_DEFERS_STACK_INFO_ARRAY_INCREASE_COUNT ;
		new_defers_stack_frame_array = (struct ZlangDefersStackFrame *)ZLREALLOC( rt->defers_stack_frame_array , sizeof(struct ZlangDefersStackFrame) * new_defers_stack_frame_array_allocount ) ;
		if( new_defers_stack_frame_array == NULL )
		{
			SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_ALLOC , "ZLREALLOC memory for defers_stack_frame_array" )
			return ZLANG_ERROR_ALLOC;
		}
		rt->defers_stack_frame_array = new_defers_stack_frame_array ;
		rt->defers_stack_frame_array_allocount = new_defers_stack_frame_array_allocount ;
	}
	
	return 0;
}

struct ZlangDefersStackFrame *IncreaseDefersStackFrame( struct ZlangRuntime *rt , char *full_func_name )
{
	struct ZlangDefersStackFrame	*old_defers_stack_frame = GetCurrentDefersStackFrame( rt ) ;
	int				stack_top ;
	struct ZlangDefersStackFrame	*new_defers_stack_frame = NULL ;
	int				nret = 0 ;
	
	stack_top = old_defers_stack_frame->stack_top ;
	
	nret = TryIncreaseDeferStackFrameArray( rt ) ;
	if( nret )
		return NULL;
	
	TEST_RUNTIME_DEBUG( rt )
	{
		struct ZlangDefersStackFrame *defers_stack_frame = GetCurrentDefersStackFrame( rt ) ;
		PRINT_TABS_AND_FORMAT( rt , "before increase defers stack : [%d][%s][%s]-[%d][%d]" , rt->defers_stack_frame_array_curridx,defers_stack_frame->in_obj_name,defers_stack_frame->full_func_name , defers_stack_frame->stack_bottom,defers_stack_frame->stack_top )
	}
	
	rt->defers_stack_frame_array_curridx++;
	new_defers_stack_frame = GetCurrentDefersStackFrame( rt ) ;
	new_defers_stack_frame->stack_top = stack_top ;
	new_defers_stack_frame->stack_bottom = stack_top ;
	if( rt->in_obj )
		new_defers_stack_frame->in_obj_name = GetObjectName(rt->in_obj) ;
	else
		new_defers_stack_frame->in_obj_name = NULL ;
	new_defers_stack_frame->full_func_name = full_func_name ;
	
	TEST_RUNTIME_DEBUG( rt )
	{
		struct ZlangDefersStackFrame *defers_stack_frame = GetCurrentDefersStackFrame( rt ) ;
		PRINT_TABS_AND_FORMAT( rt , "after increase defers stack : [%d][%s][%s]-[%d][%d]" , rt->defers_stack_frame_array_curridx,defers_stack_frame->in_obj_name,defers_stack_frame->full_func_name , defers_stack_frame->stack_bottom,defers_stack_frame->stack_top )
	}
	
	return new_defers_stack_frame;
}

struct ZlangDefersStackFrame *DecreaseDefersStackFrame( struct ZlangRuntime *rt )
{
	TEST_RUNTIME_DEBUG( rt )
	{
		struct ZlangDefersStackFrame *defers_stack_frame = GetCurrentDefersStackFrame( rt ) ;
		PRINT_TABS_AND_FORMAT( rt , "before decrease defers stack : [%d][%s][%s]-[%d][%d]" , rt->defers_stack_frame_array_curridx,defers_stack_frame->in_obj_name,defers_stack_frame->full_func_name , defers_stack_frame->stack_bottom,defers_stack_frame->stack_top )
	}
	
	PopupDefersStack( rt );
	
	memset( rt->defers_stack_frame_array+rt->defers_stack_frame_array_curridx , 0x00 , sizeof(struct ZlangDefersStackFrame) );
	rt->defers_stack_frame_array_curridx--;
	
	TEST_RUNTIME_DEBUG( rt )
	{
		struct ZlangDefersStackFrame *defers_stack_frame = GetCurrentDefersStackFrame( rt ) ;
		PRINT_TABS_AND_FORMAT( rt , "after decrease defers stack : [%d][%s][%s]-[%d][%d]" , rt->defers_stack_frame_array_curridx,defers_stack_frame->in_obj_name,defers_stack_frame->full_func_name , defers_stack_frame->stack_bottom,defers_stack_frame->stack_top )
	}
	
	return GetCurrentDefersStackFrame( rt );
}

void SetDefersStackFullFuncName( struct ZlangDefersStackFrame *defers_stack_frame , char *full_func_name )
{
	defers_stack_frame->full_func_name = full_func_name ;
	
	return;
}

struct ZlangDefersStackFrame *GetCurrentDefersStackFrame( struct ZlangRuntime *rt )
{
	return rt->defers_stack_frame_array+rt->defers_stack_frame_array_curridx;
}

int GetCurrentDefersStackFrameIndex( struct ZlangRuntime *rt )
{
	return rt->defers_stack_frame_array_curridx;
}

struct ZlangDefersStackFrame *GetPreviousDefersStackFrame( struct ZlangRuntime *rt , struct ZlangDefersStackFrame *defers_stack_frame )
{
	if( defers_stack_frame == rt->defers_stack_frame_array )
		return NULL;
	
	return defers_stack_frame-1;
}

void DebugDefersStack( struct ZlangRuntime *rt )
{
	int				defers_stack_frame_curridx = GetCurrentDefersStackFrameIndex( rt ) ;
	struct ZlangDefersStackFrame	*defers_stack_frame = GetCurrentDefersStackFrame( rt ) ;
	int				stack_no ;
	struct ZlangDefer		*defer = NULL ;
	struct ZlangTokenDataUnitHeader	*token_info = NULL ;
	char				*token = NULL ;
	
_GOTO_RETRY_DEBUG_OUTPUT :
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "DEFER-STACK - defers_stack_frame[%d][%s][%s]-[%d][%d]" , defers_stack_frame_curridx,defers_stack_frame->in_obj_name,defers_stack_frame->full_func_name , defers_stack_frame->stack_bottom,defers_stack_frame->stack_top );
	
	for( stack_no = defers_stack_frame->stack_top-1 ; stack_no >= defers_stack_frame->stack_bottom ; stack_no-- )
	{
		defer = rt->defers_stack.defers_stack + stack_no ;
		token_info = (struct ZlangTokenDataUnitHeader *)GetAppendonlyDataUnitUserHeader(defer->defer_begin_token_dataunit) ;
		token = (char*)token_info + sizeof(struct ZlangTokenDataUnitHeader) ;
		TOKENTYPE_TO_STRING( token_info->token_type , _zlang_token_type_str )
		TEST_RUNTIME_DEBUG( rt ) { PRINT_TABS(rt) printf( TAB "defer stack defer - stack_no[%d] begin[%s][%"PRIi32"][%"PRIi32"]-[%s][%"PRIi32"]-[%p][%p][%d]-[%.*s]" , stack_no , token_info->source_filename,token_info->source_row,token_info->source_col , _zlang_token_type_str,token_info->token_len , token_info->p1,token_info->p2,token_info->n3 , token_info->token_len,token ); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE }
	}
	
	if( defers_stack_frame->full_func_name == NULL && defers_stack_frame_curridx > 0 )
	{
		defers_stack_frame = GetPreviousDefersStackFrame( rt , defers_stack_frame ) ;
		if( defers_stack_frame )
		{
			defers_stack_frame_curridx--;
			goto _GOTO_RETRY_DEBUG_OUTPUT;
		}
	}
	
	return;
}

int IncreaseStackFrame( struct ZlangRuntime *rt , char *full_func_name )
{
	struct ZlangObjectsStackFrame	*new_local_objs_stack_frame = NULL ;
	struct ZlangObjectsStackFrame	*new_tmp_objs_stack_frame = NULL ;
	struct ZlangDefersStackFrame	*new_defers_stack_frame = NULL ;
	
	new_local_objs_stack_frame = IncreaseLocalObjectsStackFrame( rt , full_func_name ) ;
	if( new_local_objs_stack_frame == NULL )
		return GetRuntimeErrorNo(rt);
	
	new_tmp_objs_stack_frame = IncreaseTmpObjectsStackFrame( rt , full_func_name ) ;
	if( new_tmp_objs_stack_frame == NULL )
		return GetRuntimeErrorNo(rt);
	
	new_defers_stack_frame = IncreaseDefersStackFrame( rt , full_func_name ) ;
	if( new_defers_stack_frame == NULL )
		return GetRuntimeErrorNo(rt);
	
	return 0;
}

int DecreaseStackFrame( struct ZlangRuntime *rt )
{
	struct ZlangObjectsStackFrame	*old_local_objs_stack_frame = NULL ;
	struct ZlangObjectsStackFrame	*old_tmp_objs_stack_frame = NULL ;
	struct ZlangDefersStackFrame	*old_defers_stack_frame = NULL ;
	
	old_defers_stack_frame = DecreaseDefersStackFrame( rt ) ;
	if( old_defers_stack_frame == NULL )
		return GetRuntimeErrorNo(rt);
	
	old_local_objs_stack_frame = DecreaseLocalObjectsStackFrame( rt ) ;
	if( old_local_objs_stack_frame == NULL )
		return GetRuntimeErrorNo(rt);
	
	old_tmp_objs_stack_frame = DecreaseTmpObjectsStackFrame( rt ) ;
	if( old_tmp_objs_stack_frame == NULL )
		return GetRuntimeErrorNo(rt);
	
	return 0;
}

void SetStackTopFullFuncName( struct ZlangRuntime *rt , char *full_func_name )
{
	struct ZlangObjectsStackFrame		*local_objs_stack_frame = NULL ;
	struct ZlangObjectsStackFrame		*tmp_objs_stack_frame = NULL ;
	struct ZlangDefersStackFrame		*defers_stack_frame = NULL ;
	
	local_objs_stack_frame = GetCurrentLocalObjectsStackFrame( rt ) ;
	tmp_objs_stack_frame = GetCurrentTmpObjectsStackFrame( rt ) ;
	defers_stack_frame = GetCurrentDefersStackFrame( rt ) ;
	
	SetObjectsStackFullFuncName( local_objs_stack_frame , full_func_name );
	SetObjectsStackFullFuncName( tmp_objs_stack_frame , full_func_name );
	SetDefersStackFullFuncName( defers_stack_frame , full_func_name );
	
	return;
}

void DebugStack( struct ZlangRuntime *rt )
{
	DebugLocalObjectsStack( rt );
	DebugTmpObjectsStack( rt );
	DebugDefersStack( rt );
	
	return;
}

