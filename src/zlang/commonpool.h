#ifndef _H_COMMONPOOL_
#define _H_COMMONPOOL_

#include <stdio.h>

#if defined(__linux__)
#define DLLEXPORT	extern
#elif defined(_WIN32)
#define DLLEXPORT	__declspec(dllexport)
#endif

#define COMMONPOOL_ERROR_SUCCESS			0
#define COMMONPOOL_ERROR_INTERNAL			1
#define COMMONPOOL_ERROR_ALLOC				11
#define COMMONPOOL_ERROR_POOL_STATUS			101
#define COMMONPOOL_ERROR_CREATE_WATCHING_THREAD		102
#define COMMONPOOL_ERROR_POOL_STOPING			103
#define COMMONPOOL_ERROR_MUTEX				104
#define COMMONPOOL_ERROR_TOO_MANY_SESSIONS		105
#define COMMONPOOL_ERROR_NO_IDLE_SESSION		106
#define COMMONPOOL_ERROR_CREATE_SESSION			1001
#define COMMONPOOL_ERROR_WATCHING_SESSION		1002

struct CommonPool ;

typedef int funcInitSessionContext( struct CommonPool *pool , void *session_ctx );
typedef int funcWatchingSessions( struct CommonPool *pool );
typedef int funcCleanSessionContext( struct CommonPool *pool , void *session_ctx );

struct CommonPoolCallback
{
	size_t			session_ctx_size ;
	
	funcInitSessionContext	*pfuncInitSessionContext ;
	funcWatchingSessions	*pfuncWatchingSessions ;
	funcCleanSessionContext	*pfuncCleanSessionContext ;
} ;

DLLEXPORT struct CommonPool *CreateCommonPool( struct CommonPoolCallback *callback , size_t comm_pool_callback_size );
DLLEXPORT int StartCommonPool( struct CommonPool *pool );
DLLEXPORT int StopCommonPool( struct CommonPool *pool );
DLLEXPORT int DestroyCommonPool( struct CommonPool *pool );

DLLEXPORT int EnableAssistantThread( struct CommonPool *pool , unsigned char assistant_thread_enable );
DLLEXPORT int SetCommonPoolMinIdleSessionsCount( struct CommonPool *pool , size_t min_idle_sessions_count );
DLLEXPORT int SetCommonPoolMaxIdleSessionsCount( struct CommonPool *pool , size_t max_idle_sessions_count );
DLLEXPORT int SetCommonPoolMaxSessionsCount( struct CommonPool *pool , size_t max_sessions_count );
DLLEXPORT int SetCommonPoolMaxIdleTimeval( struct CommonPool *pool , size_t max_idle_timeval );
DLLEXPORT int SetCommonPoolWatchIdleTimeval( struct CommonPool *pool , size_t max_idle_timeval );
DLLEXPORT int SetCommonPoolInspectTimeval( struct CommonPool *pool , size_t inspect_timeval );
DLLEXPORT void SetCommonPoolUserData( struct CommonPool *pool , void *user_data );
DLLEXPORT void *GetCommonPoolUserData( struct CommonPool *pool );

DLLEXPORT int FetchCommonPoolSession( struct CommonPool *pool , void **session_ctx );
DLLEXPORT int JustFetchCommonPoolIdleSession( struct CommonPool *pool , void **session_ctx );
DLLEXPORT int GivebackCommonPoolSession( struct CommonPool *pool , void *session_ctx );

DLLEXPORT int ReleaseCommonPoolIdleSession( struct CommonPool *pool , void *session_ctx );

DLLEXPORT size_t GetCommonPoolIdleSessionsCount( struct CommonPool *pool );
DLLEXPORT size_t GetCommonPoolWorkingSessionsCount( struct CommonPool *pool );

/*
DLLEXPORT void *TravelCommonPoolIdleSession(struct CommonPool *pool , void *session_ctx , unsigned char change_status_idle2working );
DLLEXPORT void ChangebackAfterTravelCommonPoolSession( struct CommonPool *pool , void *session_ctx );
*/

#endif

