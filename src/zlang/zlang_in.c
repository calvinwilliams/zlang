/* Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "zlang_in.h"

TLS struct ZlangRuntime		*_zlang_rt = NULL ;

TLS struct StackFrameInfo	*_alart_stack_frames_info = NULL  ;
TLS size_t			_alart_stack_frames_count = 0 ;
TLS size_t			_alart_stack_frames_no = 0 ;
TLS signed long			_alart_stack_depth = 0 ;

struct ZlangRuntime *GetZlangRuntime()
{
	return _zlang_rt;
}
