/* Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "zlang_in.h"

int ThrowErrorEvent( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	int		nret = 0 ;
	
	if( rt->throw_event.event_type != ZLANG_CATCH_TYPE_NONE )
	{
		CleanEvent( rt );
	}
	
	if( obj )
	{
		rt->throw_event.throw_obj = AllocObject( rt ) ;
		if( rt->throw_event.throw_obj == NULL )
			return GetRuntimeErrorNo(rt);
		
		nret = ReferObject( rt , rt->throw_event.throw_obj , obj ) ;
		if( nret )
			return nret;
	}
	
	/*
	... rt->throw_event.stack_trace ...
	*/
	
	rt->throw_event.event_type = ZLANG_CATCH_TYPE_ERROR ;
	
	return 0;
}

int ThrowFatalEvent( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	int		nret = 0 ;
	
	if( rt->throw_event.event_type != ZLANG_CATCH_TYPE_NONE )
	{
		CleanEvent( rt );
	}
	
	if( obj )
	{
		rt->throw_event.throw_obj = AllocObject( rt ) ;
		if( rt->throw_event.throw_obj == NULL )
			return GetRuntimeErrorNo(rt);
		
		nret = ReferObject( rt , rt->throw_event.throw_obj , obj ) ;
		if( nret )
			return nret;
	}
	
	/*
	... rt->throw_event.stack_trace ...
	*/
	
	rt->throw_event.event_type = ZLANG_CATCH_TYPE_FATAL ;
	
	return 0;
}

void CleanEvent( struct ZlangRuntime *rt )
{
	rt->throw_event.event_type = ZLANG_CATCH_TYPE_NONE ;
	
	if( rt->throw_event.throw_obj )
	{
		DestroyObject( rt , rt->throw_event.throw_obj ); rt->throw_event.throw_obj = NULL ;
	}
	
	if( rt->throw_event.stack_trace )
	{
		free( rt->throw_event.stack_trace ); rt->throw_event.stack_trace = NULL ;
	}
	
	return;
}
