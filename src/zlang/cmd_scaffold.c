/* Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "zlang_in.h"

int CreateSourceFile_main( char *pathfilename )
{
	FILE		*fp = NULL ;
	int		nret = 0 ;
	
	nret = ACCESS( pathfilename , R_OK ) ;
	if( nret == 0 )
	{
		printf( "*** ERROR : file[%s] exist\n" , pathfilename );
		return ZLANG_ERROR_FILE_NOT_FOUND;
	}
	
	fp = fopen( pathfilename , "w" ) ;
	if( fp == NULL )
	{
		printf( "*** ERROR : open file[%s] failed , errno[%d]\n" , pathfilename , errno );
		return ZLANG_ERROR_FILE_NOT_FOUND;
	}
	
	fprintf( fp , "import stdtypes stdio ;\n" );
	fprintf( fp , "\n" );
	fprintf( fp , "function int main( array args )\n" );
	fprintf( fp , "{\n" );
	fprintf( fp , "	stdout.Println( \"hello world\");\n" );
	fprintf( fp , "	\n" );
	fprintf( fp , "	return 0;\n" );
	fprintf( fp , "}\n" );
	
	fclose( fp );
	
	return 0;
}

