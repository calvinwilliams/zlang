/* Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "zlang_in.h"

int Keyword_sync( struct ZlangRuntime *rt )
{
	struct ZlangInterpretStatementContext	sync_expr_interpret_statement_ctx ;
	struct ZlangObject			*mutex_obj = NULL ;
	struct ZlangTokenDataUnitHeader		*token_info1 = NULL ;
	char					*token1 = NULL ;
	struct ZlangTokenDataUnitHeader		*token_info2 = NULL ;
	char					*token2 = NULL ;
	struct ZlangTokenDataUnitHeader		*sync_body_token_dataunit_header = NULL ;
	struct ZlangFunction			*bak_in_func = NULL ;
	MUTEX					*mutex = NULL ;
	
	int					nret = 0 ;
	
	TEST_RUNTIME_DEBUG_THEN_PRINT_ENTER_FUNCTION(rt)
	
	TRAVELTOKEN_AND_SAVEINFO( rt , token_info1 , token1 )
	if( token_info1->token_type != TOKEN_TYPE_BEGIN_OF_SUB_EXPRESSION ) /* ( */
	{
		SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_SYNTAX , "expect '(' but '%s'" , rt->travel_token )
		TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
		return ZLANG_ERROR_SYNTAX;
	}
	
	TEST_RUNTIME_DEBUG( rt )
	{
		TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
		PRINT_TABS_AND_FORMAT( rt , "CALL InterpretExpression , last_token[%s][%s]" , _zlang_token_type_str , rt->travel_token )
	}
	memset( & sync_expr_interpret_statement_ctx , 0x00 , sizeof(struct ZlangInterpretStatementContext) );
	sync_expr_interpret_statement_ctx.token_of_expression_end1 = TOKEN_TYPE_END_OF_SUB_EXPRESSION ;
	nret = InterpretExpression( rt , & sync_expr_interpret_statement_ctx , & mutex_obj ) ;
	TEST_RUNTIME_DEBUG( rt )
	{
		TOKENTYPE_TO_STRING( rt->travel_token_info->token_type , _zlang_token_type_str )
		TEST_RUNTIME_DEBUG( rt ) { PRINT_TABS(rt) printf( "InterpretExpression return[%d] last_token[%s][%s] " , nret , _zlang_token_type_str , rt->travel_token ); DebugPrintObject(rt,mutex_obj); PRINT_SOURCE_FILE_LINE PRINT_NEWLINE }
	}
	if( nret != ZLANG_INFO_END_OF_EXPRESSION )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
		return nret;
	}
	
	if( STRCMP( GetCloneObjectName(mutex_obj) , == , ZLANG_OBJECT_mutex ) )
	{
		nret = CallRuntimeFunction_mutex_GetMutexPtr( rt , mutex_obj , & mutex ) ;
		if( nret )
		{
			SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_RUNTIME_DIRECT_FUNCTION_NOT_SETED , "runtime direct function not seted" )
			TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
			return ZLANG_ERROR_RUNTIME_DIRECT_FUNCTION_NOT_SETED;
		}
	}
	else if( STRCMP( GetCloneObjectName(mutex_obj) , == , ZLANG_OBJECT_condsig ) )
	{
		nret = CallRuntimeFunction_condsig_GetMutexPtr( rt , mutex_obj , & mutex ) ;
		if( nret )
		{
			SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_RUNTIME_DIRECT_FUNCTION_NOT_SETED , "runtime direct function not seted" )
			TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
			return ZLANG_ERROR_RUNTIME_DIRECT_FUNCTION_NOT_SETED;
		}
	}	
	else
	{
		SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_OBJECT_TYPE_INVALID , "object '%s' is not mutex or condsig type" , GetObjectName(mutex_obj) )
		TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
		return ZLANG_ERROR_OBJECT_TYPE_INVALID;
	}
	
	TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "sync( mutex ) ..." )
	
#if defined(__linux__)
	nret = pthread_mutex_lock( mutex ) ;
#elif defined(_WIN32)
	EnterCriticalSection( mutex ) ;
	nret = 0 ;
#endif
	if( nret )
	{
		SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_MUTEX_LOCK , "mutex lock failed[%d]" , nret )
		TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
		return ZLANG_ERROR_MUTEX_LOCK;
	}
	
	PEEKTOKEN_AND_SAVEINFO( rt , token_info2 , token2 )
	if( token_info2->token_type == TOKEN_TYPE_BEGIN_OF_STATEMENT_SEGMENT )
	{
		sync_body_token_dataunit_header = token_info2 ;
		NEXTTOKEN( rt )
		
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "CALL InterpretStatementSegment" )
		IncreaseStackFrame( rt , NULL );
		bak_in_func = rt->in_func ;
		rt->in_func = NULL ;
		nret = InterpretStatementSegment( rt , NULL ) ;
		rt->in_func = bak_in_func ;
		DecreaseStackFrame( rt );
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "InterpretStatementSegment return[%d]" , nret )
		if( nret == ZLANG_INFO_END_OF_STATEMENT_SEGMENT || nret == ZLANG_INFO_BREAK )
		{
			rt->travel_token_datapage_header = sync_body_token_dataunit_header->p1 ;
			rt->travel_token_dataunit = sync_body_token_dataunit_header->p2 ;
			NEXTTOKEN( rt )
		}
		else
		{
#if defined(__linux__)
			nret = pthread_mutex_unlock( mutex ) ;
#elif defined(_WIN32)
			LeaveCriticalSection( mutex ) ;
			nret = 0 ;
#endif
			if( nret )
			{
				SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_MUTEX_UNLOCK , "mutex unlock failed[%d]" , nret )
			}
			
			TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
			return nret;
		}
	}
	else
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "CALL InterpretStatement" )
		nret = InterpretStatement( rt ) ;
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "InterpretStatement return[%d]" , nret )
		if( nret == ZLANG_INFO_END_OF_STATEMENT || nret == ZLANG_INFO_BREAK )
		{
			;
		}
		else
		{
#if defined(__linux__)
			nret = pthread_mutex_unlock( mutex ) ;
#elif defined(_WIN32)
			LeaveCriticalSection( mutex ) ;
			nret = 0 ;
#endif
			if( nret )
			{
				SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_MUTEX_UNLOCK , "mutex unlock failed[%d]" , nret )
			}
			
			TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
			return nret;
		}
	}
	
#if defined(__linux__)
	nret = pthread_mutex_unlock( mutex ) ;
#elif defined(_WIN32)
	LeaveCriticalSection( mutex ) ;
	nret = 0 ;
#endif
	if( nret )
	{
		SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_MUTEX_UNLOCK , "mutex unlock failed[%d]" , nret )
		TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
		return ZLANG_ERROR_MUTEX_UNLOCK;
	}
	
	TEST_RUNTIME_DEBUG_THEN_PRINT_LEAVE_FUNCTION(rt)
	return ZLANG_INFO_END_OF_STATEMENT;
}

int SkipStatement_sync( struct ZlangRuntime *rt )
{
	struct ZlangTokenDataUnitHeader	*token_info1 = NULL ;
	char				*token1 = NULL ;
	struct ZlangTokenDataUnitHeader	*token_info2 = NULL ;
	char				*token2 = NULL ;
	
	int				nret = 0 ;
	
	TEST_RUNTIME_DEBUG_THEN_PRINT_ENTER_FUNCTION(rt)
	
	PEEKTOKEN_AND_SAVEINFO( rt , token_info1 , token1 )
	if( token_info1->token_type != TOKEN_TYPE_BEGIN_OF_SUB_EXPRESSION ) /* ( */
	{
		SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_SYNTAX , "unexpect '%s'" , token1 )
		TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
		return ZLANG_ERROR_SYNTAX;
	}
	
	NEXTTOKEN( rt )
	
	nret = SkipExpression( rt , TOKEN_TYPE_END_OF_SUB_EXPRESSION , 0 , 0 ) ; /* sync( ... ) */
	if( nret != ZLANG_INFO_END_OF_EXPRESSION )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
		return nret;
	}
	
	PEEKTOKEN_AND_SAVEINFO( rt , token_info2 , token2 )
	if( token_info2->token_type == TOKEN_TYPE_BEGIN_OF_STATEMENT_SEGMENT )
	{
		NEXTTOKEN( rt )
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "sync( mutex ) { statement_segment... }" )
		
		nret = SkipStatementSegment( rt ) ;
		if( nret != ZLANG_INFO_END_OF_STATEMENT_SEGMENT )
		{
			TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
			return nret;
		}
	}
	else
	{
		NEXTTOKEN( rt )
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "sync( mutex ) statement..." )
		
		nret = SkipStatement( rt ) ;
		if( nret != ZLANG_INFO_END_OF_STATEMENT )
		{
			TEST_RUNTIME_DEBUG_THEN_PRINT_INTERRUPT_FUNCTION(rt)
			return nret;
		}
	}
	
	TEST_RUNTIME_DEBUG_THEN_PRINT_LEAVE_FUNCTION(rt)
	return ZLANG_INFO_END_OF_STATEMENT;
}

