﻿/* Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define ZLANG_FULLFUNCNAME_MAIN_UTF8	"主函数(array)"

#define CHARSET_UTF8_TRUE		"真"
#define CHARSET_UTF8_FALSE		"假"
#define CHARSET_UTF8_NULL		"空"
#define CHARSET_UTF8_VOID		"无参"
#define CHARSET_UTF8_INCLUDE		"包含文件"
#define CHARSET_UTF8_NEW		"新的"
#define CHARSET_UTF8_IF			"如果"
#define CHARSET_UTF8_ELSE		"否则"
#define CHARSET_UTF8_SWITCH		"分支判断"
#define CHARSET_UTF8_CASE		"分支匹配"
#define CHARSET_UTF8_DEFAULT		"分支缺省"
#define CHARSET_UTF8_DO			"做循环"
#define CHARSET_UTF8_WHILE		"循环判断"
#define CHARSET_UTF8_FOR		"循环"
#define CHARSET_UTF8_IN			"在里面"
#define CHARSET_UTF8_FROM		"从"
#define CHARSET_UTF8_TO			"到"
#define CHARSET_UTF8_FOREACH		"循环每一个"
#define CHARSET_UTF8_CONTINUE		"继续"
#define CHARSET_UTF8_BREAK		"中断"
#define CHARSET_UTF8_IMPORT		"导入"
#define CHARSET_UTF8_FUNCTION		"函数"
#define CHARSET_UTF8_OBJECT		"对象"
#define CHARSET_UTF8_EXTENDS		"继承"
#define CHARSET_UTF8_INTERFACE		"接口"
#define CHARSET_UTF8_IMPLEMENTS		"实现"
#define CHARSET_UTF8_PUBLIC		"公共的"
#define CHARSET_UTF8_PRIVATE		"私有的"
#define CHARSET_UTF8_THIS		"本对象"
#define CHARSET_UTF8_INTERCEPT		"拦截器"
#define CHARSET_UTF8_BEFORE		"在之前"
#define CHARSET_UTF8_AFTER		"在之后"
#define CHARSET_UTF8_SET		"设置"
#define CHARSET_UTF8_GET		"得到"
#define CHARSET_UTF8_DEFER		"延后"
#define CHARSET_UTF8_RETURN		"返回"
#define CHARSET_UTF8_EXIT		"退出"
#define CHARSET_UTF8_SYNC		"同步"
#define CHARSET_UTF8_ATOMIC		"原子的"
#define CHARSET_UTF8_CONST		"常量"
#define CHARSET_UTF8_TRY		"异常监视"
#define CHARSET_UTF8_THROW		"抛出异常"
#define CHARSET_UTF8_CATCH		"捕获异常"
#define CHARSET_UTF8_UNCATCH		"不捕获异常"
#define CHARSET_UTF8_FINALLY		"异常监视后"

