#include "zlang_in.h"

ZlangInvokeFunction ZlangInvokeFunction_zobject_GetObjectName;
int ZlangInvokeFunction_zobject_GetObjectName( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangObject	*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	
	int			nret = 0 ;
	
	nret = CallRuntimeFunction_string_SetStringValue( rt , out1 , GetObjectName(obj) , -1 ) ;
	if( nret )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "CallRuntimeFunction_string_SetStringValue return[%d]" , nret )
		return nret;
	}
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_zobject_ToString;
int ZlangInvokeFunction_zobject_ToString( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangObject	*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	
	struct ZlangObject	*tostr_obj = NULL ;
	
	int			nret = 0 ;
	
	nret = ToString( rt , obj , & tostr_obj ) ;
	if( nret == ZLANG_ERROR_NO_TOSTRING_FUNC_IN_OBJECT )
	{
	}
	else if( nret )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "ToString return[%d]" , nret )
		return nret;
	}
	
	nret = ReferObject( rt , out1 , tostr_obj ) ;
	if( nret )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "ReferObject return[%d]" , nret )
		return nret;
	}
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_zobject_AddProperty_object_string;
int ZlangInvokeFunction_zobject_AddProperty_object_string( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangObject	*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject	*in2 = GetInputParameterInLocalObjectStack(rt,2) ;
	struct ZlangObject	*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	char			*prop_name = NULL ;
	struct ZlangObject	*prop_obj = NULL ;
	
	CallRuntimeFunction_string_GetStringValue( rt , in2 , & prop_name , NULL );
	
	prop_obj = AddPropertyInObject( rt , obj , in1 , prop_name ) ;
	if( prop_obj == NULL )
	{
		CallRuntimeFunction_bool_SetBoolValue( rt , out1 , FALSE );
		return GET_RUNTIME_ERROR_NO(rt);
	}
	
	CallRuntimeFunction_bool_SetBoolValue( rt , out1 , TRUE );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_zobject_ReferProperty_string;
int ZlangInvokeFunction_zobject_ReferProperty_string( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangObject	*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject	*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	char			*prop_name = NULL ;
	struct ZlangObject	*prop_obj = NULL ;
	
	CallRuntimeFunction_string_GetStringValue( rt , in1 , & prop_name , NULL );
	
	prop_obj = QueryPropertyInObjectByPropertyName( rt , obj , prop_name ) ;
	if( prop_obj == NULL )
	{
		UnreferObject( rt , out1 );
		return 0;
	}
	
	ReferObject( rt , out1 , prop_obj );
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_zobject_RemoveProperty_string;
int ZlangInvokeFunction_zobject_RemoveProperty_string( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangObject	*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject	*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	char			*prop_name = NULL ;
	int			nret = 0 ;
	
	CallRuntimeFunction_string_GetStringValue( rt , in1 , & prop_name , NULL );
	
	nret = RemovePropertyInObject( rt , obj , prop_name ) ;
	if( nret )
	{
		CallRuntimeFunction_bool_SetBoolValue( rt , out1 , FALSE );
		return 0;
	}
	
	CallRuntimeFunction_bool_SetBoolValue( rt , out1 , TRUE );
	
	return 0;
}

static struct ZlangDirectFunctions direct_funcs_zobject =
	{
		ZLANG_OBJECT_zobject ,
		
		NULL ,
		NULL ,
		
		NULL ,
		NULL ,
		NULL ,
		NULL ,
		
		NULL ,
		NULL ,
		NULL ,
		NULL ,
		NULL ,
		
		NULL ,
		NULL ,
		NULL ,
		NULL ,
		NULL ,
		
		NULL ,
		NULL ,
		NULL ,
		NULL ,
		NULL ,
		NULL ,
		
		NULL ,
		NULL ,
		
		NULL ,
		NULL ,
		NULL ,
		NULL ,
		NULL ,
		
		NULL ,
	} ;

ZlangImportObjectFunction ZlangImportObject_zobject;
struct ZlangObject *ZlangImportObject_zobject( struct ZlangRuntime *rt )
{
	struct ZlangObject	*obj = NULL ;
	struct ZlangFunction	*func = NULL ;
	int			nret = 0 ;
	
	obj = AllocObject( rt ) ;
	if( obj == NULL )
	{
		SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_ALLOC , "alloc memory for object" )
		return NULL;
	}
	memset( obj , 0x00 , sizeof(struct ZlangObject) );
	
	nret = InitObject( rt , obj , ZLANG_OBJECT_zobject , & direct_funcs_zobject , 0 ) ;
	if( nret )
	{
		FreeObject( rt , obj );
		return NULL;
	}
	
	/* zobject.GetObjectName() */
	func = AddFunctionAndParametersInObject( rt , obj , "GetObjectName" , "GetObjectName()" , ZlangInvokeFunction_zobject_GetObjectName , ZLANG_OBJECT_string , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* zobject.ToString() */
	func = AddFunctionAndParametersInObject( rt , obj , "ToString" , "ToString()" , ZlangInvokeFunction_zobject_ToString , ZLANG_OBJECT_string , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* zobject.AddProperty(object,string) */
	func = AddFunctionAndParametersInObject( rt , obj , ZLANG_FUNCNAME_ADDPROPERTY , ZLANG_FULLFUNCNAME_ADDPROPERTY , ZlangInvokeFunction_zobject_AddProperty_object_string , ZLANG_OBJECT_bool , ZLANG_OBJECT_object,NULL , ZLANG_OBJECT_string,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* zobject.ReferProperty(string) */
	func = AddFunctionAndParametersInObject( rt , obj , ZLANG_FUNCNAME_REFERPROPERTY , ZLANG_FULLFUNCNAME_REFERPROPERTY , ZlangInvokeFunction_zobject_ReferProperty_string , ZLANG_OBJECT_object , ZLANG_OBJECT_string,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	/* zobject.RemoveProperty(string) */
	func = AddFunctionAndParametersInObject( rt , obj , ZLANG_FUNCNAME_REMOVEPROPERTY , ZLANG_FULLFUNCNAME_REMOVEPROPERTY , ZlangInvokeFunction_zobject_RemoveProperty_string , ZLANG_OBJECT_bool , ZLANG_OBJECT_string,NULL , NULL ) ;
	if( func == NULL )
		return NULL;
	
	nret = ImportObjectToGlobalObjectsHeap( rt , obj , NULL ) ;
	if( nret )
	{
		SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_LINK_FUNC_TO_ENTITY , "import object to global objects heap" )
		DestroyObject( rt , obj );
		return NULL;
	}
	
	return obj ;
}

