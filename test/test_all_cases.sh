. test_all_cases_libs.sh

TestZ "nop.z" 0
TestZ "hello.z" 0 "hello world"
TestZ "test_main_return.z hello_world" 11 "hello_world"
TestZ "test_declare_and_eval.z" 0 "hello2"
TestZ "test_declare_then_eval.z" 0 "hello2"
TestZ "test_declare_and_then_and_eval.z" 0 "hello1
hello2hello3
Ahello4Bhello5Chello6D"
TestZ "test_clone_1.z" 0 "Dolly"
TestZ "test_int_PLUS_int.z" 0 "3"
TestZ "test_int_MULTI_double.z" 0 "30
30.000000
30
30.000000"
TestZ "test_float_MUL_float.z" 0 "1664.699951"
TestZ "test_string_PLUS_string_1.z" 0 "hello1hello2"
TestZ "test_string_PLUS_string_2.z" 0 "hello123456789012345678901234567890123456789012345678901234567890XYZ"
TestZ "test_PLUS_PLUS_int.z" 0 "8
9"
TestZ "test_MOD_int.z" 0 "[1]
[3]
[0]
[0]"
TestZ "test_NEGATIVE_int.z" 0 "-3"
TestZ "test_stdtypes_1.z" 0 "1
234
l[567890] f[01.20] d[34.57]
true balabala"
TestZ "test_int_FOUR_MIXED_OPERATIONS_1.z" 0 "3"
TestZ "test_int_FOUR_MIXED_OPERATIONS_2.z" 0 "12"
TestZ "test_int_FOUR_MIXED_OPERATIONS_3.z" 0 "1"
TestZ "test_int_FOUR_MIXED_OPERATIONS_4.z" 0 "-6"
TestZ "test_int_FOUR_MIXED_OPERATIONS_5.z" 0 "0"
TestZ "test_if_1_1.z" 0 "yes"
TestZ "test_if_1_2.z" 0 "yes"
TestZ "test_if_2_1.z" 0 "yes"
TestZ "test_if_2_2.z" 0 "yes"
TestZ "test_if_8_1.z" 0 "22"
TestZ "test_if_11_1.z" 0 "yes"
TestZ "test_if_11_2.z" 0 "yes"
TestZ "test_if_12_1.z" 0 "yes"
TestZ "test_if_12_2.z" 0 "yes"
TestZ "test_if_13_1.z" 0 "yes"
TestZ "test_while_1_1.z" 0 "1
2
3"
TestZ "test_while_1_2.z" 0 "4"
TestZ "test_while_2_1.z" 0 "1
3"
TestZ "test_while_2_2.z" 0 "1
3"
TestZ "test_do_while_1_1.z" 0 "1
2
3"
TestZ "test_do_while_1_2.z" 0 "4"
TestZ "test_do_while_2_1.z" 0 "1
3"
TestZ "test_do_while_2_2.z" 0 "0
1
3"
TestZ "test_for_1_1.z" 0 "1
2
3"
TestZ "test_for_1_2.z" 0 "1
2
3"
TestZ "test_for_2_1.z" 0 "1
3"
TestZ "test_for_2_2.z" 0 "1
3"
TestZ "test_short_1.z" 0 "123"
TestZ "test_long_1.z" 0 "12345678"
TestZ "test_double_1.z" 0 "12345678.876500"
TestCmd "zlang test_stdin_1.z <test_stdin_1.stdin" 0 "I am Calvin
hello
1
12345
123456789
12.340000
1234.567800"
TestZ "test_stdout_1.z" 0 "s[ 1] i[234 ] l[567890] f[1.200000] d[   3456.79] str[A234567890123456789012345678901234567890123456789B] b1[  true] b2[false ] "
TestCmd "zlang test_func_1.z <test_func_1.stdin" 6 "hello Calvin"
TestZ "test_factorial.z" 0 "720"
TestZ "test_string_format_1_1.z" 10 "str[hello]"
TestZ "test_string_format_1_1.z" 10 "str[hello]"
TestZ "test_string_format_2_1.z" 0 "s[ 1] i[234 ] l[567890] f[1.200000] d[   3456.79] str[A234567890123456789012345678901234567890123456789B] b1[  true] b2[false ] "
TestZ "test_string_format_2_2.z" 0 "s[ 1] i[234 ] l[567890] f[1.200000] d[   3456.79] str[A234567890123456789012345678901234567890123456789B] b1[  true] b2[false ] "
TestZ "test_string_format_3_1.z" 0 "s[1] i[234] l[567890] f[1.200000] d[345.678000] str[A234567890123456789012345678901234567890123456789B] b1[true] b2[false] "
TestZ "test_string_format_3_2.z" 0 "s[ 1] i[234 ] l[567890] f[1.200000] d[   3456.79] str[A234567890123456789012345678901234567890123456789B] b1[  true] b2[false ] "
TestZ "test_basetype_formatstring_1.z" 0 "s[ 1] i[234 ] l[567890] f[1.200000] d[   3456.79] str[hello] s2[false ] "
TestFile "test_stdfile_1.z" 0 "test_stdfile_1.out"
TestFile "test_stdfile_2.z" 0 "test_stdfile_2.out"
TestZ "test_stdfile_3.z" 0 "1
2
3
1.000000
2.000000
true
false"
TestZ "test_func_2.z" 0 "main
func2"
TestZ "test_func_3.z" 0 "func1 : n1[111]
func2 : n2[222]"
TestZ "test_ip_1.z" 0 "ip_str[202.101.172.35] -> ip_in_network_order[598500810] ip_in_host_order[-899306461]
ip_str[202.101.172.36] -> ip_in_network_order[615278026] ip_in_host_order[-899306460]
ip_str[203.101.172.35] -> ip_in_network_order[598500811] ip_in_host_order[-882529245]
ip_str[0.0.0.1] -> ip_in_network_order[16777216] ip_in_host_order[1]
ip_str[255.255.255.255] -> ip_in_network_order[-1] ip_in_host_order[-1]"
TestZ "test_ip_2.z" 0 "ip_in_network_order[598500810] -> ip_str[202.101.172.35]
ip_in_host_order[-899306461] -> ip_str[202.101.172.35]"
TestZ "test_datetime_getfromformat_1.z" 0 "it's 2022-08-13 22:20:01"
TestZ "test_datetime_offset_1_1.z" 0 "it's 2000-03-31 23:59:59 after offseting
it's 2000-04-01 00:00:00 after offseting"
TestZ "test_datetime_offset_1_2.z" 0 "it's 2000-02-29 23:59:59 after offseting
it's 2000-03-01 00:00:00 after offseting"
TestZ "test_datetime_offset_2_1.z" 0 "it's 2000-05-30 after offseting
it's 2000-05-31 after offseting"
TestZ "test_datetime_offset_2_2.z" 0 "it's 2000-06-30 after offseting
it's 2000-06-30 after offseting"
TestZ "test_datetime_offset_2_3.z" 0 "it's 2000-06-30 after offseting
it's 2000-06-30 after offseting"
TestZ "test_datetime_offset_2_4.z" 0 "it's 2022-01-31 after offseting
it's 2022-02-28 after offseting
it's 2022-03-28 after offseting
it's 2022-04-28 after offseting
it's 2022-05-28 after offseting
end_of_month
it's 2022-01-31 after offseting
it's 2022-02-28 after offseting
it's 2022-03-31 after offseting
it's 2022-04-30 after offseting
it's 2022-05-31 after offseting"
TestZ "test_datetime_offset_3_1.z" 0 "it's 2000-02-28 after offseting
it's 2000-02-29 after offseting"
TestZ "test_datetime_offset_3_2.z" 0 "it's 1999-02-28 after offseting
it's 1999-02-28 after offseting"
TestZ "test_datetime_offset_3_4.z" 0 "it's 1999-02-28 after offseting
it's 2000-02-28 after offseting
it's 2001-02-28 after offseting
it's 2002-02-28 after offseting
it's 2003-02-28 after offseting
it's 2004-02-28 after offseting
end_of_month
it's 1999-02-28 after offseting
it's 2000-02-29 after offseting
it's 2001-02-28 after offseting
it's 2002-02-28 after offseting
it's 2003-02-28 after offseting
it's 2004-02-28 after offseting"
TestZ "test_execmd_1.z" 0 "apple
orange
pear"
TestZ "test_funcp_1.z" 0 "hello world
hello world"
TestZ "test_crypto_des_1.z" 0 "
--- ZERO PADDING ---
dec:TEXT[1234567] ---des.Encrypt-key[87654321]-padding[0]---> enc:HEX[9A4CB153FF855891] ---des.Decrypt-key:TEXT[87654321]-padding[0]---> dec:TEXT[1234567]
dec:TEXT[12345678] ---des.Encrypt-key[87654321]-padding[0]---> enc:HEX[0DA06156D09594C3] ---des.Decrypt-key:TEXT[87654321]-padding[0]---> dec:TEXT[12345678]
dec:TEXT[123456781] ---des.Encrypt-key[87654321]-padding[0]---> enc:HEX[0DA06156D09594C33FB60F5F9394CD23] ---des.Decrypt-key:TEXT[87654321]-padding[0]---> dec:TEXT[123456781]

--- PKCS7 PADDING ---
dec:TEXT[1234567] ---des.Encrypt-key[87654321]-padding[7]---> enc:HEX[BCC6FE2A7AA93BE0] ---des.Decrypt-key:TEXT[87654321]-padding[7]---> dec:TEXT[1234567]
dec:TEXT[12345678] ---des.Encrypt-key[87654321]-padding[7]---> enc:HEX[0DA06156D09594C3CB12A48C6B54C99E] ---des.Decrypt-key:TEXT[87654321]-padding[7]---> dec:TEXT[12345678]
dec:TEXT[123456781] ---des.Encrypt-key[87654321]-padding[7]---> enc:HEX[0DA06156D09594C3551A4DA8B3285514] ---des.Decrypt-key:TEXT[87654321]-padding[7]---> dec:TEXT[123456781]"
TestZ "test_crypto_tripledes_1.z" 0 "
--- ECB MODE & key16B & ZERO PADDING ---
dec:TEXT[123456781234567] ---tripledes.Encrypt-mode[1]-key:TYPE[128]:TEXT[8765432187654321]-padding[0]---> enc:HEX[0DA06156D09594C39A4CB153FF855891] ---tripledes.Decrypt-mode[1]-key:TYPE[128]:TEXT[8765432187654321]-padding[0]---> dec:TEXT[123456781234567]
dec:TEXT[1234567812345678] ---tripledes.Encrypt-mode[1]-key:TYPE[128]:TEXT[8765432187654321]-padding[0]---> enc:HEX[0DA06156D09594C30DA06156D09594C3] ---tripledes.Decrypt-mode[1]-key:TYPE[128]:TEXT[8765432187654321]-padding[0]---> dec:TEXT[1234567812345678]
dec:TEXT[12345678123456781] ---tripledes.Encrypt-mode[1]-key:TYPE[128]:TEXT[8765432187654321]-padding[0]---> enc:HEX[0DA06156D09594C30DA06156D09594C33FB60F5F9394CD23] ---tripledes.Decrypt-mode[1]-key:TYPE[128]:TEXT[8765432187654321]-padding[0]---> dec:TEXT[12345678123456781]

--- ECB MODE & key16B & PKCS7 PADDING ---
dec:TEXT[123456781234567] ---tripledes.Encrypt-mode[1]-key:TYPE[128]:TEXT[8765432187654321]-padding[7]---> enc:HEX[0DA06156D09594C3BCC6FE2A7AA93BE0] ---tripledes.Decrypt-mode[1]-key:TYPE[128]:TEXT[8765432187654321]-padding[7]---> dec:TEXT[123456781234567]
dec:TEXT[1234567812345678] ---tripledes.Encrypt-mode[1]-key:TYPE[128]:TEXT[8765432187654321]-padding[7]---> enc:HEX[0DA06156D09594C30DA06156D09594C3CB12A48C6B54C99E] ---tripledes.Decrypt-mode[1]-key:TYPE[128]:TEXT[8765432187654321]-padding[7]---> dec:TEXT[1234567812345678]
dec:TEXT[12345678123456781] ---tripledes.Encrypt-mode[1]-key:TYPE[128]:TEXT[8765432187654321]-padding[7]---> enc:HEX[0DA06156D09594C30DA06156D09594C3551A4DA8B3285514] ---tripledes.Decrypt-mode[1]-key:TYPE[128]:TEXT[8765432187654321]-padding[7]---> dec:TEXT[12345678123456781]

--- ECB MODE & key24B & ZERO PADDING ---
dec:TEXT[123456781234567] ---tripledes.Encrypt-mode[1]-key:TYPE[192]:TEXT[876543218765432187654321]-padding[0]---> enc:HEX[0DA06156D09594C39A4CB153FF855891] ---tripledes.Decrypt-mode[1]-key:TYPE[192]:TEXT[876543218765432187654321]-padding[0]---> dec:TEXT[123456781234567]
dec:TEXT[1234567812345678] ---tripledes.Encrypt-mode[1]-key:TYPE[192]:TEXT[876543218765432187654321]-padding[0]---> enc:HEX[0DA06156D09594C30DA06156D09594C3] ---tripledes.Decrypt-mode[1]-key:TYPE[192]:TEXT[876543218765432187654321]-padding[0]---> dec:TEXT[1234567812345678]
dec:TEXT[12345678123456781] ---tripledes.Encrypt-mode[1]-key:TYPE[192]:TEXT[876543218765432187654321]-padding[0]---> enc:HEX[0DA06156D09594C30DA06156D09594C33FB60F5F9394CD23] ---tripledes.Decrypt-mode[1]-key:TYPE[192]:TEXT[876543218765432187654321]-padding[0]---> dec:TEXT[12345678123456781]

--- ECB MODE & key24B & PKCS7 PADDING ---
dec:TEXT[123456781234567] ---tripledes.Encrypt-mode[1]-key:TYPE[192]:TEXT[876543218765432187654321]-padding[7]---> enc:HEX[0DA06156D09594C3BCC6FE2A7AA93BE0] ---tripledes.Decrypt-mode[1]-key:TYPE[192]:TEXT[876543218765432187654321]-padding[7]---> dec:TEXT[123456781234567]
dec:TEXT[1234567812345678] ---tripledes.Encrypt-mode[1]-key:TYPE[192]:TEXT[876543218765432187654321]-padding[7]---> enc:HEX[0DA06156D09594C30DA06156D09594C3CB12A48C6B54C99E] ---tripledes.Decrypt-mode[1]-key:TYPE[192]:TEXT[876543218765432187654321]-padding[7]---> dec:TEXT[1234567812345678]
dec:TEXT[12345678123456781] ---tripledes.Encrypt-mode[1]-key:TYPE[192]:TEXT[876543218765432187654321]-padding[7]---> enc:HEX[0DA06156D09594C30DA06156D09594C3551A4DA8B3285514] ---tripledes.Decrypt-mode[1]-key:TYPE[192]:TEXT[876543218765432187654321]-padding[7]---> dec:TEXT[12345678123456781]

--- CBC MODE & key16B & ZERO PADDING ---
dec:TEXT[123456781234567] ---tripledes.Encrypt-mode[2]-key:TYPE[128]:TEXT[8765432187654321]-padding[0]---> enc:HEX[0DA06156D09594C3CF5A1CAF173F10DC] ---tripledes.Decrypt-mode[2]-key:TYPE[128]:TEXT[8765432187654321]-padding[0]---> dec:TEXT[123456781234567]
dec:TEXT[1234567812345678] ---tripledes.Encrypt-mode[2]-key:TYPE[128]:TEXT[8765432187654321]-padding[0]---> enc:HEX[0DA06156D09594C3771D426663F6AF80] ---tripledes.Decrypt-mode[2]-key:TYPE[128]:TEXT[8765432187654321]-padding[0]---> dec:TEXT[1234567812345678]
dec:TEXT[12345678123456781] ---tripledes.Encrypt-mode[2]-key:TYPE[128]:TEXT[8765432187654321]-padding[0]---> enc:HEX[0DA06156D09594C3771D426663F6AF809938363A112F5DAA] ---tripledes.Decrypt-mode[2]-key:TYPE[128]:TEXT[8765432187654321]-padding[0]---> dec:TEXT[12345678123456781]

--- CBC MODE & key16B & PKCS7 PADDING ---
dec:TEXT[123456781234567] ---tripledes.Encrypt-mode[2]-key:TYPE[128]:TEXT[8765432187654321]-padding[7]---> enc:HEX[0DA06156D09594C3D0586EA12C8B4457] ---tripledes.Decrypt-mode[2]-key:TYPE[128]:TEXT[8765432187654321]-padding[7]---> dec:TEXT[123456781234567]
dec:TEXT[1234567812345678] ---tripledes.Encrypt-mode[2]-key:TYPE[128]:TEXT[8765432187654321]-padding[7]---> enc:HEX[0DA06156D09594C3771D426663F6AF80C85393F005511C29] ---tripledes.Decrypt-mode[2]-key:TYPE[128]:TEXT[8765432187654321]-padding[7]---> dec:TEXT[1234567812345678]
dec:TEXT[12345678123456781] ---tripledes.Encrypt-mode[2]-key:TYPE[128]:TEXT[8765432187654321]-padding[7]---> enc:HEX[0DA06156D09594C3771D426663F6AF80C1B142DFE51D3735] ---tripledes.Decrypt-mode[2]-key:TYPE[128]:TEXT[8765432187654321]-padding[7]---> dec:TEXT[12345678123456781]

--- CBC MODE & key24B & ZERO PADDING ---
dec:TEXT[123456781234567] ---tripledes.Encrypt-mode[2]-key:TYPE[192]:TEXT[876543218765432187654321]-padding[0]---> enc:HEX[0DA06156D09594C3CF5A1CAF173F10DC] ---tripledes.Decrypt-mode[2]-key:TYPE[192]:TEXT[876543218765432187654321]-padding[0]---> dec:TEXT[123456781234567]
dec:TEXT[1234567812345678] ---tripledes.Encrypt-mode[2]-key:TYPE[192]:TEXT[876543218765432187654321]-padding[0]---> enc:HEX[0DA06156D09594C3771D426663F6AF80] ---tripledes.Decrypt-mode[2]-key:TYPE[192]:TEXT[876543218765432187654321]-padding[0]---> dec:TEXT[1234567812345678]
dec:TEXT[12345678123456781] ---tripledes.Encrypt-mode[2]-key:TYPE[192]:TEXT[876543218765432187654321]-padding[0]---> enc:HEX[0DA06156D09594C3771D426663F6AF809938363A112F5DAA] ---tripledes.Decrypt-mode[2]-key:TYPE[192]:TEXT[876543218765432187654321]-padding[0]---> dec:TEXT[12345678123456781]

--- CBC MODE & key24B & PKCS7 PADDING ---
dec:TEXT[123456781234567] ---tripledes.Encrypt-mode[2]-key:TYPE[192]:TEXT[876543218765432187654321]-padding[7]---> enc:HEX[0DA06156D09594C3D0586EA12C8B4457] ---tripledes.Decrypt-mode[2]-key:TYPE[192]:TEXT[876543218765432187654321]-padding[7]---> dec:TEXT[123456781234567]
dec:TEXT[1234567812345678] ---tripledes.Encrypt-mode[2]-key:TYPE[192]:TEXT[876543218765432187654321]-padding[7]---> enc:HEX[0DA06156D09594C3771D426663F6AF80C85393F005511C29] ---tripledes.Decrypt-mode[2]-key:TYPE[192]:TEXT[876543218765432187654321]-padding[7]---> dec:TEXT[1234567812345678]
dec:TEXT[12345678123456781] ---tripledes.Encrypt-mode[2]-key:TYPE[192]:TEXT[876543218765432187654321]-padding[7]---> enc:HEX[0DA06156D09594C3771D426663F6AF80C1B142DFE51D3735] ---tripledes.Decrypt-mode[2]-key:TYPE[192]:TEXT[876543218765432187654321]-padding[7]---> dec:TEXT[12345678123456781]"
TestZ "test_crypto_aes_1.z" 0 "
--- ECB MODE & key16B & ZERO PADDING ---
dec:TEXT[123456781234567] ---aes.Encrypt-mode[1]-key:TYPE[128]:TEXT[8765432187654321]-padding[0]---> enc:HEX[289F971ABC986500053F42F5CC4D21FE] ---tripledes.Decrypt-mode[1]-key:TYPE[128]:TEXT[8765432187654321]-padding[0]---> dec:TEXT[123456781234567]
dec:TEXT[1234567812345678] ---aes.Encrypt-mode[1]-key:TYPE[128]:TEXT[8765432187654321]-padding[0]---> enc:HEX[511BA8A228FF5205AB7BBBF8E5A4509E] ---tripledes.Decrypt-mode[1]-key:TYPE[128]:TEXT[8765432187654321]-padding[0]---> dec:TEXT[1234567812345678]
dec:TEXT[12345678123456781] ---aes.Encrypt-mode[1]-key:TYPE[128]:TEXT[8765432187654321]-padding[0]---> enc:HEX[511BA8A228FF5205AB7BBBF8E5A4509EF9E866E1A84B11D45B1F6590E5027CBB] ---tripledes.Decrypt-mode[1]-key:TYPE[128]:TEXT[8765432187654321]-padding[0]---> dec:TEXT[12345678123456781]

--- ECB MODE & key16B & PKCS7 PADDING ---
dec:TEXT[123456781234567] ---aes.Encrypt-mode[1]-key:TYPE[128]:TEXT[8765432187654321]-padding[7]---> enc:HEX[F9CB8326118EE2759E5D2F6E4971C0F0] ---tripledes.Decrypt-mode[1]-key:TYPE[128]:TEXT[8765432187654321]-padding[7]---> dec:TEXT[123456781234567]
dec:TEXT[1234567812345678] ---aes.Encrypt-mode[1]-key:TYPE[128]:TEXT[8765432187654321]-padding[7]---> enc:HEX[511BA8A228FF5205AB7BBBF8E5A4509ECF123E559CE5C7E1B9BDA5C234C7BC8E] ---tripledes.Decrypt-mode[1]-key:TYPE[128]:TEXT[8765432187654321]-padding[7]---> dec:TEXT[1234567812345678]
dec:TEXT[12345678123456781] ---aes.Encrypt-mode[1]-key:TYPE[128]:TEXT[8765432187654321]-padding[7]---> enc:HEX[511BA8A228FF5205AB7BBBF8E5A4509EF05483B713180F35DD32FA7B47F0B008] ---tripledes.Decrypt-mode[1]-key:TYPE[128]:TEXT[8765432187654321]-padding[7]---> dec:TEXT[12345678123456781]

--- ECB MODE & key32B & ZERO PADDING ---
dec:TEXT[123456781234567] ---aes.Encrypt-mode[1]-key:TYPE[256]:TEXT[87654321876543218765432187654321]-padding[0]---> enc:HEX[320D890B8CB15AD5694262A151806F43] ---tripledes.Decrypt-mode[1]-key:TYPE[256]:TEXT[87654321876543218765432187654321]-padding[0]---> dec:TEXT[123456781234567]
dec:TEXT[1234567812345678] ---aes.Encrypt-mode[1]-key:TYPE[256]:TEXT[87654321876543218765432187654321]-padding[0]---> enc:HEX[C9B0A1E5976F58F659F1AA05DC47740F] ---tripledes.Decrypt-mode[1]-key:TYPE[256]:TEXT[87654321876543218765432187654321]-padding[0]---> dec:TEXT[1234567812345678]
dec:TEXT[12345678123456781] ---aes.Encrypt-mode[1]-key:TYPE[256]:TEXT[87654321876543218765432187654321]-padding[0]---> enc:HEX[C9B0A1E5976F58F659F1AA05DC47740F1E654FF1E641115A51DE66ED08C61FBC] ---tripledes.Decrypt-mode[1]-key:TYPE[256]:TEXT[87654321876543218765432187654321]-padding[0]---> dec:TEXT[12345678123456781]

--- ECB MODE & key32B & PKCS7 PADDING ---
dec:TEXT[123456781234567] ---aes.Encrypt-mode[1]-key:TYPE[256]:TEXT[87654321876543218765432187654321]-padding[7]---> enc:HEX[EF1F9108B8E9E51BBA9DA52630611002] ---tripledes.Decrypt-mode[1]-key:TYPE[256]:TEXT[87654321876543218765432187654321]-padding[7]---> dec:TEXT[123456781234567]
dec:TEXT[1234567812345678] ---aes.Encrypt-mode[1]-key:TYPE[256]:TEXT[87654321876543218765432187654321]-padding[7]---> enc:HEX[C9B0A1E5976F58F659F1AA05DC47740FFA9C3007700C320657D81E20DDF148E9] ---tripledes.Decrypt-mode[1]-key:TYPE[256]:TEXT[87654321876543218765432187654321]-padding[7]---> dec:TEXT[1234567812345678]
dec:TEXT[12345678123456781] ---aes.Encrypt-mode[1]-key:TYPE[256]:TEXT[87654321876543218765432187654321]-padding[7]---> enc:HEX[C9B0A1E5976F58F659F1AA05DC47740F31AA2B57338FA4920E9E61ADCE733EAA] ---tripledes.Decrypt-mode[1]-key:TYPE[256]:TEXT[87654321876543218765432187654321]-padding[7]---> dec:TEXT[12345678123456781]

--- CBC MODE & key16B & ZERO PADDING ---
dec:TEXT[123456781234567] ---aes.Encrypt-mode[2]-key:TYPE[128]:TEXT[8765432187654321]-padding[0]---> enc:HEX[289F971ABC986500053F42F5CC4D21FE] ---tripledes.Decrypt-mode[2]-key:TYPE[128]:TEXT[8765432187654321]-padding[0]---> dec:TEXT[123456781234567]
dec:TEXT[1234567812345678] ---aes.Encrypt-mode[2]-key:TYPE[128]:TEXT[8765432187654321]-padding[0]---> enc:HEX[511BA8A228FF5205AB7BBBF8E5A4509E] ---tripledes.Decrypt-mode[2]-key:TYPE[128]:TEXT[8765432187654321]-padding[0]---> dec:TEXT[1234567812345678]
dec:TEXT[12345678123456781] ---aes.Encrypt-mode[2]-key:TYPE[128]:TEXT[8765432187654321]-padding[0]---> enc:HEX[511BA8A228FF5205AB7BBBF8E5A4509EF2251A19A66AA413F3DB1771E5C98642] ---tripledes.Decrypt-mode[2]-key:TYPE[128]:TEXT[8765432187654321]-padding[0]---> dec:TEXT[12345678123456781]

--- CBC MODE & key16B & PKCS7 PADDING ---
dec:TEXT[123456781234567] ---aes.Encrypt-mode[2]-key:TYPE[128]:TEXT[8765432187654321]-padding[7]---> enc:HEX[F9CB8326118EE2759E5D2F6E4971C0F0] ---tripledes.Decrypt-mode[2]-key:TYPE[128]:TEXT[8765432187654321]-padding[7]---> dec:TEXT[123456781234567]
dec:TEXT[1234567812345678] ---aes.Encrypt-mode[2]-key:TYPE[128]:TEXT[8765432187654321]-padding[7]---> enc:HEX[511BA8A228FF5205AB7BBBF8E5A4509E564E53000595926FF4D862D43DF966C8] ---tripledes.Decrypt-mode[2]-key:TYPE[128]:TEXT[8765432187654321]-padding[7]---> dec:TEXT[1234567812345678]
dec:TEXT[12345678123456781] ---aes.Encrypt-mode[2]-key:TYPE[128]:TEXT[8765432187654321]-padding[7]---> enc:HEX[511BA8A228FF5205AB7BBBF8E5A4509E301E3D45CE65AE05FC2202A22C9844FF] ---tripledes.Decrypt-mode[2]-key:TYPE[128]:TEXT[8765432187654321]-padding[7]---> dec:TEXT[12345678123456781]

--- CBC MODE & key32B & ZERO PADDING ---
dec:TEXT[123456781234567] ---aes.Encrypt-mode[2]-key:TYPE[256]:TEXT[87654321876543218765432187654321]-padding[0]---> enc:HEX[320D890B8CB15AD5694262A151806F43] ---tripledes.Decrypt-mode[2]-key:TYPE[256]:TEXT[87654321876543218765432187654321]-padding[0]---> dec:TEXT[123456781234567]
dec:TEXT[1234567812345678] ---aes.Encrypt-mode[2]-key:TYPE[256]:TEXT[87654321876543218765432187654321]-padding[0]---> enc:HEX[C9B0A1E5976F58F659F1AA05DC47740F] ---tripledes.Decrypt-mode[2]-key:TYPE[256]:TEXT[87654321876543218765432187654321]-padding[0]---> dec:TEXT[1234567812345678]
dec:TEXT[12345678123456781] ---aes.Encrypt-mode[2]-key:TYPE[256]:TEXT[87654321876543218765432187654321]-padding[0]---> enc:HEX[C9B0A1E5976F58F659F1AA05DC47740FAE9FD9E24053F708C38FC33EF8C01EF2] ---tripledes.Decrypt-mode[2]-key:TYPE[256]:TEXT[87654321876543218765432187654321]-padding[0]---> dec:TEXT[12345678123456781]

--- CBC MODE & key32B & PKCS7 PADDING ---
dec:TEXT[123456781234567] ---aes.Encrypt-mode[2]-key:TYPE[256]:TEXT[87654321876543218765432187654321]-padding[7]---> enc:HEX[EF1F9108B8E9E51BBA9DA52630611002] ---tripledes.Decrypt-mode[2]-key:TYPE[256]:TEXT[87654321876543218765432187654321]-padding[7]---> dec:TEXT[123456781234567]
dec:TEXT[1234567812345678] ---aes.Encrypt-mode[2]-key:TYPE[256]:TEXT[87654321876543218765432187654321]-padding[7]---> enc:HEX[C9B0A1E5976F58F659F1AA05DC47740F25E9C1E7D33DB90C67936EDBFCDE594E] ---tripledes.Decrypt-mode[2]-key:TYPE[256]:TEXT[87654321876543218765432187654321]-padding[7]---> dec:TEXT[1234567812345678]
dec:TEXT[12345678123456781] ---aes.Encrypt-mode[2]-key:TYPE[256]:TEXT[87654321876543218765432187654321]-padding[7]---> enc:HEX[C9B0A1E5976F58F659F1AA05DC47740F5B13C0217ED0701884D02B0C88BF4A44] ---tripledes.Decrypt-mode[2]-key:TYPE[256]:TEXT[87654321876543218765432187654321]-padding[7]---> dec:TEXT[12345678123456781]"
TestZ "test_crypto_digest_1.z" 0 "data:TEXT[hello] ---md4.Digest(string)---> digest:HEX[866437CB7A794BCE2B727ACC0362EE27]
data:TEXT[hello] ---md4.Digest(string,string)-salt:TEXT[ calvin]---> digest:HEX[86EEB015A3406184DA0669D1A63F0FF7]
data:TEXT[hello] ---md5.Digest(string)---> digest:HEX[5D41402ABC4B2A76B9719D911017C592]
data:TEXT[hello] ---md5.Digest(string,string)-salt:TEXT[ calvin]---> digest:HEX[74F0845C8BEEDCB6AF6DFC1F0F0DD35C]
data:TEXT[hello] ---sha1.Digest(string)---> digest:HEX[AAF4C61DDCC5E8A2DABEDE0F3B482CD9AEA9434D]
data:TEXT[hello] ---sha1.Digest(string,string)-salt:TEXT[ calvin]---> digest:HEX[4945308312C56B32517564B27E6A337A72093588]
data:TEXT[hello] ---sha224.Digest(string)---> digest:HEX[EA09AE9CC6768C50FCEE903ED054556E5BFC8347907F12598AA24193]
data:TEXT[hello] ---sha224.Digest(string,string)-salt:TEXT[ calvin]---> digest:HEX[1C916607531CFF2A059198106786FFE7EBD65482913D4BCB48A91825]
data:TEXT[hello] ---sha256.Digest(string)---> digest:HEX[2CF24DBA5FB0A30E26E83B2AC5B9E29E1B161E5C1FA7425E73043362938B9824]
data:TEXT[hello] ---sha256.Digest(string,string)-salt:TEXT[ calvin]---> digest:HEX[668F60633B5EC22957F7038F4C8E5A83BA228A5913889F2D8DCDA5C942229597]
data:TEXT[hello] ---sha384.Digest(string)---> digest:HEX[59E1748777448C69DE6B800D7A33BBFB9FF1B463E44354C3553BCDB9C666FA90125A3C79F90397BDF5F6A13DE828684F]
data:TEXT[hello] ---sha384.Digest(string,string)-salt:TEXT[ calvin]---> digest:HEX[65E7370A613A85F01A906D4202C1097B4575CC7476F80715364D6C750D36C11BEA043292D4C6D6EA404486533B10BB36]
data:TEXT[hello] ---sha512.Digest(string)---> digest:HEX[9B71D224BD62F3785D96D46AD3EA3D73319BFBC2890CAADAE2DFF72519673CA72323C3D99BA5C11D7C7ACC6E14B8C5DA0C4663475C2E5C3ADEF46F73BCDEC043]
data:TEXT[hello] ---sha512.Digest(string,string)-salt:TEXT[ calvin]---> digest:HEX[15B2662585E1940C68AD5ABBB6FA638AAF930C049C4BFA491CF8BA06F44FED117A8D2D40A4B2F0614ACAFF84D1131AC3B81CA436D25FA4A5C1ACE636A466DE3B]"
OPENSSL_FULL_VERSION=`openssl version`
OPENSSL_VERSION=`echo $OPENSSL_FULL_VERSION | awk '{print $2}' | sed 's/[a-zA-Z]//g' | tr '.' ' '`
OPENSSL_MAJOR_VERSION=`echo $OPENSSL_VERSION | awk '{print $1}'`
OPENSSL_MINOR_VERSION=`echo $OPENSSL_VERSION | awk '{print $2}'`
OPENSSL_PATCH_VERSION=`echo $OPENSSL_VERSION | awk '{print $3}'`
echo $OPENSSL_FULL_VERSION | grep -w "FIPS" >/dev/null
OPENSSL_HAS_FIPS=$?
if [ $OPENSSL_MAJOR_VERSION -gt 1 ] || ( [ $OPENSSL_MAJOR_VERSION -eq 1 ] && [ $OPENSSL_MINOR_VERSION -gt 1 ] ) || ( [ $OPENSSL_MAJOR_VERSION -eq 1 ] && [ $OPENSSL_MINOR_VERSION -eq 1 ] && [ $OPENSSL_PATCH_VERSION -ge 1 ] ) ; then
	if [ $OPENSSL_HAS_FIPS -ne 0 ] ; then
TestZ "test_crypto_digest_2.z" 0 "data:TEXT[hello] ---sm3.Digest(string)---> digest:HEX[BECBBFAAE6548B8BF0CFCAD5A27183CD1BE6093B1CCECCC303D9C61D0A645268]
data:TEXT[hello] ---sm3.Digest(string,string)-salt:TEXT[ calvin]---> digest:HEX[2E3E7D9B0ADD6675343C85B47EAC2FDBD284F2CB65FA903C3F855C762C86B3F0]"
	fi
fi
TestZ "test_crypto_rsa_encrypt_1.z" 0 "dec length[8]
dec:TEXT[12345678] ---rsa.PublicKeyEncrypt-padding[3]---> enc ---rsa.PrivateKeyDecrypt-padding[3]---> dec:TEXT[12345678]
dec length[260]
dec:TEXT[12345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890] ---rsa.PublicKeyEncrypt-padding[3]---> enc ---rsa.PrivateKeyDecrypt-padding[3]---> dec:TEXT[12345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890]
dec length[8]
dec:TEXT[12345678] ---rsa.PublicKeyEncrypt-padding[1]---> enc ---rsa.PrivateKeyDecrypt-padding[1]---> dec:TEXT[12345678]
dec length[260]
dec:TEXT[12345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890] ---rsa.PublicKeyEncrypt-padding[1]---> enc ---rsa.PrivateKeyDecrypt-padding[1]---> dec:TEXT[12345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890]
dec length[8]
dec:TEXT[12345678] ---rsa.PublicKeyEncrypt-padding[4]---> enc ---rsa.PrivateKeyDecrypt-padding[4]---> dec:TEXT[12345678]
dec length[260]
dec:TEXT[12345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890] ---rsa.PublicKeyEncrypt-padding[4]---> enc ---rsa.PrivateKeyDecrypt-padding[4]---> dec:TEXT[12345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890]"
TestZ "test_crypto_rsa_sign_1.z" 0 "data length[8]
data:TEXT[12345678] ---rsa.Sign-digest[4]---> data_sign ---rsa.Verify---> verify_result[true]
data length[260]
data:TEXT[12345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890] ---rsa.Sign-digest[672]---> data_sign ---rsa.Verify---> verify_result[true]"
if [ $OPENSSL_MAJOR_VERSION -gt 1 ] || ( [ $OPENSSL_MAJOR_VERSION -eq 1 ] && [ $OPENSSL_MINOR_VERSION -gt 1 ] ) || ( [ $OPENSSL_MAJOR_VERSION -eq 1 ] && [ $OPENSSL_MINOR_VERSION -eq 1 ] && [ $OPENSSL_PATCH_VERSION -ge 1 ] ) ; then
	if [ $OPENSSL_HAS_FIPS -ne 0 ] ; then
TestZ "test_crypto_sm4_1.z" 0 "
--- ECB MODE & key16B & PKCS7 PADDING ---
dec:TEXT[123456781234567] ---sm4.Encrypt-mode[1]-key:TYPE[128]:TEXT[8765432187654321]-padding[7]---> enc:HEX[A1504F32DE1D9C3C935F69E6FCE23685] ---sm4.Decrypt-mode[1]-key:TYPE[128]:TEXT[8765432187654321]-padding[7]---> dec:TEXT[123456781234567]
dec:TEXT[1234567812345678] ---sm4.Encrypt-mode[1]-key:TYPE[128]:TEXT[8765432187654321]-padding[7]---> enc:HEX[FDD9581DF03E04DA126E0F7AA816387F0EB784E6103540B77356C5BE04F4B1BF] ---sm4.Decrypt-mode[1]-key:TYPE[128]:TEXT[8765432187654321]-padding[7]---> dec:TEXT[1234567812345678]
dec:TEXT[12345678123456781] ---sm4.Encrypt-mode[1]-key:TYPE[128]:TEXT[8765432187654321]-padding[7]---> enc:HEX[FDD9581DF03E04DA126E0F7AA816387F36957CF3C2FD127F9929BB4E945FC5F7] ---sm4.Decrypt-mode[1]-key:TYPE[128]:TEXT[8765432187654321]-padding[7]---> dec:TEXT[12345678123456781]

--- CBC MODE & key16B & PKCS7 PADDING ---
dec:TEXT[123456781234567] ---sm4.Encrypt-mode[2]-key:TYPE[128]:TEXT[8765432187654321]-padding[7]---> enc:HEX[A1504F32DE1D9C3C935F69E6FCE23685] ---sm4.Decrypt-mode[2]-key:TYPE[128]:TEXT[8765432187654321]-padding[7]---> dec:TEXT[123456781234567]
dec:TEXT[1234567812345678] ---sm4.Encrypt-mode[2]-key:TYPE[128]:TEXT[8765432187654321]-padding[7]---> enc:HEX[FDD9581DF03E04DA126E0F7AA816387FBCAA14A75FCA64173005765FCCCAF0BA] ---sm4.Decrypt-mode[2]-key:TYPE[128]:TEXT[8765432187654321]-padding[7]---> dec:TEXT[1234567812345678]
dec:TEXT[12345678123456781] ---sm4.Encrypt-mode[2]-key:TYPE[128]:TEXT[8765432187654321]-padding[7]---> enc:HEX[FDD9581DF03E04DA126E0F7AA816387FEE3F869EBE448831C7C3E9C8458FC31A] ---sm4.Decrypt-mode[2]-key:TYPE[128]:TEXT[8765432187654321]-padding[7]---> dec:TEXT[12345678123456781]"
TestZ "test_crypto_sm2_sign_1.z" 0 "data length[0]
data:TEXT[] ---sm2.Sign---> data_sign ---sm2.Verify---> verify_result[true]
data length[8]
data:TEXT[12345678] ---sm2.Sign---> data_sign ---sm2.Verify---> verify_result[true]
data length[260]
data:TEXT[12345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890] ---sm2.Sign---> data_sign ---sm2.Verify---> verify_result[true]"
	fi
fi
TestZ "test_system_SleepSeconds_1.z" 0 ""
TestZ "test_system_SleepMilliseconds_1.z" 0 ""
TestZ "test_system_SleepMicroseconds_1.z" 0 ""
TestCmd "zlang test_foreach_1_1.z abc 123" 2 "abc
123"
TestCmd "zlang test_foreach_1_2.z abc 123" 2 "abc
123"
TestCmd "zlang test_foreach_2_1.z abc 123" 2 "abc"
TestCmd "zlang test_foreach_2_2.z abc 123" 2 "123"
TestCmd "zlang test_foreach_12_1.z abc 123" 0 "1
2
3
4
5
6
7
8
9
10"
TestCmd "zlang test_foreach_12_3.z abc 123" 0 "8
7
6
5
4
3
2
1
0
-1"
TestZ "test_array_1_1.z" 0 "ar is empty now
ar is not empty now
at 2 in ar is 555
--- begin travel ar ---
444
555
222
333
--- end travel ar ---"
TestZ "test_array_11_1.z" 0 "2
111
333"
TestZ "test_unaryoper_NOT_1.z" 0 "0
0
0
0.000000
0.000000
false
1
1
1
1.000000
1.000000
true"
TestZ "test_null_1.z" 0 "123.000000"
TestZ "test_null_2.z" 0 "(null)"
TestZ "test_null_12.z" 207 "*** ERROR test_null_12.z:7,14:';' -207: left obj[f]->type_name[float] type not matched with right obj[(null)]->type_name[string] type"
TestZ "test_null_3.z" 0 "(null)"
TestZ "test_ancestor_cmp_1.z" 0 "i1 == i2
i1 != i3
i1 ==& i2
i1 !=& i3
test ==: t1
test !=: d1"
TestZ "test_list_1.z" 0 "6
01
02
03
04
05
06"
TestZ "test_list_2.z" 0 "6
01
02
03
04
05
06"
TestZ "test_list_3.z" 0 "6
01
----------------------------
6
06
----------------------------
6
02"
TestZ "test_list_11.z" 0 "3
111
222
333"
TestZ "test_list_11_e.z" 0 "2
111
333"
TestZ "test_stack_1.z" 0 "3
333
333
222
111"
TestZ "test_queue_1.z" 0 "3
111
111
222
333"
TestZ "test_map_1.z" 0 "6
---------------------------
aaa
ccc
ddd
fff
(null)
---------------------------
3
---------------------------
bbb
(null)
---------------------------
0"
TestZ "test_map_11.z" 0 "0"
TestZ "test_random_1.z" 0 "71876166
708592740
1483128881
907283241
442951012
537146758
1366999021
1854614940
647800535
53523743
4
3
7
4
6
2
2
8
8
9
100
100
102
100
100
102
100
101
102
102"
TestZ "test_const_1_1.z" 0 "hello"
TestZ "test_const_1_2.z" 0 "hello"
TestZ "test_const_1_9.z" 240 "*** ERROR test_const_1_9.z:7,16:';' -240: can't copy to a constant"
TestZ "test_literal_1.z" 0 "123
324508639
751
85"
TestZ "test_literal_2.z" 0 "123456789
1.234567
324508639
751
85"
TestZ "test_bit_1_1.z" 0 "9
15
6
-2"
TestZ "test_plus_eval_1.z" 0 "24
-2
143
11
2
9
15
6"
TestZ "test_uint_1.z" 0 "1
1
1
234
234
234
567890
567890
567890"
TestZ "test_bit_2_1.z" 0 "2
160
3"
TestZ "test_bit_eval_1.z" 0 "2
160
3"
TestZ "test_object_11.z" 209 "*** ERROR test_object_11.z:12,20:'hello' -244: property 'hello' is not public in object 'o'"
TestZ "test_object_12.z" 245 "*** ERROR test_object_12.z:16,20:')' -245: func 'print(string)' or 'print(...)' is not public in object 'o'"
TestZ "test_object_13.z" 0 "hello calvin"
TestZ "test_object_20.z" 0 "i.GetName[calvin]
i.GetAge[296]"
TestZ "test_object_21.z" 0 "geting name[calvin]
geted name[calvin@Hangzhou] last
i.name[calvin@Hangzhou]
seting age[296]
seted age[200] last
i.age[200]"
TestZ "test_object_22.z" 0 "i.score1[5] i.score2[3]
i.score1[5] i.score2[3]
i.score1+i.score2[8]
i.score1 % i.score2[2]
--i.score1[5]
i.score1--[5]
i.score1>i.score2[true]"
TestZ "test_object_31.z" 0 "ahua warrior
miaomiaomiao~
:)
:(
black warrior
wangwangwang~
:>
:<"
TestZ "test_object_31_2.z" 2 "*** ERROR test_object_31_2.z:22,1:'}' -254: unimplement interface function[Eat][Eat(string)] in object[cat] defination"
TestZ "test_object_32.z" 0 "ahua warrior
miaomiaomiao~
:)
:(
black warrior
wangwangwang~
:<
:>"
TestZ "test_object_41.z" 0 "--- DECLARE black ---
cat()
--- DECLARE white ---
cat(\"White\")
SetName(\"White\")
--- USING black ---
SetName(\"Black\")
Black
--- USING white ---
White
~cat()
~cat()"
TestZ "test_add_prop_1.z" 0 "cal
vin"
TestZ "test_backquotes_string_1.z" 0 "{\"list_page_size\":20,\"list_page_no\":3}"
TestZ "test_triple_backquotes_string_1.z" 0 "hello"
mkdir -p $HOME/log
TestLogFile "test_log_1_1.z" 0 "$HOME/log/test_log_1_1.log" "test_log_1_1.log.expect"
TestCmd "rm -f ~/log/*.log*;zlang test_log_2_1.z;ls -1 ~/log/*.log* | xargs basename -a" 0 "test_log_2_1.log
test_log_2_1.log.1"
TestLogFile "test_logs_1_1.z" 0 "$HOME/log/event.log" "event.log.expect"
TestLogFile "test_logs_1_1.z" 0 "$HOME/log/test_logs_1_1.log" "test_logs_1_1.log.expect"
TestZ "test_json_1_1.z" 0 "name1:[value1]
name2:[value2]
myobj.book1:[C]
myobj.book2:[JAVA]
myarray.Get(1):[aaa]
myarray.Get(2):[bbb]
myarray.Get(3):[ccc]
myarray.Get(4).name11:[value11]
myarray.Get(4).name12:[value12]
myarray.Get(5).name21:[value21]
myarray.Get(5).name22:[value22]
mybool1:[false]
mybool2:[true]
mynull:[(null)]
no mynull2
no mynull3"
TestZ "test_json_2_1.z" 0 "json[{\"name1\":\"value1\",\"name2\":\"value2\",\"mybool1\":true,\"mybool2\":false,\"t11\":{\"key11\":\"value11\"},\"a21\":[\"ele21_1\",\"ele21_2\",\"ele21_3\",{\"key21\":\"value21\"},{\"key21\":\"value21_2\"},{\"key21\":\"value21_3\"}],\"null1\":null}]"
TestZ "test_json_3_1.z" 0 "json[{\"a2\":[\"S211\",\"S212\",\"S213\"],\"l2\":[221,222,223],\"m2\":[\"S231\":231,\"S232\":232,\"S233\":233]}]
S211
S212
S213
221
222
223
key[S231] value[231]
key[S232] value[232]
key[S233] value[233]"
TestZ "test_json_4_1.z" 0 "json[{\"a2\":[{\"id\":101,\"str\":\"S101\",\"amount\":1.010000},{\"id\":102,\"str\":\"S102\",\"amount\":1.020000},{\"id\":103,\"str\":\"S103\",\"amount\":1.030000}],\"l2\":[{\"id\":201,\"str\":\"S201\",\"amount\":2.010000},{\"id\":202,\"str\":\"S202\",\"amount\":2.020000},{\"id\":203,\"str\":\"S203\",\"amount\":2.030000}],\"m2\":[\"K1\":{\"id\":301,\"str\":\"S301\",\"amount\":3.010000},\"K2\":{\"id\":302,\"str\":\"S302\",\"amount\":3.020000},\"K3\":{\"id\":303,\"str\":\"S303\",\"amount\":3.030000}]}]
array : id[101] str[S101] amount[1.010000]
array : id[102] str[S102] amount[1.020000]
array : id[103] str[S103] amount[1.030000]
list : id[201] str[S201] amount[2.010000]
list : id[202] str[S202] amount[2.020000]
list : id[203] str[S203] amount[2.030000]
map[K1] : id[301] str[S301] amount[3.010000]
map[K2] : id[302] str[S302] amount[3.020000]
map[K3] : id[303] str[S303] amount[3.030000]"
TestZ "test_path_2_1.z" 0 "Set path is /var/log
Change parent path /var"
TestZ "test_path_3_1.z" 0 "Set path is /var
Change child path : /var/log"
TestCmd "zlang test_path_4_1.z | sort" 0 "Change path is $HOME/src/zlang/src
Current path is $HOME/src/zlang/test
file count : 4
type : 1 file : zlang
type : 1 file : zobjects
type : 2 file : makefile
type : 2 file : makefile.Linux"
TestZ "test_tostring_1_1.z" 0 "hello"
TestZ "test_IsBetween_1.z" 0 "true
false
true
false
true
false"
TestCmd "zlang test_string_2_1.z <test_string_2_1.z.stdin" 0 "Yes"
TestZ "test_string_3_1.z" 0 "
[]
[]"
TestZ "test_string_Append_1.z" 0 "append1
append2
hello3append3"
TestZ "test_string_Delete_1.z" 0 "

123456
123456
123456
123456
123456
56

123456
123
123456
123456
123456"
TestZ "test_string_StartWith_1.z" 0 "false
false
true
false"
TestZ "test_string_EndWith_1.z" 0 "false
false
true
false"
TestZ "test_string_EqualsIgnoreCase_1.z" 0 "false
false
true
false"
TestZ "test_string_IndexOf_1.z" 0 "-1
-1
3
-1"
TestZ "test_string_Insert_1.z" 0 "insert1
insert2
hello3
insert4hello
helloinsert5
hello6insert6
hello7"
TestZ "test_string_Replace_1.z" 0 "

hel103
hello4"
TestZ "test_string_ReplaceAll_1.z" 0 "

he11o3
hello4"
TestZ "test_string_ReplaceAll_2.z" 0 "[
]
[<br />]
---
[123
456
789
]
[123<br />456<br />789<br />]"
TestZ "test_string_SubString_1.z" 0 "(null)
(null)
(null)
(null)
hel
ell
lo3
(null)
(null)"
TestZ "test_string_LowerCase_1.z" 0 "

hello3"
TestZ "test_string_UpperCase_1.z" 0 "

HELLO3"
TestZ "test_string_Trim_1.z" 0 "[]
[]
[hello3]"
TestZ "test_string_ExpandEnvVariables_1.z" 0 "[14/15] [X\${SHORT_VAR}Y]
[4/15] [X0FY]
[13/14] [X\${LONG_VAR}Y]
[18/22] [X0123456789ABCDEFY]
[18/19] [X\${LONG_LONG_VAR}Y]
[50/51] [X0123456789ABCDEF0123456789ABCDEF0123456789ABCDEFY]"
TestZ "test_xml_1_1.z" 0 "name1:[value1]
name2:[value2]
myobj.book1:[C]
myobj.book2:[JAVA]
mybool1:[false]
mybool2:[true]
mynull:[(null)]
no mynull2
no mynull3"
TestZ "test_xml_1_2.z" 0 "name1:[value1]
name2:[value2]
myobj.book1:[C]
myobj.book2:[JAVA]
mybool1:[false]
mybool2:[true]
mynull:[(null)]"
TestZ "test_xml_2_1.z" 0 "xml[<t1><name1>value1</name1><name2>value2</name2><mybool1>true</mybool1><mybool2>false</mybool2><t11><key11>value11</key11></t11><a21>ele21_1</a21><a21>ele21_2</a21><a21>ele21_3</a21><a21><t21><key21>value21</key21></t21></a21><null1 /></t1>]
xml[<t1>
	<name1>value1</name1>
	<name2>value2</name2>
	<mybool1>true</mybool1>
	<mybool2>false</mybool2>
	<t11>
		<key11>value11</key11>
	</t11>
	<a21>ele21_1</a21>
	<a21>ele21_2</a21>
	<a21>ele21_3</a21>
	<a21>
		<t21>
			<key21>value21</key21>
		</t21>
	</a21>
	<null1 />
</t1>
]"
TestZ "test_new_1_1.z" 0 "hello
world
!!!"
TestZ "test_new_2_1.z" 0 "0
1
2
6
7
8"
TestZ "test_list_4.z" 0 "str - 111
str - 222
str - 333"
TestZ "test_list_5.z" 0 "1|string new str - 1
2|string new str - 2
3|string new str - 3"
TestZ "test_xml_8_1.z" 0 "xml.ObjectToString ok , [<root1>
	<a2>
		<test3>
			<i>11</i>
			<str>str11</str>
			<b>true</b>
		</test3>
	</a2>
	<a2>
		<test3>
			<i>12</i>
			<str>str12</str>
			<b>false</b>
		</test3>
	</a2>
	<a2>
		<test3>
			<i>13</i>
			<str>str13</str>
			<b>true</b>
		</test3>
	</a2>
	<l2>
		<test3>
			<i>21</i>
			<str>str21</str>
			<b>true</b>
		</test3>
	</l2>
	<l2>
		<test3>
			<i>22</i>
			<str>str22</str>
			<b>false</b>
		</test3>
	</l2>
	<l2>
		<test3>
			<i>23</i>
			<str>str23</str>
			<b>true</b>
		</test3>
	</l2>
	<m2>
		<test3>
			<i>31</i>
			<str>str31</str>
			<b>true</b>
		</test3>
	</m2>
	<m2>
		<test3>
			<i>32</i>
			<str>str32</str>
			<b>false</b>
		</test3>
	</m2>
	<m2>
		<test3>
			<i>33</i>
			<str>str33</str>
			<b>true</b>
		</test3>
	</m2>
</root1>
]
xml.StringToEntityObject ok
--- ARRAY 1/3 ---
i[11]
str[str11]
b[true]
--- ARRAY 2/3 ---
i[12]
str[str12]
b[false]
--- ARRAY 3/3 ---
i[13]
str[str13]
b[true]
--- LIST 1/3 ---
i[21]
str[str21]
b[true]
--- LIST 2/3 ---
i[22]
str[str22]
b[false]
--- LIST 3/3 ---
i[23]
str[str23]
b[true]
--- MAP 1/3 ---
i[31]
str[str31]
b[true]
--- MAP 2/3 ---
i[32]
str[str32]
b[false]
--- MAP 3/3 ---
i[33]
str[str33]
b[true]"
TestZ "test_include_1_1.z" 0 "func2
func3"
TestZ "test_judge_1_1.z" 0 "yes"
TestZ "test_judge_2_1.z" 0 "yes
yes
yes
yes"
TestZ "test_return_1_1.z" 0 "yes
yes
yes1
yes1
yes2"
TestZ "test_convert_eval_1_1.z" 0 "123.000000
123"
TestZ "test_iterator_1_1.z" 0 "--- show all ---
111
222
333
444
555
--- remove some items ---
remove 2
remove 4
--- show all ---
111
333
555
--- remove some items ---
remove 1
remove 3
--- show all ---
333
--- remove some items ---
remove 1
--- show all ---"
TestZ "test_iterator_1_1.z" 0 "--- show all ---
111
222
333
444
555
--- remove some items ---
remove 2
remove 4
--- show all ---
111
333
555
--- remove some items ---
remove 1
remove 3
--- show all ---
333
--- remove some items ---
remove 1
--- show all ---"
TestZ "test_iterator_2_1.z" 0 "--- show all ---
111
222
333
444
555
--- remove some items ---
remove 2
remove 4
--- show all ---
111
333
555
--- remove some items ---
remove 1
remove 3
--- show all ---
333
--- remove some items ---
remove 1
--- show all ---"
TestZ "test_iterator_3_1.z" 0 "--- show all ---
111
222
333
444
555
--- remove some items ---
remove 2
remove 4
--- show all ---
111
333
555
--- remove some items ---
remove 1
remove 3
--- show all ---
333
--- remove some items ---
remove 1
--- show all ---"
TestZ "test_iterator_1_2.z" 0 "--- show all ---
111
222
333
444
555
--- remove some items ---
remove 2
remove 4
--- show all ---
111
333
555
--- remove some items ---
remove 1
remove 3
--- show all ---
333
--- remove some items ---
remove 1
--- show all ---"
TestZ "test_iterator_2_2.z" 0 "--- show all ---
111
222
333
444
555
--- remove some items ---
remove 2
remove 4
--- show all ---
111
333
555
--- remove some items ---
remove 1
remove 3
--- show all ---
333
--- remove some items ---
remove 1
--- show all ---"
TestZ "test_iterator_3_2.z" 0 "--- show all ---
111
222
333
444
555
--- remove some items ---
remove 2
remove 4
--- show all ---
111
333
555
--- remove some items ---
remove 1
remove 3
--- show all ---
333
--- remove some items ---
remove 1
--- show all ---"
TestZ "test_intercept_1_1.z" 0 "before func1 : in1[3] out1[0]
func1 : in[3] out[9]
after func1 : in1[3] out1[9]
after func1 : out1->[81]
main : func1 out[81]"
TestCmd "zlang test_intercept_1_2.z 123 abc" 3 "123
abc"
TestZ "test_intercept_1_3.z" 0 "before object[(null)] function[main]
before object[(null)] function[func1]
func1 : in[3] out[9]
after object[(null)] function[func1]
after object[(null)] function[main]"
TestZ "test_intercept_2_1.z" 0 "before test1.func1
func1
after test1.func1"
TestZ "test_charset_GB18030_1_1.z" 0 "你好，世界"
TestCmd "zlang test_charset_GB18030_2_1.z 123 abc" 0 "A前中后B有字母C
击中缺省
序号[0]
序号[1]
序号[2]
命令行参数[123]
命令行参数[abc]"
TestZ "test_switch_1_1.z" 0 "2"
TestZ "test_switch_1_2.z" 0 "default"
TestZ "test_switch_1_3.z" 0 ""
TestZ "test_switch_1_4.z" 0 "2
3
default"
TestZ "test_switch_1_5.z" 200 "2
*** ERROR test_switch_1_5.z:13,12:';' -200: unexpect 'continue'"
TestZ "test_string_Regexp_1.z" 0 "1
0
2
0"
TestZ "test_string_Regexp_2.z" 0 "5 : 192 168 0 1
3 : 138****5678"
TestZ "test_string_Regexp_3.z" 0 "2 : <div>hello</td>
2 : <d>hello</td>
2 : 138.1111678"
TestZ "test_string_Regexp_4.z" 0 "2 : <div>hello</div>
2 : <d>hello</d>
2 : 138....1678"
TestZ "test_string_Regexp_5.z" 0 "2 : /ccc/bbb/aaa
-3 : /aaa/bbb/ccc"
TestZ "test_string_Split_1.z" 0 "str[我们|nimen|tamen|她们] sepstr[|]
  array element[我们]
  array element[nimen]
  array element[tamen]
  array element[她们]

str[|我们|nimen||tamen|她们|] sepstr[|]
  array element[]
  array element[我们]
  array element[nimen]
  array element[]
  array element[tamen]
  array element[她们]
  array element[]

str[我们|nimen|tamen|她们] sepstr[men]
  array element[我们|ni]
  array element[|ta]
  array element[|她们]

str[我们|nimen|tamen|她们] sepstr[我]
  array element[]
  array element[们|nimen|tamen|她们]

str[我们|nimen|tamen|她们] sepstr[们]
  array element[我]
  array element[|nimen|tamen|她]
  array element[]

str[|我们|nimen|tamen|她们|] sepstr[们]
  array element[|我]
  array element[|nimen|tamen|她]
  array element[|]"
TestZ "test_string_Split_2.z" 0 "str[我们|nimen||tamen|她们] sepstr[|] ignstr[]
  array element[我们]
  array element[nimen]
  array element[tamen]
  array element[她们]

str[|我们|nimen||tamen|她们|] sepstr[|] ignstr[]
  array element[我们]
  array element[nimen]
  array element[tamen]
  array element[她们]"
TestZ "test_xml_11_1.z" 0 "/html/head/title : [This is title]
/html/body/p : [
					<b>
						The Dormouse's story
					</b>
				]
/html/body/p.story3 : [
					Beautiful rice
				]
/html/body/p.story4[1] : [
					in zlang
				]
/html/body/p[2] : [
					Once upon a time there were three little sisters; and their names were
					<a class=\"sister\" href=\"http://example.com/elsie\" id=\"link1\">
						Elsie
					</a>
					,
					<a class=\"sister\" href=\"http://example.com/lacie\" id=\"link2\">
						Lacie
					</a>
					and
					<a class=\"sister\" href=\"http://example.com/tillie\" id=\"link2\">
						Tillie
					</a>
					; and they lived at the bottom of a well.
				]
/html/body/p[2] -> TrimTagsAndBlanks : [Once upon a time there were three little sisters; and their names were
Elsie
,
Lacie
and
Tillie
; and they lived at the bottom of a well.]"
TestZ "test_xml_12_1.z" 0 "p :
    k[class] v[title]
    k[innerText] v[The Dormouse's story]
p :
    k[class] v[story]
    k[innerText] v[Once upon a time there were three little sisters; and their names were
Elsie
,
Lacie
and
Tillie
; and they lived at the bottom of a well.]
p :
    k[class] v[story3]
    k[innerText] v[Beautiful rice]
p :
    k[class] v[story4]
    k[innerText] v[in zlang]"
TestZ "test_xml_12_2.z" 0 "xml.Xnode ok
a :
    k[id] v[link2]
    k[href] v[http://example.com/lacie]
    k[class] v[sister]
    k[innerText] v[Lacie]
a :
    k[id] v[link2]
    k[href] v[http://example.com/tillie]
    k[class] v[sister]
    k[innerText] v[Tillie]"
TestZ "test_comment_1.z" 0 ""
TestFile "test_defer_1.z" 0 "test_defer_1.out"
TestFile "test_defer_2.z" 0 "test_defer_2.out"
TestZ "test_math_sin_1.z" 0 "0.893997 = 90.000000.Sin() ;
0.893997 = 90.000000.Sin() ;"
TestZ "test_math_cos_1.z" 0 "1.000000 = 0.000000.Cos() ;
1.000000 = 0.000000.Cos() ;"
TestZ "test_math_tan_1.z" 0 "1.792527 = -200.000000.Tan() ;
1.792527 = -200.000000.Tan() ;"
TestZ "test_math_asin_1.z" 0 "0.523599 = 0.500000.ASin() ;
0.523599 = 0.500000.ASin() ;"
TestZ "test_math_sinh_1.z" 0 "0.521095 = 0.500000.SinH() ;
0.521095 = 0.500000.SinH() ;"
TestZ "test_math_log_1.z" 0 "-0.693147 = 0.500000.Log() ;
-0.693147 = 0.500000.Log() ;
-0.301030 = 0.500000.Log10() ;
-0.301030 = 0.500000.Log10() ;"
TestZ "test_math_pow_sqrt_1.z" 0 "81.000000 = 9.000000.Pow(2.0) ;
9.000000 = 81.000000.Sqrt() ;
81 = 9.Pow(2) ;
9 = 81.Sqrt() ;"
TestZ "test_math_exp_1.z" 0 "20.085537 = 3.000000.Exp() ;
20.085537 = 3.000000.Exp() ;"
TestZ "test_math_ceil_floor_1.z" 0 "101.000000 = 100.099998.Ceil() ;
100.000000 = 100.900000.Floor() ;"
TestZ "test_math_abs_1.z" 0 "9.000000 = 9.000000.Abs() ;
9.000000 = -9.000000.Abs() ;
9 = 9.Abs() ;
9 = -9.Abs() ;"
TestZ "test_math_E_PI_1.z" 0 " E[2.7182818285]
PI[3.1415926536]"
TestZ "test_encoding_2_1.z" 0 "A_base64[QQ==]
A[A]
AB_base64[QUI=]
AB[AB]
ABC_base64[QUJD]
ABC[ABC]
ABCD_base64[QUJDRA==]
ABCD[ABCD]"
TestZ "test_encoding_3_1.z" 0 "A_hex[41]
A[A]
AB_hex[4142]
AB[AB]
ABC_hex[414243]
ABC[ABC]
ABCD_hex[41424344]
ABCD[ABCD]"
TestCmd "echo '3 1' | zlang test_try_1_1.z" 0 "calculation beginning
input dividend : input divisor : [3] / [1] = [3]"
TestCmd "echo '3 0' | zlang test_try_1_1.z" 138 "calculation beginning
input dividend : input divisor : *** FATAL EXCEPTION CATCHED
*** EXCEPTION SOURCE : test_try_1_1.z:17,34
*** EXCEPTION FUNCTION : [(null)][main(array)]
*** MESSAGE : division by zero
FRAME 2 CALLER (test_try_1_1.z:7,3) - CALLEE [(null)][(null)] (test_try_1_1.z:7,3)
	LOCAL-OBJ [int][dividend] VALUE[3]
	LOCAL-OBJ [int][divisor] VALUE[0]
	LOCAL-OBJ [int][quotient] VALUE[0]
FRAME 1 CALLER (test_try_1_1.z:5,2) - CALLEE [(null)][main(array)] (test_try_1_1.z:5,2)
	IN-PARAM [array][args] VALUE[(no_tostr_func)]"
TestCmd "echo '3 0' | zlang test_try_1_2.z" 138 "calculation beginning
input dividend : input divisor : *** FATAL EXCEPTION CATCHED
*** EXCEPTION SOURCE : test_try_1_2.z:15,33
*** EXCEPTION FUNCTION : [(null)][main(array)]
*** MESSAGE : division by zero
FRAME 1 CALLER (test_try_1_2.z:5,2) - CALLEE [(null)][main(array)] (test_try_1_2.z:5,2)
	IN-PARAM [array][args] VALUE[(no_tostr_func)]
	LOCAL-OBJ [int][dividend] VALUE[3]
	LOCAL-OBJ [int][divisor] VALUE[0]
	LOCAL-OBJ [int][quotient] VALUE[0]"
TestCmd "echo '3 0' | zlang test_try_2_1.z" 138 "calculation beginning
input dividend : input divisor : *** FATAL EXCEPTION CATCHED
*** EXCEPTION SOURCE : test_try_2_1.z:7,30
*** EXCEPTION FUNCTION : [TestObject][Div(int,int)]
*** MESSAGE : division by zero
FRAME 3 CALLER (test_try_2_1.z:25,30) - CALLEE [TestObject][Div(int,int)] (test_try_2_1.z:7,3)
	IN-PARAM [int][dividend] VALUE[3]
	IN-PARAM [int][divisor] VALUE[0]
FRAME 2 CALLER (test_try_2_1.z:15,3) - CALLEE [(null)][(null)] (test_try_2_1.z:15,3)
	LOCAL-OBJ [int][dividend] VALUE[3]
	LOCAL-OBJ [int][divisor] VALUE[0]
	LOCAL-OBJ [int][quotient] VALUE[0]
FRAME 1 CALLER (test_try_2_1.z:13,2) - CALLEE [(null)][main(array)] (test_try_2_1.z:13,2)
	IN-PARAM [array][args] VALUE[(no_tostr_func)]"
TestCmd "echo '3 0' | zlang test_try_2_2.z" 138 "calculation beginning
input dividend : input divisor : *** FATAL EXCEPTION CATCHED
*** EXCEPTION SOURCE : test_try_2_2.z:7,30
*** EXCEPTION FUNCTION : [TestObject][Div(int,int)]
*** MESSAGE : division by zero
FRAME 2 CALLER (test_try_2_2.z:23,29) - CALLEE [TestObject][Div(int,int)] (test_try_2_2.z:7,3)
	IN-PARAM [int][dividend] VALUE[3]
	IN-PARAM [int][divisor] VALUE[0]
FRAME 1 CALLER (test_try_2_2.z:13,2) - CALLEE [(null)][main(array)] (test_try_2_2.z:13,2)
	IN-PARAM [array][args] VALUE[(no_tostr_func)]
	LOCAL-OBJ [int][dividend] VALUE[3]
	LOCAL-OBJ [int][divisor] VALUE[0]
	LOCAL-OBJ [int][quotient] VALUE[0]"
TestCmd "echo '3 1' | zlang test_try_3_1.z" 0 "calculation beginning
input dividend : input divisor : [3] / [1] = [3]
calculation finished"
TestCmd "echo '3 0' | zlang test_try_3_1.z" 138 "calculation beginning
input dividend : input divisor : calculation finished
*** FATAL EXCEPTION CATCHED
*** EXCEPTION SOURCE : test_try_3_1.z:17,34
*** EXCEPTION FUNCTION : [(null)][main(array)]
*** MESSAGE : division by zero
FRAME 2 CALLER (test_try_3_1.z:7,3) - CALLEE [(null)][(null)] (test_try_3_1.z:7,3)
	LOCAL-OBJ [int][dividend] VALUE[3]
	LOCAL-OBJ [int][divisor] VALUE[0]
	LOCAL-OBJ [int][quotient] VALUE[0]
FRAME 1 CALLER (test_try_3_1.z:5,2) - CALLEE [(null)][main(array)] (test_try_3_1.z:5,2)
	IN-PARAM [array][args] VALUE[(no_tostr_func)]"
TestCmd "echo '' | zlang test_try_4_1.z" 0 "*** errro exception catched
*** exception source : test_try_4_1.z:8,53
*** exception function : [test][IEatFood(string)]
*** message : food is none
FRAME 4 CALLER (test_try_4_1.z:8,33) - CALLEE [error][ThrowException(int,string)] (null:0,0)
	IN-PARAM [int][(null)] VALUE[-1]
	IN-PARAM [string][(null)] VALUE[food is none]
FRAME 3 CALLER (test_try_4_1.z:23,18) - CALLEE [test][IEatFood(string)] (test_try_4_1.z:7,3)
	IN-PARAM [string][food] VALUE[]
FRAME 2 CALLER (test_try_4_1.z:20,3) - CALLEE [(null)][(null)] (test_try_4_1.z:20,3)
	LOCAL-OBJ [string][food] VALUE[]
FRAME 1 CALLER (test_try_4_1.z:18,2) - CALLEE [(null)][main(array)] (test_try_4_1.z:18,2)
	IN-PARAM [array][args] VALUE[(no_tostr_func)]"
TestCmd "echo '' | zlang test_try_4_2.z" 0 "iEatFood return error[-1]"
TestCmd "echo '' | zlang test_try_4_3.z" 1 "test.IEatFood failed[-1]"
TestCmd "echo '' | zlang test_try_4_11.z" 0 "I catch it
finally"
TestCmd "echo '' | zlang test_try_4_12.z" 138 "finally
*** FATAL EXCEPTION CATCHED
*** EXCEPTION SOURCE : test_try_4_12.z:10,54
*** EXCEPTION FUNCTION : [test][(null)]
*** MESSAGE : food is none
FRAME 5 CALLER (test_try_4_12.z:10,34) - CALLEE [fatal][ThrowException(int,string)] (null:0,0)
	IN-PARAM [int][(null)] VALUE[-1]
	IN-PARAM [string][(null)] VALUE[food is none]
FRAME 4 CALLER (test_try_4_12.z:9,4) - CALLEE [test][(null)] (test_try_4_12.z:9,4)
FRAME 3 CALLER (test_try_4_12.z:36,18) - CALLEE [test][IEatFood(string)] (test_try_4_12.z:7,3)
	IN-PARAM [string][food] VALUE[]
FRAME 2 CALLER (test_try_4_12.z:33,3) - CALLEE [(null)][(null)] (test_try_4_12.z:33,3)
	LOCAL-OBJ [string][food] VALUE[]
FRAME 1 CALLER (test_try_4_12.z:31,2) - CALLEE [(null)][main(array)] (test_try_4_12.z:31,2)
	IN-PARAM [array][args] VALUE[(no_tostr_func)]"
TestZ "test_try_string_CharAt_1_1.z" 0 "*** error exception catched
*** exception source : test_try_string_CharAt_1_1.z:10,19
*** exception function : [str][CharAt(int)]
*** message : index out of bounds
FRAME 3 CALLER (test_try_string_CharAt_1_1.z:10,18) - CALLEE [str][CharAt(int)] (null:0,0)
	IN-PARAM [int][(null)] VALUE[6]
FRAME 2 CALLER (test_try_string_CharAt_1_1.z:10,3) - CALLEE [(null)][(null)] (test_try_string_CharAt_1_1.z:10,3)
FRAME 1 CALLER (test_try_string_CharAt_1_1.z:5,2) - CALLEE [(null)][main(array)] (test_try_string_CharAt_1_1.z:5,2)
	IN-PARAM [array][args] VALUE[(no_tostr_func)]
	LOCAL-OBJ [string][str] VALUE[hello]
	LOCAL-OBJ [string][c] VALUE[]"
TestZ "test_try_string_CharAt_1_2.z" 0 "CharAt return error"
TestZ "test_try_break_1.z" 0 "yes"
TestZ "test_try_catch_break_1.z" 0 "yes"
TestZ "test_try_finally_break_1.z" 0 "yess
yes"
TestZ "test_try_5_1.z" 0 "*** errro exception catched
*** exception source : test_try_5_1.z:9,64
*** exception function : [(null)][(null)]
*** code : -1
*** message : throw a exception forcely
FRAME 3 CALLER (test_try_5_1.z:9,31) - CALLEE [error][ThrowException(int,string)] (null:0,0)
	IN-PARAM [int][(null)] VALUE[-1]
	IN-PARAM [string][(null)] VALUE[throw a exception forcely]
FRAME 2 CALLER (test_try_5_1.z:7,3) - CALLEE [(null)][(null)] (test_try_5_1.z:7,3)
	LOCAL-OBJ [string][food] VALUE[]
FRAME 1 CALLER (test_try_5_1.z:5,2) - CALLEE [(null)][main(array)] (test_try_5_1.z:5,2)
	IN-PARAM [array][args] VALUE[(no_tostr_func)]"
TestZ "test_if_or_1.z" 0 "yes 0
yes 1"
TestZ "test_if_and_1.z" 0 "yes 1
no 0"
TestZ "test_if_and_2.z" 0 "yes 1
no 0
no 1"
TestZ "test_member_of_notnull.z" 0 "123
(null)"
TestZ "test_function_vargs_1.z" 0 "3
6
10"
TestZ "test_object_function_vargs_1.z" 0 "3
6
10"
TestZ "test_float_0.z" 0 "1
-1"
TestZ "test_funcp_11.z" 0 "hello world
hello world"
TestZ "test_funcp_12.z" 0 "hello world
hello world"














TestZ "test_thread_1_1.z" 0 "t1"
TestZ "test_thread_1_2.z" 2 "t2"
TestZ "test_thread_2_1.z" 0 "t1"
TestZ "test_thread_3_1.z" 0 "t1
t2"
TestZ "test_exit_1.z" 3 "test_exit"
TestZ "test_thread_exit_1.z" 3 "t1"
TestZ "test_thread_sync_1.z" 0 "func1 - begin
func1 - end
func2 - begin
func2 - end"
TestZ "test_thread_mutex_1.z" 0 "func1 - begin
func1 - end
func2 - begin
func2 - end"
TestZ "test_thread_rwlock_1.z" 0 "func1 - begin
func2 - begin
func1 - end
func2 - end"
TestZ "test_thread_rwlock_2.z" 0 "func2 - begin
func2 - end
func1 - begin
func1 - end"
TestZ "test_thread_rwlock_3.z" 0 "func2 - begin
func1 - not catched lock before begin
func1 - not catched lock before begin
func1 - not catched lock before begin
func2 - end
func1 - begin
func1 - end"
TestZ "test_thread_condsig_1.z" 0 "func1 - begin
func2 - begin
func1 - end
func2 - end"
TestZ "test_thread_condsig_2.z" 0 "func1 - TimedWait 2s ...
func1 - TimedWait 2s done , b[true]
func1 - TimedWait 3s ...
func1 - TimedWait 3s done , b[(null)]"
TestZ "test_thread_condsig_3_1.z" 0 "func1 - Wait ...
func2 - Wait ...
main - Broadcast
func - Wait done , b[true]
func - Wait done , b[true]"
TestZ "test_thread_condsig_3_2.z" 0 "func1 - Wait ...
func2 - Wait ...
main - Broadcast
func - Wait done , b[true]
func - Wait done , b[true]"
TestZ "test_thread_barrier_1.z" 0 "func2 - begin
func1 - begin
func1 - end
func2 - end"
TestZ "test_thread_atomic_1.z" 0 "20000"
TestZ "test_thread_sync_atomic_1_1.z" 0 "20000"
TestZ "test_thread_atomic_list_1.z" 0 "20000"













Test2Z "test_tcp_server_1_1.z" 0 "Accept ok
Receiveln ok
string for receiving : he
Sendln ok" "test_tcp_client_1_1.z he" 0 "Sendln ok
Receiveln ok
get response from server : he"
Test2Z "test_tcp_server_2_1.z" 0 "Receive s ok
Receive i ok
Receive f ok
Receive d ok
Receive lenstr ok
Receive str ok
1
234
56789
1.200000
34.560000
hello
2
235
56790
2.200000
35.560000
hello2
Send s ok
Send i ok
Send f ok
Send d ok
Send lenstr ok
Send str ok" "test_tcp_client_2_1.z" 0 "1
234
56789
1.200000
34.560000
hello
Send s ok
Send i ok
Send l ok
Send f ok
Send d ok
Send lenstr ok
Send str ok
Receive s ok
Receive i ok
Receive l ok
Receive f ok
Receive d ok
Receive lenstr ok
Receive str ok
2
235
56790
2.200000
35.560000
hello2"
Test2Z "test_tcp_server_3_1.z" 3 "Accept ok
Peer closed on Receiveln" "test_tcp_client_3_1.z" 0 ""
Test2Z "test_tcp_server_5_1.z" 11 "he
Sendln err" "test_tcp_client_5_1.z he" 2 "Receiveln timeout"

