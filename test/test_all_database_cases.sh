. test_all_cases_libs.sh

TestZ "test_database_select_0.z" 0 "--- FIELD INFO ---------
1 | id 1032 11 0
2 | name 4016 32 0
3 | salary 2032 12 31
--- QUERY RESULT ---------"

TestZ "test_database_insert_1_1.z" 0 "records 1 affected"
TestZ "test_database_insert_1_2.z" 0 "records 1 affected"
TestZ "test_database_insert_1_3.z" 0 "records 1 affected"
TestZ "test_database_select_0.z" 0 "--- FIELD INFO ---------
1 | id 1032 11 0
2 | name 4016 32 0
3 | salary 2032 12 31
--- QUERY RESULT ---------
1 | 1 calvin 1000
2 | 2 dozimoyi 2000
3 | 3 mark 2000"

TestZ "test_database_update_1.z" 0 "records 1 affected"
TestZ "test_database_select_0.z" 0 "--- FIELD INFO ---------
1 | id 1032 11 0
2 | name 4016 32 0
3 | salary 2032 12 31
--- QUERY RESULT ---------
1 | 1 calvin 10000
2 | 2 dozimoyi 2000
3 | 3 mark 2000"
TestZ "test_database_select_row2obj_1.z" 0 "--- ROW 1 ---
    id : 1
  name : calvin
salary : 10000.000000
--- ROW 2 ---
    id : 2
  name : dozimoyi
salary : 2000.000000
--- ROW 3 ---
    id : 3
  name : mark
salary : 2000.000000"

TestZ "test_database_delete_1.z" 0 "records 3 affected"
TestZ "test_database_select_0.z" 0 "--- FIELD INFO ---------
1 | id 1032 11 0
2 | name 4016 32 0
3 | salary 2032 12 31
--- QUERY RESULT ---------"

