function TestZ
{
	Z=$1
	EXPECT_RETURN=$2
	EXPECT_OUTPUT=$3
	
	printf "TestZ ${Z} ..."
	OUTPUT=`zlang ${Z}`
	RETURN=$?
	if [ ${RETURN} -ne ${EXPECT_RETURN} ] ; then
		echo " return value ${RETURN} not matched with expection ${EXPECT_RETURN}"
		exit 1
	fi
	if [ x"$EXPECT_OUTPUT" != x"" ]; then
		if [ x"${OUTPUT}" != x"${EXPECT_OUTPUT}" ] ; then
			printf " output '${OUTPUT}' not matched with expection '${EXPECT_OUTPUT}'\n"
			echo "${OUTPUT}" >/tmp/calvin1
			echo "${EXPECT_OUTPUT}" >/tmp/calvin2
			echo "--- diff ---"
			diff /tmp/calvin1 /tmp/calvin2
			exit 1
		fi
	fi
	echo " ok"
}

function Test2Z
{
	Z1=$1
	EXPECT_RETURN1=$2
	EXPECT_OUTPUT1=$3
	Z2=$4
	EXPECT_RETURN2=$5
	EXPECT_OUTPUT2=$6
	
	printf "Test2Z ${Z1} "
	zlang ${Z1} >/tmp/zlang_test1_stdout.tmp &
	PID1=`echo $!`
	sleep 1
	printf "and ${Z2} ..."
	OUTPUT2=`zlang ${Z2}`
	RETURN2=$?
	wait ${PID1}
	RETURN1=$?
	OUTPUT1=`cat /tmp/zlang_test1_stdout.tmp`
	if [ ${RETURN1} -ne ${EXPECT_RETURN1} ] ; then
		echo " return value1 ${RETURN1} not matched with expection ${EXPECT_RETURN1}"
		exit 1
	fi
	if [ x"$EXPECT_OUTPUT1" != x"" ]; then
		if [ x"${OUTPUT1}" != x"${EXPECT_OUTPUT1}" ] ; then
			printf " output1 '${OUTPUT1}' not matched with expection '${EXPECT_OUTPUT1}'\n"
			exit 1
		fi
	fi
	if [ ${RETURN2} -ne ${EXPECT_RETURN2} ] ; then
		echo " return value2 ${RETURN2} not matched with expection ${EXPECT_RETURN2}"
		exit 1
	fi
	if [ x"$EXPECT_OUTPUT2" != x"" ]; then
		if [ x"${OUTPUT2}" != x"${EXPECT_OUTPUT2}" ] ; then
			printf " output2 '${OUTPUT2}' not matched with expection '${EXPECT_OUTPUT2}'\n"
			exit 1
		fi
	fi
	echo " ok"
}

function TestCmd
{
	CMD=$1
	EXPECT_RETURN=$2
	EXPECT_OUTPUT=$3
	
	printf "TestCmd ${CMD} ..."
	OUTPUT=`sh -c "${CMD}"`
	RETURN=$?
	if [ ${RETURN} -ne ${EXPECT_RETURN} ] ; then
		echo " return value ${RETURN} not matched with expection ${EXPECT_RETURN}"
		exit 1
	fi
	if [ x"$EXPECT_OUTPUT" != x"" ]; then
		if [ x"${OUTPUT}" != x"${EXPECT_OUTPUT}" ] ; then
			printf " output '${OUTPUT}' not matched with expection '${EXPECT_OUTPUT}'\n"
			echo "${OUTPUT}" > /tmp/output
			echo "${EXPECT_OUTPUT}" > /tmp/expect_output
			diff /tmp/output /tmp/expect_output
			exit 1
		fi
	fi
	echo " ok"
}

function TestFile
{
	Z=$1
	EXPECT_RETURN=$2
	OUTPUT_FILE=$3
	EXPECT_OUTPUT_FILE=${OUTPUT_FILE}.expect
	
	printf "TestZ ${Z} ..."
	OUTPUT=`zlang ${Z}`
	RETURN=$?
	if [ ${RETURN} -ne ${EXPECT_RETURN} ] ; then
		echo " return value ${RETURN} not matched with expection ${EXPECT_RETURN}"
		exit 1
	fi
	diff ${OUTPUT_FILE} ${EXPECT_OUTPUT_FILE}
	if [ $? -ne 0 ]; then
		printf " file '${OUTPUT_FILE}' not matched with expection '${EXPECT_OUTPUT_FILE}'\n"
		exit 1
	fi
	echo " ok"
}

function TestLogFile
{
	Z=$1
	EXPECT_RETURN=$2
	LOG_FILE=$3
	EXPECT_LOG_FILE=$4
	
	printf "TestZ ${Z} ..."
	rm -f ~/log/*.log*
	LOG=`zlang ${Z}`
	RETURN=$?
	if [ ${RETURN} -ne ${EXPECT_RETURN} ] ; then
		echo " return value ${RETURN} not matched with expection ${EXPECT_RETURN}"
		exit 1
	fi
	OUTPUT=`awk 'NR==FNR{c[$8]++;next};c[$8] == 0' ${LOG_FILE} ${EXPECT_LOG_FILE}`
	if [ x"$OUTPUT" != x"" ]; then
		printf " log file not matched with expection file , diff:'$OUTPUT'\n"
		exit 1
	fi
	echo " ok"
}

