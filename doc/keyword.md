上一章：[运算符](operator.md)



**章节目录**
- [关键字](#关键字)
  - [zlang关键字](#zlang关键字)



# 关键字

## zlang关键字

| 关键字类       | 关键字     | 中文关键字 | 说明                                                         |
| -------------- | ---------- | ---------- | ------------------------------------------------------------ |
| 代码头部指令类 | import     | 导入       | 导入二进制对象；自带对象库源码在`src/zobjects/对象库名/`，第三方对象库源码任意，编译后安装到`$HOME/lib/libzlang_对象库名.so`，zlang会统一从`$HOME/lib`统一查找`libzlang_对象库名.so`的二进制对象文件 |
| 代码头部指令类 | charset    | （无）     | 设置源代码字符集，影响字符串字面量、中文编程（中文关键字、中文标识等），目前只支持`UTF8`和`GB18030`，如`charset "zh_CN.gb18030"`，值参考于操作系统`locale -a`。注意：语句最后没有';' |
| 代码头部指令类 | include    | 包含文件   | 包含zlang其他源代码文件，如`include "util.z"`                |
| 流程控制类     | if         | 如果       | 条件判断，如`if( n == 0 ) { ... }`                           |
| 流程控制类     | else       | 否则       | 条件判断，如`if( n == 0 ) { ... } else { ... }`              |
| 流程控制类     | switch     | 分支判断   | 分支判断，如`switch(str) { ... }`                            |
| 流程控制类     | case       | 分支匹配   | 分支判断，如`switch(str) { case "mark": ... break; }`        |
| 流程控制类     | default    | 分支缺省   | 分支判断，如`switch(str) { case "mark": ... break; default: ... break }` |
| 流程控制类     | for        | 循环       | 循环控制，如`for( int n = 0 ; n < 3 ; n++ ) { ... }`         |
| 流程控制类     | do         | 做循环     | 循环控制，如`do{ ... } while( n < 3 );`                      |
| 流程控制类     | while      | 循环判断   | 循环控制，如`while( n < 3 ){ ... }`                          |
| 流程控制类     | foreach    | 循环每一个 | 循环控制，如`foreach( string s in args ){ ... }`             |
| 流程控制类     | in         | 在里面     | 循环控制，如同上                                             |
| 流程控制类     | from       | 从         | 循环控制，如`foreach( int n from 1 to 10 ){ ... }`           |
| 流程控制类     | to         | 到         | 循环控制，如同上                                             |
| 流程控制类     | continue   | 继续       | 跳转控制，如`while( n < 3 ){ if( n ==1 ) continue; ... }`    |
| 流程控制类     | break      | 中断       | 跳转控制，如`while( n < 3 ){ if( n ==1 ) break; ... }`，还能跳出`try`、`catch` |
| 流程控制类     | try        | 异常监视   | 开始一个异常监视段                                           |
| 流程控制类     | throw      | 抛出异常   | 在异常监视段里主动抛出一个异常                               |
| 流程控制类     | catch      | 捕获异常   | 在异常监视段里捕获异常执行段                                 |
| 流程控制类     | uncatch    | 不捕获异常 | 语句剩余部分不捕获异常                                       |
| 流程控制类     | finally    | 异常监视后 | 异常监视段后执行段                                           |
| 流程控制类     | defer      | 延后       | 延迟到语句块或函数结束时执行                                 |
| 流程控制类     | return     | 返回       | 跳转控制，如`function f1(){ ... return; }`                   |
| 流程控制类     | exit       | 退出       | 跳转控制，如`function f1(){ ... exit; ... }`                 |
| 对象定义类     | function   | 函数       | 定义函数，如`function f1(){ ... }`                           |
| 对象定义类     | object     | 对象       | 声明对象，如`object obj{ ... }`                              |
| 对象定义类     | public     | 公共的     | 声明对象，如`object obj{ public string name; public function func(){...} }` |
| 对象定义类     | private    | 私有的     | 声明对象，如`object obj{ private string name; private function func(){...} }` |
| 对象定义类     | extends    | 继承       | 继承对象，如`object obj extends base_obj { ... }`            |
| 对象定义类     | interface  | 接口       | 声明接口，如`interface if{ ... }`                            |
| 对象定义类     | implements | 实现       | 实现接口，如`object obj implements if { ... }`               |
| 对象定义类     | this       | 本对象     | 引用本对象，如`object obj{ public string name; public function func(string name){this.name=name;} }` |
| 拦截器类       | intercept  | 拦截器     | 定义拦截器                                                   |
| 拦截器类       | before     | 在之前     | 定义函数前置拦截器，如`intercept before `func1(int)`( object o , functionp f , array in_params , array out_params )` |
| 拦截器类       | after      | 在之后     | 定义函数后置拦截器，如intercept after `func1(int)`( object o , functionp f , array in_params , array out_params )` |
| 拦截器类       | set        | 设置       | 定义设置属性拦截器，如`intercept get name( object name )`    |
| 拦截器类       | get        | 得到       | 定义获取属性拦截器，如`intercept set age( object age )`      |
| 其它类         | new        | 新的       | 用相同的变量名重新声明变量，如`int new i;`                   |
| 其它类         | sync       | 同步       | 借用mutex对象创造一个临界互斥区                              |
| 其他类         | atomic     | 原子的     | 声明变量前的修饰符，修饰为原子操作对象，如`atomic int n;`    |
| 其他类         | const      | 常量       | 声明变量前的修饰符，修饰为不可变值的对象，如`const int n;`   |



下一章：[条件语句](if_statement.md)
