上一章：[标准输入输出](stdio.md)



**章节目录**
- [系统环境](#系统环境)



# 系统环境

系统环境对象用于调用一些本地操作系统的功能，目前包括沉睡一段时间、获取环境变量。

系统环境对象：`system`

| 成员类型 | 成员名字和原型                                            | 说明                   | (错误.)(异常码,)异常消息            |
| -------- | --------------------------------------------------------- | ---------------------- | ----------------------------------- |
| 函数     | void SleepSeconds(int);                                   | 沉睡指定秒数           |                                     |
| 函数     | void SleepMilliseconds(int);                              | 沉睡指定毫秒数         |                                     |
| 函数     | void SleepMicroseconds(int);                              | 沉睡指定微秒数         |                                     |
| 函数     | void SleepNanoseconds(int);                               | 沉睡指定纳秒数         |                                     |
| 函数     | string GetEnvironmentVar(string var_name);                | 得到指定环境变量键的值 | EXCEPTION_MESSAGE_GETENV_VALUE_NULL |
| 函数     | bool SetEnvironmentVar(string var_name,string var_value); | 设置制定环境变量键和值 | EXCEPTION_MESSAGE_SETENV_FAILED     |
| 函数     | bool UnsetEnvironmentVar(string var_name);                | 删除指定环境变量键     | EXCEPTION_MESSAGE_UNSETENV_FAILED   |

代码示例：`test_system_SleepSeconds_1.z`

该代码示范了暂停应用运行一秒钟。

```
import stdtypes stdio system ;

function int main( array args )
{
        system.SleepSeconds(1);

        return 0;
}
```

执行

```
$ zlang test_system_SleepSeconds_1.z
（一秒钟后退出）
```



下一章：[文件系统路径和文件](file.md)