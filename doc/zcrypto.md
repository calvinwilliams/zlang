上一章：[中文编程](chinese_programming.md)



章节目录
- [加解密工具](#加解密工具)



# 加解密工具

调用`zlang`对象库`crypto`中的各种算法对象，可以很轻松的实现一个加解密工具。

开源工具`zcrypto`就是一个例子，源代码在`https://gitee.com/calvinwilliams/zcrypto`。

```
import stdtypes stdio encoding crypto ;

// 显示命令行参数说明
function void usage()
{
	stdout.Println( "USAGE : zcrypto.sh -v" );
	stdout.Println( "                   -a:(MD2|MD4|MD5|SHA1|SHA224|SHA256|SHA384|SHA512) -i:(TEXT|HEXSTR|BASE64|FILE) (INPUT_DATA) --salt:(TEXT|HEXSTR|BASE64|FILE) (SALT_DATA) -o:(TEXT|BINARY|HEXSTR|BASE64)" );
	stdout.Println( "                   -a:(DES|TRIPLEDES).(ENCRYPT|DECRYPT) -m:(ECB|CBC) -k:(TEXT|HEXSTR|BASE64|FILE) (KEY_DATA) -i:(TEXT|HEXSTR|BASE64|FILE) (INPUT_DATA) --padding:(ZERO|PKCS7) -o:(TEXT|BINARY|HEXSTR|BASE64)" );
	stdout.Println( "                   -a:RSA.GENERATE_KEY --key-len-bytes (512|1024|2048|4096) --e (3|F4) --export-prikey-pem prikey_pem_filename --export-pubkey-pem pubkey_pem_filename --export-prikey-der prikey_der_filename --export-pubkey-der pubkey_der_filename" );
	stdout.Println( "                   -a:RSA.ENCRYPT --pubkey-pem pubkey_pem_filename|--pubkey-der pubkey_der_filename -i:(TEXT|HEXSTR|BASE64|FILE) (INPUT_DATA) --padding:(NO|PKCS7|PKCS7_OAEP) -o:(TEXT|BINARY|HEXSTR|BASE64)" );
	stdout.Println( "                   -a:RSA.DECRYPT --prikey-pem prikey_pem_filename|--prikey-der prikey_der_filename -i:(TEXT|HEXSTR|BASE64|FILE) (INPUT_DATA) --padding:(NO|PKCS7|PKCS7_OAEP) -o:(TEXT|BINARY|HEXSTR|BASE64)" );
	stdout.Println( "                   -a:RSA.SIGN --digest:(MD5|SHA1|SHA224|SHA256|SHA384|SHA512) -i:(TEXT|HEXSTR|BASE64|FILE) (INPUT_DATA) --prikey-pem prikey_pem_filename|--prikey-der prikey_der_filename -o:(TEXT|BINARY|HEXSTR|BASE64)" );
	stdout.Println( "                   -a:RSA.VERIFY --digest:(MD5|SHA1|SHA224|SHA256|SHA384|SHA512) -i:(TEXT|HEXSTR|BASE64|FILE) (INPUT_DATA) --sign:(TEXT|HEXSTR|BASE64|FILE) (SIGN_DATA) --pubkey-pem pubkey_pem_filename|--pubkey-der pubkey_der_filename" );
}

// 显示版本号
function void version()
{
	stdout.Println( "zcrypto.sh v0.0.1.0" );
}

// 主函数
function int main( array args )
{
	// 如果不带参数运行，则显示命令行参数说明，然后退出
	if( args.Length() == 0 )
	{
		usage();
		return 0;
	}
	// 如果只有参数"-v"，则显示版本号，然后退出
	else if( args.Length() == 1 && args.Get(1) == "-v" )
	{
		version();
		return 0;
	}
	
	// 声明本地变量
	string	algorithm_and_method ;
	string	algorithm ;
	string	method ;
	string	mode ;
	string	digest ;
	string	input_format ;
	string	input_data ;
	string	salt_format ;
	string	salt_data ;
	string	sign_format ;
	string	sign_data ;
	string	key_format ;
	string	key_data ;
	string	key_len_bytes ;
	string	e ;
	string	export_prikey_pem ;
	string	export_pubkey_pem ;
	string	export_prikey_der ;
	string	export_pubkey_der ;
	string	prikey_pem ;
	string	pubkey_pem ;
	string	prikey_der ;
	string	pubkey_der ;
	string	padding_format ;
	string	output_format ;
	string	output_data ;
	string	prev_arg ;
	
	// 解析所有命令行参数和值
	prev_arg = "" ;
	foreach( string arg in args )
	{
		if( prev_arg.StartWith( "-k" ) )
		{
			key_data = arg ;
		}
		else if( prev_arg.StartWith( "-i" ) )
		{
			input_data = arg ;
		}
		else if( prev_arg.StartWith( "--sign" ) )
		{
			sign_data = arg ;
		}
		else if( prev_arg.StartWith( "--salt" ) )
		{
			salt_data = arg ;
		}
		else if( prev_arg == "--key-len-bytes" )
		{
			key_len_bytes = arg ;
		}
		else if( prev_arg == "--e" )
		{
			e = arg ;
		}
		else if( prev_arg == "--export-prikey-pem" )
		{
			export_prikey_pem = arg ;
		}
		else if( prev_arg == "--export-pubkey-pem" )
		{
			export_pubkey_pem = arg ;
		}
		else if( prev_arg == "--export-prikey-der" )
		{
			export_prikey_der = arg ;
		}
		else if( prev_arg == "--export-pubkey-der" )
		{
			export_pubkey_der = arg ;
		}
		else if( prev_arg == "--prikey-pem" )
		{
			prikey_pem = arg ;
		}
		else if( prev_arg == "--pubkey-pem" )
		{
			pubkey_pem = arg ;
		}
		else if( prev_arg == "--prikey-der" )
		{
			prikey_der = arg ;
		}
		else if( prev_arg == "--pubkey-der" )
		{
			pubkey_der = arg ;
		}
		else if( arg.StartWith( "-a" ) )
		{
			algorithm_and_method = arg.SubString( 4 , arg.Length() ) ;
			if( algorithm_and_method.IndexOf( "." ) > 0 )
				algorithm_and_method.RegexpExtract( '''([_A-Z]+)\.([_A-Z]+)''' , algorithm , method ) ;
			else
				algorithm = algorithm_and_method ;
		}
		else if( arg.StartWith( "-m" ) )
		{
			mode = arg.SubString( 4 , arg.Length() ) ;
		}
		else if( arg.StartWith( "--digest" ) )
		{
			digest = arg.SubString( 10 , arg.Length() ) ;
		}
		else if( arg.StartWith( "--sign" ) )
		{
			sign_format = arg.SubString( 8 , arg.Length() ) ;
		}
		else if( arg.StartWith( "-k" ) )
		{
			key_len_type = arg.SubString( 4 , arg.Length() ) ;
		}
		else if( arg.StartWith( "-i" ) )
		{
			input_format = arg.SubString( 4 , arg.Length() ) ;
		}
		else if( arg.StartWith( "--sign" ) )
		{
			sign_format = arg.SubString( 8 , arg.Length() ) ;
		}
		else if( arg.StartWith( "--padding" ) )
		{
			padding_format = arg.SubString( 11 , arg.Length() ) ;
		}
		else if( arg.StartWith( "--salt" ) )
		{
			salt_format = arg.SubString( 8 , arg.Length() ) ;
		}
		else if( arg == "--key-len-bytes" || arg == "--e" || arg == "--export-prikey-pem" || arg == "--export-pubkey-pem" || arg == "--export-prikey-der" || arg == "--export-pubkey-der" )
		{
			;
		}
		else if( arg == "--prikey-pem" || arg == "--pubkey-pem" || arg == "--prikey-der" || arg == "--pubkey-der" )
		{
			;
		}
		else if( arg.StartWith("-o") )
		{
			output_format = arg.SubString( 4 , arg.Length() ) ;
		}
		else
		{
			stdout.FormatPrintln( "*** ERROR : Unknow arg [%{arg}]" );
			return 1;
		}
		
		prev_arg = arg ;
	}
	
	// 检查输入数据格式和值
	if( input_format != "" && input_data != "" )
	{
		if( input_format == "TEXT" )
			;
		else if( input_format == "HEXSTR" )
			input_data = hexstr.HexFold( input_data ) ;
		else if( input_format == "BASE64" )
			input_data = base64.HexFold( input_data ) ;
		else if( input_format == "FILE" )
			input_data = stdfile.ReadFileToString( input_data ) ;
		else
		{
			stdout.FormatPrintln( "*** ERROR : -i:%{input_format} invalid" );
			return 1;
		}
	}
	
	// 如果输入盐，转换成统一格式
	if( salt_format != "" && salt_data != "" )
	{
		if( salt_format == "TEXT" )
			;
		else if( salt_format == "HEXSTR" )
			salt_data = hexstr.HexFold( salt_data ) ;
		else if( salt_format == "BASE64" )
			salt_data = base64.Decode( salt_data ) ;
		else if( salt_format == "FILE" )
			salt_data = stdfile.ReadFileToString( salt_data ) ;
		else
		{
			stdout.FormatPrintln( "*** ERROR : --salt:%{salt_format} invalid" );
			return 1;
		}
	}
	
	// 如果输入签名值，转换成统一格式
	if( sign_format != "" && sign_data != "" )
	{
		if( sign_format == "TEXT" )
			;
		else if( sign_format == "HEXSTR" )
			sign_data = hexstr.HexFold( sign_data ) ;
		else if( sign_format == "BASE64" )
			sign_data = base64.Decode( sign_data ) ;
		else if( sign_format == "FILE" )
			sign_data = stdfile.ReadFileToString( sign_data ) ;
		else
		{
			stdout.FormatPrintln( "*** ERROR : --sign:%{sign_format} invalid" );
			return 1;
		}
	}
	
	// 如果输入密钥，转换成统一格式
	if( key_format != "" && key_data != "" )
	{
		if( key_format == "TEXT" )
			;
		else if( key_format == "HEXSTR" )
			key_data = hexstr.HexFold( key_data ) ;
		else if( key_format == "BASE64" )
			key_data = base64.Decode( key_data ) ;
		else if( key_format == "FILE" )
			key_data = stdfile.ReadFileToString( key_data ) ;
		else
		{
			stdout.FormatPrintln( "*** ERROR : -k:%{key_format} invalid" );
			return 1;
		}
	}
	
	// 如果算法是散列
	if( algorithm == "MD2" || algorithm == "MD4" || algorithm == "MD5" || algorithm == "SHA1" || algorithm == "SHA224" || algorithm == "SHA256" || algorithm == "SHA384" || algorithm == "SHA512" )
	{
		// zcrypto.sh -a:MD5 -i:TEXT abc -o:HEXSTR
		// zcrypto.sh -a:SHA256 -i:TEXT abc --salt:TEXT xyz -o:HEXSTR
		
		if( input_data == "" )
		{
			stdout.FormatPrintln( "*** ERROR : no input data -a %{algorithm}" );
			return 1;
		}
		
		algorithm.LowerCase();
		if( salt_format == "" )
			output_data = ```algorithm```.Digest( input_data ) ; // 使用三引号，取出变量algorithm的值，作为中间变量的名字，调用中间变量的函数Digest
		else
			output_data = ```algorithm```.Digest( input_data , salt_data ) ;
	}
	// 如果算法是对称DES族
	else if( algorithm == "DES" || algorithm == "TRIPLEDES" )
	{
		// zcrypto.sh -a:DES.ENCRYPT -k:TEXT 12345678 -i:TEXT abc --padding:ZERO -o:HEXSTR
		// zcrypto.sh -a:DES.DECRYPT -k:TEXT 12345678 -i:HEXSTR 2C8369311A2E38FA --padding:ZERO -o:TEXT
		
		// zcrypto.sh -a:TRIPLEDES.ENCRYPT -m:CBC -k:TEXT 123456781234567812345678 -i:TEXT abc --padding:PKCS7 -o:HEXSTR
		// zcrypto.sh -a:TRIPLEDES.DECRYPT -m:CBC -k:TEXT 123456781234567812345678 -i:HEXSTR C24B98FAD5C0580E --padding:PKCS7 -o:TEXT
		
		// 检查加解密模式
		if( mode == "" )
			mode_value = crypto.ECB_MODE ;
		else if( mode == "ECB" )
			mode_value = crypto.ECB_MODE ;
		else if( mode == "CBC" )
			mode_value = crypto.CBC_MODE ;
		else
		{
			stdout.FormatPrintln( "*** ERROR : -m:%{mode} invalid" );
			return 1;
		}
		
		// 检查密钥数据
		if( key_data == "" )
		{
			stdout.FormatPrintln( "*** ERROR : no key data -a %{algorithm}" );
			return 1;
		}
		
		// 检查密钥长度
		key_len_type = key_data.Length() ;
		if( key_len_type == 8 )
			key_len_type_value = crypto.KEY_64BITS ;
		else if( key_len_type == 16 )
			key_len_type_value = crypto.KEY_128BITS ;
		else if( key_len_type == 24 )
			key_len_type_value = crypto.KEY_192BITS ;
		else
		{
			stdout.FormatPrintln( "*** ERROR : -k:%{key_len_type} invalid" );
			return 1;
		}
		
		// 检查输入数据
		if( input_data == "" )
		{
			stdout.FormatPrintln( "*** ERROR : no input data -a %{algorithm}" );
			return 1;
		}
		
		// 检查填充模式
		if( padding_format == "" )
			padding_format_value = crypto.PKCS7_PADDING ;
		else if( padding_format == "ZERO" )
			padding_format_value = crypto.ZERO_PADDING ;
		else if( padding_format == "PKCS7" )
			padding_format_value = crypto.PKCS7_PADDING ;
		else
		{
			stdout.FormatPrintln( "*** ERROR : -p:%{padding_format} invalid" );
			return 1;
		}
		
		// 用对称密钥做DES族的加密或解密处理
		algorithm.LowerCase();
		if( algorithm == "des" )
		{
			if( method == "ENCRYPT" )
			{
				output_data = ```algorithm```.Encrypt( key_len_type_value , key_data , input_data , padding_format_value ) ;
			}
			else if( method == "DECRYPT" )
			{
				output_data = ```algorithm```.Decrypt( key_len_type_value , key_data , input_data , padding_format_value ) ;
			}
		}
		else if( algorithm == "tripledes" )
		{
			if( method == "ENCRYPT" )
			{
				output_data = ```algorithm```.Encrypt( mode_value , key_len_type_value , key_data , input_data , padding_format_value ) ;
			}
			else if( method == "DECRYPT" )
			{
				output_data = ```algorithm```.Decrypt( mode_value , key_len_type_value , key_data , input_data , padding_format_value ) ;
			}
		}
	}
	// 如果算法是RSA组（密钥管理、对称加解密、非对称签名验签）
	else if( algorithm == "RSA" )
	{
		// zcrypto.sh -a:RSA.GENERATE_KEY --key-len-bytes 4096 --export-prikey-pem /tmp/zcrypto.prikey.pem --export-pubkey-pem /tmp/zcrypto.pubkey.pem --export-prikey-der /tmp/zcrypto.prikey.der --export-pubkey-der /tmp/zcrypto.pubkey.der
		
		if( method == "GENERATE_KEY" )
		{
			// 检查密钥长度
			if( key_len_bytes == "512" )
				key_len_bytes_value = 512 ;
			else if( key_len_bytes == "1024" )
				key_len_bytes_value = 1024 ;
			else if( key_len_bytes == "2048" )
				key_len_bytes_value = 2048 ;
			else if( key_len_bytes == "4096" )
				key_len_bytes_value = 4096 ;
			else
			{
				stdout.FormatPrintln( "*** ERROR : key_len_bytes:%{key_len_bytes} invalid" );
				return 1;
			}
			
			// 检查生成密钥对的种子
			if( e == "" )
				;
			else if( e == "3" )
				e_value = crypto.E_3 ;
			else if( e == "F4" )
				e_value = crypto.E_F4 ;
			else
			{
				stdout.FormatPrintln( "*** ERROR : e:%{e} invalid" );
				return 1;
			}
			
			// 生成密钥对处理
			if( e == "" )
				b = rsakey.GenerateKeyPair( key_len_bytes_value ) ;
			else
				b = rsakey.GenerateKeyPair( key_len_bytes_value , e_value ) ;
			
			// 如果输入了密钥文件名，则写密钥文件
			if( export_prikey_pem != "" )
				b = rsakey.ExportPrivateKeyToPEM( export_prikey_pem ) ;
			if( export_pubkey_pem != "" )
				b = rsakey.ExportPublicKeyToPEM( export_pubkey_pem ) ;
			if( export_prikey_der != "" )
				b = rsakey.ExportPrivateKeyToDER( export_prikey_der ) ;
			if( export_pubkey_der != "" )
				b = rsakey.ExportPublicKeyToDER( export_pubkey_der ) ;
			if( b != true )
			{
				stdout.FormatPrintln( "*** ERROR : export failed" );
				return 1;
			}
		}
		else if( method == "ENCRYPT" || method == "DECRYPT" )
		{
			// zcrypto.sh -a:RSA.ENCRYPT --pubkey-pem /tmp/zcrypto.pubkey.pem -i:TEXT abc --padding:PKCS1_OAEP -o:BINARY > /tmp/zcrypto.rsa.enc
			// zcrypto.sh -a:RSA.DECRYPT --prikey-pem /tmp/zcrypto.prikey.pem -i:FILE /tmp/zcrypto.rsa.enc --padding:PKCS1_OAEP -o:TEXT
			
			rsakey	prikey , pubkey ;
			
			// 从密钥文件导入非对称密钥
			b = false ;
			if( prikey_pem != "" )
				b = prikey.ImportPrivateKeyFromPEM( prikey_pem ) ;
			else if( prikey_der != "" )
				b = prikey.ImportPrivateKeyFromDER( prikey_der ) ;
			else if( pubkey_pem != "" )
				b = pubkey.ImportPublicKeyFromPEM( pubkey_pem ) ;
			else if( pubkey_der != "" )
				b = pubkey.ImportPublicKeyFromDER( pubkey_der ) ;
			if( b != true )
			{
				stdout.FormatPrintln( "*** ERROR : import key failed" );
				return 1;
			}
			
			// 检查填充模式
			if( padding_format == "" )
				padding_format_value = crypto.PKCS7_PADDING ;
			else if( padding_format == "NO" )
				padding_format_value = crypto.NO_PADDING ;
			else if( padding_format == "PKCS1" )
				padding_format_value = crypto.PKCS1_PADDING ;
			else if( padding_format == "PKCS1_OAEP" )
				padding_format_value = crypto.PKCS1_OAEP_PADDING ;
			else
			{
				stdout.FormatPrintln( "*** ERROR : -p:%{padding_format} invalid" );
				return 1;
			}
			
			// 用非对称密钥做RSA加密或解密处理
			if( method == "ENCRYPT" )
			{
				output_data = rsa.PublicKeyEncrypt( pubkey , input_data , padding_format_value ) ;
			}
			else if( method == "DECRYPT" )
			{
				output_data = rsa.PrivateKeyDecrypt( prikey , input_data , padding_format_value ) ;
			}
		}
		else if( method == "SIGN" || method == "VERIFY" )
		{
			// zcrypto.sh -a:RSA.SIGN --digest:SHA1 -i:TEXT abc --prikey-pem /tmp/zcrypto.prikey.pem -o:BINARY > /tmp/zcrypto.rsa.sign
			// zcrypto.sh -a:RSA.VERIFY --digest:SHA1 -i:TEXT abc --sign:FILE /tmp/zcrypto.rsa.sign --pubkey-pem /tmp/zcrypto.pubkey.pem
			
			rsakey	prikey , pubkey ;
			
			// 检查散列算法
			if( digest == "MD5" )
				digest_value = crypto.NID_MD5 ;
			else if( digest == "SHA1" )
				digest_value = crypto.NID_SHA1 ;
			else if( digest == "SHA224" )
				digest_value = crypto.NID_SHA224 ;
			else if( digest == "SHA256" )
				digest_value = crypto.NID_SHA256 ;
			else if( digest == "SHA512" )
				digest_value = crypto.NID_SHA512 ;
			else if( digest == "MD5_SHA1" )
				digest_value = crypto.NID_md5_sha1 ;
			else
			{
				stdout.FormatPrintln( "*** ERROR : digest:%{digest} invalid" );
				return 1;
			}
			
			// 从密钥文件导入非对称密钥
			b = false ;
			if( prikey_pem != "" )
				b = prikey.ImportPrivateKeyFromPEM( prikey_pem ) ;
			else if( prikey_der != "" )
				b = prikey.ImportPrivateKeyFromDER( prikey_der ) ;
			else if( pubkey_pem != "" )
				b = pubkey.ImportPublicKeyFromPEM( pubkey_pem ) ;
			else if( pubkey_der != "" )
				b = pubkey.ImportPublicKeyFromDER( pubkey_der ) ;
			if( b != true )
			{
				stdout.FormatPrintln( "*** ERROR : import key failed" );
				return 1;
			}
			
			// 用非对称密钥做RSA签名或验签
			if( method == "SIGN" )
			{
				output_data = rsa.Sign( digest_value , input_data , prikey ) ;
			}
			else if( method == "VERIFY" )
			{
				b = rsa.Verify( digest_value , input_data , sign_data , pubkey ) ;
				if( b == true )
					stdout.FormatPrintln( "Verify ok" );
				else if( b == false )
					stdout.FormatPrintln( "Verify failed" );
			}
		}
		else
		{
			stdout.FormatPrintln( "*** ERROR : method:%{method} invalid" );
			return 1;
		}
	}
	else
	{
		stdout.FormatPrintln( "*** ERROR : -a:%{algorithm} invalid" );
		return 1;
	}
	
	// 如果存在输出，转换成统一格式
	if( output_data != "" )
	{
		if( output_format == "TEXT" )
			stdout.Println( output_data );
		else if( output_format == "BINARY" )
			stdout.Write( output_data );
		else if( output_format == "HEXSTR" )
		{
			output_data = hexstr.HexExpand( output_data ) ;
			stdout.FormatPrintln( "%{output_data}" );
		}
		else if( output_format == "BASE64" )
		{
			output_data = base64.Encode( output_data ) ;
			stdout.FormatPrintln( "%{output_data}" );
		}
		else
		{
			stdout.FormatPrintln( "*** ERROR : -o:%{output_format} invalid" );
			return 1;
		}
	}
	
	return 0;
}
```

