上一章：[编码](encoding.md)



**章节目录**
- [日志](#日志)
  - [单日志](#单日志)
  - [多日志](#多日志)



# 日志

日志用于应用系统中不可缺少的调试和运维跟踪工具。应用在运行处理数据时，把内部处理步骤和数据写到日志文件里去，开发人员通过日志文件就能判断应用是否运行符合预期。

zlang日志对象库基于我的开源库`iLOG3`。

## 单日志

单日志是，应用调用一条输出日志语句，最多写出一个日志文件。

单日志对象：`log`

| 成员类型 | 成员名字和原型                     | 说明                                                         |
| :------: | ---------------------------------- | ------------------------------------------------------------ |
|   常量   | int OUTPUT_INVALID                 | 日志输出类型：无输出                                         |
|   常量   | int OUTPUT_STDOUT                  | 日志输出类型：输出到标准输出                                 |
|   常量   | int OUTPUT_STDERR                  | 日志输出类型：输出到标准错误输出                             |
|   常量   | int OUTPUT_SYSLOG                  | 日志输出类型：输出到syslog                                   |
|   常量   | int OUTPUT_FILE                    | 日志输出类型：输出到文件                                     |
|   常量   | int LEVEL_DEBUG                    | 日志等级：以DEBUG等级输出日志，DEBUG往往用于每个重要变量变化的地方，生产环境的日志等级往往比DEBUG高，那么应用就不会大量输出详细的日志了 |
|   常量   | int LEVEL_INFO                     | 日志等级：以INFO等级输出日志，INFO往往用于每个函数入出口、每段代码之间 |
|   常量   | int LEVEL_NOTICE                   | 日志等级：以NOTICE等级输出日志                               |
|   常量   | int LEVEL_WARN                     | 日志等级：以WARN等级输出日志，WARN往往用于一般警告，应用不用中断本轮数据处理，但需要提示给开发人员 |
|   常量   | int LEVEL_ERROR                    | 日志等级：以ERROR等级输出日志，ERROR往往用于一般错误，应用通过错误处理逻辑可以修正 |
|   常量   | int LEVEL_FATAL                    | 日志等级：以FATAL等级输出日志，FATAL往往用于致命错误导致应用无法继续运行下去 |
|   常量   | int LEVEL_NOLOG                    | 日志等级：无等级，始终不输出                                 |
|   常量   | int STYLE_DATE                     | 日志行样式：日志行左边固定样式区要有日期                     |
|   常量   | int STYLE_DATETIME                 | 日志行样式：日志行左边固定样式区要有日期时间                 |
|   常量   | int STYLE_DATETIMEMS               | 日志行样式：日志行左边固定样式区要有日期时间待毫秒           |
|   常量   | int STYLE_LOGLEVEL                 | 日志行样式：日志行左边固定样式区要有日志等级                 |
|   常量   | int STYLE_PID                      | 日志行样式：日志行左边固定样式区要有进程ID                   |
|   常量   | int STYLE_TID                      | 日志行样式：日志行左边固定样式区要有线程ID                   |
|   常量   | int STYLE_SOURCE                   | 日志行样式：日志行左边固定样式区要有源代码文件名和行号       |
|   常量   | int STYLE_FORMAT                   | 日志行样式：日志行中间允许出现应用自定义信息                 |
|   常量   | int STYLE_NEWLINE                  | 日志行样式：日志行右边允许出现换行                           |
|   常量   | int OPTION_OPEN_AND_CLOSE          | 日志选项：每次写日志文件都打开，写完再关闭                   |
|   常量   | int OPTION_CHANGE_TEST             | 日志选项：启用日志大小测试，与日志大小转档模式搭配使用       |
|   常量   | int OPTION_OPEN_ONCE               | 日志选项：第一次写日志要打开，完后后不关闭，下次接着写       |
|   常量   | int OPTION_SET_OUTPUT_BY_FILENAME  | 日志选项：不通过日志输出器，而是通过日志文件名，设置输出到标准输出（"#stdout#"）、标准错误输出（"#stderr#"）、syslog（"#syslog#"） |
|   常量   | int OPTION_FILENAME_APPEND_DOT_LOG | 日志选项：自动在日志文件名后追加扩展名".log"                 |
|   常量   | int ROTATEMODE_NONE                | 日志转档模式：不转档（缺省）                                 |
|   常量   | int ROTATEMODE_SIZE                | 日志转档模式：日志按大小转档                                 |
|   常量   | int ROTATEMODE_PER_DAY             | 日志转档模式：日志按天转档                                   |
|   常量   | int ROTATEMODE_PER_HOUR            | 日志转档模式：日志按小时转档                                 |
|   函数   | int SetOutput(int,string);         | 设置日志输出，输入参数为输出类型、文件名                     |
|   函数   | int SetLevel(int);                 | 设置日志等级，当等级为正时，输出过滤掉低于设置值的日志，当等级为负时，输出过滤掉等于设置值的日志 |
|   函数   | int SetStyles(int);                | 设置日志行样式                                               |
|   函数   | int SetOptions(int);               | 设置日志选项                                                 |
|   函数   | int SetRotateMode(int);            | 设置日志转档模式，见前面                                     |
|   函数   | int SetRotateSize(int);            | 设置日志转档大小，单位：字节                                 |
|   函数   | int Debug(...);                    | 输出DEBUG等级的日志                                          |
|   函数   | int Info(...);                     | 输出INFO等级的日志                                           |
|   函数   | int Notice(...);                   | 输出NOTICE等级的日志                                         |
|   函数   | int Warn(...);                     | 输出WARN等级的日志                                           |
|   函数   | int Error(...);                    | 输出ERROR等级的日志                                          |
|   函数   | int Fatal(...);                    | 输出FATAL等级的日志                                          |

代码示例：`test_log_1_1.z`

该代码示范了设置日志文件名，设置输出行样式，设置日志过滤等级，然后输出不同等级的日志。中间设置了负的日志等级，达到过滤掉等级绝对值以外的日志。

```
import stdtypes log ;

function int main( array args )
{
        log.SetOutput( log.OUTPUT_FILE , "$HOME$/log/test_log_1_1.log" );
        log.SetStyles( log.STYLE_DATETIMEMS + log.STYLE_LOGLEVEL + log.STYLE_PID + log.STYLE_TID + log.STYLE_SOURCE + log.STYLE_FORMAT + log.STYLE_NEWLINE );

        log.SetLevel( log.LEVEL_WARN );

        log.Debug( "debug" );
        log.Info( "info" );
        log.Notice( "notice" );
        log.Warn( "warn" );
        log.Error( "error" );
        log.Fatal( "i[%d] str[%s]" , 123 , "hello" );

        log.SetLevel( -log.LEVEL_ERROR );

        log.Debug( "debug" );
        log.Info( "info" );
        log.Notice( "notice" );
        log.Warn( "warn" );
        log.Error( "error" );
        log.Fatal( "i[%d] str[%s]" , 456 , "world" );

        return 0;
}
```

执行

```
$ zlang test_log_1_1.z
$ cat $HOME/log/test_log_1_1.log
2024-03-20 20:53:20.738431 | WARN   | 17644:2293331776:test_log_1_1.z:13 | warn
2024-03-20 20:53:20.738713 | ERROR  | 17644:2293331776:test_log_1_1.z:14 | error
2024-03-20 20:53:20.738850 | FATAL  | 17644:2293331776:test_log_1_1.z:15 | i[123] str[hello]
2024-03-20 20:53:20.738987 | ERROR  | 17644:2293331776:test_log_1_1.z:23 | error
```

## 日志集

日志集是：应用调用一条输出日志语句，写出多个日志文件。

日志集对象：`logs`

| 成员类型 | 成员名字和原型      | 说明                                              | (错误.)(异常码,)异常消息                 |
| :------: | ------------------- | ------------------------------------------------- | ---------------------------------------- |
|   函数   | log AddLog(string); | 往`logs`对象里添加一个`log`对象；先加入再设置选项 | EXCEPTION_MESSAGE_ADD_LOG_TO_LOGS_FAILED |
|   函数   | int Info(...);      | 输出INFO等级的日志                                |                                          |
|   函数   | int Notice(...);    | 输出NOTICE等级的日志                              |                                          |
|   函数   | int Warn(...);      | 输出WARN等级的日志                                |                                          |
|   函数   | int Error(...);     | 输出ERROR等级的日志                               |                                          |
|   函数   | int Fatal(...);     | 输出FATAL等级的日志                               |                                          |

代码示例：`test_logs_1_1.z`

该代码示范了把两个日志加到日志集合中，用日志集合输出日志。

```
import stdtypes log ;

function int main( array args )
{
        g = logs.AddLog( "event" ) ;
        g.SetOutput( log.OUTPUT_FILE , "$HOME$/log/event.log" );
        g.SetStyles( log.STYLE_DATETIMEMS + log.STYLE_LOGLEVEL + log.STYLE_PID + log.STYLE_TID + log.STYLE_SOURCE + log.STYLE_FORMAT + log.STYLE_NEWLINE );
        g.SetLevel( -log.LEVEL_NOTICE );

        g2 = logs.AddLog( "process" ) ;
        g2.SetOutput( log.OUTPUT_FILE , "$HOME$/log/test_logs_1_1.log" );
        g2.SetStyles( log.STYLE_DATETIMEMS + log.STYLE_LOGLEVEL + log.STYLE_PID + log.STYLE_TID + log.STYLE_SOURCE + log.STYLE_FORMAT + log.STYLE_NEWLINE );
        g2.SetLevel( log.LEVEL_INFO );

        logs.Debug( "debug" );
        logs.Info( "info" );
        logs.Notice( "notice" );
        logs.Warn( "warn" );
        logs.Error( "error" );
        logs.Fatal( "fatal" );

        return 0;
}
```

执行

```
$ zlang test_logs_1_1.z
$ cat $HOME/log/event.log
2024-03-20 20:56:14.769221 | NOTICE | 17657:892065600:test_logs_1_1.z:17 | notice
$ cat $HOME/log/test_logs_1_1.log
2024-03-20 20:56:14.769108 | INFO   | 17657:892065600:test_logs_1_1.z:16 | info
2024-03-20 20:56:14.769228 | NOTICE | 17657:892065600:test_logs_1_1.z:17 | notice
2024-03-20 20:56:14.769252 | WARN   | 17657:892065600:test_logs_1_1.z:18 | warn
2024-03-20 20:56:14.769275 | ERROR  | 17657:892065600:test_logs_1_1.z:19 | error
2024-03-20 20:56:14.769298 | FATAL  | 17657:892065600:test_logs_1_1.z:20 | fatal
```



下一章：[数据库](database.md)