上一章：[声明变量](declare.md)



**章节目录**
- [运算符](#运算符)
  - [优先级0](#优先级0)
  - [优先级1](#优先级1)
  - [优先级2](#优先级2)
  - [优先级4](#优先级4)
  - [优先级5](#优先级5)
  - [优先级6](#优先级6)
  - [优先级7、8](#优先级78)
  - [优先级9、10、11](#优先级91011)
  - [优先级12、13](#优先级1213)
  - [优先级14](#优先级14)
  - [优先级15](#优先级15)



# 运算符

`zlang`支持的运算符和C/Java类似。

以下是运算符总览：

| 优先级（数字越小，运算优先级越高） |                             描述                             |                        运算符                         |
| :--------------------------------: | :----------------------------------------------------------: | :---------------------------------------------------: |
|                 0                  | 字面量、第一个标识、字符串对象值为名字的对象、this、子表达式开始、语句块开始 |    LITERALS、first IDENTIFICATION、```、this、(、{    |
|                 1                  | 右缀自操作、引用成员、用相同的变量名重新声明变量、第二个标识 |   var++、var--、.、?.、"new"、second IDENTIFICATION   |
|                 2                  |              置负、置逻辑反、置位反、左缀自操作              |                 -、!、~、++var、--var                 |
|                 4                  |                       乘法、除法、取余                       |                        *、/、%                        |
|                 5                  |                          加法、减法                          |                         +、-                          |
|                 6                  |                        位左移、位右移                        |                        <<、>>                         |
|                7、8                | 逻辑等于、逻辑不等于、逻辑小于、逻辑小于等于、逻辑大于、逻辑大于等于、引用逻辑等于、引用逻辑不等于、祖先相同、祖先不相同 |       ==、!=、<、<=、>、>=、==&、!=&、==:、!=:        |
|             9、10、11              |                      位与、位异或、位或                      |                     &、^、\|、、                      |
|               12、13               |                        逻辑与、逻辑或                        |                       &&、\|\|                        |
|                 14                 |                       表达式级判断分支                       |                       ?...:...                        |
|                 15                 | 赋值、引用、强制引用、自增赋值、自减赋值、自乘赋值、自除赋值、自余赋值、自位与赋值、自位异或赋值、自位或赋值、自位左移赋值、自位右移赋值 | =*、=、=&、+=、-=、\*=、/=、%=、&=、^=、\|=、<<=、>>= |

## 优先级0

|    运算符    | 说明                                                         | 示例                                                         |
| :----------: | ------------------------------------------------------------ | ------------------------------------------------------------ |
|    字面量    | 把字面量装箱成对象                                           | 1、2.3、45.67890、1.2、345.6789、"hello"、true、false        |
|  第一个标识  | 可能是克隆对象的源对象 或 调用函数所属对象 等                | int count ; random.Rand(1,10);                               |
|     ```      | 三个反单引号包裹一个字符串类型对象，查询该对象值为名字的对象 | o="stdout"; ```o```.Println("...");                          |
|     this     | 对象函数中强制使用自己的属性                                 | object my_object { string name ; function SetName(string name){ this.name = name ; } } |
| 子表达式开始 | ( ... )                                                      |                                                              |
|  语句块开始  | { ... }                                                      |                                                              |
|  控制关键字  | if、for、break、return、...                                  |                                                              |

代码示例：`test_string_1_1.z`

```
import stdtypes stdio random ;

function int main( array args )
{
        random.Srand();

        string  str = "I roll " + random.Rand(1,10).ToString() ;
        stdout.Println( str );

        return 0;
}
```

执行代码

```
$ zlang test_string_1_1.z
I roll 6
```

代码示例：`test_triple_backquotes_string_1.z`

```
import stdtypes stdio ;

function int main( array args )
{
        string str1 = "hello" ;
        string str2 = "str1" ;

        stdout.Println( `str2` );

        return 0;
}
```

执行

```
$ zlang test_triple_backquotes_string_1.z
hello
```

## 优先级1

|                    运算符                    | 说明                 | 示例                                                         |
| :------------------------------------------: | -------------------- | ------------------------------------------------------------ |
|                  右缀自操作                  | var++、var--         | i++; n--;                                                    |
|                   引用成员                   | .                    | http.HTTP_STATUSCODE_OK、http.Connect(192.168.11.79,9527);   |
| 如果对象值为null，则不引用成员、直接返回null | ?.                   | int user_id = uncatch http_cookies.Get( "user_id" )?.ToInt() ; |
|          用相同的变量名重新声明变量          | new                  | string buf = "AAA"; stdout.Println(buf); string new buf = "BBB"; stdout.Println(buf); |
|                  第二个标识                  | 声明变量（克隆对象） | int n;                                                       |

## 优先级2

|   运算符   | 说明  | 示例 |
| :--------: | ----- | ---- |
|    置负    |       | -n   |
|  置逻辑反  |       | !b   |
|   置位反   |       | ~b   |
| 左缀自操作 | ++var | ++i  |

代码示例：`test_PLUS_PLUS_int.z`

```
import stdtypes stdio ;

function int main( array args )
{
        i1 = 7 ;

        stdout.Println( ++i1 );

        stdout.Println( ++i1 );

        return 0;
}
```

```
$ zlang test_PLUS_PLUS_int.z
8
9
```

代码示例：`test_new_2_1.z`

```
import stdtypes stdio ;

function int main( array args )
{
        for( int new n = 0 ; n < 3 ; n++ )
                stdout.Println( n );

        for( int new n = 6 ; n < 9 ; n++ )
                stdout.Println( n );

        return 0;
}
```

执行代码

```
$ zlang test_new_2_1.z
0
1
2
6
7
8
```

## 优先级4

| 运算符 | 说明 | 示例  | (错误.)(异常码,)异常消息                 |
| :----: | ---- | ----- | ---------------------------------------- |
|  乘法  |      | a * b |                                          |
|  除法  |      | a / b | fatal.EXCEPTION_MESSAGE_DIVISION_BY_ZERO |
|  取余  |      | a % b | fatal.EXCEPTION_MESSAGE_DIVISION_BY_ZERO |

注意：数值做四则运算时，如果两边类型不一致，会自动提升类型较低的一方再运算，比如`int * double`会首先把`int`提升至`double`再相乘。

## 优先级5

| 运算符 | 说明 | 示例  |
| :----: | ---- | ----- |
|  加法  |      | a + b |
|  减法  |      | a - b |

注意：数值做四则运算时，如果两边类型不一致，会自动提升类型较低的一方再运算。

代码示例：`test_int_FOUR_MIXED_OPERATIONS_4.z`

```
import stdtypes stdio ;

function int main( array args )
{
        int i = ( (1-2)*(3+4)+1 ) / (-5-(-6)) ;
        stdout.Println( i );

        return 0;
}
```

执行代码

```
$ zlang test_int_FOUR_MIXED_OPERATIONS_4.z
-6
```

## 优先级6

| 运算符 | 说明 | 示例   |
| :----: | ---- | ------ |
| 位左移 |      | n << 4 |
| 位右移 |      | n >> 4 |

## 优先级7、8

|     运算符     | 说明 | 示例          |
| :------------: | ---- | ------------- |
|    逻辑等于    |      | n == 1        |
|   逻辑不等于   |      | n != 1        |
|    逻辑小于    |      | n < 1         |
|  逻辑小于等于  |      | n <= 1        |
|    逻辑大于    |      | n > 1         |
|  逻辑大于等于  |      | n >= 1        |
|  引用逻辑等于  |      | obj1 ==& obj2 |
| 引用逻辑不等于 |      | obj2 !=& obj2 |
|    祖先相同    |      | obj1 ==: obj2 |
|   祖先不相同   |      | obj1 !=: obj2 |

代码示例：`test_ancestor_cmp_1.z`

```
import stdtypes stdio ;

object test
{
        string  name ;
}

object demo extends test
{
        string  id ;
}

function int main( array args )
{
        i1 =* 1 ;
        i2 =* i1 ;
        i3 =* 2 ;
        if( i1 == i2 )
                stdout.Println( "i1 == i2" );
        else
                stdout.Println( "i1 != i2" );
        if( i1 == i3 )
                stdout.Println( "i1 == i3" );
        else
                stdout.Println( "i1 != i3" );

        i1 = 1 ;
        i2 = i1 ;
        i3 = 1 ;
        if( i1 ==& i2 )
                stdout.Println( "i1 ==& i2" );
        else
                stdout.Println( "i1 !=& i2" );
        if( i1 ==& i3 )
                stdout.Println( "i1 ==& i3" );
        else
                stdout.Println( "i1 !=& i3" );

        test    t1 ;
        t1      t2 ;
        demo    d1 ;
        if( test ==: t1 )
                stdout.Println( "test ==: t1" );
        else
                stdout.Println( "test !=: t1" );
        if( test ==: d1 )
                stdout.Println( "test ==: d1" );
        else
                stdout.Println( "test !=: d1" );

        return 0;
}
```

执行

```
$ zlang test_ancestor_cmp_1.z
i1 == i2
i1 != i3
i1 ==& i2
i1 !=& i3
test ==: t1
test !=: d1
```

## 优先级9、10、11

| 运算符 | 说明 | 示例   |
| :----: | ---- | ------ |
|  位与  |      | a & b  |
| 位异或 |      | a ^ b  |
|  位或  |      | a \| b |

## 优先级12、13

| 运算符 | 说明 | 示例       |
| :----: | ---- | ---------- |
| 逻辑与 |      | b1 && b2   |
| 逻辑或 |      | b1 \|\| b2 |

## 优先级14

|      运算符      | 说明 | 示例                       |
| :--------------: | ---- | -------------------------- |
| 表达式级判断分支 |      | b==true ? "true" : "false" |

## 优先级15

|                运算符                | 说明 | 示例                  | (错误.)(异常码,)异常消息                 |
| :----------------------------------: | ---- | --------------------- | ---------------------------------------- |
|                 赋值                 |      | string str =* "hello" |                                          |
|                 引用                 |      | string str = "hello"  |                                          |
| 强制引用（先解引用老的，再引用新的） |      | string str =& 123 ;   |                                          |
|               自增赋值               |      | n += 3                |                                          |
|               自减赋值               |      | n -= 3                |                                          |
|               自乘赋值               |      | n *= 3                |                                          |
|               自除赋值               |      | n /= 3                | fatal.EXCEPTION_MESSAGE_DIVISION_BY_ZERO |
|               自余赋值               |      | n %= 3                | fatal.EXCEPTION_MESSAGE_DIVISION_BY_ZERO |
|              自位与赋值              |      | n&= 0x0F              |                                          |
|             自位异或赋值             |      | n ^= 0x30             |                                          |
|              自位或赋值              |      | n \|= 0x01            |                                          |
|             自位左移赋值             |      | n <<= 4               |                                          |
|             自位右移赋值             |      | n >>= 4               |                                          |

注意：赋值时，如果两边类型不一致，会先尝试自动转换右值类型为左值类型，再进行赋值。



下一章：[关键字](keyword.md)
