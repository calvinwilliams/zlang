上一章：[系统环境](system.md)



**章节目录**
- [文件系统路径和文件](#文件系统路径和文件)
  - [路径](#路径)
  - [文件](#文件)



# 文件系统路径和文件

文件系统路径和文件对象库用于有效的管理路径和文件。

## 路径

使用路径对象可以获得或指定当前目录、切换到上级/下级目录、读取目录中的所有文件。

路径对象：`path`

| 成员类型 | 成员名字和原型                | 说明                                                 | (错误.)(异常码,)异常消息                                     |
| -------- | ----------------------------- | ---------------------------------------------------- | ------------------------------------------------------------ |
| 函数     | void GetCurrentPath();        | 得到当前路径，构建path对象                           |                                                              |
| 函数     | path Path(string);            | 指定路径，构建path对象                               | EXCEPTION_MESSAGE_GET_DIRECTORY_OR_FILE_TYPE_FAILED<br />EXCEPTION_MESSAGE_PATH_NAME_TOO_LONG |
| 函数     | bool ChangeParentPath();      | 切换到上级目录                                       | EXCEPTION_MESSAGE_NO_PARENT_PATH<br />EXCEPTION_MESSAGE_NO_PARENT_PATH |
| 函数     | bool ChangeChildPath(string); | 切换到指定下级目录                                   | EXCEPTION_MESSAGE_NO_CHILD_PATH<br />EXCEPTION_MESSAGE_PATH_IS_NOT_A_DIRECTORY |
| 函数     | string GetPathname();         | 得到当前目录名                                       |                                                              |
| 函数     | array ReadFiles();            | 列出当前路径下的所有目录和文件，返回文件（file）数组 | EXCEPTION_MESSAGE_PATH_INVALID<br />EXCEPTION_MESSAGE_FILE_NAME_TOO_LONG<br />EXCEPTION_MESSAGE_APPEND_FILE_ARRAY_FAILED |

代码示例：`test_path_1_1.z`

该代码示范了得到当前路径和当前目录名。

```
import stdtypes stdio file ;

function int main( array args )
{
        path    a ;

        a.GetCurrentPath() ;
        stdout.Println( "Current path is " + a.GetPathname() );

        return 0;
}
```

执行

```
$ zlang test_path_1_1.z
Current path is /home/calvin/src/zlang/test
```

代码示例：`test_path_2_1.z`

该代码示范了设置某一个路径到`path`对象中并得到最右边的目录名，然后一级一级往上切换目录出来并显示目录名。

```
import stdtypes stdio file ;

function int main( array args )
{
        path    a ;
        bool    b ;

        a.Path("/var/log") ;
        stdout.Println( "Set path is " + a.GetPathname() );

        for( ; ; )
        {
                b = a.ChangeParentPath() ;
                if( b != true )
                        break;

                stdout.Println( "Change parent path " + a.GetPathname() );
        }

        return 0;
}
```

执行

```
$ zlang test_path_2_1.z
Set path is /var/log
Change parent path /var
```

代码示例：`test_path_4_1.z`

该代码示范了给定一个路径，查询路径下的文件名清单。

注意：通过文件对象内含函数可以得到对应路径名、文件名、类型（如目录）。

```
import stdtypes stdio file ;

function int main( array args )
{
        path    a ;
        array   files ;
        int     i , count ;

        a.GetCurrentPath() ;
        stdout.Println( "Current path is " + a.GetPathname() );

        a.ChangeParentPath();
        a.ChangeChildPath("src");
        stdout.Println( "Change path is " + a.GetPathname() );

        files = a.ReadFiles() ;
        if( files == null )
        {
                stdout.Println( "a.ReadFiles failed" );
                return 1;
        }

        count = files.Length() ;
        stdout.FormatPrintln( "file count : %d" , count );
        for( i = 1 ; i <= count ; i++ )
        {
                stdout.Println( "file : " + files.Get(i).GetFilename() );
        }

        return 0;
}
```

执行

```
$ zlang test_path_4_1.z
Current path is /home/calvin/src/zlang/test
Change path is /home/calvin/src/zlang/src
file count : 4
type : 2 file : makefile
type : 1 file : zlang
type : 1 file : zobjects
type : 2 file : makefile.Linux
```

## 文件

文件对象可以获得路径名、文件名、文件类型。

文件对象：`file`

| 成员类型 | 成员名字和原型        | 说明             | (错误.)(异常码,)异常消息                            |
| -------- | --------------------- | ---------------- | --------------------------------------------------- |
| 常量     | DIRECTORY_TYPE        | 目录类型         |                                                     |
| 常量     | REGULAR_TYPE          | 普通文件类型     |                                                     |
| 常量     | CHARACTER_TYPE        | 字符设备类型     |                                                     |
| 常量     | BLOCK_TYPE            | 块设备类型       |                                                     |
| 常量     | FIFO_TYPE             | 管道类型         |                                                     |
| 常量     | SOCKET_TYPE           | 套接字类型       |                                                     |
| 常量     | LINK_TYPE             | 链接类型         |                                                     |
| 常量     | OTHER_TYPE            | 其它类型         |                                                     |
| 函数     | string GetPathname(); | 得到文件的路径名 |                                                     |
| 函数     | string GetFilename(); | 得到文件的文件名 |                                                     |
| 函数     | int GetFiletype();    | 得到文件的类型   | EXCEPTION_MESSAGE_GET_DIRECTORY_OR_FILE_TYPE_FAILED |



下一章：[线程和同步](thread.md)