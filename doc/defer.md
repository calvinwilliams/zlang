上一章：[函数](function.md)



**章节目录**
- [延后执行](#延后执行)



# 延后执行

`zlang`支持语句块中或函数体中的一条语句延后到离开语句块或函数体时执行，在需要延后执行的语句前加上关键字`defer`即可。一般用于释放资源。

代码示例：`test_defer_1.z`

该代码示范了打开文件成功后，标记结束`main`函数时自动执行关闭文件操作。

```
import stdtypes stdio ;

function int main( array args )
{
	stdfile		f ;
	
	nret = f.Open( "test_defer_1.out" , "w" ) ;
	if( nret == -1 )
	{
		stdout.Println( "open file failed[%d]" , nret );
		return 1;
	}
	defer f.Close();
	
	f.FormatPrintln( "hello" );
	
	return 0;
}
```

代码示例：`test_defer_2.z`

```
import stdtypes stdio ;

function int main( array args )
{
	stdfile		f ;
	
	nret = f.Open( "test_defer_2.out" , "w" ) ;
	if( nret == -1 )
	{
		stdout.Println( "open file failed[%d]" , nret );
		return 1;
	}
	defer f.Close();
	
	for( int n = 1 ; n <= 3 ; n++ )
	{
		stdfile		f ;
		
		nret = f.Open( "test_defer_2_"+n.ToString()+".out" , "w" ) ;
		if( nret == -1 )
		{
			stdout.Println( "open file failed[%d]" , nret );
			return 1;
		}
		defer f.Close();
		
		f.FormatPrintln( "hello_2_"+n.ToString() );
	}
	
	f.FormatPrintln( "hello_2" );
	
	return 0;
}
```



下一章：[面向对象](object.md)