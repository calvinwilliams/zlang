上一章：[第二部分：基础对象库](zobjects.md)



**章节目录**
- [数学计算](#数学计算)



# 数学计算

若导入zlang数学库，将在基本值对象和集合对象库（stdtypes）的数字对象里附带一批数学函数，方便直接处理，具体见[字面量、值对象和集合对象](stdtypes.md)的“数值对象”章节。

数学库还定义了一些数学常量，方便直接使用。

注意：基本值对象和集合对象库（stdtypes）先`import`，数学库（math）后`import`。

数学对象：`math`

| 成员类型 | 成员名字和原型 | 说明     |
| -------- | -------------- | -------- |
| 常量     | double E       | 自然数E  |
| 常量     | double PI      | 圆周率PI |

代码示例：`test_math_E_PI_1.z`

该代码示范了直接使用数学常量E和PI。

```
import stdtypes stdio math ;

function int main( array args )
{
        stdout.FormatPrintln( " E[%.10lf]" , math.E );
        stdout.FormatPrintln( "PI[%.10lf]" , math.PI );

        return 0;
}
```

执行

```
$ zlang test_math_E_PI_1.z
 E[2.7182818285]
PI[3.1415926536]
```



下一章：[大数运算](bignum.md)