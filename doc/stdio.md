上一章：[大数运算](bignum.md)



**章节目录**
- [标准输入输出](#标准输入输出)
  - [标准输入](#标准输入)
  - [标准输出](#标准输出)
  - [标准错误输出](#标准错误输出)
  - [标准文件](#标准文件)
  - [执行外部命令](#执行外部命令)



# 标准输入输出

标准输入输出用于从输入设备（键盘）读取数据、写出数据到输出设备（屏幕终端）。

zlang标准输入输出是对操作系统标准输入输出的直接封装。

## 标准输入

标准输入对象：`stdin`

| 成员类型 | 成员名字和原型      | 说明                                                         |
| -------- | ------------------- | ------------------------------------------------------------ |
| 函数     | int Scan(string);   | 从输入设备读取一段白字符分隔的字符串，返回读取的字节数，下同 |
| 函数     | int Scan(short);    | 从输入设备读取一个白字符分隔的短整型数                       |
| 函数     | int Scan(ushort);   | 从输入设备读取一个白字符分隔的无符号短整型数                 |
| 函数     | int Scan(int);      | 从输入设备读取一个白字符分隔的整型数                         |
| 函数     | int Scan(uint);     | 从输入设备读取一个白字符分隔的无符号整型数                   |
| 函数     | int Scan(long);     | 从输入设备读取一个白字符分隔的长整型数                       |
| 函数     | int Scan(ulong);    | 从输入设备读取一个白字符分隔的无符号长整型数                 |
| 函数     | int Scan(float);    | 从输入设备读取一个白字符分隔的单精度浮点数                   |
| 函数     | int Scan(double);   | 从输入设备读取一个白字符分隔的单精度浮点数                   |
| 函数     | int Scanln(string); | 从输入设备读取一行字符串                                     |

代码示例：`test_stdin_1.z`

该代码示范了从标准输入（键盘）中读取数据到单个基本类型数值对象，并显示出来。

```
import stdtypes stdio ;

function int main( array args )
{
        string  line ;
        string  str ;
        short   s ;
        int     i ;
        long    l ;
        float   f ;
        double  d ;

        stdin.Scanln( line );
        stdout.Println( line );

        stdin.Scan( str );
        stdout.Println( str );

        stdin.Scan( s );
        stdout.Println( s );

        stdin.Scan( i );
        stdout.Println( i );

        stdin.Scan( l );
        stdout.Println( l );

        stdin.Scan( f );
        stdout.Println( f );

        stdin.Scan( d );
        stdout.Println( d );

        return 0;
}
```

测试文件：`test_stdin_1.stdin`

```
I am Calvin
hello 1 12345 123456789 12.34 1234.5678
```

执行

```
$ zlang test_stdin_1.z <test_stdin_1.stdin
I am Calvin
hello
1
12345
123456789
12.340000
1234.567800
```

## 标准输出

标准输出对象：`stdout`

| 成员类型 | 成员名字和原型          | 说明                                                         |
| -------- | ----------------------- | ------------------------------------------------------------ |
| 函数     | int Print(string);      | 写出一个字符串到输出设备，返回写出的字节数，下同             |
| 函数     | int Println(string);    | 写出一个字符串并追加一个换行到输出设备                       |
| 函数     | int Print(bool);        | 写出一个布尔型值（"true"\|"false"）到输出设备                |
| 函数     | int Println(bool);      | 写出一个布尔型值（"true"\|"false"）并并追加一个换行到输出设备 |
| 函数     | int Print(short);       | 写出一个短整型数到输出设备                                   |
| 函数     | int Println(short);     | 写出一个短整型数并追加一个换行到输出设备                     |
| 函数     | int Print(ushort);      | 写出一个无符号短整型数到输出设备                             |
| 函数     | int Println(ushort);    | 写出一个无符号短整型数并追加一个换行到输出设备               |
| 函数     | int Print(int);         | 写出一个整型数到输出设备                                     |
| 函数     | int Println(int);       | 写出一个整型数并追加一个换行到输出设备                       |
| 函数     | int Print(uint);        | 写出一个无符号整型数到输出设备                               |
| 函数     | int Println(uint);      | 写出一个无符号整型数并追加一个换行到输出设备                 |
| 函数     | int Print(long);        | 写出一个长整型数到输出设备                                   |
| 函数     | int Println(long);      | 写出一个长整型数并追加一个换行到输出设备                     |
| 函数     | int Print(ulong);       | 写出一个无符号长整型数到输出设备                             |
| 函数     | int Println(ulong);     | 写出一个无符号长整型数并追加一个换行到输出设备               |
| 函数     | int Print(float);       | 写出一个单精度浮点数到输出设备                               |
| 函数     | int Println(float);     | 写出一个单精度浮点数并追加一个换行到输出设备                 |
| 函数     | int Print(double);      | 写出一个双精度浮点数到输出设备                               |
| 函数     | int Println(double);    | 写出一个双精度浮点数并追加一个换行到输出设备                 |
| 函数     | int Print((null));      | 写出一个空指针字面量到输出设备                               |
| 函数     | int Println((null));    | 写出一个空指针字面量并追加一个换行到输出设备                 |
| 函数     | int Write(string);      | 写出一个字符串（按实际长度而不是'\0结束'）到输出设备         |
| 函数     | int FormatPrint(...);   | 格式化写出一个格式化占位符串和一组值对象到输出设备，如`FormatPrint("%d",n);`、`FormatPrint("say %s to %s",hello,name);`；可用'?'代替类型，如"%?"；支持把参数名写在格式化串中，如"%{num:02d}"； |
| 函数     | int FormatPrintln(...); | 格式化写出一个格式化占位符串和一组值对象并追加一个换行到输出设备 |

代码示例：`test_stdout_1.z`

该代码示范了格式化输出显示多个基本类型数值对象。

```
import stdtypes stdio ;

function int main( array args )
{
        short           s = 1 ;
        int             i = 234 ;
        long            l = 567890 ;
        float           f = 1.2 ;
        double          d = 3456.7890 ;
        string          str = "A234567890123456789012345678901234567890123456789B" ;
        bool            b1 = true ;
        bool            b2 = false ;

        stdout.FormatPrintln( "s[%2hd] i[%-4d] l[%ld] f[%4f] d[%10.2lf] str[%s] b1[%6b] b2[%-6b] " , s , i , l , f , d , str , b1 , b2 );

        return 0;
}
```

执行

```
$ zlang test_stdout_1.z
s[ 1] i[234 ] l[567890] f[1.200000] d[   3456.79] str[A234567890123456789012345678901234567890123456789B] b1[  true] b2[false ]
```

代码示例：`test_string_format_2_2.z`

该代码示范了直接在格式化串中给定变量名，而不用在格式化串后一个又一个对应加参数。

```
import stdtypes stdio ;

function int main( array args )
{
	string		buf ;
	
	short		s ;
	int		i ;
	long		l ;
	float		f ;
	double		d ;
	string		str ;
	bool		b1 ;
	bool		b2 ;
	
	s = 1 ;
	i = 234 ;
	l = 567890 ;
	f = 1.2 ;
	d = 3456.7890 ;
	str = "A234567890123456789012345678901234567890123456789B" ;
	b1 = true ;
	b2 = false ;
	buf.Format( "s[%{s:2hd}] i[%{i:-4d}] l[%{l:ld}] f[%{f:4f}] d[%{d:10.2lf}]" );
	buf.AppendFormat( " str[%{str:s}] b1[%{b1:6b}] b2[%{b2:-6b}] " );
	
	stdout.Println( buf );
	
	return 0;
}
```

代码示例：`test_string_format_3_1.z`

该代码示范了使用`?`自动推导参数类型。

```
import stdtypes stdio ;

function int main( array args )
{
	string		buf ;
	
	short		s ;
	int		i ;
	long		l ;
	float		f ;
	double		d ;
	string		str ;
	bool		b1 ;
	bool		b2 ;
	
	s = 1 ;
	i = 234 ;
	l = 567890 ;
	f = 1.2 ;
	d = 345.678 ;
	str = "A234567890123456789012345678901234567890123456789B" ;
	b1 = true ;
	b2 = false ;
	buf.Format( "s[%?] i[%?] l[%?] f[%?] d[%?]" , s , i , l , f , d );
	buf.AppendFormat( " str[%?] b1[%?] b2[%?] " , str , b1 , b2 );
	
	stdout.Println( buf );
	
	return 0;
}
```

## 标准错误输出

标准错误输出对象：`stderr`

成员说明同标准输出。

## 标准文件

标准文件对象：`stdfile`

| 成员类型 | 成员名字和原型                                               | 说明                                                         | (错误.)(异常码,)异常消息                                     |
| -------- | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| 函数     | int Open(string,string);                                     | 用指定模式打开文件，如`Open("file.txt","w")`、`Open("BinFile.bin","rb")`，返回打开结果 | EXCEPTION_MESSAGE_OPEN_FILE_FAILED                           |
| 函数     | int Close();                                                 | 关闭文件，返回关闭结果                                       |                                                              |
| 函数     | （同标准输入和标准输出）                                     |                                                              |                                                              |
| 函数     | int Write(short\|ushort\|int\|uint\|long\|ulong\|float\|double); | 写出一个数值（以内存表示）到文件，如`Write(int)`zlang的int占用4字节，写出这4个字节 |                                                              |
| 函数     | int Write(short\|ushort\|int\|uint\|long\|ulong\|float\|double); | 从文件读取一个数值（以内存表示）                             |                                                              |
| 函数     | int Write(bool);                                             | 写出一个布尔型值（"true"\|"false"）到文件                    |                                                              |
| 函数     | int Read(bool);                                              | 从文件读取一个布尔型值（"true"\|“false"）                    |                                                              |
| 函数     | string ReadFileToString(string);                             | 读取整个文件到字符串对象中                                   | EXCEPTION_MESSAGE_STAT_FILE_FAILED<br />EXCEPTION_MESSAGE_OPEN_FILE_FAILED |
| 函数     | string WriteStringToFile(string,string);                     | (覆盖)写字符串对象到文件中                                   | EXCEPTION_MESSAGE_OPEN_FILE_FAILED<br />EXCEPTION_MESSAGE_WRITE_FILE_FAILED |

代码示例：`test_stdfile_1.z`

```
import stdtypes stdio ;

function int main( array args )
{
        stdfile file ;
        stdfile file2 ;
        string  line ;
        string  str ;
        short   s ;
        int     i ;
        long    l ;
        float   f ;
        double  d ;

        file.Open( "test_stdfile_1.in" , "r" );
        file2.Open( "test_stdfile_1.out" , "w" );

        file.Scanln( line );
        file2.Println( line );

        file.Scan( str );
        file2.Print( str );
        file2.Print( " " );

        file.Scan( s );
        file2.Print( s );
        file2.Print( " " );

        file.Scan( i );
        file2.Print( i );
        file2.Print( " " );

        file.Scan( l );
        file2.Print( l );
        file2.Print( " " );

        file.Scan( f );
        file2.Print( f );
        file2.Print( " " );

        file.Scan( d );
        file2.Print( d );
        file2.Print( " " );

        file.Close();
        file2.Close();

        return 0;
}
```

测试文件：`test_stdfile_1.in`

```
I am Calvin
hello 1 12345 123456789 12.34 1234.5678
```

执行

```
$ zlang test_stdfile_1.z
$ cat test_stdfile_1.out
I am Calvin
hello 1 12345 123456789 12.340000 1234.567800
```

代码示例：`test_stdfile_2.z`

```
import stdtypes stdio ;

function int main( array args )
{
        short           s = 1 ;
        int             i = 234 ;
        long            l = 567890 ;
        float           f = 1.2 ;
        double          d = 3456.7890 ;
        string          str = "A234567890123456789012345678901234567890123456789B" ;
        bool            b1 = true ;
        bool            b2 = false ;

        stdfile         file ;

        file.Open( "test_stdfile_2.out" , "w" );
        file.FormatPrintln( "s[%2hd] i[%-4d] l[%ld] f[%4f] d[%10.2lf] str[%s] b1[%6b] b2[%-6b] " , s , i , l , f , d , str , b1 , b2 );
        file.Close();

        return 0;
}
```

执行

```
$ zlang test_stdfile_2.z
$ cat test_stdfile_2.out
s[ 1] i[234 ] l[567890] f[1.200000] d[   3456.79] str[A234567890123456789012345678901234567890123456789B] b1[  true] b2[false ]
```

代码示例：`test_stdfile_3.z`

该代码示范了打开文件，单个写基本类型数值对象到文件，关闭文件。

```
import stdtypes stdio ;

function int main( array args )
{
        short           s = 1 ;
        int             i = 2 ;
        long            l = 3 ;
        float           f = 1.0 ;
        double          d = 2.0 ;
        bool            b1 = true ;
        bool            b2 = false ;

        stdfile         file ;

        file.Open( "test_stdfile_3.out" , "wb" );
        file.Write( s );
        file.Write( i );
        file.Write( l );
        file.Write( f );
        file.Write( d );
        file.Write( b1 );
        file.Write( b2 );
        file.Close();

        file.Open( "test_stdfile_3.out" , "rb" );
        file.Read( s );
        file.Read( i );
        file.Read( l );
        file.Read( f );
        file.Read( d );
        file.Read( b1 );
        file.Read( b2 );
        file.Close();

        stdout.Println( s );
        stdout.Println( i );
        stdout.Println( l );
        stdout.Println( f );
        stdout.Println( d );
        stdout.Println( b1 );
        stdout.Println( b2 );

        return 0;
}
```

执行

```
$ zlang test_stdfile_3.z
1
2
3
1.000000
2.000000
true
false
```

## 执行外部命令

执行外部命令对象：`execmd`

| 成员类型 | 成员名字和原型           | 说明                                            | (错误.)(异常码,)异常消息             |
| -------- | ------------------------ | ----------------------------------------------- | ------------------------------------ |
| 函数     | int Execute(string);     | 执行外部命令，如`Execute("lsof")`，返回启动结果 | EXCEPTION_MESSAGE_DPOPEN_FAILED      |
|          | int CloseWrite();        | 执行时，当不需要交互写时，关闭写                | EXCEPTION_MESSAGE_DPHALFCLOSE_FAILED |
| 函数     | int Close();             | 执行时，关闭执行交互，清理执行资源              | EXCEPTION_MESSAGE_DPCLOSE_FAILED     |
| 函数     | （同标准输入和标准输出） |                                                 |                                      |

代码示例：`test_execmd_1.z`

该代码示范了执行`sort`创建命令管道，输入几个单词，最后读入`sort`排序结果并显示出来。

```
import stdtypes stdio ;

function int main( array args )
{
        execmd          ec ;
        string          output ;

        nret = ec.Execute( "sort" ) ;
        if( nret )
        {
                stdout.Println( nret );
                return 1;
        }

        ec.Println( "pear" );
        ec.Println( "orange" );
        ec.Println( "apple" );
        ec.CloseWrite();

        for( ; ; )
        {
                nret = ec.Scanln( output ) ;
                if( nret == -1 )
                        break;

                stdout.Println( output );
        }

        ec.Close();

        return 0;
}
```

执行

```
$ zlang test_execmd_1.z
apple
orange
pear
```



下一章：[系统环境](system.md)