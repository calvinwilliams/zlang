上一章：[字面量、值对象和集合对象](stdtypes.md)



**章节目录**
- [声明变量](#声明变量)



# 声明变量

`zlang`声明变量的语句形式为：

```
类型对象 变量名;
```

```
类型对象 变量名 = [ 字面量 | 另一个存在的变量名 | 表达式 ] ;
```

用"类型对象"克隆出一个新对象，名字为”变量名“，新对象将继承"类型对象"的所有属性和函数。

注意：可用关键字`new`用已声明的名字重新声明变量，主要用在代码块复用时简化编写（不用检查前面是否已声明相同名字的变量，反正中间声明好了），语句形式为：

```
类型对象 new 变量名;
```

代码示例：`test_declare_and_then_and_eval.z`

该代码示范了声明字符串类型的变量，或并赋值，或一条语句声明多个变量。

```
import stdtypes stdio ;

function int main( array args )
{
        string  str1 ;
        string  str2 = "hello2" ;
        string  str3 = "hello3" , str4 ;
        string  str5 , str6 = "hello6" ;

        str1 = "hello1" ;
        str4 = "hello4" ;
        str5 = "hello5" ;

        stdout.Println( str1 );
        stdout.Println( str2 + str3 );
        stdout.Println( "A" + str4 + "B" + str5 + "C" + str6 + "D" );

        return 0;
}
```

执行

```
$ zlang test_declare_and_then_and_eval.z
hello1
hello2hello3
Ahello4Bhello5Chello6D
```

注意：当右边的字面量或值对象的类型确定时，可省略"类型对象"，`zlang`会自动推导出类型对象，如`int n = 0 ;`可简写成`n = 0 ;`。



下一章：[运算符](operator.md)