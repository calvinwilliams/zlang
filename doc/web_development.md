上一章：[应用领域](domain_development.md)



**章节目录**

- [Web开发](#web开发)
	- [工程目录结构](#工程目录结构)
	- [启动一个Web服务器](#启动一个web服务器)
	- [Web请求处理函数](#web请求处理函数)
	- [读取请求数据](#读取请求数据)
		- [读取Web请求的URI(不包含参数)](#读取web请求的uri不包含参数)
		- [读取Web请求的URI中的路径](#读取web请求的uri中的路径)
		- [读取Web请求的URI路径中的某一段](#读取web请求的uri路径中的某一段)
		- [读取Web请求的URI参数数据](#读取web请求的uri参数数据)
		- [读取Web请求Cookies](#读取web请求cookies)
		- [读取Web请求的HTTP体数据](#读取web请求的http体数据)
		- [读取Web请求的表单POST数据](#读取web请求的表单post数据)
	- [写出响应数据](#写出响应数据)
		- [写出一个Web响应的HTTP头数据](#写出一个web响应的http头数据)
		- [写出一个Web响应的HTTP头Cookie](#写出一个web响应的http头cookie)
		- [一次性写出Web响应的HTTP体数据](#一次性写出web响应的http体数据)
		- [一次性格式化Web响应的HTTP体数据](#一次性格式化web响应的http体数据)
		- [分多次写出/格式化Web响应的HTTP体数据](#分多次写出格式化web响应的http体数据)
		- [装载一个外部文件作为Web响应的HTTP体数据（没啥用，一般静态网页文件都由`httpserver`以不匹配动态页面文件扩展名为由直接读磁盘了）](#装载一个外部文件作为web响应的http体数据没啥用一般静态网页文件都由httpserver以不匹配动态页面文件扩展名为由直接读磁盘了)
		- [装载一个外部模板文件渲染后作为Web响应的HTTP体数据](#装载一个外部模板文件渲染后作为web响应的http体数据)
		- [一个完整网页模板渲染的示例](#一个完整网页模板渲染的示例)
	- [官方自带的一个Web框架`zwebtemplate`](#官方自带的一个web框架zwebtemplate)
		- [关于zwebtemplate](#关于zwebtemplate)
		- [开发者可以通过这个开源项目](#开发者可以通过这个开源项目)
		- [项目目录说明](#项目目录说明)
		- [安装说明](#安装说明)
		- [源码解读](#源码解读)
			- [启动函数`main.z`](#启动函数mainz)
			- [常用全局对象`global.z`](#常用全局对象globalz)
			- [工具对象库`util.z`](#工具对象库utilz)
			- [常用拦截器库`intercept.z`](#常用拦截器库interceptz)
			- [页码控制器对象`page_control.z`](#页码控制器对象page_controlz)
			- [一个初具规模的用户模块`user.z`](#一个初具规模的用户模块userz)
			- [一个书籍管理模块`book.z`](#一个书籍管理模块bookz)



# Web开发

`zlang`具备完整、快速的Web应用开发能力。

`zlang`自带`httpserver`的一个特色是，无需注解，直接把HTTP方法和URI作为函数名，简单就是最好的，直观，一眼就看出这个函数是处理哪个URI请求，能少敲代码就少敲代码，比如：

```
function int `POST /login_or_fast_registe.do`( httpserver s )
{
   ...
}
```

## 工程目录结构

一种通用的工程目录结构如下：

```
(git仓库顶层目录)/
	www/
		index.html,*.html
		*.tpl.html
		images/
			*.jpg
		js/
			*.js
	src/
		util.z（一些本项目的工具函数和对象）
		global.z（一些全局对象和常量）
		intercept.z（一些拦截器，比如Web请求拦截器输出事件日志文件，比如数据库SQL请求拦截器输出事件日志文件）
		main.z（初始化日志文件环境，连接数据库，启动httpserver）
		(模块名.z，比如user.z),...
```

## 启动一个Web服务器

`zlang`自带web服务器`httpserver`，能做静态网页管理用，也能做Web网站用，更能做Restful应用。

```
function int main( array args )
{
        httpserver.SetListenAddress( "" , 9527 ); /* 第一个参数是IP，通配则填""；第二个参数是Web服务器端口号 */
        httpserver.SetDomain( "*" ); /* 网站域名，匹配HTTP请求中的'Host'，没有则填通配所有"*" */
        httpserver.SetWwwroot( "$HOME$/www" ); /* 网站根目录，装载外部文件或HTML模板都存放在其下 */
        httpserver.SetLogFile( "log/access.log" , "log/error.log" ); /* 网站访问事件日志和错误调试日志，根据不同日志等级过滤日志，日志等级由zlang命令行参数传递进来，默认ERROR */
        r = httpserver.Run() ; /* 运行起来 */
        if( r )
        {
                stdout.FormatPrintln( "httpserver.Run err[%d][%s]" , r , httpserver.GetError() );
                return 1;
        }
        else
        {
                stdout.Println( "httpserver.Run normal ended" );
        }

        return 0;
}
```

## Web请求处理函数

在别的语言里需要额外通过配置或注解提供URI和处理句柄映射关系信息，在`zlang`完全不必，`zlang`拥有反单引号定义包含白字符或标点符号的函数名的能力，可以在函数名中直接提供该映射关系，直接、简单明了，省却了额外定义函数名的烦恼，减少代码冗余和开发工作量。比如：

```
function int `POST /login_or_fast_registe.do`( httpserver s )
{
	...
	return http.HTTP_STATUSCODE_OK;
}
```

`POST /login_or_fast_registe.do`本身就是函数名，浏览器端`post`过来的Web请求（URI为`/login_or_fast_registe.do`）会自动交给该函数处理。

注意：所有Web请求处理函数的原型是固定的，即除了函数名外，其它不能变。函数原型为：

`function int (单反引号)(HTTP方法) (HTTP URI)(单反引号)( httpserver s )`

小知识：`httpserver`对象在导入时，会自动扫描所有全局函数（今后还将支持对象内函数），只要符合Web请求处理函数的原型的函数都会被标记。当一个Web请求进来时，在标记库里匹配是否存在相应的函数，如果有则调用该函数。

## 读取请求数据

### 读取Web请求的URI(不包含参数)

以下代码读取Web请求的不包含参数的URI

```
	string  uri = s.GetHttpRequestUri() ;
```

### 读取Web请求的URI中的路径

 以下代码读取Web请求的URI中的路径

```
	string	uri_path = s.GetHttpRequestUriPath() ;
```

### 读取Web请求的URI路径中的某一段

```
function int `GET /company/*/user/*`( httpserver s )
{
        int     company_id = s.GetHttpRequestUriPath(2).ToInt() ;
        int     user_id = s.GetHttpRequestUriPath(4).ToInt() ;

        s.FormatWriteHttpResponseBody( "company_id[%d] user_id[%d]" , company_id , user_id );
        s.WriteHttpResponseEnd();

        return http.HTTP_STATUSCODE_OK;
}
```

### 读取Web请求的URI参数数据

以下代码读取Web请求HTTP URI中的参数

```
	map uri_params = s.GetHttpRequestUriParameters() ;
	int page_size = m.Get( "page_size" ).ToInt() ;
```

### 读取Web请求Cookies

```
	map cookies = s.GetHttpRequestCookies() ;
	string telephone_no = cookies.Get( "user" ) ;
	string token = cookies.Get( "token" ) ;
```

### 读取Web请求的HTTP体数据

```
	string	http_body = s.GetHttpRequest() ;
```

### 读取Web请求的表单POST数据

以下代码读取表单POST数据到KV映射对象`post`中，再指定表单域名，得到表单域值

```
	map post = s.GetHttpRequestPost() ;
	string telephone_no = post.Get( "telephone_no" ) ;
	int page = post.Get( "page" ).ToInt() ;
```

## 写出响应数据

### 写出一个Web响应的HTTP头数据

```
	s.WriteHttpResponseHeader( "My-Header" , "myname=myvalue" );
```

### 写出一个Web响应的HTTP头Cookie

```
	s.WriteHttpResponseCookie( "myname" , "myvalue" , 600 ); /* 600秒后过期 */
```


### 一次性写出Web响应的HTTP体数据

```
	s.SetHttpResponse( "<html>...</html>" );
```

### 一次性格式化Web响应的HTTP体数据

```
	s.FormatHttpResponse( "<html><head><title>%s</title></head><body>...</body></html>" , my_title );
```

### 分多次写出/格式化Web响应的HTTP体数据

```
	s.WriteHttpResponseBody( "<p>HTTP-URI[" + uri + "]</p>\n" );
	s.WriteHttpResponseBody( "<hr>\n" );
	s.FormatWriteHttpResponseBody( "<p>name[%s]</p>\n" , name );
	...
	s.WriteHttpResponseEnd();
```

### 装载一个外部文件作为Web响应的HTTP体数据（没啥用，一般静态网页文件都由`httpserver`以不匹配动态页面文件扩展名为由直接读磁盘了）

```
	s.SetHttpResponseFromFile( "index.html" );
```

### 装载一个外部模板文件渲染后作为Web响应的HTTP体数据

`zlang`自带通用模板库用于渲染HTML，对象名为`htmlsection`。

首先编写一个HTML模板文件，需要替换的部分用小括号括起来，细分以下集中类型：

- 字段域，即用实际的代码中的对象值替换`$$(占位符名字)`
- 区域块，当代码中的占位符对象被设置时，该区域`$${占位符名字/...$$/占位符名字}`存在，否则不删除；
- 明细体，当代码中的明细占位符对象被设置时，该明细区域`$$[占位符名字/...$$/占位符名字]`循环复制，次数为明细占位符对象值数量；

然后编写代码设置所需对象到渲染对象`htmlsection`中，最后调用SetHttpResponseFromHtmlTemplate渲染出完整的HTML。

来看一个例子，HTML模板文件：

```
<html>
<head>
<title>$$(HEADER_TITLE)</title>
</head>
<body>
$${UNLOGIN/
<p>Please login first</p>
$$/UNLOGIN}
$${LOGIN/
<p>USER : $$(USER)</p>
<table border="1">
        <tr>
                <td>ID</td>
                <td>NAME</td>
                <td>AMOUNT</td>
        </tr>
        $$[DETAIL/
        <tr>
                <td>$$(ID)</td>
                <td>$$(NAME)</td>
                <td>$$(AMOUNT:%.1lf)</td>
        </tr>
        $$/DETAIL]
</table>
<p>      TOTAL  : $$(TOTAL)</p>
<p>TOTAL_AMOUNT : $$(TOTAL_AMOUNT)</p>
<p>    DATETIME : $$(DATETIME)</p>
$$/LOGIN}
</body>
</html>
```

`HEADER_TITLE`、`USER`等都是单个字段域，`UNLOGIN`、`LOGIN`是区域块，`DETAIL`是明细块。

设置以上对象的代码：

```
function int `GET /test_httpserver_htmlsection_1_1`( httpserver s )
{
        htmlsection     sec ;
        htmlsection     sec_LOGIN ;
        htmlsection     sec_DETAIL ;
        long            total ;
        double          total_amount ;
        double          amount ;
        string          tmp ;
        long            n ;

        sec.SetString( "HEADER_TITLE" , "my header title" );

        sec_LOGIN = sec.CreateSection( "LOGIN" ) ; /* 由外层区域块'sec'创建里层(第二层)区域块'sec_LOGIN'，可以嵌套多层 */
        sec_LOGIN.SetString( "USER" , "lh" ); /* 第二层区域块'sec_LOGIN'内含字段域'USER' */

        total = 0 ;
        total_amount = 0 ;
        for( n = 1 ; n <= 3 ; n++ )
        {
                sec_DETAIL = sec_LOGIN.CreateDetail( "DETAIL" ) ; /* 每次明细循环都创建里层区域块'DETAIL' */
                sec_DETAIL.SetLong( "ID" , n ); /* 里层区域块'DETAIL'内含字段域'ID'等 */
                tmp.Format( "A%ld" , n );
                sec_DETAIL.SetString( "NAME" , tmp );
                tmp.Format( "%ld" , n );
                amount = tmp.ToDouble() * 100 ;
                sec_DETAIL.SetDouble( "AMOUNT" , amount );

                total++;
                total_amount += amount ;
        }

        sec_LOGIN.SetLong( "TOTAL" , total );
        sec_LOGIN.SetDouble( "TOTAL_AMOUNT" , total_amount );
        datetime.GetNow();
        sec_LOGIN.SetString( "DATETIME" , datetime.FormatString("%Y-%m-%d %H:%M:%S") );

        s.SetHttpResponseFromHtmlTemplate( "test_httpserver_htmlsection_1_1.html" , sec ); /* 最后调用SetHttpResponseFromHtmlTemplate，输入模板文件名和外层区域块'sec'，输出渲染出来的完整HTML */

        return http.HTTP_STATUSCODE_OK;
}
```

渲染结果：

```
<html>
<head>
<title>my header title</title>
</head>
<body>


<p>USER : lihua</p>
<table border="1">
        <tr>
                <td>ID</td>
                <td>NAME</td>
                <td>AMOUNT</td>
        </tr>

        <tr>
                <td>1</td>
                <td>A1</td>
                <td>100.0</td>
        </tr>

        <tr>
                <td>2</td>
                <td>A2</td>
                <td>200.0</td>
        </tr>

        <tr>
                <td>3</td>
                <td>A3</td>
                <td>300.0</td>
        </tr>

</table>
<p>      TOTAL  : 3</p>
<p>TOTAL_AMOUNT : 600.000000</p>
<p>    DATETIME : 2024-09-01 02:22:48</p>

</body>
</html>
```

`htmlsection`内含函数详见“网络”章中的htmlsection节。

### 一个完整网页模板渲染的示例

示例代码：`test_httpserver_htmlsection_1_1.z`

```
import stdtypes stdio thread network datetime ;

/*
curl -vvv -X GET -0 'http://192.168.11.79:9527/test_httpserver_htmlsection_1_1'
*/
function int `GET /test_httpserver_htmlsection_1_1`( httpserver s )
{
        htmlsection     sec ;
        htmlsection     sec_LOGIN ;
        htmlsection     sec_DETAIL ;
        long            total ;
        double          total_amount ;
        double          amount ;
        string          tmp ;
        long            n ;

        sec.SetString( "HEADER_TITLE" , "my header title" );

        sec_LOGIN = sec.CreateSection( "LOGIN" ) ;
        sec_LOGIN.SetString( "USER" , "lihua" );

        total = 0 ;
        total_amount = 0 ;
        for( n = 1 ; n <= 3 ; n++ )
        {
                sec_DETAIL = sec_LOGIN.CreateDetail( "DETAIL" ) ;
                sec_DETAIL.SetLong( "ID" , n );
                tmp.Format( "A%ld" , n );
                sec_DETAIL.SetString( "NAME" , tmp );
                tmp.Format( "%ld" , n );
                amount = tmp.ToDouble() * 100 ;
                sec_DETAIL.SetDouble( "AMOUNT" , amount );

                total++;
                total_amount += amount ;
        }

        sec_LOGIN.SetLong( "TOTAL" , total );
        sec_LOGIN.SetDouble( "TOTAL_AMOUNT" , total_amount );
        datetime.GetNow();
        sec_LOGIN.SetString( "DATETIME" , datetime.FormatString("%Y-%m-%d %H:%M:%S") );

        s.SetHttpResponseFromHtmlTemplate( "test_httpserver_htmlsection_1_1.html" , sec );

        return http.HTTP_STATUSCODE_OK;
}

function int main( array args )
{
        httpserver.SetListenAddress( "" , 9527 );
        httpserver.SetDomain( "*" );
        httpserver.SetWwwroot( "$HOME$/www" );
        httpserver.SetLogFile( "log/access.log" , "log/error.log" );
        r = httpserver.Run() ;
        if( r )
        {
                stdout.FormatPrintln( "httpserver.Run err[%d][%s]" , r , httpserver.GetError() );
                return 1;
        }
        else
        {
                stdout.Println( "httpserver.Run normal ended" );
        }

        return 0;
}
```

## 官方自带的一个Web框架`zwebtemplate`

### 关于zwebtemplate

[`zwebtemplate`](https://gitee.com/calvinwilliams/zwebtemplate)是`zlang`语言开发的一个开源项目，它包含了一个快速成型的`zlang`语言的Web框架，还包含了一个小型的Web应用网站“家庭书籍管理系统”。它具体包含：

1. Web框架：一个完整的启动函数`main.z`、一个常用全局对象 `global.z`、一个工具对象库`util.z`、一个常用拦截器库`intercept.z`、以及一个页码控制器对象`page_control.z`；
2. Web应用：一个初具规模的用户模块`user.z`（用户注册、用户登录、用户登出、用户认证）；
3. Web应用：一个书籍管理模块`book.z`；

### 开发者可以通过这个开源项目

1. 感受用`zlang`实现的Web框架和运行机制；

2. 学习如何用`zlang`开发Web应用；

3. 作为一个Web应用网站种子，成长成自己的网站；

### 项目目录说明

​	`zlang`源码文件都放在`src/`目录里；

​	`HTML/JS/CSS文件`或`HTML模板文件`都放在`www/`目录里；

​	数据库采用`MySQL`，其DDL放在`sql/`目录里，数据放在`data/`目录里（可以直接执行.sql或通过zdbtools导入.xlsx）；

### 安装说明

​	在.profile里设置环境变量并生效

```
export DBTYPE=MYSQL
export DBHOST=localhost
export DBPORT=3306
export DBUSER=zwebtemplate
export DBPASS=zwebtemplate
export DBNAME=zwebtemplatedb
```

​	安装数据库、导入表结构和表数据

​	启动Web应用网站

```
$ zlang src/main.z
```

​	打开浏览器，访问网站

```
http://(ip):8080/
```

### 源码解读

#### 启动函数`main.z`

```
// 导入常用对象库
import stdtypes stdio thread log system network datetime database encoding json ;

/* test
http://localhost:8080/
http://192.168.11.84:8080/
ab -n 10000 -c 1 http://localhost:8080/index.zweb
*/

// 包含公共库
include "global.z"
include "util.z"
include "page_control.z"
include "intercept.z"

// 包含模块库
include "menu.z"
include "user.z"
include "book_class.z"
include "book_shelf.z"
include "book.z"

// 命令行参数说明
function void usage()
{
	stdout.Println( "USAGE : zlang main.z --log-level:[DEBUG|INFO*|WARN|ERROR|FATAL]" );
	stdout.Println( "                     --worker-processes:(num)" );
	return;
}

// 启动函数
function int main( array args )
{
	const string	LOG_LEVEL_PREFIX = "--log-level:" ;
	string		log_level ;
	const string	WORKER_PROCESSES_PREFIX = "--worker-processes:" ;
	int		worker_processes ;
	
	log_level = "INFO" ; // 设置默认日志等级
	worker_processes = 2 ; // 设置网站进程池大小
	
	// 解析命令行参数
	foreach( string a in args )
	{
		if( a.StartWith(LOG_LEVEL_PREFIX) )
			log_level = a.SubString( LOG_LEVEL_PREFIX.Length()+1 , a.Length() ) ;
		else if( a.StartWith(WORKER_PROCESSES_PREFIX) )
			worker_processes = a.SubString( WORKER_PROCESSES_PREFIX.Length()+1 , a.Length() ).ToInt() ;
	}
	
	stdout.FormatPrintln( "--- command parameters ---" );
	
	if( log_level != "" )
	{
		if( log_level != "DEBUG" && log_level != "INFO" && log_level != "WARN" && log_level != "ERROR" && log_level != "FATAL" )
		{
			stdout.FormatPrintln( "*** ERROR : command parameter '--log-level:'[%{log_level}] invalid" );
			usage();
			exit(7);
		}
		
	}
	
	// 显示解析出来的命令行参数
	stdout.FormatPrintln( "log_level : [%{log_level}]" );
	stdout.FormatPrintln( "worker_processes : [%{worker_processes}]" );
	
	try
	{
		// 从环境变量中读取数据库连接信息
		stdout.FormatPrintln( "--- database environment ---" );
		global_context.dbtype = system.GetEnvironmentVar( "DBTYPE" ) ;
		global_context.dbhost = system.GetEnvironmentVar( "DBHOST" ) ;
		global_context.dbport = system.GetEnvironmentVar( "DBPORT" ).ToInt() ;
		global_context.dbuser = system.GetEnvironmentVar( "DBUSER" ) ;
		global_context.dbpass = system.GetEnvironmentVar( "DBPASS" ) ;
		global_context.dbname = system.GetEnvironmentVar( "DBNAME" ) ;
		stdout.FormatPrintln( "DBTYPE[%s]" , global_context.dbtype );
		stdout.FormatPrintln( "DBHOST[%s]" , global_context.dbhost );
		stdout.FormatPrintln( "DBPORT[%d]" , global_context.dbport );
		stdout.FormatPrintln( "DBUSER[%s]" , global_context.dbuser );
		stdout.FormatPrintln( "DBNAME[%s]" , global_context.dbname );
		
		// 设置数据库连接模式，连接上数据库服务器
		database.EnableAssistantThread( true );
		database.SetWatchIdleTimeval( 60 );
		global_context.sess = database.Connect( global_context.dbtype , global_context.dbhost , global_context.dbport , global_context.dbuser , global_context.dbpass , global_context.dbname ) ;
		stdout.Println( "database.Connect ok" );
		
		// 设置日志环境
		log.SetOutput( log.OUTPUT_FILE , "log/zwebtemplate.log" );
		log.SetStyles( log.STYLE_DATETIMEMS + log.STYLE_LOGLEVEL + log.STYLE_PID + log.STYLE_TID + log.STYLE_SOURCE + log.STYLE_FORMAT + log.STYLE_NEWLINE );
		log.SetLevel( log.LEVEL_INFO );
		if( log_level == "DEBUG" )
			log.SetLevel( log.LEVEL_DEBUG );
		else if( log_level == "INFO" )
			log.SetLevel( log.LEVEL_INFO );
		else if( log_level == "WARN" )
			log.SetLevel( log.LEVEL_WARN );
		else if( log_level == "ERROR" )
			log.SetLevel( log.LEVEL_ERROR );
		else if( log_level == "FATAL" )
			log.SetLevel( log.LEVEL_FATAL );
		
		log.Info( "--- httpserver start ---" );
		stdout.FormatPrintln( "--- httpserver start ---" );
		
		// 设置Web服务器环境，启动W之
		httpserver.SetListenAddress( "" , 8080 );
		httpserver.SetDomain( "*" );
		httpserver.SetWwwroot( "www" );
		httpserver.SetIndex( "index.html" );
		httpserver.SetSocgiType( "zweb" );
		httpserver.SetLogFile( "log/access.log" , "log/error.log" );
		httpserver.SetWorkerProcesses( worker_processes );
		r = httpserver.Run() ;
		stdout.Println( "httpserver.Run started" );
		
		log.Info( "--- httpserver end ---" );
		
		catch
		{
			// 如果报错，则显示错误信息
			include "catch.z"
			log.Info( "--- httpserver interrupt ---" );
		}
	}
	
	// 断开数据库连接
	stdout.Println( "sess.Disconnect" );
	global_context.sess.Disconnect();
	
	return 0;
}
```

#### 常用全局对象`global.z`

```
charset "zh_CN.utf8"

object global_context
{
	// 数据库连接信息
	string		dbtype ;
	string		dbhost ;
	int		dbport ;
	string		dbuser ;
	string		dbpass ;
	string		dbname ;
	
	// 当Web请求处理报错时，设置错误信息；Web请求拦截器检查HTTP POST等方法以开启数据库事务，检查是否设置错误信息以决定回滚或提交信息
	string		error_info ;
	
	// 数据库连接会话
	dbsession	sess ;
	
	// 用于记录和计算Web请求耗时
	elapse		web_elapse ;
	
	// 列表页面的每页记录数量缺省值
	const int	PAGE_SIZE_DEFAULT = 20 ;
}
```

#### 工具对象库`util.z`

当业务逻辑中发现错误，调用工具对象html_redirect的函数，就能很方便的进行回退和前进。

```
// 用HTML实现Web请求的回退、前进、陷入报错页面
object html_redirect
{
	// 用HTML实现Web请求的回退：装载HTML模板文件"goback.tpl.html"，设置占位符MESSAGE，实例化模板文件，得到最终HTML，塞入Web响应体中
	function int goback( httpserver s , string message )
	{
		htmlsection	sec ;
		
		sec.SetString( "MESSAGE" , message );
		sec.SetInt( "WAIT_TIME" , 0 );
		s.SetHttpResponseFromHtmlTemplate( "goback.tpl.html" , sec );
		
		return 0;
	}
	
	// 比上一个函数多一个等待时间参数wait_time，在错误提示页面等待wait_time秒后再回退
	function int goback( httpserver s , string message , int wait_time )
	{
		htmlsection	sec ;
		
		sec.SetString( "MESSAGE" , message );
		sec.SetInt( "WAIT_TIME" , wait_time*1000 );
		s.SetHttpResponseFromHtmlTemplate( "goback.tpl.html" , sec );
		
		return 0;
	}
	
	// 以下两个是前进页面；占位符MESSAGE是提示消息、FORWARD_URI是前进的URI
	function int goforward( httpserver s , string message , string uri )
	{
		htmlsection	sec ;
		
		sec.SetString( "MESSAGE" , message );
		sec.SetString( "FORWARD_URI" , uri );
		sec.SetInt( "WAIT_TIME" , 0 );
		s.SetHttpResponseFromHtmlTemplate( "forward.tpl.html" , sec );
		
		return 0;
	}
	
	// 占位符WAIT_TIME是页面打开后等待wait_time秒后刷新前进
	function int goforward( httpserver s , string message , string uri , int wait_time )
	{
		htmlsection	sec ;
		
		sec.SetString( "MESSAGE" , message );
		sec.SetString( "FORWARD_URI" , uri );
		sec.SetInt( "WAIT_TIME" , wait_time*1000 );
		s.SetHttpResponseFromHtmlTemplate( "forward.tpl.html" , sec );
		
		return 0;
	}
	
	// 陷入报错页面，不会自动回退或前进
	function int showerror( httpserver s , string message )
	{
		htmlsection	sec ;
		
		sec.SetString( "MESSAGE" , message );
		s.SetHttpResponseFromHtmlTemplate( "error.tpl.html" , sec );
		
		return 0;
	}
}

// 以下拦截器是配合对象html_redirect使用，业务逻辑里调用html_redirect的函数时，自动输出相应日志
intercept after `html_redirect.goback(httpserver,string)`( object obj , functionptr func , array in_params , array out_params )
{
	string	message = in_params.Get(2) ;
	
	log.Error( "html_redirect.goback message[%s]" , message );
}

intercept after `html_redirect.goback(httpserver,string,int)`( object obj , functionptr func , array in_params , array out_params )
{
	string	message = in_params.Get(2) ;
	int	wait_time = in_params.Get(3) ;
	
	log.Error( "html_redirect.goback message[%s] wait_time[%d]" , message , wait_time );
}

intercept after `html_redirect.goforward(httpserver,string,string)`( object obj , functionptr func , array in_params , array out_params )
{
	string	message = in_params.Get(2) ;
	string	uri = in_params.Get(3) ;
	
	log.Error( "html_redirect.goforward message[%s] uri[%s]" , message , uri );
}

intercept after `html_redirect.goforward(httpserver,string,string,int)`( object obj , functionptr func , array in_params , array out_params )
{
	string	message = in_params.Get(2) ;
	string	uri = in_params.Get(3) ;
	int	wait_time = in_params.Get(4) ;
	
	log.Error( "html_redirect.goforward message[%s] uri[%s] wait_time[%d]" , message , uri , wait_time );
}

intercept after `html_redirect.showerror(httpserver,string)`( object obj , functionptr func , array in_params , array out_params )
{
	string	message = in_params.Get(2) ;
	
	log.Error( "html_redirect.showerror message[%s]" , message );
}

// 检查是否存在SQL注入的工具函数
function bool CheckSqlInjection( string value )
{
	if( value == null )
		return false;
	
	if( value.IndexOf("\"") >= 1 || value.IndexOf("'") >= 1 )
		return true;
	
	return false;
}
```

#### 常用拦截器库`intercept.z`

这些拦截器可以在业务逻辑中执行特定操作时自动输出日志，业务逻辑中就不用写大量的输出日志代码了。

```
// Web请求处理前置拦截器：输出日志，根据HTTP METHOD决定是否开启事务
intercept before `.* /*.zweb(httpserver)`( object obj , functionptr func , array in_params , array out_params )
{
	httpserver	s = in_params.Get(1) ;
	
	log.Info( "--- enter web function[%s] ---------" , func.GetFullFunctionName() );
	
	global_context.web_elapse.GetBeginTimeval();
	global_context.sess.ClearAccumulateDiffElapse();
	
	if( s.GetHttpRequestMethod() == "POST" )
	{
		global_context.error_info = "" ;
		global_context.sess.Begin();
	}
	
	return;
}

// Web请求处理后置拦截器：根据HTTP METHOD和是否设置错误信息决定是否回滚事务或提交事务，输出日志（含Web处理耗时）
intercept after `.* /*.zweb(httpserver)`( object obj , functionptr func , array in_params , array out_params )
{
	httpserver	s = in_params.Get(1) ;
	
	if( s.GetHttpRequestMethod() == "POST" )
	{
		if( global_context.error_info == "" )
			global_context.sess.Commit();
		else
			global_context.sess.Rollback();
	}
	
	web_elapse_us = global_context.web_elapse.GetEndTimevalAndDiffElapse();
	web_elapse_s = web_elapse_us / 1000000 ;
	web_elapse_us %= 1000000 ;
	db_elapse_us = global_context.sess.AccumulateDiffElapse() ;
	db_elapse_s = db_elapse_us / 1000000 ;
	db_elapse_us %= 1000000 ;
	
	log.Info( "--- leave web function[%s] , return[%d] web_elapse[%d.%06d]s db_elapse[%d.%06d]s ---------" , func.GetFullFunctionName() , out_params.Get(1) , web_elapse_s , web_elapse_us , db_elapse_s , db_elapse_us );
	
	return;
}

// 从Web请求中读取Cookies拦截器：只要业务逻辑调用读取Cookies，立即就把每一个Cookie输出到日志中
intercept after `httpserver.GetHttpRequestCookies()`( object obj , functionptr func , array in_params , array out_params )
{
	map	m = out_params.Get(1) ;
	
	foreach( string k in m )
	{
		string new v = m.Get(k) ;
		log.Info( "http cookie[%s][%s]" , k , v );
	}
	
	return;
}

// 从Web请求中读取URI参数拦截器：只要业务逻辑调用读取URI参数，立即就把每一个URI参数输出到日志中
intercept after `httpserver.GetHttpRequestUriParameters()`( object obj , functionptr func , array in_params , array out_params )
{
	map	m = out_params.Get(1) ;
	
	foreach( string k in m )
	{
		string new v = m.Get(k) ;
		log.Info( "http uri param[%s][%s]" , k , v );
	}
	
	return;
}

// 从Web请求中读取HTTP POST BODY拦截器：只要业务逻辑调用读取HTTP POST BODY，立即就把每一个HTTP POST BODY输出到日志中
intercept after `httpserver.GetHttpRequestPost()`( object obj , functionptr func , array in_params , array out_params )
{
	map	m = out_params.Get(1) ;
	
	foreach( string k in m )
	{
		string new v = m.Get(k) ;
		log.Info( "http post[%s][%s]" , k , v );
	}
	
	return;
}

// 启动数据库事务拦截器：只要业务逻辑中调用Begin，立即输出提示日志
intercept after `dbsession.Begin()`( object obj , functionptr func , array in_params , array out_params )
{
	log.Info( "database transaction Begin" );
}

// 回滚数据库事务拦截器：只要业务逻辑中调用Rollback，立即输出提示日志
intercept after `dbsession.Rollback()`( object obj , functionptr func , array in_params , array out_params )
{
	log.Info( "database transaction Rollback" );
}

// 提交数据库事务拦截器：只要业务逻辑中调用Commit，立即输出提示日志
intercept after `dbsession.Commit()`( object obj , functionptr func , array in_params , array out_params )
{
	log.Info( "database transaction Commit" );
}

// 执行数据库SQL拦截器：只要业务逻辑中执行SQL，立即输出提示日志
intercept after `dbsession.Execute(...)`( object obj , functionptr func , array in_params , array out_params )
{
	sql_elapse_us = global_context.sess.GetSqlElapse() ;
	sql_elapse_s = sql_elapse_us / 1000000 ;
	sql_elapse_us %= 1000000 ;
	
	string sql = in_params.Get(1) ;
	dbresult dbres = out_params.Get(1) ;
	if( dbres == null )
	{
		log.Error( "execute sql[%s] return result[null] retcode[%d]-[%d][%s]-[%s] , sql_elapse[%d.%06d]s" , sql , database.GetLastErrno() , database.GetLastNativeErrno() , database.GetLastNativeError() , database.GetLastSqlState() , sql_elapse_s , sql_elapse_us );
	}
	else
	{
		if( sql.StartCaseWith( "SELECT " ) )
		{
			if( dbres.NotFound() )
				log.Info( "execute sql[%s] result[NOTFOUND] , sql_elapse[%d.%06d]s" , sql , sql_elapse_s , sql_elapse_us );
			else
				log.Info( "execute sql[%s] result[%p] , sql_elapse[%d.%06d]s" , sql , dbres , sql_elapse_s , sql_elapse_us );
		}
		else
		{
			log.Info( "execute sql[%s] result[%p] , affected-rows[%d] sql_elapse[%d.%06d]s" , sql , dbres , dbres.GetAffectedRows() , sql_elapse_s , sql_elapse_us );
		}
	}
	
	return;
}
```

#### 页码控制器对象`page_control.z`

```
// 用于页码控制器的工具对象
object page_control
{
	int	page_size ;
	int	page_no ;
	int	record_count ;
	
	int	page_count ;
	int	record_no ;
	
	// 设置属性page_size、page_no、record_count，调用函数SetHtmlSectionsData，自动计算赋值page_count、record_no，并自动设置一系列HTML模板占位符的值（和HTML模板配合使用，用于生成列表页面的右下方的控制栏）
	/* HTML模板
	第$$(PAGE_NO)页 总共$$(PAGE_COUNT)页
	<input type="hidden" name="hiddenPageCount" id="hiddenPageCount" value="$$(PAGE_COUNT)" />
	|
	$${HAS_FIRST_PAGE_HYPERLINK/ <a href="/book_list.zweb?page_size=$$(PAGE_SIZE)&page_no=1"> $$/HAS_FIRST_PAGE_HYPERLINK} 首页 $${HAS_FIRST_PAGE_HYPERLINK/ </a> $$/HAS_FIRST_PAGE_HYPERLINK}
	| 
	$${HAS_PREV_PAGE_HYPERLINK/ <a href="/book_list.zweb?page_size=$$(PAGE_SIZE)&page_no=$$(PREV_PAGE_NO)"> $$/HAS_PREV_PAGE_HYPERLINK} 上一页 $${HAS_PREV_PAGE_HYPERLINK/ </a> $$/HAS_PREV_PAGE_HYPERLINK}
	| 
	$${HAS_NEXT_PAGE_HYPERLINK/ <a href="/book_list.zweb?page_size=$$(PAGE_SIZE)&page_no=$$(NEXT_PAGE_NO)"> $$/HAS_NEXT_PAGE_HYPERLINK} 下一页 $${HAS_NEXT_PAGE_HYPERLINK/ </a> $$/HAS_NEXT_PAGE_HYPERLINK}
	| 
	$${HAS_LAST_PAGE_HYPERLINK/ <a href="/book_list.zweb?page_size=$$(PAGE_SIZE)&page_no=$$(LAST_PAGE_NO)"> $$/HAS_LAST_PAGE_HYPERLINK} 末页 $${HAS_LAST_PAGE_HYPERLINK/ </a> $$/HAS_LAST_PAGE_HYPERLINK}
	|
	跳到
	<input name="textfieldJumpToPageNo" type="text" id="textfieldJumpToPageNo" size="3" maxlength="3" />
	<input type="button" name="buttonJumpToPageNo" id="buttonJumpToPageNo" value="页" onclick="onJumpToPageNo();" />
	*/
	function void SetHtmlSectionsData( int page_size_default , htmlsection sec )
	{
		if( page_size == null || page_size <= 0 )
			page_size = page_size_default ;
		page_count = record_count / page_size + (record_count%page_size?1:0) ;
		if( page_no == null || page_no <= 0 )
			page_no = 1 ;
		else if( page_no > page_count )
			page_no = page_count ;
		
		if( page_no > 1 )
		{
			sec_HAS_FIRST_PAGE_HYPERLINK = sec.CreateSection( "HAS_FIRST_PAGE_HYPERLINK" ) ;
			sec_HAS_FIRST_PAGE_HYPERLINK.SetInt( "PAGE_SIZE" , page_size );
			sec_HAS_PREV_PAGE_HYPERLINK = sec.CreateSection( "HAS_PREV_PAGE_HYPERLINK" ) ;
			sec_HAS_PREV_PAGE_HYPERLINK.SetInt( "PAGE_SIZE" , page_size );
			sec_HAS_PREV_PAGE_HYPERLINK.SetInt( "PREV_PAGE_NO" , page_no-1 );
		}
		if( page_no < page_count )
		{
			sec_HAS_NEXT_PAGE_HYPERLINK = sec.CreateSection( "HAS_NEXT_PAGE_HYPERLINK" ) ;
			sec_HAS_NEXT_PAGE_HYPERLINK.SetInt( "PAGE_SIZE" , page_size );
			sec_HAS_NEXT_PAGE_HYPERLINK.SetInt( "NEXT_PAGE_NO" , page_no+1 );
			sec_HAS_LAST_PAGE_HYPERLINK = sec.CreateSection( "HAS_LAST_PAGE_HYPERLINK" ) ;
			sec_HAS_LAST_PAGE_HYPERLINK.SetInt( "PAGE_SIZE" , page_size );
			sec_HAS_LAST_PAGE_HYPERLINK.SetInt( "LAST_PAGE_NO" , page_count );
		}
		
		sec.SetInt( "PAGE_COUNT" , page_count );
		sec.SetInt( "PAGE_SIZE" , page_size );
		sec.SetInt( "PAGE_NO" , page_no );
		
		record_no = (page_no-1) * page_size ;
		
		return;
	}
}
```

#### 一个初具规模的用户模块`user.z`

初具规模：用户注册、用户登录、用户登出、用户认证

```
/*
校验Cookie中的用户token，用于用户登录后的每一次日常操作前的检查
如果用户未登录，函数返回-1
如果数据库中没有用户信息，函数返回null
如果token不一致，函数返回null
其它报错，函数返回null
校验通过，函数返回user_id
*/
function long CheckUserToken( httpserver s )
{
	try
	{
		// 读取Cookies
		map http_cookies = s.GetHttpRequestCookies() ;
		long user_id = uncatch http_cookies.Get( "user_id" )?.ToLong() ;
		string login_token = uncatch http_cookies.Get( "login_token" ) ;
		if( user_id == null || login_token == null )
		{
			log.Info( "no cookies" );
			return -1;
		}
		
		// 查询数据库中是否存在该用户信息
		sql = "SELECT * FROM user_session WHERE user_id=?" ;
		dbres = global_context.sess.Execute( sql , user_id ) ;
		if( dbres.NotFound() )
		{
			global_context.error_info.Format( "user_id[%{user_id}] not found" );
			log.Error( global_context.error_info , sql );
			s.WriteHttpResponseCookie( "user_id" , "" , 0 );
			s.WriteHttpResponseCookie( "login_token" , "" , 0 );
			html_redirect.goforward( s , "user not found" , "/index.zweb" , 5 );
			return null;
		}
		zobject user_session = dbres.RowToObject(1) ;
		
		// 检查token是否一致
		if( login_token != user_session.login_token )
		{
			global_context.error_info = "user token not matched" ;
			log.Error( global_context.error_info );
			s.WriteHttpResponseCookie( "user_id" , "" , 0 );
			s.WriteHttpResponseCookie( "login_token" , "" , 0 );
			html_redirect.goforward( s , "token not matched" , "/index.zweb" , 5 );
			return null;
		}
		
		log.Info( "check token ok" );
		
		return user_id;
		
		catch // 如果发生异常，输出异常信息到页面
		{
			include "catch.z"
			return null;
		}
	}
}

// 渲染HTML页面：主页面上方的网站标题区域，该区域右边如果未登录则显示注册和登录等连接，如果登录则显示登出等连接
function int `GET /indexframe_title_and_user_manage.zweb`( httpserver s )
{
	htmlsection	sec ;
	
	try
	{
		// 校验用户token
		long user_id = CheckUserToken( s ) ;
		if( user_id == null )
			return http.HTTP_STATUSCODE_OK;
		
		// 如果未登录，渲染HTML区域UNLOGIN
		if( user_id == -1 )
		{
			sec.CreateSection( "UNLOGIN" );
		}
		else // 如果校验token通过，设置占位符USER_NAME，渲染HTML区域LOGIN
		{
			sql = "SELECT * FROM user_user WHERE user_id=?" ;
			dbres = global_context.sess.Execute( sql , user_id ) ;
			if( dbres.NotFound() )
			{
				global_context.error_info.Format( "user_id[%{user_id}] not found" );
				log.Error( global_context.error_info , sql );
				html_redirect.showerror( s , global_context.error_info );
				return null;
			}
			zobject user_user = dbres.RowToObject(1) ;
			
			htmlsection sec_LOGIN = sec.CreateSection( "LOGIN" );
			sec_LOGIN.SetString( "USER_NAME" , user_user.user_name );
		}
		
		// 设置HTML模板文件为indexframe_title_and_user_manage.tpl.html
		s.SetHttpResponseFromHtmlTemplate( "indexframe_title_and_user_manage.tpl.html" , sec );
		return http.HTTP_STATUSCODE_OK;
		
		catch // 如果发生异常，输出异常信息到页面
		{
			include "catch.z"
			return http.HTTP_STATUSCODE_OK;
		}
	}
}

// 渲染HTML页面：用户注册
function int `GET /user_registe.zweb`( httpserver s )
{
	s.SetHttpResponseFromFile( "user_registe.tpl.html" );
	return http.HTTP_STATUSCODE_OK;
}

// 处理Web请求：提交用户注册表单
function int `POST /user_registing.zweb`( httpserver s )
{
	try
	{
		// 读取HTTP POST BODY
		map http_post = s.GetHttpRequestPost() ;
		string user_name = http_post.Get( "user_name" ) ;
		string password = http_post.Get( "password" ) ;
		string password_repeated = http_post.Get( "password_repeated" ) ;
		string password_md5 = http_post.Get( "password_md5" ) ;
		if( user_name == "" || password == "" || password_repeated == "" || password != password_repeated )
		{
			global_context.error_info = "Http post item invalid" ;
			log.Error( global_context.error_info );
			html_redirect.goback( s , global_context.error_info , 10 );
			return http.HTTP_STATUSCODE_OK;
		}
		
		// 查询用户名，如果存在则报错
		sql = "SELECT user_name FROM user_user WHERE user_name=?" ;
		dbres = global_context.sess.Execute( sql , user_name ) ;
		if( dbres.Found() )
		{
			global_context.error_info.Format( "user_name[%{user_name}] exist" );
			html_redirect.goback( s , global_context.error_info , 10 );
			return http.HTTP_STATUSCODE_OK;
		}
		
		datetime.GetNow();
		
		// 往用户表中插入一条记录
		sql = "SELECT MAX(user_id) FROM user_user" ;
		dbres = global_context.sess.Execute( sql ) ;
		long user_id = dbres.GetFieldValue(1,1)?.ToLong() ;
		if( user_id == null )
			sql = "INSERT INTO user_user VALUES( 1 , ? , ? , ? , ? , 'V' )" ;
		else
			sql = "INSERT INTO user_user VALUES( (SELECT * FROM (SELECT MAX(user_id) FROM user_user) AS TEMP)+1 , ? , ? , ? , ? , 'V' )" ;
		dbres = global_context.sess.Execute( sql , user_name , password_md5 , datetime.FormatString("%Y-%m-%d %H:%M:%S") , datetime.FormatString("%Y-%m-%d %H:%M:%S") ) ;
		if( dbres.GetAffectedRows() != 1 )
		{
			global_context.error_info.Format( "create user failed" );
			html_redirect.goback( s , global_context.error_info , 10 );
			return http.HTTP_STATUSCODE_OK;
		}
		
		// 反查用户表，为了获得新插入的user_id
		sql = "SELECT * FROM user_user WHERE user_name=?" ;
		dbres = global_context.sess.Execute( sql , user_name ) ;
		if( dbres.NotFound() )
		{
			global_context.error_info.Format( "user_name[%{user_name}] not found" );
			html_redirect.goback( s , global_context.error_info , 10 );
			return http.HTTP_STATUSCODE_OK;
		}
		zobject user_user = dbres.RowToObject(1) ;
		
		// 往用户token表中插入一条记录
		login_token = gus_uuid.Generate() ;
		sql = "INSERT INTO user_session VALUES( ? , ? )" ;
		dbres = global_context.sess.Execute( sql , user_user.user_id , login_token ) ;
		if( dbres.GetAffectedRows() != 1 )
		{
			global_context.error_info.Format( "create user session failed" );
			html_redirect.goback( s , global_context.error_info , 10 );
			return http.HTTP_STATUSCODE_OK;
		}
		
		// 写user_id和login_token的Cookies
		s.WriteHttpResponseCookie( "user_id" , user_user.user_id.ToString() , 3600 );
		s.WriteHttpResponseCookie( "login_token" , login_token , 3600 );
		
		log.Info( "Create user[%{user_id}][%{user_name}] successful" );
		
		// 渲染出的HTML页面立即自动前进到首页
		html_redirect.goforward( s , "Registing successful" , "/index.html" );
		return http.HTTP_STATUSCODE_OK;
		
		catch // 如果发生异常，输出异常信息到页面
		{
			include "catch.z"
			return http.HTTP_STATUSCODE_OK;
		}
	}
}

// 渲染HTML页面：用户登录
function int `GET /user_login.zweb`( httpserver s )
{
	s.SetHttpResponseFromFile( "user_login.tpl.html" );
	return http.HTTP_STATUSCODE_OK;
}

// 处理Web请求：提交用户登录表单
function int `POST /user_loginning.zweb`( httpserver s )
{
	try
	{
		// 读取HTTP POST BODY
		map http_post = s.GetHttpRequestPost() ;
		string user_name = http_post.Get( "user_name" ) ;
		string password = http_post.Get( "password_md5" ) ;
		if( user_name == "" || password == "" )
		{
			global_context.error_info = "Http post item invalid" ;
			log.Error( global_context.error_info );
			html_redirect.goback( s , global_context.error_info , 10 );
			return http.HTTP_STATUSCODE_OK;
		}
		
		// 检查用户表中是否存在该用户名
		sql = "SELECT * FROM user_user WHERE user_name=?" ;
		dbres = global_context.sess.Execute( sql , user_name ) ;
		if( dbres.NotFound() )
		{
			global_context.error_info.Format( "user_name[%{user_name}] not found" );
			html_redirect.goback( s , global_context.error_info , 10 );
			return http.HTTP_STATUSCODE_OK;
		}
		
		zobject user_user = dbres.RowToObject(1) ;
		
		datetime.GetNow();
		
		// 更新用户表中的最近登录日期时间
		sql = "UPDATE user_user SET latest_login_datetime=? WHERE user_id=?" ;
		dbres = global_context.sess.Execute( sql , datetime.FormatString("%Y-%m-%d %H:%M:%S") , user_user.user_id ) ;
		
		login_token = gus_uuid.Generate() ;
		
		// 插入或更新用户token表记录
		sql = "SELECT * FROM user_session WHERE user_id=?" ;
		dbres = global_context.sess.Execute( sql , user_user.user_id ) ;
		if( dbres.NotFound() )
		{
			sql = "INSERT INTO user_session VALUES( ? , ? )" ;
			dbres = global_context.sess.Execute( sql , user_user.user_id , login_token ) ;
		}
		else
		{
			sql = "UPDATE user_session SET login_token=? WHERE user_id=?" ;
			dbres = global_context.sess.Execute( sql , login_token , user_user.user_id ) ;
		}
		
		// 写user_id和login_token的Cookies
		s.WriteHttpResponseCookie( "user_id" , user_user.user_id.ToString() , 3600 );
		s.WriteHttpResponseCookie( "login_token" , login_token , 3600 );
		
		log.Info( "Login user[%ld][%s] successful" , user_user.user_id , user_user.user_name );
		
		// 渲染出的HTML页面立即自动前进到首页
		html_redirect.goforward( s , "Loginning successful" , "/index.html" );
		return http.HTTP_STATUSCODE_OK;
		
		catch // 如果发生异常，输出异常信息到页面
		{
			include "catch.z"
			return http.HTTP_STATUSCODE_OK;
		}
	}
}

// 处理Web请求：点击用户登出链接
function int `POST /user_logouting.zweb`( httpserver s )
{
	try
	{
		// 校验用户token
		long user_id = CheckUserToken( s ) ;
		if( user_id == null )
			return http.HTTP_STATUSCODE_OK;
		
		if( user_id == -1 )
		{
			global_context.error_info = "No login token" ;
			html_redirect.goback( s , global_context.error_info , 5 );
			return http.HTTP_STATUSCODE_OK;
		}
		
		// 清除Cookies
		s.WriteHttpResponseCookie( "user_id" , "" , 0 );
		s.WriteHttpResponseCookie( "login_token" , "" , 0 );
		
		// 删除用户token表记录
		sql = "DELETE FROM user_session WHERE user_id=?" ;
		dbres = global_context.sess.Execute( sql , user_id ) ;
		if( dbres.GetAffectedRows() != 1 )
		{
			global_context.error_info.Format( "delete user session failed" );
			html_redirect.goback( s , global_context.error_info , 10 );
			return http.HTTP_STATUSCODE_OK;
		}
		
		// 渲染出的HTML页面立即自动前进到首页
		html_redirect.goforward( s , "Logouting successful" , "/index.html" );
		return http.HTTP_STATUSCODE_OK;
		
		catch // 如果发生异常，输出异常信息到页面
		{
			include "catch.z"
			return http.HTTP_STATUSCODE_OK;
		}
	}
}
```

#### 一个书籍管理模块`book.z`

（请自行查阅本项目）



下一章：[爬虫开发](web_crawler.md)
