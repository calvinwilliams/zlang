上一章：[网络](network.md)



**章节目录**

- [日期时间](#日期时间)
- [耗时](#耗时)

# 日期时间

日期时间对象库用于处理日期、时间相关数据。

日期时间对象：`datetime`

| 成员类型 | 成员名字和原型                     | 说明                                                         |
| :------: | ---------------------------------- | ------------------------------------------------------------ |
|   常量   | bool ATTRACT_END_OF_MONTH          | 吸附月底，如4月30日（月底）向后偏移一个月，不吸附月底是5月30日，吸附月底就是5月31日 |
|   常量   | bool NO_ATTRACT_END_OF_MONTH       | 不吸附月底                                                   |
|   函数   | void GetNow();                     | 得到当前日期时间放入对象`datetime`内                         |
|   函数   | void GetFromFormat(string,string); | 从参数取出日期时间放入对象`datetime`内，输入参数为日期时间格式化占位符串、日期时间字符串；占位符见《日期时间格式化串占位符表》 |
|   函数   | long SecondStamp();                | 得到当前秒戳                                                 |
|   函数   | int Year();                        | 返回对象中的年                                               |
|   函数   | int Month();                       | 返回对象中的月                                               |
|   函数   | int Day();                         | 返回对象中的日                                               |
|   函数   | int Hour();                        | 返回对象中的时                                               |
|   函数   | int Minute();                      | 返回对象中的分                                               |
|   函数   | int Second();                      | 返回对象中的秒                                               |
|   函数   | int Microsecond();                 | 返回对象中的毫秒                                             |
|   函数   | string FormatString(string);       | 按日期时间格式化占位符串，格式化对象中的日期时间，返回日期时间字符串；占位符见《日期时间格式化串占位符表》 |
|   函数   | void OffsetYears(int,bool);        | 偏移年，输入参数为偏移年数（正为向将来，负为向过去）、是否吸附月底 |
|   函数   | void OffsetMonths(int,bool);       | 偏移月，输入参数为偏移月数（正为向将来，负为向过去）、是否吸附月底 |
|   函数   | void OffsetDays(int);              | 偏移日                                                       |
|   函数   | void OffsetHours(int);             | 偏移时                                                       |
|   函数   | void OffsetMinutes(int);           | 偏移分                                                       |
|   函数   | void OffsetSeconds(int);           | 偏移秒                                                       |

日期时间格式化串占位符表：

| 占位符 | 说明                      |
| ------ | ------------------------- |
| %Y     | 年（YYYY）                |
| %m     | 月（1~12）                |
| %d     | 日（1~31）                |
| %H     | 时（0~23）                |
| %M     | 分（0~23）                |
| %S     | 秒（0~59）                |
| ...    | （其它参考C函数strftime） |

代码示例：`test_datetime.z`

该代码示范了获取当前日期时间，并显示输出。

```
import stdtypes stdio datetime ;

function int main( array args )
{
        datetime.GetNow();

        year = datetime.Year() ;
        month = datetime.Month() ;
        day = datetime.Day() ;
        hour = datetime.Hour() ;
        minute = datetime.Minute() ;
        second = datetime.Second() ;
        microsecond = datetime.Microsecond() ;

        stdout.Println( year );
        stdout.Println( month );
        stdout.Println( day );
        stdout.Println( hour );
        stdout.Println( minute );
        stdout.Println( second );
        stdout.Println( microsecond );

        return 0;
}
```

执行

```
$ zlang test_datetime.z
2024
3
20
19
39
30
459344
```

代码示例：`test_datetime_offset_2_1.z`

该代码示范了以不吸附月底和吸附月底两种模式下在指定日期往后增加一个月的日期计算。

```
import stdtypes stdio datetime ;

function int main( array args )
{
        datetime        t ;

        t.GetFromFormat( "%Y-%m-%d" , "2000-04-30" );
        t.OffsetMonths( 1 , datetime.NO_ATTRACT_END_OF_MONTH );
        stdout.Println( t.FormatString("it's %Y-%m-%d after offseting") );

        t.GetFromFormat( "%Y-%m-%d" , "2000-04-30" );
        t.OffsetMonths( 1 , datetime.ATTRACT_END_OF_MONTH );
        stdout.Println( t.FormatString("it's %Y-%m-%d after offseting") );

        return 0;
}
```

执行

```
$ zlang test_datetime_offset_2_1.z
it's 2000-05-30 after offseting
it's 2000-05-31 after offseting
```

代码示例：`test_datetime_getfromformat_1.z`

该代码示范了把指定日期时间字符串转换成`datetime`对象，再格式化成字符串。

```
import stdtypes stdio datetime ;

function int main( array args )
{
        datetime        t ;

        t.GetFromFormat( "%Y-%m-%d %H:%M:%S" , "2022-08-13 22:20:01" );
        stdout.Println( t.FormatString("it's %Y-%m-%d %H:%M:%S") );

        return 0;
}
```

执行

```
zlang test_datetime_getfromformat_1.z
it's 2022-08-13 22:20:01
```

代码示例：`test_datetime_formatstring_1.z`

```
import stdtypes stdio datetime ;

function int main( array args )
{
        datetime        now ;

        now.GetNow();
        stdout.Println( now.FormatString("now is %Y-%m-%d %H:%M:%S") );

        return 0;
}
```

执行

```
zlang test_datetime_formatstring_1.z
now is 2024-03-20 19:42:13
```

# 耗时

耗时对象库用于处理延迟检查、性能测试等场景。

耗时对象：`elapse`

| 成员类型 | 成员名字和原型                    | 说明                                 |
| :------: | --------------------------------- | ------------------------------------ |
|   函数   | bool GetBeginTimeval();           | 开始计时                             |
|   函数   | int GetEndTimevalAndDiffElapse(); | 结束计时，返回耗时（单位：微秒）     |
|   函数   | int GetElapse();                  | 返回最近一次计时的耗时（单位：微秒） |

代码示例：`test_elapse_1.z`

```
[zlang-tes 1:vim | 2:bash | 3:bash | 4:bash | 5:bash | 6:bash                                                                                                                                                             Copyright by Calvin
import stdtypes stdio system datetime ;

function int main( array args )
{
        elapse.GetBeginTimeval();
        system.SleepMicroseconds(10000);
        test_elapse = elapse.GetEndTimevalAndDiffElapse();
        stdout.FormatPrintln( "test_elapse[%{test_elapse}]" );

        get_test_elapse_again = elapse.GetElapse();
        stdout.FormatPrintln( "get_test_elapse_again[%{get_test_elapse_again}]" );

        return 0;
}
```

执行

```
$ zlang test_elapse_1.z
test_elapse[10692]
get_test_elapse_again[10692]
```



下一章：[随机数](random.md)