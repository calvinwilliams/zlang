上一章：[循环语句](while_statement.md)



**章节目录**
- [函数](#函数)



# 函数

## 定义函数

`zlang`可以直接定义全局函数，或者定义对象内函数。

函数的语句形式为：

```
[sync] function 输出参数 函数名( 输入参数列表 )
{
	函数代码
	
	return 输出值;
}
```

输入参数列表由多对类型对象和变量组成，展开后如

```
[sync] function 输出类型对象 函数名( 输入类型对象1 输入参数对象1 , 输入类型对象2 输入参数对象2 , ... )
{
	函数代码
	
	return 输出值对象;
}
```

修饰符`sync`可使函数变成临界区函数，多线程环境下最多只有一个线程允许执行函数内部。

一个简单的函数

```
function int myplus( int a , int b )
{
	int	c = a + b ;
	return c;
}
```

## 调用函数

代码示例：`test_func_1.z`

该代码示范了先调用`input`函数读取键盘输入字符串到对象`name`中，再调用`output`函数显示输出。

```
import stdtypes stdio ;

function int input( string name )
{
        return stdin.Scan( name );
}

function void output( string name )
{
        stdout.Println( "hello "+name );
        return;
}

function int main( array args )
{
        string  name ;

        n = input( name ) ;

        output( name );

        return n;
}
```

执行

```
$ echo "calvin" | zlang test_func_1.z
hello calvin
```

对象内函数见[面向对象](object.md)章节。

## 可变参数函数

可变参数函数的输入参数列表写成`...`，通过内置对象`zruntime.GetInputParametersCount()`获取输入参数数量，通过内置对象`zruntime.GetInputParameter(int index)`获取输入参数对象。

代码示例：`test_function_vargs_1.z`

```
import stdtypes stdio ;

function int adds( ... )
{
        int     s ;

        foreach( int i from 1 to zruntime.GetInputParametersCount() )
        {
                s += zruntime.GetInputParameter(i) ;
        }

        return s;
}

function int main( array args )
{
        int     a = 1 , b = 2 , c = 3 , d = 4 ;
        int     z ;

        z = adds( a , b ) ;
        stdout.Println( z );

        z = adds( a , b , c ) ;
        stdout.Println( z );

        z = adds( a , b , c , d ) ;
        stdout.Println( z );


        return 0;
}
```

执行

```
$ zlang test_function_vargs_1.z
3
6
10
```



下一章：[面向对象](object.md)