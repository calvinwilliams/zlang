上一章：[文件系统路径和文件](file.md)



**章节目录**

- [线程和同步](#线程)
  - [线程对象](#线程对象)
  - [线程同步](#线程同步)
    - [临界互斥](#临界互斥)
    - [临界互斥关键字](#临界互斥关键字)
    - [读写锁](#读写锁)
    - [条件变量](#条件变量)
    - [内存屏障](#内存屏障)



# 线程和同步

## 线程对象

线程让进程内工作任务可以并发的处理，以充分利用CPU核，缩短处理耗时。目前`zlang`线程对象库只支持简单的对象，后续会补充线程池等高级对象。

线程对象名：`thread`

| 成员类型 | 成员名字和原型                   | 说明                                                         | (错误.)(异常码,)异常消息               |
| -------- | -------------------------------- | ------------------------------------------------------------ | -------------------------------------- |
| 常量     | DAEMON_THREAD                    | 守护线程模式（子线程和主线程脱离）                           |                                        |
| 常量     | JOIN_THREAD                      | 同步线程模式（将来子线程还是要合并到主线程里）               |                                        |
| 函数     | int SetFunctionEntry(functionp); | 设置线程函数，返回0表示成功                                  |                                        |
| 函数     | void SetDaemon(bool);            | 设置线程模式                                                 |                                        |
| 函数     | int Start(...);                  | 开始一个线程，参数原型必须同线程函数输入参数列表一致         | EXCEPTION_MESSAGE_CREATE_THREAD_FAILED |
| 函数     | int Join(...);                   | 合并一个结束的子线程，参数原型必须同线程函数输出参数列表一致 | EXCEPTION_MESSAGE_JOIN_THREAD_FAILED   |

代码示例：`test_thread_1_2.z`

该代码示范了查找全局函数`func2`设置到线程对象中，启动线程，等待线程结束合并。

```
import stdtypes stdio thread system ;

function int func2( string str )
{
        system.SleepMicroseconds(300);

        stdout.Println( str );

        return 2;
}

function int main( array args )
{
        thread  t2 ;
        int     status ;

        t2.SetFunctionEntry( functionp.FindFunction("func2(string)") );
        t2.Start( "t2" );
        t2.Join( status );

        return status;
}
```

执行

```
$ zlang test_thread_1_2.z
t2
$ echo $?
2
```

代码示例：`test_thread_2_1.z`

该代码示范了守护线程（最后不需要结束合并）的启动。

```
import stdtypes stdio thread system ;

function void func1( string str )
{
        system.SleepMicroseconds(300);

        stdout.Println( str );

        return;
}

function int main( array args )
{
        thread  t1 ;
        int     nret ;

        t1.SetFunctionEntry( functionp.FindFunction("func1(string)") );
        t1.SetDaemon( thread.DAEMON_THREAD );
        t1.Start( "t1" );

        system.SleepSeconds( 1 );

        return 0;
}
```

执行

```
$ zlang test_thread_2_1.z
t1
```

## 线程同步

### 原子对象

在声明对象内属性时，凡是用`atomic`修饰的属性，在计算赋值、引用、自增/自减、自增赋值/自减赋值等操作时会保证其操作原子性。详见章节“面向对象”。

代码示例：`test_thread_atomic_1.z`

```
import stdtypes stdio thread ;

object atomic_obj
{
        atomic long     n ;
}

function void atomic_count()
{
        foreach( int i from 1 to 10000 )
        {
                atomic_obj.n++;
        }

        return;
}

function int main( array args )
{
        thread          t1 , t2 ;

        atomic_obj.n = 0 ;

        t1.SetFunctionEntry( functionptr.FindFunction("atomic_count()") );
        t1.Start();
        t2.SetFunctionEntry( functionptr.FindFunction("atomic_count()") );
        t2.Start();

        t1.Join();
        t2.Join();

        stdout.Println( atomic_obj.n );

        return 0;
}
```

执行

```
$ zlang test_thread_atomic_1.z
20000
```

在声明局部对象并传递到下层函数时，局部对象用`atomic`修饰，调用下层函数实参前用`atomic`修饰。

代码示例：`test_thread_atomic_2.z`

```
import stdtypes stdio thread ;

function void atomic_count( long atomic_n2 )
{
        foreach( int i from 1 to 10 )
        {
                atomic_n2++;
        }

        return;
}

function int main( array args )
{
        thread          t1 , t2 ;
        atomic long     atomic_n ;

        atomic_n = 0 ;

        t1.SetFunctionEntry( functionptr.FindFunction("atomic_count(long)") );
        t1.Start( atomic atomic_n );
        t2.SetFunctionEntry( functionptr.FindFunction("atomic_count(long)") );
        t2.Start( atomic atomic_n );

        t1.Join();
        t2.Join();

        stdout.Println( atomic_n );

        return 0;
}
```

执行

```
$ zlang test_thread_atomic_2.z
20000
```

注意：集合对象都可以用`atomic`修饰用以确保其原子操作。

### 临界互斥

临界互斥用于多线程应用中只有一个线程可以执行其中的代码段。

临界互斥对象名：`mutex`

| 成员类型 | 成员名字和原型  | 说明           | (错误.)(异常码,)异常消息                                     |
| -------- | --------------- | -------------- | ------------------------------------------------------------ |
| 函数     | bool Lock();    | 进入互斥区     | EXCEPTION_MESSAGE_LOCK_MUTEX_FAILED                          |
| 函数     | bool TryLock(); | 尝试进入互斥区 | EXCEPTION_MESSAGE_TRYLOCK_MUTEX_BUSY<br />EXCEPTION_MESSAGE_TRYLOCK_MUTEX_FAILED |
| 函数     | bool Unlock();  | 离开互斥区     | EXCEPTION_MESSAGE_UNLOCK_MUTEX_FAILED                        |

代码示例：`test_thread_mutex_1.z`

该代码示范了一块代码在同一时间只允许一个线程可以执行的同步场景。

```
import stdtypes stdio thread system ;

object thread_context
{
        mutex           lock ;
}

function void func1( thread_context ctx )
{
        ctx.lock.Lock();

        stdout.Println( "func1 - begin" );
        system.SleepSeconds( 1 );
        stdout.Println( "func1 - end" );

        ctx.lock.Unlock();

        return;
}

function void func2( thread_context ctx )
{
        ctx.lock.Lock();

        stdout.Println( "func2 - begin" );
        system.SleepSeconds( 1 );
        stdout.Println( "func2 - end" );

        ctx.lock.Unlock();

        return;
}

function int main( array args )
{
        thread_context  ctx ;

        thread t1 ;
        t1.SetFunctionEntry( functionptr.FindFunction("func1(thread_context)") );
        t1.Start( ctx );

        thread t2 ;
        t2.SetFunctionEntry( functionptr.FindFunction("func2(thread_context)") );
        t2.Start( ctx );

        t1.Join();
        t2.Join();

        return 0;
}
```

执行

```
$ zlang test_thread_mutex_1.z
func1 - begin
func1 - end
func2 - begin
func2 - end
```

### 临界互斥关键字

临界互斥对象使用必须`Lock`和`Unlock`配对，否则会破坏应用。`zlang`语言提供了使用临界互斥的简单方式，使用`sync`关键字。

代码示例：`test_thread_sync_1.z`

该代码示范了一块代码在同一时间只允许一个线程可以执行的同步场景，使用了`sync`关键字实现。

```
import stdtypes stdio thread system ;

object thread_context
{
        mutex   mutex_obj ;
}

function void func1( thread_context ctx )
{
        sync( ctx.mutex_obj )
        {
                stdout.Println( "func1 - begin" );
                system.SleepSeconds( 1 );
                stdout.Println( "func1 - end" );
        }

        return;
}

function void func2( thread_context ctx )
{
        sync( ctx.mutex_obj )
        {
                stdout.Println( "func2 - begin" );
                system.SleepSeconds( 1 );
                stdout.Println( "func2 - end" );
        }

        return;
}

function int main( array args )
{
        thread_context  ctx ;

        thread t1 ;
        t1.SetFunctionEntry( functionptr.FindFunction("func1(thread_context)") );
        t1.Start( ctx );

        thread t2 ;
        t2.SetFunctionEntry( functionptr.FindFunction("func2(thread_context)") );
        t2.Start( ctx );

        t1.Join();
        t2.Join();

        return 0;
}
```

执行

```
$ zlang test_thread_sync_1.z
func1 - begin
func1 - end
func2 - begin
func2 - end
```

`sync`还能用来修饰函数，表示函数整体成为临界互斥体。

代码示例：`test_thread_sync_atomic_1_1.z`

```
import stdtypes stdio thread ;

object atomic_obj
{
        long    n ;
}

sync function void atomic_obj_n_increase()
{
        atomic_obj.n++;
        return;
}

function void atomic_count()
{
        foreach( int i from 1 to 10000 )
        {
                atomic_obj_n_increase();
        }

        return;
}

function int main( array args )
{
        thread          t1 , t2 ;

        atomic_obj.n = 0 ;

        t1.SetFunctionEntry( functionptr.FindFunction("atomic_count()") );
        t1.Start();
        t2.SetFunctionEntry( functionptr.FindFunction("atomic_count()") );
        t2.Start();

        t1.Join();
        t2.Join();

        stdout.Println( atomic_obj.n );

        return 0;
}
```

执行

```
$ zlang test_thread_sync_atomic_1_1.z
20000
```

### 读写锁

读写锁对象用于对资源读和写操作分开同步，读读不互斥、读写互斥、谢谢互斥。

读写锁对象：`rwlock`。

| 成员类型 | 成员名字和原型       | 说明       | (错误.)(异常码,)异常消息                                     |
| -------- | -------------------- | ---------- | ------------------------------------------------------------ |
| 函数     | bool LockRead();     | 加读锁     | EXCEPTION_MESSAGE_RDLOCK_RWLOCK_FAILED                       |
| 函数     | bool LockWrite();    | 加写锁     | EXCEPTION_MESSAGE_WRLOCK_RWLOCK_FAILED                       |
| 函数     | bool TryLockRead();  | 尝试加读锁 | EXCEPTION_MESSAGE_TRYRDLOCK_RWLOCK_BUSY<br />EXCEPTION_MESSAGE_TRYRDLOCK_RWLOCK_FAILED |
| 函数     | bool TryLockWrite(); | 尝试加写锁 | EXCEPTION_MESSAGE_TRYWRLOCK_RWLOCK_BUSY<br />EXCEPTION_MESSAGE_TRYWRLOCK_RWLOCK_FAILED |
| 函数     | bool Unlock();       | 解锁       | EXCEPTION_MESSAGE_UNLOCK_RWLOCK_FAILED                       |

代码示例：`test_thread_rwlock_1.z`

该代码示范了读读不互斥的同步场景。

```
import stdtypes stdio thread system ;

object thread_context
{
        rwlock          lock ;
}

function void func1( thread_context ctx )
{
        ctx.lock.LockRead();

        stdout.Println( "func1 - begin" );
        system.SleepSeconds( 1 );
        stdout.Println( "func1 - end" );

        ctx.lock.Unlock();

        return;
}

function void func2( thread_context ctx )
{
        ctx.lock.LockRead();

        system.SleepMicroseconds( 200000 );
        stdout.Println( "func2 - begin" );
        system.SleepSeconds( 1 );
        stdout.Println( "func2 - end" );

        ctx.lock.Unlock();

        return;
}

function int main( array args )
{
        thread_context  ctx ;

        thread t1 ;
        t1.SetFunctionEntry( functionptr.FindFunction("func1(thread_context)") );
        t1.Start( ctx );

        thread t2 ;
        t2.SetFunctionEntry( functionptr.FindFunction("func2(thread_context)") );
        t2.Start( ctx );

        t1.Join();
        t2.Join();

        return 0;
}
```

执行

```
$ zlang test_thread_rwlock_1.z
func1 - begin
func2 - begin
func1 - end
func2 - end
```

代码示例：`test_thread_rwlock_2.z`

该代码示范了写写互斥的同步场景。

```
import stdtypes stdio thread system ;

object thread_context
{
        rwlock          lock ;
}

function void func1( thread_context ctx )
{
        ctx.lock.LockWrite();

        stdout.Println( "func1 - begin" );
        system.SleepSeconds( 1 );
        stdout.Println( "func1 - end" );

        ctx.lock.Unlock();

        return;
}

function void func2( thread_context ctx )
{
        ctx.lock.LockWrite();

        stdout.Println( "func2 - begin" );
        system.SleepSeconds( 1 );
        stdout.Println( "func2 - end" );

        ctx.lock.Unlock();

        return;
}

function int main( array args )
{
        thread_context  ctx ;

        thread t1 ;
        t1.SetFunctionEntry( functionptr.FindFunction("func1(thread_context)") );
        t1.Start( ctx );

        thread t2 ;
        t2.SetFunctionEntry( functionptr.FindFunction("func2(thread_context)") );
        t2.Start( ctx );

        t1.Join();
        t2.Join();

        return 0;
}
```

执行

```
zlang test_thread_rwlock_2.z
func1 - begin
func1 - end
func2 - begin
func2 - end
```

代码示例：`test_thread_rwlock_3.z`

该代码示范了非堵塞的写写互斥的同步场景。

```
import stdtypes stdio thread system ;

object thread_context
{
        rwlock          lock ;
}

function void func1( thread_context ctx )
{
        system.SleepMicroseconds( 200000 );
        for( ; ; )
        {
                b = ctx.lock.TryLockWrite() ;
                if( b == null )
                {
                        stdout.Println( "func1 - not catched lock before begin" );
                        system.SleepSeconds( 1 );
                }
                else if( b == true )
                {
                        break;
                }
                else
                {
                        stdout.Println( "func1 - error before begin" );
                        return;
                }
        }

        stdout.Println( "func1 - begin" );
        system.SleepSeconds( 3 );
        stdout.Println( "func1 - end" );

        ctx.lock.Unlock();

        return;
}

function void func2( thread_context ctx )
{
        for( ; ; )
        {
                b = ctx.lock.TryLockWrite() ;
                if( b == null )
                {
                        stdout.Println( "func2 - not catched lock before begin" );
                        system.SleepSeconds( 1 );
                }
                else if( b == true )
                {
                        break;
                }
                else
                {
                        stdout.Println( "func2 - error before begin" );
                        return;
                }
        }

        stdout.Println( "func2 - begin" );
        system.SleepSeconds( 3 );
        stdout.Println( "func2 - end" );

        ctx.lock.Unlock();

        return;
}

function int main( array args )
{
        thread_context  ctx ;

        thread t1 ;
        t1.SetFunctionEntry( functionptr.FindFunction("func1(thread_context)") );
        t1.Start( ctx );

        thread t2 ;
        t2.SetFunctionEntry( functionptr.FindFunction("func2(thread_context)") );
        t2.Start( ctx );

        t1.Join();
        t2.Join();

        return 0;
}
```

执行

```
zlang test_thread_rwlock_3.z
func2 - begin
func1 - not catched lock before begin
func1 - not catched lock before begin
func1 - not catched lock before begin
func2 - end
func1 - begin
func1 - end
```

### 条件变量

条件变量用于多线程应用中一个线程通知一个或多个线程的同步机制。

条件变量对象名：`condsig`

| 成员类型 | 成员名字和原型    | 说明                 | (错误.)(异常码,)异常消息                                     |
| -------- | ----------------- | -------------------- | ------------------------------------------------------------ |
| 函数     | bool Lock();      | 加锁                 | EXCEPTION_MESSAGE_LOCK_MUTEX_FAILED                          |
| 函数     | bool Unlock();    | 解锁                 | EXCEPTION_MESSAGE_UNLOCK_MUTEX_FAILED                        |
| 函数     | bool Wait();      | 等待信号，前后锁保护 | EXCEPTION_MESSAGE_WAIT_CONDITION_FAILED                      |
| 函数     | bool TimedWait(); | 有超时的等待信号     | EXCEPTION_MESSAGE_TIMEWAIT_CONDITION_TIMEOUT<br />EXCEPTION_MESSAGE_TIMEWAIT_CONDITION_FAILED |
| 函数     | bool Signal();    | 发送信号             | EXCEPTION_MESSAGE_SIGNAL_CONDITION_FAILED                    |
| 函数     | bool Broadcast(); | 群发信号             | EXCEPTION_MESSAGE_BROADCAST_CONDITION_FAILED                 |

代码示例：`test_thread_condsig_1.z`

该代码示范了一个线程用`Signal`激活另一个线程的`Wait`的同步场景。

```
import stdtypes stdio thread system ;

object thread_context
{
        condsig         lock ;
}

function void func1( thread_context ctx )
{
        stdout.Println( "func1 - begin" );

        ctx.lock.Wait();

        stdout.Println( "func1 - end" );

        return;
}

function void func2( thread_context ctx )
{
        system.SleepMicroseconds( 200000 );

        stdout.Println( "func2 - begin" );

        ctx.lock.Signal();

        system.SleepMicroseconds( 200000 );

        stdout.Println( "func2 - end" );

        return;
}

function int main( array args )
{
        thread_context  ctx ;

        thread t1 ;
        t1.SetFunctionEntry( functionptr.FindFunction("func1(thread_context)") );
        t1.Start( ctx );

        system.SleepSeconds( 2 );

        thread t2 ;
        t2.SetFunctionEntry( functionptr.FindFunction("func2(thread_context)") );
        t2.Start( ctx );

        t1.Join();
        t2.Join();

        return 0;
}
```

执行

```
$ zlang test_thread_condsig_1.z
func1 - begin
func2 - begin
func1 - end
func2 - end
```

代码示例：`test_thread_condsig_2.z`

该代码示范了一个线程带超时的`Wait`的同步场景。

```
import stdtypes stdio thread system ;

object thread_context
{
        condsig         lock ;
}

function void func1( thread_context ctx )
{
        stdout.Println( "func1 - TimedWait 2s ..." );
        b = ctx.lock.TimedWait( 2 , 2L ) ;
        stdout.FormatPrintln( "func1 - TimedWait 2s done , b[%b]" , b );

        stdout.Println( "func1 - TimedWait 3s ..." );
        b = ctx.lock.TimedWait( 2 , 3L ) ;
        stdout.FormatPrintln( "func1 - TimedWait 3s done , b[%b]" , b );

        return;
}

function void func2( thread_context ctx )
{
        system.SleepSeconds( 1 );
        ctx.lock.Signal();

        system.SleepSeconds( 3 );
        ctx.lock.Signal();

        return;
}

function int main( array args )
{
        thread_context  ctx ;

        stdout.EnableBuffer( false );

        thread t1 ;
        t1.SetFunctionEntry( functionptr.FindFunction("func1(thread_context)") );
        t1.Start( ctx );

        thread t2 ;
        t2.SetFunctionEntry( functionptr.FindFunction("func2(thread_context)") );
        t2.Start( ctx );

        t1.Join();
        t2.Join();

        return 0;
}
```

执行

```
$ zlang test_thread_condsig_2.z
func1 - TimedWait 2s ...
func1 - TimedWait 2s done , b[true]
func1 - TimedWait 3s ...
func1 - TimedWait 3s done , b[(null)]
```

代码示例：`test_thread_condsig_3_1.z`

该代码示范了多个线程同时`Wait`、另一个线程`Broadcast`同时激活前者的同步场景。

```
import stdtypes stdio thread system ;

object thread_context
{
        condsig         lock ;
}

function void func1( thread_context ctx )
{
        ctx.lock.Lock();
        stdout.Println( "func1 - Wait ..." );
        b = ctx.lock.Wait() ;
        stdout.FormatPrintln( "func1 - Wait done , b[%b]" , b );
        ctx.lock.Unlock();

        return;
}

function void func2( thread_context ctx )
{
        ctx.lock.Lock();
        stdout.Println( "func2 - Wait ..." );
        b = ctx.lock.Wait() ;
        stdout.FormatPrintln( "func2 - Wait done , b[%b]" , b );
        ctx.lock.Unlock();

        return;
}

function int main( array args )
{
        thread_context  ctx ;

        thread t1 ;
        t1.SetFunctionEntry( functionptr.FindFunction("func1(thread_context)") );
        t1.Start( ctx );

        thread t2 ;
        t2.SetFunctionEntry( functionptr.FindFunction("func2(thread_context)") );
        t2.Start( ctx );

        system.SleepSeconds( 2 );
        stdout.Println( "main - Broadcast" );
        ctx.lock.Broadcast();

        t1.Join();
        t2.Join();

        return 0;
}
```

执行

```
$ zlang test_thread_condsig_3_1.z
func1 - Wait ...
func2 - Wait ...
main - Broadcast
func1 - Wait done , b[true]
func2 - Wait done , b[true]
```

代码示例：`test_thread_condsig_3_2.z`

该代码示范了使用`sync`关键字的`Broadcast` 所有其它线程的`Wait`的同步场景。

```
import stdtypes stdio thread system ;

object thread_context
{
        condsig         lock ;
}

function void func1( thread_context ctx )
{
        sync( ctx.lock )
        {
                stdout.Println( "func1 - Wait ..." );
                b = ctx.lock.Wait() ;
                stdout.FormatPrintln( "func1 - Wait done , b[%b]" , b );
        }

        return;
}

function void func2( thread_context ctx )
{
        sync( ctx.lock )
        {
                stdout.Println( "func2 - Wait ..." );
                b = ctx.lock.Wait() ;
                stdout.FormatPrintln( "func2 - Wait done , b[%b]" , b );
        }

        return;
}

function int main( array args )
{
        thread_context  ctx ;

        thread t1 ;
        t1.SetFunctionEntry( functionptr.FindFunction("func1(thread_context)") );
        t1.Start( ctx );

        thread t2 ;
        t2.SetFunctionEntry( functionptr.FindFunction("func2(thread_context)") );
        t2.Start( ctx );

        system.SleepSeconds( 2 );
        stdout.Println( "main - Broadcast" );
        ctx.lock.Broadcast();

        t1.Join();
        t2.Join();

        return 0;
}
```

执行

```
zlang test_thread_condsig_3_2.z
func1 - Wait ...
func2 - Wait ...
main - Broadcast
func1 - Wait done , b[true]
func2 - Wait done , b[true]
```

### 内存屏障

内存屏障用于多个线程都执行到某条语句时再一起向下执行的场景。

内存屏障对象名：`barrier`

| 成员类型 | 成员名字和原型          | 说明               | (错误.)(异常码,)异常消息                                     |
| -------- | ----------------------- | ------------------ | ------------------------------------------------------------ |
| 函数     | bool SetWaitCount(int); | 设置最大同时等待数 | EXCEPTION_MESSAGE_INIT_BARRIER_FAILED                        |
| 函数     | bool Wait();            | 进入等待           | EXCEPTION_MESSAGE_BARRIER_NOT_INIT<br />EXCEPTION_MESSAGE_WAIT_BARRIER_FAILED |

代码示例：`test_thread_barrier_1.z`

该代码示范了多个线程都执行到`Wait`处，再继续一起往下执行的同步场景。

```
import stdtypes stdio thread system ;

object thread_context
{
        barrier         lock ;
}

function void func1( thread_context ctx )
{
        stdout.Println( "func1 - begin" );
        system.SleepSeconds( 2 );

        ctx.lock.Wait();
        stdout.Println( "func1 - end" );

        return;
}

function void func2( thread_context ctx )
{
        stdout.Println( "func2 - begin" );
        system.SleepSeconds( 2 );

        ctx.lock.Wait();
        stdout.Println( "func2 - end" );

        return;
}

function int main( array args )
{
        thread_context  ctx ;

        ctx.lock.SetWaitCount(2);

        thread t1 ;
        t1.SetFunctionEntry( functionptr.FindFunction("func1(thread_context)") );
        t1.Start( ctx );

        thread t2 ;
        t2.SetFunctionEntry( functionptr.FindFunction("func2(thread_context)") );
        t2.Start( ctx );

        t1.Join();
        t2.Join();

        return 0;
}
```

执行

```
$ zlang test_thread_barrier_1.z
func1 - begin
func2 - begin
func2 - end
func1 - end
```



下一章：[网络](network.md)