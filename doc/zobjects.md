上一章：[zlang命令行参数](zlang_parameters.md)



**章节目录**

- [第二部分：基础对象库](#第二部分基础对象库)

  


# 第二部分：基础对象库

`zlang`自带了一套常用对象库供开发人员直接使用，这些对象库都是用C编写、封装成对象提供出来。

对象库列表：

| 对象库名 | 对象库说明                                                   |
| -------- | ------------------------------------------------------------ |
| stdtypes | 基本值对象和集合对象库，包括short、ushort、int、uint、long、ulong、float、double、bool、string、functionp、array、list、queue、stack、map、iterator。 |
| stdio    | 输入输出对象库，包括stdin、stdout、stderr、stdfile、execmd。 |
| system   | 操作系统特性处理对象，包含Sleep等函数。                      |
| file     | path路径和目录名管理对象、file文件名关联对象。               |
| thread   | 用于线程属性管理、启动线程、合并线程等的线程对象库。         |
| network  | 网络对象库，包括TCP、HTTP、HTTPClient（发起HTTP请求）、HTTPServer（可用于静态资源管理、HTTP服务、RESTful服务）、HTMLSection（HTML模板实例库）。 |
| datetime | 日期时间对象库，包括datetime对象。                           |
| random   | 随机数对象。                                                 |
| encoding | 编码对象，目前支持可代替uuid的gus_uuid。                     |
| log      | 日志对象库，包括写日志（log）、写日志集对象（logs）。        |
| database | 数据库访问对象库，包括数据库对象（database）、会话对象（dbsession）、结果集对象（dbresult）。 |
| redis    | Redis处理对象库。                                            |
| json     | JSON序列化/反序列化工具对象。                                |
| xml      | XML序列化/反序列化工具对象。                                 |
| trace    | 打印内部内存、调用栈等信息的调试对象。                       |

以上对象库源码也在`zlang`源代码中，解释器源码在`src/zlang/`，自带对象库源码在`src/zobject/(对象库名)/`，编译后安装到`$HOME/lib/libzlang_对象库名.so`，`zlang`代码中用关键字`import`导入对象库名，即可直接使用对象库中的对象。

建议：除了工具对象外，建议不要直接使用其他对象，而是用对象克隆出新的对象后使用新对象。



下一章：[数学计算](math.md)