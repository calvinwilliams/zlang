上一章：[随机数](random.md)



**章节目录**
- [编码](#编码)
  - [gus\_uuid](#gus_uuid)
  - [base64](#base64)
  - [mbcs](#mbcs)



# 编码

编码对象库用于应用中对数据的各种编码和解码。

目前只提供了一种UUID编码，后面会再补充。

## gus_uuid

gus_uuid编码对象：`gus_uuid`

zlang编码对象库基于我的开源库`libgus`。

| 成员类型 | 成员名字和原型     | 说明                       | (错误.)(异常码,)异常消息             |
| :------: | ------------------ | -------------------------- | ------------------------------------ |
|   函数   | string Generate(); | 生成一个GUS_UUID编码并返回 | EXCEPTION_MESSAGE_GUSGENERATE_FAILED |

`gus_uuid`编码是类似`uuid`的无输入生成唯一串的编码格式，编码共128字节，十六进制展开后是256字符。可用作数据库表主键等场合。

编码格式如下：

| 段号 |          段名           | 说明                                                         |
| :--: | :---------------------: | ------------------------------------------------------------ |
|  1   |   网络地址（NETADDR）   | 14位全1(0x3FFF)+IPv4地址的32位数字形式(非回绕地址；大端网络字节序) |
|  2   |      线程ID（TID）      | 30位整型，Linux里的TID可通过"syscall(SYS_gettid)"获得，类型为pid_t；<br /> WINDOWS里的ThreadId，通过GetCurrentThreadId()获得，取其1/4，类型为DWORD；小端字节序 |
|  3   | 日期时间戳（TIMESTAMP） | 49位微秒戳，从2020年到现在的微秒数，可使用17年（至2037年）；小端字节序 |
|  4   |      序号（SEQNO）      | 3位微秒内序号，取值空间为"[0,7]"，每微秒最多排出8个序列号，如果计算机实在太强劲而超出了，原地沉睡到下一微秒；小端字节序 |

代码示例：`test_encoding_1_1.z`

该代码示范了生成唯一GUS UUID编码。

```
import stdtypes stdio encoding ;

function int main( array args )
{
        stdout.Println( gus_uuid.Generate() );

        return 0;
}
```

执行

```
$ zlang test_encoding_1_1.z
FFFD3C2EA300000446F3C84D0F441608
```

## base64

用于对字符串进行base64编码和解码。实现依赖开源库`openssl`。

对象名：`base64`

| 成员类型 | 成员名字和原型              | 说明                                                       | (错误.)(异常码,)异常消息                 |
| :------: | --------------------------- | ---------------------------------------------------------- | ---------------------------------------- |
|   函数   | string Encode(string from); | 对from进行编码，结果通过返回值返回，如果编码失败则返回null | EXCEPTION_MESSAGE_EVP_ENCODEBLOCK_FAILED |
|   函数   | string Decode(string from); | 对from进行解码，结果通过返回值返回，如果编码失败则返回null | EXCEPTION_MESSAGE_EVP_DECODEBLOCK_FAILED |

代码示例：`test_encoding_2_1.z`

该代码示范了对字符串进行`base64`编解码处理。

```
import stdtypes stdio encoding ;

function int main( array args )
{
        string A_base64 = base64.Encode("A") ;
        stdout.FormatPrintln( "A_base64[%{A_base64}]" );
        string A = base64.Decode( A_base64 ) ;
        stdout.FormatPrintln( "A[%{A}]" );

        string AB_base64 = base64.Encode("AB") ;
        stdout.FormatPrintln( "AB_base64[%{AB_base64}]" );
        string AB = base64.Decode( AB_base64 ) ;
        stdout.FormatPrintln( "AB[%{AB}]" );

        string ABC_base64 = base64.Encode("ABC") ;
        stdout.FormatPrintln( "ABC_base64[%{ABC_base64}]" );
        string ABC = base64.Decode( ABC_base64 ) ;
        stdout.FormatPrintln( "ABC[%{ABC}]" );

        string ABCD_base64 = base64.Encode("ABCD") ;
        stdout.FormatPrintln( "ABCD_base64[%{ABCD_base64}]" );
        string ABCD = base64.Decode( ABCD_base64 ) ;
        stdout.FormatPrintln( "ABCD[%{ABCD}]" );

        return 0;
}
```

执行

```
$ zlang test_encoding_2_1.z
A_base64[QQ==]
A[A]
AB_base64[QUI=]
AB[AB]
ABC_base64[QUJD]
ABC[ABC]
ABCD_base64[QUJDRA==]
ABCD[ABCD]
```

## 十六进制字符串

用于字节串（可见字符+不可见字符）和十六进制字符串（每一个字符都是`0~9或a~z`的字符）之间相互转换。

对象名：`hexstr`

| 成员类型 | 成员名字和原型            | 说明                                   | (错误.)(异常码,)异常消息           |
| :------: | ------------------------- | -------------------------------------- | ---------------------------------- |
|   函数   | string HexExpand(string); | 把输入字节串展开成十六进制字符串并返回 |                                    |
|   函数   | string HexFold(string);   | 把十六进制字符串合拢成字节串并返回     | EXCEPTION_MESSAGE_DATA_LEN_INVALID |

代码示例：`test_encoding_3_1.z`

```
import stdtypes stdio encoding ;

function int main( array args )
{
        string A_hex = hexstr.HexExpand("A") ;
        stdout.FormatPrintln( "A_hex[%{A_hex}]" );
        string A = hexstr.HexFold( A_hex ) ;
        stdout.FormatPrintln( "A[%{A}]" );

        string AB_hex = hexstr.HexExpand("AB") ;
        stdout.FormatPrintln( "AB_hex[%{AB_hex}]" );
        string AB = hexstr.HexFold( AB_hex ) ;
        stdout.FormatPrintln( "AB[%{AB}]" );

        string ABC_hex = hexstr.HexExpand("ABC") ;
        stdout.FormatPrintln( "ABC_hex[%{ABC_hex}]" );
        string ABC = hexstr.HexFold( ABC_hex ) ;
        stdout.FormatPrintln( "ABC[%{ABC}]" );

        string ABCD_hex = hexstr.HexExpand("ABCD") ;
        stdout.FormatPrintln( "ABCD_hex[%{ABCD_hex}]" );
        string ABCD = hexstr.HexFold( ABCD_hex ) ;
        stdout.FormatPrintln( "ABCD[%{ABCD}]" );

        return 0;
}
```

执行

```
$ zlang test_encoding_3_1.z
A_hex[41]
A[A]
AB_hex[4142]
AB[AB]
ABC_hex[414243]
ABC[ABC]
ABCD_hex[41424344]
ABCD[ABCD]
```

## mbcs

用于不同字符编码集的多字节字之间相互转换。

对象名：`mbcs`

| 成员类型 | 成员名字和原型                                               | 说明                                                         | (错误.)(异常码,)异常消息                         |
| :------: | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------ |
|   函数   | string ConvertEncoding(string from_encoding,string to_encoding,string from_text); | 把字符编码from_encoding的字符串from_text转换成字符编码to_encoding | EXCEPTION_MESSAGE_CONVERT_STRING_ENCODING_FAILED |

下一章：[日志](log.md)