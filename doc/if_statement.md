上一章：[关键字](keyword.md)



**章节目录**
- [条件语句](#条件语句)
	- [条件判断if](#条件判断if)
	- [分支判断switch](#分支判断switch)



# 条件语句

条件语句用于代码中依条件而执行不同的语句，一般形态为`如果条件为真则执行...否则执行...`。

## 条件判断if

条件判断语句的形式为：

```
if ( 条件1 )
	条件1为真时执行的语句;
```

```
if ( 条件2 )
	条件2为真时执行的语句;
else
	条件2为假时执行的语句;
```

```
if ( 条件3 )
	条件3为真时执行的语句;
else if( 条件4)
	条件4为真时执行的语句;
else
	条件3为假、条件4也为假时执行的语句;
```

```
if ( 条件5 )
{
	条件5为真时执行的语句块;
}
```

```
if( 条件6 )
{
	条件6为真时执行的语句块;
}
else
{
	条件6为假时执行的语句块;
}
```

```
if( 条件6 )
{
	条件6为真时执行的语句块;
}
else if( 条件7 )
{
	条件7为真时执行的语句块;
}
else
{
	条件6为假、条件7也为假时执行的语句块;
}
```

注意：条件可以是逻辑判断`i == 1`，也可以是布尔型对象的值`bool b = true ; if( b ) ...`，也可以是数值型对象的值`int n = 0 ; if( n ) ...`异或函数指针是否为空`functionp func = functionp.FindFunction( "func1(string) ; if( func ) ..."`

注意：条件可以由逻辑与/或连接多个子条件组合而成`if( n == 1 && func )` ...、`if( str == "Saturday" || str == "Sunday" ) ...`

代码示例：`test_if_1_1.z`

```
import stdtypes stdio ;

function int main( array args )
{
        i = 1 ;

        if( i == 1 )
                stdout.Println( "yes" );
        else
                stdout.Println( "no" );

        return 0;
}
```

执行

```
$ zlang test_if_1_1.z
yes
```

代码示例：`test_if_11_1.z`

```
import stdtypes stdio ;

function int main( array args )
{
        i = 1 ;
        str = "hello" ;

        if( i == 1 && str == "hello" )
        {
                stdout.Println( "yes" );
        }
        else
        {
                stdout.Println( "no" );
        }

        return 0;
}
```

执行

```
$ zlang test_if_11_1.z
yes
```

## 分支判断switch

分支判断语句的形式为：

```
switch ( 对象 )
{
	case 值1:
		对象值等于值1时执行的语句;
		break;
	case 值2:
		对象值等于值2时执行的语句;
		break;
	default:
		以上条件都不符合时执行的语句;
		break;
}
```

代码示例：`test_switch_1_1.z`

```
import stdtypes stdio ;

function int main( array args )
{
	int	i = 2 ;
	
	switch( i )
	{
		case 1:
			stdout.Println( "1" );
			break;
		case 2:
			stdout.Println( "2" );
			break;
		case 3:
			stdout.Println( "3" );
			break;
		default:
			stdout.Println( "default" );
	}
	
	return 0;
}
```

执行

```
$ zlang test_switch_1_1.z
2
```



下一章：[循环语句](while_statement.md)
