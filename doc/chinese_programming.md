上一章：[爬虫开发](web_crawler.md)

- [中文编程](#中文编程)
	- [一些示例](#一些示例)
	- [官方自带的一个Web框架`zcrypto`](#官方自带的一个web框架zcrypto)
		- [关于`zcrypto`](#关于zcrypto)
		- [开发者可以通过这个开源项目](#开发者可以通过这个开源项目)
		- [项目目录说明](#项目目录说明)
		- [运行说明](#运行说明)
		- [源码解读](#源码解读)


# 中文编程

`zlang`支持中文编程，中文使用范围包括[关键字](keyword.md)、函数名、对象名、变量名，其中自带对象库中的对象名、属性和函数已标配中文。

中文代码文件最前面必须指明字符集，如`charset "zh_CN.gb18030"`或`charset "zh_CN.UTF8"`，文件内中文字符编码符合命令`locale -a`的值。

关键字中文见 [关键字](keyword.md)，基础类型中文见 [字面量、值对象和集合对象](stdtypes.md) 中的字面量表中的类型名。

## 一些示例

代码示例：`test_factorial_chinese.z`

```
charset "zh_CN.gb18030"

导入 stdtypes stdio ;

函数 整型 阶乘计算( 整型 输入数 )
{
        如果( 输入数 == 1 )
                返回 1;
        否则
                返回 阶乘计算(输入数-1) * 输入数 ;
}

函数 整型 主函数( 数组 命令行参数集 )
{
        值 = 阶乘计算( 6 ) ;
        标准输出.显示并换行( 值 );

        返回 0;
}
```

执行

```
$ zlang test_factorial_chinese.z
720
```

代码示例：`test_object_chinese_31.z`

```
charset "zh_CN.gb18030"

导入 stdtypes stdio ;

接口 动物
{
        字符串  名字 ;

        函数 无参 设置战士名( 字符串 名字 )
        {
                本对象.名字 = 名字 + " 战士" ;
        }

        函数 无参 叫();
        函数 无参 吃( 字符串 食物 );
}

对象 猫 实现 动物
{
        函数 无参 叫()
        {
                标准输出.显示并换行( "喵喵喵~" );
        }

        函数 无参 吃( 字符串 食物 )
        {
                如果( 食物 == "鱼" )
                        标准输出.显示并换行( ":)" );
                否则
                        标准输出.显示并换行( ":(" );
        }
}

对象 狗 实现 动物
{
        函数 无参 叫()
        {
                标准输出.显示并换行( "汪汪汪~" );
        }

        函数 无参 吃( 字符串 食物 )
        {
                如果( 食物 == "骨头" )
                        标准输出.显示并换行( ":>" );
                否则
                        标准输出.显示并换行( ":<" );
        }
}

函数 整型 主函数( 数组 命令行参数集 )
{
        猫      我的猫 ;

        我的猫.设置战士名( "阿华" );
        标准输出.显示并换行( 我的猫.名字 );
        我的猫.叫();
        我的猫.吃( "鱼" );
        我的猫.吃( "骨头" );

        狗      我的狗 ;

        我的狗.设置战士名( "小黑" );
        标准输出.显示并换行( 我的狗.名字 );
        我的狗.叫();
        我的狗.吃( "骨头" );
        我的狗.吃( "鱼" );

        返回 0;
}
```

执行

```
$ zlang test_object_chinese_31.z
阿华 战士
喵喵喵~
:)
:(
小黑 战士
汪汪汪~
:>
:<
```

代码示例：`test_object_chinese_41.z`

```
charset "zh_CN.gb18030"

导入 stdtypes stdio ;

对象 猫
{
        私有的 字符串           名字 ;

        函数 猫()
        {
                标准输出.显示并换行( "猫()" );
                返回;
        }

        函数 猫( 字符串 名字 )
        {
                标准输出.格式化显示并换行( "猫(\"%{名字}\")" );
                设置名字( 名字 );
                返回;
        }

        函数 ~猫()
        {
                标准输出.显示并换行( "~猫()" );
                本对象.名字 = "" ;
                返回;
        }

        函数 无参 设置名字( 字符串 名字 )
        {
                标准输出.格式化显示并换行( "设置名字(\"%{名字}\")" );
                本对象.名字 = 名字 ;
                返回;
        }

        函数 无参 打印名字()
        {
                标准输出.显示并换行( 本对象.名字 );
                返回;
        }
}

函数 整型 主函数( 数组 命令行参数集 )
{
        标准输出.显示并换行( "--- 定义 大黑 ---" );

        猫      大黑 ;

        标准输出.显示并换行( "--- 定义 小白 ---" );

        猫      小白( "小白" ) ;

        标准输出.显示并换行( "--- 使用 大黑 ---" );

        大黑.设置名字( "大黑" );
        大黑.打印名字();

        标准输出.显示并换行( "--- 使用 小白 ---" );

        小白.打印名字();

        返回 0;
}
```

执行

```
$ zlang test_object_chinese_41.z
--- 定义 大黑 ---
猫()
--- 定义 小白 ---
猫("小白")
设置名字("小白")
--- 使用 大黑 ---
设置名字("大黑")
大黑
--- 使用 小白 ---
小白
~猫()
~猫()
```

## 官方自带的一个Web框架`zcrypto`

### 关于`zcrypto`

[`zcrypto`](https://gitee.com/calvinwilliams/zcrypto)是一个`zlang`语言开发的开源项目，它采用中文编程的方式，实现了一个加解密工具软件，支持

* 摘要算法`MD2`、`MD4`、`MD5`、`SHA1`、`SHA224`、`SHA256`、`SHA384`、`SHA512`
* 对称加解密算法`DES`、`3DES`的`ECB`、`CBC`模式
* 非对称加解密算法和签名验签算法`RSA`
* 将来会很容易的支持国密算法`SM2`、`SM3`、`SM4`
* 支持输入数据从裸字符串、十六进制展开字符串、BASE64字符串、从文本文件中读取，支持输出数据显示裸字符串、十六进制展开字符串、BASE64字符串、写到文本文件

### 开发者可以通过这个开源项目

1. 感受用`zlang`编写关键字、变量名、函数名都使用中文的程序代码；
2. 学习如何用`zlang`开发命令行工具软件；

### 项目目录说明

​	`zlang`源码文件都放在`src/`目录里；

​	快捷脚本文件在`shbin/`目录里；

### 运行说明

​	不参数的运行工具软件则显示所有支持的算法

```
$ cd shbin/
$ zcrypto.sh
USAGE : zcrypto.sh -v
                   -a:(MD2|MD4|MD5|SHA1|SHA224|SHA256|SHA384|SHA512) -i:(TEXT|HEXSTR|BASE64|FILE) (INPUT_DATA) --salt:(TEXT|HEXSTR|BASE64|FILE) (SALT_DATA) -o:(TEXT|BINARY|HEXSTR|BASE64)
                   -a:(DES|TRIPLEDES).(ENCRYPT|DECRYPT) -m:(ECB|CBC) -k:(TEXT|HEXSTR|BASE64|FILE) (KEY_DATA) -i:(TEXT|HEXSTR|BASE64|FILE) (INPUT_DATA) --padding:(ZERO|PKCS7) -o:(TEXT|BINARY|HEXSTR|BASE64)
                   -a:RSA.GENERATE_KEY --key-len-bytes (512|1024|2048|4096) --e (3|F4) --export-privkey-pem privkey_pem_filename --export-pubkey-pem pubkey_pem_filename --export-privkey-der privkey_der_filename --export-pubkey-der pubkey_der_filename
                   -a:RSA.ENCRYPT --pubkey-pem pubkey_pem_filename|--pubkey-der pubkey_der_filename -i:(TEXT|HEXSTR|BASE64|FILE) (INPUT_DATA) --padding:(NO|PKCS7|PKCS7_OAEP) -o:(TEXT|BINARY|HEXSTR|BASE64)
                   -a:RSA.DECRYPT --privkey-pem privkey_pem_filename|--privkey-der privkey_der_filename -i:(TEXT|HEXSTR|BASE64|FILE) (INPUT_DATA) --padding:(NO|PKCS7|PKCS7_OAEP) -o:(TEXT|BINARY|HEXSTR|BASE64)
                   -a:RSA.SIGN --digest:(MD5|SHA1|SHA224|SHA256|SHA384|SHA512) -i:(TEXT|HEXSTR|BASE64|FILE) (INPUT_DATA) --privkey-pem privkey_pem_filename|--privkey-der privkey_der_filename -o:(TEXT|BINARY|HEXSTR|BASE64)
                   -a:RSA.VERIFY --digest:(MD5|SHA1|SHA224|SHA256|SHA384|SHA512) -i:(TEXT|HEXSTR|BASE64|FILE) (INPUT_DATA) --sign:(TEXT|HEXSTR|BASE64|FILE) (SIGN_DATA) --pubkey-pem pubkey_pem_filename|--pubkey-der pubkey_der_filename
```

​	计算MD5摘要

```
$ zcrypto.sh -a:MD5 -i:TEXT abc -o:HEXSTR
900150983CD24FB0D6963F7D28E17F72
```

​	计算`cbc`模式的`3des`加密，填充模式为`pkcs7`

```
$ zcrypto.sh -a:TRIPLEDES.ENCRYPT -m:CBC -k:TEXT 123456781234567812345678 -i:TEXT abc --padding:PKCS7 -o:HEXSTR
C24B98FAD5C0580E
```

 生成一对RSA密钥，并导出公钥和私钥为PEM格式文件和DER格式文件

```
$ zcrypto.sh -a:RSA.GENERATE_KEY --key-len-bytes 4096 --export-prikey-pem /tmp/zcrypto.prikey.pem --export-pubkey-pem /tmp/zcrypto.pubkey.pem --export-prikey-der /tmp/zcrypto.prikey.der --export-pubkey-der /tmp/zcrypto.pubkey.der
```

利用刚才生成的私钥文件对字符串做签名

```
$ zcrypto.sh -a:RSA.SIGN --digest:SHA1 -i:TEXT abc --prikey-pem /tmp/zcrypto.prikey.pem -o:BINARY > /tmp/zcrypto.rsa.sign
```

再用公钥文件对字符串和签名做验签

```
$ zcrypto.sh -a:RSA.VERIFY --digest:SHA1 -i:TEXT abc --sign:FILE /tmp/zcrypto.rsa.sign --pubkey-pem /tmp/zcrypto.pubkey.pem
验签成功
```

### 源码解读

工具软件只有一个源代码文件`zcrypto.z`

```
charset "zh_CN.UTF8"

导入 stdtypes stdio encoding crypto ;

函数 无参 显示命令行参数说明()
{
	标准输出.显示并换行( "USAGE : zcrypto.sh -v" );
	标准输出.显示并换行( "                   -a:(MD2|MD4|MD5|SHA1|SHA224|SHA256|SHA384|SHA512) -i:(TEXT|HEXSTR|BASE64|FILE) (INPUT_DATA) --salt:(TEXT|HEXSTR|BASE64|FILE) (SALT_DATA) -o:(TEXT|BINARY|HEXSTR|BASE64)" );
	标准输出.显示并换行( "                   -a:(DES|TRIPLEDES).(ENCRYPT|DECRYPT) -m:(ECB|CBC) -k:(TEXT|HEXSTR|BASE64|FILE) (KEY_DATA) -i:(TEXT|HEXSTR|BASE64|FILE) (INPUT_DATA) --padding:(ZERO|PKCS7) -o:(TEXT|BINARY|HEXSTR|BASE64)" );
	标准输出.显示并换行( "                   -a:RSA.GENERATE_KEY --key-len-bytes (512|1024|2048|4096) --e (3|F4) --export-privkey-pem privkey_pem_filename --export-pubkey-pem pubkey_pem_filename --export-privkey-der privkey_der_filename --export-pubkey-der pubkey_der_filename" );
	标准输出.显示并换行( "                   -a:RSA.ENCRYPT --pubkey-pem pubkey_pem_filename|--pubkey-der pubkey_der_filename -i:(TEXT|HEXSTR|BASE64|FILE) (INPUT_DATA) --padding:(NO|PKCS7|PKCS7_OAEP) -o:(TEXT|BINARY|HEXSTR|BASE64)" );
	标准输出.显示并换行( "                   -a:RSA.DECRYPT --privkey-pem privkey_pem_filename|--privkey-der privkey_der_filename -i:(TEXT|HEXSTR|BASE64|FILE) (INPUT_DATA) --padding:(NO|PKCS7|PKCS7_OAEP) -o:(TEXT|BINARY|HEXSTR|BASE64)" );
	标准输出.显示并换行( "                   -a:RSA.SIGN --digest:(MD5|SHA1|SHA224|SHA256|SHA384|SHA512) -i:(TEXT|HEXSTR|BASE64|FILE) (INPUT_DATA) --privkey-pem privkey_pem_filename|--privkey-der privkey_der_filename -o:(TEXT|BINARY|HEXSTR|BASE64)" );
	标准输出.显示并换行( "                   -a:RSA.VERIFY --digest:(MD5|SHA1|SHA224|SHA256|SHA384|SHA512) -i:(TEXT|HEXSTR|BASE64|FILE) (INPUT_DATA) --sign:(TEXT|HEXSTR|BASE64|FILE) (SIGN_DATA) --pubkey-pem pubkey_pem_filename|--pubkey-der pubkey_der_filename" );
}

函数 无参 显示版本号()
{
	标准输出.显示并换行( "zcrypto.sh v0.0.1.0" );
}

函数 整型 主函数( 数组 命令行参数集 )
{
	如果( 命令行参数集.长度() == 0 )
	{
		显示命令行参数说明();
		返回 0;
	}
	否则 如果( 命令行参数集.长度() == 1 && 命令行参数集.得到(1) == "-v" )
	{
		显示版本号();
		返回 0;
	}
	
	// 声明本地变量
	字符串	算法和方法 ;
	字符串	算法 ;
	字符串	方法 ;
	字符串	模式 ;
	字符串	摘要算法 ;
	字符串	输入格式 ;
	字符串	输入数据 ;
	字符串	盐格式 ;
	字符串	盐数据 ;
	字符串	签名格式 ;
	字符串	签名数据 ;
	字符串	密钥格式 ;
	字符串	密钥数据 ;
	字符串	密钥长度 ;
	字符串	e ;
	字符串	导出私钥PEM文件名 ;
	字符串	导出公钥PEM文件名 ;
	字符串	导出私钥DER文件名 ;
	字符串	导出公钥DER文件名 ;
	字符串	私钥PEM文件名 ;
	字符串	公钥PEM文件名 ;
	字符串	私钥DER文件名 ;
	字符串	公钥DER文件名 ;
	字符串	填充格式 ;
	字符串	输出格式 ;
	字符串	输出数据 ;
	字符串	上一个命令行参数 ;
	
	// 解析所有命令行参数和值
	上一个命令行参数 = "" ;
	foreach( 字符串 命令行参数 in 命令行参数集 )
	{
		如果( 上一个命令行参数.是否匹配开始( "-k" ) )
		{
			密钥数据 = 命令行参数 ;
		}
		否则 如果( 上一个命令行参数.是否匹配开始( "-i" ) )
		{
			输入数据 = 命令行参数 ;
		}
		否则 如果( 上一个命令行参数.是否匹配开始( "--sign" ) )
		{
			签名数据 = 命令行参数 ;
		}
		否则 如果( 上一个命令行参数.是否匹配开始( "--salt" ) )
		{
			盐数据 = 命令行参数 ;
		}
		否则 如果( 上一个命令行参数 == "--key-len-bytes" )
		{
			密钥长度 = 命令行参数 ;
		}
		否则 如果( 上一个命令行参数 == "--e" )
		{
			e = 命令行参数 ;
		}
		否则 如果( 上一个命令行参数 == "--export-prikey-pem" )
		{
			导出私钥PEM文件名 = 命令行参数 ;
		}
		否则 如果( 上一个命令行参数 == "--export-pubkey-pem" )
		{
			导出公钥PEM文件名 = 命令行参数 ;
		}
		否则 如果( 上一个命令行参数 == "--export-prikey-der" )
		{
			导出私钥DER文件名 = 命令行参数 ;
		}
		否则 如果( 上一个命令行参数 == "--export-pubkey-der" )
		{
			导出公钥DER文件名 = 命令行参数 ;
		}
		否则 如果( 上一个命令行参数 == "--prikey-pem" )
		{
			私钥PEM文件名 = 命令行参数 ;
		}
		否则 如果( 上一个命令行参数 == "--pubkey-pem" )
		{
			公钥PEM文件名 = 命令行参数 ;
		}
		否则 如果( 上一个命令行参数 == "--prikey-der" )
		{
			私钥DER文件名 = 命令行参数 ;
		}
		否则 如果( 上一个命令行参数 == "--pubkey-der" )
		{
			公钥DER文件名 = 命令行参数 ;
		}
		否则 如果( 命令行参数.是否匹配开始( "-a" ) )
		{
			算法和方法 = 命令行参数.取子字符串( 4 , 命令行参数.Length() ) ;
			如果( 算法和方法.查找字符串位置( "." ) > 0 )
				算法和方法.正则提取( '''([_A-Z]+)\.([_A-Z]+)''' , 算法 , 方法 ) ;
			否则
				算法 = 算法和方法 ;
		}
		否则 如果( 命令行参数.是否匹配开始( "-m" ) )
		{
			模式 = 命令行参数.取子字符串( 4 , 命令行参数.Length() ) ;
		}
		否则 如果( 命令行参数.是否匹配开始( "--digest" ) )
		{
			摘要算法 = 命令行参数.取子字符串( 10 , 命令行参数.Length() ) ;
		}
		否则 如果( 命令行参数.是否匹配开始( "--sign" ) )
		{
			签名格式 = 命令行参数.取子字符串( 8 , 命令行参数.Length() ) ;
		}
		否则 如果( 命令行参数.是否匹配开始( "-k" ) )
		{
			密钥长度类型 = 命令行参数.取子字符串( 4 , 命令行参数.Length() ) ;
		}
		否则 如果( 命令行参数.是否匹配开始( "-i" ) )
		{
			输入格式 = 命令行参数.取子字符串( 4 , 命令行参数.Length() ) ;
		}
		否则 如果( 命令行参数.是否匹配开始( "--sign" ) )
		{
			签名格式 = 命令行参数.取子字符串( 8 , 命令行参数.Length() ) ;
		}
		否则 如果( 命令行参数.是否匹配开始( "--padding" ) )
		{
			填充格式 = 命令行参数.取子字符串( 11 , 命令行参数.Length() ) ;
		}
		否则 如果( 命令行参数.是否匹配开始( "--salt" ) )
		{
			盐格式 = 命令行参数.取子字符串( 8 , 命令行参数.Length() ) ;
		}
		否则 如果( 命令行参数 == "--key-len-bytes" || 命令行参数 == "--e" || 命令行参数 == "--export-prikey-pem" || 命令行参数 == "--export-pubkey-pem" || 命令行参数 == "--export-prikey-der" || 命令行参数 == "--export-pubkey-der" )
		{
			;
		}
		否则 如果( 命令行参数 == "--prikey-pem" || 命令行参数 == "--pubkey-pem" || 命令行参数 == "--prikey-der" || 命令行参数 == "--pubkey-der" )
		{
			;
		}
		否则 如果( 命令行参数.是否匹配开始("-o") )
		{
			输出格式 = 命令行参数.取子字符串( 4 , 命令行参数.Length() ) ;
		}
		否则
		{
			标准输出.格式化显示并换行( "*** 错误 : 未知的命令行参数 [%{命令行参数}]" );
			返回 1;
		}
		
		上一个命令行参数 = 命令行参数 ;
	}
	
	// 检查输入数据格式和值
	如果( 输入格式 != "" && 输入数据 != "" )
	{
		如果( 输入格式 == "TEXT" )
			;
		否则 如果( 输入格式 == "HEXSTR" )
			输入数据 = 十六进制字符串.十六进制合拢( 输入数据 ) ;
		否则 如果( 输入格式 == "BASE64" )
			输入数据 = base64.十六进制合拢( 输入数据 ) ;
		否则 如果( 输入格式 == "FILE" )
		{
			输入数据 = 标准文件.读文件到字符串( 输入数据 ) ;
			if( 输入数据 == 空 )
			{
				标准输出.格式化显示并换行( "*** 错误 : -i:FILE 找不到文件" );
				返回 1;
			}
		}
		否则
		{
			标准输出.格式化显示并换行( "*** 错误 : -i:%{输入格式} 无效" );
			返回 1;
		}
	}
	
	// 如果输入盐，转换成统一格式
	如果( 盐格式 != "" && 盐数据 != "" )
	{
		如果( 盐格式 == "TEXT" )
			;
		否则 如果( 盐格式 == "HEXSTR" )
			盐数据 = 十六进制字符串.十六进制合拢( 盐数据 ) ;
		否则 如果( 盐格式 == "BASE64" )
			盐数据 = base64.解码( 盐数据 ) ;
		否则 如果( 盐格式 == "FILE" )
		{
			盐数据 = 标准文件.读文件到字符串( 盐数据 ) ;
			if( 盐数据 == 空 )
			{
				标准输出.格式化显示并换行( "*** 错误 : --salt:FILE 找不到文件" );
				返回 1;
			}
		}
		否则
		{
			标准输出.格式化显示并换行( "*** 错误 : --salt:%{盐格式} 无效" );
			返回 1;
		}
	}
	
	// 如果输入签名值，转换成统一格式
	如果( 签名格式 != "" && 签名数据 != "" )
	{
		如果( 签名格式 == "TEXT" )
			;
		否则 如果( 签名格式 == "HEXSTR" )
			签名数据 = 十六进制字符串.十六进制合拢( 签名数据 ) ;
		否则 如果( 签名格式 == "BASE64" )
			签名数据 = base64.解码( 签名数据 ) ;
		否则 如果( 签名格式 == "FILE" )
		{
			签名数据 = 标准文件.读文件到字符串( 签名数据 ) ;
			if( 签名数据 == 空 )
			{
				标准输出.格式化显示并换行( "*** 错误 : --sign:FILE 找不到文件" );
				返回 1;
			}
		}
		否则
		{
			标准输出.格式化显示并换行( "*** 错误 : --sign:%{签名格式} 无效" );
			返回 1;
		}
	}
	
	// 如果输入密钥，转换成统一格式
	如果( 密钥格式 != "" && 密钥数据 != "" )
	{
		如果( 密钥格式 == "TEXT" )
			;
		否则 如果( 密钥格式 == "HEXSTR" )
			密钥数据 = 十六进制字符串.十六进制合拢( 密钥数据 ) ;
		否则 如果( 密钥格式 == "BASE64" )
			密钥数据 = base64.解码( 密钥数据 ) ;
		否则 如果( 密钥格式 == "FILE" )
		{
			密钥数据 = 标准文件.读文件到字符串( 密钥数据 ) ;
			if( 密钥数据 == 空 )
			{
				标准输出.格式化显示并换行( "*** 错误 : -k:FILE 找不到文件" );
				返回 1;
			}
		}
		否则
		{
			标准输出.格式化显示并换行( "*** 错误 : -k:%{密钥格式} 无效" );
			返回 1;
		}
	}
	
	// 如果算法是散列
	如果( 算法 == "MD2" || 算法 == "MD4" || 算法 == "MD5" || 算法 == "SHA1" || 算法 == "SHA224" || 算法 == "SHA256" || 算法 == "SHA384" || 算法 == "SHA512" )
	{
		// zcrypto.sh -a:MD5 -i:TEXT abc -o:HEXSTR
		// zcrypto.sh -a:SHA256 -i:TEXT abc --salt:TEXT xyz -o:HEXSTR
		
		如果( 输入数据 == "" )
		{
			标准输出.格式化显示并换行( "*** 错误 : -a %{算法} 没有输入数据" );
			返回 1;
		}
		
		算法.转换为小写();
		如果( 盐格式 == "" )
			输出数据 = ```算法```.摘要( 输入数据 ) ; // 使用三引号，取出变量算法的值，作为中间变量的名字，调用中间变量的函数Digest
		否则
			输出数据 = ```算法```.摘要( 输入数据 , 盐数据 ) ;
	}
	// 如果算法是对称DES族
	否则 如果( 算法 == "DES" || 算法 == "TRIPLEDES" )
	{
		// zcrypto.sh -a:DES.ENCRYPT -k:TEXT 12345678 -i:TEXT abc --padding:ZERO -o:HEXSTR
		// zcrypto.sh -a:DES.DECRYPT -k:TEXT 12345678 -i:HEXSTR 2C8369311A2E38FA --padding:ZERO -o:TEXT
		
		// zcrypto.sh -a:TRIPLEDES.ENCRYPT -m:CBC -k:TEXT 123456781234567812345678 -i:TEXT abc --padding:PKCS7 -o:HEXSTR
		// zcrypto.sh -a:TRIPLEDES.DECRYPT -m:CBC -k:TEXT 123456781234567812345678 -i:HEXSTR C24B98FAD5C0580E --padding:PKCS7 -o:TEXT
		
		// 检查加解密模式
		如果( 模式 == "" )
			模式值 = crypto.ECB_MODE ;
		否则 如果( 模式 == "ECB" )
			模式值 = crypto.ECB_MODE ;
		否则 如果( 模式 == "CBC" )
			模式值 = crypto.CBC_MODE ;
		否则
		{
			标准输出.格式化显示并换行( "*** 错误 : -m:%{模式} 无效" );
			返回 1;
		}
		
		// 检查密钥数据
		如果( 密钥数据 == "" )
		{
			标准输出.格式化显示并换行( "*** 错误 : -a %{算法} 没有密钥数据" );
			返回 1;
		}
		
		// 检查密钥长度
		密钥长度类型 = 密钥数据.长度() ;
		如果( 密钥长度类型 == 8 )
			密钥长度类型 = crypto.KEY_64BITS ;
		否则 如果( 密钥长度类型 == 16 )
			密钥长度类型 = crypto.KEY_128BITS ;
		否则 如果( 密钥长度类型 == 24 )
			密钥长度类型 = crypto.KEY_192BITS ;
		否则
		{
			标准输出.格式化显示并换行( "*** 错误 : -k:%{密钥长度类型} 密钥长度" );
			返回 1;
		}
		
		// 检查输入数据
		如果( 输入数据 == "" )
		{
			标准输出.格式化显示并换行( "*** 错误 : -a %{算法} 没有输入数据" );
			返回 1;
		}
		
		// 检查填充模式
		如果( 填充格式 == "" )
			填充格式值 = crypto.PKCS7_PADDING ;
		否则 如果( 填充格式 == "ZERO" )
			填充格式值 = crypto.ZERO_PADDING ;
		否则 如果( 填充格式 == "PKCS7" )
			填充格式值 = crypto.PKCS7_PADDING ;
		否则
		{
			标准输出.格式化显示并换行( "*** 错误 : -p:%{填充格式} 无效" );
			返回 1;
		}
		
		// 用对称密钥做DES族的加密或解密处理
		算法.转换为小写();
		如果( 算法 == "des" )
		{
			如果( 方法 == "ENCRYPT" )
			{
				输出数据 = ```算法```.加密( 密钥长度类型 , 密钥数据 , 输入数据 , 填充格式值 ) ;
			}
			否则 如果( 方法 == "DECRYPT" )
			{
				输出数据 = ```算法```.解密( 密钥长度类型 , 密钥数据 , 输入数据 , 填充格式值 ) ;
			}
		}
		否则 如果( 算法 == "tripledes" )
		{
			如果( 方法 == "ENCRYPT" )
			{
				输出数据 = ```算法```.加密( 模式值 , 密钥长度类型 , 密钥数据 , 输入数据 , 填充格式值 ) ;
			}
			否则 如果( 方法 == "DECRYPT" )
			{
				输出数据 = ```算法```.解密( 模式值 , 密钥长度类型 , 密钥数据 , 输入数据 , 填充格式值 ) ;
			}
		}
	}
	// 如果算法是RSA组（密钥管理、对称加解密、非对称签名验签）
	否则 如果( 算法 == "RSA" )
	{
		// zcrypto.sh -a:RSA.GENERATE_KEY --key-len-bytes 4096 --export-prikey-pem /tmp/zcrypto.prikey.pem --export-pubkey-pem /tmp/zcrypto.pubkey.pem --export-prikey-der /tmp/zcrypto.prikey.der --export-pubkey-der /tmp/zcrypto.pubkey.der
		
		如果( 方法 == "GENERATE_KEY" )
		{
			// 检查密钥长度
			如果( 密钥长度 == "512" )
				密钥长度值 = 512 ;
			否则 如果( 密钥长度 == "1024" )
				密钥长度值 = 1024 ;
			否则 如果( 密钥长度 == "2048" )
				密钥长度值 = 2048 ;
			否则 如果( 密钥长度 == "4096" )
				密钥长度值 = 4096 ;
			否则
			{
				标准输出.格式化显示并换行( "*** 错误 : 密钥长度:%{密钥长度} 无效" );
				返回 1;
			}
			
			// 检查生成密钥对的种子
			如果( e == "" )
				;
			否则 如果( e == "3" )
				e值 = crypto.E_3 ;
			否则 如果( e == "F4" )
				e值 = crypto.E_F4 ;
			否则
			{
				标准输出.格式化显示并换行( "*** 错误 : e:%{e} 无效" );
				返回 1;
			}
			
			// 生成密钥对处理
			如果( e == "" )
				是否成功 = rsakey.生成密钥对( 密钥长度值 ) ;
			否则
				是否成功 = rsakey.生成密钥对( 密钥长度值 , e值 ) ;
			
			// 如果输入了密钥文件名，则写密钥文件
			如果( 导出私钥PEM文件名 != "" )
				是否成功 = rsakey.导出私钥到PEM文件( 导出私钥PEM文件名 ) ;
			如果( 导出公钥PEM文件名 != "" )
				是否成功 = rsakey.导出公钥到PEM文件( 导出公钥PEM文件名 ) ;
			如果( 导出私钥DER文件名 != "" )
				是否成功 = rsakey.导出私钥到DER文件( 导出私钥DER文件名 ) ;
			如果( 导出公钥DER文件名 != "" )
				是否成功 = rsakey.导出公钥到DER文件( 导出公钥DER文件名 ) ;
			如果( 是否成功 != 真 )
			{
				标准输出.格式化显示并换行( "*** 错误 : 导出密钥失败" );
				返回 1;
			}
		}
		否则 如果( 方法 == "ENCRYPT" || 方法 == "DECRYPT" )
		{
			// zcrypto.sh -a:RSA.ENCRYPT --pubkey-pem /tmp/zcrypto.pubkey.pem -i:TEXT abc --padding:PKCS1_OAEP -o:BINARY > /tmp/zcrypto.rsa.enc
			// zcrypto.sh -a:RSA.DECRYPT --prikey-pem /tmp/zcrypto.prikey.pem -i:FILE /tmp/zcrypto.rsa.enc --padding:PKCS1_OAEP -o:TEXT
			
			rsakey	私钥 , 公钥 ;
			
			// 从密钥文件导入非对称密钥
			是否成功 = 假 ;
			如果( 私钥PEM文件名 != "" )
				是否成功 = 私钥.从PEM文件导入私钥( 私钥PEM文件名 ) ;
			否则 如果( 私钥DER文件名 != "" )
				是否成功 = 私钥.从DER文件导入私钥( 私钥DER文件名 ) ;
			否则 如果( 公钥PEM文件名 != "" )
				是否成功 = 公钥.从PEM文件导入公钥( 公钥PEM文件名 ) ;
			否则 如果( 公钥DER文件名 != "" )
				是否成功 = 公钥.从DER文件导入公钥( 公钥DER文件名 ) ;
			如果( 是否成功 != 真 )
			{
				标准输出.格式化显示并换行( "*** 错误 : 导入密钥失败" );
				返回 1;
			}
			
			// 检查填充模式
			如果( 填充格式 == "" )
				填充格式值 = crypto.PKCS7_PADDING ;
			否则 如果( 填充格式 == "NO" )
				填充格式值 = crypto.NO_PADDING ;
			否则 如果( 填充格式 == "PKCS1" )
				填充格式值 = crypto.PKCS1_PADDING ;
			否则 如果( 填充格式 == "PKCS1_OAEP" )
				填充格式值 = crypto.PKCS1_OAEP_PADDING ;
			否则
			{
				标准输出.格式化显示并换行( "*** 错误 : -p:%{填充格式} 无效" );
				返回 1;
			}
			
			// 用非对称密钥做RSA加密或解密处理
			如果( 方法 == "ENCRYPT" )
			{
				输出数据 = rsa.公钥加密( 公钥 , 输入数据 , 填充格式值 ) ;
			}
			否则 如果( 方法 == "DECRYPT" )
			{
				输出数据 = rsa.私钥解密( 私钥 , 输入数据 , 填充格式值 ) ;
			}
		}
		否则 如果( 方法 == "SIGN" || 方法 == "VERIFY" )
		{
			// zcrypto.sh -a:RSA.SIGN --digest:SHA1 -i:TEXT abc --prikey-pem /tmp/zcrypto.prikey.pem -o:BINARY > /tmp/zcrypto.rsa.sign
			// zcrypto.sh -a:RSA.VERIFY --digest:SHA1 -i:TEXT abc --sign:FILE /tmp/zcrypto.rsa.sign --pubkey-pem /tmp/zcrypto.pubkey.pem
			
			rsakey	私钥 , 公钥 ;
			
			// 检查散列算法
			如果( 摘要算法 == "MD5" )
				摘要算法值 = crypto.NID_MD5 ;
			否则 如果( 摘要算法 == "SHA1" )
				摘要算法值 = crypto.NID_SHA1 ;
			否则 如果( 摘要算法 == "SHA224" )
				摘要算法值 = crypto.NID_SHA224 ;
			否则 如果( 摘要算法 == "SHA256" )
				摘要算法值 = crypto.NID_SHA256 ;
			否则 如果( 摘要算法 == "SHA512" )
				摘要算法值 = crypto.NID_SHA512 ;
			否则 如果( 摘要算法 == "MD5_SHA1" )
				摘要算法值 = crypto.NID_md5_sha1 ;
			否则
			{
				标准输出.格式化显示并换行( "*** 错误 : 摘要算法:%{摘要算法} 无效" );
				返回 1;
			}
			
			// 从密钥文件导入非对称密钥
			是否成功 = 假 ;
			如果( 私钥PEM文件名 != "" )
				是否成功 = 私钥.从PEM文件导入私钥( 私钥PEM文件名 ) ;
			否则 如果( 私钥DER文件名 != "" )
				是否成功 = 私钥.从DER文件导入私钥( 私钥DER文件名 ) ;
			否则 如果( 公钥PEM文件名 != "" )
				是否成功 = 公钥.从PEM文件导入公钥( 公钥PEM文件名 ) ;
			否则 如果( 公钥DER文件名 != "" )
				是否成功 = 公钥.从DER文件导入公钥( 公钥DER文件名 ) ;
			如果( 是否成功 != 真 )
			{
				标准输出.格式化显示并换行( "*** 错误 : 导入密钥失败" );
				返回 1;
			}
			
			// 用非对称密钥做RSA签名或验签
			如果( 方法 == "SIGN" )
			{
				输出数据 = rsa.签名( 摘要算法值 , 输入数据 , 私钥 ) ;
			}
			否则 如果( 方法 == "VERIFY" )
			{
				是否成功 = rsa.验签( 摘要算法值 , 输入数据 , 签名数据 , 公钥 ) ;
				如果( 是否成功 == 真 )
					标准输出.格式化显示并换行( "验签成功" );
				否则 如果( 是否成功 == 假 )
					标准输出.格式化显示并换行( "验签失败" );
			}
		}
		否则
		{
			标准输出.格式化显示并换行( "*** 错误 : 方法:%{方法} 无效" );
			返回 1;
		}
	}
	否则
	{
		标准输出.格式化显示并换行( "*** 错误 : -a:%{算法} 无效" );
		返回 1;
	}
	
	// 如果存在输出，转换成统一格式
	如果( 输出数据 != "" )
	{
		如果( 输出格式 == "TEXT" )
			标准输出.显示并换行( 输出数据 );
		否则 如果( 输出格式 == "BINARY" )
			标准输出.Write( 输出数据 );
		否则 如果( 输出格式 == "HEXSTR" )
		{
			输出数据 = 十六进制字符串.HexExpand( 输出数据 ) ;
			标准输出.格式化显示并换行( "%{输出数据}" );
		}
		否则 如果( 输出格式 == "BASE64" )
		{
			输出数据 = base64.编码( 输出数据 ) ;
			标准输出.格式化显示并换行( "%{输出数据}" );
		}
		否则
		{
			标准输出.格式化显示并换行( "*** 错误 : -o:%{输出格式} 无效" );
			返回 1;
		}
	}
	
	返回 0;
}
```

快捷脚本`zcrypto.sh`

```
if [ x"$1" = x"--debug-log" ] ; then
	shift
	zlang --debug-level ../src/zcrypto.z $* > /tmp/calvin.log
else
	zlang ../src/zcrypto.z $*
fi
```



下一章：（结尾了）
