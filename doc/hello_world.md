上一章：[第一部分：zlang语言](zlang.md)



**章节目录**
- [初次见面，hello world](#初次见面hello-world)



# 初次见面，hello world

代码示例：`hello.z`

```
import stdtypes stdio ;

function int main( array args )
{
	stdout.Println( "hello world" );
	
	return 0;
}
```

是不是一眼就看懂了？这就是新语言语法继承于经典主流老语言的好处，让学习者瞬间过语法关，把有限的时间精力专注于了解特色的地方。

为提高性能，`zlang`把基础和常用对象用C来编写，构建成二进制对象，zlang组合调用它们实现一个应用。

指令`import`用于导入二进制对象，这里仅导入基础类型对象库和标准输入输出对象库即可。

每个zlang程序都需要一个入口`main`函数，`function`用于定义一个函数，`main`函数的原型是固定的，语法和C一致，函数名左边是返回值，右边小括号内是参数列表，`main`从命令行得到参数列表，通过数组`args`传入进来。

`stdout`是标准输出对象，其函数`Println`用于输出一个对象的值并附加一个换行。

运行该代码：

```
$ zlang hello.z
hello world
```



下一章：[字面量、值对象和集合对象](stdtypes.md)