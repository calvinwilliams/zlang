上一章：[注释](remark.md)



**章节目录**





# 异常处理

## 先来聊聊众多编程语言的错误处理/异常机制，大致分为两类

### 第一类：通过返回值返回

C语言的错误处理机制很简朴，找几个系统函数看看，凡是返回值是指针的，都是以NULL作为报错后的返回值，凡是返回值是int的，都是以-1作为报错后的返回值，同时设置errno。

```
void *malloc(size_t size);
DIR *opendir(const char *name);
void *mmap(void *addr, size_t length, int prot, int flags, int fd, off_t offset);

int printf(const char *format, ...);
int fprintf(FILE *stream, const char *format, ...);
int dprintf(int fd, const char *format, ...);
int sprintf(char *str, const char *format, ...);
int snprintf(char *str, size_t size, const char *format, ...);

int epoll_create(int size);
int epoll_wait(int epfd, struct epoll_event *events, int maxevents, int timeout);

int inotify_init(void);
int inotify_add_watch(int fd, const char *pathname, uint32_t mask);
```

```
#define EBADF            9      /* Bad file number */
#define EAGAIN          11      /* Try again */
#define ENOMEM          12      /* Out of memory */
#define EACCES          13      /* Permission denied */
#define ENOTDIR         20      /* Not a directory */
#define EISDIR          21      /* Is a directory */
#define EINVAL          22      /* Invalid argument */
#define EMFILE          24      /* Too many open files */
```

Go语言支持返回两个值，右边一个值固定为错误码。

```
result, err := SomeFunction("input")
if err != nil {
    // Handle the error here
    log.Printf("An error occurred: %v", err)
}
```

Rust语言把业务返回值和错误码合在一起返回。多亏了`let`才能实现。

```
pub enum Result<T, E> {
    Ok(T),
    Err(E),
}

let r = read_file_to_string(r"d:\1111.txt".to_string());
match r {
    Ok(str) => println!("OK: {str}"),
    Err(e) => println!("Error: {e}"),
};
```

### 第二类：抛出异常，外面捕获之

Java的try框住需要监视的代码段，一旦抛出异常则遍历catch选择合适的错误处理路径进入执行，finally提供正常路径或错误处理代码段结束后的后置处置。

```

try{
	...... //可能产生异常的代码
}
catch(ExceptionName1 e){
	...... //当产生ExceptionName1型异常时的处置措施
}
catch(ExceptionName2 e){
	...... //当产生ExceptionName2型异常时的处置措施
}
finally{
	...... //无论是否发生异常，都无条件执行的语句
}
```

## 分析总结

1. 基本职责：告诉调用者，被调用者在哪里发现了一个错误，并且记录调用栈信息；
2. 不泄露资源原则：报错后要及时释放资源，比如文件关闭、数据库连接返还回连接池；（Go的defer很赞）
3. 书写代码友好原则：正常流程代码尽量不关心报错和错误处理逻辑，否则代码会显得臃肿；（反面例子是C）
4. 错误处理后的流程恢复：有些错误在修正后要继续执行，比如提取不到子字符串就赋以常量；有些错误处理完后跳出当前流程，比如提取不到子字符串后就return函数/方法；（反面例子是C++、Java）
5. 区分错误的性质：有些错误是应用级的，比如某字段没传信息，业务逻辑决定是否要处理；有些错误是系统级的，比如栈用完了，无论业务逻辑关不关心都必须处理，一般任由进程/线程自杀；
6. 原型干净原则：不要对函数/方法的输入输出接口的语义中加入与业务无关的东西；（反面例子是Go的第二个返回值）

## 设计`zlang`的错误返回/异常处理机制

还记得`zlang`语言的一个顶层原则吗？我都支持，你选择自己喜欢的。`zlang`允许开发者使用返回值返回或抛出异常来检查报错。

`zlang`异常分为**错误异常**和**致命异常**。

**错误异常**是应用逻辑可以捕获并处理的异常，比如判断账户是否开立、余额是否充足、连不上数据库服务端、读套接字时对端突然断开连接。

**致命异常**是应用逻辑不可以捕获并处理的异常，比如内存耗尽、除以0等。

当被调函数报错/出现异常时，如果往上多层（函数内）没有`try`关键字，开发者须自行判断错误（错误异常被吞掉，致命异常会不断往上抛出直到遇到`catch fatal`得到处理，或者被`main`缺省处理），如果存在`try`，`catch`会抓到错误异常，如果错误异常得不到处理则会被忽略。

错误异常对象名：`error`，不需要导入，`zlang`隐式创建。

| 成员类型 | 成员名字和原型                       | 说明                                                         |
| -------- | ------------------------------------ | ------------------------------------------------------------ |
| 函数     | bool ThrowException(int,string);     | 抛出异常（内含自动记录异常发生栈帧、代码文件名/行/列、对象、函数）；第一个参数是异常码int code，第二个参数是异常消息string msg |
| 函数     | bool HaveException();                | 判断是否一个异常被抛出                                       |
| 函数     | int GetCode();                       | 得到异常的码                                                 |
| 函数     | string GetMessage();                 | 得到异常的消息                                               |
| 函数     | string GetExceptionSourceFilename(); | 得到异常发生的源代码文件名                                   |
| 函数     | int GetExceptionSourceRow();         | 得到异常发生的源代码文件行                                   |
| 函数     | int GetExceptionSourceColumn();      | 得到异常发生的源代码文件列                                   |
| 函数     | string GetExceptionObjectName();     | 得到异常发生的对象名                                         |
| 函数     | string GetExceptionFunctionName();   | 得到异常发生的函数名（全函数，带输入参数的原型）             |
| 函数     | string GetStackTrace();              | 得到异常发生的函数栈，每一个栈帧还包括输入参数、本地变量的值 |
| 函数     | void CleanException();               | 清除异常                                                     |

### try { ... catch { ... } }

外面有`try`包着，里面代码出现错误异常时，执行`catch`里的出错处理代码。一般用于开发者不想每次调用函数后都判断异常，专注于“好”的路径，任一调用出错都要跳出去。

注意：`try`、`catch`里支持用`break`跳出去。

代码示例：`test_try_string_CharAt_1_1.z`

```
import stdtypes stdio ;

function int main( array args )
{
        string  str = "hello" ;
        string  c ;

        try
        {
                c = str.CharAt(6) ;
                stdout.Println( c );

                catch
                {
                        stdout.FormatPrintln( "*** error exception catched" );
                        stdout.FormatPrintln( "*** exception source : %s:%d,%d" , error.GetExceptionSourceFilename() , error.GetExceptionSourceRow() , error.GetExceptionSourceColumn() );
                        stdout.FormatPrintln( "*** exception function : [%s][%s]" , error.GetExceptionObjectName() , error.GetExceptionFunctionName() );
                        stdout.FormatPrintln( "*** message : %s" , error.GetMessage() );
                        stdout.Print( error.GetStackTrace() );
                }
        }

        return 0;
}
```

执行

```
$ zlang test_try_string_CharAt_1_1.z
*** error exception catched
*** exception source : test_try_string_CharAt_1_1.z:10,19
*** exception function : [str][CharAt(int)]
*** message : index out of bounds
FRAME 3 CALLER (test_try_string_CharAt_1_1.z:10,18) - CALLEE [str][CharAt(int)] (null:0,0)
        IN-PARAM [int][(null)] VALUE[6]
FRAME 2 CALLER (test_try_string_CharAt_1_1.z:10,3) - CALLEE [(null)][(null)] (test_try_string_CharAt_1_1.z:10,3)
FRAME 1 CALLER (test_try_string_CharAt_1_1.z:5,2) - CALLEE [(null)][main(array)] (test_try_string_CharAt_1_1.z:5,2)
        IN-PARAM [array][args] VALUE[(no_tostr_func)]
        LOCAL-OBJ [string][str] VALUE[hello]
        LOCAL-OBJ [string][c] VALUE[]
```

### 没有try，手工处理函数异常

外面没有`try`包着，里面代码出现错误异常时，得由开发者自行判断出错并处理。一般用于某一个函数报错后执行特有的错误处理代码，并继续执行后面的代码。

代码示例：`test_try_string_CharAt_1_2.z`

```
import stdtypes stdio ;

function int main( array args )
{
        string  str = "hello" ;
        string  c ;

        c = str.CharAt(6) ;
        if( c == null )
                stdout.Println( "CharAt return error" );
        else
                stdout.Println( c );

        return 0;
}
```

执行

```
$ zlang test_try_string_CharAt_1_2.z
CharAt return error
```

### throw

如果在`try`包着的代码里，手工创建异常并跳到`catch`里，使用关键字`throw`同时设置异常。

代码示例：`test_try_5_1.z`

```
import stdtypes stdio ;

function int main( array args )
{
        try
        {
                string  food ;

                throw error.ThrowException( -1 , "throw a exception forcely" );

                stdin.Scanln( food );

                catch
                {
                        stdout.FormatPrintln( "*** errro exception catched" );
                        stdout.FormatPrintln( "*** exception source : %s:%d,%d" , error.GetExceptionSourceFilename() , error.GetExceptionSourceRow() , error.GetExceptionSourceColumn() );
                        stdout.FormatPrintln( "*** exception function : [%s][%s]" , error.GetExceptionObjectName() , error.GetExceptionFunctionName() );
                        stdout.FormatPrintln( "*** code : %d" , error.GetCode() );
                        stdout.FormatPrintln( "*** message : %s" , error.GetMessage() );
                        stdout.Print( error.GetStackTrace() );
                }
        }

        return 0;
}
```

执行

```
$ zlang test_try_5_1.z
*** errro exception catched
*** exception source : test_try_5_1.z:9,64
*** exception function : [(null)][(null)]
*** code : -1
*** message : throw a exception forcely
FRAME 3 CALLER (test_try_5_1.z:9,31) - CALLEE [error][ThrowException(int,string)] (null:0,0)
        IN-PARAM [int][(null)] VALUE[-1]
        IN-PARAM [string][(null)] VALUE[throw a exception forcely]
FRAME 2 CALLER (test_try_5_1.z:7,3) - CALLEE [(null)][(null)] (test_try_5_1.z:7,3)
        LOCAL-OBJ [string][food] VALUE[]
FRAME 1 CALLER (test_try_5_1.z:5,2) - CALLEE [(null)][main(array)] (test_try_5_1.z:5,2)
        IN-PARAM [array][args] VALUE[(no_tostr_func)]
```

### uncatch

如果在`try`包着的代码里，指定函数报错不想被`catch`抓到，希望用函数返回值自行处理，可在语句前面加上关键字`uncatch`。

代码示例：`test_try_4_3.z`

```
import stdtypes stdio ;

object test
{
        function int IEatFood( string food )
        {
                if( food == "" )
                        return error.ThrowException( -1 , "food is none" );

                stdout.FormatPrintln( "I eat %{food}" );

                return 0;
        }
}

function int main( array args )
{
        try
        {
                string  food ;

                stdin.Scanln( food );
                uncatch nret = test.IEatFood( food );
                if( nret )
                {
                        stdout.FormatPrintln( "test.IEatFood failed[%d]" , nret );
                        return 1;
                }

                catch
                {
                        stdout.FormatPrintln( "*** errro exception catched" );
                        stdout.FormatPrintln( "*** exception source : %s:%d,%d" , error.GetExceptionSourceFilename() , error.GetExceptionSourceRow() , error.GetExceptionSourceColumn() );
                        stdout.FormatPrintln( "*** exception function : [%s][%s]" , error.GetExceptionObjectName() , error.GetExceptionFunctionName() );
                        stdout.FormatPrintln( "*** message : %s" , error.GetMessage() );
                        stdout.Print( error.GetStackTrace() );
                }
        }

        return 0;
}
```

执行

```
$ echo "" | zlang test_try_4_3.z
test.IEatFood failed[-1]
```

`uncatch`只作用在当前表达式，以下是赋值语句时的正确用法

```
string login_token = uncatch http_cookies.Get( "login_token" ) ;
```

### finally

关键字`finally`用于无论从`try`里安全出来还是从`catch`跳出来，都在处理前执行一段代码。

注意：`finally`里支持用`break`跳出去。

代码示例：`test_try_4_11.z`

```
import stdtypes stdio ;

object test
{
        function int IEatFood( string food )
        {
                try
                {
                        if( food == "" )
                                return error.ThrowException( -1 , "food is none" );

                        stdout.FormatPrintln( "I eat %{food}" );

                        catch
                        {
                                stdout.FormatPrintln( "I catch it" );
                        }

                        finally
                        {
                                stdout.FormatPrintln( "finally" );
                        }
                }

                return 0;
        }
}

function int main( array args )
{
        try
        {
                string  food ;

                stdin.Scanln( food );
                test.IEatFood( food );

                catch
                {
                        stdout.FormatPrintln( "*** errro exception catched" );
                        stdout.FormatPrintln( "*** exception source : %s:%d,%d" , error.GetExceptionSourceFilename() , error.GetExceptionSourceRow() , error.GetExceptionSourceColumn() );
                        stdout.FormatPrintln( "*** exception function : [%s][%s]" , error.GetExceptionObjectName() , error.GetExceptionFunctionName() );
                        stdout.FormatPrintln( "*** message : %s" , error.GetMessage() );
                        stdout.Print( error.GetStackTrace() );
                }
        }

        return 0;
}
```

执行

```
$ echo "" | zlang test_try_4_11.z
I catch it
finally
```



下一章：[zlang命令行参数](zlang_parameters.md)

