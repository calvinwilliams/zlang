上一章：[跟踪栈和堆内存](trace.md)



**章节目录**

- [MSOffice文档处理](#msoffice文档处理)
  - [Word文档](#word文档)
  - [Excel文档](#excel文档)



# MSOffice文档处理

MSOffice文档对象用于处理MSOffice文档内容。

目前只支持读取docx、xlsx里面的内容。

注意：本对象库的构建依赖压缩工具库`minizip`。

## Word文档

Word文档对象：`msword`

| 成员类型 | 成员名字和原型                   | 说明                                                         | (错误.)(异常码,)异常消息                                     |
| :------: | -------------------------------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
|   函数   | bool SetConvertEncoding(string); | 设置应用的字符编码，如果应用字符编码与word内部编码不一致则自动转换；默认不转换，一般word内部编码为UTF-8 |                                                              |
|   函数   | bool LoadFile(string filename);  | 从filename装载word文件内容到内存                             | EXCEPTION_MESSAGE_OPEN_ZIP_FILE_FAILED<br />EXCEPTION_MESSAGE_GET_ZIP_INFO_FAILED<br />EXCEPTION_MESSAGE_GET_FILE_INFO_IN_ZIP_FAILED<br />EXCEPTION_MESSAGE_OPEN_AND_READ_AND_CLOSE_FILE_IN_ZIP_FAILED<br />EXCEPTION_MESSAGE_PARSE_DOCUMENT_FAILED<br />EXCEPTION_MESSAGE_OPEN_AND_READ_AND_CLOSE_FILE_IN_ZIP_FAILED<br />EXCEPTION_MESSAGE_PARSE_STYLES_FAILED |
|   函数   | int GetLineCount();              | 得到word文字段落数                                           |                                                              |
|   函数   | string GetLineText(int n);       | 得到第n个段落文字                                            | EXCEPTION_MESSAGE_LINE_NO_INVALID<br />EXCEPTION_MESSAGE_CONVERT_STRING_ENCODING_FAILED |
|   函数   | string GetLineStyle(int n);      | 得到第n个段落样式                                            | EXCEPTION_MESSAGE_LINE_NO_INVALID<br />EXCEPTION_MESSAGE_STYLE_NOT_FOUND |

代码示例：`test_msoffice_msword_1.z`

```
import stdtypes stdio msoffice ;

function int main( array args )
{
        msword  w ;

        w.SetConvertEncoding( "GB18030" );

        b = w.LoadFile( "test_msoffice_msword_1.docx" ) ;
        if( b != true )
        {
                stdout.Println( "load word file failed" );
                return 1;
        }

        line_count = w.GetLineCount() ;
        foreach( int line_no from 1 to line_count )
        {
                line = w.GetLineText( line_no ) ;
                style_name = w.GetLineStyle( line_no ) ;
                stdout.FormatPrintln( "line_no[%d] style_name[%s] line[%s]" , line_no , style_name , line );
        }

        return 0;
}
```

执行

```
$ zlang test_msoffice_msword_1.z
line_no[1] style_name[Title] line[中国改革开放成就]
line_no[2] style_name[] line[]
line_no[3] style_name[heading 1] line[农村改革，开启新篇章]
line_no[4] style_name[heading 2] line[家庭联产承包责任制的推行]
line_no[5] style_name[] line[   中国农村改革的序幕是从家庭联产承包责任制拉开的。这一制度的推行，使得农民获得了生产和分配的自主权，极大地激发了他们的劳动积极性。农村生产力得到了空前的发展，农业生产连年增长，为全国人民提供了充足的粮食和其?┎贰
line_no[6] style_name[heading 2] line[乡镇企业的崛起]
line_no[7] style_name[] line[   随着农村经济的蓬勃发展，乡镇企业也应运而生。这些企业利用当地的资源和优势，迅速发展壮大，为农村经济的多元化和工业化进程做出了巨大贡献。乡镇企业的崛起不仅解决了大量农民的就业问题，还推动了城乡经济的互动与发?埂
line_no[8] style_name[heading 2] line[新农村建设的步伐]
line_no[9] style_name[] line[   新农村建设是近年来中国政府提出的重大战略举措。通过改善农村基础设施、发展农村公共事业、提高农民收入等措施，新农村建设正在推动城乡协调发展，实现共同富裕的目标。农村面貌焕然一新，农民的生活水平得到了显著提高?
line_no[10] style_name[] line[]
line_no[11] style_name[heading 1] line[城市改革，释放新活力]
line_no[12] style_name[heading 2] line[国有企业改革的深入推进]
line_no[13] style_name[] line[  国有企业改革的深入推进是中国城市改革的重要组成部分。通过改制、重组等方式，国有企业逐步摆脱了历史包袱，增强了市场竞争力。它们在经济转型升级、创新驱动发展等方面发挥了重要作用，为国家财政收入的增加做出了巨大?毕住
line_no[14] style_name[heading 2] line[非公有制经济的快速发展]
line_no[15] style_name[] line[  改革开放以来，非公有制经济在中国得到了快速发展。私营企业和个体工商户如雨后春笋般涌现，为经济增长注入了新的活力。非公有制经济的发展不仅促进了就业增长和财政收入增加，还为技术创新和产业升级提供了有力支撑。]
line_no[16] style_name[heading 2] line[城市化进程的加快]
line_no[17] style_name[] line[  城市化进程的加快是中国经济发展的又一重要特征。随着城市化的推进，城市基础设施不断完善，公共服务水平逐步提高。城市化还带动了相关产业的发展和就业机会的增加，为经济发展提供了新的动力。]
line_no[18] style_name[] line[]
line_no[19] style_name[heading 1] line[对外开放，共融世界]
line_no[20] style_name[heading 2] line[对外贸易的蓬勃发展]
line_no[21] style_name[] line[  改革开放以年来，中国的对外贸易取得了举世瞩目的成就。]
line_no[22] style_name[] line[  通过与世界各国开展贸易往来，中国产品走向了世界市场，为全球消费者带来了实惠与便利。]
line_no[23] style_name[] line[  对外贸易的发展也为中国的经济增长和国际地位的提升奠定了坚实基础。]
line_no[24] style_name[heading 2] line[吸引外资的重大举措]
line_no[25] style_name[] line[  吸引外资是中国对外开放的重要一环。]
line_no[26] style_name[] line[  通过制定优惠政策、优化营商环境等措施，中国成功吸引了大量外资进入中国市场。]
line_no[27] style_name[] line[  外资的引入不仅带来了新的资金和技术，还促进了中国产业的升级和创新发展。]
line_no[28] style_name[heading 2] line[国际合作的广泛展开]
line_no[29] style_name[] line[  随着中国实力的增强和国际地位的提高，中国积极参与国际合作与交流，与世界各国共同应对全球性挑战。]
line_no[30] style_name[] line[  通过“一带一路”倡议等国际合作平台，中国与世界各国在经贸、科技、文化等领域展开了广泛合作，为世界和平与发展作出了积极贡献。]
line_no[31] style_name[] line[]
line_no[32] style_name[] line[]
```

## Excel文档

Excel文档对象：`msexcel`

| 成员类型 | 成员名字和原型                         | 说明                                                         | (错误.)(异常码,)异常消息                                     |
| :------: | -------------------------------------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
|   函数   | bool SetConvertEncoding(string);       | 设置应用的字符编码，如果应用字符编码与excel内部编码不一致则自动转换；默认不转换，一般excel内部编码为UTF-8 |                                                              |
|   函数   | bool LoadFile(string filename);        | 从filename装载excel文件内容到内存                            | EXCEPTION_MESSAGE_OPEN_ZIP_FILE_FAILED<br />EXCEPTION_MESSAGE_GET_ZIP_INFO_FAILED<br />EXCEPTION_MESSAGE_GET_FILE_INFO_IN_ZIP_FAILED<br />EXCEPTION_MESSAGE_OPEN_AND_READ_AND_CLOSE_FILE_IN_ZIP_FAILED<br />EXCEPTION_MESSAGE_PARSE_WORKBOOK_FAILED<br />EXCEPTION_MESSAGE_OPEN_AND_READ_AND_CLOSE_FILE_IN_ZIP_FAILED<br />EXCEPTION_MESSAGE_PARSE_SHARED_STRINGS_FAILED<br />EXCEPTION_MESSAGE_OPEN_AND_READ_AND_CLOSE_FILE_IN_ZIP_FAILED<br />EXCEPTION_MESSAGE_PARSE_STYLES_FAILED<br />EXCEPTION_MESSAGE_SHEET_FILE_NAME_INVALID<br />EXCEPTION_MESSAGE_OPEN_AND_READ_AND_CLOSE_FILE_IN_ZIP_FAILED<br />EXCEPTION_MESSAGE_PARSE_SHEET_FAILED<br />EXCEPTION_MESSAGE_QUERY_WORKBOOK_FAILED |
|   函数   | list GetSheetList();                   | 得到excel工作表数量                                          |                                                              |
|   函数   | bool SelectSheet(string sheet_name);   | 选择sheet_name工作表                                         | EXCEPTION_MESSAGE_SHEET_NOT_FOUND                            |
|   函数   | int GetRowCount();                     | 得到当前工作表的行数                                         | EXCEPTION_MESSAGE_SHEET_NOT_SELECTED                         |
|   函数   | int GetColumnCount();                  | 得到当前工作表的列数                                         | EXCEPTION_MESSAGE_SHEET_NOT_SELECTED                         |
|   函数   | string GetCellString(int row,int col); | 得到当前工作表的指定行、列的文本（字符串）                   | EXCEPTION_MESSAGE_SHEET_NOT_SELECTED<br />EXCEPTION_MESSAGE_ROW_NO_INVALID<br />EXCEPTION_MESSAGE_COLUMN_NO_INVALID<br />EXCEPTION_MESSAGE_MSEXCEL_DAYS_TO_DATE_SINCE_1900_01_01_FAILED<br />EXCEPTION_MESSAGE_MSEXCEL_DAYS_TO_DATE_SINCE_1900_01_01_FAILED |

代码示例：`test_msoffice_msexcel_1.z`

```
import stdtypes stdio msoffice ;

function void ShowSheet( msexcel e , string sheet_name )
{
        b = e.SelectSheet( sheet_name ) ;
        if( b != true )
        {
                stdout.FormatPrintln( "sheet[%s] not found" , sheet_name );
                return;
        }

        row_count = e.GetRowCount() ;
        col_count = e.GetColumnCount() ;
        foreach( int r from 1 to row_count )
        {
                foreach( int c from 1 to col_count )
                {
                        s = e.GetCellString( r , c ) ;
                        stdout.FormatPrint("%{s}\t");
                }

                stdout.Println();
        }

        stdout.Println();

        return;
}

function int main( array args )
{
        msexcel         e ;

        e.SetConvertEncoding( "GB18030" );

        b = e.LoadFile( "test_msoffice_msexcel_1.xlsx" ) ;
        if( b != true )
        {
                stdout.Println( "load excel file failed" );
                return 1;
        }

        ShowSheet( e , "Sheet1" );
        ShowSheet( e , "Sheet2" );
        ShowSheet( e , "Sheet3" );
        ShowSheet( e , "Sheet4" );
        ShowSheet( e , "Sheet5" );
        ShowSheet( e , "Sheet6" );
        ShowSheet( e , "Sheet7" );

        return 0;
}
```

执行

```
$ zlang test_msoffice_msexcel_1.z
field1  field2  field3
string1 11      1.1000000000000001
string2 21      2.1
string3 31      3.1

field1  field2  field3
string1 11      1.1000000000000001
string2 21      2.1
string3 31      3.1

field1  field2  field3
1900-01-01      1900-01-01      1900-01-01      1
1900-02-01      1900-02-01      1900-02-01      32
1900-03-01      1900-02-01      1900-02-01      61
1900-12-31      1900-12-31      1900-12-31      366
1901-01-01      1901-01-01      1901-01-01      367
1901-02-01      1901-02-01      1901-02-01      398
1901-03-01      1901-02-01      1901-02-01      426
1901-12-31      1901-12-31      1901-12-31      731
1999-01-01      1999-01-01      1999-01-01      36161
1999-02-01      1999-02-01      1999-02-01      36192
1999-03-01      1999-02-01      1999-02-01      36220
1999-12-31      1999-12-31      1999-12-31      36525
2000-01-01      2000-01-01      2000-01-01      36526
2000-02-01      2000-02-01      2000-02-01      36557
2000-03-01      2000-02-01      2000-02-01      36586
2000-12-31      2000-12-31      2000-12-31      36891
2100-01-01      2100-01-01      2100-01-01      73051
2100-02-01      2100-02-01      2100-02-01      73082
2100-03-01      2100-02-01      2100-02-01      73110
2100-12-31      2100-12-31      2100-12-31      73415

field1  field2  field3
00:01:00        00:01:00        00:01:00
01:00:00        01:00:00        01:00:00
23:59:59        23:59:59        23:59:59

field1  field2  field3
11.55%  12%     11.6%
21.66%  22%     21.7%
31.77%  32%     31.8%

field1  field2  field3
1900-01-01 00:00:00     1900-01-01 00:00:00     1900-01-01 00:00:00     1
1900-01-01 00:00:01     1900-01-01 00:00:01     1900-01-01 00:00:01     1.000011574074074
1900-01-01 01:00:00     1900-01-01 01:00:00     1900-01-01 01:00:00     1.0416666666666667
1900-01-01 23:00:00     1900-01-01 23:00:00     1900-01-01 23:00:00     1.9583333333333335
1900-01-01 23:59:59     1900-01-01 23:59:59     1900-01-01 23:59:59     1.9999884259259257
1900-02-01 00:00:00     1900-02-01 00:00:00     1900-02-01 00:00:00     32

字段1   字段2   字段2
数据1   二〇〇一年一月一日      一角
数据2   二〇一〇年十二月三十一日        一元
数据3   二〇二〇年二月三十一日  一百块
```



下一章：[加解密](crypto.md)