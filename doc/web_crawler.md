上一章：[Web开发](web_development.md)



**章节目录**

- [爬虫开发](#爬虫开发)
  - [获取XML，提取子XML，查找指定标记名字，得到标记数组，遍历标记属性集](#获取xml提取子xml查找指定标记名字得到标记数组遍历标记属性集)
  - [把前一个网页响应回来的Cookie传递给下一个网页](#把前一个网页响应回来的cookie传递给下一个网页)



# 爬虫开发

使用`zlang`自带对象库`network`里的对象`httpclient`可快速开发爬虫应用。

基础获取网页的示例可参考对象库 [网络](network.md) 里的对象`httpclient`。

以下是几个复杂示例。

## 获取XML，提取子XML，查找指定标记名字，得到标记数组，遍历标记属性集

`HTTP GET`一个`https://cn.bing.com/`网站，得到XML中的`/html/body/div[2]/div/div[5]/div/div/div/div[2]/section/ul`子XML（含`ul`），再在子XML中查询所有属性`rel`值为`noopener`的所有`a`链接，输出`a`链接的每一个属性和值。

代码示例：`test_httpclient_10_1.z`

```
import stdtypes stdio thread network xml ;

function int main( array args )
{
        list    req_headers ;
        map     rsp_headers ;
        string  rsp_html ;
        stdfile f ;

        req_headers.AddTail( <object> "User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/120.0.0.0 Safari/537.36" );
        req_headers.AddTail( <object> "Accept: */*" );
        req_headers.AddTail( <object> "Accept-Language: zh-CN,zh;q=0.9" );
        req_headers.AddTail( <object> "Host: cn.bing.com" );
        r = httpclient.Get( "https://cn.bing.com/" , req_headers , rsp_headers , rsp_html ) ;
        if( r != http.HTTP_STATUSCODE_OK )
        {
                stdout.FormatPrintln( "httpclient.Get err[%d]" , r );
                return 1;
        }
        else
        {
                stdout.FormatPrintln( "httpclient.Get ok" );
        }

        f.Open( "/tmp/test_httpclient_10_1.html" , "wb" );
        f.Println( rsp_html );
        f.Close();

        xml.SignSpecialHtmlTags();

        s = xml.Xnode( rsp_html , "/html/body/div[1]/div/div[3]/header/div[1]/nav/ul" ) ;
        if( s == null )
        {
                stdout.FormatPrintln( "xml.Xnode err" );
                return 1;
        }
        else
        {
                stdout.FormatPrintln( "xml.Xnode ok" );
        }

        f.Open( "/tmp/test_httpclient_10_1_2.html" , "wb" );
        f.Println( s );
        f.Close();

        map     c ;
        c.Put( "rel" , <object> "noopener" );
        l = xml.FindAll( s , "a" , c ) ;
        foreach( map m in l )
        {
                stdout.FormatPrintln( "a :" );
                foreach( string k in m )
                {
                        v = m.Get( k ) ;
                        stdout.FormatPrintln( "    k[%s] v[%s]" , k , xml.TrimTagsAndBlanks(v) );
                }
        }

        return 0;
}
```

执行

```
$ zlang test_httpclient_10_1.z
httpclient.Get ok
xml.Xnode ok
a :
    k[rel] v[noopener]
    k[href] v[/images?FORM=Z9LH]
    k[class] v[]
    k[data-h] v[ID=HpApp,9107.1]
    k[target] v[]
    k[innerText] v[图片]
a :
    k[rel] v[noopener]
    k[href] v[/videos?FORM=Z9LH1]
    k[class] v[]
    k[data-h] v[ID=HpApp,7808.1]
    k[target] v[]
    k[innerText] v[视频]
a :
    k[rel] v[noopener]
    k[href] v[/search?q=%E5%BF%85%E5%BA%94%E7%BF%BB%E8%AF%91&amp;FORM=TTAHP1]
    k[class] v[]
    k[data-h] v[ID=HpApp,14108.1]
    k[target] v[]
    k[innerText] v[翻译]
a :
    k[rel] v[noopener]
    k[href] v[/maps?FORM=Z9LH2]
    k[class] v[]
    k[data-h] v[ID=HpApp,7692.1]
    k[target] v[]
    k[innerText] v[地图]
...
```

## 把前一个网页响应回来的Cookie传递给下一个网页

代码示例：`test_httpclient_10_2.z`

```
import stdtypes stdio thread network xml ;

function int main( array args )
{
        list    req_headers ;
        map     rsp_headers ;
        string  rsp_html ;

        req_headers.AddTail( <object> "User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/120.0.0.0 Safari/537.36" );
        req_headers.AddTail( <object> "Accept: */*" );
        req_headers.AddTail( <object> "Accept-Language: zh-CN,zh;q=0.9" );
        req_headers.AddTail( <object> "Host: cn.bing.com" );
        r = httpclient.Get( "https://cn.bing.com/" , req_headers , rsp_headers , rsp_html ) ;
        if( r != http.HTTP_STATUSCODE_OK )
        {
                stdout.FormatPrintln( "httpclient.Get err[%d]" , r );
                return 1;
        }
        else
        {
                stdout.FormatPrintln( "httpclient.Get ok" );
        }

        list    cookies = httpclient.GetCookies() ;

        foreach( string c in cookies )
        {
                stdout.FormatPrintln( "Cookie : [%s]" , c );
        }

        list    new req_headers ;
        map     new rsp_headers ;
        string  new rsp_html ;

        req_headers.AddTail( <object> "User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/120.0.0.0 Safari/537.36" );
        req_headers.AddTail( <object> "Accept: */*" );
        req_headers.AddTail( <object> "Accept-Language: zh-CN,zh;q=0.9" );
        req_headers.AddTail( <object> "Host: cn.bing.com" );
        req_headers.AddTail( <object> httpclient.FormatCookies(cookies) );
        r = httpclient.Get( "https://cn.bing.com/" , req_headers , rsp_headers , rsp_html ) ;
        if( r != http.HTTP_STATUSCODE_OK )
        {
                stdout.FormatPrintln( "httpclient.Get err[%d]" , r );
                return 1;
        }
        else
        {
                stdout.FormatPrintln( "httpclient.Get ok" );
        }

        return 0;
}
```

执行

```
$ zlang test_httpclient_10_2.z
httpclient.Get ok
Cookie : [MUID=3A8776E4DF48609E29C463E2DE9A611B]
Cookie : [MUIDB=3A8776E4DF48609E29C463E2DE9A611B]
Cookie : [_EDGE_S=F=1&SID=345C0FFE1A1C61A52D7E1AF81BCE60A6]
Cookie : [_EDGE_V=1]
Cookie : [SRCHD=AF=NOFORM]
Cookie : [SRCHUID=V=2&GUID=76D3BEFA1761451D8B2A91DD8F6BB9C3&dmnchg=1]
Cookie : [SRCHUSR=DOB=20240926]
Cookie : [SRCHHPGUSR=SRCHLANG=zh-Hans]
Cookie : [_SS=SID=345C0FFE1A1C61A52D7E1AF81BCE60A6]
Cookie : [ULC=]
Cookie : [_HPVN=CS=eyJQbiI6eyJDbiI6MSwiU3QiOjAsIlFzIjowLCJQcm9kIjoiUCJ9LCJTYyI6eyJDbiI6MSwiU3QiOjAsIlFzIjowLCJQcm9kIjoiSCJ9LCJReiI6eyJDbiI6MSwiU3QiOjAsIlFzIjowLCJQcm9kIjoiVCJ9LCJBcCI6dHJ1ZSwiTXV0ZSI6dHJ1ZSwiTGFkIjoiMjAyNC0wOS0yNlQwMDowMDowMFoiLCJJb3RkIjowLCJHd2IiOjAsIlRucyI6MCwiRGZ0IjpudWxsLCJNdnMiOjAsIkZsdCI6MCwiSW1wIjoxLCJUb2JuIjowfQ==]
httpclient.Get ok
```



下一章：[中文编程](chinese_programming.md)