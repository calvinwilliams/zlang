上一章：[初次见面，hello world](hello_world.md)



**章节目录**
- [字面量、值对象和集合对象](#字面量值对象和集合对象)
  - [字面量](#字面量)
  - [值对象](#值对象)
    - [数值对象](#数值对象)
    - [字符串对象](#字符串对象)
    - [函数指针对象](#函数指针对象)
  - [集合对象](#集合对象)
    - [数组](#数组)
    - [链表](#链表)
    - [队列](#队列)
    - [栈](#栈)
    - [映射](#映射)
    - [迭代器](#迭代器)



# 字面量、值对象和集合对象

## 字面量

字面量是代码中出现的固定的数据，字面上就能看出其类型和数值，常常用作把它赋值给变量或常量。如下例`123`就是整数字面量，`white`是字符串字面量：

```
i = 123 ;
str = "white" ;
```

`zlang`支持以下类型字面量：

| 类型         | 字面量表达                                                   | 示例                                                       |
| ------------ | ------------------------------------------------------------ | ---------------------------------------------------------- |
| 短整型       | 数值右缀加`S`                                                | 1S                                                         |
| 无符号短整型 | 数值右缀加`US`                                               | 1US                                                        |
| 整型         | 数值右缀加`I`，或不加右缀                                    | 2.3I                                                       |
| 无符号整型   | 数值右缀加`UI`                                               | 2.3UI                                                      |
| 长整型       | 数值右缀加`L`                                                | 456.7890L                                                  |
| 无符号长整型 | 数值右缀加`UL`                                               | 456.7890UL                                                 |
| 单精度浮点型 | 数值右缀加`F`                                                | 1.2F                                                       |
| 双精度浮点型 | 数值右缀加`D`，或不加右缀                                    | 3456.7890D                                                 |
| 字符串       | 用双引号把字符串字面量包起来；初始值""                       | "hello"                                                    |
| 字符串块     | 用三个单引号把字符串块字面量包起来                           | \`\`\`这是第一行（换行）这是第二行（换行）这是第三行\`\`\` |
| 标识         | 用反引号把标识包起来；包含白字符的函数名在很多地方可以用到，用于自描述代码，后面会介绍 | function int \`这是 一个 包含 白字符 的 函数名\`() { ... } |
| 布尔型       |                                                              | true或false                                                |
| 空指针       |                                                              | null                                                       |

注意：数值型字面量不加右缀的话，当发生声明变量等处理时会自动类型升降级，加右缀仅仅为了更加严谨，开发人员可根据自己需要自行决定加还是不加。

注意：虽然`zlang`提供了无符号数值类型，但不建议使用，因为一个人开发时间有限，很多周边对象都暂不支持无符号数值。

注意：布尔型字面量`true`的中文标识符是“真”，`false`的中文标识符是“假”；空指针字面量`null`的中文标识符是“空”。

| 类型         | 字面量表达               | 示例       |
| ------------ | ------------------------ | ---------- |
| 十六进制整型 | 左缀`0x`+n个十六进制字符 | 0x13579BDF |
| 八进制整型   | 左缀`0o`+n个八进制字符   | 0o1357     |
| 二进制整型   | 左缀`0b`+n个二进制字符   | 0b1010101  |

注意：所有数字字面量都支持用`_`分割，比如`1_2345_6789`、`123_456_789`、`1.234_567`、`0x12_34_56_78`、`0b10_01_11_01_10_00_11_01`

代码示例：`test_literal_1.z`

该代码示范了各种进制的整型字面量的表示。

```
import stdtypes stdio ;

function int main( array args )
{
        i1 = 123 ;
        stdout.Println( i1 );

        i2 = 0x13579BDF ;
        stdout.Println( i2 );

        i3 = 0o1357 ;
        stdout.Println( i3 );

        i4 = 0b1010101 ;
        stdout.Println( i4 );

        return 0;
}
```

执行

```
$ zlang test_literal_1.z
123
324508639
751
85
```

## 值对象

zlang官方基本对象库自带了一批值对象，你可以替换其中的对象，或者把整个基本对象库都替换掉。

* 逻辑类：布尔型对象`bool`

* 数值类：短整型对象`short`(16字节)、整型对象`int`(32字节)、长整型对象`long`(64字节)、单精度浮点型对象`float`(32字节)、双精度浮点型对象`double`(64字节)

* 字符串类：字符串对象`string`

* 函数指针类：函数对象`functionptr`

注意：数值类整型对象还有配对的无符号对象，比如无符号整型对象`uint`(32字节)。

注意：以上都是对象，不是类型，不是类。

注意：`zlang`的变量声明过程统一为对象克隆。

值对象用于管理数值、字符串值等。

### 数值对象

数值对象内含函数有

| 成员类型 | 成员名字和原型                      | 中文名         | 说明                        |
| -------- | ----------------------------------- | -------------- | --------------------------- |
| 函数     | string ToString();                  | 转换为字符串   | 数值转为字符串              |
| 函数     | string FormatString(string format); | 格式化字符串   | 数值格式化为字符串          |
| 函数     | bool IsBetween(min,max);            | 数值是否在之间 | 判断数值是否在[min,max]之间 |

代码示例：`test_stdtypes_1.z`

该代码示范了各种数值对象的声明（克隆）、赋值和显示输出。

```
import stdtypes stdio ;

function int main( array args )
{
	short	s = 1 ;
	int		i = 234 ;
	long	l = 567890 ;
	float	f = 1.2 ;
	double	d = 34.56789 ;
	bool	b = true ;
	string	str = "balabala" ;
	
	stdout.Println( s );
	stdout.Println( i.ToString() );
	stdout.FormatPrintln( "l[%ld] f[%05.2f] d[%.2lf]" , l , f , d );
	stdout.Println( b.ToString() + " " + str );
	
	return 0;
}
```

执行代码

```
$ zlang test_stdtypes_1.z
1
234
l[567890] f[01.20] d[34.57]
true balabala
```

代码示例：`test_basetype_formatstring_1.z`

```
import stdtypes stdio ;

function int main( array args )
{
        s = 1S ;
        i = 234I ;
        l = 567890L ;
        f = 1.2F ;
        d = 3456.7890D ;
        str = "hello" ;
        b = false ;

        stdout.Println( s.FormatString("s[%2hd]") + i.FormatString(" i[%-4d]") + l.FormatString(" l[%ld]") + f.FormatString(" f[%4f]") + d.FormatString(" d[%10.2lf]") + str.FormatString(" str[%s]") + b.FormatString(" b2[%-6b] ") );

        return 0;
}
```

执行

```
$ zlang test_basetype_formatstring_1.z
s[ 1] i[234 ] l[567890] f[1.200000] d[   3456.79] str[hello] s2[false ]
```

代码示例：`test_IsBetween_1.z`

```
import stdtypes stdio ;

function int main( array args )
{
        int     i = 2 ;
        ulong   l = 789 ;
        double  d = 123.1 ;

        stdout.Println( i.IsBetween(1,3) );
        stdout.Println( i.IsBetween(5,7) );

        stdout.Println( l.IsBetween(678UL,890UL) );
        stdout.Println( l.IsBetween(123UL,456UL) );

        stdout.Println( d.IsBetween(123.0,123.2) );
        stdout.Println( d.IsBetween(234.0,345.2) );

        return 0;
}
```

执行

```
true
false
true
false
true
false
```

追加导入**数学对象`math`**可使数值对象增加一批数学函数。

`float`和`double`对象的数学函数如下：

| 成员类型 | 成员名字和原型 | 说明                                           |
| -------- | -------------- | ---------------------------------------------- |
| 函数     | Sin();         | 计算本对象的正弦值，返回计算结果值对象         |
| 函数     | Cos();         | 计算本对象的余弦值，返回计算结果值对象         |
| 函数     | Tan();         | 计算本对象的正切值，返回计算结果值对象         |
| 函数     | ASin();        | 计算本对象的反正弦值，返回计算结果值对象       |
| 函数     | ACos();        | 计算本对象的反余弦值，返回计算结果值对象       |
| 函数     | ATan();        | 计算本对象的反正切值，返回计算结果值对象       |
| 函数     | SinH();        | 计算本对象的双曲正弦值，返回计算结果值对象     |
| 函数     | CosH();        | 计算本对象的双曲余弦值，返回计算结果值对象     |
| 函数     | TanH();        | 计算本对象的双曲正切值，返回计算结果值对象     |
| 函数     | Log();         | 计算本对象的e为底的对数，返回计算结果值对象    |
| 函数     | Log10();       | 计算本对象的10为底的对数，返回计算结果值对象   |
| 函数     | Pow(double y); | 计算本对象的y次幂，返回计算结果值对象          |
| 函数     | Exp();         | 计算本对象的e的幂，返回计算结果值对象          |
| 函数     | Sqrt();        | 计算本对象的平方根，返回计算结果值对象         |
| 函数     | Ceil();        | 计算不比本对象小的最小整数，返回计算结果值对象 |
| 函数     | Floor();       | 计算不比本对象大的最大整数，返回计算结果值对象 |
| 函数     | Abs();         | 计算本对象的绝对值，返回计算结果值对象         |

`short`、`int`和`long`对象的数学函数如下：

| 成员类型 | 成员名字和原型 | 说明                                   |
| -------- | -------------- | -------------------------------------- |
| 函数     | Pow(int y);    | 计算本对象的y次幂，返回计算结果值对象  |
| 函数     | Sqrt();        | 计算本对象的平方根，返回计算结果值对象 |
| 函数     | Abs();         | 计算本对象的绝对值，返回计算结果值对象 |

代码示例：`test_math_sin_1.z`

该代码示范了导入数学对象库`math`后，`float`和`double`新增了三角函数`Sin`的函数，以及如何调用。

```
import stdtypes stdio math ;

function int main( array args )
{
        float   new rad ;
        float   new r ;
        rad = 90.0 ;
        r = rad.Sin() ;
        stdout.FormatPrintln( "%{r} = %{rad}.Sin() ;" );

        double  new rad ;
        double  new r ;
        rad = 90.0 ;
        r = rad.Sin() ;
        stdout.FormatPrintln( "%{r} = %{rad}.Sin() ;" );

        return 0;
}
```

执行：

```
$ zlang test_math_sin_1.z
0.893997 = 90.000000.Sin() ;
0.893997 = 90.000000.Sin() ;
```

### 字符串对象

字符串对象名：`string`

中文名：字符串

| 成员类型 | 成员名字和原型                                               | 中文名                 | 说明                                                         | (错误.)(异常码,)异常消息                                     |
| -------- | ------------------------------------------------------------ | ---------------------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| 函数     | string Append(string);                                       | 追加                   | 往本字符串右边追加字符串                                     |                                                              |
| 函数     | string CharAt(int);                                          | 取字符                 | 取出指定位置的字符                                           | EXCEPTION_MESSAGE_INDEX_OUT_OF_BOUNDS                        |
| 函数     | void Delete(int start,int end);                              | 删除                   | 从本字符串中删除从start到end的一段字符串，如果start小于1则从最左边开始，如果end大于字符串末尾则强指定最后一个字符处 | EXCEPTION_MESSAGE_ILLEGAL_STATE<br />EXCEPTION_MESSAGE_ILLEGAL_ARGUMENT<br />EXCEPTION_MESSAGE_INDEX_OUT_OF_BOUNDS |
| 函数     | bool EndWith(string match);                                  | 是否匹配结尾           | 判断本字符串末尾是否匹配match                                |                                                              |
| 函数     | bool EndCaseWith(string match);                              | 是否忽略大小写匹配结尾 | 判断本字符串末尾是否匹配match（忽略大小写）                  |                                                              |
| 函数     | bool EqualsIgnoreCase(string str);                           | 忽略大小写判断相等     | 判断本字符串是否和str相同，忽略大小写模式                    |                                                              |
| 函数     | int Length();                                                | 长度                   | 返回字符串的长度                                             |                                                              |
| 函数     | int Format(...);                                             | 格式化                 | 处理格式化串和参数列表，格式化出一个新字符串，覆盖本字符串   |                                                              |
| 函数     | int AppendFormat(...);                                       | 格式化追加             | 处理格式化串和参数列表，格式化出一个新字符串，追加到本字符串右边 | EXCEPTION_MESSAGE_FORMAT_STRING_NOT_COMPLETED<br />EXCEPTION_MESSAGE_FORMAT_PLACEHOLDER_NOT_MATCHED_WITH_PARAMETER<br />EXCEPTION_MESSAGE_FORMAT_NOT_MATCHED_WITH_PARAMETER<br />EXCEPTION_MESSAGE_FORMAT_PARAMETERS_NOT_MATCHED |
| 函数     | string FormatString(string);                                 | 格式化字符串           | 处理格式化串和本字符串值，格式化出一个新字符串，覆盖本字符串 |                                                              |
| 函数     | int IndexOf(string str);                                     | 查找字符串位置         | 从本字符串中寻找str，如果找到则返回开始出现的偏移量（从1开始） |                                                              |
| 函数     | bool IsNumeric();                                            | 是否数字               | 判断本字符串里是否都是数字                                   |                                                              |
| 函数     | bool IsAlphabetic();                                         | 是否字母               | 判断本字符串里是否都是字母                                   |                                                              |
| 函数     | bool IsNumericOrAlphabetic();                                | 是否数字或字母         | 判断本字符串里是否都是数字或字母                             |                                                              |
| 函数     | void Insert(int offset,string str);                          | 插入                   | 从本字符串的偏移量offset插入字符串str                        | EXCEPTION_MESSAGE_INDEX_OUT_OF_BOUNDS                        |
| 函数     | void Replace(string str1,string str2);                       | 替换                   | 从本字符串中寻找字符串str1，如果找到则替换成str2（只替换一次） | EXCEPTION_MESSAGE_NOT_FOUND                                  |
| 函数     | void ReplaceAll(string str1,string str2);                    | 替换所有               | 从本字符串中寻找字符串str1，如果找到则都替换成str2           | EXCEPTION_MESSAGE_NOT_FOUND                                  |
| 函数     | array Split(string sepstr);                                  | 分割                   | 用字符串sepstr分隔本字符串，分解成字符串数组                 | EXCEPTION_MESSAGE_ILLEGAL_ARGUMENT                           |
| 函数     | array Split(string sepstr,string ignstr);                    | 分割                   | 用字符串sepstr分隔本字符串，忽略字符串ignstr，分解成字符串数组 | EXCEPTION_MESSAGE_ILLEGAL_ARGUMENT                           |
| 函数     | bool StartWith(string match);                                | 是否匹配开始           | 判断本字符串开始是否匹配match                                |                                                              |
| 函数     | bool StartCaseWith(string match);                            | 是否忽略大小写匹配开始 | 判断本字符串开始是否匹配match（忽略大小写）                  |                                                              |
| 函数     | string SubString(int start,int end);                         | 取子字符串             | 取本字符串中从start到end之间的子串并返回                     | EXCEPTION_MESSAGE_INDEX_OUT_OF_BOUNDS                        |
| 函数     | short ToShort();                                             | 转换为短整型           | 把本字符串转换成短整型数字                                   |                                                              |
| 函数     | int ToInt();                                                 | 转换为整型             | 把本字符串转换成整型数字                                     |                                                              |
| 函数     | long ToLong();                                               | 转换为长整型           | 把本字符串转换成长整型数字                                   |                                                              |
| 函数     | float ToFloat();                                             | 转换为浮点型           | 把本字符串转换成单精度浮点型数字                             |                                                              |
| 函数     | double ToDouble();                                           | 转换为双精度浮点型     | 把本字符串转换成双精度浮点型数字                             |                                                              |
| 函数     | void LowerCase();                                            | 转换为小写             | 把本字符串中的大写字母都替换成小写                           |                                                              |
| 函数     | void UpperCase();                                            | 转换为大写             | 把本字符串中的小写字母都替换成大写                           |                                                              |
| 函数     | void Trim();                                                 | 清除白字符             | 删除本字符串左边和右边的白字符（空格、退格符、缩进符、回车符、换行符、换页符） |                                                              |
| 函数     | int RegexpMatch(string match);                               | 正则匹配               | 用正则表达式match匹配本字符串中出现的次数                    |                                                              |
| 函数     | int RegexpExtract(string match,string out1,string out2,...); | 正则提取               | 用正则表达式match匹配本字符串，提取出来的子串赋值到参数集合中 |                                                              |
| 函数     | int RegexpReplace(string src,string dst);                    | 正则替换               | 用正则表达式src匹配本字符串，匹配上的子串替换为dst（只替换一次） |                                                              |
| 函数     | int RegexpReplaceAll(string src,string dst);                 | 正则替换所有           | 用正则表达式src匹配本字符串，匹配上的子串都替换为dst         |                                                              |
| 函数     | int RegexpRewrite(string src,string dst);                    | 正则覆写               | 用正则表达式src匹配本字符串，匹配上的子串交换位置到dst正则表达式中，一般用于HTTP服务器的rewrite uri |                                                              |
| 函数     | bool ExpandEnvVatiables                                      | 展开环境变量           | 展开字符串内"${...}"包括的环境变量                           |                                                              |

注意：字符串对象只声明不赋值，初始值为""。

### 函数指针对象

函数指针对象名：`functionptr`

中文名：函数型

| 成员类型 | 成员名字和原型                    | 中文名       | 说明                                                         |
| -------- | --------------------------------- | ------------ | ------------------------------------------------------------ |
| 函数     | functionptr FindFunction(string); | 查找函数     | 指定一个函数原型名（"函数名(输入类型对象1,输入类型对象2,...)"）查找返回函数，如果找不到则返回null |
| 函数     | bool SetFunction(string);         | 设置函数     | 指定一个函数原型名查找设置函数进去，如果返回0表示找到        |
| 函数     | string GetFunctionName();         | 得到函数名   | 返回当前函数指针对象的函数名                                 |
| 函数     | string GetFullFunctionName();     | 得到全函数名 | 返回当前函数指针对象的函数原型名                             |

代码示例：`test_funcp_11.z`

```
import stdtypes stdio ;

function void func1( string str )
{
        stdout.Println( str );
        return;
}

function int func2( string str1 , string str2 )
{
        stdout.Println( str1 + " " + str2 );
        return 0;
}

function int main( array args )
{
        functionptr     funcp1 = func1 ;
        functionptr     funcp2 ;

        funcp2 = func2 ;

        funcp1( "hello world" );

        funcp2( "hello" , "world" );

        return 0;
}
```

执行

```
$ zlang test_funcp_11.z
hello world
hello world
```

注意：如果直接函数名标识符赋值时，如果存在多个原型，则报错。

代码示例：`test_funcp_2.z`

```
import stdtypes stdio ;

object test
{
        function void func1( string str )
        {
                stdout.Println( str );
                return;
        }

        function int func1( string str1 , string str2 )
        {
                stdout.Println( str1 + " " + str2 );
                return 0;
        }
}

function int main( array args )
{
        functionptr     funcp1_1 = functionptr.FindFunction( "test.func1(string)" ) ;
        functionptr     funcp1_2 ;

        b = funcp1_2.SetFunction( "test.func1(string,string)" );
        if( b != true )
        {
                stderr.Println( "func 'func1' not found" );
                return 1;
        }

        funcp1_1( "hello world" );

        funcp1_2( "hello" , "world" );

        return 0;
}
```

执行

```
$ zlang test_funcp_2.z
hello world
hello world
```

## 集合对象

集合对象用于管理对象的集合。

`zlang`官方基本对象库自带了一批集合对象。

集合类：数组对象`array`、链表对象`list`、队列对象`queue`、栈对象`stack`、映射对象`map`、迭代器对象`iterator`

### 数组

数组对象名：`array`

中文名：数组

数组用于管理大致确定数量的元素集合，主要操作是追加、插入、指定位置取元素、删除等。

| 成员类型 | 成员名字和原型                   | 中文名   | 说明                                                    | (错误.)(异常码,)异常消息                     |
| :------: | -------------------------------- | -------- | ------------------------------------------------------- | -------------------------------------------- |
|   函数   | object Append(object);           | 追加     | 向数组最后追加一个对象，返回数组中新对象                | EXCEPTION_MESSAGE_APPEND_ARRAY_FAILED        |
|   函数   | string Append(string);           | 追加     | 向数组最后追加一个字符串对象，返回数组中新对象          | EXCEPTION_MESSAGE_APPEND_ARRAY_FAILED        |
|   函数   | int Append(int);                 | 追加     | 向数组最后追加一个整型对象，返回数组中新对象            | EXCEPTION_MESSAGE_APPEND_ARRAY_FAILED        |
|   函数   | object InsertBefore(int,object); | 插到前面 | 向数组第n个元素前面插入一个对象，返回数组中新对象       | EXCEPTION_MESSAGE_INSERT_BEFORE_ARRAY_FAILED |
|   函数   | string InsertBefore(int,string); | 插到前面 | 向数组第n个元素前面插入一个字符串对象，返回数组中新对象 | EXCEPTION_MESSAGE_INSERT_BEFORE_ARRAY_FAILED |
|   函数   | int InsertBefore(int,int);       | 插到前面 | 向数组第n个元素前面插入一个整型对象，返回数组中新对象   | EXCEPTION_MESSAGE_INSERT_BEFORE_ARRAY_FAILED |
|   函数   | void Remove(int);                | 移除     | 删除数组中第n个对象                                     | EXCEPTION_MESSAGE_ILLEGAL_ARGUMENT           |
|   函数   | void RemoveAll();                | 移除所有 | 删除数组中所有对象                                      |                                              |
|   函数   | object Get(int);                 | 得到     | 得到数组中第n个对象，返回数组中对象的引用               |                                              |
|   函数   | bool Set(int,object);            | 设置     | 更新数组中第n个对象，返回是否更新成功                   | EXCEPTION_MESSAGE_ILLEGAL_ARGUMENT           |
|   函数   | int Length();                    | 长度     | 返回数组中的对象数量                                    |                                              |
|   函数   | bool IsEmpty();                  | 是否空   | 返回是否是空数组                                        |                                              |
|   函数   | bool IsNotEmpty();               | 是否非空 | 返回是否不是空数组                                      |                                              |

代码示例：`test_array_1_1.z`

该代码示范了对数组先追加、插入，再做非空判断，最后遍历。

```
import stdtypes stdio ;

function int main( array args )
{
        array   ar ;

        if( ar.IsEmpty() )
                stdout.Println( "ar is empty now" );
        else
                stderr.Println( "err" );

        ar.Append( "111" );
        ar.Append( "222" );
        ar.Append( "333" );
        ar.InsertBefore( 1 , "444" );
        ar.InsertBefore( 3 , "555" );
        ar.Remove( 2 );

        if( ar.IsNotEmpty() )
                stdout.Println( "ar is not empty now" );
        else
                stderr.Println( "err" );

        string  s2 = ar.Get(2) ;
        stdout.Println( "at 2 in ar is " + s2 );

        stdout.Println( "--- begin travel ar ---" );
        foreach( string s in ar )
        {
                stdout.Println( s );
        }
        stdout.Println( "--- end travel ar ---" );

        ar.RemoveAll();

        return ar.Length();
}
```

执行

```
$ zlang test_array_1_1.z
ar is empty now
ar is not empty now
at 2 in ar is 555
--- begin travel ar ---
444
555
222
333
--- end travel ar ---
```

### 链表

链表对象名：`list`

中文名：链表

链表用于管理数量不确定的元素集合，可随时在头部、中间或尾部添加和删除元素。

| 成员类型 | 成员名字和原型        | 中文名   | 说明                       |
| :------: | --------------------- | -------- | -------------------------- |
|   函数   | bool AddHead(object); | 添加头部 | 向链表头增加一个对象       |
|   函数   | bool AddHead(string); | 添加头部 | 向链表头增加一个字符串对象 |
|   函数   | bool AddHead(int);    | 添加头部 | 向链表头增加一个整型对象   |
|   函数   | bool AddTail(object); | 添加尾部 | 向链表尾增加一个对象       |
|   函数   | bool AddTail(string); | 添加尾部 | 向链表尾增加一个字符串对象 |
|   函数   | bool AddTail(int);    | 添加尾部 | 向链表尾增加一个整型对象   |
|   函数   | void RemoveAll();     | 移除所有 | 删除链表中所有对象         |
|   函数   | list_node GetHead();  | 得到头部 | 得到链表头的链表节点对象   |
|   函数   | list_node GetTail();  | 得到尾部 | 得到链表尾的链表节点对象   |
|   函数   | int Length();         | 长度     | 返回链表中的对象数量       |
|   函数   | bool IsEmpty();       | 是否空   | 返回是否是空链表           |
|   函数   | bool IsNotEmpty()     | 是否非空 | 返回是否不是空链表         |

链表节点对象：list_node

| 成员类型 | 成员名字和原型       |            | 说明                             |
| :------: | -------------------- | ---------- | -------------------------------- |
|   函数   | bool TravelNext();   | 遍历下一个 | 遍历下一个链表节点对象           |
|   函数   | bool TravelPrev();   | 遍历上一个 | 遍历上一个链表节点对象           |
|   函数   | bool IsTravelOver(); | 是否遍历完 | 是否到达链表尾或头               |
|   函数   | bool Remove();       | 移除       | 删除当前链表节点                 |
|   函数   | bool GetMember();    | 得到成员   | 返回当前链表节点的数据对象的引用 |

代码示例：`test_list_1.z`

该代码示范了对链表先追加尾部，再显示链表节点数量，最后遍历显示链表各节点数据。

```
import stdtypes stdio ;

function int main( array args )
{
        list            my_list ;
        list_node       my_list_node ;

        for( int i = 1 ; i <= 6 ; i++ )
        {
                string str ;
                str.Format( "%02d" , i );
                my_list.AddTail( str );
        }

        stdout.Println( my_list.Length() );

        my_list_node = my_list.GetHead() ;
        while( ! my_list_node.IsTravelOver() )
        {
                string str ;
                str = my_list_node.GetMember() ;
                stdout.Println( str );

                my_list_node.TravelNext();
        }

        return 0;
}
```

执行

```
$ zlang test_list_1.z
6
01
02
03
04
05
06
```

### 队列

队列对象名：`queue`

中文名：队列

队列用于管理先进先出的数据，主要操作是生产消息和消费消息。

| 成员类型 | 成员名字和原型        | 中文名   | 说明                     |
| :------: | --------------------- | -------- | ------------------------ |
|   函数   | bool Produce(object); | 生产     | 生产一个对象到队列       |
|   函数   | bool Produce(string); | 生产     | 生产一个字符串对象到队列 |
|   函数   | bool Produce(int);    | 生产     | 生产一个整型对象到队列   |
|   函数   | object Peek();        | 窥探     | 从队列窥探出一个对象     |
|   函数   | object Consume();     | 消费     | 从队列消费出一个对象     |
|   函数   | void RemoveAll();     | 移除所有 | 删除队列中所有对象       |
|   函数   | int Length();         | 长度     | 得到队列中的对象数量     |
|   函数   | bool IsEmpty();       | 是否空   | 返回是否是空队列         |
|   函数   | bool IsNotEmpty()     | 是否非空 | 返回是否不是空队列       |

代码示例：`test_queue_1.z`

该代码示范了对队列的消息生产，显示队列消息数量，消费消息，清空队列。

```
zlimport stdtypes stdio ;

function int main( array args )
{
        queue   my_queue ;

        my_queue.Produce( "111" );
        my_queue.Produce( "222" );
        my_queue.Produce( "333" );

        stdout.Println( my_queue.Length() ) ;

        str = my_queue.Peek(); stdout.Println( str );

        str = my_queue.Consume() ; stdout.Println( str ) ;
        str = my_queue.Consume() ; stdout.Println( str ) ;
        str = my_queue.Consume() ; stdout.Println( str ) ;

        my_queue.RemoveAll();

        return 0;
}
```

执行

```
$ zlang test_queue_1.z
3
111
111
222
333
```

### 栈

栈对象名：`stack`

中文名：栈

栈用于管理栈型的元素集合，主要操作是压入和弹出。

| 成员类型 | 成员名字和原型     | 中文名   | 说明                     |
| :------: | ------------------ | -------- | ------------------------ |
|   函数   | bool Push(object); | 压入     | 压入一个对象到栈顶       |
|   函数   | bool Push(string); | 压入     | 压入一个字符串对象到栈顶 |
|   函数   | bool Push(int);    | 压入     | 压入一个整型对象到栈顶   |
|   函数   | object GetTop();   | 得到顶   | 返回栈顶对象的引用       |
|   函数   | object Popup();    | 弹出     | 从栈顶弹出一个对象       |
|   函数   | void RemoveAll();  | 移除所有 | 删除栈中所有对象         |
|   函数   | int Length();      | 长度     | 得到栈中的对象数量       |
|   函数   | bool IsEmpty();    | 是否空   | 返回是否是空栈           |
|   函数   | bool IsNotEmpty()  | 是否非空 | 返回是否不是空栈         |

代码示例：`test_stack_1.z`

该代码示范了对栈的压入，显示栈深度，取出并显示栈顶元素，弹出栈所有元素。

```
import stdtypes stdio ;

function int main( array args )
{
        stack   my_stack ;

        my_stack.Push( "111" ) ;
        my_stack.Push( "222" ) ;
        my_stack.Push( "333" ) ;

        stdout.Println( my_stack.Length() ) ;

        str = my_stack.GetTop() ; stdout.Println( str ) ;

        str = my_stack.Popup() ; stdout.Println( str );
        str = my_stack.Popup() ; stdout.Println( str );
        str = my_stack.Popup() ; stdout.Println( str );

        my_stack.RemoveAll() ;

        return 0 ;
}
```

执行

```
$ zlang test_stack_1.z
3
333
333
222
111
```

### 映射

映射对象名：`map`

中文名：映射

映射用于保存和快速查询KV形态的数据。

| 成员类型 | 成员名字和原型                 | 中文名       | 说明                                               | (错误.)(异常码,)异常消息        |
| :------: | ------------------------------ | ------------ | -------------------------------------------------- | ------------------------------- |
|   函数   | bool Put(object,object);       | 放入         | 写一对键（任意对象）值（任意对象）到映射对象中     |                                 |
|   函数   | bool Put(string,object);       | 放入         | 写一对键（字符串）值（任意对象）到映射对象中       |                                 |
|   函数   | bool Put(int,object);          | 放入         | 写一对键（整型）值（任意对象）到映射对象中         |                                 |
|   函数   | object Get(object);            | 得到         | 从映射对象中返回键（任意对象）对应的值（任意对象） | EXCEPTION_MESSAGE_MAP_GET_ERROR |
|   函数   | object Get(string);            | 得到         | 从映射对象中返回键（字符串）对应的值（任意对象）   | EXCEPTION_MESSAGE_MAP_GET_ERROR |
|   函数   | object Get(int);               | 得到         | 从映射对象中返回键（整型）对应的值（任意对象）     | EXCEPTION_MESSAGE_MAP_GET_ERROR |
|   函数   | object TravelNextKey(object);  | 遍历下一个键 | 遍历下一个键值；输入object=null为从头便利          |                                 |
|   函数   | object TravelPrevKey(object);  | 遍历上一个键 | 遍历上一个键值；输入object=null为从尾便利          |                                 |
|   函数   | void UpdateKey(object,object); | 更新键       | 更新键（任意对象）对应的值（任意对象）             |                                 |
|   函数   | void UpdateKey(string,object); | 更新键       | 更新键（字符串）对应的值（任意对象）               |                                 |
|   函数   | void UpdateKey(int,object);    | 更新键       | 更新键（整型）对应的值（任意对象）                 |                                 |
|   函数   | bool Remove(object);           | 移除         | 删除键（任意对象）                                 |                                 |
|   函数   | bool Remove(string);           | 移除         | 删除键（字符串）                                   |                                 |
|   函数   | bool Remove(int);              | 移除         | 删除键（整型）                                     |                                 |
|   函数   | void RemoveAll();              | 移除         | 删除栈中所有对象                                   |                                 |
|   函数   | int Length();                  | 长度         | 得到栈中的对象数量                                 |                                 |
|   函数   | bool IsEmpty();                | 是否空       | 返回是否是空栈                                     |                                 |
|   函数   | bool IsNotEmpty()              | 是否非空     | 返回是否不是空栈                                   |                                 |

代码示例：`test_map_1.z`

该代码示范了对映射的写入、读取、删除和清空。

```
import stdtypes stdio ;

function int main( array args )
{
        map             my_map ;

        my_map.Put( <object>"111" , <object>"aaa" ) ;
        my_map.Put( <object>"222" , <object>"bbb" ) ;
        my_map.Put( <object>"333" , <object>"ccc" ) ;
        my_map.Put( <object>"444" , <object>"ddd" ) ;
        my_map.Put( <object>"555" , <object>"eee" ) ;
        my_map.Put( <object>"666" , <object>"fff" ) ;
        stdout.Println( my_map.Length() ) ;

        stdout.Println( "---------------------------" ) ;

        value = my_map.Get( <object>"111" ) ; stdout.Println( value ) ;
        value = my_map.Get( <object>"333" ) ; stdout.Println( value ) ;
        value = my_map.Get( <object>"444" ) ; stdout.Println( value ) ;
        value = my_map.Get( <object>"666" ) ; stdout.Println( value ) ;
        value = my_map.Get( <object>"777" ) ; stdout.Println( value ) ;

        stdout.Println( "---------------------------" ) ;

        my_map.Remove( <object>"111" ) ;
        my_map.Remove( <object>"444" ) ;
        my_map.Remove( <object>"666" ) ;
        stdout.Println( my_map.Length() ) ;

        stdout.Println( "---------------------------" ) ;

        value = my_map.Get( <object>"222" ) ; stdout.Println( value ) ;
        value = my_map.Get( <object>"666" ) ; stdout.Println( value ) ;
        stdout.Println( "---------------------------" ) ;

        my_map.RemoveAll();

        stdout.Println( my_map.Length() ) ;

        return 0;
}
```

执行

```
$ zlang test_map_1.z
6
---------------------------
aaa
ccc
ddd
fff
(null)
---------------------------
3
---------------------------
bbb
(null)
---------------------------
0
```

### 迭代器

迭代器对象名：`iterator`

中文名：迭代器

迭代器用于对数组、链表、映射KEY的抽象统一接口的遍历、读取节点、删除节点等操作。

| 成员类型 | 成员名字和原型            | 中文名       | 说明                                                 |
| :------: | ------------------------- | ------------ | ---------------------------------------------------- |
|   函数   | bool TravelFirst(object); | 遍历第一个   | 把迭代器和集合对象object建立关系，置迭代器指向集合头 |
|   函数   | bool TravelLast(object);  | 遍历最后一个 | 把迭代器和集合对象object建立关系，置迭代器指向集合尾 |
|   函数   | bool TravelNext();        | 遍历下一个   | 迭代器遍历下一个对象；映射只遍历键                   |
|   函数   | bool TravelPrev();        | 遍历上一个   | 迭代器遍历上一个对象；映射只遍历键                   |
|   函数   | bool IsTravelOver();      | 是否遍历完   | 迭代器是否到达头或尾                                 |
|   函数   | object GetElement();      | 得到元素     | 返回迭代器当前指向的对象的引用                       |
|   函数   | bool Remove();            | 移除         | 删除当前迭代指向的对象                               |
|   函数   | int Length();             | 长度         | 返回迭代器关联集合对象中的对象数量                   |

代码示例：`test_iterator_1_1.z`

该代码示范了使用迭代器对数组的追加、遍历、删除等操作。

```
import stdtypes stdio ;                                                                                                                                                                                                             [40/1987]

function int main( array args )
{
        array           collect ;
        int             i ;
        iterator        iter ;

        collect.Append( <object>"111" );
        collect.Append( <object>"222" );
        collect.Append( <object>"333" );
        collect.Append( <object>"444" );
        collect.Append( <object>"555" );

        stdout.Println( "--- show all ---" );
        foreach( string s in collect )
        {
                stdout.Println( s );
        }

        stdout.Println( "--- remove some items ---" );
        i = 0 ;
        for( iter.TravelFirst(<object>collect) ; ! iter.IsTravelOver() ; iter.TravelNext() )
        {
                i++;

                if( i == 2 || i == 4 )
                {
                        stdout.FormatPrintln( "remove %d" , i );
                        iter.Remove();
                }
        }

        stdout.Println( "--- show all ---" );
        foreach( string s in collect )
        {
                stdout.Println( s );
        }

        stdout.Println( "--- remove some items ---" );
        i = 0 ;
        for( iter.TravelFirst(<object>collect) ; ! iter.IsTravelOver() ; iter.TravelNext() )
        {
                i++;

                if( i == 1 || i == 3 )
                {
                        stdout.FormatPrintln( "remove %d" , i );
                        iter.Remove();
                }
        }

        stdout.Println( "--- show all ---" );
        foreach( string s in collect )
        {
                stdout.Println( s );
        }
        
        stdout.Println( "--- remove some items ---" );
        i = 0 ;
        for( iter.TravelFirst(<object>collect) ; ! iter.IsTravelOver() ; iter.TravelNext() )
        {
                i++;

                if( i == 1 )
                {
                        stdout.FormatPrintln( "remove %d" , i );
                        iter.Remove();
                }
        }

        stdout.Println( "--- show all ---" );
        foreach( string s in collect )
        {
                stdout.Println( s );
        }

        return 0;
}
```

执行

```
$ zlang test_iterator_1_1.z
--- show all ---
111
222
333
444
555
--- remove some items ---
remove 2
remove 4
--- show all ---
111
333
555
--- remove some items ---
remove 1
remove 3
--- show all ---
333
--- remove some items ---
remove 1
--- show all ---
```

注意：对映射的迭代，遍历出来的是映射的KEY，需要得到VALUE须再自行处理。



下一章：[声明变量](declare.md)