上一章：[跟踪栈和堆内存](trace.md)



**章节目录**

- [用C编写对象](#用c编写对象)
	- [示例对象：计算重力工具对象（gravity\_util）](#示例对象计算重力工具对象gravity_util)
	- [示例对象：大整数（bigint\_demo）](#示例对象大整数bigint_demo)
	- [第三方zlang对象库的工程规范](#第三方zlang对象库的工程规范)
	- [示例git代码库地址](#示例git代码库地址)



# 用C编写对象

“`zlang`的设计原则：2.一切非上层业务逻辑的底层对象都用C封装，换取足够的性能。”

一个`zlang`对象引用一个函数集实体（FunctionsEntity）和一个属性集实体（PropertiesEntity），克隆（声明局部变量）的过程就是创建一个新的`zlang`对象引用（Refer）同一个函数集实体，但复制一个新的属性集实体。

```
struct ZlangObject
{
		...
        struct ZlangFunctionsEntity     *funcs_enti ;
        struct ZlangPropertiesEntity    *props_enti ;
		...
} ;
```

函数集对象里包裹着一组直接函数集（DirectFunctions）和若干个`zlang`函数（Functions）。直接函数用于固定、常用的函数，比如创建/销毁直接属性函数、ToString函数、数学加减乘除函数、逻辑比较函数等，`zlang`提供了一个直接函数集结构类型，对象编写者实现一批直接函数，配置到直接函数集结构变量中，传递给`zlang`运行时函数`ImportObject`就能创建出一个新对象。`zlang`函数是在`zlang`源码中调用的对象函数，可在实现后在创建直接属性函数中用`zlang`运行时函数`AddFunctionAndParametersInObject`添加进去。

```
struct ZlangFunctionsEntity
{
        ...
        struct ZlangDirectFunctions     *direct_funcs ;
        struct rb_root                  funcs ; /* struct ZlangFunction */
		...
} ;
```

属性集对象里包裹着一个直接属性（DirectProperty）和若干个`zlang`属性（Property）。直接属性是C编写`zlang`对象中的上下文数据存放地，比如`zlang`对象`int`内部用`int32_t`存放整数值，其直接属性内会定义一个`int32_t`结构体变量。`zlang`属性是在`zlang`源码中调用的对象是u行，可在创建直接属性函数中用`zlang`运行时函数`AddPropertyInObject`添加进去。如果这个属性是个常量，还应调用`CallRuntimeFunction_*_Set*Value`设置值、`SetConstantObject`设置权限为不可变动。

```
struct ZlangPropertiesEntity
{
        ...
        struct rb_root                  props ;
        struct list_head                props_list ; /* struct ZlangObject */
		...
} ;
```

用C编写`zlang`对象，就是用`ImportObject`导入对象，对象需要对象名、直接函数集，那就基于直接函数集结构体类型声明一个结构体变量，里面配置进创建/销毁直接属性函数、ToString函数、数学加减乘除函数、逻辑比较函数等，最后实现这些函数。

下面介绍如何一步步用C编写`zlang`对象，源码示例可以在开源项目`https://gitee.com/calvinwilliams/zlang_c_object`中找到。

## 示例对象：计算重力工具对象（gravity_util）

计算重力公式：G=mg。m为物体质量，单位：kg，g为重力加速度9.8，G为重力，单位：N牛。

创建一个C源代码文件：`gravity_util.c`最后编译成动态库`.so`，`zlang`的`import`指令导入这个动态库时会固定调用动态库里的`ZlangImportObjects`函数，那么在`gravity_util.c`末尾写道：

```
DLLEXPORT ZlangImportObjectsFunction ZlangImportObjects;
int ZlangImportObjects( struct ZlangRuntime *rt )
{
	struct ZlangObject	*obj = NULL ;
	struct ZlangObject	*prop = NULL ;
	struct ZlangFunction	*func = NULL ;
	int			nret = 0 ;
	
	nret = ImportObject( rt , & obj , ZLANG_OBJECT_gravity_util , & direct_funcs_gravity_util , sizeof(struct ZlangDirectFunctions) , NULL ) ;
	if( nret )
	{
		SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_LINK_FUNC_TO_ENTITY , "import object to global objects heap" )
		return GetRuntimeErrorNo(rt);
	}
	
	/* double gravity_util.g */
	prop = AddPropertyInObject( rt , obj , QueryObjectByObjectName(rt,"double") , "g") ;
	if( prop == NULL )
		return GetRuntimeErrorNo(rt);
	CallRuntimeFunction_double_SetDoubleValue( rt , prop , g );
	SetConstantObject( prop );
	
	/* gravity_util.CalcGravity() */
	func = AddFunctionAndParametersInObject( rt , obj , "CalcGravity" , "CalcGravity(double)" , ZlangInvokeFunction_gravity_util_CalcGravity_double , ZLANG_OBJECT_double , ZLANG_OBJECT_double,NULL , NULL ) ;
	if( func == NULL )
		return GetRuntimeErrorNo(rt);
	
	/* gravity_util.GetLastResult() */
	func = AddFunctionAndParametersInObject( rt , obj , "GetLastResult" , "GetLastResult()" , ZlangInvokeFunction_gravity_util_GetLastResult , ZLANG_OBJECT_double , NULL ) ;
	if( func == NULL )
		return GetRuntimeErrorNo(rt);
	
	return 0 ;
}
```

`ZLANG_OBJECT_gravity_util`为对象名宏，和包含`zlang`头文件，一般都定义在C源代码文件的开头处：

```
#include "zlang.h"

#define ZLANG_OBJECT_gravity_util	"gravity_util"
```

C源代码对顺序比较讲究，`ImportObject`用到了`direct_funcs_gravity_util`，那就把它声明在`ZlangImportObjects`前面：

```
static struct ZlangDirectFunctions direct_funcs_gravity_util =
	{
		ZLANG_OBJECT_gravity_util , /* char *ancestor_name */
		
		ZlangCreateDirectProperty_gravity_util , /* ZlangCreateDirectPropertyFunction *create_entity_func */
		ZlangDestroyDirectProperty_gravity_util , /* ZlangDestroyDirectPropertyFunction *destroy_entity_func */
		
		NULL , /* ZlangFromCharPtrFunction *from_char_ptr_func */
		NULL , /* ZlangToStringFunction *to_string_func */
		NULL , /* ZlangFromDataPtrFunction *from_data_ptr_func */
		NULL , /* ZlangGetDataPtrFunction *get_data_ptr_func */
		
		NULL , /* ZlangOperatorFunction *oper_PLUS_func */
		NULL , /* ZlangOperatorFunction *oper_MINUS_func */
		NULL , /* ZlangOperatorFunction *oper_MUL_func */
		NULL , /* ZlangOperatorFunction *oper_DIV_func */
		NULL , /* ZlangOperatorFunction *oper_MOD_func */
		
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_NEGATIVE_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_NOT_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_BIT_REVERSE_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_PLUS_PLUS_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_MINUS_MINUS_func */
		
		NULL , /* ZlangCompareFunction *comp_EGUAL_func */
		NULL , /* ZlangCompareFunction *comp_NOTEGUAL_func */
		NULL , /* ZlangCompareFunction *comp_LT_func */
		NULL , /* ZlangCompareFunction *comp_LE_func */
		NULL , /* ZlangCompareFunction *comp_GT_func */
		NULL , /* ZlangCompareFunction *comp_GE_func */
		
		NULL , /* ZlangLogicFunction *logic_AND_func */
		NULL , /* ZlangLogicFunction *logic_OR_func */
		
		NULL , /* ZlangLogicFunction *bit_AND_func */
		NULL , /* ZlangLogicFunction *bit_XOR_func */
		NULL , /* ZlangLogicFunction *bit_OR_func */
		NULL , /* ZlangLogicFunction *bit_MOVELEFT_func */
		NULL , /* ZlangLogicFunction *bit_MOVERIGHT_func */
		
		NULL , /* ZlangSummarizeDirectPropertySizeFunction *summarize_direct_prop_size_func */
	} ;
```

这个对象只需要配置一对创建/销毁直接属性函数即可，无需数学、比较运算。

在`direct_funcs_gravity_util`前面定义那一对创建/销毁直接属性函数：

```
ZlangCreateDirectPropertyFunction ZlangCreateDirectProperty_gravity_util;
void *ZlangCreateDirectProperty_gravity_util( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_gravity_util	*gravity_util_prop = NULL ;
	
	gravity_util_prop = (struct ZlangDirectProperty_gravity_util *)malloc( sizeof(struct ZlangDirectProperty_gravity_util) ) ;
	if( gravity_util_prop == NULL )
	{
		SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_ALLOC , "alloc memory for entity" )
		return NULL;
	}
	memset( gravity_util_prop , 0x00 , sizeof(struct ZlangDirectProperty_gravity_util) );
	
	return gravity_util_prop;
}

ZlangDestroyDirectPropertyFunction ZlangDestroyDirectProperty_gravity_util;
void ZlangDestroyDirectProperty_gravity_util( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_gravity_util	*gravity_util_direct_prop = GetObjectDirectProperty(obj) ;
	
	free( gravity_util_direct_prop );
	
	return;
}
```

创建直接属性函数中用`AddPropertyInObject`定义了一个对象内常量`g`和两个`zlang`对象内函数`CalcGravity(double)`、`GetLastResult()`。

在`ZlangCreateDirectProperty_gravity_util`前面定义那两个`zlang`对象内函数，以及直接属性`ZlangDirectProperty_gravity_util`：

```
#define g				9.8

struct ZlangDirectProperty_gravity_util
{
	double		G ;
} ;

ZlangInvokeFunction ZlangInvokeFunction_gravity_util_CalcGravity_double;
int ZlangInvokeFunction_gravity_util_CalcGravity_double( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_gravity_util	*gravity_util_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject			*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	double					m ;
	double					G ;
	
	CallRuntimeFunction_double_GetDoubleValue( rt , in1 , & m );
	
	/* G=mg */
	G = m * g ;
	
	CallRuntimeFunction_double_SetDoubleValue( rt , out1 , G );
	
	gravity_util_direct_prop->G = G ;
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_gravity_util_GetLastResult;
int ZlangInvokeFunction_gravity_util_GetLastResult( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_gravity_util	*gravity_util_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	
	CallRuntimeFunction_double_SetDoubleValue( rt , out1 , gravity_util_direct_prop->G );
	
	return 0;
}
```

`ZlangInvokeFunction_gravity_util_CalcGravity_double`函数语义是输入一个double m，输出double G，代码逻辑为从函数调用栈中输入栈顶对象取出m，通过公式计算出G，写到函数调用栈中输出栈顶对象，并保存最后一次计算结果到直接属性上下文中。

`ZlangInvokeFunction_gravity_util_GetLastResult`函数语义是得到最后一次计算结果。

完整源代码如下：

```
#include "zlang.h"

#define ZLANG_OBJECT_gravity_util	"gravity_util"

#define g				9.8

struct ZlangDirectProperty_gravity_util
{
	double		G ;
} ;

ZlangInvokeFunction ZlangInvokeFunction_gravity_util_CalcGravity_double;
int ZlangInvokeFunction_gravity_util_CalcGravity_double( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_gravity_util	*gravity_util_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	struct ZlangObject			*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	double					m ;
	double					G ;
	
	CallRuntimeFunction_double_GetDoubleValue( rt , in1 , & m );
	
	/* G=mg */
	G = m * g ;
	
	CallRuntimeFunction_double_SetDoubleValue( rt , out1 , G );
	
	gravity_util_direct_prop->G = G ;
	
	return 0;
}

ZlangInvokeFunction ZlangInvokeFunction_gravity_util_GetLastResult;
int ZlangInvokeFunction_gravity_util_GetLastResult( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_gravity_util	*gravity_util_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*out1 = GetOutputParameterInLocalObjectStack(rt,1) ;
	
	CallRuntimeFunction_double_SetDoubleValue( rt , out1 , gravity_util_direct_prop->G );
	
	return 0;
}

ZlangCreateDirectPropertyFunction ZlangCreateDirectProperty_gravity_util;
void *ZlangCreateDirectProperty_gravity_util( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_gravity_util	*gravity_util_prop = NULL ;
	
	gravity_util_prop = (struct ZlangDirectProperty_gravity_util *)malloc( sizeof(struct ZlangDirectProperty_gravity_util) ) ;
	if( gravity_util_prop == NULL )
	{
		SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_ALLOC , "alloc memory for entity" )
		return NULL;
	}
	memset( gravity_util_prop , 0x00 , sizeof(struct ZlangDirectProperty_gravity_util) );
	
	return gravity_util_prop;
}

ZlangDestroyDirectPropertyFunction ZlangDestroyDirectProperty_gravity_util;
void ZlangDestroyDirectProperty_gravity_util( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_gravity_util	*gravity_util_direct_prop = GetObjectDirectProperty(obj) ;
	
	free( gravity_util_direct_prop );
	
	return;
}

static struct ZlangDirectFunctions direct_funcs_gravity_util =
	{
		ZLANG_OBJECT_gravity_util , /* char *ancestor_name */
		
		ZlangCreateDirectProperty_gravity_util , /* ZlangCreateDirectPropertyFunction *create_entity_func */
		ZlangDestroyDirectProperty_gravity_util , /* ZlangDestroyDirectPropertyFunction *destroy_entity_func */
		
		NULL , /* ZlangFromCharPtrFunction *from_char_ptr_func */
		NULL , /* ZlangToStringFunction *to_string_func */
		NULL , /* ZlangFromDataPtrFunction *from_data_ptr_func */
		NULL , /* ZlangGetDataPtrFunction *get_data_ptr_func */
		
		NULL , /* ZlangOperatorFunction *oper_PLUS_func */
		NULL , /* ZlangOperatorFunction *oper_MINUS_func */
		NULL , /* ZlangOperatorFunction *oper_MUL_func */
		NULL , /* ZlangOperatorFunction *oper_DIV_func */
		NULL , /* ZlangOperatorFunction *oper_MOD_func */
		
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_NEGATIVE_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_NOT_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_BIT_REVERSE_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_PLUS_PLUS_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_MINUS_MINUS_func */
		
		NULL , /* ZlangCompareFunction *comp_EGUAL_func */
		NULL , /* ZlangCompareFunction *comp_NOTEGUAL_func */
		NULL , /* ZlangCompareFunction *comp_LT_func */
		NULL , /* ZlangCompareFunction *comp_LE_func */
		NULL , /* ZlangCompareFunction *comp_GT_func */
		NULL , /* ZlangCompareFunction *comp_GE_func */
		
		NULL , /* ZlangLogicFunction *logic_AND_func */
		NULL , /* ZlangLogicFunction *logic_OR_func */
		
		NULL , /* ZlangLogicFunction *bit_AND_func */
		NULL , /* ZlangLogicFunction *bit_XOR_func */
		NULL , /* ZlangLogicFunction *bit_OR_func */
		NULL , /* ZlangLogicFunction *bit_MOVELEFT_func */
		NULL , /* ZlangLogicFunction *bit_MOVERIGHT_func */
		
		NULL , /* ZlangSummarizeDirectPropertySizeFunction *summarize_direct_prop_size_func */
	} ;

DLLEXPORT ZlangImportObjectsFunction ZlangImportObjects;
int ZlangImportObjects( struct ZlangRuntime *rt )
{
	struct ZlangObject	*obj = NULL ;
	struct ZlangObject	*prop = NULL ;
	struct ZlangFunction	*func = NULL ;
	int			nret = 0 ;
	
	nret = ImportObject( rt , & obj , ZLANG_OBJECT_gravity_util , & direct_funcs_gravity_util , sizeof(struct ZlangDirectFunctions) , NULL ) ;
	if( nret )
	{
		SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_LINK_FUNC_TO_ENTITY , "import object to global objects heap" )
		return GetRuntimeErrorNo(rt);
	}
	
	/* double gravity_util.g */
	prop = AddPropertyInObject( rt , obj , QueryObjectByObjectName(rt,"double") , "g") ;
	if( prop == NULL )
		return GetRuntimeErrorNo(rt);
	CallRuntimeFunction_double_SetDoubleValue( rt , prop , g );
	SetConstantObject( prop );
	
	/* gravity_util.CalcGravity() */
	func = AddFunctionAndParametersInObject( rt , obj , "CalcGravity" , "CalcGravity(double)" , ZlangInvokeFunction_gravity_util_CalcGravity_double , ZLANG_OBJECT_double , ZLANG_OBJECT_double,NULL , NULL ) ;
	if( func == NULL )
		return GetRuntimeErrorNo(rt);
	
	/* gravity_util.GetLastResult() */
	func = AddFunctionAndParametersInObject( rt , obj , "GetLastResult" , "GetLastResult()" , ZlangInvokeFunction_gravity_util_GetLastResult , ZLANG_OBJECT_double , NULL ) ;
	if( func == NULL )
		return GetRuntimeErrorNo(rt);
	
	return 0 ;
}
```

编译时加上`-I/home/calvin/include/zlang`，链接时加上`-L/home/calvin/lib -lzlang_runtime`，就能构建出动态库`libzlang_gravity_util.so`，放置在`$HOME/lib/`下，供`zlang`的`import`指令导入对象。

编写`zlang`源代码文件：`test_1.z`

```
import stdtypes stdio gravity_util ;

function int main( array args )
{
        double  m , G ;

        /* G = m*g */
        m = 10 ;
        G = gravity_util.CalcGravity( m ) ;
        stdout.FormatPrintln( "%{G} = gravity_util.CalcGravity( %{m} ) ;" );

        stdout.FormatPrintln( "g[%lf]" , gravity_util.g );

        stdout.FormatPrintln( "get last G is [%lf]" , gravity_util.GetLastResult() );

        return 0;
}
```

执行

```
$ zlang test_1.z
98.000000 = gravity_util.CalcGravity( 10.000000 ) ;
g[9.800000]
get last G is [98.000000]
```

以上实现了一个简单对象。

以下实现一个支持数学运算的复杂对象。

## 示例对象：大整数（bigint_demo）

创建C源代码文件：`bigint_demo.c`

动态库导入函数：

```
DLLEXPORT ZlangImportObjectsFunction ZlangImportObjects;
int ZlangImportObjects( struct ZlangRuntime *rt )
{
	struct ZlangObject	*obj = NULL ;
	struct ZlangFunction	*func = NULL ;
	int			nret = 0 ;
	
	nret = ImportObject( rt , & obj , ZLANG_OBJECT_bigint_demo , & direct_funcs_bigint_demo , sizeof(struct ZlangDirectFunctions) , NULL ) ;
	if( nret )
	{
		SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_LINK_FUNC_TO_ENTITY , "import object to global objects heap" )
		return GetRuntimeErrorNo(rt);
	}
	
	/* bigint_demo.Set(string) */
	func = AddFunctionAndParametersInObject( rt , obj , "Set" , "Set(string)" , ZlangInvokeFunction_bigint_demo_Set_string , ZLANG_OBJECT_void , ZLANG_OBJECT_string,NULL , NULL ) ;
	if( func == NULL )
		return GetRuntimeErrorNo(rt);
	
	return 0 ;
}
```

直接函数集中多配置用于复制值的直接函数`ZlangFromDataPtr_bigint_demo`和`ZlangGetDataPtr_bigint_demo`，还有数学加减乘除函数`ZlangOperator_bigint_demo_PLUS_bigint_demo`等：

```
static struct ZlangDirectFunctions direct_funcs_bigint_demo =
	{
		ZLANG_OBJECT_bigint_demo , /* char *ancestor_name */
		
		ZlangCreateDirectProperty_bigint_demo , /* ZlangCreateDirectPropertyFunction *create_entity_func */
		ZlangDestroyDirectProperty_bigint_demo , /* ZlangDestroyDirectPropertyFunction *destroy_entity_func */
		
		NULL , /* ZlangFromCharPtrFunction *from_char_ptr_func */
		ZlangToString_bigint_demo , /* ZlangToStringFunction *to_string_func */
		ZlangFromDataPtr_bigint_demo , /* ZlangFromDataPtrFunction *from_data_ptr_func */
		ZlangGetDataPtr_bigint_demo , /* ZlangGetDataPtrFunction *get_data_ptr_func */
		
		ZlangOperator_bigint_demo_PLUS_bigint_demo , /* ZlangOperatorFunction *oper_PLUS_func */
		ZlangOperator_bigint_demo_MINUS_bigint_demo , /* ZlangOperatorFunction *oper_MINUS_func */
		ZlangOperator_bigint_demo_MUL_bigint_demo , /* ZlangOperatorFunction *oper_MUL_func */
		ZlangOperator_bigint_demo_DIV_bigint_demo , /* ZlangOperatorFunction *oper_DIV_func */
		ZlangOperator_bigint_demo_MOD_bigint_demo , /* ZlangOperatorFunction *oper_MOD_func */
		
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_NEGATIVE_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_NOT_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_BIT_REVERSE_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_PLUS_PLUS_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_MINUS_MINUS_func */
		
		NULL , /* ZlangCompareFunction *comp_EGUAL_func */
		NULL , /* ZlangCompareFunction *comp_NOTEGUAL_func */
		NULL , /* ZlangCompareFunction *comp_LT_func */
		NULL , /* ZlangCompareFunction *comp_LE_func */
		NULL , /* ZlangCompareFunction *comp_GT_func */
		NULL , /* ZlangCompareFunction *comp_GE_func */
		
		NULL , /* ZlangLogicFunction *logic_AND_func */
		NULL , /* ZlangLogicFunction *logic_OR_func */
		
		NULL , /* ZlangLogicFunction *bit_AND_func */
		NULL , /* ZlangLogicFunction *bit_XOR_func */
		NULL , /* ZlangLogicFunction *bit_OR_func */
		NULL , /* ZlangLogicFunction *bit_MOVELEFT_func */
		NULL , /* ZlangLogicFunction *bit_MOVERIGHT_func */
		
		NULL , /* ZlangSummarizeDirectPropertySizeFunction *summarize_direct_prop_size_func */
	} ;
```

C源代码文件中间为一对创建/销毁直接属性函数：

```
ZlangCreateDirectPropertyFunction ZlangCreateDirectProperty_bigint_demo;
void *ZlangCreateDirectProperty_bigint_demo( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_bigint_demo	*bigint_demo_prop = NULL ;
	
	bigint_demo_prop = (struct ZlangDirectProperty_bigint_demo *)malloc( sizeof(struct ZlangDirectProperty_bigint_demo) ) ;
	if( bigint_demo_prop == NULL )
	{
		SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_ALLOC , "alloc memory for entity" )
		return NULL;
	}
	memset( bigint_demo_prop , 0x00 , sizeof(struct ZlangDirectProperty_bigint_demo) );
	
	mpz_init( bigint_demo_prop->value );
	
	return bigint_demo_prop;
}

ZlangDestroyDirectPropertyFunction ZlangDestroyDirectProperty_bigint_demo;
void ZlangDestroyDirectProperty_bigint_demo( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_bigint_demo	*bigint_demo_direct_prop = GetObjectDirectProperty(obj) ;
	
	free( bigint_demo_direct_prop );
	
	return;
}
```

前面加入对象内函数：

注意：所有对象内函数报错返回时，

* 如果是错误异常，直接`return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , "(error desc)" ); `，`EXCEPTION_CODE_GENERAL`保存到异常`Code`中，`(error desc)`保存到异常`Message`中，`ThrowErrorException`始终返回`0`;
* 如果是致命异常，直接`return ThrowFatalException( rt , ZLANG_FATAL_INVOKE_METHOD_RETURN , "(error desc)" )`，`ZLANG_FATAL_INVOKE_METHOD_RETURN`保存到异常`Code`中，`(error desc)`保存到异常`Message`中，`ThrowFatalException`始终返回第二个参数值;

```
ZlangInvokeFunction ZlangInvokeFunction_bigint_demo_Set_string;
int ZlangInvokeFunction_bigint_demo_Set_string( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_bigint_demo	*bigint_demo_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	char					*str = NULL ;
	int32_t					str_len ;
	
	CallRuntimeFunction_string_GetStringValue( rt , in1 , & str , & str_len );
	nret = mpz_set_str( bigint_demo_direct_prop->value , str , 10 ) ;
	if( nret )
		return ThrowErrorException( rt , EXCEPTION_CODE_GENERAL , "mpz_set_str return execption" );
	
	return 0;
}
```

后面加入复制值的直接函数、加减乘除函数：

```
ZlangToStringFunction ZlangToString_bigint_demo;
int ZlangToString_bigint_demo( struct ZlangRuntime *rt , struct ZlangObject *obj , struct ZlangObject **tostr_obj )
{
	struct ZlangDirectProperty_bigint_demo	*bigint_demo_direct_prop = GetObjectDirectProperty(obj) ;
	char					str[ 100 ] ;
	int32_t					str_len ;
	
	memset( str , 0x00 , sizeof(str) );
	mpz_get_str( str , 10 , bigint_demo_direct_prop->value ) ;
	str_len = strlen( str ) ;
	CallRuntimeFunction_string_SetStringValue( rt , (*tostr_obj) , str , str_len );
	
	return 0;
}

ZlangFromDataPtrFunction ZlangFromDataPtr_bigint_demo;
int ZlangFromDataPtr_bigint_demo( struct ZlangRuntime *rt , struct ZlangObject *obj , void *value_ptr , int32_t value_len )
{
	struct ZlangDirectProperty_bigint_demo	*obj_direct_prop = GetObjectDirectProperty(obj) ;
	
	mpz_set( obj_direct_prop->value , *(mpz_t *)value_ptr );
	return 0;
}

ZlangGetDataPtrFunction ZlangGetDataPtr_bigint_demo;
int ZlangGetDataPtr_bigint_demo( struct ZlangRuntime *rt , struct ZlangObject *obj , void **value_ptr , int32_t *value_len )
{
	struct ZlangDirectProperty_bigint_demo	*obj_direct_prop = GetObjectDirectProperty(obj) ;
	
	if( value_ptr )
		(*value_ptr) = & (obj_direct_prop->value) ;
	if( value_len )
		(*value_len) = sizeof(obj_direct_prop->value) ;
	return 0;
}

ZlangOperatorFunction ZlangOperator_bigint_demo_PLUS_bigint_demo;
int ZlangOperator_bigint_demo_PLUS_bigint_demo( struct ZlangRuntime *rt , struct ZlangObject *in_obj1 , struct ZlangObject *in_obj2 , struct ZlangObject **out_obj )
{
	struct ZlangDirectProperty_bigint_demo	*in1 = GetObjectDirectProperty(in_obj1) ;
	struct ZlangDirectProperty_bigint_demo	*in2 = GetObjectDirectProperty(in_obj2) ;
	struct ZlangDirectProperty_bigint_demo	*out = NULL ;
	
	out = GetObjectDirectProperty(*out_obj) ;
	mpz_add( out->value , in1->value , in2->value );
	
	return 0;
}

ZlangOperatorFunction ZlangOperator_bigint_demo_MINUS_bigint_demo;
int ZlangOperator_bigint_demo_MINUS_bigint_demo( struct ZlangRuntime *rt , struct ZlangObject *in_obj1 , struct ZlangObject *in_obj2 , struct ZlangObject **out_obj )
{
	struct ZlangDirectProperty_bigint_demo	*in1 = GetObjectDirectProperty(in_obj1) ;
	struct ZlangDirectProperty_bigint_demo	*in2 = GetObjectDirectProperty(in_obj2) ;
	struct ZlangDirectProperty_bigint_demo	*out = NULL ;
	
	out = GetObjectDirectProperty(*out_obj) ;
	mpz_sub( out->value , in1->value , in2->value );
	
	return 0;
}

ZlangOperatorFunction ZlangOperator_bigint_demo_MUL_bigint_demo;
int ZlangOperator_bigint_demo_MUL_bigint_demo( struct ZlangRuntime *rt , struct ZlangObject *in_obj1 , struct ZlangObject *in_obj2 , struct ZlangObject **out_obj )
{
	struct ZlangDirectProperty_bigint_demo	*in1 = GetObjectDirectProperty(in_obj1) ;
	struct ZlangDirectProperty_bigint_demo	*in2 = GetObjectDirectProperty(in_obj2) ;
	struct ZlangDirectProperty_bigint_demo	*out = NULL ;
	
	out = GetObjectDirectProperty(*out_obj) ;
	mpz_mul( out->value , in1->value , in2->value );
	
	return 0;
}

ZlangOperatorFunction ZlangOperator_bigint_demo_DIV_bigint_demo;
int ZlangOperator_bigint_demo_DIV_bigint_demo( struct ZlangRuntime *rt , struct ZlangObject *in_obj1 , struct ZlangObject *in_obj2 , struct ZlangObject **out_obj )
{
	struct ZlangDirectProperty_bigint_demo	*in1 = GetObjectDirectProperty(in_obj1) ;
	struct ZlangDirectProperty_bigint_demo	*in2 = GetObjectDirectProperty(in_obj2) ;
	struct ZlangDirectProperty_bigint_demo	*out = NULL ;
	
	if( mpz_get_ui(in2->value) == 0 )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "*** FATAL : division by zero" )
		return ZLANG_ERROR_DIVISION_BY_ZERO;
	}
	
	out = GetObjectDirectProperty(*out_obj) ;
	mpz_cdiv_q( out->value , in1->value , in2->value );
	
	return 0;
}

ZlangOperatorFunction ZlangOperator_bigint_demo_MOD_bigint_demo;
int ZlangOperator_bigint_demo_MOD_bigint_demo( struct ZlangRuntime *rt , struct ZlangObject *in_obj1 , struct ZlangObject *in_obj2 , struct ZlangObject **out_obj )
{
	struct ZlangDirectProperty_bigint_demo	*in1 = GetObjectDirectProperty(in_obj1) ;
	struct ZlangDirectProperty_bigint_demo	*in2 = GetObjectDirectProperty(in_obj2) ;
	struct ZlangDirectProperty_bigint_demo	*out = NULL ;
	
	if( mpz_get_ui(in2->value) == 0 )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "*** FATAL : division by zero" )
		return ZLANG_ERROR_DIVISION_BY_ZERO;
	}
	
	out = GetObjectDirectProperty(*out_obj) ;
	mpz_cdiv_r( out->value , in1->value , in2->value );
	
	return 0;
}
```

完整源代码如下：

```
#include "zlang.h"

#include "gmp.h"

#define ZLANG_OBJECT_bigint_demo	"bigint_demo"

struct ZlangDirectProperty_bigint_demo
{
	mpz_t		value ;
} ;

ZlangInvokeFunction ZlangInvokeFunction_bigint_demo_Set_string;
int ZlangInvokeFunction_bigint_demo_Set_string( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_bigint_demo	*bigint_demo_direct_prop = GetObjectDirectProperty(obj) ;
	struct ZlangObject			*in1 = GetInputParameterInLocalObjectStack(rt,1) ;
	char					*str = NULL ;
	int32_t					str_len ;
	
	CallRuntimeFunction_string_GetStringValue( rt , in1 , & str , & str_len );
	mpz_set_str( bigint_demo_direct_prop->value , str , 10 );
	
	return 0;
}

ZlangCreateDirectPropertyFunction ZlangCreateDirectProperty_bigint_demo;
void *ZlangCreateDirectProperty_bigint_demo( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_bigint_demo	*bigint_demo_prop = NULL ;
	
	bigint_demo_prop = (struct ZlangDirectProperty_bigint_demo *)malloc( sizeof(struct ZlangDirectProperty_bigint_demo) ) ;
	if( bigint_demo_prop == NULL )
	{
		SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_ALLOC , "alloc memory for entity" )
		return NULL;
	}
	memset( bigint_demo_prop , 0x00 , sizeof(struct ZlangDirectProperty_bigint_demo) );
	
	mpz_init( bigint_demo_prop->value );
	
	return bigint_demo_prop;
}

ZlangDestroyDirectPropertyFunction ZlangDestroyDirectProperty_bigint_demo;
void ZlangDestroyDirectProperty_bigint_demo( struct ZlangRuntime *rt , struct ZlangObject *obj )
{
	struct ZlangDirectProperty_bigint_demo	*bigint_demo_direct_prop = GetObjectDirectProperty(obj) ;
	
	free( bigint_demo_direct_prop );
	
	return;
}

ZlangToStringFunction ZlangToString_bigint_demo;
int ZlangToString_bigint_demo( struct ZlangRuntime *rt , struct ZlangObject *obj , struct ZlangObject **tostr_obj )
{
	struct ZlangDirectProperty_bigint_demo	*bigint_demo_direct_prop = GetObjectDirectProperty(obj) ;
	char					str[ 100 ] ;
	int32_t					str_len ;
	
	memset( str , 0x00 , sizeof(str) );
	mpz_get_str( str , 10 , bigint_demo_direct_prop->value ) ;
	str_len = strlen( str ) ;
	CallRuntimeFunction_string_SetStringValue( rt , (*tostr_obj) , str , str_len );
	
	return 0;
}

ZlangFromDataPtrFunction ZlangFromDataPtr_bigint_demo;
int ZlangFromDataPtr_bigint_demo( struct ZlangRuntime *rt , struct ZlangObject *obj , void *value_ptr , int32_t value_len )
{
	struct ZlangDirectProperty_bigint_demo	*obj_direct_prop = GetObjectDirectProperty(obj) ;
	
	mpz_set( obj_direct_prop->value , *(mpz_t *)value_ptr );
	return 0;
}

ZlangGetDataPtrFunction ZlangGetDataPtr_bigint_demo;
int ZlangGetDataPtr_bigint_demo( struct ZlangRuntime *rt , struct ZlangObject *obj , void **value_ptr , int32_t *value_len )
{
	struct ZlangDirectProperty_bigint_demo	*obj_direct_prop = GetObjectDirectProperty(obj) ;
	
	if( value_ptr )
		(*value_ptr) = & (obj_direct_prop->value) ;
	if( value_len )
		(*value_len) = sizeof(obj_direct_prop->value) ;
	return 0;
}

ZlangOperatorFunction ZlangOperator_bigint_demo_PLUS_bigint_demo;
int ZlangOperator_bigint_demo_PLUS_bigint_demo( struct ZlangRuntime *rt , struct ZlangObject *in_obj1 , struct ZlangObject *in_obj2 , struct ZlangObject **out_obj )
{
	struct ZlangDirectProperty_bigint_demo	*in1 = GetObjectDirectProperty(in_obj1) ;
	struct ZlangDirectProperty_bigint_demo	*in2 = GetObjectDirectProperty(in_obj2) ;
	struct ZlangDirectProperty_bigint_demo	*out = NULL ;
	
	out = GetObjectDirectProperty(*out_obj) ;
	mpz_add( out->value , in1->value , in2->value );
	
	return 0;
}

ZlangOperatorFunction ZlangOperator_bigint_demo_MINUS_bigint_demo;
int ZlangOperator_bigint_demo_MINUS_bigint_demo( struct ZlangRuntime *rt , struct ZlangObject *in_obj1 , struct ZlangObject *in_obj2 , struct ZlangObject **out_obj )
{
	struct ZlangDirectProperty_bigint_demo	*in1 = GetObjectDirectProperty(in_obj1) ;
	struct ZlangDirectProperty_bigint_demo	*in2 = GetObjectDirectProperty(in_obj2) ;
	struct ZlangDirectProperty_bigint_demo	*out = NULL ;
	
	out = GetObjectDirectProperty(*out_obj) ;
	mpz_sub( out->value , in1->value , in2->value );
	
	return 0;
}

ZlangOperatorFunction ZlangOperator_bigint_demo_MUL_bigint_demo;
int ZlangOperator_bigint_demo_MUL_bigint_demo( struct ZlangRuntime *rt , struct ZlangObject *in_obj1 , struct ZlangObject *in_obj2 , struct ZlangObject **out_obj )
{
	struct ZlangDirectProperty_bigint_demo	*in1 = GetObjectDirectProperty(in_obj1) ;
	struct ZlangDirectProperty_bigint_demo	*in2 = GetObjectDirectProperty(in_obj2) ;
	struct ZlangDirectProperty_bigint_demo	*out = NULL ;
	
	out = GetObjectDirectProperty(*out_obj) ;
	mpz_mul( out->value , in1->value , in2->value );
	
	return 0;
}

ZlangOperatorFunction ZlangOperator_bigint_demo_DIV_bigint_demo;
int ZlangOperator_bigint_demo_DIV_bigint_demo( struct ZlangRuntime *rt , struct ZlangObject *in_obj1 , struct ZlangObject *in_obj2 , struct ZlangObject **out_obj )
{
	struct ZlangDirectProperty_bigint_demo	*in1 = GetObjectDirectProperty(in_obj1) ;
	struct ZlangDirectProperty_bigint_demo	*in2 = GetObjectDirectProperty(in_obj2) ;
	struct ZlangDirectProperty_bigint_demo	*out = NULL ;
	
	if( mpz_get_ui(in2->value) == 0 )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "*** FATAL : division by zero" )
		return ZLANG_ERROR_DIVISION_BY_ZERO;
	}
	
	out = GetObjectDirectProperty(*out_obj) ;
	mpz_cdiv_q( out->value , in1->value , in2->value );
	
	return 0;
}

ZlangOperatorFunction ZlangOperator_bigint_demo_MOD_bigint_demo;
int ZlangOperator_bigint_demo_MOD_bigint_demo( struct ZlangRuntime *rt , struct ZlangObject *in_obj1 , struct ZlangObject *in_obj2 , struct ZlangObject **out_obj )
{
	struct ZlangDirectProperty_bigint_demo	*in1 = GetObjectDirectProperty(in_obj1) ;
	struct ZlangDirectProperty_bigint_demo	*in2 = GetObjectDirectProperty(in_obj2) ;
	struct ZlangDirectProperty_bigint_demo	*out = NULL ;
	
	if( mpz_get_ui(in2->value) == 0 )
	{
		TEST_RUNTIME_DEBUG_THEN_PRINT( rt , "*** FATAL : division by zero" )
		return ZLANG_ERROR_DIVISION_BY_ZERO;
	}
	
	out = GetObjectDirectProperty(*out_obj) ;
	mpz_cdiv_r( out->value , in1->value , in2->value );
	
	return 0;
}

static struct ZlangDirectFunctions direct_funcs_bigint_demo =
	{
		ZLANG_OBJECT_bigint_demo , /* char *ancestor_name */
		
		ZlangCreateDirectProperty_bigint_demo , /* ZlangCreateDirectPropertyFunction *create_entity_func */
		ZlangDestroyDirectProperty_bigint_demo , /* ZlangDestroyDirectPropertyFunction *destroy_entity_func */
		
		NULL , /* ZlangFromCharPtrFunction *from_char_ptr_func */
		ZlangToString_bigint_demo , /* ZlangToStringFunction *to_string_func */
		ZlangFromDataPtr_bigint_demo , /* ZlangFromDataPtrFunction *from_data_ptr_func */
		ZlangGetDataPtr_bigint_demo , /* ZlangGetDataPtrFunction *get_data_ptr_func */
		
		ZlangOperator_bigint_demo_PLUS_bigint_demo , /* ZlangOperatorFunction *oper_PLUS_func */
		ZlangOperator_bigint_demo_MINUS_bigint_demo , /* ZlangOperatorFunction *oper_MINUS_func */
		ZlangOperator_bigint_demo_MUL_bigint_demo , /* ZlangOperatorFunction *oper_MUL_func */
		ZlangOperator_bigint_demo_DIV_bigint_demo , /* ZlangOperatorFunction *oper_DIV_func */
		ZlangOperator_bigint_demo_MOD_bigint_demo , /* ZlangOperatorFunction *oper_MOD_func */
		
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_NEGATIVE_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_NOT_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_BIT_REVERSE_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_PLUS_PLUS_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_MINUS_MINUS_func */
		
		NULL , /* ZlangCompareFunction *comp_EGUAL_func */
		NULL , /* ZlangCompareFunction *comp_NOTEGUAL_func */
		NULL , /* ZlangCompareFunction *comp_LT_func */
		NULL , /* ZlangCompareFunction *comp_LE_func */
		NULL , /* ZlangCompareFunction *comp_GT_func */
		NULL , /* ZlangCompareFunction *comp_GE_func */
		
		NULL , /* ZlangLogicFunction *logic_AND_func */
		NULL , /* ZlangLogicFunction *logic_OR_func */
		
		NULL , /* ZlangLogicFunction *bit_AND_func */
		NULL , /* ZlangLogicFunction *bit_XOR_func */
		NULL , /* ZlangLogicFunction *bit_OR_func */
		NULL , /* ZlangLogicFunction *bit_MOVELEFT_func */
		NULL , /* ZlangLogicFunction *bit_MOVERIGHT_func */
		
		NULL , /* ZlangSummarizeDirectPropertySizeFunction *summarize_direct_prop_size_func */
	} ;

DLLEXPORT ZlangImportObjectsFunction ZlangImportObjects;
int ZlangImportObjects( struct ZlangRuntime *rt )
{
	struct ZlangObject	*obj = NULL ;
	struct ZlangFunction	*func = NULL ;
	int			nret = 0 ;
	
	nret = ImportObject( rt , & obj , ZLANG_OBJECT_bigint_demo , & direct_funcs_bigint_demo , sizeof(struct ZlangDirectFunctions) , NULL ) ;
	if( nret )
	{
		SET_RUNTIME_ERROR( rt , RUNTIME_ERROR , ZLANG_ERROR_LINK_FUNC_TO_ENTITY , "import object to global objects heap" )
		return GetRuntimeErrorNo(rt);
	}
	
	/* bigint_demo.Set(string) */
	func = AddFunctionAndParametersInObject( rt , obj , "Set" , "Set(string)" , ZlangInvokeFunction_bigint_demo_Set_string , ZLANG_OBJECT_void , ZLANG_OBJECT_string,NULL , NULL ) ;
	if( func == NULL )
		return GetRuntimeErrorNo(rt);
	
	return 0 ;
}
```

编译构建出`libzlang_bigint_demo.so`。

编写`zlang`源代码文件：`test_1.z`

```
import stdtypes stdio bigint_demo ;

function int main( array args )
{
        bigint_demo     x1 , x2 , x3 , x4 , r ;

        /* 123456789123456789 - 987654321987654321 * 101010101101010101 + 909090909909090909 = -99763062916822545501808205684748723 */
        x1.Set( "123456789123456789" );
        x2.Set( "987654321987654321" );
        x3.Set( "101010101101010101" );
        x4.Set( "909090909909090909" );
        r = x1 - x2 * x3 + x4 ;
        stdout.Println( r.ToString() );

        return 0;
}
```

执行

```
$ zlang test_1.z
-99763062916822545501808205684748723
```

## 附加能力

为配合调试对象`trace`采集信息，各对象直接函数集挂上计算直接属性函数，计算直接属性占用内存大小。

简单C结构体如：

```
struct ZlangDirectProperty_gravity_util
{
	double		G ;
} ;
```

计算直接属性函数如下：

```
ZlangSummarizeDirectPropertySizeFunction ZlangSummarizeDirectPropertySize_gravity_util;
void ZlangSummarizeDirectPropertySize_gravity_util( struct ZlangRuntime *rt , struct ZlangObject *obj , size_t *summarized_obj_size , size_t *summarized_direct_prop_size )
{
	SUMMARIZE_SIZE( summarized_direct_prop_size , sizeof(struct ZlangDirectProperty_gravity_util) )
	return;
}
```

内含再申请内存的C结构体如：

```
struct ZlangDirectProperty_string
{
	char		*buf ;
	int32_t		buf_size ;
	int32_t		buf_len ;
} ;
```

计算直接属性函数如下：

```
ZlangSummarizeDirectPropertySizeFunction ZlangSummarizeDirectPropertySize_string;
void ZlangSummarizeDirectPropertySize_string( struct ZlangRuntime *rt , struct ZlangObject *obj , size_t *summarized_obj_size , size_t *summarized_direct_prop_size )
{
	struct ZlangDirectProperty_string	*direct_prop = GetObjectDirectProperty(obj) ;
	
	if( direct_prop->buf == NULL || direct_prop->buf_size <= 0 )
		SUMMARIZE_SIZE( summarized_direct_prop_size , sizeof(struct ZlangDirectProperty_string) )
	else
		SUMMARIZE_SIZE( summarized_direct_prop_size , sizeof(struct ZlangDirectProperty_string)+direct_prop->buf_size )
	
	return;
}
```

最后在直接函数集的最后面挂上该直接函数

```
static struct ZlangDirectFunctions direct_funcs_gravity_util =
	{
		ZLANG_OBJECT_gravity_util , /* char *ancestor_name */
		
		ZlangCreateDirectProperty_gravity_util , /* ZlangCreateDirectPropertyFunction *create_entity_func */
		ZlangDestroyDirectProperty_gravity_util , /* ZlangDestroyDirectPropertyFunction *destroy_entity_func */
		
		NULL , /* ZlangFromCharPtrFunction *from_char_ptr_func */
		NULL , /* ZlangToStringFunction *to_string_func */
		NULL , /* ZlangFromDataPtrFunction *from_data_ptr_func */
		NULL , /* ZlangGetDataPtrFunction *get_data_ptr_func */
		
		NULL , /* ZlangOperatorFunction *oper_PLUS_func */
		NULL , /* ZlangOperatorFunction *oper_MINUS_func */
		NULL , /* ZlangOperatorFunction *oper_MUL_func */
		NULL , /* ZlangOperatorFunction *oper_DIV_func */
		NULL , /* ZlangOperatorFunction *oper_MOD_func */
		
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_NEGATIVE_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_NOT_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_BIT_REVERSE_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_PLUS_PLUS_func */
		NULL , /* ZlangUnaryOperatorFunction *unaryoper_MINUS_MINUS_func */
		
		NULL , /* ZlangCompareFunction *comp_EGUAL_func */
		NULL , /* ZlangCompareFunction *comp_NOTEGUAL_func */
		NULL , /* ZlangCompareFunction *comp_LT_func */
		NULL , /* ZlangCompareFunction *comp_LE_func */
		NULL , /* ZlangCompareFunction *comp_GT_func */
		NULL , /* ZlangCompareFunction *comp_GE_func */
		
		NULL , /* ZlangLogicFunction *logic_AND_func */
		NULL , /* ZlangLogicFunction *logic_OR_func */
		
		NULL , /* ZlangLogicFunction *bit_AND_func */
		NULL , /* ZlangLogicFunction *bit_XOR_func */
		NULL , /* ZlangLogicFunction *bit_OR_func */
		NULL , /* ZlangLogicFunction *bit_MOVELEFT_func */
		NULL , /* ZlangLogicFunction *bit_MOVERIGHT_func */
		
		ZlangSummarizeDirectPropertySize_string , /* ZlangSummarizeDirectPropertySizeFunction *summarize_direct_prop_size_func */
	} ;
```

当调用调试对象`trace`的函数`PrintStackMemory`或`PrintHeapMemory`时，会先设置

```
(*summarized_obj_size) = 0 ; /* 统计所有除直接属性以外的内存，尤其是zlang自身占用如struct ZlangObject */
(*summarized_direct_prop_size) = 0 ; /* 统计直接属性的内存 */
```

然后依次调用所有栈对象或堆对象的直接函数`ZlangSummarizeDirectPropertySize_*`，累加每个对象内存量到两个size中。

## 第三方zlang对象库的工程规范

应包含如下目录：`src/zobjects/(对象库名)。`

应包含一个目录makefile文件，如`src/zobjects/makefile`。

对象库目录中应包含一些C源码文件和一个构建动态库的makefile文件，如`src/zobjects/gravity_util/gravity_util.c`和`src/zobjects/gravity_util/makefile.Linux`。

以下是本章节的示例：

```
src/zobjects/makefile.Linux
src/zobjects/gravity_util/gravity_util.c
src/zobjects/gravity_util/makefile.Linux
```

## 示例git代码库地址

`https://gitee.com/calvinwilliams/zlang_c_object`

可以用以下命令克隆下来并自动编译和安装：

```
$ cd ~/src
$ zlang --makefile makefile.Linux --git-install https://gitee.com/calvinwilliams/zlang_c_object
```



下一章：[应用领域](domain_development.md)
