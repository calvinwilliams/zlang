上一章：[条件语句](if_statement.md)



**章节目录**
- [循环语句](#循环语句)
  - [循环for](#循环for)
  - [循环while](#循环while)
  - [循环do while](#循环do-while)
  - [循环foreach](#循环foreach)



# 循环语句

循环语句用于在程序中重复执行一段逻辑，往往带有跳出循环的测试条件。

## 循环for

for循环主要用于有明确的初始化逻辑、每轮循环结束后的逻辑、跳出循环的条件的场合。

for语句的形式为

```
for( 表达式1 ; 条件2 ; 表达式3 )
	语句;
```

```
for( 表达式1 ; 条件2 ; 表达式3 )
{
	语句块;
}
```

注意：先执行表达式1，然后判断条件2，如果为真，执行下面的语句或语句块，然后执行表达式3，再判断条件2，再执行下面的语句或语句块，循环之。

代码示例：`test_for_1_1.z`

```
import stdtypes stdio ;

function int main( array args )
{
        for( i = 1 ; i < 4 ; i++ )
        {
                stdout.Println( i );
        }

        return 0;
}
```

执行

```
$ zlang test_for_1_1.z
1
2
3
```

## 循环while

while主要用于只有继续保持循环的条件、其它逻辑想自己控制的场合。

while语句的形式为：

```
while( 条件1 )
	条件1为真时执行的语句;
```

```
while( 条件2 )
{
	条件2为真时执行的语句块;
}
```

注意：执行顺序为先判断条件，如果为真则执行下面的语句或语句块，再次判断条件，循环之。

代码示例：`test_while_1_1.z`

```
import stdtypes stdio ;

function int main( array args )
{
        i = 1 ;
        while( i < 4 )
        {
                stdout.Println( i );
                i = i + 1 ;
        }

        return 0;
}
```

执行

```
$ zlang test_while_1_1.z
1
2
3
```

## 循环do while

do while循环主要用于先执行循环，再判断继续循环条件的场合。

do while的形式为：

```
do
	先执行的语句;
while( 条件1 );
```

```
do
{
	先执行的语句块;
}
while( 条件2 );
```

代码示例：`test_do_while_1_1.z`

```
import stdtypes stdio ;

function int main( array args )
{
        i = 1 ;
        do
        {
                stdout.Println( i );
                i = i + 1 ;
        }
        while( i < 4 );

        return 0;
}
```

执行

```
$ zlang test_do_while_1_1.z
1
2
3
```

## 循环foreach

foreach循环主要用于循环遍历一个集合中每一个对象的简化代码写法的场合。

foreach的一种形式为：

```
foreach( 类型对象 变量 in 集合对象 )
	语句;
```

```
foreach( 类型对象 变量 in 集合对象 )
{
	语句块;
}
```

注意：示例中迭代变量声明在foreach首行，变量声明也可以提到外面。

注意：集合对象可以是数组、链表、映射（仅键）。

代码示例：`test_foreach_1_1.z`

```
import stdtypes stdio ;

function int main( array args )
{
        foreach( string s in args )
        {
                stdout.Println( s );
        }

        return args.Length();
}
```

执行

```
zlang test_foreach_1_1.z 111 222 AAA
111
222
AAA
```

foreach的另一种形式为：

```
foreach( 类型对象 数值型变量 from 开始数值 to 结束数值 )
	语句;
```

```
foreach( 类型对象 数值型变量 from 开始数值 to 结束数值 )
{
	语句块;
}
```

代码示例：`test_foreach_12_1.z`

```
import stdtypes stdio ;

function int main( array args )
{
        foreach( int i from 1 to 10 )
        {
                stdout.Println( i );
        }

        return 0;
}
```

执行

```
$ zlang test_foreach_12_1.z
1
2
3
4
5
6
7
8
9
10
```

注意：如果开始数值比结束数值大，则累减。



下一章：[函数](function.md)