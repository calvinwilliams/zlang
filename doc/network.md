上一章：[线程和同步](thread.md)



**章节目录**
- [网络](#网络)
  - [IP](#ip)
  - [TCP](#tcp)
  - [http](#http)
  - [httpclient](#httpclient)
  - [httpserver](#httpserver)
  - [htmlsection](#htmlsection)



# 网络

网络对象库用于网络编程所需要的对象以及功能，包括`tcp`、`http`、`httpclient`、`httpserver`等。

`zlang`网络对象库基于我的开源库`fasterhttp`、`hetao`。

## IP

IP对象名：`ip`。

| 成员类型 | 成员名字和原型                          | 说明                         |
| -------- | --------------------------------------- | ---------------------------- |
| 函数     | bool SetFromString(string ip_str);      | 从字符串ip_str设置IP         |
| 函数     | void SetFromIntegerNetworkOrder(int n); | 从网络字节序的整型n设置IP    |
| 函数     | void SetFromIntegerInHostOrder(int h);  | 从主机字节序的整型h设置IP    |
| 函数     | string ToString();                      | 格式化成字符串返回           |
| 函数     | int ToIntegerInNetworkOrder();          | 格式化成网络字节序的整型返回 |
| 函数     | int ToIntegerInHostOrder();             | 格式化成主机字节序的整型返回 |

## TCP

TCP对象名：`tcp`。

| 成员类型 | 成员名字和原型                        | 说明                                                         | (错误.)(异常码,)异常消息                                     |
| -------- | ------------------------------------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| 属性     | int ERROR                             | 发送或接收数据时出错                                         |                                                              |
| 属性     | int TIMEOUT                           | 发送或接收数据时超时                                         |                                                              |
| 属性     | int DISCONNECTED_FROM_PEER            | 接收数据时对端断开连接                                       |                                                              |
| 函数     | void SetConnectingTimeout(int);       | 设置TCP连接超时时间（毫秒）                                  |                                                              |
| 函数     | void SetDataTransmissionTimeout(int); | 设置TCP数据传输超时时间（毫秒）                              |                                                              |
| 函数     | int GetConnectingElapse();            | 得到上次TCP连接剩余时间（毫秒）                              |                                                              |
| 函数     | int GetDataTransmissionElapse();      | 得到上次TCP数据传输剩余时间（毫秒）                          |                                                              |
| 函数     | bool Listen(string,int);              | 创建TCP服务端侦听，输入参数是IP和PORT，返回是否成功          | tcp.SOCKET_ERROR,EXCEPTION_MESSAGE_LISTEN_SOCKET_FAILED      |
| 函数     | tcp Accept();                         | TCP服务端接受TCP客户端新连接，返回tcp对象                    | tcp.SOCKET_ERROR,EXCEPTION_MESSAGE_ACCEPT_SOCKET_FAILED      |
| 函数     | bool Connect(string,int);             | TCP客户端连接TCP服务端，输入参数是IP和PORT，返回是否成功     | tcp.SOCKET_ERROR,EXCEPTION_MESSAGE_CONNECT_SOCKET_FAILED     |
| 函数     | bool Close();                         | TCP服务端或TCP客户端关闭连接，返回是否成功                   | tcp.SOCKET_ERROR,EXCEPTION_MESSAGE_CLOSE_SOCKET_FAILED       |
| 函数     | string GetIp();                       | 得到TCP的IP，TCP服务端返回侦听IP，TCP服务端接受tcp返回TCP客户端IP，TCP客户端返回本地IP | tcp.SOCKET_ERROR.EXCEPTION_MESSAGE_SOCKET_INVALID            |
| 函数     | int GetPort();                        | 得到TCP的PORT，TCP服务端返回侦听PORT，TCP服务端接受tcp返回TCP客户端PORT，TCP客户端返回本地PORT | tcp.SOCKET_ERROR.EXCEPTION_MESSAGE_SOCKET_INVALID            |
| 函数     | int Send(string);                     | 发送字符串                                                   | tcp.SOCKET_ERROR,EXCEPTION_MESSAGE_SOCKET_INVALID<br />tcp.SOCKET_ERROR,EXCEPTION_MESSAGE_SEND_SOCKET_FAILED<br />tcp.SOCKET_TIMEOUT,EXCEPTION_MESSAGE_SEND_SOCKET_TIMEOUT |
| 函数     | int Send(string,int);                 | 发送指定字节数的字符串                                       | tcp.SOCKET_ERROR,EXCEPTION_MESSAGE_SOCKET_INVALID<br />tcp.SOCKET_ERROR,EXCEPTION_MESSAGE_SEND_SOCKET_FAILED<br />tcp.SOCKET_TIMEOUT,EXCEPTION_MESSAGE_SEND_SOCKET_TIMEOUT |
| 函数     | int Send(short)；                     | 发送短整型数值（网络字节序）                                 | tcp.SOCKET_ERROR,EXCEPTION_MESSAGE_SOCKET_INVALID<br />tcp.SOCKET_ERROR,EXCEPTION_MESSAGE_SEND_SOCKET_FAILED<br />tcp.SOCKET_TIMEOUT,EXCEPTION_MESSAGE_SEND_SOCKET_TIMEOUT |
| 函数     | int Send(int);                        | 发送整型数值（网络字节序）                                   | tcp.SOCKET_ERROR,EXCEPTION_MESSAGE_SOCKET_INVALID<br />tcp.SOCKET_ERROR,EXCEPTION_MESSAGE_SEND_SOCKET_FAILED<br />tcp.SOCKET_TIMEOUT,EXCEPTION_MESSAGE_SEND_SOCKET_TIMEOUT |
| 函数     | int Send(long);                       | 发送长整型数值（网络字节序）                                 | tcp.SOCKET_ERROR,EXCEPTION_MESSAGE_SOCKET_INVALID<br />tcp.SOCKET_ERROR,EXCEPTION_MESSAGE_SEND_SOCKET_FAILED<br />tcp.SOCKET_TIMEOUT,EXCEPTION_MESSAGE_SEND_SOCKET_TIMEOUT |
| 函数     | int Send(float);                      | 发送单精度浮点数数值                                         | tcp.SOCKET_ERROR,EXCEPTION_MESSAGE_SOCKET_INVALID<br />tcp.SOCKET_ERROR,EXCEPTION_MESSAGE_SEND_SOCKET_FAILED<br />tcp.SOCKET_TIMEOUT,EXCEPTION_MESSAGE_SEND_SOCKET_TIMEOUT |
| 函数     | int Send(double);                     | 发送双精度浮点数数值                                         | tcp.SOCKET_ERROR,EXCEPTION_MESSAGE_SOCKET_INVALID<br />tcp.SOCKET_ERROR,EXCEPTION_MESSAGE_SEND_SOCKET_FAILED<br />tcp.SOCKET_TIMEOUT,EXCEPTION_MESSAGE_SEND_SOCKET_TIMEOUT |
| 函数     | int Sendln(string);                   | 发送字符串和换行                                             | tcp.SOCKET_ERROR,EXCEPTION_MESSAGE_SOCKET_INVALID<br />tcp.SOCKET_ERROR,EXCEPTION_MESSAGE_SEND_SOCKET_FAILED<br />tcp.SOCKET_TIMEOUT,EXCEPTION_MESSAGE_SEND_SOCKET_TIMEOUT |
| 函数     | int Receive(string,int);              | 接收指定字节数的字符串                                       | tcp.SOCKET_ERROR,EXCEPTION_MESSAGE_SOCKET_INVALID<br />tcp.SOCKET_ERROR,EXCEPTION_MESSAGE_SOCKET_ERROR<br />tcp.SOCKET_TIMEOUT,EXCEPTION_MESSAGE_RECEIVE_SOCKET_TIMEOUT<br />tcp.SOCKET_ERROR,EXCEPTION_MESSAGE_RECEIVE_SOCKET_FAILED<br />tcp.SOCKET_CLOSED_FROM_PEER,EXCEPTION_MESSAGE_SOCKET_CLOSED_FROM_PEER |
| 函数     | int Receive(short);                   | 接收短整型数值（网络字节序）                                 | tcp.SOCKET_ERROR,EXCEPTION_MESSAGE_SOCKET_INVALID<br />tcp.SOCKET_ERROR,EXCEPTION_MESSAGE_SOCKET_ERROR<br />tcp.SOCKET_TIMEOUT,EXCEPTION_MESSAGE_RECEIVE_SOCKET_TIMEOUT<br />tcp.SOCKET_ERROR,EXCEPTION_MESSAGE_RECEIVE_SOCKET_FAILED<br />tcp.SOCKET_CLOSED_FROM_PEER,EXCEPTION_MESSAGE_SOCKET_CLOSED_FROM_PEER |
| 函数     | int Receive(int);                     | 接收整型数值（网络字节序）                                   | tcp.SOCKET_ERROR,EXCEPTION_MESSAGE_SOCKET_INVALID<br />tcp.SOCKET_ERROR,EXCEPTION_MESSAGE_SOCKET_ERROR<br />tcp.SOCKET_TIMEOUT,EXCEPTION_MESSAGE_RECEIVE_SOCKET_TIMEOUT<br />tcp.SOCKET_ERROR,EXCEPTION_MESSAGE_RECEIVE_SOCKET_FAILED<br />tcp.SOCKET_CLOSED_FROM_PEER,EXCEPTION_MESSAGE_SOCKET_CLOSED_FROM_PEER |
| 函数     | int Receive(long);                    | 接收长整型数值（网络字节序）                                 | tcp.SOCKET_ERROR,EXCEPTION_MESSAGE_SOCKET_INVALID<br />tcp.SOCKET_ERROR,EXCEPTION_MESSAGE_SOCKET_ERROR<br />tcp.SOCKET_TIMEOUT,EXCEPTION_MESSAGE_RECEIVE_SOCKET_TIMEOUT<br />tcp.SOCKET_ERROR,EXCEPTION_MESSAGE_RECEIVE_SOCKET_FAILED<br />tcp.SOCKET_CLOSED_FROM_PEER,EXCEPTION_MESSAGE_SOCKET_CLOSED_FROM_PEER |
| 函数     | int Receive(float);                   | 接收单精度浮点数数值                                         | tcp.SOCKET_ERROR,EXCEPTION_MESSAGE_SOCKET_INVALID<br />tcp.SOCKET_ERROR,EXCEPTION_MESSAGE_SOCKET_ERROR<br />tcp.SOCKET_TIMEOUT,EXCEPTION_MESSAGE_RECEIVE_SOCKET_TIMEOUT<br />tcp.SOCKET_ERROR,EXCEPTION_MESSAGE_RECEIVE_SOCKET_FAILED<br />tcp.SOCKET_CLOSED_FROM_PEER,EXCEPTION_MESSAGE_SOCKET_CLOSED_FROM_PEER |
| 函数     | int Receive(double);                  | 接收双精度浮点数数值                                         | tcp.SOCKET_ERROR,EXCEPTION_MESSAGE_SOCKET_INVALID<br />tcp.SOCKET_ERROR,EXCEPTION_MESSAGE_SOCKET_ERROR<br />tcp.SOCKET_TIMEOUT,EXCEPTION_MESSAGE_RECEIVE_SOCKET_TIMEOUT<br />tcp.SOCKET_ERROR,EXCEPTION_MESSAGE_RECEIVE_SOCKET_FAILED<br />tcp.SOCKET_CLOSED_FROM_PEER,EXCEPTION_MESSAGE_SOCKET_CLOSED_FROM_PEER |
| 函数     | int Receiveln(string);                | 接收字符串直到收到换行                                       | tcp.SOCKET_ERROR,EXCEPTION_MESSAGE_SOCKET_INVALID<br />tcp.SOCKET_ERROR,EXCEPTION_MESSAGE_SOCKET_ERROR<br />tcp.SOCKET_TIMEOUT,EXCEPTION_MESSAGE_RECEIVE_SOCKET_TIMEOUT<br />tcp.SOCKET_ERROR,EXCEPTION_MESSAGE_RECEIVE_SOCKET_FAILED<br />tcp.SOCKET_CLOSED_FROM_PEER,EXCEPTION_MESSAGE_SOCKET_CLOSED_FROM_PEER |

代码示例：`test_tcp_client_2_1.z`

该代码示范了TCP客户端连接TCP服务端，发送基本类型对象和字符串对象。

```
import stdtypes stdio thread network ;

function int main( array args )
{
	tcp	tcp_client ;
	short	s = 1 ;
	int	i = 234 ;
	long	l = 56789 ;
	float	f = 1.2 ;
	double	d = 34.56 ;
	string	lenstr ;
	string	str = "hello" ;
	
	b = tcp_client.Connect( "127.0.0.1" , 9527 ) ;
	if( b != true )
	{
		stdout.Println( "Connect err" );
		return 1;
	}
	
	stdout.Println( s );
	stdout.Println( i );
	stdout.Println( l );
	stdout.Println( f );
	stdout.Println( d );
	stdout.Println( str );
	
	n = tcp_client.Send( s ) ;
	if( n == tcp.ERROR )
	{
		stdout.Println( "Send s err" );
		return 1;
	}
	
	stdout.Println( "Send s ok" );
	
	n = tcp_client.Send( i ) ;
	if( n == tcp.ERROR )
	{
		stdout.Println( "Send i err" );
		return 1;
	}
	
	stdout.Println( "Send i ok" );
	
	n = tcp_client.Send( l ) ;
	if( n == tcp.ERROR )
	{
		stdout.Println( "Send l err" );
		return 1;
	}
	
	stdout.Println( "Send l ok" );
	
	n = tcp_client.Send( f ) ;
	if( n == tcp.ERROR )
	{
		stdout.Println( "Send f err" );
		return 1;
	}
	
	stdout.Println( "Send f ok" );
	
	n = tcp_client.Send( d ) ;
	if( n == tcp.ERROR )
	{
		stdout.Println( "Send d err" );
		return 1;
	}
	
	stdout.Println( "Send d ok" );
	
	lenstr.Format( "%04d" , str.Length() );
	n = tcp_client.Send( lenstr ) ;
	if( n == tcp.ERROR )
	{
		stdout.Println( "Send lenstr err" );
		return 1;
	}
	
	stdout.Println( "Send lenstr ok" );
	
	n = tcp_client.Send( str ) ;
	if( n == tcp.ERROR )
	{
		stdout.Println( "Send str err" );
		return 1;
	}
	
	stdout.Println( "Send str ok" );
	
	n = tcp_client.Receive( s ) ;
	if( n == tcp.ERROR )
	{
		stdout.Println( "Receive s err" );
		return 1;
	}
	else if( n == tcp.DISCONNECTED_FROM_PEER )
	{
		stdout.Println( "Peer closed on Receive s" );
		return 1;
	}
	
	stdout.Println( "Receive s ok" );
	
	n = tcp_client.Receive( i ) ;
	if( n == tcp.ERROR )
	{
		stdout.Println( "Receive i err" );
		return 1;
	}
	else if( n == tcp.DISCONNECTED_FROM_PEER )
	{
		stdout.Println( "Peer closed on Receive i" );
		return 1;
	}
	
	stdout.Println( "Receive i ok" );
	
	n = tcp_client.Receive( l ) ;
	if( n == tcp.ERROR )
	{
		stdout.Println( "Receive l err" );
		return 1;
	}
	else if( n == tcp.DISCONNECTED_FROM_PEER )
	{
		stdout.Println( "Peer closed on Receive l" );
		return 1;
	}
	
	stdout.Println( "Receive l ok" );
	
	n = tcp_client.Receive( f ) ;
	if( n == tcp.ERROR )
	{
		stdout.Println( "Receive f err" );
		return 1;
	}
	else if( n == tcp.DISCONNECTED_FROM_PEER )
	{
		stdout.Println( "Peer closed on Receive f" );
		return 1;
	}
	
	stdout.Println( "Receive f ok" );
	
	n = tcp_client.Receive( d ) ;
	if( n == tcp.ERROR )
	{
		stdout.Println( "Receive d err" );
		return 1;
	}
	else if( n == tcp.DISCONNECTED_FROM_PEER )
	{
		stdout.Println( "Peer closed on Receive d" );
		return 1;
	}
	
	stdout.Println( "Receive d ok" );
	
	n = tcp_client.Receive( lenstr , 4 ) ;
	if( n == tcp.ERROR )
	{
		stdout.Println( "Receive lenstr err" );
		return 1;
	}
	else if( n == tcp.DISCONNECTED_FROM_PEER )
	{
		stdout.Println( "Peer closed on Receive lenstr" );
		return 1;
	}
	
	stdout.Println( "Receive lenstr ok" );
	
	n = tcp_client.Receive( str , lenstr.ToInt() ) ;
	if( n == tcp.ERROR )
	{
		stdout.Println( "Receive str err" );
		return 1;
	}
	else if( n == tcp.DISCONNECTED_FROM_PEER )
	{
		stdout.Println( "Peer closed on Receive str" );
		return 1;
	}
	
	stdout.Println( "Receive str ok" );
	
	stdout.Println( s );
	stdout.Println( i );
	stdout.Println( l );
	stdout.Println( f );
	stdout.Println( d );
	stdout.Println( str );
	
	tcp_client.Close();
	
	return 0;
}
```

代码示例：`test_tcp_server_2_1.z`

```
import stdtypes stdio thread network ;

function int main( array args )
{
	tcp	tcp_server ;
	tcp	tcp_client ;
	short	s ;
	int	i ;
	long	l ;
	float	f ;
	double	d ;
	string	lenstr ;
	string	str ;
	
	b = tcp_server.Listen( "*" , 9527 ) ;
	if( b != true )
	{
		stdout.Println( "Listen err" );
		return 1;
	}
	
	tcp_client = tcp_server.Accept() ;
	if( tcp_client == null )
	{
		stdout.Println( "Accept err" );
		return 1;
	}
	
	n = tcp_client.Receive( s ) ;
	if( n == tcp.ERROR )
	{
		stdout.Println( "Receive s err" );
		return 1;
	}
	else if( n == tcp.DISCONNECTED_FROM_PEER )
	{
		stdout.Println( "Peer closed on Receive s" );
		return 1;
	}
	
	stdout.Println( "Receive s ok" );
	
	n = tcp_client.Receive( i ) ;
	if( n == tcp.ERROR )
	{
		stdout.Println( "Receive i err" );
		return 1;
	}
	else if( n == tcp.DISCONNECTED_FROM_PEER )
	{
		stdout.Println( "Peer closed on Receive i" );
		return 1;
	}
	
	stdout.Println( "Receive i ok" );
	
	n = tcp_client.Receive( l ) ;
	if( n == tcp.ERROR )
	{
		stdout.Println( "Receive err" );
		return 1;
	}
	else if( n == tcp.DISCONNECTED_FROM_PEER )
	{
		stdout.Println( "Peer closed on Receive l" );
		return 1;
	}
	
	n = tcp_client.Receive( f ) ;
	if( n == tcp.ERROR )
	{
		stdout.Println( "Receive f err" );
		return 1;
	}
	else if( n == tcp.DISCONNECTED_FROM_PEER )
	{
		stdout.Println( "Peer closed on Receive f" );
		return 1;
	}
	
	stdout.Println( "Receive f ok" );
	
	n = tcp_client.Receive( d ) ;
	if( n == tcp.ERROR )
	{
		stdout.Println( "Receive d err" );
		return 1;
	}
	else if( n == tcp.DISCONNECTED_FROM_PEER )
	{
		stdout.Println( "Peer closed on Receive d" );
		return 1;
	}
	
	stdout.Println( "Receive d ok" );
	
	n = tcp_client.Receive( lenstr , 4 ) ;
	if( n == tcp.ERROR )
	{
		stdout.Println( "Receive lenstr err" );
		return 1;
	}
	else if( n == tcp.DISCONNECTED_FROM_PEER )
	{
		stdout.Println( "Peer closed on Receive lenstr" );
		return 1;
	}
	
	stdout.Println( "Receive lenstr ok" );
	
	n = tcp_client.Receive( str , lenstr.ToInt() ) ;
	if( n == tcp.ERROR )
	{
		stdout.Println( "Receive str err" );
		return 1;
	}
	else if( n == tcp.DISCONNECTED_FROM_PEER )
	{
		stdout.Println( "Peer closed on Receive str" );
		return 1;
	}
	
	stdout.Println( "Receive str ok" );
	
	stdout.Println( s );
	stdout.Println( i );
	stdout.Println( l );
	stdout.Println( f );
	stdout.Println( d );
	stdout.Println( str );
	
	s++;
	i++;
	l++;
	f++;
	d++;
	str = str + "2" ;
	
	stdout.Println( s );
	stdout.Println( i );
	stdout.Println( l );
	stdout.Println( f );
	stdout.Println( d );
	stdout.Println( str );
	
	n = tcp_client.Send( s ) ;
	if( n == -1 )
	{
		stdout.Println( "Send s err" );
		return 1;
	}
	
	stdout.Println( "Send s ok" );
	
	n = tcp_client.Send( i ) ;
	if( n == tcp.ERROR )
	{
		stdout.Println( "Send i err" );
		return 1;
	}
	
	stdout.Println( "Send i ok" );
	
	n = tcp_client.Send( l ) ;
	if( n == tcp.ERROR )
	{
		stdout.Println( "Send err" );
		return 1;
	}
	
	n = tcp_client.Send( f ) ;
	if( n == tcp.ERROR )
	{
		stdout.Println( "Send f err" );
		return 1;
	}
	
	stdout.Println( "Send f ok" );
	
	n = tcp_client.Send( d ) ;
	if( n == tcp.ERROR )
	{
		stdout.Println( "Send d err" );
		return 1;
	}
	
	stdout.Println( "Send d ok" );
	
	lenstr.Format( "%04d" , str.Length() );
	n = tcp_client.Send( lenstr ) ;
	if( n == tcp.ERROR )
	{
		stdout.Println( "Send lenstr err" );
		return 1;
	}
	
	stdout.Println( "Send lenstr ok" );
	
	n = tcp_client.Send( str ) ;
	if( n == tcp.ERROR )
	{
		stdout.Println( "Send str err" );
		return 1;
	}
	
	stdout.Println( "Send str ok" );
	
	tcp_client.Close();
	
	tcp_server.Close();
	
	return 0;
}
```

执行

```
$ zlang test_tcp_server_2_1.z
Receive s ok
Receive i ok
Receive f ok
Receive d ok
Receive lenstr ok
Receive str ok
1
234
56789
1.200000
34.560001
hello
2
235
56790
2.200000
35.560001
hello2
Send s ok
Send i ok
Send f ok
Send d ok
Send lenstr ok
Send str ok
```

```
$ zlang test_tcp_client_2_1.z
1
234
56789
1.200000
34.560001
hello
Send s ok
Send i ok
Send l ok
Send f ok
Send d ok
Send lenstr ok
Send str ok
Receive s ok
Receive i ok
Receive l ok
Receive f ok
Receive d ok
Receive lenstr ok
Receive str ok
2
235
56790
2.200000
35.560001
hello2
```

代码示例：`test_tcp_client_3_1.z`

```
import stdtypes stdio thread network ;

function int main( array args )
{
	tcp	tcp_client ;
	
	tcp_client.SetConnectingTimeout( 2000 );
	
	b = tcp_client.Connect( "127.0.0.1" , 9527 ) ;
	if( b != true )
	{
		stdout.Println( "Connect err" );
		return 1;
	}
	
	tcp_client.Close();
	
	return 0;
}
```

代码示例：`test_tcp_server_3_1.z`

```
import stdtypes stdio thread network ;

function int main( array args )
{
	tcp	tcp_server ;
	tcp	tcp_client ;
	string	str ;
	
	b = tcp_server.Listen( "*" , 9527 ) ;
	if( b != true )
	{
		stdout.Println( "Listen err" );
		return 1;
	}
	
	tcp_client = tcp_server.Accept() ;
	if( tcp_client == null )
	{
		stdout.Println( "Accept err" );
		return 1;
	}
	
	stdout.Println( "Accept ok" );
	
	n = tcp_client.Receiveln( str ) ;
	if( n == tcp.ERROR )
	{
		stdout.Println( "Receiveln err" );
		return 1;
	}
	else if( n == tcp.TIMEOUT )
	{
		stdout.Println( "Receiveln timeout" );
		return 2;
	}
	else if( n == tcp.DISCONNECTED_FROM_PEER )
	{
		stdout.Println( "Peer closed on Receiveln" );
		return 3;
	}
	
	stdout.Println( "Receiveln ok" );
	
	tcp_client.Close();
	
	tcp_server.Close();
	
	return 0;
}
```

执行

```
$ zlang test_tcp_server_3_1.z
Accept ok
Peer closed on Receiveln
```

```
$ zlang test_tcp_client_3_1.z

```

代码示例：`test_tcp_client_5_1.z`

```
import stdtypes stdio thread network ;

function int main( array args )
{
	tcp	tcp_client ;
	string	str ;
	
	if( args.Length() == 0 )
	{
		stdout.Print( "input string for sending : " );
		stdin.Scanln( str );
		stdout.Println( "string for sending : " + str );
	}
	else
	{
		str = args.Get(1) ;
	}
	
	b = tcp_client.Connect( "127.0.0.1" , 9527 ) ;
	if( b != true )
	{
		stdout.Println( "Connect err" );
		return 1;
	}
	
	tcp_client.SetDataTransmissionTimeout( 1000 );
	
	n = tcp_client.Sendln( str ) ;
	if( n == tcp.ERROR )
	{
		stdout.Println( "Sendln err" );
		return 1;
	}
	else if( n == tcp.TIMEOUT )
	{
		stdout.Println( "Sendln timeout" );
		return 2;
	}
	
	n = tcp_client.Receiveln( str ) ;
	if( n == tcp.ERROR )
	{
		stdout.Println( "Receiveln err" );
		return 1;
	}
	else if( n == tcp.TIMEOUT )
	{
		stdout.Println( "Receiveln timeout" );
		return 2;
	}
	else if( n == tcp.DISCONNECTED_FROM_PEER )
	{
		stdout.Println( "Peer closed or recv timeout on Receiveln" );
		return 3;
	}
	
	stdout.Println( "get response from server : " + str );
	
	stdout.Println( tcp_client.GetDataTransmissionElapse() );
	
	tcp_client.Close();
	
	return 0;
}
```

代码示例：`test_tcp_server_5_1.z`

```
import stdtypes stdio system thread network ;

function int main( array args )
{
	tcp	tcp_server ;
	tcp	tcp_client ;
	string	str ;
	
	b = tcp_server.Listen( "*" , 9527 ) ;
	if( b != true )
	{
		stdout.Println( "Listen err" );
		return 1;
	}
	
	tcp_client = tcp_server.Accept() ;
	if( tcp_client == null )
	{
		stdout.Println( "Accept err" );
		return 1;
	}
	
	n = tcp_client.Receiveln( str ) ;
	if( n == tcp.ERROR )
	{
		stdout.Println( "Receiveln err" );
		return 1;
	}
	else if( n == tcp.DISCONNECTED_FROM_PEER )
	{
		stdout.Println( "Peer closed on Receiveln" );
		return 1;
	}
	
	stdout.Println( str );
	
	system.SleepSeconds( 2 );
	
	n = tcp_client.Sendln( str ) ;
	if( n == tcp.ERROR )
	{
		stdout.Println( "Sendln err" );
		return 11;
	}
	
	tcp_client.Close();
	
	tcp_server.Close();
	
	return 0;
}
```

执行

```
$ zlang test_tcp_server_5_1.z
he
Sendln err
```

```
$ zlang test_tcp_client_5_1.z he
Receiveln timeout
```

## http

http对象名：`http`

| 成员类型 | 成员名字和原型                                | 说明                                                         | (错误.)(异常码,)异常消息                                     |
| -------- | --------------------------------------------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| 常量     | string HTTP_METHOD_GET                        | HTTP的GET方法请求                                            |                                                              |
| 常量     | string HTTP_METHOD_POST                       | HTTP的POST方法请求                                           |                                                              |
| 常量     | string HTTP_VERSION_1_0                       | HTTP的1.0版本                                                |                                                              |
| 常量     | string HTTP_VERSION_1_1                       | HTTP的1.1版本                                                |                                                              |
| 常量     | int HTTP_STATUSCODE_OK                        | HTTP响应成功                                                 |                                                              |
| 常量     | int HTTP_STATUSCODE_CREATED                   | HTTP响应已创建                                               |                                                              |
| 常量     | int HTTP_STATUSCODE_NO_CONTENT                | HTTP响应没有体数据                                           |                                                              |
| 常量     | int HTTP_STATUSCODE_BAD_REQUEST               | HTTP响应请求有误                                             |                                                              |
| 常量     | int HTTP_STATUSCODE_FORBIDDEN                 | HTTP响应没有权限                                             |                                                              |
| 常量     | int HTTP_STATUSCODE_NOT_FOUND                 | HTTP响应页不存在                                             |                                                              |
| 常量     | int HTTP_STATUSCODE_METHOD_NOT_ALLOWED        | HTTP响应方法无效                                             |                                                              |
| 常量     | int HTTP_STATUSCODE_INTERNAL_SERVER_ERROR     | HTTP响应服务端内部错误                                       |                                                              |
| 函数     | void SetConnectingTimeout(int);               | 设置TCP连接超时时间（毫秒）                                  |                                                              |
| 函数     | void SetDataTransmissionTimeout(int);         | 设置TCP数据传输超时时间（毫秒）                              |                                                              |
| 函数     | int GetConnectingElapse();                    | 得到上次TCP连接剩余时间（毫秒）                              |                                                              |
| 函数     | bool Listen(string,int);                      | 创建TCP服务端侦听，输入参数是IP和PORT，返回是否成功          | http.SOCKET_ERROR,EXCEPTION_MESSAGE_LISTEN_SOCKET_FAILED     |
| 函数     | tcp Accept();                                 | TCP服务端接受TCP客户端新连接，返回tcp对象                    | http.SOCKET_ERROR,EXCEPTION_MESSAGE_ACCEPT_SOCKET_FAILED     |
| 函数     | bool Connect(string,int);                     | TCP客户端连接TCP服务端，输入参数是IP和PORT，返回是否成功     | http.SOCKET_ERROR,EXCEPTION_MESSAGE_CONNECT_SOCKET_FAILED    |
| 函数     | bool Close();                                 | TCP服务端或TCP客户端关闭连接，返回是否成功                   | http.SOCKET_ERROR,EXCEPTION_MESSAGE_CLOSE_SOCKET_FAILED      |
| 函数     | string GetIp();                               | 得到TCP的IP，TCP服务端返回侦听IP，TCP服务端接受tcp返回TCP客户端IP，TCP客户端返回本地IP | http.SOCKET_ERROR,EXCEPTION_MESSAGE_SOCKET_INVALID           |
| 函数     | int GetPort();                                | 得到TCP的PORT，TCP服务端返回侦听PORT，TCP服务端接受tcp返回TCP客户端PORT，TCP客户端返回本地PORT | http.SOCKET_ERROR,EXCEPTION_MESSAGE_SOCKET_INVALID           |
| 函数     | bool SetHttpRequestUri(string,string,string); | 客户端：发起HTTP请求前，设置HTTP方法、URI、HTTP版本          | http.HTTP_ERROR,EXCEPTION_MESSAGE_HTTP_METHOD_INVALID<br />http.HTTP_ERROR,EXCEPTION_MESSAGE_HTTP_URI_INVALID<br />http.HTTP_ERROR,EXCEPTION_MESSAGE_HTTP_VERSION_INVALID |
| 函数     | bool SetHttpRequestHeader(string,string);     | 客户端：发起HTTP请求前，设置HTTP头的键和值                   | http.HTTP_ERROR,EXCEPTION_MESSAGE_NO_HTTP_ENVIRONMENT        |
| 函数     | bool SetHttpRequestBody(string);              | 客户端：发起HTTP请求前，设置HTTP体数据                       | http.HTTP_ERROR,EXCEPTION_MESSAGE_NO_HTTP_ENVIRONMENT        |
| 函数     | bool RequestHttp();                           | 客户端：发起HTTP请求、接收HTTP响应，返回是否成功             | http.HTTP_ERROR,EXCEPTION_MESSAGE_SOCKET_INVALID<br />http.HTTP_ERROR,EXCEPTION_MESSAGE_NO_HTTP_ENVIRONMENT<br />http.HTTP_ERROR,EXCEPTION_MESSAGE_REQUESTHTTP_FAILED |
| 函数     | int GetHttpResponseStatus();                  | 客户端：得到HTTP响应码                                       | http.HTTP_ERROR,EXCEPTION_MESSAGE_NO_HTTP_ENVIRONMENT<br />http.HTTP_ERROR,EXCEPTION_MESSAGE_GET_HTTP_STATUS_CODE_FAILED |
| 函数     | string GetHttpResponseHeader(string);         | 客户端：得到HTTP响应头，指定键，返回值                       | http.HTTP_ERROR,EXCEPTION_MESSAGE_NO_HTTP_ENVIRONMENT<br />http.HTTP_ERROR,EXCEPTION_MESSAGE_QUERY_HTTP_HEADER_FAILED |
| 函数     | string GetHttpResponseBody();                 | 客户端：得到HTTP响应体                                       | http.HTTP_ERROR,EXCEPTION_MESSAGE_NO_HTTP_ENVIRONMENT<br />http.HTTP_ERROR,EXCEPTION_MESSAGE_GET_HTTP_BODY_FAILED |
| 函数     | bool ReceiveHttpRequest();                    | 服务端：接收HTTP请求                                         | http.HTTP_ERROR,EXCEPTION_MESSAGE_RECEIVE_HTTP_REQUEST_FAILED |
| 函数     | string GetHttpRequestHeader(string);          | 服务端：得到HTTP请求头，指定键，返回值                       | http.HTTP_ERROR,EXCEPTION_MESSAGE_NO_HTTP_ENVIRONMENT<br />http.HTTP_ERROR,EXCEPTION_MESSAGE_QUERY_HTTP_HEADER_FAILED |
| 函数     | string GetHttpRequestBody();                  | 服务端：得到HTTP请求体                                       | http.HTTP_ERROR,EXCEPTION_MESSAGE_NO_HTTP_ENVIRONMENT<br />http.HTTP_ERROR,EXCEPTION_MESSAGE_GET_HTTP_BODY_FAILED |
| 函数     | bool SetHttpResponseStatus(int);              | 服务端：设置HTTP响应码                                       |                                                              |
| 函数     | bool SetHttpResponseHeader(string,string);    | 服务端：设置HTTP响应头键值                                   | http.HTTP_ERROR,EXCEPTION_MESSAGE_NO_HTTP_ENVIRONMENT        |
| 函数     | bool SetHttpResponseBody(string);             | 服务端：设置HTTP响应体                                       | http.HTTP_ERROR,EXCEPTION_MESSAGE_NO_HTTP_ENVIRONMENT        |
| 函数     | bool SendHttpResponse();                      | 服务端：发送HTTP响应                                         | http.SOCKET_ERROR,EXCEPTION_MESSAGE_SOCKET_INVALID<br />http.HTTP_ERROR,EXCEPTION_MESSAGE_NO_HTTP_ENVIRONMENT |

代码示例i：`test_http_client_1_1.z`

该代码示范了HTTP客户端的连接服务端，设置HTTP头和HTTP体，发送HTTP请求并接收HTTP响应，显示HTTP响应码、HTTP头和HTTP体。

```
import stdtypes stdio network ;

// test :
// zlang test_http_client_1.z
// vl-zlang-debug.sh test_http_client_1.z
// vl-zlang-debug.sh test_http_client_1.z >/tmp/calvin2 2>&1

function int main( array args )
{
        http    http_client ;

        r = http_client.Connect( "127.0.0.1" , 8080 ) ;
        if( r != true )
        {
                stdout.Println( "Connect err" );
                return 1;
        }

        http_client.SetDataTransmissionTimeout( 10 );
        http_client.SetHttpRequestUri( http.HTTP_METHOD_GET , "/hello" , http.HTTP_VERSION_1_1 );
        http_client.SetHttpRequestHeader( "Host" , "www.mydomain.com" );
        http_client.SetHttpRequestHeader( "User-Agent" , "Mozilla/5.0 (Windows NT 5.1; rv:45.0) Gecko/20100101 Firefox/45.0" );
        http_client.SetHttpRequestBody( "{'name':'value'}" );

        stdout.Println( "request http ..." );
        r = http_client.RequestHttp();
        if( r != true )
        {
                stdout.Println( "request http err" );
                return 1;
        }
        stdout.Println( "request http ok" );

        status_code = http_client.GetHttpResponseStatus() ;
        stdout.FormatPrintln( "status_code : %d" , status_code );
        cookie_value = http_client.GetHttpResponseHeader( "Cookie" ) ;
        stdout.Println( "http header 'Cookie' value : " + cookie_value );
        http_body = http_client.GetHttpRequestBody() ;
        stdout.Println( "http body : " + http_body );

        http_client.Close();

        return 0;
}
```

代码示例：`test_http_server_1_1.z`

该代码示范了HTTP服务端接受HTTP客户端连接，接收HTTP请求，显示HTTP请求头和请求体，设置HTTP响应头和HTTP响应体并发送。

```
import stdtypes stdio network ;

/*
test :
zlang test_http_server_1.z
vl-zlang-debug.sh test_http_server_1.z
vl-zlang-debug.sh test_http_server_1.z >/tmp/calvin1 2>&1
*/

function int main( array args )
{
        http    http_server ;
        http    http_client ;

        r = http_server.Listen( "*" , 8080 ) ;
        if( r != true )
        {
                stdout.Println( "Listen err" );
                return 1;
        }

        http_client = http_server.Accept() ;
        if( http_client == null )
        {
                stdout.Println( "Accept err" );
                return 1;
        }

        stdout.Println( "receive http request ..." );
        r = http_client.ReceiveHttpRequest() ;
        if( r != true )
        {
                stdout.Println( "receive http request err" );
                return 1;
        }
        stdout.Println( "receive http request ok" );

        http_header_value = http_client.GetHttpRequestHeader( "Host" ) ;
        if( http_header_value == null )
        {
                stdout.Println( "expect http header 'Host'" );
                return 1;
        }
        stdout.Println( "http header 'Host' : " + http_header_value );

        http_header_value = http_client.GetHttpRequestHeader( "User-Agent" ) ;
        if( http_header_value == null )
        {
                stdout.Println( "expect http header 'User-Agent'" );
                return 1;
        }
        stdout.Println( "http header 'User-Agent' : " + http_header_value );

        http_body = http_client.GetHttpRequestBody() ;
        if( http_body == null )
        {
                stdout.Println( "expect http body" );
                return 1;
        }
        stdout.Println( "http body : " + http_body );

        r = http_client.SetHttpResponseStatus( 200 ) ;
        if( r != true )
        {
                stdout.Println( "set response status err" );
                return 1;
        }
        stdout.Println( "set response status ok" );

        r = http_client.SetHttpResponseHeader( "Cookie" , "UID=1E27B789D33BF3C43C6022BD0182CF8F" ) ;
        if( r != true )
        {
                stdout.Println( "set response header 'Cookie' err" );
                return 1;
        }
        stdout.Println( "set response header 'Cookie' ok" );

        r = http_client.SetHttpResponseBody( "<html>OK</html>" ) ;
        if( r != true )
        {
                stdout.Println( "set response body err" );
                return 1;
        }
        stdout.Println( "set response body ok" );

        r = http_client.SendHttpResponse() ;
        if( r != true )
        {
                stdout.Println( "send http response err" );
                return 1;
        }
        stdout.Println( "send http response ok" );

        http_client.Close();

        http_server.Close();

        return 0;
}
```

执行

```
$ zlang test_http_server_1_1.z
receive http request ...
receive http request ok
http header 'Host' : www.mydomain.com
http header 'User-Agent' : Mozilla/5.0 (Windows NT 5.1; rv:45.0) Gecko/20100101 Firefox/45.0
http body : {'name':'value'}
set response status ok
set response header 'Cookie' ok
set response body ok
send http response ok
```

```
$ zlang test_http_client_1_1.z
request http ...
request http ok
status_code : 200
http header 'Cookie' value : UID=1E27B789D33BF3C43C6022BD0182CF8F
http body : <html>OK</html>
```

## httpclient

`httpclient`用于做HTTP客户端时的高阶、简洁编程。

| 成员类型 | 成员名字和原型                    | 说明                                                         | (错误.)(异常码,)异常消息                                     |
| -------- | --------------------------------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| 函数     | int Get(string,list,map,string);  | 以GET方法发起HTTP请求，输入参数依次是URL、HTTP请求头列表、HTTP响应头列表、HTTP请求体/HTTP响应体，返回HTTP响应码；URL可以使用https | httpclient.HTTPCLIENT_ERROR,EXCEPTION_MESSAGE_ANALYSE_URL_FAILED<br />httpclient.HTTPCLIENT_ERROR,EXCEPTION_MESSAGE_GET_IP_BY_DOMAIN_NAME_FAILED<br />httpclient.HTTPCLIENT_ERROR,EXCEPTION_MESSAGE_ASSEMBLE_HTTP_REQUEST_HEADERS_FAILED<br />httpclient.HTTPCLIENT_ERROR,EXCEPTION_MESSAGE_ASSEMBLE_HTTP_REQUEST_BODY_FAILED<br />tcp.SOCKET_ERROR,EXCEPTION_MESSAGE_CONNECT_SOCKET_FAILED<br />tcp.SOCKET_ERROR,EXCEPTION_MESSAGE_SSL_NEW_FAILED<br />tcp.SOCKET_ERROR,EXCEPTION_MESSAGE_SSL_CONNECT_FAILED<br />httpclient.HTTPCLIENT_ERROR,EXCEPTION_MESSAGE_EXTRACT_HTTP_RESPONSE_HEADERS_FAILED<br />httpclient.HTTPCLIENT_ERROR,EXCEPTION_MESSAGE_EXTRACT_HTTP_RESPONSE_BODY_FAILED |
| 函数     | int Post(string,list,map,string); | 以POST方法发起HTTP请求，输入参数依次是URL、HTTP请求头列表、HTTP响应头列表、HTTP请求体/HTTP响应体，返回HTTP响应码；URL可以使用https | httpclient.HTTPCLIENT_ERROR,EXCEPTION_MESSAGE_ANALYSE_URL_FAILED<br />httpclient.HTTPCLIENT_ERROR,EXCEPTION_MESSAGE_GET_IP_BY_DOMAIN_NAME_FAILED<br />httpclient.HTTPCLIENT_ERROR,EXCEPTION_MESSAGE_ASSEMBLE_HTTP_REQUEST_HEADERS_FAILED<br />httpclient.HTTPCLIENT_ERROR,EXCEPTION_MESSAGE_ASSEMBLE_HTTP_REQUEST_BODY_FAILED<br />tcp.SOCKET_ERROR,EXCEPTION_MESSAGE_CONNECT_SOCKET_FAILED<br />tcp.SOCKET_ERROR,EXCEPTION_MESSAGE_SSL_NEW_FAILED<br />tcp.SOCKET_ERROR,EXCEPTION_MESSAGE_SSL_CONNECT_FAILED<br />httpclient.HTTPCLIENT_ERROR,EXCEPTION_MESSAGE_EXTRACT_HTTP_RESPONSE_HEADERS_FAILED<br />httpclient.HTTPCLIENT_ERROR,EXCEPTION_MESSAGE_EXTRACT_HTTP_RESPONSE_BODY_FAILED |
| 函数     | list GetCookies();                | 得到最近HTTP请求后得到的响应中的响应头中的Cookie，一般用于保存上一响应的Cookie |                                                              |
| 函数     | string FormatCookies(list);       | 格式化Cookie列表转换为HTTP请求头Cookie值格式，一般用于把上一响应中的Cookie传递给下一个请求 |                                                              |

代码示例：`test_httpclient_2_1.z`

该代码示范了HTTP客户端的高阶编程：准备HTTP请求数据，发送并接收HTTP响应，写入文件。

```
import stdtypes stdio thread network ;

function int main( array args )
{
        list    req_headers ;
        map     rsp_headers ;
        string  rsp_html ;
        stdfile f ;

        req_headers.AddTail( <object> "Host: www.163.com" );
        req_headers.AddTail( <object> "User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/120.0.0.0 Safari/537.36" );
        req_headers.AddTail( <object> "Accept: */*" );
        r = httpclient.Get( "https://www.163.com/" , req_headers , rsp_headers , rsp_html ) ;
        if( r != http.HTTP_STATUSCODE_OK )
        {
                stdout.FormatPrintln( "httpclient.Get err[%d]" , r );
                return 1;
        }
        else
        {
                stdout.Println( "httpclient.Get ok" );
        }

        f.Open( "/tmp/test_httpclient_2_1.html" , "wb" );
        f.Println( rsp_html );
        f.Close();

        return 0;
}
```

执行

```
$ zlang test_httpclient_2_1.z
httpclient.Get ok
$ head /tmp/test_httpclient_2_1.html
head /tmp/test_httpclient_2_1.html
<!DOCTYPE HTML>
<!--[if IE 6 ]> <html class="ne_ua_ie6 ne_ua_ielte8" id="ne_wrap"> <![endif]-->
<!--[if IE 7 ]> <html class="ne_ua_ie7 ne_ua_ielte8" id="ne_wrap"> <![endif]-->
<!--[if IE 8 ]> <html class="ne_ua_ie8 ne_ua_ielte8" id="ne_wrap"> <![endif]-->
<!--[if IE 9 ]> <html class="ne_ua_ie9" id="ne_wrap"> <![endif]-->
<!--[if (gte IE 10)|!(IE)]><!--> <html phone="1" id="ne_wrap"> <!--<![endif]-->
<head>
<meta name="google-site-verification" content="PXunD38D6Oui1T44OkAPSLyQtFUloFi5plez040mUOc" />
<meta name="baidu-site-verification" content="oiT8OEfzes" />
<meta name="360-site-verification" content="527ad00f66a93c31134d6a20b2246950" />
...
```

代码示例：`test_httpclient_3_1.z`

该代码示范了HTTPS客户端的高阶编程。 

```
import stdtypes stdio thread network ;

function int main( array args )
{
        list    req_headers ;
        map     rsp_headers ;
        string  html ;
        stdfile f ;

        req_headers.AddTail( <object> "Host: www.163.com" );
        req_headers.AddTail( <object> "User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/120.0.0.0 Safari/537.36" );
        req_headers.AddTail( <object> "Accept: */*" );
        html = "key1=value1&key2=value2" ;
        r = httpclient.Post( "https://www.163.com/" , req_headers , rsp_headers , html ) ;
        if( r != http.HTTP_STATUSCODE_METHOD_NOT_ALLOWED )
        {
                stdout.FormatPrintln( "httpclient.Post err[%d]" , r );
                return 1;
        }
        else
        {
                stdout.Println( "httpclient.Post ok" );
        }

        f.Open( "/tmp/test_httpclient_3_1.html" , "wb" );
        f.Println( html );
        f.Close();

        return 0;
}
```

代码示例：`test_httpclient_10_2.z`

该代码示范了HTTPS客户端的高阶编程：传递Cookies。

```
import stdtypes stdio thread network xml ;

function int main( array args )
{
        list    req_headers ;
        map     rsp_headers ;
        string  rsp_html ;

        req_headers.AddTail( <object> "User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/120.0.0.0 Safari/537.36" );
        req_headers.AddTail( <object> "Accept: */*" );
        req_headers.AddTail( <object> "Accept-Language: zh-CN,zh;q=0.9" );
        req_headers.AddTail( <object> "Host: cn.bing.com" );
        r = httpclient.Get( "https://cn.bing.com/" , req_headers , rsp_headers , rsp_html ) ;
        if( r != http.HTTP_STATUSCODE_OK )
        {
                stdout.FormatPrintln( "httpclient.Get err[%d]" , r );
                return 1;
        }
        else
        {
                stdout.FormatPrintln( "httpclient.Get ok" );
        }

        list    cookies = httpclient.GetCookies() ;

        foreach( string c in cookies )
        {
                stdout.FormatPrintln( "Cookie : [%s]" , c );
        }

        list    new req_headers ;
        map     new rsp_headers ;
        string  new rsp_html ;

        req_headers.AddTail( <object> "User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/120.0.0.0 Safari/537.36" );
        req_headers.AddTail( <object> "Accept: */*" );
        req_headers.AddTail( <object> "Accept-Language: zh-CN,zh;q=0.9" );
        req_headers.AddTail( <object> "Host: cn.bing.com" );
        req_headers.AddTail( <object> httpclient.FormatCookies(cookies) );
        r = httpclient.Get( "https://cn.bing.com/" , req_headers , rsp_headers , rsp_html ) ;
        if( r != http.HTTP_STATUSCODE_OK )
        {
                stdout.FormatPrintln( "httpclient.Get err[%d]" , r );
                return 1;
        }
        else
        {
                stdout.FormatPrintln( "httpclient.Get ok" );
        }

        return 0;
}
```

## httpserver

`httpserver`用于HTTP服务端的高阶处理，比如创建一个WEB服务器、CGI服务器、RESTful服务器。

| 成员类型 | 成员名字和原型                                            | 说明                                                         | (错误.)(异常码,)异常消息                                     |
| -------- | --------------------------------------------------------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| 函数     | void SetListenAddress(string,int);                        | 设置本地侦听地址，输入参数为IP、PORT                         |                                                              |
| 函数     | void SetDomain(string);                                   | 设置域名                                                     |                                                              |
| 函数     | void SetWwwroot(string);                                  | 设置静态资源根路径                                           |                                                              |
| 函数     | void SetIndex(string);                                    | 设置目录缺省静态资源文件名                                   |                                                              |
| 函数     | void SetSocgiType(string);                                | 设置动态页面的文件扩展名                                     |                                                              |
| 函数     | void SetLogFile(string,string);                           | 设置日志文件名，输入参数为access日志文件名（一次HTTP请求写一行）、error日志文件名（按不同日志等级输出httpserver内部日志） |                                                              |
| 函数     | void DisableXForwardedFor(bool);                          | 禁用HTTP头选项X-Forwarded-For；如果启用，且请求中带有该头，服务端应用看到的客户端IP不是真实IP，而是HTTP头选项X-Forwarded-For的值，一般用于前面有反向代理转发时保留的源地址 |                                                              |
| 函数     | void SetWorkerProcesses(int);                             | 设置httpserver进程池规模；httpserver为静态进程池模式，启动后派生固定数量的进程 |                                                              |
| 函数     | int Run();                                                | 以上设置好后把httpserver运行起来                             | httpserver.HTTPSERVER_ERROR,EXCEPTION_MESSAGE_RUN_HTTPSERVER_FAILED |
| 函数     | string GetError();                                        | Run()如果报错，可以调用这个函数获取错误的具体描述            |                                                              |
| 函数     | string GetHttpRequestIp();                                | 服务端：得到客户端IP                                         |                                                              |
| 函数     | string GetHttpRequestMethod();                            | 服务端：得到客户端送过来的HTTP方法                           | httpserver.HTTPSERVER_ERROR,EXCEPTION_MESSAGE_GET_HTTP_METHOD_FAILED |
| 函数     | string GetHttpRequestUri();                               | 服务端：得到客户端送过来的HTTP URI                           | httpserver.HTTPSERVER_ERROR,EXCEPTION_MESSAGE_GET_HTTP_URI_FAILED |
| 函数     | string GetHttpRequestUriPath(int);                        | 服务端：得到客户端送过来的URI的目录段，输入参数为段号，如"/a/b/c"第二段是"b" | httpserver.HTTPSERVER_ERROR,EXCEPTION_MESSAGE_HTTP_URI_PATH_SEGMENT_NO_INVALID<br />httpserver.HTTPSERVER_ERROR,EXCEPTION_MESSAGE_GET_HTTP_URI_FAILED<br />httpserver.HTTPSERVER_ERROR,EXCEPTION_MESSAGE_HTTP_URI_INVALID |
| 函数     | string GetHttpRequest();                                  | 服务端：得到客户端送过来的HTTP请求体                         |                                                              |
| 函数     | map GetHttpRequestUriParameters();                        | 服务端：得到客户端送过来的HTTP请求URI参数列表                | httpserver.HTTPSERVER_ERROR,EXCEPTION_MESSAGE_NO_HTTP_URI_PARAMETER |
| 函数     | map GetHttpRequestPost();                                 | 服务端：得到客户端送过来的HTTP请求体键值（HTTP Post格式）列表 | httpserver.HTTPSERVER_ERROR,EXCEPTION_MESSAGE_NO_HTTP_BODY   |
| 函数     | map GetHttpRequestCookies();                              | 服务端：得到客户端送过来的HTTP请求头Cookies                  |                                                              |
| 函数     | bool SetHttpResponse(string);                             | 服务端：设置待发送客户端的HTTP响应体，给定报文               |                                                              |
| 函数     | bool SetHttpResponseFromFile(string);                     | 服务端：设置待发送客户端的HTTP响应体，从文件读               |                                                              |
| 函数     | bool SetHttpResponseFromHtmlTemplate(string,htmlsection); | 服务端：设置待发送客户端的HTTP响应体，输入参数为模板文件名、实例化数据容器 | httpserver.HTTPSERVER_ERROR,EXCEPTION_MESSAGE_INSTANTIATE_TEMPLATE |
| 函数     | bool FormatHttpResponse(...);                             | 服务端：设置待发送客户端的HTTP响应体，从格式化字符串拼装     |                                                              |
| 函数     | bool WriteHttpResponseHeader(string,string);              | 服务端：逐步写模式，写待发送客户端的HTTP响应头，输入参数为键、值 |                                                              |
| 函数     | bool WriteHttpResponseCookie(string,string,int)；         | 服务端：逐步写模式，写待发送客户端的HTTP响应头Cookie，输入参数为键、值、存活秒数 |                                                              |
| 函数     | bool WriteHttpResponseBody(string);                       | 服务端：逐步写模式，写待发送客户端的HTTP响应体               |                                                              |
| 函数     | bool FormatWriteHttpResponseBody(...);                    | 服务端：逐步写模式，写待发送客户端的HTTP响应体，从格式化字符串拼装 |                                                              |
| 函数     | bool WriteHttpResponseEnd();                              | 服务端：逐步写模式，写待发送客户端的HTTP响应完毕             |                                                              |

代码示例：`test_httpserver_1_1.z`

该代码示范了RESTful服务的编写，所有HTTP服务端处理细节都由我的开源项目`hetao`封装。

```
import stdtypes stdio thread network ;

/*
curl -vvv -X GET -0 'http://192.168.11.79:9527/hello'
*/
function int `GET /hello`( httpserver s )
{
        s.SetHttpResponse( "hello world" );

        return http.HTTP_STATUSCODE_OK;
}

function int main( array args )
{
        httpserver.SetListenAddress( "" , 9527 );
        httpserver.SetDomain( "*" );
        httpserver.SetLogFile( "log/access.log" , "log/error.log" );
        r = httpserver.Run() ;
        if( r )
        {
                stdout.FormatPrintln( "httpserver.Run err[%d][%s]" , r , httpserver.GetError() );
                return 1;
        }
        else
        {
                stdout.Println( "httpserver.Run normal ended" );
        }

        return 0;
}
```

注意：zlang反引号\`用于表达包含白字符（空格、TAB、换行）的标识，`httpserver`利用这一点，直接要求把函数名设置成HTTP方法和URI的格式，直观、简单，无需配置或注解。

执行

```
$ zlang test_httpserver_1_1.z
```

```
$ curl -vvv -X GET -0 'http://192.168.11.79:9527/hello'
* About to connect() to 192.168.11.79 port 9527 (#0)
*   Trying 192.168.11.79...
* Connected to 192.168.11.79 (192.168.11.79) port 9527 (#0)
> GET /hello HTTP/1.0
> User-Agent: curl/7.29.0
> Host: 192.168.11.79:9527
> Accept: */*
>
* HTTP 1.0, assume close after body
< HTTP/1.0 200 OK
< Content-length: 11
<
* Closing connection 0
hello world
```

代码示例：`test_httpserver_2_1.z`

该代码示范了某一WEB页面的响应HTML由代码拼接的写法，数据从URL中取得。

```
import stdtypes stdio thread network ;

/*
curl -vvv -X GET -0 'http://192.168.11.79:9527/querytest?page_size=10&page_no=1'
*/
function int `GET /querytest`( httpserver s )
{
        string  uri = s.GetHttpRequestUri() ;

        s.WriteHttpResponseBody( "<p>HTTP-URI[" + uri + "]</p>\n" );
        s.WriteHttpResponseBody( "<hr>\n" );

        map m = s.GetHttpRequestUriParameters() ;

        int page_size = m.Get( "page_size" ).ToInt() ;
        s.FormatWriteHttpResponseBody( "<p>page_size[%d]</p>\n" , page_size );

        int page_no = m.Get( "page_no" ).ToInt() ;
        s.FormatWriteHttpResponseBody( "<p>page_no[%d]</p>\n" , page_no );

        s.WriteHttpResponseEnd();

        return http.HTTP_STATUSCODE_OK;
}

function int main( array args )
{
        httpserver.SetListenAddress( "" , 9527 );
        httpserver.SetDomain( "*" );
        httpserver.SetLogFile( "log/access.log" , "log/error.log" );
        r = httpserver.Run() ;
        if( r )
        {
                stdout.FormatPrintln( "httpserver.Run err[%d][%s]" , r , httpserver.GetError() );
                return 1;
        }
        else
        {
                stdout.Println( "httpserver.Run normal ended" );
        }

        return 0;
}
```

执行

```
$ zlang test_httpserver_2_1.z
```

```
curl -vvv -X GET -0 'http://192.168.11.79:9527/querytest?page_size=10&page_no=1'
* About to connect() to 192.168.11.79 port 9527 (#0)
*   Trying 192.168.11.79...
* Connected to 192.168.11.79 (192.168.11.79) port 9527 (#0)
> GET /querytest?page_size=10&page_no=1 HTTP/1.0
> User-Agent: curl/7.29.0
> Host: 192.168.11.79:9527
> Accept: */*
>
* HTTP 1.0, assume close after body
< HTTP/1.0 200 OK
< Content-length: 95
<
<p>HTTP-URI[/querytest?page_size=10&page_no=1]</p>
<hr>
<p>page_size[10]</p>
<p>page_no[1]</p>
* Closing connection 0
```

代码示例：`test_httpserver_3_1.z`

该代码示范了另一个代码拼接HTML的例子，数据从HTTP POST表单里取得。

```
import stdtypes stdio thread network ;

function int `POST /formtest`( httpserver s )
{
        req = s.GetHttpRequest();

        s.WriteHttpResponseBody( "<p>HTTP-REQUEST[" + req + "]</p>\n" );
        s.WriteHttpResponseBody( "<hr>\n" );

        s.WriteHttpResponseHeader( "Set-Cookie" , "myname=myvalue" );

        m = s.GetHttpRequestPost();

        name = m.Get( "name" ) ;
        s.FormatWriteHttpResponseBody( "<p>name[%s]</p>\n" , name );

        email = m.Get( "email" ) ;
        s.FormatWriteHttpResponseBody( "<p>email[%s]</p>\n" , email );

        checkbox1 = m.Get( "checkbox1" ) ;
        s.FormatWriteHttpResponseBody( "<p>checkbox1[%s]</p>\n" , checkbox1 );

        checkbox2 = m.Get( "checkbox2" ) ;
        s.FormatWriteHttpResponseBody( "<p>checkbox2[%s]</p>\n" , checkbox2 );

        checkbox3 = m.Get( "checkbox3" ) ;
        s.FormatWriteHttpResponseBody( "<p>checkbox3[%s]</p>\n" , checkbox3 );

        radio1 = m.Get( "radio1" ) ;
        s.FormatWriteHttpResponseBody( "<p>radio1[%s]</p>\n" , radio1 );

        radio2 = m.Get( "radio2" ) ;
        s.FormatWriteHttpResponseBody( "<p>radio2[%s]</p>\n" , radio2 );

        select1 = m.Get( "select1" ) ;
        s.FormatWriteHttpResponseBody( "<p>select1[%s]</p>\n" , select1 );

        submit1 = m.Get( "submit1" ) ;
        s.FormatWriteHttpResponseBody( "<p>submit1[%s]</p>\n" , submit1 );

        s.WriteHttpResponseEnd();

        return http.HTTP_STATUSCODE_OK;
}

function int main( array args )
{
        httpserver.SetListenAddress( "192.168.11.79" , 9527 );
        httpserver.SetDomain( "*" );
        httpserver.SetLogFile( "log/access.log" , "log/error.log" );
        r = httpserver.Run() ;
        if( r )
        {
                stdout.FormatPrintln( "httpserver.Run err[%d][%s]" , r , httpserver.GetError() );
                return 1;
        }
        else
        {
                stdout.Println( "httpserver.Run normal ended" );
        }

        return 0;
}
```

代码示例：`test_httpserver_5_1.z`

该代码示范了从URL路径中取得每一段参数的写法。

```
import stdtypes stdio network ;

function int `GET /company/*/user/*`( httpserver s )
{
        int     company_id = s.GetHttpRequestUriPath(2).ToInt() ;
        int     user_id = s.GetHttpRequestUriPath(4).ToInt() ;

        s.FormatWriteHttpResponseBody( "company_id[%d] user_id[%d]" , company_id , user_id );
        s.WriteHttpResponseEnd();

        return http.HTTP_STATUSCODE_OK;
}

function int main( array args )
{
        httpserver.SetListenAddress( "" , 9527 );
        httpserver.SetDomain( "*" );
        httpserver.SetLogFile( "log/access.log" , "log/error.log" );
        r = httpserver.Run() ;
        if( r )
        {
                stdout.FormatPrintln( "httpserver.Run err[%d][%s]" , r , httpserver.GetError() );
                return 1;
        }
        else
        {
                stdout.Println( "httpserver.Run normal ended" );
        }

        return 0;
}
```

## htmlsection

`htmlsection`用于实例化HTML模板的数据容器。

在代码中填充好`htmlsection`数据容器，指定模板文件名，调用模板实例化函数，一个实例化的HTML就出来了。

模板示例：`test_httpserver_htmlsection_1_1.html`

```
<html>
<head>
<title>((HEADER_TITLE))</title>
</head>
<body>
$${UNLOGIN/
<p>Please login first</p>
$$/UNLOGIN}
$${LOGIN/
<p>USER : $$(USER)</p>
<table border="1">
        <tr>
                <td>ID</td>
                <td>NAME</td>
                <td>AMOUNT</td>
        </tr>
        $$[DETAIL/
        <tr>
                <td>$$(ID)</td>
                <td>$$(NAME)</td>
                <td>$$(AMOUNT:%.1lf)</td>
        </tr>
        $$/DETAIL]
</table>
<p>      TOTAL  : $$(TOTAL)</p>
<p>TOTAL_AMOUNT : $$(TOTAL_AMOUNT)</p>
<p>    DATETIME : $$(DATETIME)</p>
$$/LOGIN}
</body>
</html>
```

`$$(HEADER_TITLE)`表示待替换的字符串占位，`HEADER_TITLE`是占位键。占位键有部可选设置控制格式。

`$${UNLOGIN/`和`$$/UNLOGIN}`成对出现，表示数据容器中存在`UNLOGIN`子区域键时，实例化HTML中出现一块子区域。

`$$[DETAIL/`和`$$/DETAIL]`成对出现，表示数据容器中存在DETAIL明细子区域键时，循环实例化该明细子区域。

代码示例：`test_httpserver_htmlsection_1_1.z`

该代码示范了基于HTML模板实例化得到响应HTML的写法，基于我的开源项目通用模板库`fastertpl`。

```
import stdtypes stdio thread network datetime ;

/*
curl -vvv -X GET -0 'http://192.168.11.79:9527/test_httpserver_htmlsection_1_1'
*/
function int `GET /test_httpserver_htmlsection_1_1`( httpserver s )
{
        htmlsection     sec ;
        htmlsection     sec_LOGIN ;
        htmlsection     sec_DETAIL ;
        long            total ;
        double          total_amount ;
        double          amount ;
        string          tmp ;
        long            n ;

        sec.SetString( "HEADER_TITLE" , "my header title" );

        sec_LOGIN = sec.CreateSection( "LOGIN" ) ;
        sec_LOGIN.SetString( "USER" , "lihua" );

        total = 0 ;
        total_amount = 0 ;
        for( n = 1 ; n <= 3 ; n++ )
        {
                sec_DETAIL = sec_LOGIN.CreateDetail( "DETAIL" ) ;
                sec_DETAIL.SetLong( "ID" , n );
                tmp.Format( "A%ld" , n );
                sec_DETAIL.SetString( "NAME" , tmp );
                tmp.Format( "%ld" , n );
                amount = tmp.ToDouble() * 100 ;
                sec_DETAIL.SetDouble( "AMOUNT" , amount );

                total++;
                total_amount += amount ;
        }

        sec_LOGIN.SetLong( "TOTAL" , total );
        sec_LOGIN.SetDouble( "TOTAL_AMOUNT" , total_amount );
        datetime.GetNow();
        sec_LOGIN.SetString( "DATETIME" , datetime.FormatString("%Y-%m-%d %H:%M:%S") );

        s.SetHttpResponseFromHtmlTemplate( "test_httpserver_htmlsection_1_1.html" , sec );

        return http.HTTP_STATUSCODE_OK;
}

function int main( array args )
{
        httpserver.SetListenAddress( "" , 9527 );
        httpserver.SetDomain( "*" );
        httpserver.SetWwwroot( "$HOME$/www" );
        httpserver.SetLogFile( "log/access.log" , "log/error.log" );
        r = httpserver.Run() ;
        if( r )
        {
                stdout.FormatPrintln( "httpserver.Run err[%d]" , r );
                return 1;
        }
        else
        {
                stdout.Println( "httpserver.Run normal ended" );
        }

        return 0;
}
```

执行

```
$ zlang test_httpserver_htmlsection_1_1.z
```

实例化出来的最终HTML为

```
$ curl -vvv -X GET -0 'http://localhost:9527/test_httpserver_htmlsection_1_1'
Note: Unnecessary use of -X or --request, GET is already inferred.
*   Trying ::1...
* TCP_NODELAY set
* connect to ::1 port 9527 failed: Connection refused
*   Trying 127.0.0.1...
* TCP_NODELAY set
* Connected to localhost (127.0.0.1) port 9527 (#0)
> GET /test_httpserver_htmlsection_1_1 HTTP/1.0
> Host: localhost:9527
> User-Agent: curl/7.61.1
> Accept: */*
>
* HTTP 1.0, assume close after body
< HTTP/1.0 200 OK
< Content-Type: text/html
< Content-length: 467
<
<html>
<head>
<title>((HEADER_TITLE))</title>
</head>
<body>


<p>USER : lihua</p>
<table border="1">
        <tr>
                <td>ID</td>
                <td>NAME</td>
                <td>AMOUNT</td>
        </tr>

        <tr>
                <td>1</td>
                <td>A1</td>
                <td>100.0</td>
        </tr>

        <tr>
                <td>2</td>
                <td>A2</td>
                <td>200.0</td>
        </tr>

        <tr>
                <td>3</td>
                <td>A3</td>
                <td>300.0</td>
        </tr>

</table>
<p>      TOTAL  : 3</p>
<p>TOTAL_AMOUNT : 600.000000</p>
<p>    DATETIME : 2024-09-01 02:22:48</p>

</body>
</html>
* Closing connection 0
```

HTML模板实例对象名：`htmlsection`。以下是该对象的内含函数：

| 成员类型 | 成员名字和原型                          | 说明                                                         | (错误.)(异常码,)异常消息                                     |
| -------- | --------------------------------------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| 函数     | string HTML_FORM_SELECT_OPTION_SELECTED | HTML select元素的值option中表示默认选中的HTML代码，即" selected=\"selected\"" |                                                              |
| 函数     | void SetShort(string,short);            | 设置短整型，输入参数为占位键名，短整型数值                   | htmlsection.HTMLSECTION_ERROR,EXCEPTION_MESSAGE_SET_SHORT_FAILED |
| 函数     | void SetInt(string,int);                | 设置整型，输入参数为占位键名，整型数值                       | htmlsection.HTMLSECTION_ERROR,EXCEPTION_MESSAGE_SET_SHORT_FAILED |
| 函数     | void SetLong(string,long);              | 设置长整型，输入参数为占位键名，长整型数值                   | htmlsection.HTMLSECTION_ERROR,EXCEPTION_MESSAGE_SET_SHORT_FAILED |
| 函数     | void SetFloat(string,float);            | 设置单精度浮点，输入参数为占位键名，单精度浮点数值           | htmlsection.HTMLSECTION_ERROR,EXCEPTION_MESSAGE_SET_SHORT_FAILED |
| 函数     | void SetDouble(string,double);          | 设置双精度浮点，输入参数为占位键名，双精度浮点数值           | htmlsection.HTMLSECTION_ERROR,EXCEPTION_MESSAGE_SET_SHORT_FAILED |
| 函数     | void SetString(string,string);          | 设置字符串，输入参数为占位键名，字符串                       | htmlsection.HTMLSECTION_ERROR,EXCEPTION_MESSAGE_SET_SHORT_FAILED |
| 函数     | htmlsection CreateSection(string);      | 创建子区域数据容器                                           | htmlsection.HTMLSECTION_ERROR,EXCEPTION_MESSAGE_CREATE_SECTION_FAILED |
| 函数     | htmlsection CreateDetail(string);       | 创建明细子区域数据容器                                       | htmlsection.HTMLSECTION_ERROR,EXCEPTION_MESSAGE_CREATE_DETAIL_FAILED |



下一章：[日期时间](datetime.md)
