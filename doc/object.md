上一章：[函数](function.md)



**章节目录**
- [面向对象](#面向对象)
  - [对象](#对象)
  - [对象的构造和析构](#对象的构造和析构)
  - [接口](#接口)



# 面向对象

## 对象

`zlang`支持以面向对象的代码结构来构建你的应用程序，和别的面向对象语言不同的是`zlang`没有类，直接定义出对象。

对象的语句形式为：

```
object 对象名 [ extends 父对象名 ] [ implements 接口名 ]
{
	[属性权限] [是否原子操作] 属性类型对象 属性名 ;
	
	[属性权限] [sync] function 输出类型对象 函数名( 输入类型对象 输入参数对象 , ... )
	{
		...
	}
}
```

属性权限有`private`（只允许对象内部访问）、`public`（允许外部访问）。

是否原子操作有`atomic`。修饰有`atomic`的属性，在计算赋值、引用、自增/自减、自增赋值/自减赋值等操作时会保证其操作原子性。

函数前缀修饰符`sync`可使函数变成临界区函数，多线程环境下最多只有一个线程允许执行函数内部。

代码示例：`test_object_13.z`

该代码示范了定义了一个对象`my_object`，内有一个隐藏属性`hello`、一个隐藏函数`echo`、一个公开函数`print`，`main`函数调用对象的`print`函数，`print`函数调用隐藏函数`echo`，`echo`函数拼接并输出显示`hello`和`name`。

```
import stdtypes stdio ;

object my_object
{
        private string          hello = "hello" ;

        private function void echo( string name )
        {
                stdout.Println( hello + " " + name );
                return;
        }

        public function int print( string name )
        {
                echo( name );
                return 0;
        }
}

function int main( array args )
{
        my_object       o ;

        o.print( "calvin" );

        return 0;
}
```

执行

```
$ zlang test_object_13.z
hello calvin
```

## 对象的构造和析构

每个对象可以有函数名同对象名的若干个构造函数，如果有则对象克隆时会被调用，一般用于初始化数据。可以有左缀`~`+对象名的一个析构函数，在对象销毁时被自动调用，一般用于清理资源。

`object`支持在创建时自动调用构造函数或手动调用自定义构造函数，也支持销毁时自动调用析构函数。

构造函数和析构函数的语句形式为：

```
object 对象名 [ extends 父对象名 ] [ implements 接口名 ]
{
	属性权限 function 对象名()
	{
		...
	}

	属性权限 function 对象名( 输入类型对象 输入参数对象 , ... )
	{
		...
	}

	属性权限 function ~对象名( 输入类型对象 输入参数对象 , ... )
	{
		...
	}
}
```

代码示例：`test_object_41.z`

```
import stdtypes stdio ;

object cat
{
        private string          name ;

        function cat()
        {
                stdout.Println( "cat()" );
                return;
        }

        function cat( string name )
        {
                stdout.FormatPrintln( "cat(\"%{name}\")" );
                SetName( name );
                return;
        }

        function ~cat()
        {
                stdout.Println( "~cat()" );
                this.name = "" ;
                return;
        }

        function void SetName( string name )
        {
                stdout.FormatPrintln( "SetName(\"%{name}\")" );
                this.name = name ;
                return;
        }

        function void PrintName()
        {
                stdout.Println( this.name );
                return;
        }
}

function int main( array args )
{
        stdout.Println( "--- DECLARE black ---" );

        cat     black ;

        stdout.Println( "--- DECLARE white ---" );

        cat     white( "White" ) ;

        stdout.Println( "--- USING black ---" );

        black.SetName( "Black" );
        black.PrintName();

        stdout.Println( "--- USING white ---" );

        white.PrintName();

        return 0;
}
```

执行

```
$ zlang test_object_41.z
--- DECLARE black ---
cat()
--- DECLARE white ---
cat("White")
SetName("White")
--- USING black ---
SetName("Black")
Black
--- USING white ---
White
~cat()
~cat()
```



## 接口

`zlang`支持接口（interface），通过接口实现对象达成对象函数规范。

`zlang`接口并没有限制只能放置未实现的函数，`zlang`接口里可以定义属性和实现的函数，为应用设计提供更多的灵活性。

接口的语句形式为：

```
interface 接口名
{
	属性权限 属性类型对象 属性名 ;
	
	属性权限 function 输出类型对象 函数名( 输入类型对象 输入参数对象 , ... )
	{
		...
	}
	
	属性权限 function 输出类型对象 函数名( 输入类型对象 输入参数对象 , ... );
}
```

代码示例：`test_object_31.z`

该代码先定义了`animal`接口，内含函数`SetWarriorName`，然后使用该接口实现了两个对象`cat`和`dog`，重载了函数`Cry`和`Eat`。

```
import stdtypes stdio ;

interface animal
{
        string  name ;

        function void SetWarriorName( string name )
        {
                this.name = name + " warrior" ;
        }

        function void Cry();
        function void Eat( string food );
}

object cat implements animal
{
        function void Cry()
        {
                stdout.Println( "miaomiaomiao~" );
        }

        function void Eat( string food )
        {
                if( food == "fish" )
                        stdout.Println( ":)" );
                else
                        stdout.Println( ":(" );
        }
}

object dog implements animal
{
        function void Cry()
        {
                stdout.Println( "wangwangwang~" );
        }

        function void Eat( string food )
        {
                if( food == "bone" )
                        stdout.Println( ":>" );
                else
                        stdout.Println( ":<" );
        }
}

function int main( array args )
{
        cat     my_cat ;
        dog     my_dog ;
        zobject my_animal ;

        my_cat.SetWarriorName( "ahua" );
        my_animal =& my_cat ;
        stdout.Println( my_animal.name );
        my_animal.Cry();
        my_animal.Eat( "fish" );
        my_animal.Eat( "bone" );

        my_dog.SetWarriorName( "black" );
        stdout.Println( my_dog.name );
        my_dog.Cry();
        my_dog.Eat( "bone" );
        my_dog.Eat( "fish" );

        return 0;
}
```

执行

```
ahua warrior
miaomiaomiao~
:)
:(
black warrior
wangwangwang~
:<
:>
```



下一章：[拦截器](intercept.md)