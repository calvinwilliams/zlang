上一章：[日志](log.md)



**章节目录**
- [数据库](#数据库)
	- [数据库对象](#数据库对象)
	- [数据库会话对象](#数据库会话对象)
	- [数据结果集对象](#数据结果集对象)
	- [数据库-CDBC-zlang对象类型映射表](#数据库-cdbc-zlang对象类型映射表)



# 数据库

数据库对象库用于应用连接、操作数据库处理。

`zlang`数据库对象库基于我的开源库`cdbc`,`cdbc`封装了各种数据库访问层细节，以通用接口方式提供给应用，`cdbc`支持`MySQL`、`PostgreSQL`、`Sqlite`。`zlang`把`cdbc`功能包装成数据库对象`database`、数据库会话对象`dbsession`、数据结果集对象`dbresult`。

## 数据库对象

数据库对象用于连接等处理。

数据库对象名：`database`

数据库对象的成员如下：

| 成员类型 | 成员名字和原型                                             | 说明                                                         | (错误.)(异常码,)异常消息                                     |
| :------: | ---------------------------------------------------------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
|   函数   | dbsession Connect(string,string,int,string,string,string); | 连接数据库服务端，输入参数为数据库类型、数据库IP、数据库端口、数据库连接用户名、数据库连接密码、数据库名，连接成功返回数据库会话对象`dbsession`；数据库类型有"MySQL"、"PostgreSQL"、"Sqlite" | EXCEPTION_MESSAGE_GET_DATABASE_DRIVER<br />EXCEPTION_MESSAGE_CREATE_AND_START_CONNECTION_POOL_FAILED |
|   函数   | int GetLastErrno();                                        | 返回`cdbc`错误码                                             |                                                              |
|   函数   | int GetLastNativeErrno();                                  | 返回数据库错误码                                             |                                                              |
|   函数   | string GetLastNativeError();                               | 返回数据库错误信息                                           |                                                              |
|   函数   | string GetSqlState();                                      | 返回标准SQLSTATE代码                                         |                                                              |

连接函数成功后返回数据库会话对象，客户端连接池参数在数据库对象中设置并继承给数据库会话对象。

客户端连接池会可选的创建一个独立线程：助手线程，每隔InspectTimeval微秒醒过来一次，每隔WatchIdleTimeval秒自动向空闲连接发心跳，如果当前空闲数量大于该值，每隔MaxIdleTimeval秒自动清理一个空闲连接。

| 成员类型 | 成员名字和原型                        | 说明                                                         |
| :------: | ------------------------------------- | ------------------------------------------------------------ |
|   函数   | void EnableAssistantThread(bool);     | 是否附带开启助手线程，enable=true开启，enable=false不开启。助手线程是一个独立线程，用于定期执行观察函数、清理过多的连接等旁路处理 |
|   函数   | void SetMinIdleConnectionsCount(int); | 设置连接池最小空闲连接数量；缺省值1                          |
|   函数   | void SetMaxIdleConnectionsCount(int); | 设置连接池最大空闲连接数量；缺省值2                          |
|   函数   | void SetMaxConnectionsCount(int);     | 设置连接池最大连接数量，空闲加工作连接数量绝不允许超过该值；缺省值2 |
|   函数   | void SetMaxIdleTimeval(int);          | 如果当前空闲数量大于该值，每隔MaxIdleTimeval秒自动清理一个空闲连接；缺省值300秒 |
|   函数   | void SetWatchIdleTimeval(int);        | 每隔WatchIdleTimeval秒自动向空闲连接发心跳；缺省值60秒       |
|   函数   | void SetInspectTimeval(int);          | 每隔InspectTimeval微秒连接池独立线程醒过来一次；缺省值1000000微秒（1秒） |

## 数据库会话对象

数据库会话对象用于执行SQL、设置选项、事务控制、断开连接等处理。

数据库会话对象名：`dbsession`

| 成员类型 | 成员名字和原型                    | 说明                                                         | (错误.)(异常码,)异常消息                                     |
| :------: | --------------------------------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
|   函数   | dbresult Execute(string,...);     | 发送SQL到数据库服务端，可在SQL中使用占位符'?'，对应后续可变参数中给定数据，成功返回数据结果集对象，否则返回null | EXCEPTION_MESSAGE_CREATE_AND_START_CONNECTION_POOL_FAILED<br />EXCEPTION_MESSAGE_FETCH_CONNECTION_SESSION_FAILED<br />EXCEPTION_MESSAGE_NO_CONNECTION_POOL<br />EXCEPTION_MESSAGE_EXECUTE_SQL |
|   函数   | int GetSqlElapse();               | 取得最后一次SQL的执行耗时（单位：微秒）                      |                                                              |
|   函数   | void ClearAccumulateDiffElapse(); | 清理SQL累积执行耗时                                          |                                                              |
|   函数   | int AccumulateDiffElapse();       | 取得SQL累积的执行耗时（单位：微秒）                          |                                                              |
| ~~函数~~ | ~~void AutoCommit(bool);~~        | ~~是否开启自动数据库事务模式~~                               |                                                              |
|   函数   | void Begin();                     | 显式开启一个数据库事务                                       | EXCEPTION_MESSAGE_BEGIN_TRANSACTION_FAILED<br />EXCEPTION_MESSAGE_FETCH_CONNECTION_SESSION_FAILED |
|   函数   | void Commit();                    | 提交数据库事务                                               | EXCEPTION_MESSAGE_CREATE_AND_START_CONNECTION_POOL_FAILED<br />EXCEPTION_MESSAGE_GIVEBACK_CONNECTION_SESSION_FAILED |
|   函数   | void Rollback();                  | 回滚数据库事务                                               | EXCEPTION_MESSAGE_CREATE_AND_START_CONNECTION_POOL_FAILED<br />EXCEPTION_MESSAGE_GIVEBACK_CONNECTION_SESSION_FAILED |
|   函数   | void Disconnect();                | 断开数据库连接                                               |                                                              |

## 数据结果集对象

数据结果集对象用于存放SQL执行响应带回来的记录结果集。

数据结果集对象名：`dbresult`

| 成员类型 | 成员名字和原型                      | 说明                                                         | (错误.)(异常码,)异常消息                                     |
| :------: | ----------------------------------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
|   函数   | bool NotFound();                    | 是否SQL执行结果是找不到记录                                  |                                                              |
|   函数   | bool Found();                       | 是否SQL执行结果是找到记录                                    |                                                              |
|   函数   | int GetRowCount();                  | 得到数据结果集中的记录行数                                   |                                                              |
|   函数   | int GetColumnCount();               | 得到数据结果集中的记录列数                                   |                                                              |
|   函数   | string GetFieldName(int);           | 得到数据结果集中第n列的字段名，输入参数为列号                | EXCEPTION_MESSAGE_ILLEGAL_ARGUMENT                           |
|   函数   | int GetFieldType(int);              | 得到数据结果集中第n列的字段类型，输入参数为列号              | EXCEPTION_MESSAGE_ILLEGAL_ARGUMENT                           |
|   函数   | int GetFieldLength(int);            | 得到数据结果集中第n列的（字符串型、整型）字段长度，输入参数为列号 | EXCEPTION_MESSAGE_ILLEGAL_ARGUMENT                           |
|   函数   | int GetFieldDecimalLength(int);     | 得到数据结果集中第n列的（浮点型）字段精度长度，输入参数为列号 | EXCEPTION_MESSAGE_ILLEGAL_ARGUMENT                           |
|   函数   | string GetFieldValue(int,int);      | 得到数据结果集中第n行、第n列的字段值，输入参数为行号、列号   | EXCEPTION_MESSAGE_ILLEGAL_ARGUMENT                           |
|   函数   | int GetAffectedRows();              | 得到SQL执行时影响的行数；如果更新时没有SET不同的值算作没影响 |                                                              |
|   函数   | object RowToObject(int);            | 得到数据结果集中第n行记录，转换成对象的属性集返回，对象初始输入为zobject，输出前添加好属性名为字段名、属性值为字段值 |                                                              |
|   函数   | bool RowToEntityObject(int,object); | 得到数据结果集中第n行记录，转换成实体对象的属性集返回，对象是预定义的、属性集和表结构一致的 | EXCEPTION_MESSAGE_ILLEGAL_ARGUMENT<br />EXCEPTION_MESSAGE_PROPERTY_TYPE_NOT_MATCHED |

## 数据库-CDBC-zlang对象类型映射表

| MySQL字段类型 | PostgreSQL字段类型 | CDBC标准类型 | zlang对象    |
| :-----------: | ------------------ | ------------ | ------------ |
|      BIT      | （暂不支持）       | （暂不支持） | （暂不支持） |
|     TINY      | （暂不支持）       | INT8         | short        |
|     SHORT     | INT2OID            | INT16        | short        |
|     INT24     | （暂不支持）       | （暂不支持） | （暂不支持） |
|     LONG      | INT4OID            | INT32        | int          |
|   LONGLONG    | INT8OID            | INT64        | long         |
|     FLOAT     | FLOAT4OID          | FLOAT        | float        |
|    DOUBLE     | FLOAT8OID          | DOUBLE       | double       |
|    DECIMAL    | NUMERICOID         | DECIMAL      | double       |
|  NEWDECIMAL   | （暂不支持）       | （暂不支持） | （暂不支持） |
|     DATE      | DATEOID            | DATE         | datetime     |
|     TIME      | TIMEOID            | TIME         | datetime     |
|   DATETIME    | TIMESTAMPOID       | DATETIME     | datetime     |
|   TIMESTAMP   | TIMESTAMPOID       | TIMESTAMP    | datetime     |
|    NEWDATE    | （暂不支持）       | （暂不支持） | datetime     |
|     YEAR      | （暂不支持）       | （暂不支持） | datetime     |
|    STRING     | CHAROID            | CHAR         | string       |
|    VARCHAR    | VARCHAROID         | VARCHAR      | string       |
|  VAR_STRING   | VARCHAROID         | VARCHAR      | string       |

代码示例：`test_database_insert_1_1.z`

该代码示范了连接数据库服务端，插入记录。

```
import stdtypes stdio database datetime ;

/* test ddl
MySQL :
CREATE TABLE `mytable` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `my_int16` smallint(6) DEFAULT NULL,
  `my_int32` int(11) DEFAULT NULL,
  `my_int64` bigint(20) DEFAULT NULL,
  `my_float` float DEFAULT NULL,
  `my_double` double DEFAULT NULL,
  `my_decimal` decimal(12,4) DEFAULT NULL,
  `my_char` char(3) DEFAULT NULL,
  `my_varchar` varchar(30) DEFAULT NULL,
  `my_date` date DEFAULT NULL,
  `my_time` time DEFAULT NULL,
  `my_datetime` datetime DEFAULT NULL,
  `my_timestamp` timestamp NULL DEFAULT NULL,
  `my_null` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4

PostgreSQL :
CREATE TABLE mytable (
  id int NOT NULL,
  my_int16 smallint DEFAULT NULL,
  my_int32 int DEFAULT NULL,
  my_int64 bigint DEFAULT NULL,
  my_float real DEFAULT NULL,
  my_double double precision DEFAULT NULL,
  my_decimal decimal(12,4) DEFAULT NULL,
  my_char char(3) DEFAULT NULL,
  my_varchar varchar(30) DEFAULT NULL,
  my_date date DEFAULT NULL,
  my_time time DEFAULT NULL,
  my_datetime timestamp DEFAULT NULL,
  my_timestamp timestamp NULL DEFAULT NULL,
  my_null varchar(45) DEFAULT NULL,
  PRIMARY KEY (id)
)

Sqlite :
CREATE TABLE `mytable` (
  `id` int(11) NOT NULL,
  `my_int16` smallint(6) DEFAULT NULL,
  `my_int32` int(11) DEFAULT NULL,
  `my_int64` bigint(20) DEFAULT NULL,
  `my_float` float DEFAULT NULL,
  `my_double` double DEFAULT NULL,
  `my_decimal` decimal(12,4) DEFAULT NULL,
  `my_char` char(3) DEFAULT NULL,
  `my_varchar` varchar(30) DEFAULT NULL,
  `my_date` date DEFAULT NULL,
  `my_time` time DEFAULT NULL,
  `my_datetime` datetime DEFAULT NULL,
  `my_timestamp` timestamp NULL DEFAULT NULL,
  `my_null` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
)
*/

function int main( array args )
{
	dbsession	dbsess ;
	dbresult	dbres ;
	string		sql ;
	int		nret = 0 ;
	
	if( args.Get(1) == "PostgreSQL" )
		dbsess = database.Connect( args.Get(1) , "localhost" , 5432 , "calvin" , "calvin" , "calvindb" ) ;
	else if( args.Get(1) == "Sqlite" )
		dbsess = database.Connect( args.Get(1) , "" , 0 , "" , "" , "/tmp/calvindb.db" ) ;
	else
		dbsess = database.Connect( "MySQL" , "localhost" , 3306 , "calvin" , "calvin" , "calvindb" ) ;
	if( dbsess == null )
	{
		stderr.FormatPrintln( "connect db failed[%d]-[%d][%s]-[%s]" , database.GetLastErrno() , database.GetLastNativeErrno() , database.GetLastNativeError() , database.GetLastSqlState() );
		return 1;
	}
	
	if( args.Get(1) == "PostgreSQL" )
		sql = "INSERT INTO mytable ( id , my_int16 , my_int32 , my_int64 , my_float , my_double , my_decimal , my_char , my_varchar , my_date , my_time , my_datetime , my_timestamp ) VALUES( " + datetime.GetNow().SecondStamp().ToString() + " , 23 , 456 , 7890 , 1.2 , 34.56 , 78.90 , 'ABC' , 'calvin' , TO_DATE('2000-01-02','YYYY-MM-DD') , TO_TIMESTAMP('12:34:56','HH24:MI:SS') , TO_TIMESTAMP('2000-01-02 12:34:56','YYYY-MM-DD HH24:MI:SS') , TO_TIMESTAMP('2000-01-02 12:34:56','YYYY-MM-DD HH24:MI:SS') );" ;
	else if( args.Get(1) == "Sqlite" )
		sql = "INSERT INTO mytable ( id , my_int16 , my_int32 , my_int64 , my_float , my_double , my_decimal , my_char , my_varchar , my_date , my_time , my_datetime , my_timestamp ) VALUES( " + datetime.GetNow().SecondStamp().ToString() + " , 23 , 456 , 7890 , 1.2 , 34.56 , 78.90 , 'ABC' , 'calvin' , '2000-01-02' , '12:34:56' , '2000-01-02 12:34:56' , '2000-01-02 12:34:56' )" ;
	else
		sql = "INSERT INTO mytable ( my_int16 , my_int32 , my_int64 , my_float , my_double , my_decimal , my_char , my_varchar , my_date , my_time , my_datetime , my_timestamp ) VALUES( 23 , 456 , 7890 , 1.2 , 34.56 , 78.90 , 'ABC' , 'calvin' , STR_TO_DATE('2000-01-02','%Y-%m-%d') , STR_TO_DATE('12:34:56','%H:%i:%S') , STR_TO_DATE('2000-01-02 12:34:56','%Y-%m-%d %H:%i:%S') , STR_TO_DATE('2000-01-02 12:34:56','%Y-%m-%d %H:%i:%S') )" ;
	dbres = dbsess.Execute( sql ) ;
	if( dbres == null )
	{
		stderr.FormatPrintln( "execute sql[%s] failed[%d]-[%d][%s]-[%s]" , sql , database.GetLastErrno() , database.GetLastNativeErrno() , database.GetLastNativeError() , database.GetLastSqlState() );
		return 1;
	}
	
	stdout.FormatPrintln( "records %d affected" , dbres.GetAffectedRows() );
	
	dbsess.Disconnect();
	
	return 0;
}
```

代码示例：`test_database_update_1.z`

该代码示范了连接数据库服务端，更新记录。

```
import stdtypes stdio database ;

function int main( array args )
{
	if( args.Get(1) == "PostgreSQL" )
		dbsess = database.Connect( args.Get(1) , "localhost" , 5432 , "calvin" , "calvin" , "calvindb" ) ;
	else if( args.Get(1) == "Sqlite" )
		dbsess = database.Connect( args.Get(1) , "" , 0 , "" , "" , "/tmp/calvindb.db" ) ;
	else
		dbsess = database.Connect( "MySQL" , "localhost" , 3306 , "calvin" , "calvin" , "calvindb" ) ;
	if( dbsess == null )
	{
		stderr.FormatPrintln( "connect db failed[%d]-[%d][%s]-[%s]" , database.GetLastErrno() , database.GetLastNativeErrno() , database.GetLastNativeError() , database.GetLastSqlState() );
		return 1;
	}
	
	sql = "UPDATE mytable SET my_float=3.4 WHERE my_varchar='calvin'" ;
	dbres = dbsess.Execute( sql ) ;
	if( dbres == null )
	{
		stderr.FormatPrintln( "execute sql[%s] failed[%d]-[%d][%s]-[%s]" , sql , database.GetLastErrno() , database.GetLastNativeErrno() , database.GetLastNativeError() , database.GetLastSqlState() );
		return 1;
	}
	
	stdout.FormatPrintln( "records %d affected" , dbres.GetAffectedRows() );
	
	dbsess.Disconnect();
	
	return 0;
}
```

代码示例：`test_database_delete_1.z`

该代码示范了连接数据库服务端，删除记录。

```
import stdtypes stdio database ;

function int main( array args )
{
	if( args.Get(1) == "PostgreSQL" )
		dbsess = database.Connect( args.Get(1) , "localhost" , 5432 , "calvin" , "calvin" , "calvindb" ) ;
	else if( args.Get(1) == "Sqlite" )
		dbsess = database.Connect( args.Get(1) , "" , 0 , "" , "" , "/tmp/calvindb.db" ) ;
	else
		dbsess = database.Connect( "MySQL" , "localhost" , 3306 , "calvin" , "calvin" , "calvindb" ) ;
	if( dbsess == null )
	{
		stderr.FormatPrintln( "connect db failed[%d]-[%d][%s]-[%s]" , database.GetLastErrno() , database.GetLastNativeErrno() , database.GetLastNativeError() , database.GetLastSqlState() );
		return 1;
	}
	
	sql = "DELETE FROM mytable WHERE my_int32 IN ( 789 )" ;
	dbres = dbsess.Execute( sql ) ;
	if( dbres == null )
	{
		stderr.FormatPrintln( "execute sql[%s] failed[%d]-[%d][%s]-[%s]" , sql , database.GetLastErrno() , database.GetLastNativeErrno() , database.GetLastNativeError() , database.GetLastSqlState() );
		return 1;
	}
	
	stdout.FormatPrintln( "records %d affected" , dbres.GetAffectedRows() );
	
	dbsess.Disconnect();
	
	return 0;
}
```

代码示例：`test_database_select_0.z`

该代码示范了连接数据库服务端，查询整表记录到二维字符串矩阵中。

```
import stdtypes stdio database ;

function int main( array args )
{
	if( args.Get(1) == "PostgreSQL" )
		dbsess = database.Connect( args.Get(1) , "localhost" , 5432 , "calvin" , "calvin" , "calvindb" ) ;
	else if( args.Get(1) == "Sqlite" )
		dbsess = database.Connect( args.Get(1) , "" , 0 , "" , "" , "/tmp/calvindb.db" ) ;
	else
		dbsess = database.Connect( "MySQL" , "localhost" , 3306 , "calvin" , "calvin" , "calvindb" ) ;
	if( dbsess == null )
	{
		stdout.FormatPrintln( "connect database failed[%d]-[%d][%s]-[%s]" , database.GetLastErrno() , database.GetLastNativeErrno() , database.GetLastNativeError() , database.GetLastSqlState() );
		return 1;
	}
	
	sql = "SELECT * FROM mytable" ;
	dbres = dbsess.Execute( sql ) ;
	if( dbres == null )
	{
		stderr.FormatPrintln( "execute sql[%s] failed[%d]-[%d][%s]-[%s]" , sql , database.GetLastErrno() , database.GetLastNativeErrno() , database.GetLastNativeError() , database.GetLastSqlState() );
		return 1;
	}
	
	stdout.Println( "--- FIELD INFO ---------" ) ;
	col_count = dbres.GetColumnCount() ;
	for( c = 1 ; c <= col_count ; c++ )
	{
		stdout.FormatPrintln( "%d | %s %d %d %d" , c , dbres.GetFieldName(c) , dbres.GetFieldType(c) , dbres.GetFieldLength(c) , dbres.GetFieldDecimalLength(c) ) ;
	}
	
	stdout.Println( "--- QUERY RESULT ---------" ) ;
	row_count = dbres.GetRowCount() ;
	for( r = 1 ; r <= row_count ; r++ )
	{
		stdout.FormatPrint( "%d |" , r );
		for( c = 1 ; c <= col_count ; c++ )
		{
			stdout.FormatPrint( " %s" , dbres.GetFieldValue(r,c) ) ;
		}
		stdout.Println( "" ) ;
	}
	
	dbsess.Disconnect();
	
	return 0;
}
```

代码示例：`test_database_select_1.z`

该代码示范了连接数据库服务端，以占位符输入条件的查询记录到二维字符串矩阵中。

```
import stdtypes stdio database datetime ;

function int main( array args )
{
	datetime	t ;
	
	if( args.Get(1) == "PostgreSQL" )
		dbsess = database.Connect( args.Get(1) , "localhost" , 5432 , "calvin" , "calvin" , "calvindb" ) ;
	else if( args.Get(1) == "Sqlite" )
		dbsess = database.Connect( args.Get(1) , "" , 0 , "" , "" , "/tmp/calvindb.db" ) ;
	else
		dbsess = database.Connect( "MySQL" , "localhost" , 3306 , "calvin" , "calvin" , "calvindb" ) ;
	if( dbsess == null )
	{
		stdout.FormatPrintln( "connect database failed[%d]-[%d][%s]-[%s]" , database.GetLastErrno() , database.GetLastNativeErrno() , database.GetLastNativeError() , database.GetLastSqlState() );
		return 1;
	}
	
	if( args.Get(1) == "PostgreSQL" )
		dbres = dbsess.Execute( "SELECT * FROM mytable WHERE my_int32=$1 AND my_double=$2 AND my_varchar=$3 AND my_datetime=$4" , 456 , 34.56 , "calvin" , t.GetFromFormat("%Y-%m-%d %H:%M:%S","2000-01-02 12:34:56") ) ;
	else if( args.Get(1) == "Sqlite" )
		dbres = dbsess.Execute( "SELECT * FROM mytable WHERE my_int32=? AND my_double=? AND my_varchar=? AND my_datetime=?" , 456 , 34.56 , "calvin" , t.GetFromFormat("%Y-%m-%d %H:%M:%S","2000-01-02 12:34:56") ) ;
	else
		dbres = dbsess.Execute( "SELECT * FROM mytable WHERE my_int32=? AND my_double=? AND my_varchar=? AND my_datetime=?" , 456 , 34.56 , "calvin" , t.GetFromFormat("%Y-%m-%d %H:%M:%S","2000-01-02 12:34:56") ) ;
	if( dbres == null )
	{
		stderr.FormatPrintln( "execute statement sql failed[%d]-[%d][%s]-[%s]" , database.GetLastErrno() , database.GetLastNativeErrno() , database.GetLastNativeError() , database.GetLastSqlState() );
		return 1;
	}
	
	stdout.Println( "--- FIELD INFO ---------" ) ;
	col_count = dbres.GetColumnCount() ;
	for( c = 1 ; c <= col_count ; c++ )
	{
		stdout.FormatPrintln( "%d | %s %d %d %d" , c , dbres.GetFieldName(c) , dbres.GetFieldType(c) , dbres.GetFieldLength(c) , dbres.GetFieldDecimalLength(c) ) ;
	}
	
	stdout.Println( "--- QUERY RESULT ---------" ) ;
	row_count = dbres.GetRowCount() ;
	for( r = 1 ; r <= row_count ; r++ )
	{
		stdout.FormatPrint( "%d |" , r );
		for( c = 1 ; c <= col_count ; c++ )
		{
			stdout.FormatPrint( " %s" , dbres.GetFieldValue(r,c) ) ;
		}
		stdout.Println( "" ) ;
	}
	
	dbsess.Disconnect();
	
	return 0;
}
```

代码示例：`test_database_select_row2obj_1.z`

该代码示范了连接数据库服务端，整表查询记录到自动构造记录对象的属性中。

```
import stdtypes stdio database datetime ;

function int main( array args )
{
	if( args.Get(1) == "PostgreSQL" )
		dbsess = database.Connect( args.Get(1) , "localhost" , 5432 , "calvin" , "calvin" , "calvindb" ) ;
	else if( args.Get(1) == "Sqlite" )
		dbsess = database.Connect( args.Get(1) , "" , 0 , "" , "" , "/tmp/calvindb.db" ) ;
	else
		dbsess = database.Connect( "MySQL" , "localhost" , 3306 , "calvin" , "calvin" , "calvindb" ) ;
	if( dbsess == null )
	{
		stdout.FormatPrintln( "connect database failed[%d]-[%d][%s]-[%s]" , database.GetLastErrno() , database.GetLastNativeErrno() , database.GetLastNativeError() , database.GetLastSqlState() );
		return 1;
	}
	
	sql = "SELECT * FROM mytable" ;
	dbres = dbsess.Execute( sql ) ;
	if( dbres == null )
	{
		stderr.FormatPrintln( "execute sql[%s] failed[%d]-[%d][%s]-[%s]" , sql , database.GetLastErrno() , database.GetLastNativeErrno() , database.GetLastNativeError() , database.GetLastSqlState() );
		return 1;
	}
	
	row_count = dbres.GetRowCount() ;
	for( r = 1 ; r <= row_count ; r++ )
	{
		o = dbres.RowToObject(r) ;
		if( o == null )
		{
			stdout.Println( "RowToObject failed" );
			break;
		}
		
		stdout.FormatPrintln( "--- ROW %d ---" , r ) ;
		stdout.FormatPrintln( "          id : %?" , o.id );
		stdout.FormatPrintln( "    my_int16 : %?" , o.my_int16 );
		stdout.FormatPrintln( "    my_int32 : %?" , o.my_int32 );
		stdout.FormatPrintln( "    my_int64 : %?" , o.my_int64 );
		stdout.FormatPrintln( "    my_float : %?" , o.my_float );
		stdout.FormatPrintln( "   my_double : %?" , o.my_double );
		stdout.FormatPrintln( "  my_decimal : %?" , o.my_decimal );
		stdout.FormatPrintln( "     my_char : %s" , o.my_char );
		stdout.FormatPrintln( "  my_varchar : %s" , o.my_varchar );
		stdout.FormatPrint  ( "     my_date : " ); stdout.Println( o.my_date );
		stdout.FormatPrint  ( "     my_time : " ); stdout.Println( o.my_time );
		stdout.FormatPrint  ( " my_datetime : " ); stdout.Println( o.my_datetime );
		stdout.FormatPrint  ( "my_timestamp : " ); stdout.Println( o.my_timestamp );
		stdout.FormatPrintln( "     my_null : %s" , o.my_null );
	}
	
	dbsess.Disconnect();
	
	return 0;
}
```



下一章：[Redis](redis.md)

