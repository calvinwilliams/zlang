上一章：[XML序列化/反序列化](xml.md)



**章节目录**

- [加解密](#加解密)
  - [安全公共对象](#安全公共对象)
  - [消息摘要](#消息摘要)
    - [MD4](#md4)
    - [MD5](#md5)
    - [SHA1](#sha1)
    - [SHA224](#sha224)
    - [SHA256](#sha256)
    - [SHA384](#sha384)
    - [SHA512](#sha512)
    - [SM3](#sm3)
  - [对称加解密](#对称加解密)
    - [DES](#des)
    - [3DES](#3des)
    - [AES](#aes)
    - [SM4](#sm4)
  - [非对称加解密和签名验签](#非对称加解密和签名验签)
    - [RSA](#rsa)
    - [SM2](#sm2)


# 加解密

加解密对象库提供了常用的消息摘要、加解密、签名验签等基础安全算法对象供应用使用。

## 安全公共对象

安全公共对象定义了一些常量，比如密钥长度类型、填充算法等。

安全公共对象名：`crypto`

| 成员类型 | 成员名字和原型         | 说明                  |
| :------: | ---------------------- | --------------------- |
|   属性   | int ECB_MODE           | ECB处理过程           |
|   属性   | int CBC_MODE           | CBC处理过程           |
|   属性   | int KEY_64BITS         | 64位（8字节）密钥     |
|   属性   | int KEY_128BITS        | 128位（16字节）密钥   |
|   属性   | int KEY_192BITS        | 192位（24字节）密钥   |
|   属性   | int KEY_256BITS        | 256位（32字节）密钥   |
|   属性   | int KEY_512BITS        | 512位（64字节）密钥   |
|   属性   | int KEY_1024BITS       | 1024位（128字节）密钥 |
|   属性   | int KEY_2048BITS       | 2048位（256字节）密钥 |
|   属性   | int KEY_4096BITS       | 4096位（512字节）密钥 |
|   属性   | int NO_PADDING         | 无填充算法            |
|   属性   | int ZERO_PADDING       | 用'\x00'填充算法      |
|   属性   | int E_3                | 公钥指数3             |
|   属性   | int E_F4               | 公钥指数F4            |
|   属性   | int PKCS7_PADDING      | PKCS7填充算法         |
|   属性   | int PKCS1_PADDING      | PKCS1填充算法         |
|   属性   | int PKCS1_OAEP_PADDING | PKCS1_OAEP填充算法    |
|   属性   | int NID_MD5            | MD5摘要算法ID         |
|   属性   | int NID_SHA1           | SHA1摘要算法ID        |
|   属性   | int NID_SHA224         | SHA224摘要算法ID      |
|   属性   | int NID_SHA256         | SHA256摘要算法ID      |
|   属性   | int NID_SHA384         | SHA384摘要算法ID      |
|   属性   | int NID_SHA512         | SHA512摘要算法ID      |
|   属性   | int NID_MD5_SHA1       | MD5+SHA摘要算法ID     |

## 消息摘要

消息摘要用于把任意长度的消息变成固定长度的短消息，是一种验证数据完整性的算法，主要对付信息防篡改。常用的消息摘要算法有MD5、SHA256。

### MD4

MD4把任意长度的字符串变成16字节（不可显示）字节串。

对象名：`md4`。

| 成员类型 | 成员名字和原型         | 中文名 | 说明                                  | (错误.)(异常码,)异常消息        |
| :------: | ---------------------- | ------ | ------------------------------------- | ------------------------------- |
|   函数   | string Digest(string); | 摘要   | 对输入字符串进行MD4消息摘要并返回结果 | EXCEPTION_MESSAGE_DIGEST_FAILED |

### MD5

MD5把任意长度的字符串变成16字节（不可显示）字节串。

对象名：`md5`。

对象内函数同上。

### SHA1

SHA1把任意长度的字符串变成20字节（不可显示）字节串。

对象名：`sha1`。

### SHA224

SHA224把任意长度的字符串变成28字节（不可显示）字节串。

对象名：`sha224`。

### SHA256

SHA256把任意长度的字符串变成32字节（不可显示）字节串。

对象名：`sha256`。

### SHA384

SHA384把任意长度的字符串变成48字节（不可显示）字节串。

对象名：`sha384`。

### SHA512

SHA512把任意长度的字符串变成64字节（不可显示）字节串。

对象名：`sha512`。

### SM3

SHA512把任意长度的字符串变成16字节（不可显示）字节串。

对象名：`sm3`。

## 对称加解密

对称加解密是用同一个密钥用于数据的加密和解密，即用密钥K加密明文为密文，再用密钥K解密密文为明文。

### DES

`DES`全称为Data Encryption Standard，即数据加密标准，是一种使用密钥加密的块算法，1977年被美国联邦政府的国家标准局确定为联邦资料处理标准（FIPS），并授权在非密级政府通信中使用，随后该算法在国际上广泛流传开来。

DES对象名：`des`

| 成员类型 | 成员名字和原型                                               | 中文名 | 说明                                                         |                                                              |
| :------: | ------------------------------------------------------------ | ------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
|   函数   | string Encrypt(int key_len,string key,string dec,int padding_type); | 加密   | 先把数据按8字节分块，再用填充算法（padding_type）补充右边部分，最后用DES算法和DES密钥加密明文变成密文。密钥长度有`crypto.KEY_64BITS`。填充算法有`crypto.ZERO_PADDING`（右边补充'\x00'直到填充满一个块）、`crypto.PKCS7_PADDING`（用ASCII值为“整块不足字节数”的字节填充满一个块，如果已经是整块了则再填充8个字节的"\x08"）。 | EXCEPTION_MESSAGE_KEY_LEN_TYPE_INVALID<br />EXCEPTION_MESSAGE_KEY_LEN_NOT_MATCHED_KEY_LEN_TYPE<br />EXCEPTION_MESSAGE_PADDING_TYPE_INVALID<br />EXCEPTION_MESSAGE_PADDING_FAILED |
|   函数   | string Decrypt(int key_len,string key,string enc,int padding_type); | 解密   | 先用DES算法和DES密钥解密密文变成明文，再用反填充算法去除尾巴上多余的数据 | EXCEPTION_MESSAGE_KEY_LEN_TYPE_INVALID<br />EXCEPTION_MESSAGE_KEY_LEN_NOT_MATCHED_KEY_LEN_TYPE<br />EXCEPTION_MESSAGE_PADDING_TYPE_INVALID<br />EXCEPTION_MESSAGE_UNPADDING_FAILED |

代码示例：`test_crypto_des_1.z`

```
import stdtypes stdio encoding crypto;

function int des_Encrypt( string key , string dec , int padding_type )
{
        enc = des.Encrypt( crypto.KEY_64BITS , key , dec , padding_type ) ;
        if( enc == null )
        {
                stdout.Println( "des.Encrypt failed" );
                return -1;
        }
        enc_exp = hexstr.HexExpand( enc ) ;

        dec2 = des.Decrypt( crypto.KEY_64BITS , key , enc , padding_type ) ;
        if( dec2 == null )
        {
                stdout.Println( "des.Decrypt failed" );
                return -2;
        }

        stdout.FormatPrintln( "dec:TEXT[%{dec}] ---des.Encrypt-key[%{key}]-padding[%{padding_type}]---> enc:HEX[%{enc_exp}] ---des.Decrypt-key:TEXT[%{key}]-padding[%{padding_type}]---> dec:TEXT[%{dec2}]" );

        return 0;
}

function int main( array args )
{
        string  key = "87654321" ;

        stdout.Println( "\n--- ZERO PADDING ---" );

        dec = "1234567" ;
        nret = des_Encrypt( key , dec , crypto.ZERO_PADDING ) ;
        if( nret )
                return -nret;

        dec = "12345678" ;
        nret = des_Encrypt( key , dec , crypto.ZERO_PADDING ) ;
        if( nret )
                return -nret;

        dec = "123456781" ;
        nret = des_Encrypt( key , dec , crypto.ZERO_PADDING ) ;
        if( nret )
                return -nret;

        stdout.Println( "\n--- PKCS7 PADDING ---" );

        dec = "1234567" ;
        nret = des_Encrypt( key , dec , crypto.PKCS7_PADDING ) ;
        if( nret )
                return -nret;

        dec = "12345678" ;
        nret = des_Encrypt( key , dec , crypto.PKCS7_PADDING ) ;
        if( nret )
                return -nret;

        dec = "123456781" ;
        nret = des_Encrypt( key , dec , crypto.PKCS7_PADDING ) ;
        if( nret )
                return -nret;

        return 0;
}
```

执行

```
$ zlang test_crypto_des_1.z

--- ZERO PADDING ---
dec:TEXT[1234567] ---des.Encrypt-key[87654321]-padding[0]---> enc:HEX[9A4CB153FF855891] ---des.Decrypt-key:TEXT[87654321]-padding[0]---> dec:TEXT[1234567]
dec:TEXT[12345678] ---des.Encrypt-key[87654321]-padding[0]---> enc:HEX[0DA06156D09594C3] ---des.Decrypt-key:TEXT[87654321]-padding[0]---> dec:TEXT[12345678]
dec:TEXT[123456781] ---des.Encrypt-key[87654321]-padding[0]---> enc:HEX[0DA06156D09594C33FB60F5F9394CD23] ---des.Decrypt-key:TEXT[87654321]-padding[0]---> dec:TEXT[123456781]

--- PKCS7 PADDING ---
dec:TEXT[1234567] ---des.Encrypt-key[87654321]-padding[7]---> enc:HEX[BCC6FE2A7AA93BE0] ---des.Decrypt-key:TEXT[87654321]-padding[7]---> dec:TEXT[1234567]
dec:TEXT[12345678] ---des.Encrypt-key[87654321]-padding[7]---> enc:HEX[0DA06156D09594C3CB12A48C6B54C99E] ---des.Decrypt-key:TEXT[87654321]-padding[7]---> dec:TEXT[12345678]
dec:TEXT[123456781] ---des.Encrypt-key[87654321]-padding[7]---> enc:HEX[0DA06156D09594C3551A4DA8B3285514] ---des.Decrypt-key:TEXT[87654321]-padding[7]---> dec:TEXT[123456781]
```

### 3DES

3DES算法也称为Triple Data Encryption Algorithm（TDEA）或Triple DES，也是一种对称密钥加解密算法，它通过对每个数据块应用三次数据加密标准（DES）算法来提高安全性。

3DES对象名：`tripledes`

| 成员类型 | 成员名字和原型                                               | 中文名 | 说明                                                         | (错误.)(异常码,)异常消息                                     |
| :------: | ------------------------------------------------------------ | ------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
|   函数   | string Encrypt(int mode,int key_len,string key,string dec,int padding_type); | 加密   | 先把数据按8字节分块，再用填充算法（padding_type）补充右边部分，最后用3DES算法和3DES密钥加密明文变成密文。mode为计算模式，取值有`tripledes.ECB_MODE`、`tripledes.CBC_MODE`。密钥长度有`crypto.KEY_128BITS`、`crypto.KEY_192BITS`；如果报错返回null。 | EXCEPTION_MESSAGE_KEY_LEN_TYPE_INVALID<br />EXCEPTION_MESSAGE_KEY_LEN_NOT_MATCHED_KEY_LEN_TYPE<br />EXCEPTION_MESSAGE_PADDING_TYPE_INVALID<br />EXCEPTION_MESSAGE_PADDING_FAILED |
|   函数   | string Decrypt(int mode,int key_len,string key,string enc,int padding_type); | 解密   | 先用3DES算法和3DES密钥解密密文变成明文，再用反填充算法去除尾巴上多余的数据；如果报错返回null | EXCEPTION_MESSAGE_KEY_LEN_TYPE_INVALID<br />EXCEPTION_MESSAGE_KEY_LEN_NOT_MATCHED_KEY_LEN_TYPE<br />EXCEPTION_MESSAGE_PADDING_TYPE_INVALID<br />EXCEPTION_MESSAGE_ENC_LEN_INVALID<br />EXCEPTION_MESSAGE_UNPADDING_FAILED |

代码示例：`test_crypto_tripledes_1.z`

```
import stdtypes stdio encoding crypto;

function int tripledes_Encrypt( int mode , int key_len_type , string key , string dec , int padding_type )
{
        enc = tripledes.Encrypt( mode , key_len_type , key , dec , padding_type ) ;
        if( enc == null )
        {
                stdout.Println( "des.Encrypt failed" );
                return -1;
        }
        enc_exp = hexstr.HexExpand( enc ) ;

        dec2 = tripledes.Decrypt( mode , key_len_type , key , enc , padding_type ) ;
        if( dec2 == null )
        {
                stdout.Println( "des.Decrypt failed" );
                return -2;
        }

        stdout.FormatPrintln( "dec:TEXT[%{dec}] ---tripledes.Encrypt-mode[%{mode}]-key:TYPE[%{key_len_type}]:TEXT[%{key}]-padding[%{padding_type}]---> enc:HEX[%{enc_exp}] ---tripledes.Decrypt-mode[%{mode}]-key:TYPE[%{key_len_type}]:TEXT[
%{key}]-padding[%{padding_type}]---> dec:TEXT[%{dec2}]" );

        return 0;
}

function int main( array args )
{
        key16B = "8765432187654321" ;
        key24B = "876543218765432187654321" ;

        stdout.Println( "\n--- ECB MODE & key16B & ZERO PADDING ---" );

        dec = "123456781234567" ;
        nret = tripledes_Encrypt( crypto.ECB_MODE , crypto.KEY_128BITS , key16B , dec , crypto.ZERO_PADDING ) ;
        if( nret )
                return -nret;

        dec = "1234567812345678" ;
        nret = tripledes_Encrypt( crypto.ECB_MODE , crypto.KEY_128BITS , key16B , dec , crypto.ZERO_PADDING ) ;
        if( nret )
                return -nret;

        dec = "12345678123456781" ;
        nret = tripledes_Encrypt( crypto.ECB_MODE , crypto.KEY_128BITS , key16B , dec , crypto.ZERO_PADDING ) ;
        if( nret )
                return -nret;

        stdout.Println( "\n--- ECB MODE & key16B & PKCS7 PADDING ---" );

        dec = "123456781234567" ;
        nret = tripledes_Encrypt( crypto.ECB_MODE , crypto.KEY_128BITS , key16B , dec , crypto.PKCS7_PADDING ) ;
        if( nret )
                return -nret;

        dec = "1234567812345678" ;
        nret = tripledes_Encrypt( crypto.ECB_MODE , crypto.KEY_128BITS , key16B , dec , crypto.PKCS7_PADDING ) ;
        if( nret )
                return -nret;

        dec = "12345678123456781" ;
        nret = tripledes_Encrypt( crypto.ECB_MODE , crypto.KEY_128BITS , key16B , dec , crypto.PKCS7_PADDING ) ;
        if( nret )
                return -nret;

        stdout.Println( "\n--- ECB MODE & key24B & ZERO PADDING ---" );

        dec = "123456781234567" ;
        nret = tripledes_Encrypt( crypto.ECB_MODE , crypto.KEY_192BITS , key24B , dec , crypto.ZERO_PADDING ) ;
        if( nret )
                return -nret;

        dec = "1234567812345678" ;
        nret = tripledes_Encrypt( crypto.ECB_MODE , crypto.KEY_192BITS , key24B , dec , crypto.ZERO_PADDING ) ;
        if( nret )
                return -nret;

        dec = "12345678123456781" ;
        nret = tripledes_Encrypt( crypto.ECB_MODE , crypto.KEY_192BITS , key24B , dec , crypto.ZERO_PADDING ) ;
        if( nret )
                return -nret;

        stdout.Println( "\n--- ECB MODE & key24B & PKCS7 PADDING ---" );

        dec = "123456781234567" ;
        nret = tripledes_Encrypt( crypto.ECB_MODE , crypto.KEY_192BITS , key24B , dec , crypto.PKCS7_PADDING ) ;
        if( nret )
                return -nret;

        dec = "1234567812345678" ;
        nret = tripledes_Encrypt( crypto.ECB_MODE , crypto.KEY_192BITS , key24B , dec , crypto.PKCS7_PADDING ) ;
        if( nret )
                return -nret;

        dec = "12345678123456781" ;
        nret = tripledes_Encrypt( crypto.ECB_MODE , crypto.KEY_192BITS , key24B , dec , crypto.PKCS7_PADDING ) ;
        if( nret )
                return -nret;

        stdout.Println( "\n--- CBC MODE & key16B & ZERO PADDING ---" );

        dec = "123456781234567" ;
        nret = tripledes_Encrypt( crypto.CBC_MODE , crypto.KEY_128BITS , key16B , dec , crypto.ZERO_PADDING ) ;
        if( nret )
                return -nret;

        dec = "1234567812345678" ;
        nret = tripledes_Encrypt( crypto.CBC_MODE , crypto.KEY_128BITS , key16B , dec , crypto.ZERO_PADDING ) ;
        if( nret )
                return -nret;

        dec = "12345678123456781" ;
        nret = tripledes_Encrypt( crypto.CBC_MODE , crypto.KEY_128BITS , key16B , dec , crypto.ZERO_PADDING ) ;
        if( nret )
                return -nret;

        stdout.Println( "\n--- CBC MODE & key16B & PKCS7 PADDING ---" );

        dec = "123456781234567" ;
        nret = tripledes_Encrypt( crypto.CBC_MODE , crypto.KEY_128BITS , key16B , dec , crypto.PKCS7_PADDING ) ;
        if( nret )
                return -nret;

        dec = "1234567812345678" ;
        nret = tripledes_Encrypt( crypto.CBC_MODE , crypto.KEY_128BITS , key16B , dec , crypto.PKCS7_PADDING ) ;
        if( nret )
                return -nret;

        dec = "12345678123456781" ;
        nret = tripledes_Encrypt( crypto.CBC_MODE , crypto.KEY_128BITS , key16B , dec , crypto.PKCS7_PADDING ) ;
        if( nret )
                return -nret;

        stdout.Println( "\n--- CBC MODE & key24B & ZERO PADDING ---" );

        dec = "123456781234567" ;
        nret = tripledes_Encrypt( crypto.CBC_MODE , crypto.KEY_192BITS , key24B , dec , crypto.ZERO_PADDING ) ;
        if( nret )
                return -nret;

        dec = "1234567812345678" ;
        nret = tripledes_Encrypt( crypto.CBC_MODE , crypto.KEY_192BITS , key24B , dec , crypto.ZERO_PADDING ) ;
        if( nret )
                return -nret;

        dec = "12345678123456781" ;
        nret = tripledes_Encrypt( crypto.CBC_MODE , crypto.KEY_192BITS , key24B , dec , crypto.ZERO_PADDING ) ;
        if( nret )
                return -nret;

        stdout.Println( "\n--- CBC MODE & key24B & PKCS7 PADDING ---" );

        dec = "123456781234567" ;
        nret = tripledes_Encrypt( crypto.CBC_MODE , crypto.KEY_192BITS , key24B , dec , crypto.PKCS7_PADDING ) ;
        if( nret )
                return -nret;

        dec = "1234567812345678" ;
        nret = tripledes_Encrypt( crypto.CBC_MODE , crypto.KEY_192BITS , key24B , dec , crypto.PKCS7_PADDING ) ;
        if( nret )
                return -nret;

        dec = "12345678123456781" ;
        nret = tripledes_Encrypt( crypto.CBC_MODE , crypto.KEY_192BITS , key24B , dec , crypto.PKCS7_PADDING ) ;
        if( nret )
                return -nret;

        return 0;
}
```

执行

```
$ zlang test_crypto_tripledes_1.z                                                                                                                                                        [3/1985]

--- ECB MODE & key16B & ZERO PADDING ---
dec:TEXT[123456781234567] ---tripledes.Encrypt-mode[1]-key:TYPE[128]:TEXT[8765432187654321]-padding[0]---> enc:HEX[0DA06156D09594C39A4CB153FF855891] ---tripledes.Decrypt-mode[1]-key:TYPE[128]:TEXT[8765432187654321]-padding[0]---> dec:TEX
T[123456781234567]
dec:TEXT[1234567812345678] ---tripledes.Encrypt-mode[1]-key:TYPE[128]:TEXT[8765432187654321]-padding[0]---> enc:HEX[0DA06156D09594C30DA06156D09594C3] ---tripledes.Decrypt-mode[1]-key:TYPE[128]:TEXT[8765432187654321]-padding[0]---> dec:TE
XT[1234567812345678]
dec:TEXT[12345678123456781] ---tripledes.Encrypt-mode[1]-key:TYPE[128]:TEXT[8765432187654321]-padding[0]---> enc:HEX[0DA06156D09594C30DA06156D09594C33FB60F5F9394CD23] ---tripledes.Decrypt-mode[1]-key:TYPE[128]:TEXT[8765432187654321]-padd
ing[0]---> dec:TEXT[12345678123456781]

--- ECB MODE & key16B & PKCS7 PADDING ---
dec:TEXT[123456781234567] ---tripledes.Encrypt-mode[1]-key:TYPE[128]:TEXT[8765432187654321]-padding[7]---> enc:HEX[0DA06156D09594C3BCC6FE2A7AA93BE0] ---tripledes.Decrypt-mode[1]-key:TYPE[128]:TEXT[8765432187654321]-padding[7]---> dec:TEX
T[123456781234567]
dec:TEXT[1234567812345678] ---tripledes.Encrypt-mode[1]-key:TYPE[128]:TEXT[8765432187654321]-padding[7]---> enc:HEX[0DA06156D09594C30DA06156D09594C3CB12A48C6B54C99E] ---tripledes.Decrypt-mode[1]-key:TYPE[128]:TEXT[8765432187654321]-paddi
ng[7]---> dec:TEXT[1234567812345678]
dec:TEXT[12345678123456781] ---tripledes.Encrypt-mode[1]-key:TYPE[128]:TEXT[8765432187654321]-padding[7]---> enc:HEX[0DA06156D09594C30DA06156D09594C3551A4DA8B3285514] ---tripledes.Decrypt-mode[1]-key:TYPE[128]:TEXT[8765432187654321]-padd
ing[7]---> dec:TEXT[12345678123456781]

--- ECB MODE & key24B & ZERO PADDING ---
dec:TEXT[123456781234567] ---tripledes.Encrypt-mode[1]-key:TYPE[192]:TEXT[876543218765432187654321]-padding[0]---> enc:HEX[0DA06156D09594C39A4CB153FF855891] ---tripledes.Decrypt-mode[1]-key:TYPE[192]:TEXT[876543218765432187654321]-paddin
g[0]---> dec:TEXT[123456781234567]
dec:TEXT[1234567812345678] ---tripledes.Encrypt-mode[1]-key:TYPE[192]:TEXT[876543218765432187654321]-padding[0]---> enc:HEX[0DA06156D09594C30DA06156D09594C3] ---tripledes.Decrypt-mode[1]-key:TYPE[192]:TEXT[876543218765432187654321]-paddi
ng[0]---> dec:TEXT[1234567812345678]
dec:TEXT[12345678123456781] ---tripledes.Encrypt-mode[1]-key:TYPE[192]:TEXT[876543218765432187654321]-padding[0]---> enc:HEX[0DA06156D09594C30DA06156D09594C33FB60F5F9394CD23] ---tripledes.Decrypt-mode[1]-key:TYPE[192]:TEXT[87654321876543
2187654321]-padding[0]---> dec:TEXT[12345678123456781]

--- ECB MODE & key24B & PKCS7 PADDING ---
dec:TEXT[123456781234567] ---tripledes.Encrypt-mode[1]-key:TYPE[192]:TEXT[876543218765432187654321]-padding[7]---> enc:HEX[0DA06156D09594C3BCC6FE2A7AA93BE0] ---tripledes.Decrypt-mode[1]-key:TYPE[192]:TEXT[876543218765432187654321]-paddin
g[7]---> dec:TEXT[123456781234567]
dec:TEXT[1234567812345678] ---tripledes.Encrypt-mode[1]-key:TYPE[192]:TEXT[876543218765432187654321]-padding[7]---> enc:HEX[0DA06156D09594C30DA06156D09594C3CB12A48C6B54C99E] ---tripledes.Decrypt-mode[1]-key:TYPE[192]:TEXT[876543218765432
187654321]-padding[7]---> dec:TEXT[1234567812345678]
dec:TEXT[12345678123456781] ---tripledes.Encrypt-mode[1]-key:TYPE[192]:TEXT[876543218765432187654321]-padding[7]---> enc:HEX[0DA06156D09594C30DA06156D09594C3551A4DA8B3285514] ---tripledes.Decrypt-mode[1]-key:TYPE[192]:TEXT[87654321876543
2187654321]-padding[7]---> dec:TEXT[12345678123456781]

--- CBC MODE & key16B & ZERO PADDING ---
dec:TEXT[123456781234567] ---tripledes.Encrypt-mode[2]-key:TYPE[128]:TEXT[8765432187654321]-padding[0]---> enc:HEX[0DA06156D09594C3CF5A1CAF173F10DC] ---tripledes.Decrypt-mode[2]-key:TYPE[128]:TEXT[8765432187654321]-padding[0]---> dec:TEX
T[123456781234567]
dec:TEXT[1234567812345678] ---tripledes.Encrypt-mode[2]-key:TYPE[128]:TEXT[8765432187654321]-padding[0]---> enc:HEX[0DA06156D09594C3771D426663F6AF80] ---tripledes.Decrypt-mode[2]-key:TYPE[128]:TEXT[8765432187654321]-padding[0]---> dec:TE
XT[1234567812345678]
dec:TEXT[12345678123456781] ---tripledes.Encrypt-mode[2]-key:TYPE[128]:TEXT[8765432187654321]-padding[0]---> enc:HEX[0DA06156D09594C3771D426663F6AF809938363A112F5DAA] ---tripledes.Decrypt-mode[2]-key:TYPE[128]:TEXT[8765432187654321]-padd
ing[0]---> dec:TEXT[12345678123456781]

--- CBC MODE & key16B & PKCS7 PADDING ---
dec:TEXT[123456781234567] ---tripledes.Encrypt-mode[2]-key:TYPE[128]:TEXT[8765432187654321]-padding[7]---> enc:HEX[0DA06156D09594C3D0586EA12C8B4457] ---tripledes.Decrypt-mode[2]-key:TYPE[128]:TEXT[8765432187654321]-padding[7]---> dec:TEX
T[123456781234567]
dec:TEXT[1234567812345678] ---tripledes.Encrypt-mode[2]-key:TYPE[128]:TEXT[8765432187654321]-padding[7]---> enc:HEX[0DA06156D09594C3771D426663F6AF80C85393F005511C29] ---tripledes.Decrypt-mode[2]-key:TYPE[128]:TEXT[8765432187654321]-paddi
ng[7]---> dec:TEXT[1234567812345678]
dec:TEXT[12345678123456781] ---tripledes.Encrypt-mode[2]-key:TYPE[128]:TEXT[8765432187654321]-padding[7]---> enc:HEX[0DA06156D09594C3771D426663F6AF80C1B142DFE51D3735] ---tripledes.Decrypt-mode[2]-key:TYPE[128]:TEXT[8765432187654321]-padd
ing[7]---> dec:TEXT[12345678123456781]

--- CBC MODE & key24B & ZERO PADDING ---
dec:TEXT[123456781234567] ---tripledes.Encrypt-mode[2]-key:TYPE[192]:TEXT[876543218765432187654321]-padding[0]---> enc:HEX[0DA06156D09594C3CF5A1CAF173F10DC] ---tripledes.Decrypt-mode[2]-key:TYPE[192]:TEXT[876543218765432187654321]-paddin
g[0]---> dec:TEXT[123456781234567]
dec:TEXT[1234567812345678] ---tripledes.Encrypt-mode[2]-key:TYPE[192]:TEXT[876543218765432187654321]-padding[0]---> enc:HEX[0DA06156D09594C3771D426663F6AF80] ---tripledes.Decrypt-mode[2]-key:TYPE[192]:TEXT[876543218765432187654321]-paddi
ng[0]---> dec:TEXT[1234567812345678]
dec:TEXT[12345678123456781] ---tripledes.Encrypt-mode[2]-key:TYPE[192]:TEXT[876543218765432187654321]-padding[0]---> enc:HEX[0DA06156D09594C3771D426663F6AF809938363A112F5DAA] ---tripledes.Decrypt-mode[2]-key:TYPE[192]:TEXT[87654321876543
2187654321]-padding[0]---> dec:TEXT[12345678123456781]

--- CBC MODE & key24B & PKCS7 PADDING ---
dec:TEXT[123456781234567] ---tripledes.Encrypt-mode[2]-key:TYPE[192]:TEXT[876543218765432187654321]-padding[7]---> enc:HEX[0DA06156D09594C3D0586EA12C8B4457] ---tripledes.Decrypt-mode[2]-key:TYPE[192]:TEXT[876543218765432187654321]-paddin
g[7]---> dec:TEXT[123456781234567]
dec:TEXT[1234567812345678] ---tripledes.Encrypt-mode[2]-key:TYPE[192]:TEXT[876543218765432187654321]-padding[7]---> enc:HEX[0DA06156D09594C3771D426663F6AF80C85393F005511C29] ---tripledes.Decrypt-mode[2]-key:TYPE[192]:TEXT[876543218765432
187654321]-padding[7]---> dec:TEXT[1234567812345678]
dec:TEXT[12345678123456781] ---tripledes.Encrypt-mode[2]-key:TYPE[192]:TEXT[876543218765432187654321]-padding[7]---> enc:HEX[0DA06156D09594C3771D426663F6AF80C1B142DFE51D3735] ---tripledes.Decrypt-mode[2]-key:TYPE[192]:TEXT[87654321876543
2187654321]-padding[7]---> dec:TEXT[12345678123456781]
```

### AES

AES加密算法（Advanced Encryption Standard）是一种对称加密算法，也称为高级加密标准。它是由美国国家标准与技术研究院（NIST）于2001年发布，作为DES加密算法的替代方案。AES加密算法使用128位、192位或256位密钥对数据进行加密和解密，具有高强度、高速度和易于实现等优点。

AES对象名：`aes`

对象内函数同上。

AES密钥长度类型有`crypto.KEY_128BITS`、`crypto.KEY_192BITS`、`crypto.KEY_256BITS`。

代码示例：`test_crypto_aes_1.z`

```
import stdtypes stdio encoding crypto;

function int aes_Encrypt( int mode , int key_len_type , string key , string dec , int padding_type )
{
        enc = aes.Encrypt( mode , key_len_type , key , dec , padding_type ) ;
        if( enc == null )
        {
                stdout.Println( "des.Encrypt failed" );
                return -1;
        }
        enc_exp = hexstr.HexExpand( enc ) ;

        dec2 = aes.Decrypt( mode , key_len_type , key , enc , padding_type ) ;
        if( dec2 == null )
        {
                stdout.Println( "des.Decrypt failed" );
                return -2;
        }

        stdout.FormatPrintln( "dec:TEXT[%{dec}] ---aes.Encrypt-mode[%{mode}]-key:TYPE[%{key_len_type}]:TEXT[%{key}]-padding[%{padding_type}]---> enc:HEX[%{enc_exp}] ---tripledes.Decrypt-mode[%{mode}]-key:TYPE[%{key_len_type}]:TEXT[%{key}
]-padding[%{padding_type}]---> dec:TEXT[%{dec2}]" );

        return 0;
}

function int main( array args )
{
        key16B = "8765432187654321" ;
        key32B = "87654321876543218765432187654321" ;

        stdout.Println( "\n--- ECB MODE & key16B & ZERO PADDING ---" );

        dec = "123456781234567" ;
        nret = aes_Encrypt( crypto.ECB_MODE , crypto.KEY_128BITS , key16B , dec , crypto.ZERO_PADDING ) ;
        if( nret )
                return -nret;

        dec = "1234567812345678" ;
        nret = aes_Encrypt( crypto.ECB_MODE , crypto.KEY_128BITS , key16B , dec , crypto.ZERO_PADDING ) ;
        if( nret )
                return -nret;

        dec = "12345678123456781" ;
        nret = aes_Encrypt( crypto.ECB_MODE , crypto.KEY_128BITS , key16B , dec , crypto.ZERO_PADDING ) ;
        if( nret )
                return -nret;

        stdout.Println( "\n--- ECB MODE & key16B & PKCS7 PADDING ---" );

        dec = "123456781234567" ;
        nret = aes_Encrypt( crypto.ECB_MODE , crypto.KEY_128BITS , key16B , dec , crypto.PKCS7_PADDING ) ;
        if( nret )
                return -nret;

        dec = "1234567812345678" ;
        nret = aes_Encrypt( crypto.ECB_MODE , crypto.KEY_128BITS , key16B , dec , crypto.PKCS7_PADDING ) ;
        if( nret )
                return -nret;

        dec = "12345678123456781" ;
        nret = aes_Encrypt( crypto.ECB_MODE , crypto.KEY_128BITS , key16B , dec , crypto.PKCS7_PADDING ) ;
        if( nret )
                return -nret;

        stdout.Println( "\n--- ECB MODE & key32B & ZERO PADDING ---" );

        dec = "123456781234567" ;
        nret = aes_Encrypt( crypto.ECB_MODE , crypto.KEY_256BITS , key32B , dec , crypto.ZERO_PADDING ) ;
        if( nret )
                return -nret;

        dec = "1234567812345678" ;
        nret = aes_Encrypt( crypto.ECB_MODE , crypto.KEY_256BITS , key32B , dec , crypto.ZERO_PADDING ) ;
        if( nret )
                return -nret;

        dec = "12345678123456781" ;
        nret = aes_Encrypt( crypto.ECB_MODE , crypto.KEY_256BITS , key32B , dec , crypto.ZERO_PADDING ) ;
        if( nret )
                return -nret;

        stdout.Println( "\n--- ECB MODE & key32B & PKCS7 PADDING ---" );

        dec = "123456781234567" ;
        nret = aes_Encrypt( crypto.ECB_MODE , crypto.KEY_256BITS , key32B , dec , crypto.PKCS7_PADDING ) ;
        if( nret )
                return -nret;

        dec = "1234567812345678" ;
        nret = aes_Encrypt( crypto.ECB_MODE , crypto.KEY_256BITS , key32B , dec , crypto.PKCS7_PADDING ) ;
        if( nret )
                return -nret;

        dec = "12345678123456781" ;
        nret = aes_Encrypt( crypto.ECB_MODE , crypto.KEY_256BITS , key32B , dec , crypto.PKCS7_PADDING ) ;
        if( nret )
                return -nret;

        stdout.Println( "\n--- CBC MODE & key16B & ZERO PADDING ---" );

        dec = "123456781234567" ;
        nret = aes_Encrypt( crypto.CBC_MODE , crypto.KEY_128BITS , key16B , dec , crypto.ZERO_PADDING ) ;
        if( nret )
                return -nret;

        dec = "1234567812345678" ;
        nret = aes_Encrypt( crypto.CBC_MODE , crypto.KEY_128BITS , key16B , dec , crypto.ZERO_PADDING ) ;
        if( nret )
                return -nret;

        dec = "12345678123456781" ;
        nret = aes_Encrypt( crypto.CBC_MODE , crypto.KEY_128BITS , key16B , dec , crypto.ZERO_PADDING ) ;
        if( nret )
                return -nret;

        stdout.Println( "\n--- CBC MODE & key16B & PKCS7 PADDING ---" );

        dec = "123456781234567" ;
        nret = aes_Encrypt( crypto.CBC_MODE , crypto.KEY_128BITS , key16B , dec , crypto.PKCS7_PADDING ) ;
        if( nret )
                return -nret;

        dec = "1234567812345678" ;
        nret = aes_Encrypt( crypto.CBC_MODE , crypto.KEY_128BITS , key16B , dec , crypto.PKCS7_PADDING ) ;
        if( nret )
                return -nret;

        dec = "12345678123456781" ;
        nret = aes_Encrypt( crypto.CBC_MODE , crypto.KEY_128BITS , key16B , dec , crypto.PKCS7_PADDING ) ;
        if( nret )
                return -nret;

        stdout.Println( "\n--- CBC MODE & key32B & ZERO PADDING ---" );

        dec = "123456781234567" ;
        nret = aes_Encrypt( crypto.CBC_MODE , crypto.KEY_256BITS , key32B , dec , crypto.ZERO_PADDING ) ;
        if( nret )
                return -nret;

        dec = "1234567812345678" ;
        nret = aes_Encrypt( crypto.CBC_MODE , crypto.KEY_256BITS , key32B , dec , crypto.ZERO_PADDING ) ;
        if( nret )
                return -nret;

        dec = "12345678123456781" ;
        nret = aes_Encrypt( crypto.CBC_MODE , crypto.KEY_256BITS , key32B , dec , crypto.ZERO_PADDING ) ;
        if( nret )
                return -nret;

        stdout.Println( "\n--- CBC MODE & key32B & PKCS7 PADDING ---" );

        dec = "123456781234567" ;
        nret = aes_Encrypt( crypto.CBC_MODE , crypto.KEY_256BITS , key32B , dec , crypto.PKCS7_PADDING ) ;
        if( nret )
                return -nret;

        dec = "1234567812345678" ;
        nret = aes_Encrypt( crypto.CBC_MODE , crypto.KEY_256BITS , key32B , dec , crypto.PKCS7_PADDING ) ;
        if( nret )
                return -nret;

        dec = "12345678123456781" ;
        nret = aes_Encrypt( crypto.CBC_MODE , crypto.KEY_256BITS , key32B , dec , crypto.PKCS7_PADDING ) ;
        if( nret )
                return -nret;

        return 0;
}
```

执行

```
$ zlang test_crypto_aes_1.z                                                                                                                                                              [3/1809]

--- ECB MODE & key16B & ZERO PADDING ---
dec:TEXT[123456781234567] ---aes.Encrypt-mode[1]-key:TYPE[128]:TEXT[8765432187654321]-padding[0]---> enc:HEX[289F971ABC986500053F42F5CC4D21FE] ---tripledes.Decrypt-mode[1]-key:TYPE[128]:TEXT[8765432187654321]-padding[0]---> dec:TEXT[1234
56781234567]
dec:TEXT[1234567812345678] ---aes.Encrypt-mode[1]-key:TYPE[128]:TEXT[8765432187654321]-padding[0]---> enc:HEX[511BA8A228FF5205AB7BBBF8E5A4509E] ---tripledes.Decrypt-mode[1]-key:TYPE[128]:TEXT[8765432187654321]-padding[0]---> dec:TEXT[123
4567812345678]
dec:TEXT[12345678123456781] ---aes.Encrypt-mode[1]-key:TYPE[128]:TEXT[8765432187654321]-padding[0]---> enc:HEX[511BA8A228FF5205AB7BBBF8E5A4509EF9E866E1A84B11D45B1F6590E5027CBB] ---tripledes.Decrypt-mode[1]-key:TYPE[128]:TEXT[876543218765
4321]-padding[0]---> dec:TEXT[12345678123456781]

--- ECB MODE & key16B & PKCS7 PADDING ---
dec:TEXT[123456781234567] ---aes.Encrypt-mode[1]-key:TYPE[128]:TEXT[8765432187654321]-padding[7]---> enc:HEX[F9CB8326118EE2759E5D2F6E4971C0F0] ---tripledes.Decrypt-mode[1]-key:TYPE[128]:TEXT[8765432187654321]-padding[7]---> dec:TEXT[1234
56781234567]
dec:TEXT[1234567812345678] ---aes.Encrypt-mode[1]-key:TYPE[128]:TEXT[8765432187654321]-padding[7]---> enc:HEX[511BA8A228FF5205AB7BBBF8E5A4509ECF123E559CE5C7E1B9BDA5C234C7BC8E] ---tripledes.Decrypt-mode[1]-key:TYPE[128]:TEXT[8765432187654
321]-padding[7]---> dec:TEXT[1234567812345678]
dec:TEXT[12345678123456781] ---aes.Encrypt-mode[1]-key:TYPE[128]:TEXT[8765432187654321]-padding[7]---> enc:HEX[511BA8A228FF5205AB7BBBF8E5A4509EF05483B713180F35DD32FA7B47F0B008] ---tripledes.Decrypt-mode[1]-key:TYPE[128]:TEXT[876543218765
4321]-padding[7]---> dec:TEXT[12345678123456781]

--- ECB MODE & key32B & ZERO PADDING ---
dec:TEXT[123456781234567] ---aes.Encrypt-mode[1]-key:TYPE[256]:TEXT[87654321876543218765432187654321]-padding[0]---> enc:HEX[320D890B8CB15AD5694262A151806F43] ---tripledes.Decrypt-mode[1]-key:TYPE[256]:TEXT[876543218765432187654321876543
21]-padding[0]---> dec:TEXT[123456781234567]
dec:TEXT[1234567812345678] ---aes.Encrypt-mode[1]-key:TYPE[256]:TEXT[87654321876543218765432187654321]-padding[0]---> enc:HEX[C9B0A1E5976F58F659F1AA05DC47740F] ---tripledes.Decrypt-mode[1]-key:TYPE[256]:TEXT[87654321876543218765432187654
321]-padding[0]---> dec:TEXT[1234567812345678]
dec:TEXT[12345678123456781] ---aes.Encrypt-mode[1]-key:TYPE[256]:TEXT[87654321876543218765432187654321]-padding[0]---> enc:HEX[C9B0A1E5976F58F659F1AA05DC47740F1E654FF1E641115A51DE66ED08C61FBC] ---tripledes.Decrypt-mode[1]-key:TYPE[256]:T
EXT[87654321876543218765432187654321]-padding[0]---> dec:TEXT[12345678123456781]

--- ECB MODE & key32B & PKCS7 PADDING ---
dec:TEXT[123456781234567] ---aes.Encrypt-mode[1]-key:TYPE[256]:TEXT[87654321876543218765432187654321]-padding[7]---> enc:HEX[EF1F9108B8E9E51BBA9DA52630611002] ---tripledes.Decrypt-mode[1]-key:TYPE[256]:TEXT[876543218765432187654321876543
21]-padding[7]---> dec:TEXT[123456781234567]
dec:TEXT[1234567812345678] ---aes.Encrypt-mode[1]-key:TYPE[256]:TEXT[87654321876543218765432187654321]-padding[7]---> enc:HEX[C9B0A1E5976F58F659F1AA05DC47740FFA9C3007700C320657D81E20DDF148E9] ---tripledes.Decrypt-mode[1]-key:TYPE[256]:TE
XT[87654321876543218765432187654321]-padding[7]---> dec:TEXT[1234567812345678]
dec:TEXT[12345678123456781] ---aes.Encrypt-mode[1]-key:TYPE[256]:TEXT[87654321876543218765432187654321]-padding[7]---> enc:HEX[C9B0A1E5976F58F659F1AA05DC47740F31AA2B57338FA4920E9E61ADCE733EAA] ---tripledes.Decrypt-mode[1]-key:TYPE[256]:T
EXT[87654321876543218765432187654321]-padding[7]---> dec:TEXT[12345678123456781]

--- CBC MODE & key16B & ZERO PADDING ---
dec:TEXT[123456781234567] ---aes.Encrypt-mode[2]-key:TYPE[128]:TEXT[8765432187654321]-padding[0]---> enc:HEX[289F971ABC986500053F42F5CC4D21FE] ---tripledes.Decrypt-mode[2]-key:TYPE[128]:TEXT[8765432187654321]-padding[0]---> dec:TEXT[1234
56781234567]
dec:TEXT[1234567812345678] ---aes.Encrypt-mode[2]-key:TYPE[128]:TEXT[8765432187654321]-padding[0]---> enc:HEX[511BA8A228FF5205AB7BBBF8E5A4509E] ---tripledes.Decrypt-mode[2]-key:TYPE[128]:TEXT[8765432187654321]-padding[0]---> dec:TEXT[123
4567812345678]
dec:TEXT[12345678123456781] ---aes.Encrypt-mode[2]-key:TYPE[128]:TEXT[8765432187654321]-padding[0]---> enc:HEX[511BA8A228FF5205AB7BBBF8E5A4509EF2251A19A66AA413F3DB1771E5C98642] ---tripledes.Decrypt-mode[2]-key:TYPE[128]:TEXT[876543218765
4321]-padding[0]---> dec:TEXT[12345678123456781]

--- CBC MODE & key16B & PKCS7 PADDING ---
dec:TEXT[123456781234567] ---aes.Encrypt-mode[2]-key:TYPE[128]:TEXT[8765432187654321]-padding[7]---> enc:HEX[F9CB8326118EE2759E5D2F6E4971C0F0] ---tripledes.Decrypt-mode[2]-key:TYPE[128]:TEXT[8765432187654321]-padding[7]---> dec:TEXT[1234
56781234567]
dec:TEXT[1234567812345678] ---aes.Encrypt-mode[2]-key:TYPE[128]:TEXT[8765432187654321]-padding[7]---> enc:HEX[511BA8A228FF5205AB7BBBF8E5A4509E564E53000595926FF4D862D43DF966C8] ---tripledes.Decrypt-mode[2]-key:TYPE[128]:TEXT[8765432187654
321]-padding[7]---> dec:TEXT[1234567812345678]
dec:TEXT[12345678123456781] ---aes.Encrypt-mode[2]-key:TYPE[128]:TEXT[8765432187654321]-padding[7]---> enc:HEX[511BA8A228FF5205AB7BBBF8E5A4509E301E3D45CE65AE05FC2202A22C9844FF] ---tripledes.Decrypt-mode[2]-key:TYPE[128]:TEXT[876543218765
4321]-padding[7]---> dec:TEXT[12345678123456781]

--- CBC MODE & key32B & ZERO PADDING ---
dec:TEXT[123456781234567] ---aes.Encrypt-mode[2]-key:TYPE[256]:TEXT[87654321876543218765432187654321]-padding[0]---> enc:HEX[320D890B8CB15AD5694262A151806F43] ---tripledes.Decrypt-mode[2]-key:TYPE[256]:TEXT[876543218765432187654321876543
21]-padding[0]---> dec:TEXT[123456781234567]
dec:TEXT[1234567812345678] ---aes.Encrypt-mode[2]-key:TYPE[256]:TEXT[87654321876543218765432187654321]-padding[0]---> enc:HEX[C9B0A1E5976F58F659F1AA05DC47740F] ---tripledes.Decrypt-mode[2]-key:TYPE[256]:TEXT[87654321876543218765432187654
321]-padding[0]---> dec:TEXT[1234567812345678]
dec:TEXT[12345678123456781] ---aes.Encrypt-mode[2]-key:TYPE[256]:TEXT[87654321876543218765432187654321]-padding[0]---> enc:HEX[C9B0A1E5976F58F659F1AA05DC47740FAE9FD9E24053F708C38FC33EF8C01EF2] ---tripledes.Decrypt-mode[2]-key:TYPE[256]:T
EXT[87654321876543218765432187654321]-padding[0]---> dec:TEXT[12345678123456781]

--- CBC MODE & key32B & PKCS7 PADDING ---
dec:TEXT[123456781234567] ---aes.Encrypt-mode[2]-key:TYPE[256]:TEXT[87654321876543218765432187654321]-padding[7]---> enc:HEX[EF1F9108B8E9E51BBA9DA52630611002] ---tripledes.Decrypt-mode[2]-key:TYPE[256]:TEXT[876543218765432187654321876543
21]-padding[7]---> dec:TEXT[123456781234567]
dec:TEXT[1234567812345678] ---aes.Encrypt-mode[2]-key:TYPE[256]:TEXT[87654321876543218765432187654321]-padding[7]---> enc:HEX[C9B0A1E5976F58F659F1AA05DC47740F25E9C1E7D33DB90C67936EDBFCDE594E] ---tripledes.Decrypt-mode[2]-key:TYPE[256]:TE
XT[87654321876543218765432187654321]-padding[7]---> dec:TEXT[1234567812345678]
dec:TEXT[12345678123456781] ---aes.Encrypt-mode[2]-key:TYPE[256]:TEXT[87654321876543218765432187654321]-padding[7]---> enc:HEX[C9B0A1E5976F58F659F1AA05DC47740F5B13C0217ED0701884D02B0C88BF4A44] ---tripledes.Decrypt-mode[2]-key:TYPE[256]:T
EXT[87654321876543218765432187654321]-padding[7]---> dec:TEXT[12345678123456781]
```

### SM4

SM4是一种对称加密算法，属于分组密码的一种，由中国国家密码管理局于2012年3月21日发布，属于国家商用密码算法。SM4算法采用128位密钥，加密和解密使用相同的密钥，属于对称加密算法。其分组长度为128位，即16字节。

值得注意的是，由于SM4算法属于国家商用密码算法，其具体的实现细节和安全性评估都需要遵循国家的相关规定和标准。因此，在使用SM4算法时，需要确保遵守相关的法律法规和规定，以确保信息的安全性和合规性。

SM4对象名：`sm4`

对象内函数同上。

SM4密钥长度类型目前只支持`crypto.KEY_128BITS`。

代码示例：`test_crypto_sm4_1.z`

```
import stdtypes stdio encoding crypto;

function int sm4_Encrypt( int mode , int key_len_type , string key , string dec , int padding_type )
{
        enc = sm4.Encrypt( mode , key_len_type , key , dec , padding_type ) ;
        if( enc == null )
        {
                stdout.Println( "des.Encrypt failed" );
                return -1;
        }
        enc_exp = hexstr.HexExpand( enc ) ;

        dec2 = sm4.Decrypt( mode , key_len_type , key , enc , padding_type ) ;
        if( dec2 == null )
        {
                stdout.Println( "des.Decrypt failed" );
                return -2;
        }

        stdout.FormatPrintln( "dec:TEXT[%{dec}] ---sm4.Encrypt-mode[%{mode}]-key:TYPE[%{key_len_type}]:TEXT[%{key}]-padding[%{padding_type}]---> enc:HEX[%{enc_exp}] ---sm4.Decrypt-mode[%{mode}]-key:TYPE[%{key_len_type}]:TEXT[%{key}]-padd
ing[%{padding_type}]---> dec:TEXT[%{dec2}]" );

        return 0;
}

function int main( array args )
{
        key16B = "8765432187654321" ;

        stdout.Println( "\n--- ECB MODE & key16B & PKCS7 PADDING ---" );

        dec = "123456781234567" ;
        nret = sm4_Encrypt( crypto.ECB_MODE , crypto.KEY_128BITS , key16B , dec , crypto.PKCS7_PADDING ) ;
        if( nret )
                return -nret;

        dec = "1234567812345678" ;
        nret = sm4_Encrypt( crypto.ECB_MODE , crypto.KEY_128BITS , key16B , dec , crypto.PKCS7_PADDING ) ;
        if( nret )
                return -nret;

        dec = "12345678123456781" ;
        nret = sm4_Encrypt( crypto.ECB_MODE , crypto.KEY_128BITS , key16B , dec , crypto.PKCS7_PADDING ) ;
        if( nret )
                return -nret;

        stdout.Println( "\n--- CBC MODE & key16B & PKCS7 PADDING ---" );

        dec = "123456781234567" ;
        nret = sm4_Encrypt( crypto.CBC_MODE , crypto.KEY_128BITS , key16B , dec , crypto.PKCS7_PADDING ) ;
        if( nret )
                return -nret;

        dec = "1234567812345678" ;
        nret = sm4_Encrypt( crypto.CBC_MODE , crypto.KEY_128BITS , key16B , dec , crypto.PKCS7_PADDING ) ;
        if( nret )
                return -nret;

        dec = "12345678123456781" ;
        nret = sm4_Encrypt( crypto.CBC_MODE , crypto.KEY_128BITS , key16B , dec , crypto.PKCS7_PADDING ) ;
        if( nret )
                return -nret;
                
        return 0;
}

```

执行

```
$ zlang test_crypto_sm4_1.z

--- ECB MODE & key16B & PKCS7 PADDING ---
dec:TEXT[123456781234567] ---sm4.Encrypt-mode[1]-key:TYPE[128]:TEXT[8765432187654321]-padding[7]---> enc:HEX[A1504F32DE1D9C3C935F69E6FCE23685] ---sm4.Decrypt-mode[1]-key:TYPE[128]:TEXT[8765432187654321]-padding[7]---> dec:TEXT[123456781234567]
dec:TEXT[1234567812345678] ---sm4.Encrypt-mode[1]-key:TYPE[128]:TEXT[8765432187654321]-padding[7]---> enc:HEX[FDD9581DF03E04DA126E0F7AA816387F0EB784E6103540B77356C5BE04F4B1BF] ---sm4.Decrypt-mode[1]-key:TYPE[128]:TEXT[8765432187654321]-padding[7]---> dec:TEXT[1234567812345678]
dec:TEXT[12345678123456781] ---sm4.Encrypt-mode[1]-key:TYPE[128]:TEXT[8765432187654321]-padding[7]---> enc:HEX[FDD9581DF03E04DA126E0F7AA816387F36957CF3C2FD127F9929BB4E945FC5F7] ---sm4.Decrypt-mode[1]-key:TYPE[128]:TEXT[8765432187654321]-padding[7]---> dec:TEXT[12345678123456781]

--- CBC MODE & key16B & PKCS7 PADDING ---
dec:TEXT[123456781234567] ---sm4.Encrypt-mode[2]-key:TYPE[128]:TEXT[8765432187654321]-padding[7]---> enc:HEX[A1504F32DE1D9C3C935F69E6FCE23685] ---sm4.Decrypt-mode[2]-key:TYPE[128]:TEXT[8765432187654321]-padding[7]---> dec:TEXT[123456781234567]
dec:TEXT[1234567812345678] ---sm4.Encrypt-mode[2]-key:TYPE[128]:TEXT[8765432187654321]-padding[7]---> enc:HEX[FDD9581DF03E04DA126E0F7AA816387FBCAA14A75FCA64173005765FCCCAF0BA] ---sm4.Decrypt-mode[2]-key:TYPE[128]:TEXT[8765432187654321]-padding[7]---> dec:TEXT[1234567812345678]
dec:TEXT[12345678123456781] ---sm4.Encrypt-mode[2]-key:TYPE[128]:TEXT[8765432187654321]-padding[7]---> enc:HEX[FDD9581DF03E04DA126E0F7AA816387FEE3F869EBE448831C7C3E9C8458FC31A] ---sm4.Decrypt-mode[2]-key:TYPE[128]:TEXT[8765432187654321]-padding[7]---> dec:TEXT[12345678123456781]
```

## 非对称加解密和签名验签

### RSA

RSA算法是一种非对称加密算法，由罗纳德·李维斯特（Ron Rivest）、阿迪·萨莫尔（Adi Shamir）和伦纳德·阿德曼（Leonard Adleman）共同提出，是第一个既能用于数据加密也能用于数字签名的算法。RSA算法的安全性基于大数分解的难度，即两个大质数相乘很容易，但将其乘积分解回原来的两个质数却极其困难。这种计算上的不对称性是RSA算法安全性的基础。

RSA对大型数据的加解密的性能较差。

RSA签名过程是先对数据进行摘要，得到较短的摘要值，再对摘要值进行加密。验签过程是对密文进行解密，在和对数据进行摘要的摘要值进行对比，即验证签名。

RSA密钥是一对公私钥，公钥分发给对方，自己保管好私钥；对方用公钥加密，只有自己用私钥才能解密；自己用私钥签名，别人用公钥验签通过证明当初是自己签名的。从技术上讲，反过来用，用私钥加密+公钥解密、用公钥签名+私钥验签也是可以的，但应用场景决定了不会反过来用，即你拿在手里的肯定是私钥而不会是公钥。

RSA密钥对象：`rsakey`。

| 成员类型 | 成员名字和原型                        | 中文名            | 说明                                                         | (错误.)(异常码,)异常消息                                     |
| :------: | ------------------------------------- | ----------------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
|   函数   | bool GenerateKeyPair(int bits);       | 生成密钥对        | 给定密钥位数bits，生成RSA一对公私钥，保存在对象内；如果报错返回false | EXCEPTION_MESSAGE_GENERATE_KEY_PAIR_FAILED                   |
|   函数   | bool GenerateKeyPair(int bits,int e); | 生成密钥对        | 给定密钥位数bits和公钥指数e，生成RSA一对公私钥，保存在对象内；如果报错返回false | EXCEPTION_MESSAGE_GENERATE_KEY_PAIR_FAILED                   |
|   函数   | bool ImportPublicKeyFromPEM(string);  | 从PEM文件导入公钥 | 从PEM编码格式的公钥文件中导入公钥；如果报错返回false         | EXCEPTION_MESSAGE_FILE_NOT_FOUND<br />EXCEPTION_MESSAGE_READ_RSA_PUBLIC_KEY_IN_FILE_FAILED |
|   函数   | bool ImportPrivateKeyFromPEM(string); | 从PEM文件导入私钥 | 从PEM编码格式的私钥文件中导入私钥；如果报错返回false         | EXCEPTION_MESSAGE_FILE_NOT_FOUND<br />EXCEPTION_MESSAGE_READ_RSA_PRIVATE_KEY_IN_FILE_FAILED |
|   函数   | bool ImportPublicKeyFromDER(string);  | 从DER文件导入公钥 | 从DER编码格式的公钥文件中导入公钥；如果报错返回false         | EXCEPTION_MESSAGE_FILE_NOT_FOUND<br />EXCEPTION_MESSAGE_READ_RSA_PUBLIC_KEY_IN_FILE_FAILED |
|   函数   | bool ImportPrivateKeyFromDER(string); | 从DER文件导入私钥 | 从DER编码格式的私钥文件中导入私钥；如果报错返回false         | EXCEPTION_MESSAGE_FILE_NOT_FOUND<br />EXCEPTION_MESSAGE_READ_RSA_PUBLIC_KEY_IN_FILE_FAILED |
|   函数   | bool ExportPublicKeyToPEM(string);    | 导出公钥到PEM文件 | 导出公钥到PEM编码格式的公钥文件中；如果报错返回false         | EXCEPTION_MESSAGE_RSAKEY_HASNT_PUBLIC_KEY<br />EXCEPTION_MESSAGE_OPEN_WRITE_FILE_FAILED<br />EXCEPTION_MESSAGE_WRITE_RSA_PUBLIC_KEY_INTO_FILE_FAILED |
|   函数   | bool ExportPrivateKeyToPEM(string);   | 导出私钥到PEM文件 | 导出私钥到PEM编码格式的私钥文件中；如果报错返回false         | EXCEPTION_MESSAGE_RSAKEY_HASNT_PRIVATE_KEY<br />EXCEPTION_MESSAGE_OPEN_WRITE_FILE_FAILED<br />EXCEPTION_MESSAGE_WRITE_RSA_PRIVATE_KEY_INTO_FILE_FAILED |
|   函数   | bool ExportPublicKeyToDER(string);    | 导出公钥到DER文件 | 导出公钥到DER编码格式的公钥文件中；如果报错返回false         | EXCEPTION_MESSAGE_RSAKEY_HASNT_PUBLIC_KEY<br />EXCEPTION_MESSAGE_OPEN_WRITE_FILE_FAILED<br />EXCEPTION_MESSAGE_WRITE_RSA_PUBLIC_KEY_INTO_FILE_FAILED |
|   函数   | bool ExportPrivateKeyToDER(string);   | 导出私钥到DER文件 | 导出私钥到DER编码格式的私钥文件中；如果报错返回false         | EXCEPTION_MESSAGE_RSAKEY_HASNT_PUBLIC_KEY<br />EXCEPTION_MESSAGE_OPEN_WRITE_FILE_FAILED<br />EXCEPTION_MESSAGE_WRITE_RSA_PRIVATE_KEY_INTO_FILE_FAILED |

代码示例：`test_crypto_rsakey_1.z`

```
import stdtypes stdio encoding crypto;

function int main( array args )
{
        rsakey  key_pair ;
        rsakey  pubkey ;
        rsakey  prikey ;

        key_pair.GenerateKeyPair( crypto.KEY_2048BITS ) ;
        key_pair.ExportPublicKeyToPEM( "/tmp/rsa.pubkey.pem" ) ;
        key_pair.ExportPrivateKeyToPEM( "/tmp/rsa.prikey.pem" ) ;
        key_pair.ExportPublicKeyToDER( "/tmp/rsa.pubkey.der" ) ;
        key_pair.ExportPrivateKeyToDER( "/tmp/rsa.prikey.der" ) ;

        b = pubkey.ImportPublicKeyFromPEM( "/tmp/rsa.pubkey.pem" ) ;
        if( b != true )
        {
                stdout.Println( "ImportPublicKeyFromPEM failed" );
                return 1;
        }

        b = prikey.ImportPrivateKeyFromPEM( "/tmp/rsa.prikey.pem" ) ;
        if( b != true )
        {
                stdout.Println( "ImportPrivateKeyFromPEM failed" );
                return 1;
        }

        b = pubkey.ImportPublicKeyFromDER( "/tmp/rsa.pubkey.der" ) ;
        if( b != true )
        {
                stdout.Println( "ImportPublicKeyFromDER failed" );
                return 1;
        }

        b = prikey.ImportPrivateKeyFromDER( "/tmp/rsa.prikey.der" ) ;
        if( b != true )
        {
                stdout.Println( "ImportPrivateKeyFromDER failed" );
                return 1;
        }

        stdout.Println( "ok" );

        return 0;
}
```

执行

```
$ zlang test_crypto_rsakey_1.z
ok
$ ls -l /tmp/rsa.*.*
-rw-rw-r--. 1 calvin calvin 1191 Jun 17 21:22 /tmp/rsa.prikey.der
-rw-rw-r--. 1 calvin calvin 1675 Jun 17 21:22 /tmp/rsa.prikey.pem
-rw-rw-r--. 1 calvin calvin  270 Jun 17 21:22 /tmp/rsa.pubkey.der
-rw-rw-r--. 1 calvin calvin  426 Jun 17 21:22 /tmp/rsa.pubkey.pem
```

RSA对象：`rsa`。

| 成员类型 | 成员名字和原型                                               | 中文名   | 说明                                                         | (错误.)(异常码,)异常消息                                     |
| :------: | ------------------------------------------------------------ | -------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
|   函数   | string PublicKeyEncrypt(rsakey,string dec,int padding_type); | 公钥加密 | 用填充算法padding_type填充数据data后，用公钥加密；如果报错返回null。填充算法有`crypto.NO_PADDING`、`crypto.PKCS1_PADDING`、`crypto.PKCS1_OAEP_PADDING`。 | EXCEPTION_MESSAGE_RSAKEY_HASNT_PUBLIC_KEY<br />EXCEPTION_MESSAGE_PADDING_TYPE_INVALID<br />EXCEPTION_MESSAGE_RSA_ENCRYPT_FAILED |
|   函数   | string PrivateKeyEncrypt(rsakey,string dec,int padding_type); | 私钥加密 | 用填充算法padding_type填充数据data后，用私钥加密（一般不会这么用）；如果报错返回null | EXCEPTION_MESSAGE_RSAKEY_HASNT_PRIVATE_KEY<br />EXCEPTION_MESSAGE_DEC_OR_DEC_LEN_INVALID<br />EXCEPTION_MESSAGE_PADDING_TYPE_INVALID<br />EXCEPTION_MESSAGE_RSA_ENCRYPT_FAILED |
|   函数   | string PublicKeyDecrypt(rsakey,string enc,int padding_type); | 公钥解密 | 用公钥解密，然后用填充算法padding_type去除多余数据（一般不会这么用）；如果报错返回null | EXCEPTION_MESSAGE_RSAKEY_HASNT_PUBLIC_KEY<br />EXCEPTION_MESSAGE_ENC_OR_ENC_LEN_INVALID<br />EXCEPTION_MESSAGE_PADDING_TYPE_INVALID<br />EXCEPTION_MESSAGE_RSA_DECRYPT_FAILED |
|   函数   | string PrivateKeyDecrypt(rsakey,string enc,int padding_type); | 私钥解密 | 用私钥解密，然后用填充算法padding_type去除多余数据；如果报错返回null | EXCEPTION_MESSAGE_RSAKEY_HASNT_PRIVATE_KEY<br />EXCEPTION_MESSAGE_ENC_OR_ENC_LEN_INVALID<br />EXCEPTION_MESSAGE_PADDING_TYPE_INVALID<br />EXCEPTION_MESSAGE_RSA_DECRYPT_FAILED |
|   函数   | string Sign(int digest_type,string data,rsakey);             | 签名     | 用私钥签名数据（用摘要算法digest_type计算数据data后得到较短的摘要值，然后用私钥加密）；如果报错返回null。摘要算法有`crypto.NID_MD5`、`crypto.NID_SHA256`等，还有`crypto.NID_MD5_SHA1`。 | EXCEPTION_MESSAGE_DATA_OR_DATA_LEN_INVALID<br />EXCEPTION_MESSAGE_RSAKEY_HASNT_PRIVATE_KEY<br />EXCEPTION_MESSAGE_EVP_MD_CTX_NEW_FAILED<br />EXCEPTION_MESSAGE_EVP_PKEY_NEW_FAILED<br />EXCEPTION_MESSAGE_EVP_PKEY_SET_RSA_FAILED<br />EXCEPTION_MESSAGE_EVP_SIGNINIT_FAILED<br />EXCEPTION_MESSAGE_EVP_SIGNUPDATE_FAILED<br />EXCEPTION_MESSAGE_EVP_SIGNFINAL_FAILED |
|   函数   | bool Verify(int digest_type,string data,string sign,rsakey); | 验签     | 用公钥验签数据（用摘要算法digest_type计算数据data后得到较短的摘要值，然后用公钥解密签名数据，两者做对比），如果验签正确返回true，否则返回false；如果报错返回null | EXCEPTION_MESSAGE_DATA_OR_DATA_LEN_INVALID<br />EXCEPTION_MESSAGE_SIGN_DATA_OR_SIGN_DATA_LEN_INVALID<br />EXCEPTION_MESSAGE_RSAKEY_HASNT_PRIVATE_KEY<br />EXCEPTION_MESSAGE_EVP_MD_CTX_NEW_FAILED<br />EXCEPTION_MESSAGE_EVP_PKEY_NEW_FAILED<br />EXCEPTION_MESSAGE_EVP_PKEY_SET_RSA_FAILED<br />EXCEPTION_MESSAGE_EVP_VERIFYINIT_EX_FAILED<br />EXCEPTION_MESSAGE_EVP_VERIFYUPDATE_FAILED<br / |

代码示例：`test_crypto_rsa_encrypt_1.z`

````
import stdtypes stdio encoding crypto;

function int rsa_Encrypt( rsakey pubkey , rsakey prikey , string dec , int padding_type )
{
        stdout.FormatPrintln( "dec length[%d]" , dec.Length() );

        enc = rsa.PublicKeyEncrypt( pubkey , dec , padding_type ) ;
        if( enc == null )
        {
                stdout.Println( "rsa.PublicKeyEncrypt failed" );
                return -1;
        }
        enc_exp = hexstr.HexExpand( enc ) ;

        dec2 = rsa.PrivateKeyDecrypt( prikey , enc , padding_type ) ;
        if( dec2 == null )
        {
                stdout.Println( "rsa.PrivateKeyDecrypt failed" );
                return -2;
        }

        stdout.FormatPrintln( "dec:TEXT[%{dec}] ---rsa.PublicKeyEncrypt-padding[%{padding_type}]---> enc ---rsa.PrivateKeyDecrypt-padding[%{padding_type}]---> dec:TEXT[%{dec2}]" );

        return 0;
}

function int main( array args )
{
        rsakey  pubkey ;
        rsakey  prikey ;

        b = pubkey.ImportPublicKeyFromPEM( "/tmp/rsa.pubkey.pem" ) ;
        if( b != true )
        {
                stdout.Println( "ImportPublicKeyFromPEM failed" );
                return 1;
        }

        b = prikey.ImportPrivateKeyFromDER( "/tmp/rsa.prikey.der" ) ;
        if( b != true )
        {
                stdout.Println( "ImportPrivateKeyFromPEM failed" );
                return 1;
        }

        dec = "12345678" ;
        nret = rsa_Encrypt( pubkey , prikey , dec , crypto.NO_PADDING ) ;
        if( nret )
                return -nret;

        dec = "123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012
34567890123456789012345678901234567890" ;
        nret = rsa_Encrypt( pubkey , prikey , dec , crypto.NO_PADDING ) ;
        if( nret )
                return -nret;

        dec = "12345678" ;
        nret = rsa_Encrypt( pubkey , prikey , dec , crypto.PKCS1_PADDING ) ;
        if( nret )
                return -nret;

        dec = "123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012
34567890123456789012345678901234567890" ;
        nret = rsa_Encrypt( pubkey , prikey , dec , crypto.PKCS1_PADDING ) ;
        if( nret )
                return -nret;

        dec = "12345678" ;
        nret = rsa_Encrypt( pubkey , prikey , dec , crypto.PKCS1_OAEP_PADDING ) ;
        if( nret )
                return -nret;

        dec = "123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012
34567890123456789012345678901234567890" ;
        nret = rsa_Encrypt( pubkey , prikey , dec , crypto.PKCS1_OAEP_PADDING ) ;
        if( nret )
                return -nret;

        return 0;
}
````

执行

```
$ dec length[8]
dec:TEXT[12345678] ---rsa.PublicKeyEncrypt-padding[3]---> enc ---rsa.PrivateKeyDecrypt-padding[3]---> dec:TEXT[12345678]
dec length[260]
dec:TEXT[12345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890] ---rsa.PublicKeyEncrypt-padding[3]---> enc ---rsa.PrivateKeyDecrypt-padding[3]---> dec:TEXT[12345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890]
dec length[8]
dec:TEXT[12345678] ---rsa.PublicKeyEncrypt-padding[1]---> enc ---rsa.PrivateKeyDecrypt-padding[1]---> dec:TEXT[12345678]
dec length[260]
dec:TEXT[12345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890] ---rsa.PublicKeyEncrypt-padding[1]---> enc ---rsa.PrivateKeyDecrypt-padding[1]---> dec:TEXT[12345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890]
dec length[8]
dec:TEXT[12345678] ---rsa.PublicKeyEncrypt-padding[4]---> enc ---rsa.PrivateKeyDecrypt-padding[4]---> dec:TEXT[12345678]
dec length[260]
dec:TEXT[12345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890] ---rsa.PublicKeyEncrypt-padding[4]---> enc ---rsa.PrivateKeyDecrypt-padding[4]---> dec:TEXT[12345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890]
```

代码示例：`test_crypto_rsa_sign_1.z`

```
import stdtypes stdio encoding crypto;

function int rsa_Sign( int digest , string data , rsakey prikey , rsakey pubkey )
{
        stdout.FormatPrintln( "data length[%d]" , data.Length() );

        data_sign = rsa.Sign( crypto.NID_SHA256 , data , prikey ) ;
        if( data_sign == null )
        {
                stdout.Println( "rsa.Sign failed" );
                return -1;
        }
        data_sign_exp = hexstr.HexExpand( data_sign ) ;

        verify_result = rsa.Verify( crypto.NID_SHA256 , data , data_sign , pubkey ) ;
        if( verify_result != true )
        {
                stdout.Println( "rsa.Verify failed" );
                return -2;
        }

        stdout.FormatPrintln( "data:TEXT[%{data}] ---rsa.Sign-digest[%{digest}]---> data_sign[%{data_sign_exp}] ---rsa.Verify---> verify_result[%{verify_result}]" );

        return 0;
}

function int main( array args )
{
        rsakey  pubkey ;
        rsakey  prikey ;

        b = pubkey.ImportPublicKeyFromPEM( "/tmp/rsa.pubkey.pem" ) ;
        if( b != true )
        {
                stdout.Println( "ImportPublicKeyFromPEM failed" );
                return 1;
        }

        b = prikey.ImportPrivateKeyFromDER( "/tmp/rsa.prikey.der" ) ;
        if( b != true )
        {
                stdout.Println( "ImportPrivateKeyFromPEM failed" );
                return 1;
        }

        data = "12345678" ;
        nret = rsa_Sign( crypto.NID_MD5 , data , prikey , pubkey ) ;
        if( nret )
                return -nret;

        data = "12345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901
234567890123456789012345678901234567890" ;
        nret = rsa_Sign( crypto.NID_SHA256 , data , prikey , pubkey ) ;
        if( nret )
                return -nret;

        return 0;
}
```

执行

```
data length[8]
data:TEXT[12345678] ---rsa.Sign-digest[4]---> data_sign[69813C280C6ACE6B25F6E290BDA99FED849CD1308BB71138066316E7DFBF10FFC06F314301A16AE42BB5B8C2D099EF8FF0EF4E6EAC760EBBA7250256B977AFC1199F279962B9C1CB00BD6F6F7F5E80A5A7F14647D5A4E83178C5CF10B1639C166453539D06BB5CCD9ED472564D5D3B855354F3E0FE225FB5B1C16AD78D31F5B153CF849D3961155F6A2780C9B0DD7120C7AC57DB195F8EB4D9A0A84E867277601B3B294394C29C939BBAA21DA3980298629987DE5797CF311D60D9793FC50F888B50FC9A649F1AA5166D3235A299467EF22533EF008C0B9788D91A988A3AD769E1CF9AC8B2E6A8DCEE745A250679F6BDFD82BB6272ECCF7C425910D52F33F03A] ---rsa.Verify---> verify_result[true]
data length[260]
data:TEXT[12345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890] ---rsa.Sign-digest[672]---> data_sign[89F6B60A118F123E5A93BC173179C588AE683A664998480C172A402C6D80E9F7D8CF2861F4BDF9D4DC847D088A31F42D04CA2DC4647D00687BC95F9F300E2713B7AF08665BF1301191152830AED36432F825B2DC5E7551E162A2CEA9D51BE17D126D926C1BB5E2D30CB383F8A702E231D051277B1B000D44E8AFA1171334738B06A6161E750D72845E1E50C8F27DFB860664D6BBE150BA527DD428CC0301777767797FBD1193C7B5F25F6A0801E41989EBC1B565CDF65BF51EBABC08E0394ACC51E3926DF4C44196AC5DB5A2E306FAA0E92708CACEB70442F2AD379BEDFFD32285E0438A2DCB2EC3167E5B71639460E568F43BFE199469F7B35CB2AC72932DC4] ---rsa.Verify---> verify_result[true]
```

### SM2

SM2是[国家密码管理局](https://baike.baidu.com/item/国家密码管理局/2712999?fromModule=lemma_inlink)于2010年12月17日发布的椭圆曲线公钥密码算法。

SM2算法和[RSA算法](https://baike.baidu.com/item/RSA算法/263310?fromModule=lemma_inlink)都是公钥密码算法，SM2算法是一种更先进安全的算法，在我们国家[商用密码](https://baike.baidu.com/item/商用密码/10636778?fromModule=lemma_inlink)体系中被用来替换[RSA算法](https://baike.baidu.com/item/RSA算法/263310?fromModule=lemma_inlink)。

SM2密钥对象：`sm2key`。

| 成员类型 | 成员名字和原型                        | 中文名            | 说明                                                 | (错误.)(异常码,)异常消息 |
| :------: | ------------------------------------- | ----------------- | ---------------------------------------------------- | ------------------------ |
|   函数   | bool GenerateKeyPair();               | 生成密钥对        | 生成一对SM2公私钥，保存在对象内；如果报错返回false   | （同RSA）                |
|   函数   | bool ImportPublicKeyFromPEM(string);  | 从PEM文件导入公钥 | 从PEM编码格式的公钥文件中导入公钥；如果报错返回false | （同RSA）                |
|   函数   | bool ImportPrivateKeyFromPEM(string); | 从PEM文件导入私钥 | 从PEM编码格式的私钥文件中导入私钥；如果报错返回false | （同RSA）                |
|   函数   | bool ImportPublicKeyFromDER(string);  | 从DER文件导入公钥 | 从DER编码格式的公钥文件中导入公钥；如果报错返回false | （同RSA）                |
|   函数   | bool ImportPrivateKeyFromDER(string); | 从DER文件导入私钥 | 从DER编码格式的私钥文件中导入私钥；如果报错返回false | （同RSA）                |
|   函数   | bool ExportPublicKeyToPEM(string);    | 导出公钥到PEM文件 | 导出公钥到PEM编码格式的公钥文件中；如果报错返回false | （同RSA）                |
|   函数   | bool ExportPrivateKeyToPEM(string);   | 导出私钥到PEM文件 | 导出私钥到PEM编码格式的私钥文件中；如果报错返回false | （同RSA）                |
|   函数   | bool ExportPublicKeyToDER(string);    | 导出公钥到DER文件 | 导出公钥到DER编码格式的公钥文件中；如果报错返回false | （同RSA）                |
|   函数   | bool ExportPrivateKeyToDER(string);   | 导出私钥到DER文件 | 导出私钥到DER编码格式的私钥文件中；如果报错返回false | （同RSA）                |

代码实例：`test_crypto_sm2key_1.z`

```
import stdtypes stdio encoding crypto;

function int main( array args )
{
        sm2key  key_pair ;
        sm2key  pubkey ;
        sm2key  prikey ;

        key_pair.GenerateKeyPair() ;
        key_pair.ExportPublicKeyToPEM( "/tmp/sm2.pubkey.pem" ) ;
        key_pair.ExportPrivateKeyToPEM( "/tmp/sm2.prikey.pem" ) ;
        key_pair.ExportPublicKeyToDER( "/tmp/sm2.pubkey.der" ) ;
        key_pair.ExportPrivateKeyToDER( "/tmp/sm2.prikey.der" ) ;

        b = pubkey.ImportPublicKeyFromPEM( "/tmp/sm2.pubkey.pem" ) ;
        if( b != true )
        {
                stdout.Println( "ImportPublicKeyFromPEM failed" );
                return 1;
        }

        b = prikey.ImportPrivateKeyFromPEM( "/tmp/sm2.prikey.pem" ) ;
        if( b != true )
        {
                stdout.Println( "ImportPrivateKeyFromPEM failed" );
                return 1;
        }

        b = pubkey.ImportPublicKeyFromDER( "/tmp/sm2.pubkey.der" ) ;
        if( b != true )
        {
                stdout.Println( "ImportPublicKeyFromDER failed" );
                return 1;
        }

        b = prikey.ImportPrivateKeyFromDER( "/tmp/sm2.prikey.der" ) ;
        if( b != true )
        {
                stdout.Println( "ImportPrivateKeyFromDER failed" );
                return 1;
        }

        stdout.Println( "ok" );

        return 0;
}
```

执行

```
$ zlang test_crypto_sm2key_1.z
ok
```

SM2对象：`sm2`。

| 成员类型 | 成员名字和原型                               | 中文名 | 说明                                                         | (错误.)(异常码,)异常消息 |
| :------: | -------------------------------------------- | ------ | ------------------------------------------------------------ | ------------------------ |
|   函数   | string Sign(string data,sm2key,string data); | 签名   | 用私钥签名数据（用摘要算法SM3计算数据data后得到较短的摘要值，然后用私钥加密）；如果报错返回null。 | （同RSA）                |
|   函数   | bool Verify(string data,string sign,rsakey); | 验签   | 用公钥验签数据（用摘要算法SM3计算数据data后得到较短的摘要值，然后用公钥解密签名数据，两者做对比），如果验签正确返回true，否则返回false；如果报错返回null | （同RSA）                |

代码示例：`test_crypto_sm2_sign_1.z`

```
import stdtypes stdio encoding crypto;

function int sm2_Sign( string data , sm2key prikey , sm2key pubkey )
{
        stdout.FormatPrintln( "data length[%d]" , data.Length() );

        data_sign = sm2.Sign( data , prikey ) ;
        if( data_sign == null )
        {
                stdout.Println( "sm2.Sign failed" );
                return -1;
        }
        data_sign_exp = hexstr.HexExpand( data_sign ) ;

        verify_result = sm2.Verify( data , data_sign , pubkey ) ;
        if( verify_result != true )
        {
                stdout.Println( "sm2.Verify failed" );
                return -2;
        }

        stdout.FormatPrintln( "data:TEXT[%{data}] ---sm2.Sign---> data_sign ---sm2.Verify---> verify_result[%{verify_result}]" );

        return 0;
}

function int main( array args )
{
        sm2key  pubkey ;
        sm2key  prikey ;

        b = pubkey.ImportPublicKeyFromPEM( "sm2.pubkey.pem" ) ;
        if( b != true )
        {
                stdout.Println( "ImportPublicKeyFromPEM failed" );
                return 1;
        }

        b = prikey.ImportPrivateKeyFromDER( "sm2.prikey.der" ) ;
        if( b != true )
        {
                stdout.Println( "ImportPrivateKeyFromDER failed" );
                return 1;
        }

        data = "" ;
        nret = sm2_Sign( data , prikey , pubkey ) ;
        if( nret )
                return -nret;

        data = "12345678" ;
        nret = sm2_Sign( data , prikey , pubkey ) ;
        if( nret )
                return -nret;

        data = "12345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901
234567890123456789012345678901234567890" ;
        nret = sm2_Sign( data , prikey , pubkey ) ;
        if( nret )
                return -nret;

        return 0;
}
```

执行

```
$ zlang test_crypto_sm2_sign_1.z
data length[0]
data:TEXT[] ---sm2.Sign---> data_sign ---sm2.Verify---> verify_result[true]
data length[8]
data:TEXT[12345678] ---sm2.Sign---> data_sign ---sm2.Verify---> verify_result[true]
data length[260]
data:TEXT[12345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890] ---sm2.Sign---> data_sign ---sm2.Verify---> verify_result[true]
```



下一章：[跟踪栈和堆内存](trace.md)