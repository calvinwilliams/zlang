上一章：[异常处理](exception.md)



**章节目录**
- [zlang命令行参数](#zlang命令行参数)
  - [执行zlang源码文件](#执行zlang源码文件)
  - [快速生一个最小的zlang源码文件](#快速生一个最小的zlang源码文件)
  - [克隆并自动编译和安装一个第三方`zlang`对象库](#克隆并自动编译和安装一个第三方zlang对象库)



# zlang命令行参数

不带任何参数执行`zlang`，显示主程序版本号和参数说明

```
$ zlang
zlang v0.3.1.0 build May 10 2024 21:31:39

USAGE : zlang --version
        zlang [--debug-level|--info-level|--warn-level|--error-level|--fatal-level|--nolog-level] z_source_file
        zlang --scaffold-main new_z_source_file
```

仅显示主程序版本号和构建日期

```
$ zlang --version
zlang v0.3.1.0 build May 10 2024 21:30:14
```

## 执行zlang源码文件

`zlang`后面直接跟`zlang`源代码文件名就是执行她，中间可以加调试输出等级，缺省ERROR。

```
$ zlang hello.z
hello world
```

## 快速生一个最小的zlang源码文件

`--scaffold-main`用于快速生成一个`hello world`源代码文件

```
$ zlang --scaffold-main a.z
$ cat a.z
import stdtypes stdio ;

func int main( array args )
{
        stdout.Println( "hello world");

        return 0;
}
```

## 克隆并自动编译和安装一个第三方`zlang`对象库

`--git-install`用于克隆并自动编译和安装一个目录规范的包含第三方`zlang`对象库源码的git代码库，子参数`--git-branch`用于显示指定分支名，缺省让git指定，子参数`--makefile`用于指定makefile文件，缺省让make指定。完整命令行参数列表如下：

```
zlang --git-branch (branch_name) --makefile (makefile) --git-install (url)
```

下列示例克隆本教程章节[用C编写对象](c_object.md)中的对象库并自动编译和安装：

```
$ cd ~/src
$ zlang --makefile makefile.Linux --git-install https://gitee.com/calvinwilliams/zlang_c_object
```



下一章：[开始介绍自带对象库](zobjects.md)