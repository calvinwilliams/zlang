上一章：[zlang教程目录](index.md)



# 第一部分：zlang语言

# zlang简介

## zlang是什么

`zlang`是一门C/Java语系的、追求干净但又高阶的、基于对象克隆的解释型编程语言。

```
import stdtypes stdio ;

function int main( array args )
{
        stdout.Println( "hello world" );

        return 0;
}
```

创造`zlang`的一部分初衷是觉得手头的语言总存在这样那样的不方便之处，`C`在处理字符串时不够高阶简单，`Java`的`Getter`/`Setter`太八股，这些通过扩展很难彻底解决，但如果拥有一门自己的语言，就能打造成自己想要的样子，岂不爽哉。

```
import stdtypes stdio ;

function int main( array args )
{
        string  fruit = "pear" ;
        string  guess ;
        stdin.Scan( guess );
        if( guess == fruit )
                stdout.Println( "Yes" );
        else
                stdout.Println( "No" );

        return 0;
}
```

首先是如何设计`zlang`的语法，程序员在学习一门新的编程语言时最烦的就是新语言的语法完全特立独行、奇形怪状、满眼都是符号，自己得重新培养新的语感，其实`C/Java`的语法已经设计的相当完美了，新语言根本没必要完全设计一套新的语法，新语言的重点应该放在如何解决老语言的问题上，以及基于既有语法做扩展，`zlang`语法继承自`C/Java`，能让最广大同学们快速学习掌握，把有限的时间精力放在差异化部分。即非必要不要增加新的语法和符号，语法保持简洁、干净，同时保留多样性的表达能力。

```
import stdtypes stdio ;

function int factorial( int n )
{
        if( n == 1 )
                return 1;
        else
                return factorial(n-1) * n ;
}

function int main( array args )
{
        n = factorial( 6 ) ;
        stdout.Println( n );

        return 0;
}
```

`UNIX/Linux`的伟大之一是所有进程都是从根进程克隆出来，这和`Windows`笨重的从头创建一个新进程形成鲜明的对比，克隆体现出一种简单而又不失强大的UNIX哲学，`zlang`也尝试通过克隆根对象来创造整个应用程序来实践一门语言哲学，希望这是正确的。

```
import stdtypes stdio ;

function int main( array args )
{
        string  sheep1 = "Dolly" ;
        sheep1  sheep2 ;
        stdout.Println( sheep2 );

        return 0;
}
```

性能方面，`zlang`虽然是解释性语言，但还未做大规模的优化就拥有不俗的性能了，在我的老爷机上短连接、串行请求HTTP服务端WEB动态页面（模板渲染两个区域、一组明细、明细合计），TPS就能跑到5000多，RT在0.18ms左右。

```
<html>
<head>
<title>((HEADER_TITLE))</title>
</head>
<body>
$${UNLOGIN/
<p>Please login first</p>
$$/UNLOGIN}
$${LOGIN/
<p>USER : $$(USER)</p>
<table border="1">
        <tr>
                <td>ID</td>
                <td>NAME</td>
                <td>AMOUNT</td>
        </tr>
        $$[DETAIL/
        <tr>
                <td>$$(ID)</td>
                <td>$$(NAME)</td>
                <td>$$(AMOUNT:%.1lf)</td>
        </tr>
        $$/DETAIL]
</table>
<p>      TOTAL  : $$(TOTAL)</p>
<p>TOTAL_AMOUNT : $$(TOTAL_AMOUNT)</p>
<p>    DATETIME : $$(DATETIME)</p>
$$/LOGIN}
</body>
</html>
```

```
$ ab -c 1 -n 10000 'http://192.168.11.79:9527/test_httpserver_htmlsection_1_1'
This is ApacheBench, Version 2.3 <$Revision: 1430300 $>
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/
Licensed to The Apache Software Foundation, http://www.apache.org/

Benchmarking 192.168.11.79 (be patient)
Completed 1000 requests
Completed 2000 requests
Completed 3000 requests
Completed 4000 requests
Completed 5000 requests
Completed 6000 requests
Completed 7000 requests
Completed 8000 requests
Completed 9000 requests
Completed 10000 requests
Finished 10000 requests


Server Software:
Server Hostname:        192.168.11.79
Server Port:            9527

Document Path:          /test_httpserver_htmlsection_1_1
Document Length:        151 bytes

Concurrency Level:      1
Time taken for tests:   1.832 seconds
Complete requests:      10000
Failed requests:        0
Write errors:           0
Total transferred:      1910000 bytes
HTML transferred:       1510000 bytes
Requests per second:    5457.81 [#/sec] (mean)
Time per request:       0.183 [ms] (mean)
Time per request:       0.183 [ms] (mean, across all concurrent requests)
Transfer rate:          1018.01 [Kbytes/sec] received

Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:        0    0   0.0      0       3
Processing:     0    0   0.1      0      12
Waiting:        0    0   0.0      0       2
Total:          0    0   0.2      0      12

Percentage of the requests served within a certain time (ms)
  50%      0
  66%      0
  75%      0
  80%      0
  90%      0
  95%      0
  98%      0
  99%      0
 100%     12 (longest request)
```

以下是一个真实生产项目的压测数据：

### 静态页面

​     页面大小：10KB

​     并发数5、总请求数20万：

​          CPU：20％

​          内存：充裕

​          单个页面耗时：0.04至0.2毫秒

​          每秒请求页面数：24274次

​     并发数10、总请求数20万：

​          CPU：30％

​          内存：充裕

​          单个页面耗时：0.04至0.38毫秒

​          每秒请求页面数：25851次

### 动态页面

​    页面后端逻辑：内含5条结果集查询SQL、23条单记录查询SQL、1条插入SQL

​	页面模板大小：6KB

​     并发数5、总请求数1万：

​          CPU：70％

​          内存：充裕

​          单个页面耗时：4至21毫秒

​          每秒请求页面数：232次

​     并发数10、总请求数1万：

​          CPU：90％

​          内存：充裕

​          单个页面耗时：5至29毫秒

​          每秒请求页面数：344次

## `zlang`的设计原则

1. 语言引擎运行时，与基本对象库、第三方对象充分解耦；
2. 一切非上层业务逻辑的对象都用C封装，换取优越的性能；
3. 成年人不做选择题，成年人什么都想要。不要纠结面向过程和面向对象哪个好，都提供，满足不同人群的不同偏好；
4. 想到了继续写...

## `zlang`特性

1. 同时支持面向过程和面向对象，语言设计者根本不用考虑面向过程和面向对象哪个好，都支持好了，看用户怎么喜欢怎么用。

   ```
   import stdtypes stdio ;
   
   interface animal
   {
           string  name ;
   
           function void SetWarriorName( string name )
           {
                   this.name = name + " warrior" ;
           }
   
           function void Cry();
           function void Eat( string food );
   }
   
   object cat implements animal
   {
           function void Cry()
           {
                   stdout.Println( "miaomiaomiao~" );
           }
   
           function void Eat( string food )
           {
                   if( food == "fish" )
                           stdout.Println( ":)" );
                   else
                           stdout.Println( ":(" );
           }
   }
   
   object dog implements animal
   {
           function void Cry()
           {
                   stdout.Println( "wangwangwang~" );
           }
   
           function void Eat( string food )
           {
                   if( food == "bone" )
                           stdout.Println( ":>" );
                   else
                           stdout.Println( ":<" );
           }
   }
   
   function int main( array args )
   {
           cat     my_cat ;
   
           my_cat.SetWarriorName( "ahua" );
           stdout.Println( my_cat.name );
           my_cat.Cry();
           my_cat.Eat( "fish" );
           my_cat.Eat( "bone" );
   
           dog     my_dog ;
   
           my_dog.SetWarriorName( "black" );
           stdout.Println( my_dog.name );
           my_dog.Cry();
           my_dog.Eat( "bone" );
           my_dog.Eat( "fish" );
   
           return 0;
   }
   ```

2. 自动推导类型，声明变量时，明显可自动推导出来类型，就不用写出来。

   ```
   import stdtypes stdio ;
   
   function int main( array args )
   {
           str2 = "hello2" ;
           stdout.Println( str2 );
   
           return 0;
   }
   ```

3. 对象动态增删方法和属性，既然是解释语言，就要把运行时这个优势发挥好，语言只管提供这种能力，天知道会被神一般的用户玩出什么花来，比如我对实体类就很感冒。

   ```
   import stdtypes stdio ;
   
   function int main( array args )
   {
           zobject o ;
           string  str ;
   
           o.AddProperty( <object>string , "name" );
   
           o.name = "cal" ;
           stdout.Println( o.name );
   
           str = o.ReferProperty( "name" ) ;
           str =* "vin" ;
           stdout.Println( o.name );
   
           o.RemoveProperty( "name" );
   
           return 0;
   }
   ```

4. 提供完整、必要的集合对象：数组、链表、映射、队列、栈，数据容器管理基本无忧。

   ```
   import stdtypes stdio ;
   
   function int main( array args )
   {
           list<string>    my_list ;
   
           my_list.AddTail( "111" );
           my_list.AddTail( ”222“ );
           my_list.AddTail( "333" );
   
           stdout.Println( my_list.Length() );
   
           foreach( string s in my_list )
           {
                   stdout.Println( s );
           }
   
           return 0;
   }
   ```

5. 人必云`Getter`、`Setter`能做到封装性、提供扩展能力，我觉得封装性不一定非要用方法，对象中的一些属性为什么就不能作为接口暴露出来，不要人云亦云为了未来的拦截处理，拦截处理不一定非要用`Getter`、`Setter`，引入`Getter`、`Setter`带来的编写代码的繁琐性就没意见了，有没有考虑过其它解决方案呢。zlang`提供了属性拦截器解决扩展性问题，没必要为了将来极小概率的扩展性而要求每个属性都用繁琐的存取写法，大胆的直接赋值好了，直接的、简单的就是最好的，将来碰到需要扩展性时写一个属性拦截器就解决了，真没必要为了将来1%的扩展性导致99%都遵循繁琐的写法。比如以下表达，你更喜欢哪一种？

   赋值

   `Java`

   ```
   tortoise.setName( msg.getName() );
   ```

   `zlang`

   ```
   falcon.name = msg.name ;
   ```

   字符串比较

   `Java`

   ```
   if( name.equals(name2) )
   ```

   `zlang`

   ```
   if( name == name2 )
   ```

   自创的属性拦截器

   `zlang`

   ```
   import stdtypes stdio ;
   
   object greasy_man
   {
           public string   name ;
           public int      age ;
   
           intercept get name( object name )
           {
                   if( name == null || name == "" )
                           name = "" ;
                   else
                           name += "@Hangzhou" ;
                   return;
           }
   
           intercept set age( object age )
           {
                   if( age < 0 )
                           age = 0 ;
                   else if( age > 200 )
                           age = 200 ;
                   return;
           }
   }
   
   function int main( array args )
   {
           greasy_man      i ;
   
           i.name = "calvin" ;
           stdout.FormatPrintln( "i.name[%s]" , i.name );
   
           i.age = 296 ;
           stdout.FormatPrintln( "i.age[%d]" , i.age );
   
           return 0;
   }
   ```

6. `zlang`的迭代器支持前后移动。

7. 关键字、函数名、变量名都可以使用中文。还是那句话，我提供功能，喜欢的人就用，不喜欢的人可以不碰。

   ```
   charset "zh_CN.gb18030"
   
   导入 stdtypes stdio ;
   
   函数 整型 主函数( 数组 命令行参数集 )
   {
           字符串  招呼 = "你好，世界" ;
           字符串  A前中后B有字母C = "A前中后B有字母C" ;
           整型    数 = 3 ;
           整型    序号 , 总数 ;
   
           如果(招呼!="你好，世界")
           {
                   标准输出.显示并换行( "不对" );
           }
           否则
           {
                   标准输出.显示并换行( A前中后B有字母C );
           }
   
           分支判断(数)
           {
                   分支匹配 1 :
                           标准输出.显示并换行( "击中1" );
                   分支匹配 2 :
                           标准输出.显示并换行( "击中1" );
                   分支缺省:
                           标准输出.显示并换行( "击中缺省" );
           }
   
           总数 = 3 ;
           循环( 序号=0 ; 序号<总数 ; 序号++ )
           {
                   标准输出.格式化显示并换行( "序号[%d]" , 序号 );
           }
   
           循环每一个( 字符串 参数 在里面 命令行参数集 )
           {
                   标准输出.格式化显示并换行( "命令行参数[%s]" , 参数 );
           }
   
           返回 0;
   }
   ```

8. `zlang`加入了很多各种编程语言的精华特性，比如延迟执行等。

   ```
   import stdtypes stdio ;
   
   function int main( array args )
   {
           stdfile         f ;
   
           nret = f.Open( "test_defer_1.out" , "w" ) ;
           if( nret == -1 )
           {
                   stdout.Println( "open file failed[%d]" , nret );
                   return 1;
           }
           defer f.Close();
   
           f.FormatPrintln( "hello" );
   
           return 0;
   }
   ```

9. `zlang`的输出对象支持格式化串中不给定参数对应的类型而通过自动推导、支持直接在格式化串给定变量名而不用在后面苦哈哈的一个又一个加对应的参数。

   ```
   stdout.FormatPrintln( "s[%?] i[%?] l[%?] f[%?] d[%?]" , s , i , l , f , d );
   stdout.FormatPrintln( "s[%{s:2hd}] i[%{i:-4d}] l[%{l:ld}] f[%{f:4f}] d[%{d:10.2lf}]" );
   ```

10. （更多特性持续增加中...）

    

## `zlang`能力一览

* 承袭了几乎所有C、Java关键字、运算符、控制语法，还借鉴了其他语言的精华`atomic`、`defer`等；
* 既支持面向过程编程，也支持面向对象编程；没有`class`，所有皆是`object`；
* 增加属性拦截器；拒绝`Setter`、`Getter`八股文；
* 既支持返回值判断异常，也支持`throw`抛出捕获异常；
* 代码文件头上用`charset`声明字符编码后就能中文编程了；
* 数学计算函数：绝对值、幂、三角函数；大数对象库；
* 标准输入输出对象库：输入对象、输出对象、错误输出对象、执行外部命令对象；
* 系统对象库：沉睡指定秒/毫秒/微秒/纳秒、存取环境变量；
* 文件系统路径和文件对象库：路径对象、文件对象；
* 线程对象库：线程对象、临界互斥对象/关键字、读写锁对象、条件变量对象、内存屏障对象；
* 网络对象库：IP对象、TCP对象、HTTP对象、HTTPCLIENT对象、HTTPSERVER对象；
* 日期时间对象库：日期时间对象；
* 随机数对象库：随机数对象；
* 编码对象库：GUS_UUID对象、BASE64对象、MBCS字符编码转换对象；
* 日志对象库：日志对象、多日志对象；
* 数据库对象库：数据库对象、会话对象、结果集对象；
* 应用缓存对象库：Redis对象；
* JSON序列化/反序列化对象；
* XML序列化/反序列化对象；
* 加解密对象库：消息摘要（MD4对象、MD5对象、SHA1对象、SHA224对象、SHA256对象、SHA384对象、SHA512对象、SM3对象）、对称加解密（DES对象、3DES对象、AES对象、SM4对象）、非对称加解密和签名验签（RSA对象、SM2）；
* MSOffice文档处理对象库：Word文档对象、Excel表格对象；
* 跟踪栈和堆内存对象库；



## `zlang`依赖

`zlang`是我几乎完全自研的编程语言，连词法分析器都手工编写。以下是仅有的依赖：

   * 我的开源项目：Linux红黑树库的模板库`rbtree_tpl`（`zlang`源代码中已包含其源码）

   * 我的开源项目：追加模式数据页管理函数库`aodatapage`（`zlang`源代码中已包含其源码）

   * 我的开源项目：字节序函数库`byteorder`（`zlang`源代码中已包含其源码）

   * 我的开源项目：通用数据库访问层`cdbc`（`zlang`源代码中已包含其源码）

   * 我的开源项目：GUS_UUID编码库`libgus`（`zlang`源代码中已包含其源码）

   * 我的开源项目：JSON解析库`fasterjson`（`zlang`源代码中已包含其源码）

   * 我的开源项目：XML解析库`fasterxml`（`zlang`源代码中已包含其源码）

   * 我的开源项目：HTTP解析库（支持流式）`fasterhttp`（`zlang`源代码中已包含其源码）

   * 我的开源项目：通用模板库`fastertpl`（`zlang`源代码中已包含其源码）

   * 我的开源项目：WEB服务器`hetao`（`zlang`源代码中已包含其源码）

   * 我的开源项目：日志库`iLOG3`（`zlang`源代码中已包含其源码）

   * 我的开源项目：性能更好的IP地理位置库（编译链接基础对象库时外部依赖）

   * Linux内核：链表和红黑树函数库`list`、`rbtree`（`zlang`源代码中已包含其源码）

   * 安全加解密库：`openssl`（编译链接基础对象库时外部依赖）

   * Redis客户端库：`hiredis`（编译链接基础对象库时外部依赖）

   * 各个数据库客户端API库：`libmysqlclient.so`、`libpq.so`、`libsqlite3.so`（编译链接基础对象库时外部依赖）

   * 压缩工具库：`minizip`（`zlang`源代码中已包含其源码）

   * 大数运算库：`libgmp`（编译链接基础对象库时外部依赖）

     注意：编译时，如果未安装外部依赖会自动跳过相应基础对象的构建，构建出来的对象文件是`libzlang_*.so`，放在`$HOME/lib`下，`zlang`代码头部用`import ... ;`导入。

     注意：`openssl version`达到v1.1.1且非`FIPS`版本才能构建出`SM`算法对象，否则自动跳过。



下一章：[初次见面，hello world](hello_world.md)