上一章：[Redis](redis.md)



**章节目录**
- [JSON序列化/反序列化](#json序列化反序列化)



# JSON序列化/反序列化

JSON对象库用于JSON序列化/反序列化。

zlang的JSON对象库基于我的开源库`fasterjson`封装。

JSON对象名：`json`

| 成员类型 | 成员名字和原型                            | 说明                                                         | (错误.)(异常码,)异常消息                         |
| :------: | ----------------------------------------- | ------------------------------------------------------------ | ------------------------------------------------ |
|   常量   | int OBJECTTOSTRING_STYLE_COMPACT          | 对象转换成JSON字符串时，按紧缩样式（去除一切不影响的白字符） |                                                  |
|   常量   | int OBJECTTOSTRING_STYLE_INDENT_TAB       | 对象转换成JSON字符串时，按缩进样式（每个元素前加缩进，后加换行） |                                                  |
|   函数   | object StringToObject(string);            | JSON反序列化，即JSON字符串转换成对象，输入参数为JSON字符串，成功返回zobject对象，失败返回null | EXCEPTION_MESSAGE_STRING_TO_OBJECT_FAILED        |
|   函数   | bool StringToEntityObject(string,object); | JSON反序列化，即JSON字符串转换成预定义好的实体对象，输入参数为JSON字符串、实体对象，成功返回true，失败返回false | EXCEPTION_MESSAGE_STRING_TO_ENTITY_OBJECT_FAILED |
|   函数   | string ObjectToString(object);            | JSON序列化，即对象转换成JSON字符串，默认按紧缩样式           | EXCEPTION_MESSAGE_OBJECT_TO_STRING_FAILED        |
|   函数   | string ObjectToString(object,int);        | JSON序列化，即对象转换成JSON字符串，以指定的样式             | EXCEPTION_MESSAGE_OBJECT_TO_STRING_FAILED        |

代码示例：`test_json_2_1.z`

该代码示范了构造一个复杂对象，序列化成JSON字符串。

```
import stdtypes stdio json ;

object test11
{
        string  key11 ;
} ;

object test21
{
        string  key21 ;
} ;

object test1
{
        string  name1 ;
        string  name2 ;
        bool    mybool1 ;
        bool    mybool2 ;
        test11  t11 ;
        array   a21 ;
        string  null1 ;
} ;

function int main( array args )
{
        test1   t1 ;

        t1.name1 = "value1" ;
        t1.name2 = "value2" ;
        t1.mybool1 = true ;
        t1.mybool2 = false ;
        t1.t11.key11 = "value11" ;
        t1.a21.Append( <object>"ele21_1" );
        t1.a21.Append( <object>"ele21_2" );
        t1.a21.Append( <object>"ele21_3" );

        test21 new t21 ;
        t21.key21 = "value21" ;
        t1.a21.Append( <object>t21 );

        test21 new t21 ;
        t21.key21 = "value21_2" ;
        t1.a21.Append( <object>t21 );

        test21 new t21 ;
        t21.key21 = "value21_3" ;
        t1.a21.Append( <object>t21 );

        t1.null1 = null ;

        string  j = json.ObjectToString( <object> t1 ) ;
        stdout.FormatPrintln( "json[%s]" , j );

        return 0;
}
```

执行

```
$ zlang test_json_2_1.z
json[{"name1":"value1","name2":"value2","mybool1":true,"mybool2":false,"t11":{"key11":"value11"},"a21":["ele21_1","ele21_2","ele21_3",{"key21":"value21"},{"key21":"value21_2"},{"key21":"value21_3"}],"null1":null}]
```

代码示例：`test_json_1_1.z`

该代码示范了把JSON字符串反序列化成复杂对象，对象中的属性动态创建。

```
import stdtypes stdio json ;

function int main( array args )
{
        t = '''{"name1":"value1","name2":"value2","myobj":{"book1":"C","book2":"JAVA"},"myarray":["aaa","bbb","ccc",{"name11":"value11","name12":"value12"},{"name21":"value21","name22":"value22"}],"mybool1":false,"mybool2":true,"mynull":
null}''' ;

        zobject o = json.StringToObject( t ) ;
        if( o == null )
        {
                stdout.Println( "json.StringToObject failed" );
                return 1;
        }

        stdout.FormatPrintln( "name1:[%s]" , o.name1 );
        stdout.FormatPrintln( "name2:[%s]" , o.name2 );
        stdout.FormatPrintln( "myobj.book1:[%s]" , o.myobj.book1 );
        stdout.FormatPrintln( "myobj.book2:[%s]" , o.myobj.book2 );
        stdout.FormatPrintln( "myarray.Get(1):[%s]" , o.myarray.Get(1) );
        stdout.FormatPrintln( "myarray.Get(2):[%s]" , o.myarray.Get(2) );
        stdout.FormatPrintln( "myarray.Get(3):[%s]" , o.myarray.Get(3) );
        stdout.FormatPrintln( "myarray.Get(4).name11:[%s]" , o.myarray.Get(4).name11 );
        stdout.FormatPrintln( "myarray.Get(4).name12:[%s]" , o.myarray.Get(4).name12 );
        stdout.FormatPrintln( "myarray.Get(5).name21:[%s]" , o.myarray.Get(5).name21 );
        stdout.FormatPrintln( "myarray.Get(5).name22:[%s]" , o.myarray.Get(5).name22 );
        stdout.FormatPrintln( "mybool1:[%b]" , o.mybool1 );
        stdout.FormatPrintln( "mybool2:[%b]" , o.mybool2 );
        stdout.FormatPrintln( "mynull:[%s]" , o.mynull );
        if( o.ReferProperty("mynull2") != null )
                stdout.FormatPrintln( "mynull2:[%s]" , o.mynull2 );
        else
                stdout.Println( "no mynull2" );
        if( o.ReferProperty("mynull3") )
                stdout.FormatPrintln( "mynull3:[%s]" , o.mynull3 );
        else
                stdout.Println( "no mynull3" );

        return 0;
}
```

执行

```
$ zlang test_json_1_1.z
name1:[value1]
name2:[value2]
myobj.book1:[C]
myobj.book2:[JAVA]
myarray.Get(1):[aaa]
myarray.Get(2):[bbb]
myarray.Get(3):[ccc]
myarray.Get(4).name11:[value11]
myarray.Get(4).name12:[value12]
myarray.Get(5).name21:[value21]
myarray.Get(5).name22:[value22]
mybool1:[false]
mybool2:[true]
mynull:[(null)]
no mynull2
no mynull3
```

代码示例：`test_json_3_1.z`

该代码示范了把JSON字符串反序列化成复杂对象，对象为实体对象即预定义好属性的对象。

```
import stdtypes stdio json ;

object test1
{
        array   a2 ;
        list    l2 ;
        map     m2 ;
} ;

function int main( array args )
{
        test1   t1 ;

        t1.a2.Append( "S211" );
        t1.a2.Append( "S212" );
        t1.a2.Append( "S213" );
        t1.l2.AddTail( 221 );
        t1.l2.AddTail( 222 );
        t1.l2.AddTail( 223 );
        t1.m2.Put( "S231" , <object>231 );
        t1.m2.Put( "S232" , <object>232 );
        t1.m2.Put( "S233" , <object>233 );

        string  j = json.ObjectToString( <object> t1 ) ;
        stdout.FormatPrintln( "json[%s]" , j );

        test1 new t1 ;
        b = json.StringToEntityObject( j , <object>t1 ) ;
        if( b != true )
        {
                stdout.Println( "json.StringToEntityObject failed" );
                return 1;
        }

        foreach( string s in t1.a2 )
        {
                stdout.Println( s );
        }

        foreach( int n in t1.l2 )
        {
                stdout.Println( n );
        }

        foreach( string k in t1.m2 )
        {
                v = t1.m2.Get( k ) ;
                stdout.FormatPrintln( "key[%s] value[%d]" , k , v );
        }

        return 0;
}
```

执行

```
$ zlang test_json_3_1.z
json[{"a2":["S211","S212","S213"],"l2":[221,222,223],"m2":["S231":231,"S232":232,"S233":233]}]
S211
S212
S213
221
222
223
key[S231] value[231]
key[S232] value[232]
key[S233] value[233]
```

代码示例：`test_json_4_1.z`

该代码示范了构建一个复杂对象，对象内含数组、链表、映射，把对象序列化成JSON字符串。

```
import stdtypes stdio json ;

object test2
{
        int     id ;
        string  str ;
        double  amount ;
} ;

object test1
{
        array <test2>           a2 ;
        list <test2>            l2 ;
        map <string,test2>      m2 ;
} ;

function int main( array args )
{
        test1   t1 ;
        test2   _test2 ;

        _test2 new test2 ;
        test2.id = 101 ;
        test2.str = "S101" ;
        test2.amount = 1.01 ;
        t1.a2.Append( <object> test2 );

        _test2 new test2 ;
        test2.id = 102 ;
        test2.str = "S102" ;
        test2.amount = 1.02 ;
        t1.a2.Append( <object> test2 );

        _test2 new test2 ;
        test2.id = 103 ;
        test2.str = "S103" ;
        test2.amount = 1.03 ;
        t1.a2.Append( <object> test2 );

        _test2 new test2 ;
        test2.id = 201 ;
        test2.str = "S201" ;
        test2.amount = 2.01 ;
        t1.l2.AddTail( <object> test2 );

        _test2 new test2 ;
        test2.id = 202 ;
        test2.str = "S202" ;
        test2.amount = 2.02 ;
        t1.l2.AddTail( <object> test2 );

        _test2 new test2 ;
        test2.id = 203 ;
        test2.str = "S203" ;
        test2.amount = 2.03 ;
        t1.l2.AddTail( <object> test2 );
        
        _test2 new test2 ;
        test2.id = 301 ;
        test2.str = "S301" ;
        test2.amount = 3.01 ;
        t1.m2.Put( "K1" , <object> test2 );

        _test2 new test2 ;
        test2.id = 302 ;
        test2.str = "S302" ;
        test2.amount = 3.02 ;
        t1.m2.Put( "K2" , <object> test2 );

        _test2 new test2 ;
        test2.id = 303 ;
        test2.str = "S303" ;
        test2.amount = 3.03 ;
        t1.m2.Put( "K3" , <object> test2 );

        string  j = json.ObjectToString( <object> t1 ) ;
        stdout.FormatPrintln( "json[%s]" , j );

        test1 new t1 ;
        b = json.StringToEntityObject( j , <object>t1 ) ;
        if( b != true )
        {
                stdout.Println( "json.StringToEntityObject failed" );
                return 1;
        }

        foreach( test2 v in t1.a2 )
        {
                stdout.FormatPrintln( "array : id[%d] str[%s] amount[%lf]" , v.id , v.str , v.amount );
        }

        foreach( test2 v in t1.l2 )
        {
                stdout.FormatPrintln( "list : id[%d] str[%s] amount[%lf]" , v.id , v.str , v.amount );
        }

        foreach( string k in t1.m2 )
        {
                test2 v = t1.m2.Get( k ) ;
                stdout.FormatPrintln( "map[%s] : id[%d] str[%s] amount[%lf]" , k , v.id , v.str , v.amount );
        }

        return 0;
}
```

执行

```
zlang test_json_4_1.z
json[{"a2":[{"id":101,"str":"S101","amount":1.010000},{"id":102,"str":"S102","amount":1.020000},{"id":103,"str":"S103","amount":1.030000}],"l2":[{"id":201,"str":"S201","amount":2.010000},{"id":202,"str":"S202","amount":2.020000},{"id":203,"str":"S203","amount":2.030000}],"m2":["K1":{"id":301,"str":"S301","amount":3.010000},"K2":{"id":302,"str":"S302","amount":3.020000},"K3":{"id":303,"str":"S303","amount":3.030000}]}]
array : id[101] str[S101] amount[1.010000]
array : id[102] str[S102] amount[1.020000]
array : id[103] str[S103] amount[1.030000]
list : id[201] str[S201] amount[2.010000]
list : id[202] str[S202] amount[2.020000]
list : id[203] str[S203] amount[2.030000]
map[K1] : id[301] str[S301] amount[3.010000]
map[K2] : id[302] str[S302] amount[3.020000]
map[K3] : id[303] str[S303] amount[3.030000]
```



下一章：[XML序列化/反序列化](xml.md)

