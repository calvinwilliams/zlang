上一章：[JSON序列化/反序列化](json.md)



**章节目录**
- [XML序列化/反序列化](#xml序列化反序列化)



# XML序列化/反序列化

XML对象库用于XML序列化/反序列化。

zlang的XML对象库基于我的开源库`fasterxml`封装。

XML对象名：`xml`

| 成员类型 | 成员名字和原型                            | 说明                                                         | (错误.)(异常码,)异常消息                         |
| :------: | ----------------------------------------- | ------------------------------------------------------------ | ------------------------------------------------ |
|   常量   | int STRINGTOOBJECT_OPTION_LESSSTRICT      | XML字符串转换成对象时，如果碰到不规范之处，尽量忽略和解析完  |                                                  |
|   常量   | int OBJECTTOSTRING_STYLE_COMPACT          | 对象转换成XML字符串时，按紧缩样式（去除一切不影响的白字符）  |                                                  |
|   常量   | int OBJECTTOSTRING_STYLE_INDENT_TAB       | 对象转换成XML字符串时，按缩进样式（每个元素前加缩进，后加换行） |                                                  |
|   函数   | object StringToObject(string);            | XML反序列化，即XML字符串转换成对象，输入参数为XML字符串，成功返回zobject对象，失败返回null |                                                  |
|   函数   | bool StringToEntityObject(string,object); | XML反序列化，即XML字符串转换成预定义好的实体对象，输入参数为XML字符串、实体对象，成功返回true，失败返回false | EXCEPTION_MESSAGE_STRING_TO_ENTITY_OBJECT_FAILED |
|   函数   | void SignSpecialHtmlTags();               | 声明可能不规范的HTML标记，应对不规范的HTML解析               |                                                  |
|   函数   | string Xpath(string,string);              | 从XML字符串按XPATH取出指定节点的下面区域XML字符串，输入参数为XML字符串、XPATH | EXCEPTION_MESSAGE_FIND_XPATH_FAILED              |
|   函数   | string Xnode(string,string);              | 从XML字符串按XPATH取出指定节点的XML字符串，输入参数为XML字符串、XPATH | EXCEPTION_MESSAGE_FIND_XNODE_FAILED              |
|   函数   | list FindAll(string,string,map);          | 从XML字符串搜索所有，输入参数为XML字符串、XML标记，属性条件集合，成功返回节点XML字符串列表 | EXCEPTION_MESSAGE_TRAVEL_XML_FAILED              |
|   函数   | string TrimTagsAndBlanks(string);         | 去除XML字符串中的所有XML标记，重复的白字符替换成一个，但保留换行 | EXCEPTION_MESSAGE_UNCOMPLETED_STRING             |
|   函数   | string ObjectToString(object);            | XML序列化，即对象转换成XML字符串，默认按紧缩样式             |                                                  |
|   函数   | string ObjectToString(object,int);        | XML序列化，即对象转换成XML字符串，以指定的样式               | EXCEPTION_MESSAGE_OBJECT_TO_STRING               |

注意：方法`Xpath`、`Xnode`、`FindAll`通常用于爬虫细节爬下来的HTML。`SignSpecialHtmlTags`方法用于声明HTML可能不规范，不用大惊小怪。`TrimTagsAndBlanks`方法用于删除其中的HTML标记。

代码示例：`test_xml_2_1.z`

该代码示范了构建复杂对象，序列化成XML字符串。

```
import stdtypes stdio xml ;

object test11
{
        string  key11 ;
} ;

object test1
{
        string  name1 ;
        string  name2 ;
        bool    mybool1 ;
        bool    mybool2 ;
        test11  t11 ;
        array   a21 ;
        string  null1 ;
} ;

object test21
{
        string  key21 ;
} ;

function int main( array args )
{
        test1   t1 ;

        t1.name1 = "value1" ;
        t1.name2 = "value2" ;
        t1.mybool1 = true ;
        t1.mybool2 = false ;
        t1.t11.key11 = "value11" ;
        t1.a21.Append( <object>"ele21_1" );
        t1.a21.Append( <object>"ele21_2" );
        t1.a21.Append( <object>"ele21_3" );

        test21 new t21 ;
        t21.key21 = "value21" ;
        t1.a21.Append( <object>t21 );

        t1.null1 = null ;

        string  x = xml.ObjectToString( <object> t1 ) ;
        stdout.FormatPrintln( "xml[%s]" , x );

        x = xml.ObjectToString( <object> t1 , 1 ) ;
        stdout.FormatPrintln( "xml[%s]" , x );

        return 0;
}
```

执行

```
zlang test_xml_2_1.z
xml[<t1><name1>value1</name1><name2>value2</name2><mybool1>true</mybool1><mybool2>false</mybool2><t11><key11>value11</key11></t11><a21>ele21_1</a21><a21>ele21_2</a21><a21>ele21_3</a21><a21><t21><key21>value21</key21></t21></a21><null1 /></t1>]
xml[<t1>
        <name1>value1</name1>
        <name2>value2</name2>
        <mybool1>true</mybool1>
        <mybool2>false</mybool2>
        <t11>
                <key11>value11</key11>
        </t11>
        <a21>ele21_1</a21>
        <a21>ele21_2</a21>
        <a21>ele21_3</a21>
        <a21>
                <t21>
                        <key21>value21</key21>
                </t21>
        </a21>
        <null1 />
</t1>
]

```

代码示例：`test_xml_1_1.z`

该代码示范了把XML字符串反序列化成一个复杂对象，对象中的属性动态创建。

```
import stdtypes stdio xml ;

function int main( array args )
{
        t = '''<o>
                <name1>value1</name1>
                <name2>value2</name2>
                <myobj>
                        <book1>C</book1>
                        <book2>JAVA</book2>
                </myobj>
                <mybool1>false</mybool1>
                <mybool2>true</mybool2>
                <mynull />
        </o>''' ;

        zobject o = xml.StringToObject( t ) ;
        if( o == null )
        {
                stdout.Println( "xml.StringToObject failed" );
                return 1;
        }

        stdout.FormatPrintln( "name1:[%s]" , o.name1 );
        stdout.FormatPrintln( "name2:[%s]" , o.name2 );
        stdout.FormatPrintln( "myobj.book1:[%s]" , o.myobj.book1 );
        stdout.FormatPrintln( "myobj.book2:[%s]" , o.myobj.book2 );
        stdout.FormatPrintln( "mybool1:[%b]" , o.mybool1 );
        stdout.FormatPrintln( "mybool2:[%b]" , o.mybool2 );
        stdout.FormatPrintln( "mynull:[%s]" , o.mynull );
        if( o.ReferProperty("mynull2") != null )
                stdout.FormatPrintln( "mynull2:[%s]" , o.mynull2 );
        else
                stdout.Println( "no mynull2" );
        if( o.ReferProperty("mynull3") )
                stdout.FormatPrintln( "mynull3:[%s]" , o.mynull3 );
        else
                stdout.Println( "no mynull3" );

        return 0;
}
```

执行

```
$ zlang test_xml_1_1.z
name1:[value1]
name2:[value2]
myobj.book1:[C]
myobj.book2:[JAVA]
mybool1:[false]
mybool2:[true]
mynull:[(null)]
no mynull2
no mynull3
```

代码示例：`test_xml_8_1.z`

该代码示范了构建一个复杂对象，对象内含数组、链表、映射，把对象序列化成XML字符串。

```
import stdtypes stdio xml ;

object test3
{
        int     i ;
        string  str ;
        bool    b ;
} ;

object test1
{
        array   a2 ;
        list    l2 ;
        map     m2 ;
} ;

function int main( array args )
{
        test1   root1 ;
        test3   _test3 ;

        _test3  new test3 ;
        test3.i = 11 ;
        test3.str = "str11" ;
        test3.b = true ;
        root1.a2.Append( <object> test3 );

        _test3  new test3 ;
        test3.i = 12 ;
        test3.str = "str12" ;
        test3.b = false ;
        root1.a2.Append( <object> test3 );

        _test3  new test3 ;
        test3.i = 13 ;
        test3.str = "str13" ;
        test3.b = true ;
        root1.a2.Append( <object> test3 );

        _test3  new test3 ;
        test3.i = 21 ;
        test3.str = "str21" ;
        test3.b = true ;
        root1.l2.AddTail( <object> test3 );

        _test3  new test3 ;
        test3.i = 22 ;
        test3.str = "str22" ;
        test3.b = false ;
        root1.l2.AddTail( <object> test3 );

        _test3  new test3 ;
        test3.i = 23 ;
        test3.str = "str23" ;
        test3.b = true ;
        root1.l2.AddTail( <object> test3 );
        
        _test3  new test3 ;
        test3.i = 31 ;
        test3.str = "str31" ;
        test3.b = true ;
        root1.m2.Put( <object> test3.i , <object> test3 );

        _test3  new test3 ;
        test3.i = 32 ;
        test3.str = "str32" ;
        test3.b = false ;
        root1.m2.Put( <object> test3.i , <object> test3 );

        _test3  new test3 ;
        test3.i = 33 ;
        test3.str = "str33" ;
        test3.b = true ;
        root1.m2.Put( <object> test3.i , <object> test3 );

        string new s = xml.ObjectToString( <object> root1 , xml.OBJECTTOSTRING_STYLE_INDENT_TAB ) ;
        if( s == null )
        {
                stdout.FormatPrintln( "xml.ObjectToString failed" );
                return 1;
        }
        else
        {
                stdout.FormatPrintln( "xml.ObjectToString ok , [%s]" , s );
        }

        test1   new root1 ;

        b = xml.StringToEntityObject( s , <object> root1 ) ;
        if( b != true )
        {
                stdout.FormatPrintln( "xml.StringToEntityObject failed" );
                return 1;
        }
        else
        {
                stdout.FormatPrintln( "xml.StringToEntityObject ok" );
        }

        int count = root1.a2.Length() ;
        for( int i = 1 ; i <= count ; i++ )
        {
                _test3 test3 = root1.a2.Get(i) ;
                stdout.FormatPrintln( "--- ARRAY %d/%d ---" , i , count );
                stdout.FormatPrintln( "i[%d]" , test3.i );
                stdout.FormatPrintln( "str[%s]" , test3.str );
                stdout.FormatPrintln( "b[%b]" , test3.b );
        }
        
        int new count = root1.l2.Length() ;
        list_node = root1.l2.GetHead() ;
        for( int new i = 1 ; ; i++ )
        {
                if( list_node.IsTravelOver() )
                        break;

                stdout.FormatPrintln( "--- LIST %d/%d ---" , i , count );
                _test3 test3 = list_node.GetMember() ;
                stdout.FormatPrintln( "i[%d]" , test3.i );
                stdout.FormatPrintln( "str[%s]" , test3.str );
                stdout.FormatPrintln( "b[%b]" , test3.b );

                list_node.TravelNext();
        }

        int new count = root1.m2.Length() ;
        int key = null ;
        for( int new i = 1 ; ; i++ )
        {
                key = root1.m2.TravelNextKey( <object> key ) ;
                if( key == null )
                        break;
                _test3 new test3 = root1.m2.Get( <object> key ) ;

                stdout.FormatPrintln( "--- MAP %d/%d ---" , i , count );
                stdout.FormatPrintln( "i[%d]" , test3.i );
                stdout.FormatPrintln( "str[%s]" , test3.str );
                stdout.FormatPrintln( "b[%b]" , test3.b );
        }

        return 0;
}
```

执行

```
zlang test_xml_8_1.z
xml.ObjectToString ok , [<root1>
        <a2>
                <test3>
                        <i>11</i>
                        <str>str11</str>
                        <b>true</b>
                </test3>
        </a2>
        <a2>
                <test3>
                        <i>12</i>
                        <str>str12</str>
                        <b>false</b>
                </test3>
        </a2>
        <a2>
                <test3>
                        <i>13</i>
                        <str>str13</str>
                        <b>true</b>
                </test3>
        </a2>
        <l2>
                <test3>
                        <i>21</i>
                        <str>str21</str>
                        <b>true</b>
                </test3>
        </l2>
        <l2>
                <test3>
                        <i>22</i>
                        <str>str22</str>
                        <b>false</b>
                </test3>
        </l2>
        <l2>
                <test3>
                        <i>23</i>
                        <str>str23</str>
                        <b>true</b>
                </test3>
        </l2>
        <m2>
                <test3>
                        <i>31</i>
                        <str>str31</str>
                        <b>true</b>
                </test3>
        </m2>
        <m2>
                <test3>
                        <i>32</i>
                        <str>str32</str>
                        <b>false</b>
                </test3>
        </m2>
        <m2>
                <test3>
                        <i>33</i>
                        <str>str33</str>
                        <b>true</b>
                </test3>
        </m2>
</root1>
]
xml.StringToEntityObject ok
--- ARRAY 1/3 ---
i[11]
str[str11]
b[true]
--- ARRAY 2/3 ---
i[12]
str[str12]
b[false]
--- ARRAY 3/3 ---
i[13]
str[str13]
b[true]
--- LIST 1/3 ---
i[21]
str[str21]
b[true]
--- LIST 2/3 ---
i[22]
str[str22]
b[false]
--- LIST 3/3 ---
i[23]
str[str23]
b[true]
--- MAP 1/3 ---
i[31]
str[str31]
b[true]
--- MAP 2/3 ---
i[32]
str[str32]
b[false]
--- MAP 3/3 ---
i[33]
str[str33]
b[true]
```

代码示例：`test_xml_11_1.z`

该代码示范了通过`Xpath`方法取得XML中的指定路径下的子字符串。

```
import stdtypes stdio xml ;

function int main( array args )
{
        html = '''
                <html>
                        <head>
                                <title>This is title</title>
                                <link rel="stylesheet" href="https://media.readthedocs.org/css/sphinx_rtd_theme.css" type="text/css" />
                        </head>
                        <body>
                                <p class="title">
                                        <b>
                                                The Dormouse's story
                                        </b>
                                </p>
                                <p class="story">
                                        Once upon a time there were three little sisters; and their names were
                                        <a class="sister" href="http://example.com/elsie" id="link1">
                                                Elsie
                                        </a>
                                        ,
                                        <a class="sister" href="http://example.com/lacie" id="link2">
                                                Lacie
                                        </a>
                                        and
                                        <a class="sister" href="http://example.com/tillie" id="link2">
                                                Tillie
                                        </a>
                                        ; and they lived at the bottom of a well.
                                </p>
                                <p class="story3">
                                        Beautiful rice
                                </p>
                                <p class="story4">
                                        in zlang
                                </p>
                        </body>
                </html>
        ''' ;

        stdout.FormatPrintln( "/html/head/title : [%s]" , xml.Xpath( html , "/html/head/title" ) );
        stdout.FormatPrintln( "/html/body/p : [%s]" , xml.Xpath( html , "/html/body/p" ) );
        stdout.FormatPrintln( "/html/body/p.story3 : [%s]" , xml.Xpath( html , "/html/body/p.story3" ) );
        stdout.FormatPrintln( "/html/body/p.story4[1] : [%s]" , xml.Xpath( html , "/html/body/p.story4[1]" ) );
        s = xml.Xpath( html , "/html/body/p[2]" ) ;
        stdout.FormatPrintln( "/html/body/p[2] : [%s]" , s );
        s2 = xml.TrimTagsAndBlanks( s ) ;
        stdout.FormatPrintln( "/html/body/p[2] -> TrimTagsAndBlanks : [%s]" , s2 );

        return 0;
}
```

执行

```
$ zlang test_xml_11_1.z
/html/head/title : [This is title]
/html/body/p : [
                                        <b>
                                                The Dormouse's story
                                        </b>
                                ]
/html/body/p.story3 : [
                                        Beautiful rice
                                ]
/html/body/p.story4[1] : [
                                        in zlang
                                ]
/html/body/p[2] : [
                                        Once upon a time there were three little sisters; and their names were
                                        <a class="sister" href="http://example.com/elsie" id="link1">
                                                Elsie
                                        </a>
                                        ,
                                        <a class="sister" href="http://example.com/lacie" id="link2">
                                                Lacie
                                        </a>
                                        and
                                        <a class="sister" href="http://example.com/tillie" id="link2">
                                                Tillie
                                        </a>
                                        ; and they lived at the bottom of a well.
                                ]
/html/body/p[2] -> TrimTagsAndBlanks : [Once upon a time there were three little sisters; and their names were
Elsie
,
Lacie
and
Tillie
; and they lived at the bottom of a well.]
```

代码示例：`test_xml_12_2.z`

该代码示范了通过`Xnode`方法取得XML中的指定路径节点的子字符串。

```
import stdtypes stdio xml ;

function int main( array args )
{
        html = '''
                <html>
                        <head>
                                <title>This is title</title>
                                <link rel="stylesheet" href="https://media.readthedocs.org/css/sphinx_rtd_theme.css" type="text/css" />
                        </head>
                        <body>
                                <p class="title">
                                        <b>
                                                The Dormouse's story
                                        </b>
                                </p>
                                <p class="story">
                                        Once upon a time there were three little sisters; and their names were
                                        <a class="sister" href="http://example.com/elsie" id="link1">
                                                Elsie
                                        </a>
                                        ,
                                        <a class="sister" href="http://example.com/lacie" id="link2">
                                                Lacie
                                        </a>
                                        and
                                        <a class="sister" href="http://example.com/tillie" id="link2">
                                                Tillie
                                        </a>
                                        ; and they lived at the bottom of a well.
                                        <a class="brother" href="http://example.com/mark" id="link3">
                                                Mark
                                        </a>
                                </p>
                                <p class="story3">
                                        Beautiful rice
                                </p>
                                <p class="story4">
                                        in zlang
                                </p>
                        </body>
                </html>
        ''' ;

        s = xml.Xnode( html , "/html/body/p[2]" ) ;
        if( s == null )
        {
                stdout.Println( "xml.Xnode failed" );
                return 1;
        }
        else
        {
                stdout.Println( "xml.Xnode ok" );
        }

        map c ;
        c.Put( "class" , <object>"sister" );
        c.Put( "id" , <object>"link2" );
        l = xml.FindAll( s , "a" , c ) ;
        foreach( map m in l )
        {
                stdout.FormatPrintln( "a :" );
                foreach( string k in m )
                {
                        v = m.Get( k ) ;
                        stdout.FormatPrintln( "    k[%s] v[%s]" , k , xml.TrimTagsAndBlanks(v) );
                }
        }

        return 0;
}
```

执行

```
zlang test_xml_12_2.z
xml.Xnode ok
a :
    k[id] v[link2]
    k[href] v[http://example.com/lacie]
    k[class] v[sister]
    k[innerText] v[Lacie]
a :
    k[id] v[link2]
    k[href] v[http://example.com/tillie]
    k[class] v[sister]
    k[innerText] v[Tillie]
```

代码示例：`test_xml_12_1.z`

该代码示范了通过`FindAll`方法取得XML中的给定条件的所有节点的子字符串。

```
import stdtypes stdio xml ;

function int main( array args )
{
        html = '''
                <html>
                        <head>
                                <title>This is title</title>
                                <link rel="stylesheet" href="https://media.readthedocs.org/css/sphinx_rtd_theme.css" type="text/css" />
                        </head>
                        <body>
                                <p class="title">
                                        <b>
                                                The Dormouse's story
                                        </b>
                                </p>
                                <p class="story">
                                        Once upon a time there were three little sisters; and their names were
                                        <a class="sister" href="http://example.com/elsie" id="link1">
                                                Elsie
                                        </a>
                                        ,
                                        <a class="sister" href="http://example.com/lacie" id="link2">
                                                Lacie
                                        </a>
                                        and
                                        <a class="sister" href="http://example.com/tillie" id="link2">
                                                Tillie
                                        </a>
                                        ; and they lived at the bottom of a well.
                                </p>
                                <p class="story3">
                                        Beautiful rice
                                </p>
                                <p class="story4">
                                        in zlang
                                </p>
                        </body>
                </html>
        ''' ;

        l = xml.FindAll( html , "p" , <map>null ) ;
        foreach( map m in l )
        {
                stdout.FormatPrintln( "p :" );
                foreach( string k in m )
                {
                        v = m.Get( k ) ;
                        stdout.FormatPrintln( "    k[%s] v[%s]" , k , xml.TrimTagsAndBlanks(v) );
                }
        }

        return 0;
}
```

执行

```
zlang test_xml_12_1.z
p :
    k[class] v[title]
    k[innerText] v[The Dormouse's story]
p :
    k[class] v[story]
    k[innerText] v[Once upon a time there were three little sisters; and their names were
Elsie
,
Lacie
and
Tillie
; and they lived at the bottom of a well.]
p :
    k[class] v[story3]
    k[innerText] v[Beautiful rice]
p :
    k[class] v[story4]
    k[innerText] v[in zlang]
```



下一章：[加解密](crypto.md)