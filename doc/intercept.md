上一章：[面向对象](object.md)



**章节目录**
- [拦截器](#拦截器)
  - [全局函数拦截器](#全局函数拦截器)
  - [对象内函数拦截器](#对象内函数拦截器)
  - [对象内属性拦截器](#对象内属性拦截器)



# 拦截器

`zlang`允许对全局函数或对象内函数做前置/后置代码增强。

函数拦截器语句的形式为：

```
intercept before|after 拦截函数名( object o , functionp f , array in_params , array out_params )
{
	拦截函数代码;
	
	return;
}
```

注意：拦截函数名格式为`被拦截函数名`或`被拦截对象祖先名.被拦截函数名`，名字可以用`*`或`?`做通配。

## 全局函数拦截器

代码示例：`test_intercept_1_1.z`

该代码示范了拦截`func1`函数，前置拦截器捕获并输出显示了输入参数值和输出参数缺省值，后置拦截器篡改了输出参数值。

```
import stdtypes stdio ;

intercept before `func1(int)`( object o , functionp f , array in_params , array out_params )
{
        int     in1 = in_params.Get(1) ;
        int     out1 = out_params.Get(1) ;

        stdout.FormatPrintln( "before func1 : in1[%d] out1[%d]" , in1 , out1 );

        return;
}

intercept after `func1(int)`( object o , functionp f , array in_params , array out_params )
{
        int     in1 = in_params.Get(1) ;
        int     out1 = out_params.Get(1) ;

        stdout.FormatPrintln( "after func1 : in1[%d] out1[%d]" , in1 , out1 );

        out1 = out1 * out1 ;
        out_params.Set(1,<object>out1);
        stdout.FormatPrintln( "after func1 : out1->[%d]" , out1 );

        return;
}

function int func1( int in0 )
{
        int     out0 = in0 * in0 ;

        stdout.FormatPrintln( "func1 : in[%d] out[%d]" , in0 , out0 );

        return out0;
}

function int main( array args )
{
        int n = func1( 3 ) ;
        stdout.FormatPrintln( "main : func1 out[%d]" , n );

        return 0;
}
```

执行

```
$ zlang test_intercept_1_1.z
before func1 : in1[3] out1[0]
func1 : in[3] out[9]
after func1 : in1[3] out1[9]
after func1 : out1->[81]
main : func1 out[81]
```

## 对象内函数拦截器

代码示例：`test_intercept_2_1.z`

该代码示范了对对象`test1`的函数`func1`的拦截。

```
import stdtypes stdio ;

intercept before `test1.func1(int)`( object o , functionptr f , array in_params , array out_params )
{
        stdout.FormatPrintln( "before test1.func1" );

        return;
}

intercept after `test1.func1(int)`( object o , functionptr f , array in_params , array out_params )
{
        stdout.FormatPrintln( "after test1.func1" );

        return;
}

object test1
{
        function int func1( int in0 )
        {
                stdout.FormatPrintln( "func1" );

                return 0;
        }
}

function int main( array args )
{
        test1 t1 ;

        t1.func1( 3 );

        return 0;
}
```

执行

```
$ zlang test_intercept_2_1.z
before test1.func1
func1
after test1.func1
```

## 对象内属性拦截器

Java没有属性拦截器，为了扩展性就必须对对象的每一个属性都加上`Get`、`Set`方法，要求调用方通过`Get`、`Set`来访问内部属性，也就是说为了扩展将来极小概率的自定义读写属性，而要求所有属性都必须用这种繁琐的写法来访问属性，其实大可不必，赋值语句用`=`来表达是最直接、简单、自然的写法，写成`Set...(...)`完全是自给自己找麻烦，那么赋值语句回归`=`后如何解决将来的自定义读写需求呢，`zlang`引入了属性拦截器能力，当将来需要自定义读写属性时，针对需要的属性写相应的拦截器即可，这样保证其他大多数赋值语句的易读和开发效能。对比一下：

`Java`

```
i.setNickName(msg.header.nickName);
```

`zlang`

```
i.nick_name = msg.header.nick_name ;
```

你觉得哪个更符合赋值语义的易读表达？

属性拦截器语句的形式为

```
object ...
{
	intercept get|set 属性名为拦截器函数名( object 属性名为输入参数名 )
	{
		修改属性值的代码
		
		return;
	}
}
```

代码示例：`test_object_21.z`

该代码示范了对`greasy_man`对象的内含属性`name`的读拦截、`age`的写拦截。

```
import stdtypes stdio ;

object greasy_man
{
        public string   name ;
        public int      age ;

        intercept get name( object name )
        {
                if( name == null || name == "" )
                        name = "" ;
                else
                        name += "@Hangzhou" ;
                return;
        }

        intercept set age( object age )
        {
                if( age < 0 )
                        age = 0 ;
                else if( age > 200 )
                        age = 200 ;
                return;
        }
}

function int main( array args )
{
        greasy_man      i ;

        i.name = "calvin" ;
        stdout.FormatPrintln( "i.name[%s]" , i.name );

        i.age = 296 ;
        stdout.FormatPrintln( "i.age[%d]" , i.age );

        return 0;
}
```

执行

```
$ zlang  test_object_21.z
i.name[calvin@Hangzhou]
i.age[200]
```



下一章：[注释](remark.md)