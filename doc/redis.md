上一章：[数据库](database.md)



**章节目录**
- [Redis](#redis)



# Redis

Redis对象库用于应用连接、操作Redis处理。

zlang数据库对象库基于开源库`hiredis`封装。

Redis对象名：`redis`

| 成员类型 | 成员名字和原型            | 说明                                               | (错误.)(异常码,)异常消息                                |
| :------: | ------------------------- | -------------------------------------------------- | -------------------------------------------------- |
|   常量   | string redis.NX | 用于SET指令的不存在条件选项         |          |
|   常量   | string redis.XX | 用于SET指令的存在条件选项         |          |
|   函数   | bool Connect(string,int); | 连接Redis服务端，输入参数为Redis的IP、PORT         | EXCEPTION_MESSAGE_CONNECT_REDIS_SERVER_FAILED |
|   函数   | void Disconnect();        | 断开Redis连接                                      |                                       |
|   函数   | int DEL(...);             | 删除键 | EXCEPTION_MESSAGE_CONNECT_REDIS_SERVER_FAILED<br />EXCEPTION_MESSAGE_FETCH_CONNECTION_FAILED<br /><br />EXCEPTION_MESSAGE_EXECUTE_REDIS_COMMAND_FAILED<br />EXCEPTION_MESSAGE_REDIS_RESPONSE_TYPE_NOT_MATCHED |
| 函数 | int EXISTS(string key); | 检查是否存在 | （同上） |
| 函数 | array KEYS(string keys); | 查询通配的键清单 | （同上） |
|   函数   | bool SET(string key,string value); | 设置键值                                    | （同上）                                |
|   函数   | string GET(string key);   | 用键得到值，成功返回值，失败返回null            | （同上）        |
|   函数   | array MGet(string key[, string key]); | 用多个键得到多个值，成功返回字符串数组，失败返回null | （同上） |
| 函数 | int HSET(string key,string field,string value); | 设置哈希键、域、值，创建新值返回1，覆盖旧值返回0，失败返回null | （同上） |
| 函数 | string HGET(string key,string field); | 用哈希键、域查询得到值，找到返回值，找不到返回null | （同上） |
| 函数 | bool HMSET(string key,string field,string value[,string field,string value...]); | 设置键的多个域和值，成功返回TRUE，失败返回null | （同上） |
| 函数 | array HMGET(string key,string field[, string field...]); | 用哈希键、多个域查询得到多个值，单个找不到返回数组成员null | （同上） |
| 函数 | array HGETALL(string key); | 用哈希键查询所有域，键不存在则返回null | （同上） |
| 函数 | array HKEYS(string key); | 查询通配键的哈希键数组，键不存在则返回null | （同上） |
| 函数 | array HVALS(string key); | 查询通配键的哈希值数组，键不存在则返回null | （同上） |
| 函数 | int LPUSH(string key,string value[, string value]); | 压入键和多个值到左边，返回压入后的列表长度，失败则返回null | （同上） |
| 函数 | string LPOP(string key); | 从左边弹出键的一个值，失败返回null | （同上） |
| 函数 | int RPUSH(string key,string value[, string value]); | 压入键和多个值到右边，返回压入后的列表长度，失败则返回null | （同上） |
| 函数 | int RPOP(string key); | 从右边弹出键的一个值，失败返回null | （同上） |
| 函数 | int LLEN(string key); | 得到键的数量 | （同上） |
| 函数 | array LRANGE(string key,int start,int stop); | 得到队列第start个到第stop个元素，0是第一个，-1是最后一个，-2是倒数第二个，键不存在则返回null | （同上） |
| 函数 | array BLPOP(string key[,key...],timeout]); | 堵塞的从弹出左边，超时返回null | （同上） |
| 函数 | array BRPOP(string key[,key...],timeout]); | 堵塞的从弹出右边，超时返回null | （同上） |
| 函数 | int SADD(string key,string member[,string member...]); | 返回列表添加元素数量 | （同上） |
| 函数 | int SREM(string key,string member[,string  member...]); | 返回列表删除元素数量 | （同上） |
| 函数 | int SCARD(string key); | 返回列表键的值数量 | （同上） |
| 函数 | int SISMEMBER(string key,string member); | 返回列表键的值是否存在，存在返回1，不存在返回0 | （同上） |
| 函数 | array SMEMBERS(string key); | 返回列表键的值数组 | （同上） |
| 函数 | array SINTER(string key[,string key...]); | 返回多个列表键的交集 | （同上） |
| 函数 | array SDIFF(string key[,string key ...]); | 返回多个列表键的差集 | （同上） |
| 函数 | array SUNION(string key[,string key...]); | 返回多个列表键的并集 | （同上） |
| 函数 | int ZADD(string key,string score,string member[,string score,string member...]); | 把分数（字符串类型）和值添加到有序列表中，返回新增数量 | （同上） |
| 函数 | int ZREM(string key,string member[,string member...]); | 从有序列表中删除值，返回删除数量 | （同上） |
| 函数 | string ZSCORE(string key,string member); | 返回有序列表键的值的分数（字符串类型） | （同上） |
| 函数 | int ZRANK(string key,string member); | 返回有序列表键的值的排名，0是第一个 | （同上） |
| 函数 | int ZCARD(string key); | 返回有序列表键的值的数量 | （同上） |
| 函数 | int ZCOUNT(string key,string min_score,string max_score); | 返回有序列表键的[]分数1,分数2]之间的值的数量 | （同上） |
| 函数 | array ZRANGE(string key,int start,int stop[,WITHSCORES]); | 返回有序列表键的第start个到第stop个的元素数组，0是第一个，-1是最后一个，-2是倒数第二个，键不存在则返回null | （同上） |

客户端连接池会可选的创建一个独立线程：助手线程，每隔InspectTimeval微秒醒过来一次，每隔WatchIdleTimeval秒自动向空闲连接发心跳，如果当前空闲数量大于该值，每隔MaxIdleTimeval秒自动清理一个空闲连接。

| 成员类型 | 成员名字和原型                           | 说明                                                         |
| :------: | ---------------------------------------- | ------------------------------------------------------------ |
|   函数   | void EnableAssistantThread(bool enable); | 是否附带开启助手线程，enable=true开启，enable=false不开启。助手线程是一个独立线程，用于定期执行观察函数、清理过多的连接等旁路处理 |
|   函数   | void SetMinIdleConnectionsCount(int);    | 设置连接池最小空闲连接数量；缺省值1                          |
|   函数   | void SetMaxIdleConnectionsCount(int);    | 设置连接池最大空闲连接数量；缺省值2                          |
|   函数   | void SetMaxConnectionsCount(int);        | 设置连接池最大连接数量，空闲加工作连接数量绝不允许超过该值；缺省值2 |
|   函数   | void SetMaxIdleTimeval(int);             | 如果当前空闲数量大于该值，每隔MaxIdleTimeval秒自动清理一个空闲连接；缺省值300秒 |
|   函数   | void SetWatchIdleTimeval(int);           | 每隔WatchIdleTimeval秒自动向空闲连接发心跳；缺省值60秒       |
|   函数   | void SetInspectTimeval(int);             | 每隔InspectTimeval微秒连接池独立线程醒过来一次；缺省值1000000微秒（1秒） |

代码示例：`test_redis_SET_GET_1.z`

该代码示范了连接Redis服务端，设置KV字符串，查询KV字符串。

```
import stdtypes stdio redis ;

function int main( array args )
{
	bool	b ;
	
	b = redis.Connect( "127.0.0.1" , 6379 ) ;
	if( b != true )
	{
		stdout.Println( "redis.Connect failed" );
		return 1;
	}
	else
	{
		stdout.Println( "redis.Connect ok" );
	}
	
	redis.DEL( "MYKEY-1" );
	
	b = redis.SET( "MYKEY-1" , "MYVALUE-1" , null , null , null ) ;
	if( b == null )
	{
		stdout.Println( "redis.SET failed" );
		redis.Disconnect() ;
		return 1;
	}
	else
	{
		stdout.Println( "redis.SET ok" );
	}
	
	myvalue = redis.GET( "MYKEY-1" ) ;
	stdout.FormatPrintln( "redis.GET [MYKEY1] [%s]" , myvalue );
	
	stdout.Println( "redis.Disconnect" );
	redis.Disconnect() ;
	
	return 0;
}
```

代码示例：`test_redis_HSET_HGET_1.z`

该代码示范了连接Redis服务端，设置哈希值，查询哈希值。

```
import stdtypes stdio redis ;

function int main( array args )
{
	bool	b ;
	
	b = redis.Connect( "127.0.0.1" , 6379 ) ;
	if( b != true )
	{
		stdout.Println( "redis.Connect failed" );
		return 1;
	}
	else
	{
		stdout.Println( "redis.Connect ok" );
	}
	
	redis.DEL( "MYHKEY-1" );
	
	n = redis.HSET( "MYHKEY-1" , "MYHFIELD-1" , "MYHVALUE-1" ) ;
	if( n == null )
	{
		stdout.Println( "redis.HSET failed" );
		return 1;
	}
	else
	{
		stdout.Println( "redis.HSET ok" );
	}
	
	myvalue = redis.HGET( "MYHKEY-1" , "MYHFIELD-1" ) ;
	stdout.FormatPrintln( "redis.HGET [MYHKEY-1][MYHFIELD-1] [%s]" , myvalue );
	
	stdout.Println( "redis.Disconnect" );
	redis.Disconnect() ;
	
	return 0;
}
```

代码示例：`test_redis_LPUSH_RPOP_1.z`

该代码示范了连接Redis服务端，设置列表值，查询列表值。

```
import stdtypes stdio redis ;

function int main( array args )
{
	b = redis.Connect( "127.0.0.1" , 6379 ) ;
	if( b != true )
	{
		stdout.Println( "redis.Connect failed" );
		return 1;
	}
	else
	{
		stdout.Println( "redis.Connect ok" );
	}
	
	redis.DEL( "MYLIST-1" );
	
	n = redis.LPUSH( "MYLIST-1" , "MYMSG-1" , "MYMSG-2" , "MYMSG-3" , "MYMSG-4" , "MYMSG-5" ) ;
	if( n == null )
	{
		stdout.Println( "redis.LPUSH failed" );
		return 1;
	}
	else
	{
		stdout.Println( "redis.LPUSH ok" );
	}
	
	for( int i = 0 ; i < 5 ; i++ )
	{
		mymsg = redis.RPOP( "MYLIST-1" ) ;
		stdout.FormatPrintln( "redis.RPOP [MYLIST-1] [%s]" , mymsg );
	}
	
	stdout.Println( "redis.Disconnect" );
	redis.Disconnect() ;
	
	return 0;
}
```

代码示例：`test_redis_SADD_SCARD_SISMEMBER_SMEMBERS_SREM_1.z`

该代码示范了连接Redis服务端，设置有序列表值，查询有序列表值。

```
import stdtypes stdio redis ;

function int main( array args )
{
	b = redis.Connect( "127.0.0.1" , 6379 ) ;
	if( b != true )
	{
		stdout.Println( "redis.Connect failed" );
		return 1;
	}
	else
	{
		stdout.Println( "redis.Connect ok" );
	}
	
	redis.DEL( "MYSET-1" );
	
	n = redis.SADD( "MYSET-1" , "MYMEM-1" , "MYMEM-2" , "MYMEM-3" ) ;
	if( n == null )
	{
		stdout.Println( "redis.SADD failed" );
		return 1;
	}
	else
	{
		stdout.Println( "redis.SADD ok" );
	}
	
	count = redis.SCARD( "MYSET-1" ) ;
	stdout.FormatPrintln( "redis.SCARD [%d]" , count );
	
	n = redis.SISMEMBER( "MYSET-1" , "MYMEM-2" ) ;
	stdout.FormatPrintln( "redis.SISMEMBER [%d]" , n );
	
	n = redis.SISMEMBER( "MYSET-1" , "MYMEM-4" ) ;
	stdout.FormatPrintln( "redis.SISMEMBER [%d]" , n );
	
	a = redis.SMEMBERS( "MYSET-1" ) ;
	foreach( string s in a )
	{
		stdout.FormatPrintln( "redis.SMEMBERS [MYSET-1] [%s]" , s );
	}
	
	n = redis.SREM( "MYSET-1" , "MYMEM-1" , "MYMEM-2" , "MYMEM-3" ) ;
	if( n == null )
	{
		stdout.Println( "redis.SREM failed" );
		return 1;
	}
	else
	{
		stdout.Println( "redis.SREM ok" );
	}
	
	count = redis.SCARD( "MYSET-1" ) ;
	stdout.FormatPrintln( "redis.SCARD [%d]" , count );
	
	stdout.Println( "redis.Disconnect" );
	redis.Disconnect() ;
	
	return 0;
}
```

代码示例：`test_redis_ZADD_ZCARD_ZRANGE_ZCOUNT_ZSCORE_ZRANK_1.z`

该代码示范了连接Redis服务端，设置分数-列表值，查询分数-列表值。

```
import stdtypes stdio redis ;

function int main( array args )
{
	b = redis.Connect( "127.0.0.1" , 6379 ) ;
	if( b != true )
	{
		stdout.Println( "redis.Connect failed" );
		return 1;
	}
	else
	{
		stdout.Println( "redis.Connect ok" );
	}
	
	redis.DEL( "MYZET-1" );
	
	n = redis.ZADD( "MYZET-1" , "10" , "MYMEM-1" , "20" , "MYMEM-2" , "30" , "MYMEM-3" ) ;
	if( n == null )
	{
		stdout.Println( "redis.ZADD failed" );
		return 1;
	}
	else
	{
		stdout.Println( "redis.ZADD ok" );
	}
	
	count = redis.ZCARD( "MYZET-1" ) ;
	stdout.FormatPrintln( "redis.ZCARD [%d]" , count );
	
	a = redis.ZRANGE( "MYZET-1" , 0 , -1 ) ;
	foreach( string s in a )
	{
		stdout.FormatPrintln( "redis.ZRANGE [MYZET-1] [%s]" , s );
	}
	
	count = redis.ZCOUNT( "MYZET-1" , "15" , "25" ) ;
	stdout.FormatPrintln( "redis.ZCOUNT [%d]" , count );
	
	score = redis.ZSCORE( "MYZET-1" , "MYMEM-2" ) ;
	stdout.FormatPrintln( "redis.ZSCORE [%s]" , score );
	
	rank = redis.ZRANK( "MYZET-1" , "MYMEM-3" ) ;
	stdout.FormatPrintln( "redis.ZRANK [%d]" , rank );
	
	stdout.Println( "redis.Disconnect" );
	redis.Disconnect() ;
	
	return 0;
}
```



下一章：[JSON序列化/反序列化](json.md)